<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('css/inspire/inspire.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">
</head>
<body>
    <div class="container">
        <div class="invoice-container">
            <div class="row">
                <div class="col-md-7">
                    <img src="{{asset('img/logo.png')}}" class="img-invoice" alt="imga">
                    <h3>sales invoice</h3>
                </div>
                <div class="col-md-5">
                    <div class="order-details">
                        <h4>Skywater Tower 6, Otunba Oshikoya drive, Mobolaji Bank Anthony way, Ikeja. Lagos. Nigeria</h4>

                        <table class="table">
                            <tr>
                                <td style="vertical-align: middle">Order No</td>
                                <td><span class="orderNum">1004534353</span></td>
                            </tr>
                            <tr>
                                <td>Order Date</td>
                                <td>7-Feb-2019</td>
                            </tr>
                            <tr>
                                <td>Deliver To:</td>
                                <td>Falua Oyewole</td>
                            </tr>
                            <tr>
                                <td>Delivery Method</td>
                                <td>Pick up</td>
                            </tr>
                            <tr>
                                <td>Invoice No:</td>
                                <td>100442424-W131</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="header">
                <div class="row">
                    <div class="col-md-6"><h5>Delivery details:</h5></div>
                    <div class="col-md-6"><h5>Delivery address</h5></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <td width="150px">Dispatch date:</td>
                            <td>8-Feb-2019</td>
                        </tr>
                        <tr>
                            <td>Order type:</td>
                            <td>Ready made</td>
                        </tr>
                        <tr>
                            <td>Amount Paid</td>
                            <td>N3,000</td>
                        </tr>
                        <tr>
                            <td>Return Tracking ID:</td>
                            <td>RTW-10043829000</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table">
                        <tr>
                            <td width="100px">Address</td>
                            <td style="vertical-align: middle;">Skywater Tower 6, Otunba Oshikoya drive, Mobolaji Bank Anthony way, Ikeja. Lagos.
                                Nigeria
                            </td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>Lagos</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>Nigeria</td>
                        </tr>
                        <tr>
                            <td>Contact:</td>
                            <td>07068912907</td>
                        </tr>
                    </table>
                </div>
            </div>

            <h5 class="d-title">Order details</h5>
            <div class="orderItemDetails">
                <table class="table">
                    <thead>
                        <th>Item No.</th>
                        <th>Item Name</th>
                        <th>Qty</th>
                        <th>Item Price</th>
                        <th>Subtotal</th>
                    </thead>
                    <tbody>
                    <tr style="text-align: center;">
                        <td>1</td>
                        <td>Patterned Shirt</td>
                        <td>1</td>
                        <td>N3000</td>
                        <td>3000</td>
                    </tr>
                    <tr style="text-align: center;">
                        <td>1</td>
                        <td>Patterned Shirt</td>
                        <td>1</td>
                        <td>N3000</td>
                        <td>3000</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-7"></div>
                <div class="col-md-5">
                    <table class="table">
                        <tr>
                            <td width="250px">Promo Code Value</td>
                            <td>0%</td>
                        </tr>
                        <tr>
                            <td>Discount</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td>Subtotal Less Discount</td>
                            <td>N3000</td>
                        </tr>
                        <tr>
                            <td>Shipping</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td>Total Paid</td>
                            <td>N3000</td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="content-user">
                        <h4>Dear Falua Oyewole,</h4>

                        <p>Thank you for shopping with us. We are delighted to have you as a customer</p>
                        <p>
                           Please note, we aim to deliver your items as fast as possible and some of your order may come in multiple deliveries.
                            <br>
                            If there is a problem with your product. Please call 0700wawooh (0700929664) or visit https://www.wawooh.com
                            <br>
                            Thank you again for shopping with us.

                            <br><br>
                            Sincerely. <br>
                            Wawooh Team.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>