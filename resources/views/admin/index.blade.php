<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Login &mdash; Wawooh | Express you</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('css/admin-wawooh.css')}}">
    <link rel="stylesheet" href="{{asset('css/wawooh.css')}}">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400|Open+Sans:100,300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400|Open+Sans:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu|Titillium+Web" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
</head>
<body>
<div class="login-wrapper">
    <div class="col-md-4 col-lg-4 offset-4">
        <div class="login-container">
            <div class="l-header">
                <img src="{{asset('img/waw-white.png')}}" alt="wawooh-logo">
                <h3>Welcome Back: Login </h3>
            </div>
            <div class="login-form animated zoomIn">
                <div class="login-body">
                    <div class="auth-alert"></div>

                    <form name="login" action="">
                        <div class="form-group">
                            <label for="username">Email Address:</label>
                            <div class="input-group">
                                <input type="email" id="email" name="username" placeholder="Enter Email Address"
                                       class="form-control">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <div class="input-group">
                                <input type="password" id="password" name="password" placeholder="Password"
                                       class="form-control">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <input type="button" id="admLog" name="submit" class="btn btn-submit submit"
                                   value="SIGN IN">
                        </div>
                    </form>
                </div>
            </div>
            <div class="l-footer"><span> &copy; <?= date('Y'); ?> Made with<span class="love">&hearts;</span>From Wawooh Integrated Services</span>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/validation.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.submit').click(function () {
            if (isNotEmpty('.form-control')) {
                let data = {
                    "email": $('#email').val(),
                    "password": $('#password').val(),
                    "socialFlag": 'N'
                };
                $('#admLog').val('Please wait...').attr('disabled', 'disabled');
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: `{{route('admin.login.auth')}}`,
                    success: function (result) {
                        console.log(result);
                        if (result.status === '00') {
                            if (result.data.role === 3) {
                                $('.form-control').removeClass('error');
                                var id = result.data.id;
                                localStorage.setItem('adminId', JSON.stringify(id));
                                $('#admLog').val('Login Successfully. Redirecting...').attr('disabled', 'disabled');
                                window.location.href = '{{route('admin.dashboard')}}';
                            }
                            else if (result.data.role === 5) {
                                $('.form-control').removeClass('error');
                                $('#admLog').val('Login Successfully. Redirecting...').attr('disabled', 'disabled');
                                window.location.href = '{{route('qa.dashboard')}}';
                            }
                            else if (result.data.role === 6) {
                                $('.form-control').removeClass('error');
                                $('#admLog').val('Login Successfully. Redirecting...').attr('disabled', 'disabled');
                                window.location.href = '{{route('customer.dashboard')}}';
                            } else if (result.data.role === 7) {
                                $('.form-control').removeClass('error');
                                $('#admLog').val('Login Successfully. Redirecting...').attr('disabled', 'disabled');
                                window.location.href = '{{route('auditor.dashboard')}}'
                            }
                            else {
                                $('#admLog').val('SIGN IN').removeAttr('disabled', 'disabled');
                                swal('User is not a admin', '', 'error');
                                $('.form-control').removeClass('error');
                            }
                        } else if (result.status === "99") {
                            $('.form-control').removeClass('error');
                            swal(result.message, "", "error");
                            $('#admLog').val('SIGN IN').removeAttr('disabled', 'disabled');
                        } else {
                            $('.form-control').removeClass('error');
                            $('#admLog').val('SIGN IN').removeAttr('disabled', 'disabled');
                            $('#signEmail').notify(result.message, 'error');
                        }
                    },
                    error: function (e) {
                        $('.form-control').removeClass('error');
                        $('.submit').removeAttr('disabled');
                        swal(e, '', 'error');
                        $('#admLog').val('SIGN IN').removeAttr('disabled', 'disabled');
                    },
                });
            } else {
                $('.form-control').addClass('error');
                swal('Please, enter valid login credentials', '', 'warning');
            }
        })
    });
</script>
</body>
</html>