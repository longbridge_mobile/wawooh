@extends('layouts.admin.default')

@push('styles')

@endpush


@section('pageTitle','Tags')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Manage Tag Events</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-home"></i> Events</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> Manage Tags</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white tag-list">
                <div class="row">

                </div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class=""><a href="#all" aria-controls="all" role="tab"
                                                        data-toggle="tab">All</a></li>
                    <li role="presentation" id="taggedShow"><a href="#tagged" aria-controls="tagged" role="tab"
                                                               data-toggle="tab">Tagged</a></li>
                    <li role="presentation" id="untaggedShow"><a href="#untagged" aria-controls="untagged" role="tab"
                                                                 data-toggle="tab">Untagged</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content m-top-2">
                    <div role="tabpanel" class="tab-pane active" id="all">
                        <div id="js-grid-masonry" class="cbp">
                            @if(isset($unTaggedPictures))
                                @if(count($unTaggedPictures))
                                    @foreach($unTaggedPictures as $unTaggedPicture)
                                        <div class="cbp-item">
                                            <a href="{{route('admin.tags.images',$unTaggedPicture->id)}}"
                                               class="cbp-caption cbp-lightbox">
                                                <div class="cbp-caption-defaultWrap"><img
                                                            src="{{$unTaggedPicture->picture}}" alt=""></div>
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                                @else
                                <label class="label label-warning">No Record Found</label>
                            @endif
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tagged">
                        <div id="js-grid-masonry2" class="cbp">
                            @if(isset($taggedPictures))
                                @if(count($taggedPictures))
                                    @foreach($taggedPictures as $TaggedPicture)
                                        <div class="cbp-item">
                                            <a href="{{route('admin.tags.images',$TaggedPicture->id)}}"
                                               class="cbp-caption cbp-lightbox">
                                                <div class="cbp-caption-defaultWrap"><img
                                                            src="{{$TaggedPicture->picture}}" alt=""></div>
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            @else
                                <p>No Tagged Image</p>
                            @endif
                        </div>
                        @if(isset($taggedPictures))
                            <div>
                                {!! $taggedPictures->render() !!}
                            </div>
                        @endif
                    </div>

                    <div role="tabpanel" class="tab-pane" id="untagged">
                        <div id="js-grid-masonry3" class="cbp">
                            <div class="cbp-item">hello3</div>
                            @if(isset($unTaggedPictures))
                                @if(count($unTaggedPictures))
                                    @foreach($unTaggedPictures as $unTaggedPicture)
                                        <div class="cbp-item">
                                            <a href="{{route('admin.tags.images',$unTaggedPicture->id)}}"
                                               class="cbp-caption cbp-lightbox">
                                                <div class="cbp-caption-defaultWrap"><img
                                                            src="{{$unTaggedPicture->picture}}" alt=""></div>
                                            </a>
                                        </div>
                                    @endforeach
                                @endif
                            @else
                                <p>No Untagged Image</p>
                            @endif
                        </div>
                        @if(isset($unTaggedPictures))
                            <div>
                                {!! $unTaggedPictures->render() !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/cubeportfolio/main.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#all').addClass('active');
        });

        $('#taggedShow').click(function () {
            $('#js-grid-masonry3').removeClass('cbp');
        });

        $('#untaggedShow').click(function () {
            $('#js-grid-masonry2').removeClass('cbp');
        });
    </script>
@endpush