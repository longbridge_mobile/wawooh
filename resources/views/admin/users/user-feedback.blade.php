@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','User Feedback')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-mail bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>User's Feedback</h5>
                                    <span>General feedback about site, product, enquiries, etc</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Feedback</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped category-table" id="transferInfo">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Sender</th>
                                                            <th scope="col">Message</th>
                                                            <th scope="col">Date</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($feedbacks))
                                                            @if(count($feedbacks))
                                                                @foreach($feedbacks as $feedback)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$feedback->fullName}} <br> {{$feedback->phoneNumber}}
                                                                            <br> <a href="mailto:{{$feedback->email}}">{{$feedback->email}}</a>
                                                                        </td>
                                                                        <td><span style="line-height: 30px;">{{$feedback->message}}</span></td>
                                                                        <td>{{\Carbon\Carbon::createFromTimestamp($feedback->createdOn / 1000)->format('dS M, Y')}}
                                                                            <br> <small class="text-success text-centerC">({{\Carbon\Carbon::createFromTimestamp($feedback->createdOn / 1000)->diffForHumans()}})</small>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>No Feedback at the moment</p>
                                                        @endif


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush