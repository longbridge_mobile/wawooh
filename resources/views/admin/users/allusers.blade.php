@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Registered Users')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-user-plus bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>All Users</h5>
                                    <span>The list contain both designers, users and admin level</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">All Users</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-bordered table-striped" id="usersTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Email</th>
                                                            <th scope="col">Phone</th>
                                                            <th scope="col">Role</th>
                                                            <th scope="col">Date of Reg</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($users))
                                                            @if(count($users))
                                                                @foreach($users as $user)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td width="200px">{{$user->firstName}} {{$user->lastName}}</td>
                                                                        <td>{{$user->email}}</td>
                                                                        <td>{{$user->phoneNo}}</td>
                                                                        <td>@if ($user->role === 'admin')
                                                                                <label class="label label-danger">Super Admin</label>
                                                                            @elseif($user->role === 'user')
                                                                                <label class="label label-processing">User</label>
                                                                            @elseif ($user->role === 'designer')
                                                                                <label class="label label-completed">Designer</label>
                                                                            @elseif ($user->role === 'qa')
                                                                                <label class="label label-info">Quality Assurance</label>
                                                                            @endif</td>
                                                                        <td>{{\Carbon\Carbon::createFromTimestamp($user->createdOn / 1000)->format('dS M, Y')}}
                                                                            <br>
                                                                            <small>
                                                                                ({{Carbon\Carbon::createFromTimestamp($user->createdOn / 1000)->diffForHumans()}}
                                                                                )
                                                                            </small>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    {{--<a href="{{route('exportUsers')}}" class="pull-right btn btn-primary">Export Users--}}
                                                        {{--Data</a>--}}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
