@extends('layouts.admin.default')

@push('styles')
  
@endpush



@section('content')
<div class="content-wrapper"> 
    <section class="content-header">
        <h1>User Details</h1>
        <ol class="breadcrumb">
            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="{{route('admin.events')}}"><i class="fa fa-home"></i> User</a></li>
            <li class="active"><i class="fa fa-dashboard"></i> User Details</li>
        </ol>
    </section>
    <section class="container">
                    <div class="row">
                        <div class="col-lg-4 col-xs-4">
                            <div class="media-box bg-wawooh">
                                <div class="media-icon pull-left"><i class="icon-bargraph"></i> </div>
                                <div class="media-info">
                                    <h5>No of orders</h5>
                                    <h3>10</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <div class="media-box">
                                <div class="media-icon pull-left"><i class="icon-wallet"></i> </div>
                                <div class="media-info c-wawooh">
                                    <h5>Total amount spent</h5>
                                    <h3>$8,530</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                            <div class="media-box">
                                <div class="media-icon pull-left"><i class="icon-basket"></i> </div>
                                    <div class="media-info c-wawooh">
                                        <h5>No of Pending order</h5>
                                        <h3>2</h3>
                                    </div>
                            </div>
                        </div>
                    </div>
    </section>

    <aside class='container-fluid'>
        <div class='bg-white'>
                    <div class=''>
                        <h4 class="makeBold brand-color">User Information</h4>
                        <hr/>
                            <div class=''>
                                <table class='table'>
                                    <tr>
                                        <th>User Name: </td>
                                        <td>Marcia</td>
                                    </tr>
                                    <tr>
                                        <th>Email Address: </td>
                                        <td>marcigeoca28@gmail.com</td>
                                    </tr>
                                    <tr>
                                        <th>Phone Number: </td>
                                        <td>08100969815</td>
                                    </tr>
                                    <tr>
                                        <th>Address: </td>
                                        <td>Iyana-Era B/stop</td>
                                    </tr>
                                    <tr>
                                        <th>Date Joined:</td>
                                        <td>2017</td>
                                    </tr>
                                </table>
                            </div>  
                    </div>
        </div>

    </aside>
    <section class="content container-fluid">
      <div class="bg-white userOrdered">
          <h4 class="brand-color makeBold">Ordered Item</h4>
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#allOrders" aria-controls="allOrders" role="tab" data-toggle="tab">All</a></li>
            <li role="presentation"><a href="#pending" aria-controls="pending" role="tab" data-toggle="tab">Pending</a></li>
            <li role="presentation"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab">Completed</a></li>
          </ul>

          <div class="tab-content m-top-2">
            <div role="tabpanel" class="tab-pane active" id="allOrders">
                <section class="container-fluid"> 
                    
                    <div class="row style-heading">
							<div class="col-lg-6 col-md-6 col-6">
								<p>Item</p>
							</div>
							<div class="col-lg-2 col-md-2 col-6">
								<p class="moveCenter">Quantity</p>
							</div>
							<div class="col-lg-2 col-md-2">
								<p class="moveCenter">Total Price</p>
							</div>
							<div class="col-lg-2 col-md-2">
								<p class="moveCenter">Status</p>
							</div>
					</div>
                    <div class="row">
											<div class="col-md-6 col-6">
												<div class="row">
													<div class="col-md-3 col-12">
														<img src="{{ asset('img/img12.jpg')}}" class="cart-img" alt="product-image">
													</div>
													<div class="col-md-9 col-12">
														<p class="cart-des">Addidas Sandals</p>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-2">
												<select class="form-control" disabled>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </select>
											</div>
											<div class="col-md-2 col-2">
												<p class="moveCenter">N25,000</p>
												
											</div>
											<div class="col-md-2">
												<p class="userOrderStatus btn-danger" >Returned</p>
											</div>
					</div>
                </section>
            </div>
            <div role="tabpanel" class="tab-pane" id="pending">
                <section class="container-fluid"> 
                    
                    <div class="row style-heading">
							<div class="col-lg-6 col-md-6 col-6">
								<p>Item</p>
							</div>
							<div class="col-lg-2 col-md-2 col-6">
								<p class="moveCenter">Quantity</p>
							</div>
							<div class="col-lg-2 col-md-2">
								<p class="moveCenter">Total Price</p>
							</div>
							<div class="col-lg-2 col-md-2">
								<p class="moveCenter">Status</p>
							</div>
					</div>
                    <div class="row">
											<div class="col-md-6 col-6">
												<div class="row">
													<div class="col-md-3 col-12">
														<img src="{{ asset('img/img13.jpg')}}" class="cart-img" alt="product-image">
													</div>
													<div class="col-md-9 col-12">
														<p class="cart-des">Addidas Sandals</p>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-2">
												<select class="form-control" disabled>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </select>
											</div>
											<div class="col-md-2 col-2">
												<p class="moveCenter">N25,000</p>
												
											</div>
											<div class="col-md-2">
												<p class="userOrderStatus btn-wawooh" >In Transit</p>
											</div>
					</div>
                </section>
            </div>
            <div role="tabpanel" class="tab-pane" id="completed">
                <section class="container-fluid"> 
                    
                    <div class="row style-heading">
							<div class="col-lg-6 col-md-6 col-6">
								<p>Item</p>
							</div>
							<div class="col-lg-2 col-md-2 col-6">
								<p class="moveCenter">Quantity</p>
							</div>
							<div class="col-lg-2 col-md-2">
								<p class="moveCenter">Total Price</p>
							</div>
							<div class="col-lg-2 col-md-2">
								<p class="moveCenter">Status</p>
							</div>
					</div>
                    <div class="row">
											<div class="col-md-6 col-6">
												<div class="row">
													<div class="col-md-3 col-12">
														<img src="{{ asset('img/img14.jpg')}}" class="cart-img" alt="product-image">
													</div>
													<div class="col-md-9 col-12">
														<p class="cart-des">Addidas Sandals</p>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-2">
												<select class="form-control" disabled>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </select>
											</div>
											<div class="col-md-2 col-2">
												<p class="moveCenter">N25,000</p>
												
											</div>
											<div class="col-md-2">
												<p class="userOrderStatus btn-success">Delivered</p>
											</div>
					</div>
                </section>
            </div>
          </div>
        </div>
    </section>
</div>

@endsection

@push('scripts')
<script src="{{ asset('js/utility.js') }}"></script>
<script src="{{ asset('js/admin/events.js') }}"></script>
  <script>
    $(document).ready(function(){

      $('#uploadPicture').on('change', async function(){
        var pix = [];
        counter = 0;
        $('#loader').removeClass('hide');
            for (var i = 0; i < $(this).get(0).files.length; ++i) {                            
                pix.push(await toBase64($(this).get(0).files[i]));
             }
            console.log(pix);                  
            displayUploadedImages(pix,'#more', counter);
    });

     //delete an image
     $(document).on('click','.delete', function(){
      var target = $(this).attr('data-target');
      $(target).remove();
    });
     $(document).on('click','.deleteImage', function(){
       var answer = confirm('Are you sure you want to continue. Click ok to continue');
       if(answer){
          var checked = $('input[name=check]:checked');
          var arr = [];
          $(checked).each(function(){
            arr.push($(this).attr('data-target'));
          });
          var data = {
            //"id" : "{{request('event')}}",
            "ids": arr,
          }

            console.log(data);

          $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',                        
                url: '{{route('admin.events.deletePictures')}}',
                success: function(result){
                    console.log(result);
                    $('#loader').hide();
                    if(result.status == 0){
                        location.reload();
                    }else{
                      $.notify(result.message, 'error');
                    }
                },
                error: function(e){
                    $('#loader').hide();                
                    console.log(e)
                },
          });
       }
     


    });
      $('.editInfo').on('click', function(){
        $('.eventDetails').hide();
        $('input.eventDetails,textarea.eventDetails').show().removeClass('hide');
        $('#x').show();
        $(this).hide();
      });

      $('.cancel').on('click', function(){
        $('#x').hide();
        $('.editInfo').show();
        $('.eventDetails').show();
        $('input.eventDetails,textarea.eventDetails').hide().addClass('hide');
        
      });

      $('.selectAll').on('click', function(){
        
        if($('input[name=check]:checked').length == $('input[name=check]').length ){
          $('input[name=check]').prop('checked', false);
          $(this).text('Select All');
          $('.p').css({
            filter: 'none'
          }
          );
        }else{
          $(this).text('Deselect');
          $('input[name=check]').prop('checked', true);
          $('.p').css({
            filter: 'grayscale(100%)'
          }
          );
          
        }
      });
 
      $('[name=check]').on('change', function(){
        if($(this).is(":checked")){
          var target = $(this).attr('data-target');
          console.log(target);
          $(target).css({
            filter: 'grayscale(100%)'
          }
          );
        }else{
          var target = $(this).attr('data-target');
          $(target).css({
            filter: 'none'
          }
          );
        }
      });


      //save event info

      $('.saveInfo').click(function(){
        var data = {
          "id": "{{request('event')}}",
          "eventName": $('#eventName').val(),
          "location": $('#location').val(),
          "description": $('#description').val(),
          "eventDate": $('#date').val(),          
          "eventType": ($('#status:checked').val())?$('#status').val():'E'
        }

        console.log(data);

        $.ajax({
              type: 'POST',
              data: JSON.stringify(data),
              contentType: "application/json; charset=utf-8",
              dataType: 'json',                        
              url: '{{route('admin.events.updateInfo')}}',
              success: function(result){
                  console.log(result);
                  $('#loader').hide();
                  if(result.status == 0){
                      location.reload();
                  }else{
                    $.notify(result.message, 'error');
                  }
              },
              error: function(e){
                  $('#loader').hide();                
                  console.log(e)
              },
        });
      });
      //upload event pictures

      $('.saveImages').click(function(){
          var images = [];
         $('.pix').each(function(i,obj){
              images.push({
                  "picture": getImageSrc($(this)),
                  "pictureDesc": $(this).siblings('input.pictureName').val()
              })
          });
        var data = {
          "id": "{{request('event')}}",
          "eventPictures": images,
        }

        console.log(data);

        $.ajax({
              type: 'POST',
              data: JSON.stringify(data),
              contentType: "application/json; charset=utf-8",
              dataType: 'json',                        
              url: '{{route('admin.events.updateEventPicture')}}',
              success: function(result){
                  console.log(result);
                  $('#loader').hide();
                  if(result.status == 0){
                      location.reload();
                  }else{
                    $.notify(result.message, 'error');
                  }
              },
              error: function(e){
                  $('#loader').hide();                
                  console.log(e)
              },
        });
      });
    });
  </script>

		
@endpush