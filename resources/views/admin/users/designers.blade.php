@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Designers')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">

            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-user-x bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Registered Designers: <span
                                                class="badge badge-info">{{ count($designers) }}</span></h5>
                                    <span>All Registered designers on the platform</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.allusers')}}">All Users</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Designers Only</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table style="font-size: 13px;" class="table table-striped table-bordered"
                                                           id="designerTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Store Name</th>
                                                            <th scope="col">Email</th>
                                                            <th scope="col">Phone</th>
                                                            <th scope="col">Total Products</th>
                                                            <th scope="col">Date Joined</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @if(isset($designers))
                                                            @if(count($designers))
                                                                @foreach($designers as $designer)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$designer->firstName}} {{$designer->lastName}}</td>
                                                                        <td>
                                                                            <a style="color: deepskyblue;" href='{{route('admin.designer.view', $designer->id)}}'>{{$designer->storeName}}</a>
                                                                        </td>
                                                                        <td>{{$designer->email}}</td>
                                                                        <td>{{$designer->phoneNo}}</td>
                                                                        <td>{{$designer->numOfProducts}}</td>
                                                                        <td>{{\Carbon\Carbon::parse($designer->createdDate)->format('d-M-Y')}}
                                                                            <br>
                                                                            <small class="text-success">
                                                                                ({{\Carbon\Carbon::parse($designer->createdDate)->diffForHumans()}}
                                                                                )
                                                                            </small>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="{{route('exportDesigner')}}"
                                                       class="pull-right btn btn-primary">Export Designer
                                                        Data</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush