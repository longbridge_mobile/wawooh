@extends('layouts.admin.default')

@push('styles')
    <style>
        .hide {
            display: none;
        }
    </style>
@endpush

@section('pageTitle', $newresp->name. ' | Product Detail')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-home bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Products Management</h5>
                                    <span>Product Details for: {{$newresp->name}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Products</a></li>
                                    <li class="breadcrumb-item"><a href="#">Product Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="row productDetails">
                                            <div class="col-md-6">
                                                <div class="row product-img">
                                                    <div class="col-md-9 product-big-img productDisplay product-one productDisplayOne">
                                                        <a title="Click to zoom" data-fancybox
                                                           href="{{str_replace('http','https',@$newresp->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                            <img id='mainPicture'
                                                                 src="{{str_replace('http','https',@$newresp->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}"
                                                                 class="img-fluid main-p fitDiv">
                                                        </a>

                                                    </div>
                                                    <div class="col-md-3 product-small-img productThumb product-one productThumbOne">
                                                        @if(isset($newresp->productColorStyleDTOS[0]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[0]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[0]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>


                                                    <div class="col-md-9 hide product-big-img productDisplay product-two productDisplayTwo">
                                                        <a title="Click to zoom" data-fancybox
                                                           href="{{str_replace('http','https', @$newresp->productColorStyleDTOS[1]->productPictureDTOS[0]->picture)}}">
                                                            <img id='mainPicture'
                                                                 src="{{str_replace('http','https', @$newresp->productColorStyleDTOS[1]->productPictureDTOS[0]->picture)}}"
                                                                 class="img-fluid main-p fitDiv">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-two productThumbTwo">
                                                        @if(isset($newresp->productColorStyleDTOS[1]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[1]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[1]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                    <div class="col-md-9 hide product-big-img productDisplay product-three productDisplayThree">
                                                        <img id='mainPicture'
                                                             src="{{str_replace('http','https',@$newresp->productColorStyleDTOS[2]->productPictureDTOS[2]->picture)}}"
                                                             class="img-fluid main-p fitDiv">
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-three productThumbThree">
                                                        @if(isset($newresp->productColorStyleDTOS[2]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[2]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[2]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                    <div class="col-md-9 hide product-big-img productDisplay product-four productDisplayFour">
                                                        <img id='mainPicture'
                                                             src="{{@$newresp->productColorStyleDTOS[3]->productPictureDTOS[3]->picture}}"
                                                             class="img-fluid main-p fitDiv">
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-four productThumbFour">
                                                        @if(isset($newresp->productColorStyleDTOS[3]->productPictureDTOS))
                                                            )
                                                            @if(count($newresp->productColorStyleDTOS[3]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[3]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                    <div class="col-md-9 hide product-big-img productDisplay product-five productDisplayFive">
                                                        <img id='mainPicture'
                                                             src="{{@$newresp->productColorStyleDTOS[4]->productPictureDTOS[4]->picture}}"
                                                             class="img-fluid main-p fitDiv">
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-five productThumbFive">
                                                        @if(isset($newresp->productColorStyleDTOS[4]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[4]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[4]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-6 product-des">
                                                <p class="product-name">{{strtolower($newresp->name)}}</p>
                                                <p class="product-store">Seller: <a
                                                            href="{{route('admin.designer.view', $newresp->designerId)}}">{{$newresp->designerName}}</a>
                                                </p>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>Product Type</h6>
                                                        @if ($newresp->availability === 'Y' && $newresp->bespokeProductDTO === null)
                                                            <label class="label label-inspection">Ready To Wear</label>
                                                        @elseif($newresp->availability === 'Y' && $newresp->bespokeProductDTO !== null)
                                                            <label class="label label-processing">Bespoke</label>  and
                                                            <label class="label label-inspection">Ready To Wear</label>
                                                        @elseif($newresp->availability === 'N' && $newresp->bespokeProductDTO !== null)
                                                            <label class="label label-info">Bespoke Only</label>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h6>Product Status</h6>
                                                        @if($newresp->verifiedFlag === 'N')
                                                            <label class="label label-rejected">Not Verified</label>
                                                        @else
                                                            <label class="label label-completed">Verified</label>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row pt-2">
                                                    @if($newresp->slashedPrice > 0)
                                                        <div class="col-md-6">
                                                            <div class="productPrice">
                                                                <h5><strong><u>New Discounted Price: </u></strong></h5>
                                                                <p>
                                                                    ₦<span>{{number_format($newresp->slashedPrice)}}</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="productPrice">
                                                                <h5><strong><u>Old Price:</u></strong> </h5>
                                                                <p>₦<span>{{number_format($newresp->amount)}}</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="productPrice">
                                                                <h5>Percentage Discount:</h5>
                                                                <p> &dash;{{$newresp->percentageDiscount}}%</p>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="col-md-6">
                                                            <h5><strong><u>Price:</u></strong> </h5>
                                                            <p>₦<span>{{number_format($newresp->amount)}}</span></p>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="">
                                                            <h6><strong><u>Category</u></strong></h6>
                                                            <p>{{$newresp->categoryName}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="">
                                                            <h6>Sub Category: </h6>
                                                            <p>{{$newresp->subCategoryName}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6><strong><u>Summary</u></strong></h6>
                                                <p class="product-des-long">{{$newresp->prodSummary}}</p>

                                                <h6><strong><u>Description</u></strong></h6>
                                                <span class="product-des-long">{!! $newresp->description !!}
                                                </span>
                                                @if($newresp->inStock === 'N')
                                                    <h5 style="margin-bottom: 20px;"><span class="makeBold">Availability:</span>
                                                        <span>On Demand</span>
                                                    </h5>
                                                @else
                                                    <h5 style="margin-bottom: 20px;"><span class="makeBold">Availability:</span>
                                                        <span>In Stock</span>
                                                    </h5>
                                                @endif
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="product-name-label"><strong><u>Total Sales:</u></strong>
                                                            <span>{{$newresp->totalSales}}</span></p>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p class="product-name-label"><strong><u>Sales in Queue:</u></strong>
                                                            <span> {{$newresp->salesInQueue}}</span></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 productColorAdmin">
                                                    <p class="product-name-label"><strong><u>Colors</u></strong></p>
                                                    <ul>
                                                        @if(isset($newresp->productColorStyleDTOS))
                                                            @if(count($newresp->productColorStyleDTOS))
                                                                @php
                                                                    $var = 0;
                                                                @endphp
                                                                @foreach($newresp->productColorStyleDTOS as $colorPicture)
                                                                    <div class="c-label">{{$colorPicture->colourName}}</div>
                                                                    <li class="pColor">
                                                                        <img imageId="{{$colorPicture->id}}"
                                                                             src="{{str_replace(['http','upload'],['https','upload/q_40'],$colorPicture->colourPicture)}}"
                                                                             alt="1"
                                                                             class="addProductAttribute"
                                                                             productCount="{{$var++}}">
                                                                    </li>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </ul>
                                                </div>
                                                @if(isset($newresp->unVerifiedReason) && $newresp->verifiedFlag == 'N')
                                                    <div class="row mt-3">
                                                        <div class="col-md-12 unverifyBlock">
                                                            <h6><strong><u>Reason</u></strong></h6>
                                                            <p>{{$newresp->unVerifiedReason}}</p>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="col-md-12 no-padding" style="padding-top: 15px;">
                                                    <div class="product-size seller-size">
                                                        <p class="product-name-label"><strong><u>Sizes</u></strong></p>
                                                        <div class="size-inline thumbD thumbOneSize">
                                                            @if(isset($newresp->productColorStyleDTOS[0]->productSizes) && is_array($newresp->productColorStyleDTOS[0]->productSizes))
                                                                @if(count($newresp->productColorStyleDTOS[0]->productSizes))
                                                                    @foreach($newresp->productColorStyleDTOS[0]->productSizes as $sizes)
                                                                        <div>{{$sizes->name}}</div>
                                                                        <div>{{$sizes->numberInStock}}</div>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>

                                                        <div class="size-inline thumbD hide thumbTwoSize">
                                                            @if(isset($newresp->productColorStyleDTOS[1]->productSizes) && is_array($newresp->productColorStyleDTOS[1]->productSizes))
                                                                @if(count($newresp->productColorStyleDTOS[1]->productSizes))
                                                                    @foreach($newresp->productColorStyleDTOS[1]->productSizes as $sizes)
                                                                        <div>{{$sizes->name}}</div>
                                                                        <div>{{$sizes->numberInStock}}</div>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>

                                                        <div class="size-inline thumbD hide thumbThreeSize">
                                                            @if(isset($newresp->productColorStyleDTOS[2]->productSizes) && is_array($newresp->productColorStyleDTOS[2]->productSizes))
                                                                @if(count($newresp->productColorStyleDTOS[2]->productSizes))
                                                                    @foreach($newresp->productColorStyleDTOS[2]->productSizes as $sizes)
                                                                        <div>{{$sizes->name}}</div>
                                                                        <div>{{$sizes->numberInStock}}</div>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>

                                                        <div class="size-inline thumbD thumbFourSize">
                                                            @if(isset($newresp->productColorStyleDTOS[3]->productSizes) && is_array($newresp->productColorStyleDTOS[3]->productSizes))
                                                                @if(count($newresp->productColorStyleDTOS[3]->productSizes))
                                                                    @foreach($newresp->productColorStyleDTOS[3]->productSizes as $sizes)
                                                                        <div>{{$sizes->name}}</div>
                                                                        <div>{{$sizes->numberInStock}}</div>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>

                                                        <div class="size-inline thumbD thumbFiveSize">
                                                            @if(isset($newresp->productColorStyleDTOS[4]->productSizes) && is_array($newresp->productColorStyleDTOS[4]->productSizes))
                                                                @if(count($newresp->productColorStyleDTOS[4]->productSizes))
                                                                    @foreach($newresp->productColorStyleDTOS[4]->productSizes as $sizes)
                                                                        <div>{{$sizes->name}}</div>
                                                                        <div>{{$sizes->numberInStock}}</div>
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 product-size selected-measurement pl-0 mt-2 mb-2">
                                                    <h6><strong><u>Selected Measurements</u></strong></h6>
                                                    @if(isset($reqMes))
                                                        @if(count($reqMes) > 0)
                                                            <ul>
                                                                @foreach($reqMes as $measure)
                                                                    <li>{{strtolower($measure)}}</li>
                                                                @endforeach
                                                            </ul>
                                                        @else
                                                            <label class="label label-rejected">No Selected
                                                                Measurement</label>
                                                        @endif
                                                    @endif
                                                </div>

                                                <hr>
                                                <div class="row" id="flags" style="display: flex; align-items: center">
                                                    <div class="col-md-8">
                                                        @if($newresp->status == 'A' && $newresp->verifiedFlag == 'N')
                                                            <input type="button" value="Verify Product" id="verified"
                                                                   class="btn-small btn btn-primary">
                                                            <a href="#" data-toggle="modal"
                                                               data-target="#unverifiedModal" id="unverified"
                                                               class="btn-small btn btn-danger">Reject Product</a>
                                                        @elseif($newresp->status == 'A' && $newresp->verifiedFlag == 'Y')
                                                            <a href="#" data-toggle="modal"
                                                               data-target="#unverifiedModal" id="unverified"
                                                               class="btn-small btn btn-danger">Unverify Product</a>
                                                        @elseif($newresp->status == 'R' && $newresp->verifiedFlag == 'N')
                                                            <input type="button" value="Verify Product" id="verified"
                                                                   class="btn-small btn btn-primary">
                                                        @endif
                                                        {{--<div class="pretty p-icon p-square p-smooth">
                                                            <input class="form-check-input" type="checkbox" name="verified" id="verified"
                                                                   value="verified" @if($newresp->verifiedFlag == 'Y') checked @endif >
                                                            <div class="state p-warning-o">
                                                                <i class="icon fa fa-check"></i>
                                                                <label class="form-check-label" for="verified">Verified</label>
                                                            </div>
                                                        </div>--}}
                                                    </div>
                                                    @if($newresp->verifiedFlag == 'Y')
                                                        <div class="col-md-4">
                                                            <div class="pretty p-icon p-square p-smooth">
                                                                <input class="form-check-input" type="checkbox"
                                                                       name="sponsored"
                                                                       id="sponsored"
                                                                       value="sponsored"
                                                                       @if($newresp->sponsoredFlag == 'Y') checked @endif >
                                                                <div class="state p-warning-o">
                                                                    <i class="icon fa fa-check"></i>
                                                                    <label class="form-check-label" for="sponsored">Sponsored</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-block">
                                        <h5>Other Information</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5 class="productDetailsLabel">Fabric</h5>
                                                <div class="row">
                                                    @if(isset($newresp->bespokeProductDTO->materialPicture) && is_array($newresp->bespokeProductDTO->materialPicture))
                                                        @if(count($newresp->bespokeProductDTO->materialPicture))
                                                            @foreach($newresp->bespokeProductDTO->materialPicture as $picture)
                                                                <div class="col-md-3 ml-pic">
                                                                    <div class="">
                                                                        <img class="materialImgAdmin"
                                                                             src="{{str_replace('http','https',$picture->materialPicture)}}">
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="col-md-3 offset-3 mt-4">
                                                                <label class="label label-pending">No Fabric
                                                                    Uploaded</label>
                                                            </div>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <h5 class="productDetailsLabel">Artworks</h5>
                                                <div class="row">
                                                    @if(isset($newresp->bespokeProductDTO->artPictureDTOS) && is_array($newresp->bespokeProductDTO->artPictureDTOS))
                                                        @if(count($newresp->bespokeProductDTO->artPictureDTOS))
                                                            @foreach($newresp->bespokeProductDTO->artPictureDTOS as $artWorkPicture)
                                                                <div class="col-md-3 ml-pic">
                                                                    <div class="">
                                                                        <img class="materialImgAdmin"
                                                                             src="{{str_replace('http','https',$artWorkPicture->artWorkPicture) }}">
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="col-md-3 offset-3 mt-4">
                                                                <label class="label label-pending">No Artwork
                                                                    Uploaded</label>
                                                            </div>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" data-backdrop="static" id="unverifiedModal" style="margin-top:51px;" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Enter Reason</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <textarea required placeholder="Enter the reason" class="form-control"
                                  id="notVerifiedValue"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary removeNotVerifiedDialog" data-dismiss="modal">
                            Close
                        </button>
                        <input type="button" class="btn btn-primary notVerified" value="Save Changes">
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('scripts')
    <script>

        $(document).ready(function () {
            $('.product-small-img >div >img').on('click', function () {
                let src = $(this).attr('src');
                let url = '' + src;
                $('.product-small-img >div >img').css('border', 'none').removeClass('brand-bd-color');

                $(this).css({
                    borderWidth: 1,
                    borderStyle: "solid"
                }).addClass('thumbBorder');
                $(this).parents('.productThumb').siblings('.productDisplay').find('a').attr('href', src);
                $(this).parents('.productThumb').siblings('.productDisplay').find('img').attr('src', src);
                $('.magnify-lens').css('background-image', 'url(' + src + ')');
            });
        })

        $(document).on('click', '.removeNotVerifiedDialog', function () {
            $('#roleModal').removeClass('show').addClass('fade');
            $('#verified').prop('checked', true);
        });

        $('.addProductAttribute').click(function () {
            if ($(this).attr('productcount') === '0') {
                var img = $(this).parents('.product-des').siblings('.col-md-6').find('.productThumbOne img').attr('src');
                $(this).parents('.product-des').siblings('.col-md-6').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-one').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbOneSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '1') {
                var img = $(this).parents('.product-des').siblings('.col-md-6').find('.productThumbTwo img').attr('src');
                $(this).parents('.product-des').siblings('.col-md-6').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-two').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbTwoSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '2') {
                var img = $(this).parents('.product-des').siblings('.col-md-6').find('.productThumbThree img').attr('src');
                $(this).parents('.product-des').siblings('.col-md-6').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-three').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbThreeSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '3') {
                var img = $(this).parents('.product-des').siblings('.col-md-6').find('.productThumbFour img').attr('src');
                $(this).parents('.product-des').siblings('.col-md-6').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-four').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbFourSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '4') {
                var img = $(this).parents('.product-des').siblings('.col-md-6').find('.productThumbFive img').attr('src');
                $(this).parents('.product-des').siblings('.col-md-6').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-five').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbFiveSize').removeClass('hide');
            }

        });

        $('.product-small-img >div >img').on('click', function () {
            let src = $(this).attr('src');
            let url = '' + src;
            $('.product-small-img >div >img').css('border', 'none').removeClass('brand-bd-color');

            $(this).css({
                borderWidth: 1,
                borderStyle: "solid"
            }).addClass('thumbBorder');
            $(this).parents('.productThumb').siblings('.productDisplay').find('a').attr('href', src);
            $(this).parents('.productThumb').siblings('.productDisplay').find('img').attr('src', src);

        });

        $(document).on('click', '.product-small-img-added2 div img, .product-small-img-added3 div img, .product-small-img-added4 div img, .product-small-img-added5 div img', function () {
            console.log($(this).attr('src'));
            let pictureSwap = $(this).attr('src');
            $('#mainPicture').attr('src', pictureSwap);
        });
                @if(!is_null(session('adminToken')))
        var tok = @php echo stripslashes(session('adminToken')) @endphp;
                @else
        var tok = '';
        @endif

        $(document).on('click', '#verified', function () {
            $('#verified').val('Processing request...').attr('disabled', 'disabled');
            var verifiedFlag = 'Y';
            var datas = {
                id: `{{$newresp->id}}`,
                verifiedFlag: verifiedFlag
            };
            $.ajax({
                url: `{{env('GET_BASE_URL')}}{{env('ADMIN_PRODUCT')}}${datas.id}/verifyproduct/${ datas.verifiedFlag}`,
                data: datas,
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                success: function (res) {
                    console.log(res);
                    if (res.status === '00') {
                        $('#verified').notify("Verified", "success");
                        swal('Product Verified', 'Product is now live', 'success');
                        $('.unverifyBlock').html('');
                        location.reload();
                    } else {
                        $('#verified').val('Verify Product').removeAttr('disabled', 'disabled');
                        swal(res.message, '', 'error');
                        // location.reload();
                    }
                },
                error: function (e) {
                    $('#conVerify').removeAttr("disabled", "disabled");
                    $('#verified').val('Verify Product').removeAttr('disabled', 'disabled');
                    console.log("this is an error " + e);
                    swal(e, '', 'error');
                }
            })
        });

        $(document).on('click', '.notVerified', function () {
            $('.notVerified').val('Processing request...').attr('disabled', 'disabled');
            var reasonBlock = $('#notVerifiedValue').val();
            <?php if(!is_null(session('adminToken'))): ?>
                tok = <?= session('adminToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>
            if ($('#notVerifiedValue').val() === '') {
                $('#notVerifiedValue').notify('Please, enter your reason', 'error');
                $('.notVerified').val('Save & Continue').removeAttr('disabled', 'disabled');
            } else {
                var data = {
                    id: '{{request()->route('pId')}}',
                    flag: 'N',
                    unverifyReason: $('#notVerifiedValue').val(),
                }
                $.ajax({
                    url: `{{env('GET_BASE_URL')}}{{env('ADMIN_PRODUCT')}}unverifyproduct`,
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (response) {
                        if (response.status === '00') {
                            swal('Successfully unverified', '', 'success');
                            $('#roleModal').removeClass('show').addClass('fade');
                            $(document).find('.unverifyBlock').html(reasonBlock);
                            location.reload();
                        } else if (response.status === '99') {
                            swal(response.message, '', 'error');
                            $('#roleModal').removeClass('show').addClass('fade');
                            location.reload();
                        }
                    },
                    error: function (error) {
                        $('.notVerified').val('Save & Continue').removeAttr('disabled', 'disabled');
                        swal(error, '', 'error');
                    }
                })
            }
        });

        $(document).on('click', '#sponsored', function () {
            if ($(this).is(':checked')) {
                sponsoredFlag = 'Y';
            } else {
                sponsoredFlag = 'N';
            }

            var datas = {
                id: "{{$newresp->id}}",
                sponsoredFlag: sponsoredFlag
            };

            $.ajax({
                url: "{{env('GET_BASE_URL')}}{{env('ADMIN_PRODUCT')}}" + datas.id + "/sponsor/" + datas.sponsoredFlag,
                data: datas,
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                success: function (res) {
                    if (res.status === '00') {
                        $('#sponsored').notify("saved", "success");
                        swal('Product Sponsored', '', 'success');
                        location.reload();
                    } else {
                        swal(res.message, '', 'error');
                        location.reload();
                    }
                },
                error: function (e) {
                    swal(e, '', 'error');
                }
            })
        });
    </script>

@endpush