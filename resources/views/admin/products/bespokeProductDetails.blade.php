@extends('layouts.admin.default')

@push('styles')
    <style>
        .hide {
            display: none;
        }
    </style>
@endpush

@section('pageTitle', $bespokeProductDetail->productName. ' | Product Detail')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-home bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Products Management</h5>
                                    <span>Product Details for: {{$bespokeProductDetail->productName}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.products')}}">Products</a></li>
                                    <li class="breadcrumb-item"><a href="#">Product Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="card">
                                    <div class="card-block">
                                        <div class="row productDetails">
                                            <div class="col-md-6">
                                                <div class="row product-img">
                                                    <div class="col-md-9 product-big-img productDisplay product-one productDisplayOne">
                                                       <a title="Click to zoom" data-fancybox
                                                           href="{{@$bespokeProductDetail->bespokePicturesDTO->viewOne}}">
                                                            <img id='mainPicture'
                                                                 src="{{@$bespokeProductDetail->bespokePicturesDTO->viewOne}}"
                                                                 class="img-fluid main-p fitDiv">
                                                        </a>

                                                    </div>
                                                  {{--  <div class="col-md-3 product-small-img productThumb product-one productThumbOne">
                                                        @if(isset($newresp->productColorStyleDTOS[0]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[0]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[0]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>--}}

{{--
                                                    <div class="col-md-9 hide product-big-img productDisplay product-two productDisplayTwo">
                                                        <a title="Click to zoom" data-fancybox
                                                           href="{{str_replace('http','https', @$newresp->productColorStyleDTOS[1]->productPictureDTOS[0]->picture)}}">
                                                            <img id='mainPicture'
                                                                 src="{{str_replace('http','https', @$newresp->productColorStyleDTOS[1]->productPictureDTOS[0]->picture)}}"
                                                                 class="img-fluid main-p fitDiv">
                                                        </a>
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-two productThumbTwo">
                                                        @if(isset($newresp->productColorStyleDTOS[1]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[1]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[1]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                    <div class="col-md-9 hide product-big-img productDisplay product-three productDisplayThree">
                                                        <img id='mainPicture'
                                                             src="{{str_replace('http','https',@$newresp->productColorStyleDTOS[2]->productPictureDTOS[2]->picture)}}"
                                                             class="img-fluid main-p fitDiv">
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-three productThumbThree">
                                                        @if(isset($newresp->productColorStyleDTOS[2]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[2]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[2]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                    <div class="col-md-9 hide product-big-img productDisplay product-four productDisplayFour">
                                                        <img id='mainPicture'
                                                             src="{{@$newresp->productColorStyleDTOS[3]->productPictureDTOS[3]->picture}}"
                                                             class="img-fluid main-p fitDiv">
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-four productThumbFour">
                                                        @if(isset($newresp->productColorStyleDTOS[3]->productPictureDTOS))
                                                            )
                                                            @if(count($newresp->productColorStyleDTOS[3]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[3]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>

                                                    <div class="col-md-9 hide product-big-img productDisplay product-five productDisplayFive">
                                                        <img id='mainPicture'
                                                             src="{{@$newresp->productColorStyleDTOS[4]->productPictureDTOS[4]->picture}}"
                                                             class="img-fluid main-p fitDiv">
                                                    </div>
                                                    <div class="col-md-3 hide product-small-img productThumb product-five productThumbFive">
                                                        @if(isset($newresp->productColorStyleDTOS[4]->productPictureDTOS))
                                                            @if(count($newresp->productColorStyleDTOS[4]->productPictureDTOS))
                                                                @foreach($newresp->productColorStyleDTOS[4]->productPictureDTOS as $smallProductPicture)
                                                                    <div>
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                                             class="img-fluid">
                                                                    </div>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                    </div>--}}

                                                </div>
                                            </div>
                                            <div class="col-md-6 product-des">
                                                <p class="product-name">{{strtolower($bespokeProductDetail->productName)}}</p>
                                                {{--<p class="product-store">Seller: <a
                                                            href="{{route('admin.designer.view', $newresp->designerId)}}">{{$newresp->designerName}}</a>
                                                </p>--}}
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>Product Type</h6>

                                                            <label class="label label-processing">Bespoke</label>
                                                    <div class="col-md-6">
                                                        <h6>Product Status</h6>
                                                        @if($bespokeProductDetail->verifiedFlag === 'N')
                                                            <label class="label label-rejected">Not Verified</label>
                                                        @else
                                                            <label class="label label-completed">Verified</label>
                                                        @endif
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="row pt-2">

                                                        <div class="col-md-6">
                                                            <h5><strong><u>Price:</u></strong> </h5>
                                                            <p>₦<span>{{number_format($bespokeProductDetail->estimatedSewingPrice)}}</span></p>
                                                        </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="">
                                                            <h6><strong><u>Category</u></strong></h6>
                                                            <p>{{$bespokeProductDetail->categoryId}}</p>
                                                        </div>
                                                    </div>

                                                </div>
                                                <h6><strong><u>Summary</u></strong></h6>


                                                <h6><strong><u>Description</u></strong></h6>
                                                <span class="product-des-long">{!! $bespokeProductDetail->description !!}</span>





                                                <hr>

                                                <div class="row" id="flags" style="display: flex; align-items: center">
                                                    <div class="col-md-8">
                                                        @if($bespokeProductDetail->verifiedFlag == 'N')
                                                           {{-- <input type="button" value="Verify Product" id="verified"
                                                                   class="btn-small btn btn-primary">--}}
                                                            <button id="unverified" data-verify="1"
                                                                    class="btn-small btn btn-danger">Verify Product</button>
                                                        @elseif($bespokeProductDetail->verifiedFlag == 'Y')
                                                            <button id="unverified" data-verify="0"
                                                               class="btn-small btn btn-danger">Unverify Product</button>
                                                        @elseif($bespokeProductDetail->verifiedFlag == 'N')
                                                            <input type="button" value="Verify Product" id="verified"
                                                                   class="btn-small btn btn-primary">
                                                        @endif

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-block">
                                        <h5>Other Information</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h5 class="productDetailsLabel">Fabric</h5>
                                                <div class="row">
                                                    @if(isset($bespokeProductDetail->bespokeFabricPicturesDTO) && is_array($bespokeProductDetail->bespokeFabricPicturesDTO))
                                                        @if(count($bespokeProductDetail->bespokeFabricPicturesDTO))
                                                            @foreach($bespokeProductDetail->bespokeFabricPicturesDTO as $picture)
                                                                <div class="col-md-3 ml-pic">
                                                                    <div class="">
                                                                        <img class="materialImgAdmin"
                                                                             src="{{$picture->viewOne}}">
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="col-md-3 offset-3 mt-4">
                                                                <label class="label label-pending">No Fabric
                                                                    Uploaded</label>
                                                            </div>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <h5 class="productDetailsLabel">Artworks</h5>
                                                <div class="row">
                                                    @if(isset($bespokeProductDetail->bespokeArtworkPicturesDTO) && is_array($bespokeProductDetail->bespokeArtworkPicturesDTO))
                                                        @if(count($bespokeProductDetail->bespokeArtworkPicturesDTO))
                                                            @foreach($bespokeProductDetail->bespokeArtworkPicturesDTO as $artWorkPicture)
                                                                <div class="col-md-3 ml-pic">
                                                                    <div class="">
                                                                        <img class="materialImgAdmin"
                                                                             src="{{$artWorkPicture->viewOne}}">
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @else
                                                            <div class="col-md-3 offset-3 mt-4">
                                                                <label class="label label-pending">No Artwork
                                                                    Uploaded</label>
                                                            </div>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" data-backdrop="static" id="unverifiedModal" style="margin-top:51px;" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Enter Reason</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <textarea required placeholder="Enter the reason" class="form-control"
                                  id="notVerifiedValue"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary removeNotVerifiedDialog" data-dismiss="modal">
                            Close
                        </button>
                        <input type="button" class="btn btn-primary notVerified" value="Save Changes">
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@push('scripts')
    <script>

        $(document).ready(function () {
            $('.product-small-img >div >img').on('click', function () {
                let src = $(this).attr('src');
                let url = '' + src;
                $('.product-small-img >div >img').css('border', 'none').removeClass('brand-bd-color');

                $(this).css({
                    borderWidth: 1,
                    borderStyle: "solid"
                }).addClass('thumbBorder');
                $(this).parents('.productThumb').siblings('.productDisplay').find('a').attr('href', src);
                $(this).parents('.productThumb').siblings('.productDisplay').find('img').attr('src', src);
                $('.magnify-lens').css('background-image', 'url(' + src + ')');
            });



            $("#unverified").on('click', function(){
                $(this).attr('data-verify');

                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                <?php endif; ?>

               var data = {
                    bespokeStyleModelId: "{{$bespokeProductDetail->id}}",
                }

                if($(this).attr('data-verify') === '1'){
                            data.adminApprove = true;
                }else if($(this).attr('data-verify') === '0'){
                          data.adminApprove = false;
                }


               $.ajax({
                   url: `{{env('GET_BASE_URL')}}/fashion/secure/admin/product/admin_approve_bespoke_product`,
                   type: "POST",
                   dataType: 'json',
                   headers: {
                       'Authorization': tok,
                       'Content-Type': 'application/json; charset=utf-8',
                       'accept': 'application/json'
                   },
                    data: JSON.stringify(data),
                    success: function(response) {
                       console.log(response);
                       if(response.status === 200){
                           swal('Successfully unverified', '', 'success');
                           location.reload();
                       }else{
                           swal(response.message, '', 'error');
                       }
                    },
                   error: function(error){
                       console.log(error);
                   }
               })
            });
        })


    </script>

@endpush