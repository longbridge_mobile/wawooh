@extends('layouts.admin.default')

@section('pageTitle','Add Inspire Me Product')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Create Inspire Product</h1>
        </section>
        <section class="contact container-fluid">
            <div class="bg-white">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select name="" id="category" class="form-control">
                                <option value="" hidden>--select category--</option>
                                <option value="Weddings">Weddings</option>
                                <option value="Birthday">Birthday</option>
                                <option value="Dinners">Dinners</option>
                                <option value="Pure African">Pure African</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="eventName">Product Name</label>
                            <input type="text" class="form-control" id="eventName" placeholder="Enter name of event">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="productPrice">Product Price*</label>
                            <input type="number" class="form-control impt-control" id="productPrice"
                                   placeholder="Input Product Cost">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group discountGroup" style="margin-bottom: 0 !important;">
                            <label for="productPrice">Discounted Product Price</label>
                            <input type="number" class="form-control discountField" id="DiscountproductPrice"
                                   placeholder="Input Discounted Price">
                        </div>

                        <div class="form-group hide percentGroup" style="margin-bottom: 0 !important;">
                            <label for="productPrice">Percentage</label>
                            <input type="number" class="form-control discountField" id="percent"
                                   placeholder="Input Percentage">
                        </div>
                        <div id="selectPercentage"><label class="label label-warning">Select <i
                                        class="fa fa-fw fa-percent"
                                        aria-hidden="true"></i></label>
                        </div>
                        <div id="selectDiscount" class="hide"><label class="label label-rejected">Select
                                Discount</label></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="productDetails">Product Summary</label>
                            <textarea class="form-control impt-control" id="productSummary"
                                      placeholder="Not more than 100 characters" rows="3"
                                      maxlength="100"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="productDescription">Product Full Description</label>
                            <textarea class="form-control impt-control" id="productDescription"
                                      placeholder="Write full details of your product describing the basic information (fabric, color, how to wash etc.). Each description should be in a paragraph."
                                      rows="6"></textarea>
                        </div>
                    </div>
                </div>
                <div id='loader2' style='height:50px;display:none' class='col-md-12 text-center'>
                    <img style='width:auto;height:100%' class='center-block' src='{{asset('img/loader.gif')}}'/>
                </div>

                <div class="uploadButton">
                    <button class="btn btn-wawooh continue">Upload Product</button>
                    <button class="btn btn-update"> Cancel</button>
                </div>
            </div>
        </section>
    </div>
@stop