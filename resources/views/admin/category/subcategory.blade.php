@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Sub-Category')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-edit bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Sub Categories</h5>
                                    <span>Manage subcategories</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.category')}}">Categories</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Sub Categories</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row promoCode">
                                    <div class="col-sm-4" id="addSubCard">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Add New SubCategory</h5>
                                            </div>
                                            <div class="card-block">
                                                @if(isset($error))
                                                    <div class="alert alert-warning">
                                                        <span>{{$error}}</span>
                                                    </div>
                                                @endif
                                                <form action='{{route('addSubCategory')}}' method='post'>
                                                    <div class="form-group">
                                                        <label for="productGroup">Product Group</label>
                                                        <select name="producttype" id="productGroup"
                                                                class="form-control" aria-required="true" required>
                                                            <option value="" hidden>--select--</option>
                                                            <option value="1">Clothings</option>
                                                            <option value="2">Accessories</option>
                                                            <option value="3">Shoes</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="categoryListName">Category List*</label>
                                                        <select name='id' id="categoryListName"
                                                                class="form-control" required aria-required="true">
                                                            <option value="" hidden>--select--</option>
                                                            @if(isset($categories))
                                                                @if(count($categories))
                                                                    @foreach($categories as $category)
                                                                        <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endif

                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label for="subCategoryName">Sub Category Name*</label>
                                                            <input type="text" name='subcategories'
                                                                   class="form-control"
                                                                   id="subCategoryName"
                                                                   placeholder="Enter Sub Category Name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type='submit' class="btn btn-warning">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    {{--Edit SubCategories Card--}}
                                    <div class="col-sm-4" id="editSub" style="display: none;">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Edit This SubCategory</h5>
                                            </div>
                                            <div class="card-block">
                                                @if(isset($error))
                                                    <div class="alert alert-warning">
                                                        <span>{{$error}}</span>
                                                    </div>
                                                @endif
                                                <form>
                                                    <div class="form-group">
                                                        <label for="productGroup">Product Group</label>
                                                        <select name="producttype" id="productGroup"
                                                                class="form-control" aria-required="true" required>
                                                            <option value="" hidden>--select--</option>
                                                            <option value="1">Clothings</option>
                                                            <option value="2">Accessories</option>
                                                            <option value="3">Shoes</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="categoryListName">Category List*</label>
                                                        <select name='id' id="categoryListNameSub"
                                                                class="form-control" required aria-required="true">
                                                            <option value="" hidden>--select--</option>
                                                            @if(isset($categories))
                                                                @if(count($categories))
                                                                    @foreach($categories as $category)
                                                                        <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endif

                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label for="subCategoryName">Sub Category Name*</label>
                                                            <input type="text" name='subcategories'
                                                                   class="form-control"
                                                                   id="subCategoryNameSub"
                                                                   placeholder="Enter Sub Category Name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type='button' id="actionEdit"
                                                               class="btn btn-wawooh editCatSub"
                                                               value="Update">
                                                        <img src="{{asset('img/AjaxLoader.gif')}}"
                                                             alt="" id="preLoad" class="proLoad">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Subcategory List</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="table-responsive">
                                                    <table class="table table-striped category-table" id="subCatTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Product Category</th>
                                                            <th scope="col">Sub-categories Name</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($subcategories))
                                                            @if(count($subcategories))
                                                                @foreach($subcategories as $subcategory)
                                                                    <tr class='clickable-row'
                                                                        data-href='order-details.html'>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>@if($subcategory->productType == '1')
                                                                                Clothings
                                                                            @elseif($subcategory->productType == '2')
                                                                                Accessory
                                                                            @else
                                                                                Shoe
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <a href='{{route('admin.style', $subcategory->subCategory)}}/?subcat={{$subcategory->id}}'>{{$subcategory->subCategory}}</a>
                                                                        </td>
                                                                        <td>
                                                                            <label class="label label-{{$subcategory->delFlag === 'N' ? 'completed':'rejected'}}">
                                                                                {{$subcategory->delFlag === 'N' ? 'Active' : 'Deleted'}}
                                                                            </label>
                                                                        </td>
                                                                        <td>
                                                                            <a href="" data-toggle="modal"
                                                                               linkAttr="{{$subcategory->subCategory}}"
                                                                               id="{{$subcategory->id}}"
                                                                               data-target="#subCategoryModal"
                                                                               class="editSubCategory"
                                                                               prodtype="{{$subcategory->productType}}"><i
                                                                                        class="fa fa-pencil"></i></a>&nbsp;
                                                                            <a href="#" delId="{{$subcategory->id}}"
                                                                               id="deleteSubCat"><i
                                                                                        class="fa fa-trash"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan='5' class='text-center'>No Sub category
                                                                        yet.
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Subcategory--}}
    {{--<div id="subCategoryModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content spaceTop">
                <div class="modal-body">
                    <div class="">
                        <div class="form-group">
                            <div class="in-stock">
                                <div class="pretty p-icon p-round p-smooth">
                                    <input value='1' type="radio" name='producttype'
                                           class="form-check-input noLeft"
                                           id="outfit">
                                    <div class="state p-warning-o">
                                        <i class="icon fa fa-check"></i>
                                        <label class="form-check-label" for="outfit">Clothings</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="categoryListNameSub">Category List*</label>
                            <select name='id' id="categoryListNameSub" class="form-control">
                                @if(isset($categories))
                                    @if(count($categories))
                                        @foreach($categories as $category)
                                            --}}{{--@if($category->id == request('cat'))--}}{{--
                                            <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                            --}}{{-- @endif--}}{{--
                            @endforeach
                            @endif
                            @endif

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

@endsection

@push('scripts')
    <script>
        var prodType = '1';
        $('[name=producttype]').change(function () {
            prodType = $(this).val();
        });

        $(document).on('click', '.editSubCategory', function (e) {
            e.preventDefault();
            $('#editSub').show(500);
            $('#addSubCard').hide(300);
            var subCat = $(this).attr('linkAttr');
            var id = $(this).attr('id');
            $('#subCategoryNameSub').val(subCat);
            $('.editCatSub').attr('id', id);

            $(document).on('click', '.editSubCategory', function () {
                var subCat = $(this).attr('linkAttr');
                var id = $(this).attr('id');
                $('#subCategoryNameSub').val(subCat);
                $('.editCatSub').attr('id', id);

                if ($(this).attr('prodtype') === '3') {
                    $('.in-stock #shoes').prop('checked', true);
                    $('.in-stock #outfit').prop('checked', false);
                    $('.in-stock #accessories').prop('checked', false);
                    prodType = '3';
                } else if ($(this).attr('prodtype') === '2') {
                    $('.in-stock #accessories').prop('checked', true);
                    $('.in-stock #shoes').prop('checked', false);
                    $('.in-stock #outfit').prop('checked', false);
                    prodType = '2';
                } else {
                    $('.in-stock #outfit').prop('checked', true);
                    $('.in-stock #accessories').prop('checked', false);
                    $('.in-stock #shoes').prop('checked', false);
                    prodType = '1';
                }
            });

            $(document).on('click', '.editCatSub', function () {
                $('#actionEdit').val('Updating...').attr('disabled', 'disabled');
                var data = {
                    id: $(this).attr('id'),
                    name: $('#subCategoryNameSub').val(),
                    productType: prodType,
                };
                console.log(data);
                $.ajax({
                    url: '{{route('edit.subcategories')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.status === '00') {
                            swal(result.message, '', 'success');
                            $('#actionEdit').val('Updated').attr('disabled', 'disabled');
                            location.reload();
                        }

                    },
                    error: function (error) {
                        swal(error, '', 'error');
                        // console.log(error);
                        $('#actionEdit').val('Update').removeAttr('disabled', 'disabled');
                    }

                })
            });
        });
        $(document).ready(function () {
            $('#deleteSubCat').on('click', function (e) {
                e.preventDefault();
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                <?php endif; ?>
                e.preventDefault();
                let catId = $(this).closest('a').attr('delId');
                let data = {
                    "id": catId
                };
                console.log(catId);
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                url: '{{env('GET_BASE_URL')}}{{env('CATEGORY_API')}}' + catId + '/deletesubcategory',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                success: function (response) {
                                    if (response.status === "00") {
                                        swal('Deleted Successfully', '', 'success');
                                        location.reload();
                                    }
                                    else {
                                        swal('Not Successful', '', 'error');
                                    }
                                },
                                error: function (e) {
                                    swal(e, '', 'error');
                                }
                            })
                        }
                    })
            });
        });
    </script>
@endpush