@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Categories')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-mail bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Categories</h5>
                                    <span>Manage the categories</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Categories</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Add New Category</h5>
                                            </div>
                                            <div class="card-block">
                                                @if(isset($error))
                                                    <div class="alert alert-warning">
                                                        <span>{{$error}}</span>
                                                    </div>
                                                @endif
                                                <form class="promoCode" action='{{route('addCategory')}}' method='POST'>
                                                    <div class="form-group">
                                                        <label for="CategoryName">Category Name*</label>
                                                        <input type="text" name='categoryName'
                                                               class="form-control" id="CategoryName"
                                                               placeholder="Enter name of Category">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="description">Description</label>
                                                        <input type="text" class="form-control" id="description"
                                                               placeholder="Enter a short description">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type='submit' id="actionBtn " value="Add Category" class="btn btn-warning addCategory">
                                                        <img src="{{asset('img/AjaxLoader.gif')}}" alt="" id="preLoad" class="proLoad">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="categoryTable"
                                                           class="table table-striped category-table">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Category Name</th>
                                                            <th scope="col">Sub-categories</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($categories))
                                                            @if(count($categories))
                                                                @foreach($categories as $category)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>
                                                                            <a href='{{route('admin.subcategory')}}/?cat={{$category->id}}'>{{$category->categoryName}}</a>
                                                                        </td>
                                                                        <td>{{count($category->subCategories)}}</td>
                                                                        <td>{{--<a href="#" id="delCat"><i
                                                                                        class="fa fa-trash"></i></a>--}}
                                                                            <label class="label label-danger">Not Available</label>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.addCategory').on('click', function () {
                if ($())
                    var data = {
                        "categoryName": $('#cat').val(),
                    }
            });

            $('#delCat').on('click', function (e) {
                e.preventDefault();
                let catId = "{{$category->id}}";
                let data = {
                    "id": catId
                };
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{env('GET_BASE_URL')}}{{env('CATEGORY_API')}}' + catId + '/deletesubcategory'
                })
            })
        });
    </script>
@endpush