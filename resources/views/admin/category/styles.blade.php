@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle',request('subcategory') .' | Styles')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-mail bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Subcategory Styles</h5>
                                    <span>Manage the styles in each subcategory</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.category')}}">Category</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Styles</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row promoCode">
                                    <div class="col-sm-4" id="addSubCard">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Add New Style</h5>
                                            </div>
                                            <div class="card-block">
                                                @if(isset($error))
                                                    <div class="alert alert-warning">
                                                        <span>{{$error}}</span>
                                                    </div>
                                                @endif
                                                <form action='{{route('addStyle')}}' method='post'>
                                                    <div class="promoCode">
                                                        <div class="form-group">
                                                            <label for="categoryList">Sub Category*</label>
                                                            <select name='subcat' class="form-control"
                                                                    id="categoryListName">
                                                                <option value='{{@request('subcat')}}'>{{@request('subcategory')}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="subCategoryName">Styles*</label>
                                                            <input name='styles' type="text" class="form-control"
                                                                   id="subCategoryName"
                                                                   placeholder="Enter Styles">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="description">Description</label>
                                                            <input type="text" class="form-control" id="description"
                                                                   placeholder="Enter a short description 50 - 80 characters">
                                                        </div>
                                                        <div class="form-group">
                                                            <button type='submit' class="btn btn-warning">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    {{--Edit SubCategories Card--}}
                                    <div class="col-sm-4" id="editSub" style="display: none;">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Edit This SubCategory</h5>
                                            </div>
                                            <div class="card-block">
                                                @if(isset($error))
                                                    <div class="alert alert-warning">
                                                        <span>{{$error}}</span>
                                                    </div>
                                                @endif
                                                <form>
                                                    <div class="form-group">
                                                        <label for="productGroup">Product Group</label>
                                                        <select name="producttype" id="productGroup"
                                                                class="form-control" aria-required="true" required>
                                                            <option value="" hidden>--select--</option>
                                                            <option value="1">Clothings</option>
                                                            <option value="2">Accessories</option>
                                                            <option value="3">Shoes</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="categoryListName">Category List*</label>
                                                        <select name='id' id="categoryListNameSub"
                                                                class="form-control" required aria-required="true">
                                                            <option value="" hidden>--select--</option>
                                                            @if(isset($categories))
                                                                @if(count($categories))
                                                                    @foreach($categories as $category)
                                                                        <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                                                                    @endforeach
                                                                @endif
                                                            @endif

                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label for="subCategoryName">Sub Category Name*</label>
                                                            <input type="text" name='subcategories'
                                                                   class="form-control"
                                                                   id="subCategoryNameSub"
                                                                   placeholder="Enter Sub Category Name" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type='button' id="actionEdit"
                                                               class="btn btn-wawooh editCatSub"
                                                               value="Update">
                                                        <img src="{{asset('img/AjaxLoader.gif')}}"
                                                             alt="" id="preLoad" class="proLoad">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Subcategory Style List</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="table-responsive">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped category-table">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">S/N</th>
                                                                <th scope="col">Sub-categories Name</th>
                                                                <th scope="col">No of Product</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @if(isset($styles))
                                                                @if(count($styles))
                                                                    @foreach ($styles as $style)
                                                                        <tr class='clickable-row'>
                                                                            <td>{{$loop->iteration}}</td>
                                                                            <td>{{$style->style}}</td>
                                                                            <td></td>
                                                                            <td><i class="fa fa-trash"></i></td>
                                                                        </tr>
                                                                    @endforeach
                                                                @endif
                                                            @endif

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush