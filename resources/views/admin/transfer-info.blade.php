@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Bank Transfer Information')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-refresh-cw bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Bank Transfer Information: {{ count($transferInfo) }}</h5>
                                    <span>The list of all bank transfer payment</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Bank Transfers Info</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table style="font-size: 13px;" class="table table-striped category-table" id="transferInfo">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Order Number</th>
                                                            <th scope="col">Account Name</th>
                                                            <th scope="col">Amount Paid</th>
                                                            <th scope="col">Bank</th>
                                                            <th scope="col">Date Paid</th>
                                                            <th scope="col">Time Paid</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($transferInfo))
                                                            @if(count($transferInfo))
                                                                @foreach($transferInfo as $transfer)
                                                                    <tr class='clickable-row' data-href='order-details.html'>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$transfer->orderNum}}</td>
                                                                        <td>{{$transfer->accountName}}</td>
                                                                        <td>₦{{number_format($transfer->amountPayed)}}</td>
                                                                        <td>{{$transfer->bank}}</td>
                                                                        <td>{{\Carbon\Carbon::parse($transfer->paymentDate)->format('d-M-Y')}}
                                                                            {{--{{date('d-m-Y',$transfer->paymentDate)}}--}}
                                                                        </td>
                                                                        <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($transfer->paymentDate))->format('h:m:i A')}}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>No transaction at the moment</p>
                                                        @endif


                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="{{route('admin.export.transfer')}}" class="pull-right btn btn-primary">Export Data</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

    </script>
@endpush