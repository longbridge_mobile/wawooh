@extends('layouts.admin.default')

@push('styles')
    <style>
        #orderList, #designerList {
            text-align: center;
        }

        table th {
            text-align: center;
        }
    </style>
@endpush

@section('pageTitle','Dashboard')
@section('content')
    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-home bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Dashboard</h5>
                            <span>The summary of the system</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card prod-p-card card-red">
                                    <div class="card-body">
                                        <div class="row align-items-center m-b-30">
                                            <div class="col">
                                                <h6 class="m-b-5 text-white">Total Products</h6>
                                                <h3 class="m-b-0 f-w-700 text-white">
                                                    @if(isset($resp2->totalProducts)) {{$resp2->totalProducts}} @endif
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card prod-p-card card-blue">
                                    <div class="card-body">
                                        <div class="row align-items-center m-b-30">
                                            <div class="col">
                                                <h6 class="m-b-5 text-white">Active Orders</h6>
                                                <h3 class="m-b-0 f-w-700 text-white">
                                                    @if(isset($resp2->newOrders)){{$resp2->newOrders}}@endif
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card prod-p-card card-green">
                                    <div class="card-body">
                                        <div class="row align-items-center m-b-30">
                                            <div class="col">
                                                <h6 class="m-b-5 text-white">Total Payment</h6>
                                                <h3 class="m-b-0 f-w-700 text-white">
                                                    @if(isset($resp2->totalPayment))
                                                        ₦{{number_format($resp2->totalPayment)}} @endif
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card prod-p-card card-yellow">
                                    <div class="card-body">
                                        <div class="row align-items-center m-b-30">
                                            <div class="col">
                                                <h6 class="m-b-5 text-white">Total Sale</h6>
                                                <h3 class="m-b-0 f-w-700 text-white">@if(isset($resp2->totalOrders)){{$resp2->totalOrders}} @endif

                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-xl-12 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Latest Orders</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="verified-table"
                                                   class="table table-striped table-bordered nowrap reduced-font">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>OrderID</th>
                                                    <th>Customer Name</th>
                                                    <th>Status</th>
                                                    <th>Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($resp2->recentOrders))
                                                    @foreach($resp2->recentOrders as $resp)
                                                        <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>{{$resp->orderNumber}}</td>
                                                            <td>{{$resp->customerName}}</td>
                                                            <td>
                                                                @if($resp->deliveryStatus == 'P')
                                                                    <label class="label label-pending">Pending</label>
                                                                @elseif($resp->deliveryStatus == 'PC')
                                                                    <label class="label label-completed">Payment
                                                                        Confirmed</label>
                                                                @elseif($resp->deliveryStatus == 'OP')
                                                                    <label class="label label-processing">Processing</label>
                                                                @elseif($resp->deliveryStatus == 'A')
                                                                    <label class="label label-processing">Accepted</label>
                                                                @elseif($resp->deliveryStatus == 'WR')
                                                                    <label class="label label-rejected">Wawooh
                                                                        Rejected</label>
                                                                @elseif($resp->deliveryStatus == 'CO')
                                                                    <label class="label label-completed">Completed</label>
                                                                @elseif($resp->deliveryStatus == 'RS')
                                                                    <label class="label label-processing">Ready for
                                                                        Shipping</label>
                                                                @elseif($resp->deliveryStatus == 'RI')
                                                                    <label class="label label-processing">Ready for
                                                                        Inspection</label>
                                                                @elseif($resp->deliveryStatus == 'OS')
                                                                    <label class="label label-processing">Order
                                                                        Shipped</label>
                                                                @elseif($resp->deliveryStatus == 'C')<label
                                                                        class="label label-rejected">Cancelled</label>
                                                                @endif
                                                            </td>
                                                            <td>₦{{number_format($resp->paidAmount)}}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-md-12">
                                <div class="card comp-card">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <h6 class="m-b-25">Total Users</h6>
                                                <h3 class="f-w-700 text-c-blue">{{isset($userCount) ? $userCount : '0'}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card comp-card">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <h6 class="m-b-25">Total Designers</h6>
                                                <h3 class="f-w-700 text-c-green">{{isset($designerCount) ? $designerCount : '0'}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card comp-card">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <h6 class="m-b-25">All Users</h6>
                                                <h3 class="f-w-700 text-c-yellow">{{isset($allUserCount) ? $allUserCount : '0'}}</h3>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-hand-paper bg-c-yellow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card comp-card">
                                    <div class="card-body">
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <h6 class="m-b-25">Subscribers</h6>
                                                <h3 class="f-w-700 text-c-yellow">{{isset($emailSub) ? $emailSub : '0'}}</h3>
                                            </div>
                                            <div class="col-auto">
                                                <i class="fas fa-hand-paper bg-c-yellow"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Latest Designers</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="verified-table"
                                                   class="table table-striped table-bordered nowrap reduced-font">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Store Name</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Date Reg</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($resp2->recentCustomers))
                                                    @foreach($resp2->recentCustomers as $resp)
                                                        <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>{{$resp->storeName}}</td>
                                                            <td>{{$resp->email}}</td>
                                                            <td>{{$resp->phoneNo}}</td>
                                                            <td>
                                                                {{\Carbon\Carbon::parse($resp->createdDate)->format('Y-m-d')}}
                                                                <br>
                                                                <small class="text-success">
                                                                    ({{\Carbon\Carbon::parse($resp->createdDate)->diffForHumans()}}
                                                                    )
                                                                </small>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-right mt-4">
                                            <a href="{{route('admin.designers')}}"
                                               class=" b-b-primary text-primary">View
                                                all
                                                designers</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


