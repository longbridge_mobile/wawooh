<section class="event-details eventDetails">
    <div class="inner-details">
        <h3>Event Title: {{str_replace('-', ' ', title_case(request('preview')))}}</h3>
        <p class="other">Location: {{urldecode(request('location'))}}</p>
        <div class="desc">
            <span>{{urldecode(request('description'))}}</span>
        </div>
        <p class="other">Date of Event: {{Carbon\Carbon::parse(urldecode(request('date')))->format('Y-m-d')}}</p>

        <p class="other">Sponsored? <label
                    class="label label-{{request('type') == 'S' ? 'success' : 'danger'}}">{{request('type') == 'S' ? 'Sponsored' : 'Not Sponsor'}}</label>
        </p>

        <hr>
        <div class="row">
            <div class="col-md-6">
                <button class='btn btn-wawooh editInfo'>Edit</button>
                <span style='display:none' id='x'>
            </div>
        </div>
    </div>

    <div class="edit-events">
        <div class='bg-white'>
            <h3>Update Event Information</h3>
            <hr/>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="eventName">Event Title</label>
                        <input id='eventName' type="text"
                               value='{{str_replace('-', ' ', title_case(request('preview')))}}'
                               class='form-control eventDetails'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="location">location</label>
                        <input id='location' type="text" value='{{urldecode(request('location'))}}'
                               class='form-control eventDetails'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="date">Date of Event</label>
                        <input id='date' value="" type="date" value='' class='form-control eventDetails'/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {{-- <label for="">Sponsor this event</label> --}}
                        <div class="form-group" style="margin-top: .81em">
                            <div class="pretty p-switch p-fill">
                                <input id='status' @if(request('type') == 'S') checked @endif type='checkbox' value='S'
                                       class='form-control eventDetails'/>
                                <div class="state">
                                    <label for="status">{{request('type') == 'S' ? 'Sponsored' : 'Not Sponsor'}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Event description</label>
                        <textarea id='description' rows="10" cols="6"
                                  class='form-control eventDetails'>{{urldecode(request('description'))}}</textarea>
                    </div>
                </div>

            </div>
            <div class='row'>
                <div class='col-md-6'>
                    <button class='btn btn-wawooh cancel'>Cancel</button>
                    <button class='btn btn-wawooh saveInfo'>Save</button>
                </div>
            </div>
        </div>
    </div>
</section>


<table class='table'>
    <tr>
        <td>Event Name:</td>
        <td>
            <span class='eventDetails'>  {{str_replace('-', ' ', title_case(request('preview'))) }}</span>
            <input id='eventName' type="text"
                   value='{{str_replace('-', ' ', title_case(request('preview')))}}'
                   class='form-control hide eventDetails'/>
        </td>
    </tr>
    <tr>
        <td>Location:</td>
        <td>

            <span class='eventDetails'> {{request('location')}}</span>

            <input id='location' type="text" value='{{request('location')}}'
                   class='form-control hide eventDetails'/>

        </td>

    </tr>
    <tr>
        <td>Date of Event:</td>
        <td>
            {{--<span class='eventDetails'>{{Carbon\Carbon::parse(request('date'))->format('Y-m-d')}}</span>--}}
            <span class='eventDetails'>{{request('date')}}</span>
            <input id='date' type="date" value='' class='form-control hide eventDetails'/>

        </td>

    </tr>
    <tr>
        <td>Description:</td>
        <td>
            <span class='eventDetails'>{{request('description')}}</span>
            <textarea id='description'
                      class='form-control hide eventDetails'>{{request('description')}}</textarea>

        </td>
    </tr>
    <tr>
        <td>Sponsored:</td>
        <td>
            <span class='eventDetails'>@if(request('type') == 'S') Yes @else No @endif</span>
            <input id='status' @if(request('type') == 'S') checked @endif type='checkbox'
                   value='S' class='hide eventDetails'/>
        </td>
    </tr>
</table>