@extends('layouts.admin.admin')

@push('styles')
    <link rel="stylesheet" href="{{asset('css/tag.css')}}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <style>

    </style>

@endpush


@section('pageTitle','Tag Information')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Tag Image</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-home"></i> Tags</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> Tag Image</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <div class="row">
                    <div class="col-md-7">
                        <div id='pictureHolder' class="tag-asset">
                            <img id='picture' src="{{$unTaggedPicture->picture}}" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="">
                            <button class="btn btn-wawooh addTag">Tag</button>
                            <button style='display:none' class="btn btn-wawooh cancelTag">Reset</button>
                            <button id='removeTag' style='display:none' class="btn btn-wawooh ">Remove tag</button>
                            <h5 style="font-weight: 400; line-height: 25px; font-size: 12px">Click on Tag Button and
                                then double click on any part of the image to Tag. <br>Click on the created tag to add
                                information to the tag.</h5>

                            <br>
                            <div style='display:none' id='tagInfo'>
                                <h6>Add Tag details below</h6>
                                <div id='content'></div>

                                <div id='tag-info' style='display:none'>
                                    <div class="tag-space">
                                        <select id='cat' class="form-control">
                                            <option>Select Category</option>
                                            @if(isset($categories))
                                                @if(count($categories))
                                                    @foreach($categories as $category)
                                                        <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                    </div>
                                    <div class="tag-space">
                                        <select id='subcat' class="form-control">

                                        </select>
                                    </div>
                                    <div class="tag-space">
                                        <button id='addDesigner' class="btn btn-block">Add Designer</button>
                                        <button style='display:none' id='cancelDesigner' class="btn btn-block">Cancel
                                        </button>
                                    </div>

                                    <div style='display:none' id='designerList' class=" tag-space form-group">

                                        <select id='designers'
                                                class='{{--selectpicker--}} form-control' {{--data-live-search="true"--}}>

                                            <option>Select Designer</option>

                                        </select>
                                        {{--  <input type="text" list="designers" class="form-control">
                                            <datalist id="designers">
                                            <option>Marcia</option>
                                            <option>Cole</option>
                                            <option>Michael</option>
                                            <option>Francis</option>
                                            </datalist>  --}}
                                    </div>
                                    <div style='display:non' class=" tag-space form-group">

                                        <select id='designerProducts' class='selectpicker form-control'
                                                data-live-search="true">
                                            <option hidden>Select Products</option>
                                        </select>
                                    </div>
                                    {{--  <div  class=''>
                                        <select style='display:none' id='designerList'  class='form-control' placeholder="select Designer">
                                            
                                        </select>
                                    </div>  --}}
                                    <div class="row">
                                        <div id='loader' style='height:50px;display:none' class='text-center'>
                                            <img style='width:auto;height:100%' class='center-block'
                                                 src='{{asset('img/loader.gif')}}'/>
                                        </div>

                                        <div class="col-md-1 col-md-offset-7">

                                        </div>
                                        <div class="col-md-2">
                                            <button id='saveTag' class="btn btn-wawooh">Save Tag</button>
                                        </div>

                                    </div>
                                    <div style="margin-top: 30px;">
                                        <button id='submitTag' class="btn btn-wawooh btn-block">Submit all Tags</button>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/validation.js')}}"></script>
    <script src="{{asset('js/tag.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="{{ asset('js/plugins/jquery.initialize.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            //initialize tag addition


            $('.addTag').on('click', function () {
                $(this).hide();

                $('#tagInfo').show();
                $('.cancelTag').show();
                addTag('#pictureHolder', '#picture', '#content')
            });
            $.initialize('.selectpicker', function () {
                    $(this).selectpicker();
                }
            );

            //cancel created tags
            $('.cancelTag').on('click', function () {
                $('.addTag').show();
                $('.cancelTag').hide();
                $('#tagInfo').hide();
                $('#map-area').remove();

            });

            //get subcat
            $('#cat').on('change', function () {
                var id = $(this).val();
                $.ajax({
                    url: '{{route('getSubCat')}}/?id=' + id,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (result) {
                        console.log(result);
                        var html = '<option>Select SubCategory</option>';
                        if (result.length) {
                            for (i = 0; i < result.length; i++) {
                                html += `<option value='${result[i].id}'>${result[i].subCategory}</option>`;
                            }
                        }
                        $('#subcat').html(html);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });

            $('#addDesigner').on('click', function () {
                $('#designerList').show();
                $('#cancelDesigner').show();
                $(this).hide();
                $.ajax({
                    url: '{{route('getAllDesigners')}}',
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',

                    success: function (result) {
                        console.log('this is a store');
                        console.log(result);
                        let html = '';
                        if (result.data.length) {
                            for (i = 0; i < result.data.length; i++) {
                                html += `<option value='${result.data[i].id}'>${result.data[i].storeName}</option>`;
                            }
                        }
                        $('#designers').append(html);
                        $('.selectpicker').selectpicker('refresh');

                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });
            $('#cancelDesigner').on('click', function () {
                $(this).hide();
                $('#designers').val('');
                $('#designers').html('');
                $('#designerProducts').val('');
                $('#designerProducts').html('');
                $('#addDesigner').show();
                $('#designerList').hide();
            });

            $('#designers').on('change', function () {
                var id = $(this).val();
                console.log(id);
                $('[data-id=designerProducts]').hide();
                $.ajax({
                    url: '{{route('admin.designer.getDesignerProducts')}}?id=' + id,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    success: function (result) {
                        console.log(result);
                        var html = '';
                        if (result.status == 0) {
                            if (result.data.length > 0) {
                                for (i = 0; i < result.data.length; i++) {
                                    html += `<option value='${result.data[i].id}'>${result.data[i].name}</option>`;
                                }
                            }
                            $('[data-id=designerProducts]').show();
                            $('#designerProducts').html(html);
                            $('#designerProducts').selectpicker('refresh');
                        } else {
                            $.notify(result.message, 'error');
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });

            });
        });

        $(document).on('click', '#submitTag', function () {
            var ls_var = 'pictureTag';
            $('#loader').show();
            if (localStorage.getItem(ls_var)) {

                var tags = localStorage.getItem(ls_var);
                var data = {
                    "eventPicturesId": {{request('images')}},
                    "tags": JSON.parse(tags)

                }
                console.log(data);

                $.ajax({
                    url: '{{route('addTag')}}',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    data: JSON.stringify(data),
                    success: function (result) {
                        $('#loader').hide();
                        if (result.status == 0) {
                            localStorage.removeItem('pictureTag');
                            window.location.href = '{{route('admin.tags')}}';
                        } else {
                            console.log(result);
                            $.notify('An error occurred while submitting', 'error')
                        }
                    },
                    error: function (e) {
                        $('#loader').hide();
                        console.log(e);

                    }
                });
            }

        });
    </script>

@endpush