@extends('layouts.admin.admin')

@push('styles')
    <style>
        .fa-chevron-left {
            left: -17px !important;
            color: black
        }

        .fa-chevron-right {
            right: -17px !important;
            color: black
        }
    </style>

@endpush

@section('pageTitle','View Events')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>View Events</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-home"></i> Events</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> View Events</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <div id='events'>
                    @if(isset($events))
                        @if(count($events))
                            @foreach($events as $event)
                                <div class="learn19">
                                    <div class="row">
                                        <div class="col-md-11">
                                            <h3>{{$event->eventName}}
                                                <small>@if($event->eventType == 'S') Sponsored @endif</small>
                                            </h3>
                                            <hr>
                                        </div>
                                        <div class="col-md-1">
                                        <span class='pull-right'>
                                            <a id='{{$event->id}}' class='btn btn-link delete'><i
                                                        class="fa fa-trash"></i></a>
                                        </span>
                                        </div>
                                    </div>
                                    <div style='clear:both'>
                                    </div>
                                    <div id="{{str_slug($event->eventName)}}{{$event->id}}" class="carousel slide"
                                         data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            @if($event->eventPictures != null)
                                                @if(count($event->eventPictures))
                                                    @foreach(array_chunk($event->eventPictures, 3) as $pictures)
                                                        <div class="item @if($loop->iteration == 1) active @endif">
                                                            <div class="row learn20">
                                                                @foreach($pictures as $picture)
                                                                    <div class="col-md-4 col-xs-6">
                                                                        <div><img class='img-fluid'
                                                                                  src='{{$picture->picture}}'></div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </div>

                                        <a class="left carousel-control"
                                           href="#{{str_slug($event->eventName)}}{{$event->id}}" data-slide="prev">
                                            <span class="fa fa-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control"
                                           href="#{{str_slug($event->eventName)}}{{$event->id}}" data-slide="next">
                                            <span class="fa fa-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <div class="containe learn21">
                                        <p>
                                          {{-- <a href="{{route('admin.events.preview', str_slug($event->eventName) )}}/?location={{$event->location}}&date={{$event->eventDate}}&description={{$event->description}}&event={{$event->id}}&type={{$event->eventType}}"
                                               class="view">View all</a></p>--}}
                                            <a href="{{route('admin.events.preview', str_slug($event->id) )}}">View all</a>
                                    </div>

                                </div>

                            @endforeach
                        @endif

                    @endif
                </div>
                <div class='row'>
                    <div id='loader' style='height:50px;display:none' class='col-md-12 text-center'>
                        <img style='width:auto;height:100%' class='center-block' src='{{asset('img/loader.gif')}}'/>
                    </div>
                    <div class='col-md-6'>
                        <select id='size'>
                            <option>5</option>
                            <option>10</option>
                            <option>15</option>
                            <option>20</option>
                        </select>
                    </div>
                    <div class='col-md-6 text-right'>
                        <button style='display:none' page='0' class='btn btn-wawooh previous'>Previous</button>
                        <button page='1' class='btn btn-wawooh next'>Next</button>
                    </div>

                </div>

            </div>
        </section>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('js/admin/events.js') }}"></script>

    <script>
        $(document).ready(function () {
            page = 0;
            size = $('#size').val();

            $('.next').on('click', function () {
                page = parseInt($(this).attr('page'));
                var data = {
                    "page": page,
                    "size": size,
                    "eventType": 'A'
                };
                $('#loader').show();
                prev = page - 1;
                $('.previous').attr('page', prev);
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{route('admin.events.api')}}',
                    success: function (result) {
                        console.log(result);
                        $('#loader').hide();
                        $('.previous').show();

                        if (result.status == 0) {
                            if (result.data.events.length > 0) {
                                appendAllEventsToAdminDocument(result.data.events, '#events');
                            } else {
                                $('#events').html('There are no more events');
                                $('.next').attr('disabled', 'disabled');
                            }
                        }
                    },
                    error: function (e) {
                        $('#loader').hide();
                        swal(e, '','error');
                    },
                });
                page++;
                $(this).attr('page', page);
            });

            $('.previous').on('click', function () {
                page = parseInt($(this).attr('page'));
                $('#loader').show();
                var data = {
                    "page": page,
                    "size": size,
                    "eventType": 'A'
                };
                page = page + 1;
                console.log(page);
                $('.next').removeAttr('disabled');
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{route('admin.events.api')}}',
                    success: function (result) {
                        console.log(result);
                        $('#loader').hide();
                        if (result.status == 0) {
                            if (result.data.events.length > 0) {
                                appendAllEventsToAdminDocument(result.data.events, '#events');
                            } else {

                            }
                        }
                    },
                    error: function (e) {
                        $('#loader').hide();

                        console.log(e)
                    },
                });
                $('.next').attr('page', page);

                page = page - 2;

                if (page < 0) $(this).hide();
                $(this).attr('page', page);
            });

            $(document).on('click', '.delete', function () {
                swal({
                    title: "Are you sure?",
                    text: `Are you sure to want to delete this event? Click yes to continue.`,
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            swal(`Event Deleted Successfully`, {
                                icon: "success",
                            });
                            let id = $(this).attr('id');
                            window.location.href = '{{route('admin.events.delete')}}/?id=' + id;
                        } else {
                        }
                    });
                /*var answer = confirm('Are you sure to want to delete this event? Click ok to continue');
                if (answer) {
                    var id = $(this).attr('id');
                    window.location.href = '{{route('admin.events.delete')}}/?id=' + id;
                }*/
            });
        });

    </script>
@endpush