@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Bespoke Applications')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">

            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-scissors bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Bespoke Applications</h5>
                                    <span>All Applications for bespoke request</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Bespoke Applications</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table style="font-size: 13px" class="table table-striped table-bordered category-table" id="refundTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Store Name</th>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Gender Category</th>
                                                            <th scope="col">Date Reg</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($bespokeApp))
                                                            @if(count($bespokeApp))
                                                                @foreach($bespokeApp as $application)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$application->storeName}}</td>
                                                                        <td>
                                                                            @if(is_array($application->productType))
                                                                                @foreach($application->productType as $type)
                                                                                    {{$type.','}}
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if(is_array($application->category))
                                                                                @foreach($application->category as $cat)
                                                                                    {{$cat.','}}
                                                                                @endforeach
                                                                            @endif
                                                                        </td>
                                                                        <td>{{\Carbon\Carbon::parse($application->registrationDate)->format('dS M, y')}}
                                                                            <br>
                                                                            <small class="text-success">
                                                                                ({{\Carbon\Carbon::parse($application->registrationDate)->diffForHumans()}}
                                                                                )
                                                                            </small>
                                                                        </td>
                                                                        <td>
                                                                            @if($application->bespokeEligibility == null)
                                                                                <label class="label label-processing">Under Review</label>
                                                                            @elseif($application->bespokeEligibility == 'Y')
                                                                                <label class="label label-completed">Approved</label>
                                                                            @else
                                                                                <label class="label label-rejected">Declined</label>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{route('admin.bespoke.detail', $application->id)}}">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>No Application at the moment</p>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush