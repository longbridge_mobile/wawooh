@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Details | Bespoke Request')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-scissors bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Bespoke Request Details</h5>
                                    <span>The full information from a bespoke request</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('adminBespokeRequest')}}">Bespoke Request</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Bespoke Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                @if(isset($preview))
                                                    <div class="card-header">
                                                        <h5>Bespoke Request Details for {{$preview->customerName}}</h5>
                                                        <div class="all-order mt-2">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <table class="table table-bordered">
                                                                        <tr>
                                                                            <td width="200px">Customer Information</td>
                                                                            <td>{{$preview->customerName}} <br>
                                                                                <label class="label label-info"
                                                                                       style="text-transform: lowercase !important;">{{$preview->email}}</label>
                                                                                <br>
                                                                                <a href="tel:{{$preview->phone}}">{{$preview->phone}}</a>
                                                                            </td>
                                                                            <td rowspan="8">
                                                                                <div class="main-product-img">
                                                                                    @if(isset($preview->frontImage) && $preview->frontImage !== null)
                                                                                        <a data-fancybox
                                                                                           href="{{str_replace('http','https', $preview->frontImage)}}"><img
                                                                                                    class="prodImg" width="200px" height="100%"
                                                                                                    src="{{str_replace('http','https', $preview->frontImage)}}"></a>
                                                                                    @endif
                                                                                    @if(isset($preview->backImage) && $preview->backImage !== null)
                                                                                        <a data-fancybox
                                                                                           href="{{str_replace('http','https',$preview->backImage)}}"><img
                                                                                                    class="prodImg" width="200px" height="100%"
                                                                                                    src="{{$preview->backImage}}"></a>
                                                                                    @endif
                                                                                    @if(isset($preview->sideImage) && $preview->sideImage !== null)
                                                                                        <a href="{{str_replace('http','https',$preview->sideImage)}}"
                                                                                           data-toggle="lightbox">
                                                                                            <img class="prodImg" width="200px" height="100%"
                                                                                                 src="{{str_replace('http','https',$preview->sideImage)}}"
                                                                                                 alt="">
                                                                                        </a>
                                                                                    @elseif($preview->bespokeStyleBank !== null)
                                                                                        <a href="{{str_replace('http','https',$preview->bespokeStyleBank->picture)}}"
                                                                                           data-toggle="lightbox">
                                                                                            <img class="prodImg" width="200px" height="100%"
                                                                                                 src="{{str_replace('http','https',$preview->bespokeStyleBank->picture)}}"
                                                                                                 alt="">
                                                                                        </a>
                                                                                    @endif
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Gender</td>
                                                                            <td>{{$preview->gender}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Fabric</td>
                                                                            <td>
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <strong>Has Fabric?</strong> <br>
                                                                                        {{$preview->hasFabric}}
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <strong>Type/Name of Fabric</strong> <br>
                                                                                        {{$preview->fabricType}}
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Estimated Budget</td>
                                                                            <td>₦{{number_format($preview->budget)}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Height</td>
                                                                            <td> {{$preview->heightFeet. '\'' . $preview->heightInches}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Expected Date of Delivery</td>
                                                                            <td>{{\Carbon\Carbon::createFromTimestamp($preview->timeOfDelivery / 1000)->format('dS M, Y')}}
                                                                                <br>
                                                                                <small class="text-info">
                                                                                    ({{\Carbon\Carbon::createFromTimestamp($preview->timeOfDelivery / 1000)->diffForHumans()}}
                                                                                    )
                                                                                </small>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Additional Note</td>
                                                                            <td>{!! nl2br($preview->notes) !!}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Measurement</td>
                                                                            @if(!is_null($preview->measurement))
                                                                                <td><a href="#" data-toggle="modal" data-target="#measurementModal">Measurement
                                                                                        Available</a></td>
                                                                            @else
                                                                                <td><a href="#" data-toggle="modal" data-target="#">No Measurement
                                                                                        Available</a>
                                                                                </td>
                                                                            @endif
                                                                        </tr>

                                                                        <tr>
                                                                            <td>Date Ordered</td>
                                                                            <td>{{\Carbon\Carbon::createFromTimestamp($preview->orderDate / 1000)->format('dS M, Y')}}
                                                                                <br>
                                                                                <small class="text-success">
                                                                                    ({{\Carbon\Carbon::createFromTimestamp($preview->orderDate / 1000)->diffForHumans()}}
                                                                                    )
                                                                                </small>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Status</td>
                                                                            <td>
                                                                                @if($preview->status === "PENDING")
                                                                                    <label class="label label-rejected">Request Pending</label>
                                                                                @elseif($preview->status === "OPEN")
                                                                                    <label class="label label-rejected">Request is Open for Bidding</label>
                                                                                @elseif($preview->status === "CLOSED")
                                                                                    <label class="label label-rejected">Sorry! Bid Closed</label>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Selected SubCategory</td>
                                                                            <td>{{$preview->bespokeStyleBank !== null ? $preview->bespokeStyleBank->subCategory->subCategory: ' '}}</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Send to More</td>
                                                                            <td colspan="2">
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <label for="mDesigners" class="sr-only">Category</label>
                                                                                        <select name="" class="form-control req" id="mDesigners">
                                                                                            <option value="" hidden>--Select Category--</option>
                                                                                            @foreach($categories as $category)
                                                                                                <option value="{{$category->id}}">
                                                                                                    {{$category->categoryName}}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row mt-2">
                                                                                    <div class="col-md-12 bespoke-bidding-subcat">
                                                                                        <label class="label-title">Select Sub Category</label>
                                                                                        <ul id="subcat"></ul>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        @if($preview->bidEndDate !== null)
                                                                            <tr>
                                                                                <td>Bid End Date</td>
                                                                                <td>{{\Carbon\Carbon::createFromTimestamp($preview->bidEndDate / 1000)->format('dS M, Y')}}
                                                                                    <br>
                                                                                    <small class="text-info">
                                                                                        ({{\Carbon\Carbon::createFromTimestamp($preview->bidEndDate / 1000)->diffForHumans()}}
                                                                                        )
                                                                                    </small>
                                                                                </td>
                                                                            </tr>
                                                                        @endif
                                                                        <tr>
                                                                            <td>Bid Expiry Date</td>
                                                                            <td>
                                                                                <label for="bidexpirydate">Set Bid Expiry Date</label>
                                                                                <input type="text" class="form-control req calendar" id="bidexpirydate"
                                                                                       placeholder="Set Expiry Date">
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                    <div class="row text-center">
                                                                        <input type="button" value="Assign to Designer(s)" id="assignBidToDesigners"
                                                                               class="btn btn-success">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <label>No data found</label>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--Modal For Measurement-->
                                @if(isset($requestMeasurement))
                                    <div class="modal fade" data-backdrop="static" id="measurementModal" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel">
                                        <div class="modal-dialog modal-measurement" role="document">
                                            <div class="modal-content productSampleUpload">
                                                <div class="modal-header">
                                                    <h5>Measurement Details {{--in <span style="font-size: 10px" id="default">centimeters</span>
                                        <span style='font-size:10px' id='unit'></span>--}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                                aria-hidden="true">&times;</span></button>
                                                    {{--<hr>--}}
                                                    {{--<label id='convert' class='btn btn-wawooh'>Convert Measurement</label>
                                                    <label id="convertBack" class="btn btn-wawooh hide">Convert Measurement</label>--}}
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row measurement-input">
                                                        <table class="table table-stripped table-bordered measurementTable">
                                                            <thead>
                                                            <th class="heading" colspan="2">Measurement
                                                                Name: @if(isset($requestMeasurement)) {{$requestMeasurement->name}} @endif</th>
                                                            </thead>
                                                            @if(isset($requestMeasurement))
                                                                <tbody>
                                                                @if($requestMeasurement->fitType !== '' || $requestMeasurement->fitType != null)
                                                                    <tr>
                                                                        <td>Fitness Type</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{ucfirst(@$requestMeasurement->fitType)}}
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->ankle != '' || $requestMeasurement->ankle != null || $requestMeasurement->ankle != '0')
                                                                    <tr>
                                                                        <td width="150px">Ankle</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{@$requestMeasurement->ankle}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->armHole != '' || $requestMeasurement->armHole != null)
                                                                    <tr>
                                                                        <td width="150px">Arm Hole</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{@$requestMeasurement->armHole}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->biceps != '' || $requestMeasurement->biceps != null)
                                                                    <tr>
                                                                        <td width="150px">Biceps</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{@$requestMeasurement->biceps}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->backShirtLength != '' || $requestMeasurement->backShirtLength != null)
                                                                    <tr>
                                                                        <td width="150px">BackShirtLength</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{@$requestMeasurement->backShirtLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->blouseLength != '' || $requestMeasurement->blouseLength != null)
                                                                    <tr>
                                                                        <td width="150px">Blouse Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->blouseLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->bubaLength != '' || $requestMeasurement->bubaLength != null)
                                                                    <tr>
                                                                        <td width="150px">Buba Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{@$requestMeasurement->bubaLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->bust != '' || $requestMeasurement->bust != null)
                                                                    <tr>
                                                                        <td width="150px">Bust</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->bust}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->chest != '' || $requestMeasurement->chest != null)
                                                                    <tr>
                                                                        <td width="150px">Chest</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->chest}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->crotch != '' || $requestMeasurement->crotch != null)
                                                                    <tr>
                                                                        <td width="150px">Crotch</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->crotch}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->elbowCircumference != '' || $requestMeasurement->elbowCircumference != null)
                                                                    <tr>
                                                                        <td width="150px">Elbow Circumference</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->elbowCircumference}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->fistCircumference != '' || $requestMeasurement->fistCircumference != null)
                                                                    <tr>
                                                                        <td width="150px">FistCircumference</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->fistCircumference}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->fullLength != '' || $requestMeasurement->fullLength != null)
                                                                    <tr>
                                                                        <td width="150px">Elbow</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->fullLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->halfLength != '' || $requestMeasurement->halfLength != null)
                                                                    <tr>
                                                                        <td width="150px">Half Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->halfLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->kneeCircumference != '' || $requestMeasurement->kneeCircumference != null)
                                                                    <tr>
                                                                        <td width="150px">Knee Circumference</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->kneeCircumference}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->kneeLength != '' || $requestMeasurement->kneeLength != null)
                                                                    <tr>
                                                                        <td width="150px">Knee Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->kneeLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->longSleeve != '' || $requestMeasurement->longSleeve != null)
                                                                    <tr>
                                                                        <td width="150px">Long Sleeve</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->longSleeve}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->lapCircumference != '' || $requestMeasurement->lapCircumference != null)
                                                                    <tr>
                                                                        <td width="150px">Lap Circumference</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->lapCircumference}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->neck != '' || $requestMeasurement->neck != null)
                                                                    <tr>
                                                                        <td width="150px">Neck</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->neck}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->senatorLength != '' || $requestMeasurement->senatorLength != null)
                                                                    <tr>
                                                                        <td width="150px">Senator Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->senatorLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->shirtLength != '' || $requestMeasurement->shirtLength != null)
                                                                    <tr>
                                                                        <td width="150px">Shirt Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->shirtLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->shortsLength != '' || $requestMeasurement->shortsLength != null)
                                                                    <tr>
                                                                        <td width="150px">Shorts Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->shortsLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->shortSleeve != '' || $requestMeasurement->shortSleeve != null)
                                                                    <tr>
                                                                        <td width="150px">Short Sleeve</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->shortSleeve}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->shoulderWidth != '' || $requestMeasurement->shoulderWidth != null)
                                                                    <tr>
                                                                        <td width="150px">Shoulder Width</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->shoulderWidth}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->thigh != '' || $requestMeasurement->thigh != null)
                                                                    <tr>
                                                                        <td width="150px">Thigh</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->thigh}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->threeQuarterSleeve != '' || $requestMeasurement->threeQuarterSleeve != null)
                                                                    <tr>
                                                                        <td width="150px">ThreeQuarterSleeve</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->threeQuarterSleeve}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->trouserLength != '' || $requestMeasurement->trouserLength != null)
                                                                    <tr>
                                                                        <td width="150px">Trouser Length</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->trouserLength}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->waist != '' || $requestMeasurement->waist != null)
                                                                    <tr>
                                                                        <td width="150px">Waist</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->waist}}</td>
                                                                    </tr>
                                                                @endif
                                                                @if($requestMeasurement->wrist != '' || $requestMeasurement->wrist != null)
                                                                    <tr>
                                                                        <td width="150px">Wrist</td>
                                                                        <td style="text-align: center;"
                                                                            class="measureValue">{{$requestMeasurement->wrist}}</td>
                                                                    </tr>
                                                                @endif
                                                                </tbody>
                                                            @else
                                                                <label class="label label-rejected">Measurement is empty</label>
                                                            @endif
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href="#" id="printMeasure" class="btn btn-primary"><i class="fa fa-print"></i> Print
                                                        Out</a>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#mDesigners').on('change', function () {
                let id = $(this).val();
                let data = {
                    id: id
                };
                console.log(data);
                $.ajax({
                    url: "{{route('getSubCatByCatId')}}",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        console.log(result);
                        if (result.status === '00') {
                            let subCat = result.data;
                            let html = '';
                            if (subCat.length > 0) {
                                for (let i = 0; i < subCat.length; i++) {
                                    html += `<li>
                                              <input id="${subCat[i].id}" type="checkbox" value="${subCat[i].id}" class="req form-check-input">
                                              <label for="${subCat[i].id}">${subCat[i].subCategory}</label>
                                          </li>`;
                                    // html += `<li value=${subCat[i].id}>${subCat[i].subCategory}</li>`
                                }
                                $('#subcat').html(html);
                            } else {
                                // $.notify('error', 'error');
                                $('#subcat').html('<li>No Sub category</li>');
                            }
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                })

            });

            $('#assignBidToDesigners').on('click', function () {
                $('#assignBidToDesigners').val('Dispatching in progress...').attr('disabled', 'disabled');
                if ($('#mDesigners').val() === '' || $('#bidexpirydate').val() === '') {
                    $('.req').addClass('error');
                    $('#assignBidToDesigners').val('Assign to Designer(s)').removeAttr('disabled', 'disabled');
                } else {
                    let subCat = [];
                    $('#subcat li').each(function () {
                        if ($(this).find('.form-check-input').prop('checked')) {
                            subCat.push(parseInt($(this).find('.form-check-input').val()))
                        }
                    });

                    var data = {
                        bespokeOrderId: "{{$preview->id}}" ? parseInt("{{$preview->id}}") : null,
                        subcategory: subCat,
                        bidEndDate: $('#bidexpirydate').val() ? $('#bidexpirydate').val() : '2019-01-23T10:18:37.933Z',
                    };
                    console.log(data);
{{--                    var url = "{{route('dispatchBespoke')}}";--}}
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        url: url,
                        success: function (result) {
                            // console.log(result);
                            if (result.status === '00') {
                                swal(result.message, '', 'success');
                                $('#assignBidToDesigners').val('Assigned to Successfully').attr('disabled', 'disabled');
                                window.location.href = "{{route('adminBespokeRequest')}}";
                            } else {
                                swal(result.message, '', 'warning');
                                $('#assignBidToDesigners').val('Assign to Designer(s)').removeAttr('disabled', 'disabled');
                            }
                        },
                        error: function (error) {
                            console.log(error);
                            swal(error, '', 'error');
                            $('#assignBidToDesigners').val('Assign to Designer(s)').removeAttr('disabled', 'disabled');
                        }
                    });
                }
            });

            $('#printMeasure').on('click', function (e) {
                e.preventDefault();
                printPage();
            });
        });

        function printPage() {
            let prtContent = document.getElementById("measurementModal");
            let WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        }
    </script>
@endpush