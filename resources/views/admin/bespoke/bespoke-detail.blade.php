@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Details | Bespoke Applications')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-scissors bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Bespoke Application Details</h5>
                                    <span>Application details for each designer</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.bespoke.apply')}}">Bespoke Application</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Bespoke Detail</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    @if(isset($bespokeApp))
                                                        <div class="card-header">
                                                            <h5 class="sub-orders">Bespoke Applications Details for {{$bespokeApp->storeName}}</h5>
                                                            <div class="all-order mt-4">
                                                                <div class="row">
                                                                    <div class="product-image">
                                                                        {{--<h3>Product Picture</h3>--}}
                                                                        <div class="main-product-img">
                                                                            <a href="" data-toggle="lightbox">
                                                                                <img width="200px" height="200px" class="prodImg" src="" alt="">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                        <table class="table table-bordered">
                                                                            <tr>
                                                                                <td width="200px">Store Details</td>
                                                                                <td>Name: {{$bespokeApp->storeName}} <br> {{$bespokeApp->phoneNumber}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Product Type</td>
                                                                                <td>
                                                                                    @if(is_array($bespokeApp->productType))
                                                                                        @foreach($bespokeApp->productType as $type)
                                                                                            {{$type.','}}
                                                                                        @endforeach
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Gender Category</td>
                                                                                <td>
                                                                                    @if(is_array($bespokeApp->category))
                                                                                        @foreach($bespokeApp->category as $cat)
                                                                                            {{$cat.','}}
                                                                                        @endforeach
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>WorkForce Size</td>
                                                                                <td>{{$bespokeApp->workForceSize}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Threshold</td>
                                                                                <td>{{$bespokeApp->thresholdValue}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Location</td>
                                                                                <td>{{$bespokeApp->workshopAddress}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Verification Method</td>
                                                                                <td>
                                                                                    @if($bespokeApp->locationVerificationType == 'Video Call')
                                                                                        <label class="label label-inspection">Video Call</label>
                                                                                        <br>
                                                                                        <i class="fa fa-whatsapp"></i> <a href="https://wa.me/{{$bespokeApp->locationVerificationMethodId}}">{{$bespokeApp->locationVerificationMethodId}}</a>
                                                                                        <br>
                                                                                        <i class="fa fa-skype"></i> &dash;
                                                                                    @elseif($bespokeApp->locationVerificationType == 'Physical Visit')
                                                                                        <label class="label label-info">Physical Visit</label> <br>
                                                                                        <strong><i class="fa fa-calendar-o"></i> {{$bespokeApp->dayForVisitation}}
                                                                                        </strong> <br>
                                                                                        <i class="fa fa-clock-o"></i> {{$bespokeApp->time}}
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Visitation Details</td>
                                                                                <td>Day: {{$bespokeApp->dayForVisitation . ' | ' . $bespokeApp->time}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Date Registered</td>
                                                                                <td>{{\Carbon\Carbon::parse($bespokeApp->registrationDate)->format('dS M, Y')}}
                                                                                    <br>
                                                                                    <small class="text-success">
                                                                                        ({{\Carbon\Carbon::parse($bespokeApp->registrationDate)->diffForHumans()}}
                                                                                        )
                                                                                    </small>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Status</td>
                                                                                <td>@if($bespokeApp->bespokeEligibility == null)
                                                                                        <label class="label label-processing">Under Review</label>
                                                                                    @elseif($bespokeApp->bespokeEligibility == 'Y')
                                                                                        <label class="label label-completed">Application Approved</label>
                                                                                    @else
                                                                                        <label class="label label-rejected">Application Declined</label>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        </table>

                                                                        <div class="row text-center">
                                                                            @if($bespokeApp->bespokeEligibility == 'Y')
                                                                                <label class="label label-completed">Application Approved</label>
                                                                            @else
                                                                                <a href="#" id="approveBespoke" class="btn-small btn btn-primary"><i
                                                                                            class="fa fa-check"></i>
                                                                                    Approve</a>
                                                                            @endif
                                                                            <a href="#" id="cancel-request" class="btn-small btn btn-danger"><i
                                                                                        class="fa fa-times"></i> Decline</a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    @else
                                                        <label>No data found</label>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#approveBespoke').on('click', function (e) {
                $(this).val('Processing...').attr('disabled', 'disabled');
                e.preventDefault();
                let data = {
                    "id": "{{$bespokeApp->id}}",
                    "bespokeEligibleFlag": 'Y',
                };
                console.log(data);
                $.ajax({
                    url: "{{route('admin.bespoke.confirm')}}",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (result) {
                        console.log(result);
                        swal('Application Approved', '', 'success');
                        location.reload();
                    },
                    error: function (e) {
                        console.log(e);
                        swal('Application Not Approved', '', 'error');
                        $('#approveBespoke').val('Approve').removeAttr('disabled', 'disabled');
                    }
                })
            });

            $('#cancel-request').click(function (e) {
                e.preventDefault();
                $('#cancel-request').val('Declining in progress...').attr('disabled', 'disabled');
                $('#reasonModal').modal({});
                let data = {
                    "id": {{$bespokeApp->id}},
                    "bespokeEligibleFlag": "N",
                    "reason": ""
                };
                $.ajax({
                    url: "{{route('admin.bespoke.confirm')}}",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (result) {
                        console.log(result);
                        swal('Application Declined', '', 'success');
                        location.reload();
                        $('#cancel-request').val('Declined Successfully').attr('disabled', 'disabled');
                    },
                    error: function (e) {
                        console.log(e);
                        $('#cancel-request').val('Decline').removeAttr('disabled', 'disabled');
                        swal('Application Not Approved', '', 'error');
                        $('#approveBespoke').val('Approve').removeAttr('disabled', 'disabled');
                    }
                })
            })
        });

    </script>
@endpush