@extends('layouts.admin.default')

@push('styles')
    <style type="text/css">
        .img-ho {
            width: 25px;
            padding: 0 0 3px 0;
        }
    </style>
@endpush

@section('pageTitle','Bespoke Request')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-scissors bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Bespoke Request</h5>
                                    <span>Special Request (advance bespoke bidding)</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Bespoke Request</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped category-table" id="refundTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">User Details</th>
                                                            <th scope="col">Date Ordered</th>
                                                            <th scope="col">Sample Image(s)</th>
                                                            <th scope="col">action</th>
                                                            {{--<th scope="col">status</th>--}}
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($bespokeRequests))
                                                            @if(count($bespokeRequests))
                                                                @foreach($bespokeRequests as $request)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td><strong>{{$request->customerName }}</strong> <br> {{ $request->email }}
                                                                            <br> {{$request->phone}}</td>
                                                                        <td>{{\Carbon\Carbon::createFromTimestamp($request->orderDate / 1000)->format('dS M, Y')}}
                                                                            <br>
                                                                            <small class="text-success">
                                                                                ({{\Carbon\Carbon::createFromTimestamp($request->orderDate / 1000)->diffForHumans()}}
                                                                                )
                                                                            </small>
                                                                        </td>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-2">
                                                                                    @if(isset($request->frontImage) && $request->frontImage !== null)
                                                                                        <a data-fancybox
                                                                                           href="{{str_replace('http','https', $request->frontImage)}}"><img
                                                                                                    class="img-ho"
                                                                                                    src="{{str_replace('http','https', $request->frontImage)}}"></a>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    @if(isset($request->backImage) && $request->backImage !== null)
                                                                                        <a data-fancybox
                                                                                           href="{{str_replace('http','https',$request->backImage)}}"><img
                                                                                                    class="img-ho"
                                                                                                    src="{{$request->backImage}}"></a>
                                                                                    @endif
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    @if(isset($request->sideImage) && $request->sideImage !== null)
                                                                                        <a data-fancybox
                                                                                           href="{{str_replace('http','https', $request->sideImage)}}"><img
                                                                                                    class="img-ho"
                                                                                                    src="{{str_replace('http','https',$request->sideImage)}}"></a>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td><a href="{{route('adminBespokePerRequest', $request->id)}}">view</a></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>No Application at the moment</p>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @if(isset($request->measurement))
                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                ...
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush