@extends('layouts.admin.default')

@section('pageTitle','Products')

@section('content')
    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        {{--<i class="feather icon-home bg-c-blue"></i>--}}
                        <div class="d-inline">
                            <h5>Users Uploaded Styles</h5>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="allproducts" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="order-table" class="table table-striped table-bordered nowrap reduced-font">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Style Name</th>
                                                            <th scope="col">Budget</th>
                                                            <th scope="col">Fit Type</th>
                                                            <th scope="col">Image</th>
                                                            <th scope="col">action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($uploadedStyles))
                                                            @if(count($uploadedStyles))
                                                                @foreach($uploadedStyles as $uploadedStyle)
                                                                    <tr>
                                                                        <td>{{@$uploadedStyle->styleName}}</td>
                                                                        <td>{{@$uploadedStyle->budget}}</td>
                                                                        <td>{{@$uploadedStyle->fitType}}</td>
                                                                        <td>
                                                                            @if(isset($uploadedStyle->bespokePicturesDTO))
                                                                                <a data-fancybox href="{{$uploadedStyle->bespokePicturesDTO->viewOne}}"><img class="img-ho" src="{{$uploadedStyle->bespokePicturesDTO->viewOne}}"></a>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{route('admin.uploadStyle.detail', $uploadedStyle->userBespokeStyleUploadId)}}">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @if(isset($totalOrder))
                                                    <div>
                                                        {!! $totalOrder->render() !!}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="verified" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Verified Products</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="verified-table"
                                                           class="table table-striped table-bordered nowrap reduced-font">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Product Name</th>
                                                            <th scope="col">Category</th>
                                                            <th scope="col">Designer Name</th>
                                                            <th scope="col">Price Details</th>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Image</th>
                                                            <th scope="col">action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($verifyProducts))
                                                            @if(count($verifyProducts))
                                                                @foreach($verifyProducts as $product)
                                                                    <tr>
                                                                        <td width="250px">{{$product->name}}</td>
                                                                        <td>{{$product->categoryName}}</td>
                                                                        <td>{{$product->designerName}}</td>
                                                                        <td>@if($product->slashedPrice > 0)
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <p style="margin-bottom: 0;">
                                                                                        ₦<span>{{number_format($product->slashedPrice)}}</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-xs-12 col-md-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <p class="text-danger"
                                                                                               style="font-size: 11px;">
                                                                                                <span style="text-decoration: line-through">₦{{number_format($product->amount)}}</span>
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="col-md-6 textRight discountpercent">
                                                                                            <p>
                                                                                    <span>&dash;{{$product->percentageDiscount}}
                                                                                        %</span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @else
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <p style="margin-bottom: 0;">
                                                                                        ₦{{number_format($product->amount)}}
                                                                                    </p>
                                                                                </div>
                                                                            @endif</td>
                                                                        {{--<td></td>--}}
                                                                        <td>
                                                                            @if ($product->availability === 'Y')
                                                                                <label class="label label-inspection">Ready To Wear</label>
                                                                            @elseif($product->availability === 'Y' && $product->bespokeProductDTO !== null)
                                                                                <label class="label label-processing">Bespoke</label>  and <label class="label label-inspection">Ready To Wear</label>
                                                                            @else
                                                                                <label class="label label-info">Unknown Status</label>
                                                                        @endif
                                                                        <td>
                                                                            <label class="label label-{{$product->verifiedFlag == 'N' ? 'rejected':'completed'}}">{{$product->verifiedFlag == 'N' ? 'Not Verified':'Verified'}}</label>
                                                                        </td>
                                                                        <td>
                                                                            @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                                                <a data-fancybox href="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                                                    <img class="img-ho" src="{{str_replace(['http','upload'],['https','upload/q_40'],$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                                                </a>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{route('admin.product.details', $product->id)}}">View
                                                                                Details</a></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="unverified" role="tabpanel"
                                         aria-labelledby="contact-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Unverified Products</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="unverified-table"
                                                           class="table table-striped table-bordered nowrap reduced-font">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Product Name</th>
                                                            <th scope="col">Category</th>
                                                            <th scope="col">Designer Name</th>
                                                            <th scope="col">Price Details</th>
                                                            <th scope="col">Product Type</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Image</th>
                                                            <th scope="col">action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($unverifyProducts))
                                                            @if(is_array($unverifyProducts))
                                                                @foreach($unverifyProducts as $product)
                                                                    <tr>
                                                                        <td>{{$product->name}}</td>
                                                                        <td>{{$product->categoryName}}</td>
                                                                        <td>{{$product->designerName}}</td>
                                                                        <td>
                                                                            @if($product->slashedPrice > 0)
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <p style="margin-bottom: 0;">
                                                                                        ₦<span>{{number_format($product->slashedPrice)}}</span>
                                                                                    </p>
                                                                                </div>
                                                                                <div class="col-xs-12 col-md-12">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6">
                                                                                            <p class="text-danger"
                                                                                               style="font-size: 11px;">
                                                                                                <span style="text-decoration: line-through">₦{{number_format($product->amount)}}</span>
                                                                                            </p>
                                                                                        </div>
                                                                                        <div class="col-md-6 textRight discountpercent">
                                                                                            <p><span>&dash;{{$product->percentageDiscount}}%</span>
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @else
                                                                                <div class="col-md-6 col-xs-12">
                                                                                    <p style="margin-bottom: 0;">
                                                                                        ₦{{number_format($product->amount)}}
                                                                                    </p>
                                                                                </div>
                                                                            @endif
                                                                        </td>
                                                                        {{--<td></td>--}}
                                                                        <td>
                                                                            @if ($product->availability === 'Y')
                                                                                <label class="label label-inspection">Ready To Wear</label>
                                                                            @elseif($product->availability === 'Y' && $product->bespokeProductDTO !== null)
                                                                                <label class="label label-processing">Bespoke</label>  and <label class="label label-inspection">Ready To Wear</label>
                                                                            @else
                                                                                <label class="label label-info">Unknown Status</label>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <label class="label label-{{$product->verifiedFlag == 'N' ? 'rejected':'completed'}}">{{$product->verifiedFlag == 'N' ? 'Not Verified':'Verified'}}</label>
                                                                        </td>
                                                                        <td>
                                                                            @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                                                <a data-fancybox href="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                                                    <img class="img-ho" src="{{str_replace(['http','upload'],['https','upload/q_40'],$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                                                </a>
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            <a href="{{route('admin.product.details', $product->id)}}">View
                                                                                Details</a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{route('exportProducts')}}" class="pull-right btn btn-primary">Export Product
                                    Data</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="styleSelector">
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $(document).ready(function () {

        });
    </script>
@endpush
