@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Change Admin Password')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>Change Password</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active"><i class="fa fa-dashboard"></i>Change Password</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <div class="row">
                    <div class="col-md-3">
                        <h5>Old Password</h5>
                        <input type="password" id="oldPassword" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                       <h5>New Password</h5>
                        <input type="password" id="newPassword" class="form-control">
                    </div>
                    {{--<div class="col-md-6">
                        <h5>Verify Password</h5>
                        <input type="password" id="newAdminVerifyPassword" class="form-control">
                    </div>--}}


                    <div class="col-md-4" style="margin-top: 30px;">
                        <input type="button" id="submitChangeAdminPassword"  class="btn btn-primary" value="Change Password">

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.addCategory').on('click', function () {
                if ($())
                    var data = {
                        "categoryName": $('#cat').val(),
                    }

            });
        });

        $('#submitChangeAdminPassword').on('click', function () {
            $('#submitChangeAdminPassword').val('Processing...').attr('disabled', 'disabled');
            let oldPassword = $('#oldPassword').val();
            let newPassword = $('#newPassword').val();
            if (oldPassword === '' || newPassword === '') {
                swal('Field is required', '', 'warning');
                $('#submitChangeAdminPassword').val('Update Password').removeAttr('disabled', 'disabled');
            } else {
                let data = {
                    email: 'developers@wawooh.com',
                    oldPassword: oldPassword,
                    newPassword: newPassword,
                };
                console.log(data);
                $.ajax({
                    url: '{{route('change.admin.password')}}',
                    method: 'POST',
                    data: JSON.stringify(data),
                    success: function (result) {
                        console.log(result);
                        if (result.status === "00") {
                            $(this).val('Password Changed').attr('disabled', 'disabled');
                            window.location.href = '{{route('admin.dashboard')}}';
                           // location.reload();
                        } else if (result.status === '99') {
                            swal('Something went wrong', '', 'error');
                            $('#submitChangeAdminPassword').val('Update Password').removeAttr('disabled', 'disabled');
                        }
                    },
                    error: function (e) {
                        $('#submitChangeAdminPassword').val('Update Password').removeAttr('disabled', 'disabled');
                        console.log(e);
                    }
                });
            }

        });
    </script>
@endpush