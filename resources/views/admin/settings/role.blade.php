@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Admin Role')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-user-check bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Admin Role Management</h5>
                                    <span>Add new record and manage sub-admins</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Admin Role</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12 promoCode">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Enter New Admin Record</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="all-order">
                                                    <div class="col-md-11 offset-1 pl-0">
                                                        <form class="">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="firstName">First Name*</label>
                                                                        <input type="text" class="form-control"
                                                                               placeholder="First Name*" id="firstName">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="lastName">Last Name*</label>
                                                                        <input type="text" class="form-control"
                                                                               placeholder="Last Name*" id="lastName">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5 intVal">
                                                                    <div class="form-group">
                                                                        <label for="email">Email Address*</label>
                                                                        <input type="text" class="form-control"
                                                                               placeholder="Email Address*" id="email">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="dateOfBirth">Date of Birth*</label>
                                                                        <input type="date" class="form-control"
                                                                               placeholder="Date of Birth*"
                                                                               id="dateOfBirth">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="gender">Gender*</label>
                                                                        <select class="form-control" id="gender">
                                                                            <option value="male">Male</option>
                                                                            <option value="female">Female</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="phoneNo">Phone Number*</label>
                                                                        <input type="text" class="form-control"
                                                                               placeholder="Phone Number*" id="phoneNo">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="password">Password*</label>
                                                                        <input type="password" class="form-control"
                                                                               placeholder="Password*" id="password">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="confirmPassword">Confirm
                                                                            Password*</label>
                                                                        <input type="password" class="form-control"
                                                                               placeholder="Confirm Password*"
                                                                               id="confirmPassword">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="role">Role*</label>
                                                                        <select class="form-control" id="role">
                                                                            <option value="" hidden>--select--</option>
                                                                            <option value="qa">Quality Assurance
                                                                            </option>
                                                                            <option value="customer_service">Customer
                                                                                Service
                                                                            </option>
                                                                            <option value="auditor">Auditor</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="col-md-3 offset-4"
                                                                     style="position: relative;">
                                                                    <div class="form-group">
                                                                        <input type="button" value="Add New Admin"
                                                                               class="form-control btn btn-warning"
                                                                               id="addUser">
                                                                        <img src="{{asset('img/AjaxLoader.gif')}}"
                                                                             alt="" id="preLoad" class="proLoad">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-block">
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a href="#customer_service" aria-controls="customer_service"
                                                           role="tab" class="nav-link active"
                                                           data-toggle="tab">Customer Service</a>
                                                    </li>

                                                    <li class="nav-item">
                                                        <a href="#qa" aria-controls="qa" role="tab" class="nav-link"
                                                           data-toggle="tab">QA</a>
                                                    </li>

                                                    <li class="nav-item">
                                                        <a href="#auditor" aria-controls="qa" role="tab"
                                                           class="nav-link"
                                                           data-toggle="tab">Auditor</a>
                                                    </li>
                                                </ul>

                                                <div class="tab-content mt-3">
                                                    <div role="tabpanel" class="tab-pane active" id="customer_service">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-striped" id="incompleteTable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">S/N</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Email</th>
                                                                    <th scope="col">PhoneNo</th>
                                                                    <th scope="col">Gender</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($dataForCustomer))
                                                                    @if(count($dataForCustomer))
                                                                        @foreach($dataForCustomer as $dataForCustomers)
                                                                            <tr>
                                                                                <td>{{$loop->iteration}}</td>
                                                                                <td>{{$dataForCustomers->firstName}} {{$dataForCustomers->lastName}}</td>
                                                                                <td>{{$dataForCustomers->email}}</td>
                                                                                <td>{{$dataForCustomers->phoneNo}}</td>
                                                                                <td>{{$dataForCustomers->gender}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="qa">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-striped"
                                                                   id="incompleteTable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">S/N</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Email</th>
                                                                    <th scope="col">PhoneNo</th>
                                                                    <th scope="col">Gender</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($dataForQa))
                                                                    @if(count($dataForQa))
                                                                        @foreach($dataForQa as $dataForQas)
                                                                            <tr>
                                                                                <td>{{$loop->iteration}}</td>
                                                                                <td>{{$dataForQas->firstName}} {{$dataForQas->lastName}}</td>
                                                                                <td>{{$dataForQas->email}}</td>
                                                                                <td>{{$dataForQas->phoneNo}}</td>
                                                                                <td>{{$dataForQas->gender}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="auditor">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-striped"
                                                                   id="incompleteTable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">S/N</th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Email</th>
                                                                    <th scope="col">PhoneNo</th>
                                                                    <th scope="col">Gender</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($dataForAuditor))
                                                                    @if(count($dataForAuditor))
                                                                        @foreach($dataForAuditor as $dataForAuditors)
                                                                            <tr>
                                                                                <td>{{$loop->iteration}}</td>
                                                                                <td>{{$dataForAuditors->firstName}} {{$dataForAuditors->lastName}}</td>
                                                                                <td>{{$dataForAuditors->email}}</td>
                                                                                <td>{{$dataForAuditors->phoneNo}}</td>
                                                                                <td>{{$dataForAuditors->gender}}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>
        $(document).on('click', '#addUser', function (e) {
            e.preventDefault();
            if ($('#password').val() !== $('#confirmPassword').val()) {
                swal('Password and Confirm Password not Identical', '', 'warning');
            } else {
                var data = {
                    firstName: $('#firstName').val(),
                    lastName: $('#lastName').val(),
                    email: $('#email').val(),
                    dateOfBirth: $('#dateOfBirth').val(),
                    gender: $('#gender').val(),
                    phoneNo: $('#phoneNo').val(),
                    password: $('#password').val(),
                    role: $('#role').val()
                }
                console.log(data);

                $.ajax({
                    url: "{{route('createAdminRole')}}",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.status === '00') {
                            swal(result.message, '', 'success');
                            window.location.reload();
                        } else {
                            swal(result.message, '', 'warning');
                            window.location.reload();
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                })
            }

        })
    </script>

@endpush