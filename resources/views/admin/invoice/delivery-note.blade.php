<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Delivery Note For: {{isset($invoice->orderNumber) ? $invoice->orderNumber : ''}} | Wawooh &mdash; Express
        yourself</title>

    <link rel="stylesheet" href="{{asset('css/invoice.css')}}">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <!-- Title Icon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    <style type="text/css">
        .toUpper {
            text-transform: capitalize;
        }
    </style>
</head>
<body>
<div class="container">
    @if(isset($invoice))
        <div class="invoice-container delivery-note">
            <div class="rowHolder noteHead">
                <div class="Col7"><img src="{{asset('img/img15.png')}}" class="img-invoice" alt="imga"></div>
                <div class="Col5">
                    <div class="order-details delivery-note">
                        <h4>Skywater Tower 6, Otunba Oshikoya drive, Mobolaji Bank Anthony way, Ikeja. Lagos.
                            Nigeria</h4>
                    </div>
                </div>
            </div>
            <div class="rowHolder">
                <div class="Col7 delivery-note">
                    <h3>Delivery Note</h3>
                </div>
                <div class="Col5">
                    <div class="order-details">
                        <table class="table">
                            <tr>
                                <td style="vertical-align: middle">Order No</td>
                                <td><span class="orderNum">{{$invoice->orderNumber}}</span></td>
                            </tr>
                            <tr>
                                <td>Order Date</td>
                                <td>{{\Carbon\Carbon::createFromTimestamp($invoice->orderDate / 1000)->format('d-M-Y')}}</td>
                            </tr>
                            <tr>
                                <td>Deliver To:</td>
                                <td>{{$invoice->lastName}} {{$invoice->firstName}}</td>
                            </tr>
                            <tr>
                                <td>Delivery Method</td>
                                <td>{{str_replace('_', ' ', $invoice->deliveryType)}}</td>
                            </tr>
                            <tr>
                                <td>Invoice No:</td>
                                <td>{{$invoice->orderNumber}}-W{{invoiceRefID('4')}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="header head-bg">
                <div class="rowHolder">
                    <div class="Col6"><h5>Delivery details:</h5></div>
                    <div class="Col6"><h5>Delivery address</h5></div>
                </div>
            </div>
            <div class="rowHolder">
                <div class="Col6">
                    <table class="table">
                        <tr>
                            <td width="150px">Dispatch date:</td>
                            <td>{{\Carbon\Carbon::createFromTimestamp($invoice->orderDate / 1000)->format('d-M-Y')}}</td>
                        </tr>
                        <tr>
                            <td>Order type:</td>
                            <td>Ready made</td>
                        </tr>
                        <tr>
                            <td>Amount Paid</td>
                            <td>{{env('NAIRA_SIGN')}}{{number_format($invoice->paidAmount)}}</td>
                        </tr>
                        <tr>
                            <td>Return Tracking ID:</td>
                            <td>RTW-{{$invoice->orderNumber}}</td>
                        </tr>
                    </table>
                </div>
                <div class="Col6">
                    <table class="table">
                        <tr>
                            <td width="100px">Address</td>
                            <td style="vertical-align: middle;">{{$invoice->deliveryAddress}}</td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>{{$invoice->city}}</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>{{$invoice->country}}</td>
                        </tr>
                        <tr>
                            <td>Contact:</td>
                            <td>{{$invoice->phoneNumber}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="header head-bg">
                <div class="rowHolder">
                    <div class="Col6"><h5>Delivery Agent:</h5></div>
                    <div class="Col6"><h5>Pickup address</h5></div>
                </div>
            </div>
            <div class="rowHolder">
                <div class="Col6">
                    <table class="table">
                        <tr>
                            <td width="150px">Pickup date:</td>
                            <td>{{\Carbon\Carbon::createFromTimestamp($invoice->orderDate / 1000)->format('d-M-Y')}}</td>
                        </tr>
                        <tr>
                            <td>Delivery Service:</td>
                            <td>Others</td>
                        </tr>
                        <tr>
                            <td>Tracking Number:</td>
                            <td>No</td>
                        </tr>
                        <tr>
                            <td>Return Tracking ID:</td>
                            <td>RTW-{{$invoice->orderNumber}}</td>
                        </tr>
                    </table>
                </div>
                <div class="Col6">
                    <table class="table">
                        <tr>
                            <td width="100px">Address</td>
                            <td style="vertical-align: middle;">Skywater Tower 6, Otunba Oshikoya drive, Mobolaji Bank Anthony way, Ikeja. Lagos.
                                Nigeria</td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>Lagos</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>Nigeria</td>
                        </tr>
                        <tr>
                            <td>Contact:</td>
                            <td>07000929664</td>
                        </tr>
                    </table>
                </div>
            </div>

            <h5 class="d-title">Order details</h5>
            <div class="orderItemDetails" style="border-bottom: none !important;">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Item No.</th>
                        <th>Item Name</th>
                        <th>Order No</th>
                        <th>Package Status</th>
                        <th>Number of Items in package</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($invoice->item))
                        @if(count($invoice->item))
                            @foreach($invoice->item as $item)
                                <tr style="text-align: center;">
                                    <td>{{$loop->iteration}}</td>
                                    <td><span class="toUpper">{{strtolower($item->productName)}}</span></td>
                                    <td>{{$invoice->orderNumber}}-{{invoiceRefID('4')}}</td>
                                    <td>Good</td>
                                    <td>{{$item->quantity}}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="rowHolder appendSign">
                <div>
                    <h5>Delivery Agent Sign</h5>
                </div>
                <div>
                    <h5>Wawooh Agent Sign</h5>
                </div>
            </div>
            <div class="rowHolder" style="text-align: center; margin-top: 2em;">
                <div class="Col12">
                    <div class="content-user">
                        <p>
                            Tel: 0700WAWOOH (0700929664) or Email us: customercare@wawooh.com <br>
                            https://www.wawooh.com
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="invoice-container">
            <div class="rowHolder">
                <div class="Col5"><a onclick="window.print()" href="#" class="btn-next">Print</a></div>
            </div>
        </div>
    @else
        <div class="invoice-container" style="text-align: center;">
            <div class="rowHolder">
                <div class="Col6 offset-3">
                    <div class="alert alert-warning"><strong>Invoice Not Available</strong></div>
                </div>
            </div>
        </div>
    @endif
</div>
</body>
</html>