{{--@php
// The function header by sending raw excel
//header("Content-type: application/vnd-ms-word");
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

// Defines the name of the export file "codelution-export.xls"
header("Content-Disposition: attachment; filename=order-list.xls");

// Add data table
@endphp--}}
        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Export Orders | Admin &mdash; Express You</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>
<section class="export-orders">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">P_ID</th>
            <th scope="col">Order No</th>
            <th scope="col">Total</th>
            <th scope="col">Customer Name</th>
            <th scope="col">Delivery Address</th>
            <th scope="col">Delivery Name</th>
            <th scope="col">Delivery Phone</th>
            <th scope="col">Product</th>
            <th scope="col">Quantity</th>
            {{--<th scope="col">Email</th>--}}
            <th scope="col">Payment</th>
            <th scope="col">Order Type</th>
            <th scope="col">Delivery Type</th>
            <th scope="col">Order Date</th>
            <th scope="col">Time</th>
            <th scope="col">Status</th>
            <th scope="col">Delivery Date</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($orders))
            @if(count($orders))
                @foreach($orders as $order)
                    @if(isset($order->itemsList))
                        @if(count($order->itemsList))
                            @foreach($order->itemsList as $item)
                                <tr>
                                    <td>{{$item->productId}}</td>
                                    <td>{{$order->orderNumber}}</td>
                                    <td>₦{{number_format($order->totalAmount)}}</td>
                                    <td>{{$item->customerName}}</td>
                                    <td>{{$order->deliveryAddress}}</td>
                                    <td>{{$order->customerName}}</td>
                                    <td>{{$order->deliveryPhoneNumber}}</td>
                                    <td>{{$item->productName }}</td>
                                    <td>{{$item->quantity}}</td>
                                    {{--<td>{{$item->customerEmail}}</td>--}}
                                    <td>{{strtoupper(str_replace('_', ' ', $order->paymentType))}}</td>
                                    <td>{{$item->readyMade !== false ? 'Bespoke':'Ready To Wear'}}</td>
                                    <td>{{str_replace('_', ' ', $order->deliveryType)}}
                                        @if(!is_null($order->deliveryAddress))
                                            <label class="label-info">Standard Delivery</label>
                                        @else
                                            <label class="label label-primary">Pick up</label>
                                        @endif
                                    </td>
                                    <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}</td>
                                    <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($order->orderDate))->format('h:m:i A')}}</td>
                                    <td>
                                        @if($item->status == 'A')
                                            <label class="label label-info">Accepted By Designer</label>
                                        @elseif($item->status == 'PC')
                                            <label class="label label-primary">Payment
                                                Confirmed</label>
                                        @elseif($item->status == 'C')
                                            <label class="label label-danger">Cancelled</label>
                                        @elseif($item->status == 'P')
                                            <label class="label label-warning">Pending</label>
                                        @elseif($item->status == 'PC')
                                            <label class="label label-processing">Payment Confirmed</label>
                                        @elseif($item->status == 'OP')
                                            <label class="label label-warning">Pending</label>
                                        @elseif($item->status == 'OP')
                                            <label class="label label-info">Order
                                                Processing</label>
                                        @elseif($item->status == 'OR')
                                            <label class="label label-danger">Order
                                                Rejected</label>
                                        @elseif($item->status == 'CO')
                                            <label class="label label-success">Completed By Designer</label>
                                        @elseif($item->status == 'RI')
                                            <label class="label label-info">Ready for
                                                Inspection</label>
                                        @elseif($item->status == 'RS')
                                            <label class="label label-info">Ready for
                                                Shipping</label>
                                        @elseif($item->status == 'OS')
                                            <label class="label label-primary">Order
                                                Shipped</label>
                                        @elseif($item->status == 'NV')
                                            <label class="label label-danger">Not
                                                Verified</label>
                                        @elseif($item->status == 'WR')
                                            <label class="label label-danger">Wawooh Rejected</label>
                                        @elseif($item->status == 'WC')
                                            <label class="label label-success">Wawooh Collected</label>
                                        @elseif($item->status == 'D')
                                            <label class="label label-success">Order
                                                Delivered</label>
                                        @endif
                                    </td>
                                    <td>{{$order->deliveryDate}}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endif
                @endforeach
            @endif
        @endif
        </tbody>
    </table>

    {{--<div class="row">--}}
    {{--<div class="col-md-12" style="text-align: center; margin-bottom: 20px;">--}}
    {{--<a class="btn btn-success" href="{{route('admin.export.file')}}">Export Preview</a>--}}
    {{--</div>--}}
    {{--</div>--}}
</section>
</body>
</html>