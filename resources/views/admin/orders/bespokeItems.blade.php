@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Order Details')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-briefcase bg-c-blue"></i>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.orders.all')}}">Orders</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Order Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Order Details</h5>
                                            </div>
                                            <div class="card-block">
                                                <table style="font-size: 13px" class="table table-bordered">
                                                    <tr>
                                                        <td>Order Number</td>
                                                        <td><strong>{{$order->orderNum}}</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Order Status</td>
                                                        <td>{{$order->status}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paid Amount</td>
                                                        <td>₦{{number_format($order->price)}}</td>
                                                    </tr>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Details of each Item</h5>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="row reduced-font" style="padding: 0 15px;">
                                                            <div class="col-md-4 pr-3"
                                                                 style="padding-left: 0; margin-bottom: 1em;">
                                                                <a data-fancybox href="{{$order->fabricDetailsDTO[0]->fabricPicture}}">
                                                                    <img style="border: 1px solid rgba(0,0,0,.2);" src="{{$order->fabricDetailsDTO[0]->fabricPicture}}"
                                                                            alt="product-image" height="150px">
                                                                </a>
                                                            </div>

                                                            <div class="col-md-8">
                                                                <div class="row pb-3">
                                                                    <div class="col-md-6"><span class="makeBold brand-color">Time of Order:</span>
                                                                    </div>
                                                                    <div class="col-md-6">
{{--
                                                                        {{\Carbon\Carbon::createFromTimeStamp(strtotime($orderItem->orderDate))->format('h:m:i A')}}
--}}
                                                                    </div>
                                                                </div>
                                                                <div class="row pb-3">
                                                                    <div class="col-md-3">
                                                                        <span class="makeBold brand-color">Designer Status:</span>
                                                                    </div>

                                                                        <div class="col-md-6">
                                                                            <label class="label label-processing">{{$order->status}}</label>
                                                                        </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Item(s) in the Order</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped" style="font-size: 13px">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Order No</th>
                                                            <th scope="col">Product</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Status</th>
                                                            {{--<th scope="col">Action</th>--}}
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                                    <tr>
                                                                        <td>{{$order->orderNum}}</td>
                                                                        <td>{{$order->productName}}</td>
                                                                        <td>₦{{number_format($order->price)}}</td>
                                                                        <td>
                                                                        </td>
                                                                        <td>
                                                                                <label class="label label-completed">{{$order->status}}</label>

                                                                        </td>
                                                                        {{--<td class="btn-display">
                                                                           --}}{{-- @if($item->status == 'A')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='C' class="btn btn-wawooh accept">Confirm
                                                                                </button><span class="mySpinner"></span>
                                                                            @elseif($item->status == 'CO')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        statId=6 stat='RI' class="btn btn-wawooh accept">Ready for
                                                                                    Inspection
                                                                                </button><span class="mySpinner"></span>
                                                                            @elseif($item->deliveryStatus == 'P')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='PC' class="btn btn-wawooh accept">Approve
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>


                                                                            @elseif($item->status == 'RI')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='RS' statId=7 class="btn btn-wawooh accept">Ready for
                                                                                    shipping
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        statId=11 stat="PI" class="btn btn-wawooh accept">Pass
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='RS' statId=7 class="btn btn-wawooh accept">Fail
                                                                                </button>
                                                                                <a href="#ModalFail" data-toggle="modal" class="btn btn-warning"
                                                                                   id="inspection-failed" stat="RI">Fail</a><span
                                                                                        class="mySpinner"></span>
                                                                            @elseif($item->status == 'RS')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='OS' statId=8 class="btn btn-wawooh accept">Ready for
                                                                                    shipping
                                                                                </button><span class="mySpinner"></span>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='OS' statId=8 class="btn btn-wawooh accept">Ship
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>
                                                                            @elseif($item->status == 'OS')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                        order='{{$order->id}}' stat='D' statId=9
                                                                                        class="btn btn-wawooh accept">Delivered
                                                                                </button><span class="mySpinner"></span>
                                                                            @elseif($item->status == 'RS')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='D' statId=9 class="btn btn-wawooh accept">Delivered
                                                                                </button><span class="mySpinner"></span>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>

                                                                            @endif
                            --}}{{--
                                                                        </td>--}}
                                                                    </tr>


                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="styleSelector">
                </div>
            </div>
        </div>
    </div>
{{--
    <div class="modal fade" data-backdrop="static" id="ModalFail" tabindex="-1" role="dialog"
         aria-labelledby="ModalFail" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="ModalFail">
                        <h5>Reason</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" id="reason-text">
                        </div>
                        <div>
                            --}}{{--{{dd($order)}}--}}{{--
                            <input type="submit" class="btn btn-primary reason-btn"
                                   customer="{{$order->userId}}" orderNumber="{{@$order->orderNumber}}"
                                   order="{{$order->id}}">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-top: 5em;" class="modal fade" data-backdrop="static" id="ModalComplain" tabindex="-1"
         role="dialog"
         aria-labelledby="ModalComplain" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="ModalFail">

                        @if(isset($order->itemsList))
                            @if(count($order->itemsList))
                                @foreach($order->itemsList as $item)
                                    <div class="productComplaint">
                                        <h5>Complains for <strong>{{$item->productName}}</strong></h5>
                                        @if(isset($item->userComplain))
                                            <span>{{$item->userComplain}}</span>
                                        @else
                                            <p>No Complaint found!</p>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

@endsection

@push('scripts')

    <script>
        $(document).ready(function () {


            $('.accept').on('click', function () {
                $('.mySpinner').addClass('lds-dual-ring');
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                        {{-- if($(this).attr('stat', 'RI')){
                             $('.btn-display').html('<div><button  class="btn btn-info" customer="{{$order->customerId}}" orderNumber="{{@$order->orderNumber}}" order="{{$order->id}}" statId=11 stat="PI" id="inspection-passed">Pass</button><a href="#ModalFail" data-toggle="modal" class="btn btn-warning" id="inspection-failed" stat="RI">Fail</a></div>');
                                    }else {--}}

                var data = {
                        "id": $(this).attr('order'),
                        "orderNumber": $(this).attr('orderNumber'),
                        "status": $(this).attr('stat'),
                        "customerId": $(this).attr('customer'),
                        "statusId": $(this).attr('statId')
                    };
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                url: "{{env('GET_BASE_URL')}}/fashion/order/admin/updateorderitem",
                                type: "POST",
                                dataType: 'json',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                data: JSON.stringify(data),

                                success: function (result) {
                                    $('.mySpinner').removeClass('lds-dual-ring');
                                    //window.location.reload();

                                    // $('div.loader').hide();
                                    if (result.status === 0) {
                                        swal('Status changed successfully', '', 'success');
                                        location.reload();
                                    } else {
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('.mySpinner').removeClass('lds-dual-ring');
                                    $('#loaderModal').modal('hide');

                                    swal(e, '', 'error');
                                }
                            });
                        } else {
                            $('.mySpinner').removeClass('lds-dual-ring');
                            $('div.loader').hide();
                            $('#loaderModal').modal('hide');
                        }
                    });
            });

            $(document).on('click', '#inspection-passed', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        "id": $(this).attr('order'),
                        "orderNumber": $(this).attr('orderNumber'),
                        "status": $(this).attr('stat'),
                        "customerId": $(this).attr('customer'),
                        "statusId": $(this).attr('statId')
                    };
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click ok to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                url: "{{env('GET_BASE_URL')}}/fashion/order/admin/updateorderitem",
                                type: "POST",
                                dataType: 'json',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                data: JSON.stringify(data),
                                success: function (result) {
                                    if (result.status === 0) {
                                        swal('status changed successfully', '', 'success');
                                        location.reload();
                                    } else {
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                    swal(e, '', 'error');
                                }
                            });
                        }
                    });
            });

            $(document).on('click', '.reason-btn', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        "id": $(this).attr('order'),
                        "status": 'FI',
                        "statusId": 12,
                        "orderNumber": $(this).attr('orderNumber'),
                        "customerId": $(this).attr('customer'),
                        "action": $('#reason-text').val()
                    }
                $.ajax({
                    url: "{{env('GET_BASE_URL')}}/fashion/order/admin/updateorderitem",
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (result) {
                        window.location.reload();
                    },
                    error: function (e) {
                        $('div.loader').hide();
                        $('#loaderModal').modal('hide');
                        swal(e, '', 'error');
                    }
                });
            });

            {{--$('.generateInvoice').on('click', function () {--}}
                {{--window.location.href = "{{route('generateInvoice')}}";--}}
            {{--})--}}
        })

    </script>

@endpush