@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Order Details')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-briefcase bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Order Details:
                                        ({{count($order->itemsList)}} {{count($order->itemsList) > 1 ? 'items':'item'}})</h5>
                                    <span>The item(s) is a particular order</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.orders.all')}}">Orders</a></li>
                                    <li class="breadcrumb-item"><a href="#!">Order Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Order Details</h5>
                                            </div>
                                            <div class="card-block">
                                                <table style="font-size: 13px" class="table table-bordered">
                                                    <tr>
                                                        <td>Order Number</td>
                                                        <td><strong>{{$order->orderNumber}}</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Order Status</td>
                                                        <td><p>
                                                                @if(isset($order->itemsList))
                                                                    @if(count($order->itemsList))
                                                                        @foreach($order->itemsList as $list)
                                                                            @if($list->status == 'PC')
                                                                                <label class="label label-completed">Payment
                                                                                    Confirmed</label>
                                                                            @elseif($list->status == 'P')
                                                                                <label class="label label-pending">Pending</label>
                                                                            @elseif($list->status == 'C')
                                                                                <label class="label label-rejected">Order Cancelled</label>
                                                                            @elseif($list->status == 'OP')
                                                                                <label class="label label-processing">Order
                                                                                    Processing</label>
                                                                            @elseif($list->status == 'A')
                                                                                <label class="label label-processing">Accepted</label>
                                                                            @elseif($list->status == 'WR')
                                                                                <label class="label label-rejected">Wawooh
                                                                                    Rejected</label>
                                                                            @elseif($list->status == 'WC')
                                                                                <label class="label label-completed">Wawooh
                                                                                    Collected</label>
                                                                            @elseif($list->status == 'CO')
                                                                                <label class="label label-completed">Completed</label>
                                                                            @elseif($list->status == 'RS')
                                                                                <label class="label label-completed">Ready
                                                                                    for
                                                                                    Shipping</label>
                                                                            @elseif($list->status == 'RI')
                                                                                <label class="label label-inspection">Ready
                                                                                    for
                                                                                    Inspection</label>
                                                                            @elseif($list->status == 'OS')
                                                                                <label class="label label-completed">Order
                                                                                    Shipped</label>
                                                                            @elseif($list->status == 'OD')
                                                                                <label class="label label-completed">Order
                                                                                    Delivered</label>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            </p></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Payment Method</td>
                                                        <td style="text-transform: capitalize;">{{title_case(str_replace('_',' ',strtolower($order->paymentType)))}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Customer Detail</td>
                                                        <td>@if(isset($order->customerName)) {{$order->customerName}} @endif
                                                            <br>
                                                                {{--<a href="mailto:{{$order->email}}">{{$order->email}}</a>--}}
                                                            <br>
                                                            <a href="tel:{{$order->customerPhoneNumber}}">{{$order->customerPhoneNumber}}</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paid Amount</td>
                                                        <td>₦{{number_format($order->paidAmount)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Total Amount</td>
                                                        <td>₦{{number_format($order->totalAmount)}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Delivery Type</td>
                                                        <td>
                                                            <label class="label label-{{$order->deliveryAddress === null ? 'processing':'inspection'}}">
                                                                {{$order->deliveryAddress === null ? 'Pick Up':'Standard Delivery'}}
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    @if ($order->deliveryAddress !== null)
                                                        <tr>
                                                            <td>Delivery Details</td>
                                                            <td>
                                                                <strong>{{$order->deliveryName}}</strong> <br>
                                                                {{ucfirst($order->deliveryAddress)}}, <br>
                                                                {{$order->deliveryState .'.'}}
                                                                <br>
                                                                {{$order->deliveryCountry.'.'}}
                                                                <br>
                                                                <a href="tel:{{$order->deliveryPhoneNumber}}">
                                                                    {{$order->deliveryPhoneNumber}}
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        @if(isset($order->itemsList))
                                            @foreach($order->itemsList as $orderItem)
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h5>Details of each Item</h5>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="row reduced-font" style="padding: 0 15px;">
                                                            <div class="col-md-4 pr-3"
                                                                 style="padding-left: 0; margin-bottom: 1em;">
                                                                <a data-fancybox href="{{str_replace('http','https', $orderItem->productPicture)}}">
                                                                    <img style="border: 1px solid rgba(0,0,0,.2);" src="{{str_replace(['http','upload'],['https','upload/q_40'], $orderItem->productPicture)}}"
                                                                            alt="product-image" height="150px">
                                                                </a>
                                                            </div>

                                                            <div class="col-md-8">
                                                                <div class="row pb-3">
                                                                    <div class="col-md-6">
                                                                        <span class="makeBold brand-color">Product Size:</span>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <strong>{{$orderItem->size}}</strong>
                                                                    </div>
                                                                </div>
                                                                <div class="row pb-3">
                                                                    <div class="col-md-6">
                                                                        <span class="makeBold brand-color">Designer:</span>
                                                                    </div>
                                                                    <div class="col-md-6 designerContentId">
                                                                        {{$orderItem->designerStoreName}}
                                                                    </div>
                                                                </div>
                                                                @if(isset($orderItem->promoCode))
                                                                    <div class="row pb-3">
                                                                        <div class="col-md-6">
                                                                                <span class="makeBold brand-color">Promo Code</span>
                                                                        </div>
                                                                        <div class="col-md-6">{{$orderItem->promoCode}}</div>
                                                                    </div>
                                                                @endif
                                                                <div class="row pb-3">
                                                                    <div class="col-md-6"><span class="makeBold brand-color">Time of Order:</span>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        {{\Carbon\Carbon::createFromTimeStamp(strtotime($orderItem->orderDate))->format('h:m:i A')}}
                                                                    </div>
                                                                </div>
                                                                <div class="row pb-3">
                                                                    <div class="col-md-3">
                                                                        <span class="makeBold brand-color">Designer Status:</span>
                                                                    </div>
                                                                    @if($orderItem->status !== 'P')
                                                                        <div class="col-md-6">
                                                                            <label class="label label-processing">Accepted
                                                                                by Designer</label>
                                                                        </div>
                                                                    @else
                                                                        <div class="col-md-9">
                                                                            <label class="label label-pending">Waiting
                                                                                for Designer to Accept</label>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Item(s) in the Order</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped" style="font-size: 13px">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Order No</th>
                                                            <th scope="col">Product</th>
                                                            <th scope="col">Quantity</th>
                                                            <th scope="col">Price</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Status</th>
                                                            {{--<th scope="col">Action</th>--}}
                                                            <th scope="col">Has Complaint?</th>
                                                            <th scope="col"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($order->itemsList))
                                                            @if(count($order->itemsList))
                                                                @foreach($order->itemsList as $item)
                                                                    <tr>
                                                                        <td>{{$item->orderNumber}}</td>
                                                                        <td>{{$item->productName}}</td>
                                                                        <td>{{$item->quantity}}</td>
                                                                        <td>₦{{number_format($item->amount)}}</td>
                                                                        <td>{{\Carbon\Carbon::parse($item->orderDate)->format('d-M-Y')}}
                                                                            <br>
                                                                            <small>
                                                                                ({{\Carbon\Carbon::createFromTimeStamp(strtotime($orderItem->orderDate))->diffForHumans()}}
                                                                                )
                                                                            </small>
                                                                        </td>
                                                                        <td>
                                                                            @if($item->status == 'PC')
                                                                                <label class="label label-completed">Payment
                                                                                    Confirmed</label>
                                                                            @elseif($item->status == 'P')
                                                                                <label class="label label-pending">Pending</label>
                                                                            @elseif($item->status == 'OP')
                                                                                <label class="label label-processing">Order
                                                                                    Processing</label>
                                                                            @elseif($item->status == 'OR')
                                                                                <label class="label label-rejected">Order
                                                                                    Rejected</label>
                                                                            @elseif($item->status == 'CO')
                                                                                <label class="label label-completed">Completed
                                                                                    by designer</label>
                                                                            @elseif($item->status == 'RI')
                                                                                <label class="label label-inspection">Ready
                                                                                    for Inspection</label>
                                                                            @elseif($item->status == 'RS')
                                                                                <label class="label label-processing">Ready
                                                                                    for Shipping</label>
                                                                            @elseif($item->status == 'OS')
                                                                                <label class="label label-completed">Order
                                                                                    Shipped</label>
                                                                            @elseif($item->status == 'NV')
                                                                                <label class="label label-rejected">Not
                                                                                    Verified</label>
                                                                            @elseif($item->status == 'D')
                                                                                <label class="label label-completed">Order
                                                                                    Delivered</label>
                                                                            @elseif($item->status == 'C')
                                                                                <label class="label label-rejected">Order
                                                                                    Cancelled</label>
                                                                            @elseif($item->status == 'WR')
                                                                                <label class="label label-rejected">Wawooh
                                                                                    Rejected</label>
                                                                            @elseif($item->status == 'WC')
                                                                                <label class="label label-completed">Wawooh
                                                                                    Collected</label>
                                                                            @elseif($item->status == 'A')
                                                                                <label class="label label-processing">Accepted</label>
                                                                            @elseif($item->status == 'PC')
                                                                                <label class="label label-completed">Payment
                                                                                    Confirmed</label>
                                                                            @endif

                                                                        </td>
                                                                        {{--<td class="btn-display">
                                                                           --}}{{-- @if($item->status == 'A')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='C' class="btn btn-wawooh accept">Confirm
                                                                                </button><span class="mySpinner"></span>
                                                                            @elseif($item->status == 'CO')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        statId=6 stat='RI' class="btn btn-wawooh accept">Ready for
                                                                                    Inspection
                                                                                </button><span class="mySpinner"></span>
                                                                            @elseif($item->deliveryStatus == 'P')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='PC' class="btn btn-wawooh accept">Approve
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>


                                                                            @elseif($item->status == 'RI')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='RS' statId=7 class="btn btn-wawooh accept">Ready for
                                                                                    shipping
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        statId=11 stat="PI" class="btn btn-wawooh accept">Pass
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='RS' statId=7 class="btn btn-wawooh accept">Fail
                                                                                </button>
                                                                                <a href="#ModalFail" data-toggle="modal" class="btn btn-warning"
                                                                                   id="inspection-failed" stat="RI">Fail</a><span
                                                                                        class="mySpinner"></span>
                                                                            @elseif($item->status == 'RS')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='OS' statId=8 class="btn btn-wawooh accept">Ready for
                                                                                    shipping
                                                                                </button><span class="mySpinner"></span>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='OS' statId=8 class="btn btn-wawooh accept">Ship
                                                                                </button>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>
                                                                            @elseif($item->status == 'OS')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                        order='{{$order->id}}' stat='D' statId=9
                                                                                        class="btn btn-wawooh accept">Delivered
                                                                                </button><span class="mySpinner"></span>
                                                                            @elseif($item->status == 'RS')
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='D' statId=9 class="btn btn-wawooh accept">Delivered
                                                                                </button><span class="mySpinner"></span>
                                                                                <button customer='{{$item->customerId}}'
                                                                                        orderNumber='{{@$item->orderNumber}}' order='{{$item->id}}'
                                                                                        stat='DE' class="btn btn-wawooh accept">Decline
                                                                                </button>

                                                                            @endif
                            --}}{{--
                                                                        </td>--}}
                                                                        <td><a href="#ModalComplain"
                                                                               data-target="#ModalComplain"
                                                                               data-toggle="modal">{{@$item->userComplain ? 'Yes' : 'No'}}</a>
                                                                        </td>
                                                                        <td>
                                                                            @if(!($item->status == 'OS' || $item->status == 'D'))
                                                                                <label class="label label-warning">Invoice Not Available</label>
                                                                            @else
                                                                                <a target="_new" href="{{route('generateInvoice', $order->orderNumber)}}" class="btn btn-sm btn-primary generateInvoice">Generate Invoice</a>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="styleSelector">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" id="ModalFail" tabindex="-1" role="dialog"
         aria-labelledby="ModalFail" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="ModalFail">
                        <h5>Reason</h5>
                        <div class="form-group">
                            <input type="text" class="form-control" id="reason-text">
                        </div>
                        <div>
                            {{--{{dd($order)}}--}}
                            <input type="submit" class="btn btn-primary reason-btn"
                                   customer="{{$order->userId}}" orderNumber="{{@$order->orderNumber}}"
                                   order="{{$order->id}}">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="margin-top: 5em;" class="modal fade" data-backdrop="static" id="ModalComplain" tabindex="-1"
         role="dialog"
         aria-labelledby="ModalComplain" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="ModalFail">

                        @if(isset($order->itemsList))
                            @if(count($order->itemsList))
                                @foreach($order->itemsList as $item)
                                    <div class="productComplaint">
                                        <h5>Complains for <strong>{{$item->productName}}</strong></h5>
                                        @if(isset($item->userComplain))
                                            <span>{{$item->userComplain}}</span>
                                        @else
                                            <p>No Complaint found!</p>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script>
        $(document).ready(function () {


            $('.accept').on('click', function () {
                $('.mySpinner').addClass('lds-dual-ring');
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                        {{-- if($(this).attr('stat', 'RI')){
                             $('.btn-display').html('<div><button  class="btn btn-info" customer="{{$order->customerId}}" orderNumber="{{@$order->orderNumber}}" order="{{$order->id}}" statId=11 stat="PI" id="inspection-passed">Pass</button><a href="#ModalFail" data-toggle="modal" class="btn btn-warning" id="inspection-failed" stat="RI">Fail</a></div>');
                                    }else {--}}

                var data = {
                        "id": $(this).attr('order'),
                        "orderNumber": $(this).attr('orderNumber'),
                        "status": $(this).attr('stat'),
                        "customerId": $(this).attr('customer'),
                        "statusId": $(this).attr('statId')
                    };
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                url: "{{env('GET_BASE_URL')}}/fashion/order/admin/updateorderitem",
                                type: "POST",
                                dataType: 'json',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                data: JSON.stringify(data),

                                success: function (result) {
                                    $('.mySpinner').removeClass('lds-dual-ring');
                                    //window.location.reload();

                                    // $('div.loader').hide();
                                    if (result.status === 0) {
                                        swal('Status changed successfully', '', 'success');
                                        location.reload();
                                    } else {
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('.mySpinner').removeClass('lds-dual-ring');
                                    $('#loaderModal').modal('hide');

                                    swal(e, '', 'error');
                                }
                            });
                        } else {
                            $('.mySpinner').removeClass('lds-dual-ring');
                            $('div.loader').hide();
                            $('#loaderModal').modal('hide');
                        }
                    });
            });

            $(document).on('click', '#inspection-passed', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        "id": $(this).attr('order'),
                        "orderNumber": $(this).attr('orderNumber'),
                        "status": $(this).attr('stat'),
                        "customerId": $(this).attr('customer'),
                        "statusId": $(this).attr('statId')
                    };
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click ok to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                url: "{{env('GET_BASE_URL')}}/fashion/order/admin/updateorderitem",
                                type: "POST",
                                dataType: 'json',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                data: JSON.stringify(data),
                                success: function (result) {
                                    if (result.status === 0) {
                                        swal('status changed successfully', '', 'success');
                                        location.reload();
                                    } else {
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                    swal(e, '', 'error');
                                }
                            });
                        }
                    });
            });

            $(document).on('click', '.reason-btn', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        "id": $(this).attr('order'),
                        "status": 'FI',
                        "statusId": 12,
                        "orderNumber": $(this).attr('orderNumber'),
                        "customerId": $(this).attr('customer'),
                        "action": $('#reason-text').val()
                    }
                $.ajax({
                    url: "{{env('GET_BASE_URL')}}/fashion/order/admin/updateorderitem",
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (result) {
                        window.location.reload();
                    },
                    error: function (e) {
                        $('div.loader').hide();
                        $('#loaderModal').modal('hide');
                        swal(e, '', 'error');
                    }
                });
            });

            {{--$('.generateInvoice').on('click', function () {--}}
                {{--window.location.href = "{{route('generateInvoice')}}";--}}
            {{--})--}}
        })

    </script>

@endpush