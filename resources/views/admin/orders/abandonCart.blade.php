@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Abandon Carts')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-briefcase bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Abandon Cart</h5>
                                    <span>The list of all abandon cart items</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Abandon Carts</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#all" aria-controls="all"
                                                           role="tab" data-toggle="tab">Abandon Cart</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content mt-3">
                                                    <div role="tabpanel" class="tab-pane active" id="all">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-bordered table-hover"
                                                                   id="allOrderTable98">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col"></th>
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Email</th>
                                                                    <th scope="col">Phone</th>
                                                                    {{--<th scope="col">Status</th>--}}
                                                                    <th scope="col">Product Name</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($res))
                                                                    @if(count($res))
                                                                        @foreach($res as $order)
                                                                            <tr class='clickable-row'>
                                                                                <td>{{$loop->index}}</td>
                                                                                <td>{{$order->firstName}} {{$order->lastName}}</td>
                                                                                <td><a href="mailto:{{$order->email}}">{{$order->email}}</a></td>
                                                                                <td>
                                                                                    <a href="tel:{{$order->phoneNumber}}">{{$order->phoneNumber}}</a></td>
                                                                                <td>
                                                                                    {{-- @foreach($order->abandonedItemsDTO as $cartItem)--}}
                                                                                    <div>
                                                                                        <span>{{$order->abandonedItemsDTO->productName}}</span>
                                                                                        <span>QTY: {{$order->abandonedItemsDTO->quantity}}</span>
                                                                                    </div>
                                                                                    {{--
                                                                                                                                                                           @endforeach
                                                                                    --}}
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>


    </script>

@endpush