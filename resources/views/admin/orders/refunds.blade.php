@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Refunds')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-pencil bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Refund Management</h5>
                                    <span>...</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Refund</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped category-table" id="refundTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Customer</th>
                                                            <th scope="col">Order Number</th>
                                                            <th scope="col">Product Name</th>
                                                            <th scope="col">Amount</th>
                                                            <th scope="col"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($allRefunds))
                                                            @if(count($allRefunds))
                                                                @foreach($allRefunds as $allRefund)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$allRefund->accountName}}</td>
                                                                        <td>{{$allRefund->orderNum}}</td>
                                                                        <td>{{$allRefund->productName}}</td>
                                                                        <td>{{$allRefund->amount}}</td>
                                                                        <td>
                                                                            <buttton class="btn btn-wawooh refund">Refund</buttton>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>No transaction at the moment</p>
                                                        @endif

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('.refund').click(function () {
            swal("Are you sure you want to refund?", "", "warning");
            $(this).html('Refunded');
            $(this).attr('disabled', true);
        })
    </script>
@endpush