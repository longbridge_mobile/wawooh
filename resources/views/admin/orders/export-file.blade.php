@php
// The function header by sending raw excel
//header("Content-type: application/vnd-ms-word");
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

// Defines the name of the export file "codelution-export.xls"
header("Content-Disposition: attachment; filename=order-list.xls");

// Add data table
@endphp
@include('admin.orders.order-export')