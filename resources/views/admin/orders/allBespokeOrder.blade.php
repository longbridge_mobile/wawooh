@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Orders')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">

            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-briefcase bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Order Management</h5>
                                    <span>The list of all orders</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Orders</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#all" aria-controls="all"
                                                           role="tab" data-toggle="tab">All</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#paymentMethod"
                                                           aria-controls="paymentMethod" role="tab" data-toggle="tab">Card
                                                            Payment: <label class="badge badge-primary"></label></a>
                                                    </li>
                                                    {{--<li role="presentation">
                                                        <a href="#confirmed" aria-controls="confirmed" role="tab" data-toggle="tab">Confirmed</a>
                                                    </li>--}}
                                                    <li class="nav-item" id="incompleted">
                                                        <a class="nav-link" href="#incomplete"
                                                           aria-controls="incomplete" role="tab" data-toggle="tab">Incomplete</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content mt-3">
                                                    <div role="tabpanel" class="tab-pane active" id="all">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-bordered table-hover"
                                                                   id="allOrderTable98">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Order No</th>
                                                                    <th scope="col">Total</th>

                                                                    <th scope="col">Payment Method</th>

                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Time</th>
                                                                    {{--<th scope="col">Status</th>--}}
                                                                    <th scope="col">Actions</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($orders))
                                                                    @if(count($orders))
                                                                        @foreach($orders as $order)
                                                                                <tr class='clickable-row'>
                                                                                    <td> <a href='{{route('admin.Bespokeorder.items', $order->orderNum)}}'>{{$order->orderNum}}</a></td>
                                                                                    <td>{{$order->price}}</td>
                                                                                    <td>
                                                                                        @if($order->paymentType  === 'CARD_PAYMENT')
                                                                                            <p>CARD PAYMENT</p>
                                                                                            @else
                                                                                        <p>BANK TRANSFER</p>
                                                                                        @endif

                                                                                    </td>
                                                                                    <td>{{$order->designer}}</td>
                                                                                    <td>{{$order->status}}</td>
                                                                                    <td>
                                                                                        @if($order->paymentType  === 'CARD_PAYMENT')
                                                                                            <p>Payment Confirmed</p>
                                                                                            @else
                                                                                            {{--
                                                                                                                                                                                        <button class="btn btn-wawooh bespokeConfirm">Confirm Payment</button>
                                                                                            --}}
                                                                                            <button class="btn btn-wawooh confirmP" data-toggle="modal" data-target="#confirmModal"  data-id="{{@$order->transactionRef}}">Confirm Payment</button>

                                                                                        @endif
                                                                                    </td>
                                                                                </tr>

                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                </div>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="{{route('admin.export.orders')}}"
                                                           class="pull-right btn btn-primary">Export Data</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="modal fade" data-backdrop="static" id="confirmModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                                    <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" class="art-close">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <div>
                                       Payment Confirm
                                       <select class="form-control valueConfirm">
                                           <option>Please Select</option>
                                           <option value=1>Yes</option>
                                           <option value=2>No</option>
                                       </select>
                                   </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                                    </button>
                                    <button type="button" class="btn btn-primary modalSaveBtn bespokeConfirm">Save
                                        changes
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        var num = '';

      $('.confirmP').click(function(){
         num = $(this).attr('data-id');
      });


       $('.bespokeConfirm').on('click', function(){
          var data = {
              transactionRef: num,
          };

          if($('.valueConfirm').val() === "1"){
              data.paymentConfirmed = true
          } else {
              data.paymentConfirmed = false;
          }
          console.log(data);
           $.ajax({
               url: '{{route('confirm.bespoke.order')}}',
               type: "POST",
               dataType: "json",
               data: JSON.stringify(data),
               success: function (response) {
                  console.log(response);
                  if(response.status === '00') {
                      swal(response.message, '', 'success');
                      location.reload();
                  }else{
                      swal(response.message, '', 'error');
                  }
               },
               error: function (e) {
                   console.log(e);
               }
           });


       });

    </script>

@endpush