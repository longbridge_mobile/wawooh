@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Orders')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-briefcase bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Order Management</h5>
                                    <span>The list of all Delivered Order</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Delivered Orders</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-bordered table-hover"
                                                           id="allOrderTable98">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Order No</th>
                                                            <th scope="col">Total</th>
                                                            <th scope="col">Payment Method</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Time</th>
                                                            <th scope="col">Actions</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($orders))
                                                            @if(count($orders))
                                                                @foreach($orders as $order)
                                                                    <a href='{{route('admin.order.items', $order->id)}}'>
                                                                        <tr>
                                                                            <td>{{$order->orderNum}}</td>
                                                                            <td>₦{{number_format($order->totalAmount)}}</td>
                                                                            <td style="text-transform: capitalize;">{{str_replace('_', ' ', strtolower($order->paymentType))}}</td>
                                                                            <td>{{\Carbon\Carbon::createFromTimestamp($order->orderDate / 1000)->format('d-M-Y')}}
                                                                                <br>
                                                                                <small class="text-success text-centerC">
                                                                                    ({{\Carbon\Carbon::createFromTimestamp($order->orderDate / 1000)->diffForHumans()}}
                                                                                    )
                                                                                </small>
                                                                            </td>
                                                                            <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($order->orderDate / 1000))->format('h:m:i A')}}</td>
                                                                            <td>
                                                                                <a href='{{route('admin.order.items', $order->id)}}'
                                                                                   style="color: grey;"
                                                                                   class=""><button class="btn btn-sm btn-info"><i class="fa fa-eye"></i></button>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </a>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection