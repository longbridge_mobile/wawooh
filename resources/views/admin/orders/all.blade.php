@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','All Orders')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">

            <div class="pcoded-content">

                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-briefcase bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Order Management</h5>
                                    <span>The list of all orders</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Orders</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <!-- Nav tabs -->
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" href="#all" aria-controls="all"
                                                           role="tab" data-toggle="tab">All</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" href="#paymentMethod"
                                                           aria-controls="paymentMethod" role="tab" data-toggle="tab">Card
                                                            Payment: <label class="badge badge-primary"></label></a>
                                                    </li>
                                                    {{--<li role="presentation">
                                                        <a href="#confirmed" aria-controls="confirmed" role="tab" data-toggle="tab">Confirmed</a>
                                                    </li>--}}
                                                    <li class="nav-item" id="incompleted">
                                                        <a class="nav-link" href="#incomplete"
                                                           aria-controls="incomplete" role="tab" data-toggle="tab">Incomplete</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content mt-3">
                                                    <div role="tabpanel" class="tab-pane active" id="all">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-bordered table-hover"
                                                                   id="allOrderTable98">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Order No</th>
                                                                    <th scope="col">Total</th>
                                                                    <th scope="col">Payment Method</th>
                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Time</th>
                                                                    {{--<th scope="col">Status</th>--}}
                                                                    <th scope="col">Actions</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($orders))
                                                                    @if(count($orders))
                                                                        @foreach($orders as $order)
                                                                            <a href='{{route('admin.order.items', $order->id)}}'>
                                                                                <tr class='clickable-row'>
                                                                                    <td>{{$order->orderNumber}}</td>
                                                                                    <td>₦{{number_format($order->totalAmount)}}</td>
                                                                                    <td style="text-transform: capitalize;">{{str_replace('_', ' ', strtolower($order->paymentType))}}</td>
                                                                                    <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}
                                                                                        <br>
                                                                                        <small class="text-success text-centerC">
                                                                                            ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                                            )
                                                                                        </small>
                                                                                    </td>
                                                                                    <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($order->orderDate))->format('h:m:i A')}}</td>
                                                                                    {{-- <td>
                                                                                         @if($order->deliveryStatus == 'PC')
                                                                                             <label class="label label-completed">Payment
                                                                                                 Confirmed</label>
                                                                                         @elseif($order->deliveryStatus == 'P')
                                                                                             <label class="label label-pending">Pending</label>
                                                                                         @elseif($order->deliveryStatus == 'OP')
                                                                                             <label class="label label-processing">Order
                                                                                                 Processing</label>
                                                                                         @elseif($order->deliveryStatus == 'OR')
                                                                                             <label class="label label-rejected">Order
                                                                                                 Rejected</label>
                                                                                         @elseif($order->deliveryStatus == 'CO')
                                                                                             <label class="label label-completed">Completed by
                                                                                                 designer</label>
                                                                                         @elseif($order->deliveryStatus == 'RI')
                                                                                             <label class="label label-inspection">Ready for
                                                                                                 Inspection</label>
                                                                                         @elseif($order->deliveryStatus == 'RS')
                                                                                             <label class="label label-processing">Ready for
                                                                                                 Shipping</label>
                                                                                         @elseif($order->deliveryStatus == 'OS')
                                                                                             <label class="label label-completed">Order
                                                                                                 Shipped</label>
                                                                                         @elseif($order->deliveryStatus == 'NV')
                                                                                             <label class="label label-inspection">Not
                                                                                                 Verified</label>
                                                                                         @elseif($order->deliveryStatus == 'OD')
                                                                                             <label class="label label-completed">Order
                                                                                                 Delivered</label>
                                                                                         @endif
                                                                                     </td>--}}
                                                                                    <td>
                                                                                        @if($order->paymentType !== 'CARD_PAYMENT')
                                                                                            @if($order->deliveryStatus == 'A')
                                                                                                {{--//p-pending--}}
                                                                                                {{--//co-completed--}}
                                                                                                {{--//ri-ready of inspection--}}
                                                                                                {{--//rs-ready of shipping--}}
                                                                                                {{--//os-order ship--}}

                                                                                            @elseif($order->deliveryStatus == 'P')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='PC'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                     <i class="fa fa-check-circle"></i>
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'CO')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='RI'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    <i class="fa fa-check-circle"></i>
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'RI')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='RS'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    <i class="fa fa-check-circle"></i>
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'RS')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='OS'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    <i class="fa fa-check-circle"></i>
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'OS')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='OD'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    <i class="fa fa-check-circle"></i>
                                                                                                </button>
                                                                                            @endif
                                                                                        @endif
                                                                                        @if($order->deliveryStatus == 'C')
                                                                                            <label class="label label-rejected">Cancelled</label>
                                                                                        @elseif($order->deliveryStatus !== 'OS' || $order->deliveryStatus !== 'OD')
                                                                                            <button class="btn btn-small btn-danger cancelOrder"
                                                                                                    customer='{{$order->userId}}'
                                                                                                    orderNumber='{{@$order->orderNumber}}'
                                                                                                    order='{{$order->id}}'
                                                                                                    stat='C'
                                                                                                    payment='{{@$order->totalAmount}}'>
                                                                                                <i class="fa fa-times-circle"></i>
                                                                                            </button>
                                                                                        @endif
                                                                                        <a href='{{route('admin.order.items', $order->id)}}'
                                                                                           style="color: grey;"
                                                                                           class=""><button class="btn btn-sm btn-info"><i class="fa fa-eye"></i></button>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </a>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="paymentMethod">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-bordered table-striped"
                                                                   id="incompleteTable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Order No</th>
                                                                    <th scope="col">Amount</th>
                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Time</th>
                                                                    {{--<th scope="col">Status</th>--}}
                                                                    <th scope="col">Action</th>
                                                                    <th scope="col"></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($orders))
                                                                    @if(count($orders))
                                                                        @foreach($orders as $order)
                                                                            @if($order->paymentType === 'CARD_PAYMENT')
                                                                                <a href='{{route('admin.order.items', $order->id)}}'>
                                                                                    <tr class='clickable-row'>
                                                                                        <td>{{$order->orderNumber}}</td>
                                                                                        <td>
                                                                                            ₦{{number_format($order->paidAmount)}}</td>
                                                                                        <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}
                                                                                            <br>
                                                                                            <small class="text-success text-centerC">
                                                                                                ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                                                )
                                                                                            </small>
                                                                                        </td>
                                                                                        <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($order->orderDate))->format('h:m:i A')}}</td>
                                                                                        {{--<td>--}}
                                                                                        {{--@if($order->deliveryStatus == 'PC')--}}
                                                                                        {{--<label class="label label-completed">Payment--}}
                                                                                        {{--Confirmed</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'P')--}}
                                                                                        {{--<label class="label label-pending">Pending</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'OP')--}}
                                                                                        {{--<label class="label label-processing">Order--}}
                                                                                        {{--Processing</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'OR')--}}
                                                                                        {{--<label class="label label-rejected">Order--}}
                                                                                        {{--Rejected</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'CO')--}}
                                                                                        {{--<label class="label label-completed">Completed by--}}
                                                                                        {{--designer</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'RI')--}}
                                                                                        {{--<label class="label label-inspection">Ready for--}}
                                                                                        {{--Inspection</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'RS')--}}
                                                                                        {{--<label class="label label-processing">Ready for--}}
                                                                                        {{--Shipping</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'OS')--}}
                                                                                        {{--<label class="label label-completed">Order--}}
                                                                                        {{--Shipped</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'NV')--}}
                                                                                        {{--<label class="label label-rejected">Not--}}
                                                                                        {{--Verified</label>--}}
                                                                                        {{--@elseif($order->deliveryStatus == 'OD')--}}
                                                                                        {{--<label class="label label-completed">Order--}}
                                                                                        {{--Delivered</label>--}}
                                                                                        {{--@endif--}}
                                                                                        {{--</td>--}}
                                                                                        <td>
                                                                                            @if($order->deliveryStatus == 'A')
                                                                                            @elseif($order->deliveryStatus == 'P')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='PC'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'CO')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='RI'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'RI')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='RS'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'RS')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='OS'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'OS')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='OD'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>

                                                                                            @endif
                                                                                            <a href='{{route('admin.order.items', $order->id)}}'
                                                                                               style="color: grey;"
                                                                                               class="">View Order
                                                                                            </a>

                                                                                        </td>
                                                                                        <td>
                                                                                            <div del="{{$order->id}}"
                                                                                                 id="delIncompleteOrder">
                                                                                                <i
                                                                                                        class="fa fa-trash brand-color"></i>
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </a>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div role="tabpanel" class="tab-pane" id="confirmed"></div>

                                                    <div role="tabpanel" class="tab-pane" id="incomplete">
                                                        <div class="dt-responsive table-responsive">
                                                            <table class="table table-bordered table-striped"
                                                                   id="incompleteTable">
                                                                <thead>
                                                                <tr>
                                                                    <th scope="col">Order</th>
                                                                    {{--  <th scope="col">Product</th>  --}}
                                                                    {{--  <th scope="col">Customer</th>  --}}
                                                                    <th scope="col">Name</th>
                                                                    <th scope="col">Phone</th>
                                                                    <th scope="col">Payment Method</th>
                                                                    <th scope="col">Amount</th>
                                                                    <th scope="col">Date</th>
                                                                    <th scope="col">Status</th>
                                                                    <th scope="col">Action</th>
                                                                    <th scope="col"></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                @if(isset($incompleteOrder) && is_array($incompleteOrder))
                                                                    @if(count($incompleteOrder))
                                                                        @foreach($incompleteOrder as $order)
                                                                            <a href='{{route('admin.order.items', $order->id)}}'>
                                                                                <tr class='clickable-row'>
                                                                                    <td>{{$order->orderNumber}}</td>
                                                                                    <td>{{$order->customerName}}</td>
                                                                                    <td>{{$order->deliveryPhoneNumber}}</td>
                                                                                    <td>{{str_replace('_', ' ', $order->paymentType)}}</td>
                                                                                    <td>
                                                                                        ₦{{number_format($order->paidAmount)}}</td>
                                                                                    <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}</td>
                                                                                    <td>
                                                                                        @if($order->deliveryStatus == 'PC')
                                                                                            <label class="label label-completed">Payment
                                                                                                Confirmed</label>
                                                                                        @elseif($order->deliveryStatus == 'P')
                                                                                            <label class="label label-pending">Pending</label>
                                                                                        @elseif($order->deliveryStatus == 'OP')
                                                                                            <label class="label label-processing">Order
                                                                                                Processing</label>
                                                                                        @elseif($order->deliveryStatus == 'OR')
                                                                                            <label class="label label-rejected">Order
                                                                                                Rejected</label>
                                                                                        @elseif($order->deliveryStatus == 'CO')
                                                                                            <label class="label label-completed">Completed
                                                                                                by
                                                                                                designer</label>
                                                                                        @elseif($order->deliveryStatus == 'RI')
                                                                                            <label class="label label-inspection">Ready
                                                                                                for
                                                                                                Inspection</label>
                                                                                        @elseif($order->deliveryStatus == 'RS')
                                                                                            <label class="label label-processing">Ready
                                                                                                for
                                                                                                Shipping</label>
                                                                                        @elseif($order->deliveryStatus == 'OS')
                                                                                            <label class="label label-completed">Order
                                                                                                Shipped</label>
                                                                                        @elseif($order->deliveryStatus == 'NV')
                                                                                            <label class="label label-rejected">Not
                                                                                                Verified</label>
                                                                                        @elseif($order->deliveryStatus == 'OD')
                                                                                            <label class="label label-completed">Order
                                                                                                Delivered</label>
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>
                                                                                        @if($order->paymentType === 'BANK_TRANSFER')
                                                                                            @if($order->deliveryStatus == 'A')
                                                                                            @elseif($order->deliveryStatus == 'P')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='PC'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'CO')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='RI'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'RI')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='RS'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'RS')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='OS'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @elseif($order->deliveryStatus == 'OS')
                                                                                                <button customer='{{$order->userId}}'
                                                                                                        orderNumber='{{@$order->orderNumber}}'
                                                                                                        order='{{$order->id}}'
                                                                                                        stat='OD'
                                                                                                        payment='{{@$order->totalAmount}}'
                                                                                                        class="btn btn-accept accept">
                                                                                                    Confirm
                                                                                                </button>
                                                                                            @endif
                                                                                        @endif
                                                                                        <a href='{{route('admin.order.items', $order->id)}}'
                                                                                           style="color: grey;"
                                                                                           class="">View Order
                                                                                        </a>

                                                                                    </td>
                                                                                    <td>
                                                                                        <div del="{{$order->id}}"
                                                                                             id="delIncompleteOrder">
                                                                                            <i
                                                                                                    class="fa fa-trash brand-color"></i>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </a>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="{{route('admin.export.orders')}}"
                                                           class="pull-right btn btn-primary">Export Data</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>

        $('.cancelOrder').click(function () {
            var data = {
                "id": $(this).attr('order'),
                "orderNumber": $(this).attr('orderNumber'),
                "deliveryStatus": $(this).attr('stat'),
                "userId": $(this).attr('customer'),
                "paidAmount": $(this).attr('payment')
            };
            console.log(data);
            swal({
                title: "Are you sure?",
                text: "This action is irreversible. Click yes to continue if you are sure",
                icon: "warning",
                buttons: ["No", "Yes"],
                dangerMode: true,
            })
                .then((confirm) => {
                    if (confirm) {
                        $('#loaderModal').modal();
                        $.ajax({
                            url: '{{route('admin.cancelOrder')}}',
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function (result) {
                                console.log(result);
                                $('div.loader').hide();
                                if (result.status == 0) {
                                    $('#loaderModal').modal('hide');
                                    swal('Order Confirmed Successfully', '', 'success');
                                    location.reload();
                                } else if (result.status == 56) {
                                    swal({
                                        title: "Cannot confirm Order",
                                        text: result.data,
                                        icon: "warning",
                                        buttons: ["No", "Yes"],
                                        dangerMode: true,
                                    })
                                    location.reload();
                                } else {
                                    $('#loaderModal').modal('hide');
                                    swal(result.message, '', 'error');
                                }
                            },
                            error: function (e) {
                                $('div.loader').hide();
                                $('#loaderModal').modal('hide');
                                swal(e, '', 'error');
                            }
                        });
                    }
                })
        });


        $(document).ready(function () {
            $('#delIncompleteOrder').on('click', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var id = $(this).attr('del');
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click yes to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                }).then(confirm => {
                    if (confirm) {
                        $('#loaderModal').modal();
                        $.ajax({
                            url: `{{env('GET_BASE_URL')}}/fashion/secure/admin/order/${id}/deleteorder`,
                            headers: {
                                'Authorization': tok,
                                'Content-Type': 'application/json; charset=utf-8',
                                'accept': 'application/json'
                            },
                            dataType: 'json',
                            success: function (result) {
                                console.log(result);
                                if (result.status == 0) {
                                    $('#loaderModal').modal('hide');
                                    $(this).closest('.clickable-row').hide();
                                    // console.log($(this).closest('.clickable-row').hide());
                                    swal('Successfully Deleted', '', 'success');
                                    location.reload();
                                    //$(this).closest('.clickable-row').hide();
                                    $('#incompleted').addClass('active');
                                }
                            }
                        });
                    }
                })
            });
            $('.accept').on('click', function () {
                var data = {
                    "id": $(this).attr('order'),
                    "orderNumber": $(this).attr('orderNumber'),
                    "deliveryStatus": $(this).attr('stat'),
                    "userId": $(this).attr('customer'),
                    "paidAmount": $(this).attr('payment')
                };
                console.log(data);
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click yes to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $('#loaderModal').modal();
                            $.ajax({
                                url: '{{route('admin.comfirmPayments')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (result) {
                                    console.log(result);
                                    $('div.loader').hide();
                                    if (result.status == 0) {
                                        $('#loaderModal').modal('hide');
                                        swal('Order Confirmed Successfully', '', 'success');
                                        location.reload();
                                    } else if (result.status == 56) {
                                        swal({
                                            title: "Cannot confirm Order",
                                            text: result.data,
                                            icon: "warning",
                                            buttons: ["No", "Yes"],
                                            dangerMode: true,
                                        })
                                        location.reload();
                                    } else {
                                        $('#loaderModal').modal('hide');
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                    swal(e, '', 'error');
                                }
                            });
                        }
                    })
            });
        });

    </script>

@endpush