@extends('layouts.admin.default')

@push('styles')

@endpush


@section('content')
<div class="content-wrapper"> 
    <section class="content-header">
        <h1>Orders</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-home"></i> Order</a></li>
            <li class="active"><i class="fa fa-dashboard"></i> Completed order</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="bg-white">
            <h3 class="sub-orders">Completed Orders</h3>
            <div class="all-order">
                            <div class="row">
                            <div class="col-md-3">
                                <div class="row">
                                <div class="col-md-7">
                                    <p>No of entries per page</p>
                                </div>
                                <div class="col-md-5">
                                    <button class="null-btn">10</button>
                                </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-md-offset-5">
                                <div class="input-group  learn29">
                                <input type="text" class="form-control" placeholder="Search" aria-label="Se" aria-describedby="learn9">
                                <span class="input-group-addon" id="learn9"><i class="fa fa-search" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            </div>

                            <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Order No</th>
                                    <th scope="col">Product</th>
                                    <th scope="col">Customer</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>
                                <tr class='clickable-row' data-href='order-details.html'>
                                    <td>234567890</td>
                                    <td>Adidas Originals Superstar Trainers</td>
                                    <td>Deji-Ola Ayomiposi</td>
                                    <td>N15,000</td>
                                    <td>12/12/2018</td>
                                    <td>In Transit</td>
                                </tr>                              
                                </tbody>
                            </table>
                            </div>

                            <p>Showing <span>1</span> to <span>10</span> of <span>2000</span> entries</p>
                            <div>
                            <nav aria-label="...">
                                <ul class="pagination order-pages">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item active">
                                    <span class="page-link">
                                    1
                                    <span class="sr-only">(current)</span>
                                    </span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                    </a>
                                </li>
                                </ul>
                            </nav>
                            </div>
                        </div>
            </div>
    </section>
</div>

@endsection

@push('scripts')


		
@endpush