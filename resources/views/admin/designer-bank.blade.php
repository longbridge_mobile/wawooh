@extends('layouts.admin.default')

@push('styles')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <style>
        .approved {
            cursor: pointer;
        }

        .toggle {
            padding: 0 !important;
        }

        .toggle.btn {
            min-width: 109px;
        }

        .toggle-handle {
            display: none !important;
        }
        .hide {
            display: none;
        }
    </style>
@endpush

@section('pageTitle','Designer Style Catalogue')
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-picture bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Designers' Style Catalogue</h5>
                                    <span>All styles that doesn't meet product requirement</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Style Catalogue</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped category-table reduced-font" id="refundTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Store Name</th>
                                                            <th scope="col">Style Details</th>
                                                            <th scope="col">EST & Days</th>
                                                            <th scope="col">Picture</th>
                                                            <th scope="col">Date Uploaded</th>
                                                            <th scope="col">Action</th>
                                                            <th scope="col">Make Patented</th>
                                                            <th scope="col">Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($resp))
                                                            @if(count($resp))
                                                                @foreach($resp as $resp2)
                                                                    <tr>
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$resp2->designerName}}</td>
                                                                        <td>
                                                                            <strong>Style SKU</strong> <br>
                                                                            {{$resp2->sku}} <br>

                                                                            <strong>Sub Category</strong> <br>
                                                                            {{$resp2->subCategory}} <br>

                                                                            <strong>Keywords</strong> <br>
                                                                            @foreach($resp2->keyWords as $keyWord)
                                                                                <div>{{$keyWord}}</div>
                                                                            @endforeach
                                                                        </td>
                                                                        <td>
                                                                            <strong>EST</strong> <br>
                                                                            ₦{{number_format($resp2->estimatedPrice)}} <br>

                                                                            <strong>Production Days</strong> <br>
                                                                            {{$resp2->estimatedDeliveryTime}} <br>
                                                                        </td>
                                                                        <td>
                                                                            <a data-fancybox href="{{str_replace('http','https',$resp2->picture)}}">
                                                                                <img class="img-ho" width="70px"
                                                                                     src="{{str_replace(['http','upload'],['https','upload/q_40'],$resp2->picture)}}">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            {{\Carbon\Carbon::parse($resp2->dateCreated)->format('Y-m-d')}} <br>
                                                                            <small class="text-success">({{\Carbon\Carbon::parse($resp2->dateCreated)->diffForHumans()}})</small>
                                                                        </td>
                                                                        <td>
                                                                            <button class="approved btn btn-shop hide" bespokeStyle="{{$resp2->id}}"
                                                                                    verified="{{$resp2->verified}}">Disapprove
                                                                            </button>

                                                                            <input type="checkbox" class="toggleStatus" checked
                                                                                   data-toggle="toggle">
                                                                        </td>
                                                                        <td>
                                                                            <div class="pretty p-icon p-round">
                                                                                <input id="confirm" type="checkbox"
                                                                                       {{$resp2->verified ? '' : 'disabled'}} class="patented"
                                                                                       bespokeStyle="{{$resp2->id}}"
                                                                                       patented="{{$resp2->patented}}">
                                                                                <div class="state p-warning-o">
                                                                                    <i class="icon fa fa-check"></i>
                                                                                    <label for="confirm" style="font-size: inherit !important;"></label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="veri">
                                                                                @if($resp2->verified)
                                                                                    <label class="label label-completed">Approved</label>
                                                                                @else
                                                                                    <label class="label label-rejected">Declined</label>
                                                                                @endif
                                                                            </div>

                                                                            {{--<label class="label hide label-completed">Approved</label>
                                                                            <label class="label hide label-rejected">Declined</label>--}}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>{{$error}}</p>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.toggle-on').html('Approve');
            $('.toggle-off').html('Disapprove');
        })

        $('#refundTable tbody tr .approved').each(function () {
            if ($(this).attr('verified')) {
                $(this).siblings('.toggleStatus').bootstrapToggle('on');
                $(this).siblings('.toggleStatus').bootstrapToggle({
                    on: 'Approve',
                    off: 'Disapprove'
                });
            } else {
                $(this).siblings('.toggleStatus').bootstrapToggle('off');
                $(this).siblings('.toggleStatus').bootstrapToggle({
                    on: 'Approve',
                    off: 'Disapprove'
                });
            }
        });

        $('.toggleStatus').bootstrapToggle({
            on: 'Approve',
            off: 'Disapprove'
        });

        $('.toggle').click(function () {

            var data = {
                bespokeStyleId: $(this).siblings('.approved').attr('bespokeStyle'),
                verified: $(this).siblings('.approved').attr('verified')

            }

            console.log(data);
            $.ajax({
                url: "{{route('verifyDesignerImage')}}",
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    if (result.status === '00') {
                        swal(result.message, '', 'success');
                        window.location.reload();
                    } else {
                        swal(result.message, '', 'warning');
                        window.location.reload();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        });

        $('.patented').change(function () {
            var data = {
                bespokeStyleId: $(this).attr('bespokeStyle'),
                patented: $(this).attr('patented')
            }
            $.ajax({
                url: "{{route('verifyDesignerPatent')}}",
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (result) {
                    console.log(result);
                    if (result.status === '00') {
                        swal(result.message, '', 'success');
                        window.location.reload();
                    } else {
                        swal(result.message, '', 'warning');
                        window.location.reload();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        });
    </script>
@endpush