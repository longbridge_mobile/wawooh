@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Create Event')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-calendar bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Create Event</h5>
                                    <span>Create event here</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Create Event</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="eventName">Event Name*</label>
                                                            <input type="text" class="form-control" id="eventName" placeholder="Enter name of event">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="eventLocation">Event Location*</label>
                                                            <input type="text" class="form-control" id="location" placeholder="Enter Location of event">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label for="eventDes">Event Description</label>
                                                            <textarea class="form-control" id="description"
                                                                      rows = "5" cols="20" placeholder="Enter Name of Events"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="date">Event Date*</label>
                                                            <input type='date' id='date' class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Upload Event Images*</label>
                                                            <input type='file' multiple id='uploadPicture' class="form-custom">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label>Sponsored</label>
                                                        <input id='status' type='checkbox' value='S'>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label for="eventVideo">Event Video Link</label>
                                                        <input type="text" id="eventVideo" class="form-control" placeholder="Paste the link of the event video">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div id='pictures'></div>
                                                </div>
                                                <div id='loader2' style='height:50px;display:none' class='col-md-12 text-center'>
                                                    <img style='width:auto;height:100%' class='center-block' src='{{asset('img/loader.gif')}}'/>
                                                </div>

                                                <div class="uploadButton">
                                                    <button class="btn btn-wawooh continue">Save and continue</button>
                                                    <button class="btn btn-update"> Cancel</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/utility.js') }}"></script>
    <script src="{{ asset('js/admin/events.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.richtext.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#description').richText();
            counter = 0;
            $('#uploadPicture').on('change', async function () {
                var pix = [];
                $('#loader').removeClass('hide');
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    pix.push(await toBase64($(this).get(0).files[i]));
                }
                console.log(pix);
                displayUploadedImages(pix, '#pictures', counter);
            });

            //add more images
            $('.addMoreBtn').on('click', function () {
                $('.addMoreBox').toggle();
                $('.uploadBox').toggle();
            });

            //delete an image
            $(document).on('click', '.delete', function () {
                var target = $(this).attr('data-target');
                $(target).remove();
            });

            $('.continue').on('click', function () {
                //validate fields,
                //get all images,
                //create json object
                $('#loader2').show();
                images = [];
                $('.pix').each(function (i, obj) {
                    images.push({
                        "picture": getImageSrc($(this)),
                        "pictureDesc": $(this).parent('div.uploadRes').siblings('input').val()
                    })
                });
                var data = {
                    "eventName": $('#eventName').val(),
                    "location": $('#location').val(),
                    "description": $('#description').val(),
                    "eventDate": $('#date').val(),
                    "mainPicture": getImageSrc('.pix:first'),
                    "eventPictures": images,
                    "eventType": ($('#status:checked').val()) ? $('#status').val() : 'E'
                }
                console.log(data);
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    beforeSend: function () {

                    },
                    url: `{{route('admin.events.create.submit')}}`,
                    success: function (result) {
                        console.log(result);
                        $('#loader2').hide();
                        if (result.status == 0) {
                            swal('Event Added Successfully','','success');
                            window.location.href = "{{route('admin.events')}}";
                        } else {
                            swal(result.message, '', 'error');
                        }
                        //console.log(result);
                    },
                    error: function (e) {
                        $('#loader2').hide();
                        swal(e, '', 'error');
                        //console.log(e)
                    }

                });

            })
        });
    </script>
@endpush