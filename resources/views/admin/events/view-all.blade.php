@extends('layouts.admin.default')

@push('styles')

@endpush



@section('content')
<div class="content-wrapper"> 
    <section class="content-header">
        <h1>Events</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-home"></i> Events</a></li>
            <li class="active"><i class="fa fa-dashboard"></i> View Events</li>
        </ol>
    </section>
    <section class="content container-fluid">
        <div class="bg-white">
            {{--  <div id='events'>
                @if(count($events))
                   
                    @foreach($events as $event)
						<div class="container learn19">
								<h3>{{$event->eventName}}</h3>
								<hr>
				
								<div id="{{str_slug($event->eventName)}}{{$event->id}}" class="carousel slide" data-ride="carousel">
										<div class="carousel-inner">
											
												@if($event->eventPictures != null)
													@if(count($event->eventPictures))
														@foreach(array_chunk($event->eventPictures, 3) as $pictures)
														
															<div class="carousel-item @if($loop->iteration == 1) active @endif">
																<div class="row learn20">
																	@foreach($pictures as $picture)                                                                    
																		<div class="col-md-4 col-xs-12">
																			<div><img class='img-fluid' src='{{getBaseUrl().$picture->picture}}'></div>
																		</div>
																	@endforeach
																	
																</div>
															</div>
														@endforeach
													@endif
												@endif
										</div>
										
										<a class="carousel-control-prev hidden-xs" href="#{{str_slug($event->eventName)}}{{$event->id}}" role="button" data-slide="prev">
											<span class="fa fa-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next hidden-xs" href="#{{str_slug($event->eventName)}}{{$event->id}}" role="button" data-slide="next">
											<span class="fa fa-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
								</div>
								
								<div class="container learn21">
									<p><a href="/events/{{str_slug($event->eventName)}}?event={{$event->id}}" class="view">View all</a></p>
								</div>
						</div>	
                    @endforeach

                @endif
			
			
			</div>  --}}
        </div>
    </section>
</div>

@endsection

@push('scripts')
<script src="{{ asset('js/admin/events.js') }}"></script>

		<script>
            $(document).ready(function(){
                 counter = 0;
                $('#uploadPicture').on('change', async function(){
                    var pix = [];
                    $('#loader').removeClass('hide');
                        for (var i = 0; i < $(this).get(0).files.length; ++i) {                            
                            pix.push(await toBase64($(this).get(0).files[i]));
                         }
                        console.log(pix);                  
                        displayUploadedImages(pix,'#pictures', counter);
                });

                //add more images
                $('.addMoreBtn').on('click', function(){
                    $('.addMoreBox').toggle();
                    $('.uploadBox').toggle();
                });

                //delete an image
                $(document).on('click','.delete', function(){
                    var target = $(this).attr('data-target');
                    $(target).remove();
                });

                $('.continue').on('click', function(){
                    //validate fields,
                    //get all images,
                    //create json object

                    images = [];
                    $('.pix').each(function(i,obj){
                        images.push({
                            "picture": getImageSrc($(this)),
                            "pictureDesc": ""
                        })
                    });
                     var data = {
                         "eventName": $('#eventName').val(),
                         "location": $('#location').val(),
                         "description": $('#description').val(),
                         "eventDate": $('#date').val(),
                         "mainPicture": getImageSrc('.pix:first'),
                         "eventPictures": images
                     }
                     console.log(data);
                     $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        beforeSend: function() {
                
                        },
                
                        url: `{{route('admin.events.create')}}`,
                        success: function(result) {
                            console.log(result);
                        },
                        error:function(e){
                            console.log(e)
                        }
                
                     });
                    
                })
            });

           

            

        </script>
@endpush