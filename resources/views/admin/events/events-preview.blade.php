@extends('layouts.admin.default')

@push('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <style>
        .button-event-text {
            position: relative;
            top: -75px;
            z-index: 9999;
        }
    </style>
@endpush

@section('pageTitle', @$eventPictures->eventName . ' | Preview')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Event Pictures</h1>
            <ol class="breadcrumb">
                <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{{route('admin.events')}}"><i class="fa fa-home"></i> Events</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> Event Images</li>
            </ol>
        </section>
        <aside class='container-fluid'>
            <div class='bg-white'>
                <h3>Event Information</h3>
                <hr/>
                <div class='row'>
                    <div class='col-md-12'>
                        @if(isset($eventPictures))
                            <table class='table'>
                                <tr>
                                    <td style="width: 150px;">Event Name:</td>
                                    <td>
                                        <span class='eventDetails'>  {{$eventPictures->eventName}}</span>
                                        <input id='eventName' type="text"
                                               value='{{$eventPictures->eventName}}'
                                               class='form-control hide eventDetails'/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Location:</td>
                                    <td>

                                        <span class='eventDetails'>{{$eventPictures->location}}</span>

                                        <input id='location' type="text" value='{{$eventPictures->location}}'
                                               class='form-control hide eventDetails'/>

                                    </td>

                                </tr>
                                <tr>
                                    <td>Date of Event:</td>
                                    <td>
                                        <span class='eventDetails'>{{Carbon\Carbon::parse($eventPictures->eventDate)->format('Y-m-d')}}</span>

                                        <input id='date' type="text" value='{{$eventPictures->eventDate}}'
                                               class='form-control hide eventDetails'/>

                                    </td>

                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>
                                        <span class='eventDetails'
                                              style="line-height: 25px; height: 250px;">{!! nl2br($eventPictures->description) !!}</span>
                                        <textarea id='description' rows="10" cols="6"
                                                  class='form-control hide eventDetails'>{!! nl2br($eventPictures->description) !!}</textarea>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Sponsored:</td>
                                    <td>
                                        <span class='eventDetails'>@if($eventPictures->eventType == 'S') Yes @else
                                                No @endif</span>
                                        <input id='status' @if($eventPictures->eventType == 'S') checked
                                               @endif type='checkbox'
                                               value='S' class='hide eventDetails'/>
                                    </td>
                                </tr>
                            </table>
                        @endif
                    </div>
                    <div class='col-md-6 text-right'>
                        <button class='btn btn-wawooh editInfo'>Edit</button>
                        <span style='display:none' id='x'>
                    <button class='btn btn-wawooh cancel'>Cancel</button>
                    <button class='btn btn-wawooh saveInfo'>Save</button>
                </span>
                    </div>
                </div>
            </div>

            <div id='loader' style='height:50px;display:none' class='col-md-12 text-center'>
                <img style='width:auto;height:100%' class='center-block' src='{{asset('img/loader.gif')}}'/>
            </div>

        </aside>
        <hr>
        <section class="content container-fluid">
            <div class="bg-white tag-list">
                <div class="form-group">
                    <button class='btn selectAll'>Select All</button>
                    <button class='btn deleteImage'>Delete</button>
                </div>

                <div id="js-filters-masonry" class="cbp-l-filters-alignRight">
                    <div data-filter=".all" class="cbp-filter-item-active cbp-filter-item allClicked">
                        All
                        <div class="cbp-filter-counter"></div>
                    </div>

                    <div data-filter=".tagged" class="cbp-filter-item taggedClicked">
                        Tagged
                        <div class="cbp-filter-counter"></div>
                    </div>
                    <div data-filter=".untagged" class="cbp-filter-item untaggedClicked">
                        Untagged
                        <div class="cbp-filter-counter"></div>
                    </div>

                </div>


                <div id="js-grid-masonry" class="cbp">
                    @if(isset($eventPictures))
                        @if(count($eventPictures->eventPictures))
                            @foreach($eventPictures->eventPictures as $eventPicture)
                                <div class="cbp-item all">
                                    <a href="{{$eventPicture->picture}}" class="cbp-caption cbp-lightbo">
                                        <div class="cbp-caption-defaultWrap">
                                            <img class='p' id='{{$eventPicture->id}}' src="{{$eventPicture->picture}}"
                                                 alt="">
                                        </div>
                                        <input data-target='{{$eventPicture->id}}' name='check'
                                               class='form-contro button-event-text'
                                               type="checkbox"/>
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class='text-center'>No Pictures Here</div>
                        @endif
                    @endif


                    @if(isset($unTaggedPictures))
                        @if(count($unTaggedPictures))
                            @foreach($unTaggedPictures as $unTaggedPicture)
                                <div class="cbp-item all">
                                    <a href="{{$eventPicture->picture}}" class="cbp-caption cbp-lightbo">
                                        <div class="cbp-caption-activeWrap">

                                            <img class='p' id='{{$eventPicture->id}}' src="{{$eventPicture->picture}}"
                                                 alt="">
                                            <div>
                                                <input data-target='{{$eventPicture->id}}' name='check'
                                                       class='form-contro button-event-text' type="checkbox"/>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="cbp-item untagged">
                                    <a href="{{route('admin.tags.images',$unTaggedPicture->id)}}"
                                       class="cbp-caption cbp-lightbo">
                                        <div class="cbp-caption-defaultWrap">
                                            <img id='{{$unTaggedPicture->id}}' src="{{$unTaggedPicture->picture}}"
                                                 alt="">
                                        </div>

                                    </a>
                                </div>

                            @endforeach
                        @else
                            <div class='text-center'>No Pictures Here</div>
                        @endif

                    @endif


                    @if(isset($TaggedPictures))
                        @if(count($TaggedPictures))
                            @foreach($TaggedPictures as $TaggedPicture)
                                <div class="cbp-item all">
                                    <a href="{{$eventPicture->picture}}" class="cbp-caption cbp-lightbo">
                                        <div class="cbp-caption-activeWrap">

                                            <img class='p' id='{{$eventPicture->id}}' src="{{$eventPicture->picture}}"
                                                 alt="">
                                            <div>
                                                <input data-target='{{$eventPicture->id}}' name='check'
                                                       class='form-contro' type="checkbox"/>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="cbp-item tagged cbp-item-off">
                                    <a href="{{route('admin.tags.edit',$TaggedPicture->id)}}"
                                       class="cbp-caption cbp-lightbo">
                                        <div class="cbp-caption-defaultWrap">
                                            <img id='{{$TaggedPicture->id}}' src="{{$TaggedPicture->picture}}" alt="">
                                        </div>

                                    </a>
                                </div>

                            @endforeach
                        @else
                            <div class='text-center'>No Pictures Here</div>
                        @endif

                    @endif


                </div>


                <div id='more' class='row'>

                </div>
                <div class=''>
                    <label for='uploadPicture' class="btn btn-wawooh">Add More Images</label>
                    <input type='file' multiple id='uploadPicture' class='hide-input'/>
                </div>
                <div class="" style="text-align: right; margin-top: 30px;">
                    <button type='button' productId='{{@$eventPictures->id}}' class='saveImages btn btn-wawooh'>
                        Upload
                    </button>
                </div>

                {{--  <div class=>
              <button type='button' class='saveImages btn btn-wawooh'>
                Upload
              </button>
            </div>  --}}
            </div>
        </section>

        {{--<img class='p' id='{{$eventPicture->id}}' src="{{$eventPicture->picture}}" alt="">
        <div>
            <input data-target='{{$eventPicture->id}}' name='check' class='form-contro' type="checkbox"/>

        </div>--}}
    </div>
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script src="{{ asset('js/utility.js') }}"></script>
    <script src="{{ asset('js/admin/events.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.richtext.js') }}"></script>
    <script>
        window.onload = function () {
            $('.untagged').addClass('cbp-item-off');
        };
        $(document).ready(function () {
            $('#date').flatpickr();

            $('#uploadPicture').on('change', async function () {
                var pix = [];
                counter = 0;
                $('#loader').removeClass('hide');
                for (var i = 0; i < $(this).get(0).files.length; ++i) {
                    pix.push(await toBase64($(this).get(0).files[i]));
                }
                //console.log(pix);
                displayUploadedImages(pix, '#more', counter);
            });

            //delete an image
            $(document).on('click', '.delete', function () {
                var target = $(this).attr('data-target');
                $(target).remove();
            });
            $(document).on('click', '.deleteImage', function () {
                var answer = confirm('Are you sure you want to continue. Click ok to continue');
                if (answer) {
                    var checked = $('input[name=check]:checked');
                    var arr = [];
                    $(checked).each(function () {
                        arr.push($(this).attr('data-target'));
                    });
                    var data = {
                        //"id" : "{{request('event')}}",
                        "ids": arr,
                    }

                    //console.log(data);

                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        url: '{{route('admin.events.deletePictures')}}',
                        success: function (result) {
                            //console.log(result);
                            $('#loader').hide();
                            if (result.status == 0) {
                                location.reload();
                            } else {
                                $.notify(result.message, 'error');
                            }
                        },
                        error: function (e) {
                            $('#loader').hide();
                            console.log(e)
                        },
                    });
                }

                //console.log(data);

            });
            $('.editInfo').on('click', function () {
                $('.eventDetails').hide();
                $('input.eventDetails,textarea.eventDetails').show().removeClass('hide');
                $('#x').show();
                $(this).hide();
                $('#description').richText();
            });

            $('.cancel').on('click', function () {
                $('#x').hide();
                $('.editInfo').show();
                $('.eventDetails').show();
                $('input.eventDetails,textarea.eventDetails').hide().addClass('hide');

            });

            $('.selectAll').on('click', function () {

                if ($('input[name=check]:checked').length == $('input[name=check]').length) {
                    $('input[name=check]').prop('checked', false);
                    $(this).text('Select All');
                    $('.p').css({
                            filter: 'none'
                        }
                    );
                } else {
                    $(this).text('Deselect');
                    $('input[name=check]').prop('checked', true);
                    $('.p').css({
                            filter: 'grayscale(100%)'
                        }
                    );

                }
            });

            $('[name=check]').on('change', function () {
                if ($(this).is(":checked")) {
                    {{--  var target = $(this).attr('data-target');
                    console.log(target);  --}}
                    $(this).siblings('div').find('img').css({
                            filter: 'grayscale(100%)'
                        }
                    );
                } else {
                    var target = $(this).attr('data-target');
                    $(this).siblings('div').find('img').css({
                            filter: 'none'
                        }
                    );
                }
            });


            //save event info

            $('.saveInfo').click(function () {
                var data = {
                    "id": "{{$eventPictures->id}}",
                    "eventName": $('#eventName').val(),
                    "location": $('#location').val(),
                    "description": $('#description').val(),
                    "eventDate": $('#date').val(),
                    "eventType": ($('#status:checked').val()) ? $('#status').val() : 'E'
                }
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{route('admin.events.updateInfo')}}',
                    success: function (result) {
                        console.log(result);
                        console.log(data);
                        //  $('#loader').hide();
                        if (result.status === "00") {
                            swal('Event Updated Successfully','','success');
                            location.reload();
                        } else {
                            swal(result.message, '', 'error');
                            $('#loaderModal').modal('hide');
                        }
                    },
                    error: function (e) {
                        $('#loader').hide();
                        swal(e, '', 'error');
                        $('#loaderModal').modal('hide');
                    },
                });
            });
            //upload event pictures

            $('.saveImages').click(function () {
                var images = [];
                $('.pix').each(function (i, obj) {
                    images.push({
                        "picture": getImageSrc($(this)),
                        "pictureDesc": $(this).siblings('input.pictureName').val()
                    })
                });
                var data = {
                    "id": $(this).attr('productid'),
                    "eventPictures": images,
                }

                console.log(data);

                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{route('admin.events.updateEventPicture')}}',
                    success: function (result) {
                        console.log(result);
                        $('#loader').hide();
                        if (result.status == 0) {
                            location.reload();
                        } else {
                            $.notify(result.message, 'error');
                        }
                    },
                    error: function (e) {
                        $('#loader').hide();
                        console.log(e)
                    },
                });
            });
        });
    </script>
@endpush