@extends('layouts.admin.default')

@push('styles')

@endpush

@section('pageTitle','Promo Code Management')
@section('content')

    <div class="modal fade" id="promoCodeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control promoChecker" id="promoEmail">
                    </div>

                    <div>
                        <input type="password" class="form-control promoChecker" id="promoPassword">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary authenticatePromoCode">Authenticate Promo Code</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="promoCodeActivateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Activate Promo Code</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>Promo Code:</b> <span class="promocodeAmodal"></span></p>
                    <p><b>Value:</b> ₦<span class="promocodeAmodalValue"></span></p>
                    <p><b>Status:</b> <span class="promocodeAmodalStatus"></span></p>
                    <p><b>Verified:</b> <span class="promocodeAmodalPromo"></span></p>
                    <p><b>Value Type:</b> <span class="promocodeAmodalValueType"></span></p>
                    <p><b>Email:</b> <span class="promocodeEmail"></span></p>
                    <p><b>Promo Active Limit:</b> <span class="promoActiveLimit"></span></p>
                    <p><b>Expiry Date:</b> <span class="promocodeExpiryDate"></span></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary checkedTimes">Authenticate Promo Code</button>
                </div>
            </div>
        </div>
    </div>

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                {{--<i class="feather icon-home bg-c-blue"></i>--}}
                                <div class="d-inline">
                                    <h5>Promo Code Management</h5>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Promo Code</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12 promoCode">
                                        <div class="card">
                                            <div class="card-block">
                                                <h3 class="sub-orders">Add Promo Codes </h3>
                                                <hr>
                                                <div class="all-order">
                                                    <div class="col-md-11 offset-1 pl-0">
                                                        <form class="">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="code">Enter Code</label>
                                                                        <div class="input-group">
                                                                            <input type="text"
                                                                                   placeholder="Enter the promo code"
                                                                                   class="form-control reqCode"
                                                                                   id="code">
                                                                            <span class="input-group-addon">
                                                                                <button class="btn btn-warning generate-promo" style="padding: 10px 19px; !important;">GENERATE</button>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="valueType">Select Value Type</label>
                                                                        <select name="vt" id="valueType"
                                                                                class="form-control reqCode">
                                                                            <option value="0" hidden>--Select--</option>
                                                                            <option value="vd">Value Discount</option>
                                                                            <option value="fs">Free Shipping</option>
                                                                            <option value="pd">Percentage Discount
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5 intVal">
                                                                    <div class="form-group">
                                                                        <label for="value">Enter Value</label>
                                                                        <input type="number" id="value"
                                                                               class="form-control"
                                                                               placeholder="Enter value">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="numberOfUsage">Number of
                                                                            Usage</label>
                                                                        <select id="numberOfUsage" class="form-control reqCode">
                                                                            <option value="0" hidden>--Select--</option>
                                                                            <option value="1">Once</option>
                                                                            <option value="-1">Multiple</option>
                                                                            <option value="custom">Custom</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group hide customDigit">
                                                                        <label for="numberOfUsage">Number of
                                                                            Usage</label>
                                                                        <input type="number" id="noValue"
                                                                               class="form-control reqCode"
                                                                               placeholder="Enter value">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="expiryDate">Select Expiry Date and
                                                                            Time</label>
                                                                        <input type="text" id="expiryDate"
                                                                               placeholder="Select Date and Time"
                                                                               class="form-control promo-calender reqCode">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label for="applyLimit">Minimum purchase
                                                                            amount</label>
                                                                        <input type="number" id="applyLimit"
                                                                               class="form-control reqCode"
                                                                               placeholder="Minimum purchase amount">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {{-- <div class="form-group">
                                                                 <label>Apply to all</label>
                                                                 <input type="checkbox" id="applyAll">
                                                             </div>

                                                             <div class="catDiv">

                                                             <div class="form-group">
                                                                 <label for="selCat">Select Categories</label>
                                                                 <select name="" id="selCat" class="form-control">
                                                                     <option value="0" hidden>--Select--</option>
                                                                     @if(isset($orders))
                                                                         @if(count($orders))
                                                                             @foreach($orders as $order)
                                                                                 --}}{{-- @if($category->id == request('cat'))--}}{{--
                                                                                 <option value='{{$order->id}}'>{{$order->categoryName}}</option>
                                                                                 --}}{{-- @endif--}}{{--
                                                                             @endforeach
                                                                         @endif
                                                                     @endif

                                                                 </select>
                                                             </div>


                                                             <div class="form-group">
                                                                 <label for="selCat">Select SubCategories</label>
                                                                 <select name="" id="selSubCat" class="form-control">

                                                                 </select>
                                                             </div>

                                                             <div class="form-group">
                                                                 <label for="selProd">Select Product</label>
                                                                 <select name="" id="selProd" multiple class="form-control">
                                                                     <option value="0" hidden>--Select--</option>
                                                                 </select>
                                                             </div>
                                                             </div>--}}
                                                            <div class="row">
                                                                <div class="col-md-3 offset-4" style="position: relative;">
                                                                    <div class="form-group">
                                                                        {{-- id="addPromoCode"   data-toggle="modal" data-target="#promoCodeModal"--}}
                                                                        <input type="button" value="Add Code"
                                                                               class="form-control btn btn-warning"
                                                                               id="addPromoCode">
                                                                        <img src="{{asset('img/AjaxLoader.gif')}}"
                                                                             alt="" id="preLoad" class="proLoad">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="verified-table"
                                                           class="table table-striped table-bordered nowrap reduced-font">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Code</th>
                                                            <th scope="col">Value</th>
                                                            <th scope="col">Value Type</th>
                                                            <th scope="col">Promo <br>Active <br>Limit</th>
                                                            <th scope="col">Number <br>Of Usage</th>
                                                            <th scope="col">Expire Date</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col">Used <br> Status</th>
                                                            <th scope="col">Created By</th>
                                                            <th scope="col">Verified By</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($allPromoCode))
                                                            @if(count($allPromoCode))
                                                                @foreach($allPromoCode as $promo)
                                                                    <tr class="tableDiv" id="{{@$promo->id}}"
                                                                        promoCode="{{$promo->code}}">
                                                                        <td>{{$loop->iteration}}</td>
                                                                        <td>{{$promo->code}}</td>
                                                                        <td>{{$promo->value}}</td>
                                                                        <td>{{$promo->promoActiveLimit}}</td>
                                                                        <td>{{$promo->valueType}}</td>
                                                                        <td>{{$promo->numberOfUsage}}</td>
                                                                        <td>{{$promo->expiryDate}}</td>
                                                                        <td><label class="label label-{{$promo->verifiedFlag === 'N' ? 'rejected':'completed'}}">{{$promo->verifiedFlag === 'N' ? 'Not Verified':'Verified'}}</label></td>
                                                                        <td><label class="label label-{{$promo->isUsedStatus === 'N' ? 'rejected':'completed'}}">{{$promo->isUsedStatus === 'N' ? 'Not Used':'Already Used'}}</label></td>
                                                                        <td>{{$promo->createdBy->email}}</td>
                                                                        @if(@$promo->verifiedFlag === 'N')
                                                                            <td><button class="btn btn-accept getPromoCodeById" id="{{@$promo->id}}"
                                                                                        data-toggle="modal"
                                                                                        data-target="#promoCodeActivateModal">
                                                                                    <i class="fa fa-check-circle"></i>
                                                                                </button>
                                                                            </td>
                                                                        @else
                                                                            <td><i class="fa fa-check getPromoCodeById"
                                                                                   id="{{@$promo->id}}"
                                                                                   data-toggle="modal"
                                                                                   data-target="#promoCodeActivateModal"></i>
                                                                                By: {{@$promo->verifiedBy->firstName}} {{@$promo->verifiedBy->lastName}}
                                                                            </td>
                                                                        @endif
                                                                        {{--//checkedTimes--}}
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            <p>No transaction at the moment</p>
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>

        $('.getPromoCodeById').click(function () {
            let id = $(this).attr('id');
            var data = {
                id: id
            }
            $.ajax({
                url: `{{route('getPromoCodeById')}}`,
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(data),
                type: "POST",
                success: function (response) {
                    // console.log(response);
                    $(".promocodeAmodal").html(response.data.code);
                    $('.promocodeAmodalValue').html(response.data.value);
                    $('.promocodeAmodalStatus').html(response.data.isUsedStatus === 'N' ? 'Not been Used' : 'Used');
                    $('.promocodeAmodalPromo').html(response.data.verifiedFlag === 'N' ? 'Not Verified' : 'Verified');
                    if (response.data.verifiedFlag === 'vd') {
                        $('.promocodeAmodalValueType').html('Value Discount');
                    } else if (response.data.verifiedFlag === 'fs') {
                        $('.promocodeAmodalValueType').html('Free Shipping');
                    } else {
                        $('.promocodeAmodalValueType').html('Percentage Discount');
                    }
                    $('.promocodeEmail').html(response.data.createdBy.email);
                    $('.promocodeExpiryDate').html(response.data.expiryDate);
                    $('.promoActiveLimit').html(response.data.promoActiveLimit);
                },
                error: function (error) {
                    console.log(error);
                }
            })

        })

        $('.checkedTimes').click(function () {
            console.log('tttsssaddd');
            var adminId = JSON.parse(window.localStorage.getItem('adminId')) ? JSON.parse(window.localStorage.getItem('adminId')) : '';
            var promoCode = $('.promocodeAmodal').html();

            var data = {
                code: promoCode,
                verifierId: adminId
            };
            console.log(data);
            $.ajax({
                url: `{{route('verifyPromoCode')}}`,
                type: "POST",
                data: JSON.stringify(data),
                dataType: 'json',
                success: function (response) {
                    if (response.status === '00') {
                        swal(response.message, '', 'info');
                        window.location.reload();
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        $('#applyAll').click(function () {
            if ($(this).is(':checked')) {
                $('.catDiv').addClass("hide");
            } else {
                $('.catDiv').removeClass("hide");
            }
        });

        $('#numberOfUsage').change(function () {
            if ($(this).val() === 'custom') {
                $('.customDigit').removeClass('hide');
            } else {
                $('.customDigit').addClass('hide');
            }
        });

        $('.refund').click(function () {

        });

        $('#valueType').on('change', function () {
            console.log($(this).val())
            if ($(this).val() === 'fs') {
                $('.intVal').hide(300);
            } else {
                $('.intVal').show(400);
            }
        })

        $('#selCat').change(function () {
            var id = $(this).val();
            $.ajax({
                url: `{{env('GET_BASE_URL')}}/fashion/product/${id}/getsubcategories`,
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (response) {
                    if (response.status === '00') {
                        var subcat = response.data;
                        var option = '';
                        if (subcat.length > 0) {
                            for (var i = 0; i < subcat.length; i++) {
                                option += `<option value="${subcat[i].id}">${subcat[i].subCategory}</option>`;
                                $('#selSubCat').html(option);
                            }
                        } else {
                            option += `<option value="0" hidden>--Select--</option>`;
                            $('#selSubCat').html(option);
                        }

                    }

                },
                error: function (error) {
                    console.log(error);
                }

            })
        });

        $(document).on('change', '#valueType', function () {

        });

        ///fashion/product/getproductsbysub
        $(document).on('change', '#selSubCat', function () {
            var data = {
                page: 0,
                size: 6,
                subcategoryId: $('#selCat').val()
            };
            $.ajax({
                url: `{{env('GET_BASE_URL')}}/fashion/product/getproductsbysub`,
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (response) {
                    if (response.status === '00') {
                        var prod = response.data;
                        var option = '';
                        if (prod.length > 0) {
                            for (var i = 0; i < prod.length; i++) {
                                option += `<option value="${prod[i].id}">${prod[i].name}</option>`;
                                $('#selProd').html(option);
                            }
                        } else {
                            option += `<option value="0" hidden>--Select--</option>`;
                            $('#selProd').html(option);
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        })

        $('.generate-promo').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: `{{route('admin.generate.promo.code')}}`,
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (response) {
                    if (response.status === '00') {
                        var value = response.data;
                        $('#code').val(value);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        });

        /*$('.authenticatePromoCode').click(function(){

                let data = {
                    email: $('#promoEmail').val(),
                    password: $('#promoPassword').val(),
                    socialFlag: 'N'
                }

                console.log(data);
            $.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                url: `{{route('admin.login')}}`,
                success: function(res) {
                    if(res.status === '00'){
                        console.log(res);
                        var adminId = JSON.parse(window.localStorage.getItem('adminId'));
                        let userId = res.data.id;
                        console.log(userId);
                        console.log(adminId);

                        let user = adminId + ',' + userId;
                        console.log(user);
                        let promoChecker = user.split(',');

                        var promo = [];
                        console.log($('#code').val());
                        if($('#applyAll').prop('checked')){
                            promo.push({
                                itemId: -1,
                                itemType: 'all'
                            })
                        }else{
                            if($('#selProd').val() === null){
                                promo.push({
                                    itemId: $('#selCat').val(),
                                    itemType: 'category'
                                })
                            }else{
                                for(var i = 0; i < $('#selProd').val().length; i++){
                                    promo.push({
                                        itemId: $('#selProd').val()[i],
                                        itemType: 'product'
                                    })
                                }
                            }
                        }



                        var promoCode = {
                            code: $('#code').val(),
                            value: $('#valueType').val() === 'fs' ? '0' : $('#value').val(),
                            valueType: $('#valueType').val(),
                            numberOfUsage:$('#numberOfUsage').val() === 'custom' ? $('#noValue').val() : $('#numberOfUsage').val(),
                            expiryDate: $('#expiryDate').val(),
                            promoItems: promo,
                            promoItemSizes: [],
                            promoMakerCheckersUserIds: promoChecker
                        }


                        console.log(promoCode);
                         $.ajax({
                              url: `{{route('addPromoCode')}}`,
                        type: "POST",
                        data: JSON.stringify(promoCode),
                        dataType: 'json',
                        success: function(response){
                            console.log(response);
                            if(response.status === '00'){
                                swal(response.message, "", "success");
                                window.location.reload();
                            }else{
                                swal(response.message, "", "warning");
                                window.location.reload()
                            }
                        },
                        error: function(error){
                            console.log(error);
                        }

                    })
                    }

                },
                error: function (err) {
                    console.log(err);
                }

            })
        })*/

        $('#addPromoCode').click(function () {
            var promo = [];
            var adminId = JSON.parse(window.localStorage.getItem('adminId'));
            /*
                        if($('#applyAll').prop('checked')){
                                promo.push({
                                    itemId: -1,
                                    itemType: 'all'
                                })
                        }else{
                            if($('#selProd').val() === null){
                                promo.push({
                                    itemId: $('#selCat').val(),
                                    itemType: 'category'
                                })
                            }else{
                                for(var i = 0; i < $('#selProd').val().length; i++){
                                    promo.push({
                                        itemId: $('#selProd').val()[i],
                                        itemType: 'product'
                                    })
                                }
                            }
                        }*/
            $('#addPromoCode').attr('disabled','disabled');
            $('#preLoad').show();
            if($('.reqCode').val() === '') {
                $('.reqCode').addClass('error');
                $('#addPromoCode').removeAttr('disabled','disabled');
                $('#preLoad').hide();
            } else {
                var promoCode = {
                    code: $('#code').val(),
                    value: $('#valueType').val() === 'fs' ? '0' : $('#value').val(),
                    valueType: $('#valueType').val(),
                    numberOfUsage: $('#numberOfUsage').val() === 'custom' ? $('#noValue').val() : $('#numberOfUsage').val(),
                    expiryDate: $('#expiryDate').val(),
                    promoActiveLimit: $('#applyLimit').val(),
                    /*promoItems: promo,
                    promoItemSizes: [],*/
                    creatorId: adminId
                }
                console.log(promoCode);
                $.ajax({
                    url: `{{route('addPromoCode')}}`,
                    type: "POST",
                    data: JSON.stringify(promoCode),
                    dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        if (response.status === '00') {
                            swal(response.message, "", "success");
                            location.reload();
                        } else {
                            swal(response.message, "", "warning");
                            $('.reqCode').removeClass('error');
                            $('#addPromoCode').removeAttr('disabled','disabled');
                            $('#preLoad').hide();
                            location.reload()
                        }
                    },
                    error: function (error) {
                        console.log(error);
                        $('.reqCode').removeClass('error');
                        $('#addPromoCode').removeAttr('disabled','disabled');
                        $('#preLoad').hide();
                    }

                })
            }
            //[{"itemId":1,"itemType":"c"},{"itemId":2,"itemType":"e"}]
        })
    </script>
@endpush