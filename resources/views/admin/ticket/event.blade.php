@extends('layouts.admin.default')

@push('styles')
    <style>
        .proImg > img {
            height: 150px !important;
        }

        .col-image {
            position: relative;
        }

        .progress {
            display: none;
            position: absolute;
            top: 0;
            right: 0;
            padding: 10px 4px 24px 4px;
        }

        .hide-input {
            display: none;
        }
    </style>
@endpush

@section('pageTitle','Ticket')

@section('content')
    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-tag bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>Event Ticket</h5>
                            <span>Managing Event Tickets</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Event Ticket</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <button type="button" class="btn btn-primary createMyTicket" data-toggle="modal"
                                        data-target="#eventTicket">
                                    <i class="feather icon-plus"></i> Create Ticket
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="allproducts" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="order-table"
                                                           class="table table-striped table-bordered nowrap reduced-font">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">S/N</th>
                                                            <th scope="col">Type</th>
                                                            <th scope="col">Cost</th>
                                                            <th scope="col">Image</th>
                                                            <th scope="col">Expiry Date</th>
                                                            <th scope="col">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($res as $resp)
                                                            <tr>
                                                                <td>{{$loop->index + 1}}</td>
                                                                <td>{{$resp->type}}</td>
                                                                <td>{{number_format($resp->cost)}}</td>
                                                                <td>
                                                                    <div style="width: 150px;"><img style="width: 50%;"
                                                                                                    src="{{@$resp->ticketImage}}">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    {{\Carbon\Carbon::createFromTimestamp(@$resp->expiryDate / 1000)->format('d-M-Y')}}
                                                                    <br>
                                                                    <small class="text text-success">{{\Carbon\Carbon::createFromTimestamp(@$resp->expiryDate / 1000)->diffForHumans()}}</small>
                                                                </td>
                                                                <td>
                                                                    <button class="btn btn-small btn-primary updateTicket"
                                                                            ticketId="{{$resp->id}}" data-toggle="modal"
                                                                            data-target="#eventTicket"><i
                                                                                class="fa fa-pencil"></i></button>
                                                                    <button class="btn btn-small btn-danger deleteTicket"
                                                                            ticketId="{{$resp->id}}"><i
                                                                                class="fa fa-trash"></i></button>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>

            <div id="styleSelector">
            </div>
        </div>


        <div class="modal fade" data-backdrop="static" id="eventTicket" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Event Ticket</h5>
                        <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="art-close">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div>
                            <div class="form-group">
                                <label for="type">Type</label>
                                <input type="text" class="form-control" id="type">
                            </div>
                            <div class="form-group">
                                <label for="cost">Cost</label>
                                <input type="text" class="form-control" id="cost">
                            </div>
                            <div class="form-group">
                                <label for="expiryDate">Expiry Date</label>
                                <input type="datetime-local" class="form-control" id="expiryDate">
                            </div>
                            <div class="form-group">
                                <div class="col col-image">
                                    <label for="mainFabricType-1" class="proImg">
                                        <img id="mainFabric" src="{{ asset('img/material1.jpg')}}"
                                             class="mainFabric proFabricImg">
                                        <input multiple type='file' accept="image/png, image/jpeg" id='mainFabricType-1'
                                               image-attr="viewOne"
                                               class='hide-input mainFabricType proFabricImgInput'/>
                                        <div class="mainFabricAction proFabricImgAction"></div>
                                    </label>
                                    <div class="progress"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                        </button>
                        <button type="button"
                                class="btn btn-primary modalSaveBtn createTicket" {{--data-dismiss="modal"--}}>Save
                            changes
                        </button>
                    </div>
                </div>
            </div>
        </div>


    </div>
@stop

@push('scripts')
    <script>
        var ticketId;
        var edit = false;
        var cloudinaryDetails = {
            uploadPreset: 'c0tphuck',
            apiKey: '629146977531321',
            cloudName: 'har9qnw3d',
            ticketImage: '',
            secure_url: [],
            progress: 0
        };

        $('.createMyTicket').on('click', function () {
            edit = false;
        });


        $(document).on('change', '.proFabricImgInput', function (e) {
            var myFiles = e.target.files;
            var file = $(this).get(0).files[0];
            if (typeof file === 'undefined') {
                return false;
            }

            var that = $(this);

            if (file.size / 1024 > 1200) {
                swal("Cannot upload image size more than 500kb", "", "warning");
            } else {
                convertMoreToBase64(this, '.proFabricImg');

                const formData = new FormData();
                formData.append('file', myFiles[0]);
                formData.append('upload_preset', cloudinaryDetails.uploadPreset);
                formData.append('tags', 'upload');
                var request = new XMLHttpRequest();

                request.upload.onprogress = function (e) {
                    if (e.lengthComputable) {
                        console.log(Math.round(e.loaded / e.total * 100) + '%');
                        that.parents('.col-image').find('.progress').css('display', 'block');
                        that.parents('.col-image').find('.progress').html(Math.round(e.loaded / e.total * 100) + '%');
                        cloudinaryDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                    }
                }

                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        console.log(JSON.parse(request.response));
                        that.parents('.col-image').find('.progress').html('Completed');
                        console.log(JSON.parse(request.response).secure_url);
                        cloudinaryDetails.ticketImage = JSON.parse(request.response).secure_url;

                        cloudinaryDetails.secure_url.push(JSON.parse(request.response).secure_url);
                    }
                }

                request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
                request.send(formData);
            }
        });

        $(document).on('click', '.createTicket', function () {
            var data = {
                type: $('#type').val(),
                cost: $('#cost').val(),
                expiryDate: $('#expiryDate').val() + ':25.313Z',
                ticketImage: cloudinaryDetails.ticketImage
            };


            console.log(data);
            if (edit) {
                data.id = ticketId;
                console.log(data);

                $.ajax({
                    url: '{{route('admin.updateTicket')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    success: function (response) {
                        console.log(response);
                        if (response.status === '200') {
                            swal(response.message, '', 'success');
                            location.reload();
                        } else {
                            swal(response.message, '', 'warning');
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            } else {
                $.ajax({
                    url: '{{route('admin.ticketCreate')}}',
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.stringify(data),
                    success: function (response) {
                        console.log(response);
                        if (response.status === '200') {
                            swal(response.message, '', 'success');
                            location.reload();
                        } else {
                            swal(response.message, '', 'warning');
                        }
                    },
                    error: function (error) {
                        console.log(error);
                    }
                })
            }
        });

        $('.deleteTicket').on('click', function () {
            var data = {
                ticketId: $(this).attr('ticketId')
            };

            console.log(data);
            swal({
                title: "Are you sure you want to delete?",
                text: "Click yes to continue",
                icon: "warning",
                buttons: ["No","Yes"],
                dangerMode: true,
            }).then((confirm) => {
                if(confirm) {
                    $.ajax({
                        url: '{{route('admin.deleteTicket')}}',
                        type: 'POST',
                        dataType: 'json',
                        data: JSON.stringify(data),
                        success: function (response) {
                            console.log(response);
                            if (response.status === '200') {
                                swal(response.message, '', 'success');
                                location.reload();
                            } else {
                                swal(response.message, '', 'warning');
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    })
                }
            })
        });


        $('.updateTicket').on('click', function () {
            console.log($(this).attr('ticketId'));
            var data = {
                ticketId: $(this).attr('ticketId')
            }

            $.ajax({
                url: "{{route('admin.getTicket')}}",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    console.log(response);
                    if (response.status === '200') {
                        edit = true;
                        ticketId = response.data.id;
                        $('#type').val(response.data.type);
                        $('#cost').val(response.data.cost);
                        $('#expiryDate').val(response.data.expiryDate);
                        $('#mainFabric').attr('src', response.data.ticketImage);
                        cloudinaryDetails.ticketImage = response.data.ticketImage;
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        });
    </script>
@endpush
