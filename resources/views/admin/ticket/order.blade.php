@extends('layouts.admin.default')

@section('pageTitle','Order')

@section('content')
    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-4">
                    <div class="page-header-title">
                        {{--<i class="feather icon-home bg-c-blue"></i>--}}
                        <div class="d-inline">
                            <h5>Event Order</h5>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div style="display: flex">
                        <form id="form2" action="{{route('admin.search.ticket')}}" method='post' class="row">
                            {{csrf_field()}}
                        <input type="text" class="form-control" name="search">
                        <button class="btn btn-search" style="margin-top: 5px;">Search</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Dashboard</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="allproducts" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    @if(strpos(Request::url(), 'auth/manage/ticket-order'))
                                                        <table id="order-table" class="table table-striped table-bordered nowrap reduced-font">
                                                            <thead>
                                                            <tr>
                                                                <th scope="col">Order Num</th>
                                                                <th scope="col">User Detail</th>
                                                                <th scope="col">Amount</th>
                                                                <th scope="col">Detail</th>
                                                                <th scope="col">Payment Type</th>
                                                                <th scope="col">Update Payment</th>
                                                                <th scope="col">Status</th>
                                                                <th scope="col">Action</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($res as $resp)
                                                                <tr>
                                                                    <td>{{@$resp->orderNum}}</td>
                                                                    <td>
                                                                        <div>{{@$resp->firstName}} {{@$resp->lastName}}</div>
                                                                        <div>
                                                                            <div>{{@$resp->email}}</div>
                                                                            <div>{{@$resp->phoneNo}}</div>
                                                                        </div>
                                                                    </td>
                                                                    <td>{{@$resp->totalAmount}}</td>
                                                                    <td>
                                                                        @if(is_array(@$resp->ticketCartDTOs))
                                                                            @for($i = 0; $i < count(@$resp->ticketCartDTOs); $i++)
                                                                                <div><strong>TYPE: </strong>{{@$resp->ticketCartDTOs[$i]->type}}</div>
                                                                                <div><strong>QUANTITY: </strong>{{@$resp->ticketCartDTOs[$i]->quantity}}</div>
                                                                                <div><strong>COST: </strong>{{@$resp->ticketCartDTOs[$i]->cost}}</div>
                                                                            @endfor
                                                                        @endif
                                                                    </td>
                                                                    <td>{{@$resp->paymentType}}</td>
                                                                    <td>
                                                                        @if(@$resp->isCollected)
                                                                            <div style="color: #41801f; font-weight: bold">Collected</div>
                                                                        @else
                                                                            <div style="color: #ff5635; font-weight: bold">Not Collected</div>
                                                                        @endif
                                                                    </td>
                                                                    <td> @if(@$resp->transactionReference && @!$resp->hasPaid)
                                                                            <button class="btn btn-wawooh confirmP" data-toggle="modal" data-target="#confirmModal" data-id="{{@$resp->transactionReference}}">Update Payment</button>
                                                                        @else
                                                                            <div>Confirm Payment</div>
                                                                        @endif
                                                                    </td>
                                                                    <td>
                                                                        @if(@$resp->isCollected)
                                                                            <button class="btn btn-wawooh" disabled>Collected</button></td>

                                                                    @else
                                                                        <button class="btn btn-wawooh collectTicket"  orderNum="{{@$resp->orderNum}}">Collect</button></td>
                                                                    @endif

                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    @else

                                                        <table id="order-table" class="table table-striped table-bordered nowrap reduced-font">
                                                            <thead>
                                                               <tr>
                                                                   <th scope="col"></th>
                                                                   <th scope="col">orderNum</th>
                                                                   <th scope="col">Type</th>
                                                                   <th scope="col">Cost</th>
                                                                   <th scope="col">Quantity</th>
                                                                   <th scope="col">Total</th>
                                                                   <th scope="col">Status</th>
                                                                   <th scope="col">Action</th>
                                                               </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($res as $resp)
                                                            <tr>
                                                                <td>{{$loop->index + 1}}</td>
                                                                <td>{{@$resp->orderNum}}</td>
                                                                <td>{{@$resp->type}}</td>
                                                                <td>{{@$resp->cost}}</td>
                                                                <td>{{@$resp->quantity}}</td>
                                                                <td>{{@$resp->total}}</td>
                                                                <td><div style="color: #ff5635; font-weight: bold">Not Collected</div></td>
                                                                <td>
                                                                    <button class="btn btn-wawooh collectTicket"  orderNum="{{@$resp->orderNum}}">Collect</button></td>
                                                                </td>


                                                            </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr>

                    </div>

                </div>
            </div>

            <div id="styleSelector">
            </div>
        </div>


        <div class="modal fade" data-backdrop="static" id="confirmModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="art-close">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div>
                            Payment Confirm
                            <select class="form-control valueConfirm">
                                <option>Please Select</option>
                                <option value=1>Yes</option>
                                <option value=2>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-primary modalSaveBtn bespokeConfirm">Save
                            changes
                        </button>
                    </div>
                </div>
            </div>
        </div>



        <div class="modal fade" data-backdrop="static" id="eventTicket" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="art-close">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                       <div>
                           <div class="form-group">
                               <label for="type">Type</label>
                               <input type="text" class="form-control" id="type">
                           </div>
                           <div class="form-group">
                           <label for="cost">Cost</label>
                           <input type="text" class="form-control" id="cost">
                           </div>
                           <div class="form-group">
                               <label for="expiryDate">Expiry Date</label>
                               <input type="datetime-local" class="form-control" id="expiryDate">
                           </div>
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-primary modalSaveBtn createTicket" {{--data-dismiss="modal"--}}>Save
                            changes
                        </button>
                    </div>
                </div>
            </div>
        </div>


    </div>
@stop

@push('scripts')

    <script>

     $(document).ready(function () {

         <?php if(strpos(Request::url(), 'auth/manage/search-ticket-order')): ?>
            localStorage.setItem('searchTicket', 'searchT');
         <?php else: ?>
             localStorage.removeItem('searchTicket');
         <?php endif; ?>
     })


        var num = '';


        $('.confirmP').click(function(){
            num = $(this).attr('data-id');
        });

        $('.bespokeConfirm').on('click', function() {
            var data = {
                transactionReference: num,
            };

            if($('.valueConfirm').val() === "1"){
                data.paymentConfirmed = true
            } else {
                data.paymentConfirmed = false;
            }

            console.log(data);

            $.ajax({
                url: '{{route('confirm.ticket.order')}}',
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    console.log(response);
                    if(response.status === '200') {
                        swal(response.message, '', 'success');

                        location.reload();
                    }else{
                        swal(response.message, '', 'error');
                    }
                },
                error: function (e) {
                    console.log(e);
                }

            })
        });


        $('.collectTicket').click(function(){
           // console.log($(this).attr('orderNum'));
            var data = {
                ticketCollected: true,
                orderNum: $(this).attr('orderNum')
            };

            console.log(data);

            $.ajax({
                url: '{{route('admin.collect.ticket')}}',
                type: 'POST',
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if(response.status === '200') {
                        swal(response.data, '', 'success');

                        if(localStorage.getItem('searchTicket') === 'searchT') {
                            location.href = '{{route('admin.ticket.order')}}'
                        } else {
                            location.reload();
                        }
                    }else{
                        swal(response.data, '', 'error');
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            })
        });

        $('.searchTicket').click(function(){
            let orderNum = $('.searchTicketValue').val()

           $.ajax({
               url: `{{route('admin.search.ticket')}}/orderNum=${orderNum}`,
               type: "GET",
               dataType: "json",
               success: function (response) {
                   console.log(response);
               },
               error: function (error) {
                   console.log(error);
               }
           })
        });
    </script>

@endpush
