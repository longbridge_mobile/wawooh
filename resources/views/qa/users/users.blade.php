@extends('layouts.qa.default')

@push('styles')

@endpush

@section('pageTitle','All Users')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-user bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Users Only</h5>
                                    <span>All Registered users on the platform</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('qa.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Users</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">

                                        <div class="card">
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped" id="usersTable">
                                                        <thead>
                                                        <tr>
                                                            {{--<th scope="col">S/N</th>--}}
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Email</th>
                                                            <th scope="col">Phone</th>
                                                            <th scope="col">Date of Reg</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($allUsers))
                                                            @if(count($allUsers))
                                                                @foreach($allUsers as $user)
                                                                    @if($user->role == 'user')
                                                                        <tr>
{{--                                                                            <td>{{$loop->iteration}}</td>--}}
                                                                            <td>{{$user->firstName . ' ' . $user->lastName}}</td>
                                                                            <td>{{$user->email}}</td>
                                                                            <td>{{$user->phoneNo}}</td>
                                                                            <td>{{\Carbon\Carbon::parse($user->activationDate)->format('d-M-Y')}}</td>
                                                                        </tr>
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <label class="label label-warning">No Available
                                                                    Data</label>
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <a href="{{route('exportUsers')}}"
                                                       class="pull-right btn btn-primary">Export Users
                                                        Data</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="styleSelector">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

@endpush