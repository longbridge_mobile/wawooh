@extends('layouts.qa.default')

@push('styles')

@endpush

@section('pageTitle', 'Designer | ' . $designer->storeName)
@section('content')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="page-header card">
                    <div class="row align-items-end">
                        <div class="col-lg-8">
                            <div class="page-header-title">
                                <i class="feather icon-home bg-c-blue"></i>
                                <div class="d-inline">
                                    <h5>Designer Details</h5>
                                    <span>The full details of {{$designer->storeName}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="page-header-breadcrumb">
                                <ul class=" breadcrumb breadcrumb-title">
                                    <li class="breadcrumb-item">
                                        <a href="{{route('admin.dashboard')}}"><i class="feather icon-home"></i></a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('admin.designers')}}">Designers</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#!">Detail</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper designerProfile">
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-block">
                                                <div class="sellerCover">
                                                    @if(isset($designer->banner))
                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$designer->banner)}}"
                                                             alt="Banner">
                                                    @else
                                                        <img src="{{asset('img/designerBanner.jpeg')}}" alt="">
                                                    @endif
                                                </div>
                                                <div class="owner-store-img view-img-store">
                                                    @if(isset($designer->logo))
                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'], @$designer->logo)}}" class="img-admin-des">
                                                        <span class="online-status online"></span>
                                                    @else
                                                        <img src="{{ asset('img/logo-sm.png') }}" class="img-admin-des">
                                                    @endif
                                                </div>
                                                <div class="bottomDetail">
                                                    <h3>{{@$designer->storeName}}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Sale's Information</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="store-des-small edit-store">
                                                    <div class="dt-responsive table-responsive">
                                                        <table class="table reduced-font">
                                                            <tr>
                                                                <td>Successful Sales</td>
                                                                <td>{{@$designer->noOfDeliveredOrders}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Active Orders</td>
                                                                <td>{{@$designer->noOfConfirmedOrders}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Completed Orders</td>
                                                                <td>{{@$designer->noOfDeliveredOrders}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Member Since</td>
                                                                <td>{{\Carbon\Carbon::parse($designer->createdDate)->format('d-M-Y')}}
                                                                    <br>
                                                                    <small class="text-success">
                                                                        ({{\Carbon\Carbon::parse($designer->createdDate)->diffForHumans()}}
                                                                        )
                                                                    </small>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    @if($designer->status === 'A')
                                                                        <label class="label label-completed">Designer is
                                                                            Active</label>
                                                                    @elseif($designer->status === 'D')
                                                                        <label class="label label-danger">Designer is
                                                                            Deactivated</label>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    {{--<div>
                                                        <a href="#">
                                                            --}}{{-- status='@if($designer->status == 'A')D @else A @endif'--}}{{--
                                                            <button status='{{@$designer->status === 'A' ? 'D' : 'A'}}'
                                                                    designerId='{{$designer->id}}'
                                                                    class="btn btn-update view-customer action">@if($designer->status == 'A')
                                                                    Deactivate Designer @else Activate Designer @endif</button>
                                                        </a>
                                                    </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Personal Information</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table reduced-font">
                                                        <tr>
                                                            <td>Full Name</td>
                                                            <td>{{$designer->firstName}} {{$designer->lastName}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>{{$designer->email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Phone Number</td>
                                                            <td>{{$designer->phoneNo}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Date of Birth</td>
                                                            <td>{{$designer->dateOfBirth}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gender</td>
                                                            <td>{{$designer->gender}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Account Information</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table reduced-font">
                                                        <tr>
                                                            <td>Account Name</td>
                                                            <td>{{$designer->accountName}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Account Number</td>
                                                            <td>{{$designer->accountNumber}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bank</td>
                                                            <td>{{$designer->bankName}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Currency</td>
                                                            <td>{{$designer->currency}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Country</td>
                                                            <td></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Business Information</h5>
                                            </div>
                                            <div class="card-block businessInfo">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="row m-0">
                                                            <div class="col-md-12 pl-0 pb-2">
                                                                <label class="label label-inspection">Physical
                                                                    Address</label>
                                                                <div>{{$designer->address}}</div>
                                                            </div>
                                                            <div class="col-md-12 pl-0 pb-2">
                                                                <label class="label label-inspection">Country:</label>
                                                                <span>{{$designer->country}}</span>
                                                            </div>
                                                            <div class="col-md-12 pl-0 pb-2">
                                                                <label class="label label-inspection">State/Region:</label>
                                                                <span>{{$designer->state}}</span>
                                                            </div>
                                                            <div class="col-md-12 pl-0 pb-2">
                                                                <label class="label label-inspection">Local
                                                                    Govt:</label>
                                                                <span>{{$designer->localGovt}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4"
                                                         style="border-right: 1px solid rgba(0,0,0,.2)">
                                                        <div class="col-md-12 pl-0 pb-2">
                                                            <label>Business Status:</label>
                                                            <label class="label label-{{$designer->registeredFlag === 'N' ? 'pending':'completed'}}">
                                                                {{$designer->registeredFlag === 'N' ? 'Not Registered':'Registered'}}
                                                            </label>
                                                        </div>
                                                        @if($designer->registeredFlag === 'N')
                                                            <div class="col-md-12 pl-0 pb-2">
                                                                <label class="label label-inspection">Business
                                                                    ID:</label>
                                                                <div>
                                                                    <a data-fancybox
                                                                       href="{{str_replace('http','https', $designer->registrationDocument)}}">
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'], $designer->registrationDocument)}}"
                                                                             alt="ID" class="BiD">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @elseif($designer->registeredFlag === 'Y')
                                                            <div class="col-md-12 pl-0 pb-2">
                                                                <label class="label label-inspection">Business
                                                                    Number:</label>
                                                                <span><strong>{{$designer->registrationNumber}}</strong></span>
                                                                <div>
                                                                    <a data-fancybox
                                                                       href="{{str_replace('http','https', $designer->registrationDocument)}}">
                                                                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'], $designer->registrationDocument)}}"
                                                                             alt="ID" class="BiD">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <hr>
                                                        <div class="col-md-12 pl-0 pb-2">
                                                            <label>Size Guide: </label>
                                                            @if($designer->sizeGuideFlag === 'N')
                                                                <label class="label label-pending">No Size Guide. Wawooh Size Guide In Use</label>
                                                            @elseif($designer->sizeGuideFlag === 'Y')
                                                                <label class="label label-completed">Uploaded Designer Guide</label>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="col-md-12 pl-0 pb-2">
                                                            <label>Bespoke Eligible Status: </label>
                                                            @if($designer->bespokeEligible === null)
                                                                <label class="label label-pending">Not Registered</label>
                                                            @elseif($designer->bespokeEligible === 'N')
                                                                <label class="label label-rejected">Not Verify</label>
                                                            @elseif($designer->bespokeEligible === 'Y')
                                                                <label class="label label-completed">Verified</label>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-12 pl-0 pb-2">
                                                            <div class="form-group">
                                                                <label for="threshold" class="label label-inspection">Bespoke
                                                                    Threshold Production</label>
                                                                <input type="number" class="form-control" id="threshold"
                                                                       value="{{$designer->threshold}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="button" value="Update"
                                                                       class="btn btn-sm btn-warning">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Other Information</h5>
                                            </div>
                                            <div class="card-block businessInfo">
                                                <div class="row">
                                                    {{--<div class="col-xl-3 col-md-6">
                                                        <div class="card prod-p-card card-green">
                                                            <div class="card-body">
                                                                <div class="row align-items-center text-center">
                                                                    <div class="col">
                                                                        <h6 class="m-b-5 text-white">Total Products</h6>
                                                                        <h3 class="m-b-0 f-w-700 text-white text-center f24">
                                                                            @if(is_array($designer->products) && isset($designer->products))
                                                                                {{count($designer->products)}}
                                                                            @else
                                                                                0
                                                                            @endif
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>--}}
                                                    <div class="col-xl-3 col-md-6">
                                                        <div class="card prod-p-card card-blue">
                                                            <div class="card-body">
                                                                <div class="row align-items-center text-center">
                                                                    <div class="col">
                                                                        <h6 class="m-b-5 text-white">Total Rating</h6>
                                                                        <h3 class="m-b-0 f-w-700 text-white f24">
                                                                            {{$designer->qualityRating}}
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6">
                                                        <div class="card prod-p-card card-red">
                                                            <div class="card-body">
                                                                <div class="row align-items-center">
                                                                    <div class="col">
                                                                        <h6 class="m-b-5 text-white text-center">
                                                                            Cancelled Order</h6>
                                                                        <h3 class="m-b-0 f-w-700 text-white text-center f24">
                                                                            {{isset($designer->noOfCancelledOrders) ? $designer->noOfCancelledOrders : '0'}}
                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-md-6">
                                                        <div class="card prod-p-card card-yellow">
                                                            <div class="card-body">
                                                                <div class="row align-items-center">
                                                                    <div class="col">
                                                                        <h6 class="m-b-5 text-white text-center">Total
                                                                            Sale</h6>
                                                                        <h3 class="m-b-0 f-w-700 text-white f24">

                                                                        </h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Designer's Products</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="shift product-pics">
                                                    @if(count($designer->products))
                                                        @foreach(array_chunk($designer->products, 4) as $products)

                                                            <div class="row">
                                                                @foreach($products as $product)
                                                                    <div class="col-md-3">
                                                                        <div class="reduce back-change">
                                                                            <div class="fix-height">
                                                                                @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                                                    <a data-fancybox
                                                                                       href="{{str_replace(['http','upload'],['https','upload/q_40'], $product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}"><img
                                                                                                class="img-ho"
                                                                                                src="{{str_replace(['http','upload'],['https','upload/q_40'], $product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}"></a>
                                                                                @endif
                                                                            </div>
                                                                            <div class="nameProd">
                                                                                <div class="prodName1">
                                                                                    <h3 class="Gucci">{{$product->name}}</h3>
                                                                                </div>
                                                                                <div class="prodDes">
                                                                                    <h3>{{$designer->storeName}}</h3>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row font-small">
                                                                                @if($product->slashedPrice > 0)
                                                                                    <div class="col-md-6 col-xs-12">
                                                                                        <p style="margin-bottom: 0px;">
                                                                                            <del>₦
                                                                                            </del> {{getNewPrice($product->amount, $product->slashedPrice)}}
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="col-xs-12 col-md-12">
                                                                                        <div class="row">
                                                                                            <div class="col-md-6">
                                                                                                <p class="text-danger"
                                                                                                   style="font-size: 11px;">
                                                                                                    <span style="text-decoration: line-through">₦ {{number_format($product->amount)}}</span>
                                                                                                </p>
                                                                                            </div>
                                                                                            <div class="col-md-6 textRight discountpercent">
                                                                                                <p>
                                                            <span>&dash;{{getDiscountPercent($product->slashedPrice,$product->amount)}}
                                                                %</span></p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                @else
                                                                                    <div class="col-md-6 col-xs-12">
                                                                                        <p style="margin-bottom: 0px;">
                                                                                            <del>₦
                                                                                            </del>{{number_format($product->amount)}}
                                                                                        </p>
                                                                                    </div>
                                                                                @endif
                                                                                <div class="col-md-12 col-6">
                                                                                    @if(isset($product->productQualityRating) && $product->productQualityRating > 0)
                                                                                        @for($i= 0; $i < $product->productQualityRating; $i++ )
                                                                                            <span class="fa fa-star checked"></span>
                                                                                        @endfor
                                                                                        @for($i= 0; $i < (5 - $product->productQualityRating); $i++ )
                                                                                            <span class="fa fa-star"></span>
                                                                                        @endfor
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach

                                                            </div>

                                                        @endforeach

                                                    @else
                                                        <hr>
                                                        <div class="row">
                                                            <div class="col-md-12 text-center">
                                                                <span class="alert alert-danger">Designer has no verified product</span>
                                                            </div>
                                                        </div>

                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="styleSelector">
            </div>
        </div>
    </div>

    <div id="messageModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <br>
                    <br>
                    <div class="form-group">
                        <textarea class="form-control" id="mssgName" rows="10" placeholder="Write Message"></textarea>
                    </div>
                    <div style="text-align: right;">
                        <button class="btn btn-wawooh">Send</button>
                        <button class="btn btn-update">Cancel</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@push('scripts')

@endpush