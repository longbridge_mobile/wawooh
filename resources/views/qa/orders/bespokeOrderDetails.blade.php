@extends('layouts.qa')

@push('styles')

@endpush

@section('pageTitle', $newresp->productName . ' - Order Detail')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Order-Details</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="{{route('qa.orders.all')}}"><i class="fa fa-home"></i> Orders</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> Order Details</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <div class="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row product-img">
                                <div class="col-md-9 product-big-img">
                                    <img id='mainPicture' src="{{$newresp->bespokePicture[0]}}" class="img-fluid fitDiv">
                                </div>
                                <div class="col-md-3 product-small-img">
                                    {{--@if(count($newresp->picture))
                                        @foreach($newresp->picture as $picture)
                                            <div>
                                                <img  src="{{@$picture->picture}}" class="img-fluid">
                                            </div>
                                        @endforeach
                                    @endif--}}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 product-des">
                            <table class="table">
                                <tr>
                                    @if($newresp->status == 'P')
                                        <td>Order Status</td>
                                        <td><label class="label label-pending">Pending</label></td>
                                    @elseif($newresp->status == 'PC')
                                        <td>Order Status</td>
                                        <td><label class="label label-completed">Payment Confirmed</label></td>
                                    @elseif($newresp->status == 'OP')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Processing</label></td>
                                    @elseif($newresp->status == 'A')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Accepted</label></td>
                                    @elseif($newresp->status == 'WR')
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Wawooh Rejected</label></td>
                                    @elseif($newresp->status == 'WC')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Wawooh Collected</label></td>
                                    @elseif($newresp->status == 'CO')
                                        <td>Order Status</td>
                                        <td><label class="label label-completed">Completed</label></td>
                                    @elseif($newresp->status == 'RS')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Ready for Shipping</label></td>
                                    @elseif($newresp->status == 'RI')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Ready for Inspection</label></td>
                                    @elseif($newresp->status == 'OS')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Order Shipped</label></td>
                                    @elseif($newresp->status == 'C')
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Cancelled</label></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Order Number</td>
                                    <td>{{$newresp->orderNum}}</td>
                                </tr>
                                <tr>
                                    <td>Product Details/Name</td>
                                    <td>{{$newresp->productName}}</td>
                                </tr>
                                <tr>
                                  {{--  <td>Order Date</td>
                                    <td>{{Carbon\Carbon::parse($newresp->orderDate)->format('d-m-Y')}}
                                        <small class="text-success">({{\Carbon\Carbon::parse($newresp->orderDate)->diffForHumans()}})</small>
                                    </td>--}}
                                </tr>
                                <tr>
                                    <td>Amount</td>
                                    <td>₦{{number_format($newresp->price)}}</td>
                                </tr>
                                @if(isset($newresp->failedInspectionReason))
                                    <tr>
                                        <td>Has Message?</td>
                                        <td><a href="#myModalMessage" data-target="#myModalMessage" data-toggle="modal"><i
                                                        class="animated pulse infinite fa fa-fw fa-envelope-o"></i>Message</a>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>Measurement</td>
                                    @if(!is_null($newresp->requiredMeasurement))
                                        <td><a href="#" data-toggle="modal" data-target="#measurementModal">Measurement
                                                Available</a></td>
                                    @else
                                        <td><a href="#" data-toggle="modal" data-target="#">No Measurement
                                                Available</a>
                                        </td>
                                    @endif
                                </tr>
                                @if(isset($newresp->pictures))
                                    <tr>
                                        <td>Inspection Image from Designer</td>
                                        <td>
                                            <ul class="inspection-image">
                                                @foreach($newresp->pictures as $picture)
                                                    <li>
                                                        <a id='lb' href='{{str_replace('http','https',$picture->pictureName)}}'
                                                           data-lightbox="image-1">
                                                            <img src="{{str_replace('http','https',$picture->pictureName)}}"
                                                                 class="">
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div>
                   <div class="giveMarginTop">
                       <h5 class="productDetailsLabel">Materials</h5>
                       <div class="row">
                           @if(count($newresp->materialPicture))
                               @foreach($newresp->materialPicture as $materialPicture)
                                   <div class="col-md-3">
                                       <div class="material-img">
                                           <img src="{{$materialPicture->materialPicture}}">
                                       </div>
                                   </div>
                               @endforeach
                           @endif
                       </div>
                   </div>

                   <div class="giveMarginTop">
                       <h5 class="productDetailsLabel">Artworks</h5>
                       <div class="row">
                           @if(count($newresp->artWorkPicture))
                               @foreach($newresp->artWorkPicture as $artWorkPicture)
                                   <div class="col-md-3">
                                       <div class="material-img">
                                           <img src="{{ $artWorkPicture->artWorkPicture }}">
                                       </div>
                                   </div>
                               @endforeach
                           @endif
                       </div>
                   </div>
               </div>--}}

            <div style="text-align: right" class="giveMarginTop">
                <a href="{{route('qa.orders.all')}}" class="btn btn-wawooh">Go Back</a>
            </div>
    </div>
    </section>
    </div>
    <div class="modal orderDetailsModal fade" data-backdrop="static" id="measurementModal" tabindex="-1" role="dialog"
         aria-labelledby="measurementModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Measurement in <span style="font-size: 10px" id="default">centimeters</span> <span
                                style='font-size:10px' id='unit'></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label id='convert' class='btn btn-wawooh'>Convert Measurement</label>
                    <label id='convertBack' class='btn btn-wawooh hide'>Convert Measurement</label>


                    <div class="row measurement-input"  style="padding: 10px">

                        @foreach($newresp->requiredMeasurement as $measurement => $value)
                            @if(($measurement !== 'id') && ($measurement !== 'createdOn') && ($measurement !== 'delFlag') && ($measurement !== 'notes') && ($measurement !== 'unit') && ($value !== null))
                            <div class="row">
                                <div class="col-lg-7">
                                     <p>{{$measurement}}</p>
                                </div>
                                <div class="col-lg-5">
                                    <input id='overBust' type="text" class="form-control custom"
                                           value="{{$value}}">
                                </div>
                            </div>

                                @endif

                        @endforeach


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-wawooh" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('scripts')

    <script>
        $('#convert').click(function () {
            $('.custom').each(function () {
               if($(this).val() !== ''){
                   $val = $(this).val() / 2.54;
                   $(this).val($val);
               }
            });
           /* if ($('.custom').val()) {
                oldValue = $('.custom').val();
                $('.custom').val($('.custom').val() * 5);
            }*/
            $(this).addClass('hide');
            $('#convertBack').removeClass('hide');
            $('#default').html('inches');
        });

        $('#convertBack').click(function(){
            $('.custom').each(function () {
                if($(this).val() !== ''){
                    $val = $(this).val() * 2.54;
                    $(this).val($val);
                }
            });
            $(this).addClass('hide');
            $('#convert').removeClass('hide');
            $('#default').html('centimeters');
        });
    </script>

@endpush