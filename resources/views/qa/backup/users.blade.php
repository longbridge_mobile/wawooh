@extends('layouts.qa.default')

@push('styles')

@endpush

@section('pageTitle','All Users')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="{{route('qa.dashboard')}}"><i class="fa fa-home"></i> Home</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> User</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">Users</h3>
                <div class="all-order">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Total Number of Registered Users: <span class="badge badge-info">{{--{{ count($designers) }}--}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped" id="designerTable">
                            <thead>
                            <tr>
                                {{--<th scope="col">S/N</th>--}}
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Date Joined</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(isset($allUsers))
                                @if(count($allUsers))
                                    @foreach($allUsers as $user)
                                        @if($user->role == 'user')
                                        <tr>
{{--                                            <td>{{$loop->iteration}}</td>--}}
                                            <td>{{$user->firstName . ' ' . $user->lastName}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phoneNo}}</td>
                                            <td>{{\Carbon\Carbon::parse($user->activationDate)->format('d-M-Y')}}</td>
                                        </tr>
                                        @endif
                                    @endforeach
                                    @else
                                    <label class="label label-warning">No Available Data</label>
                                @endif
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <a href="" class="pull-right btn btn-primary">Export Data</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('scripts')

@endpush