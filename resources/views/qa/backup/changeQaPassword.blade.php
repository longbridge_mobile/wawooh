@extends('layouts.qa.default')

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style>
        #orderT {
            text-align: center;
        }
        table th {
            text-align: center;
        }
    </style>
@endpush

@section('pageTitle','All Orders')
@section('content')
    <div class="content-wrapper">
        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">Change Password</h3>
                <div class="all-order">
                    <div class="row">

                        <div class="row">
                            <div class="col-md-3">
                                <h5>Old Password</h5>
                                <input type="password" id="oldPassword" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h5>New Password</h5>
                                <input type="password" id="newPassword" class="form-control">
                            </div>
                            {{--<div class="col-md-6">
                                <h5>Verify Password</h5>
                                <input type="password" id="newAdminVerifyPassword" class="form-control">
                            </div>--}}


                            <div class="col-md-4" style="margin-top: 30px;">
                                <input type="button" id="submitChangeQAPassword"  class="btn btn-primary" value="Change Password">

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('scripts')

    <script>

        $('#submitChangeQAPassword').on('click', function () {
        $('#submitChangeQAPassword').val('Processing...').attr('disabled', 'disabled');
        let oldPassword = $('#oldPassword').val();
        let newPassword = $('#newPassword').val();
        if (oldPassword === '' || newPassword === '') {
        swal('Field is required', '', 'warning');
        $('#submitChangeQAPassword').val('Update Password').removeAttr('disabled', 'disabled');
        } else {
        let data = {
        email: 'qa@wawooh.com',
        oldPassword: oldPassword,
        newPassword: newPassword,
        };
        console.log(data);
        $.ajax({
        url: '{{route('change.qa.password')}}',
        method: 'POST',
        data: JSON.stringify(data),
        success: function (result) {
        console.log(result);
        if (result.status === "00") {
        $(this).val('Password Changed').attr('disabled', 'disabled');
        window.location.href = '{{route('admin.dashboard')}}';
        // location.reload();
        } else if (result.status === '99') {
        swal('Something went wrong', '', 'error');
        $('#submitChangeQAPassword').val('Update Password').removeAttr('disabled', 'disabled');
        }
        },
        error: function (e) {
        $('#submitChangeQAPassword').val('Update Password').removeAttr('disabled', 'disabled');
        console.log(e);
        }
        });
        }

        });
    </script>
@endpush