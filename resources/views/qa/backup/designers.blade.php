@extends('layouts.qa.default')

@push('styles')

@endpush

@section('pageTitle','All Designers')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> Designer</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">Designers</h3>
                <div class="all-order">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Total Number of Registered Designers: <span class="badge badge-info">{{--{{ count($designers) }}--}}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped" id="designerTable">
                            <thead>
                            <tr>
                                <th scope="col">S/N</th>
                                <th scope="col">Name</th>
                                <th scope="col">Store Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Date Joined</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(isset($allDesigners))
                                @if(count($allDesigners))
                                    @foreach($allDesigners as $designer)
                                        <tr class='clickable-row' data-href='order-details.html'>
                                            <td>{{$loop->iteration}}</td>
                                            <td>
                                                <a href='{{route('admin.designer.view', $designer->id)}}'>{{$designer->firstName}} {{$designer->lastName}}</a>
                                            </td>
                                            <td>{{$designer->storeName}}</td>
                                            <td>{{$designer->email}}</td>
                                            <td>{{$designer->phoneNo}}</td>
                                            <td>{{\Carbon\Carbon::parse($designer->createdDate)->format('d-M-Y')}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <label class="label label-warning">No Available Data</label>
                                @endif
                            @endif

                            </tbody>
                        </table>

                        <hr>

                        <div class="row">
                            <div class="col-md-12">
                                <a href="" class="pull-right btn btn-primary">Export Data</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('scripts')

@endpush