@extends('layouts.qa')

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <style>
        #orderT {
            text-align: center;
        }

        table th {
            text-align: center;
        }
    </style>
@endpush

@section('pageTitle','All Orders')
@section('content')
    <div class="content-wrapper">
        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">All Orders</h3>
                <div class="all-order">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Total number of entries</p>
                                </div>
                                <div class="col-md-5">
                                    <button class="null-btn">{!! $totalOrder->total() !!}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-striped" id="orderT">
                            <thead>
                            <tr>
                                <th scope="col">Order No</th>
                                <th scope="col">Product</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                                <th scope="col">Picture</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($totalOrder))
                                @if(count($totalOrder))
                                    @foreach($totalOrder as $items => $order)
                                        <tr class='clickable-row' data-href='order-details.html'>
                                            <td>
                                                <a href="{{route('qa.order.details')}}?id={{$order->id}}">{{$order->orderNumber}}</a>
                                            </td>
                                            <td>{{$order->productName}}</td>
                                            <td>{{$order->quantity}}</td>
                                            <td>₦{{number_format($order->amount)}}</td>
                                            <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}</td>
                                            <td>
                                                @if($order->status == 'PC')
                                                    <label class="label label-completed">Payment Confirmed</label>
                                                @elseif($order->status == 'P')
                                                    <label class="label label-pending">Pending</label>
                                                @elseif($order->status == 'OR')
                                                    <label class="label label-rejected">Order Rejected</label>
                                                @elseif($order->status == 'OP')
                                                    <label class="label label-processing">Order Processing</label>
                                                @elseif($order->status == 'CO')
                                                    <label class="label label-completed">Completed by designer</label>
                                                @elseif($order->status == 'RI')
                                                    <label class="label label-inspection">Ready for Inspection</label>
                                                @elseif($order->status == 'WC')
                                                    <label class="label label-processing">Wawooh Collected</label>
                                                @elseif($order->status == 'WR')
                                                    <label class="label label-rejected">Wawooh Rejected</label>
                                                @elseif($order->status == 'RS')
                                                    <label class="label label-processing">Ready for Shipping</label>
                                                @elseif($order->status == 'OS')
                                                    <label class="label label-completed">Order Shipped</label>
                                                @elseif($order->status == 'OD')
                                                    <label class="label label-completed">Order Delivered</label>
                                                @endif
                                            </td>
                                            <td class="btn-display">

                                                @if($order->status == 'A')
                                                    <button customer='{{$order->customerId}}'
                                                            orderNumber='{{@$order->orderNumber}}'
                                                            order='{{$order->id}}' stat='C'
                                                            class="btn btn-order accept">Confirm
                                                    </button><span class="mySpinner"></span>
                                                @elseif($order->status == 'CO')
                                                    <button customer='{{$order->customerId}}'
                                                            orderNumber='{{@$order->orderNumber}}'
                                                            order='{{$order->id}}' statId=6 stat='RI'
                                                            class="btn btn-order accept">Ready for Inspection
                                                    </button><span class="mySpinner"></span>
                                                @elseif($order->status == 'WR')
                                                    <button customer='{{$order->customerId}}'
                                                            orderNumber='{{@$order->orderNumber}}'
                                                            order='{{$order->id}}' statId=32 stat='DP'
                                                            class="btn btn-order accept">Pick up by Designer
                                                    </button><span class="mySpinner"></span>

                                                @elseif($order->status == 'RI')
                                                    <button customer='{{$order->customerId}}'
                                                            orderNumber='{{@$order->orderNumber}}'
                                                            order='{{$order->id}}' statId=33 stat='WC'
                                                            class="btn btn-order accept">Wawooh Collected</button><span class="mySpinner"></span>
                                                @elseif($order->status == 'WC')
                                                    <button data-toggle="modal" data-target="#myModal"
                                                            customer='{{$order->customerId}}'
                                                            orderNumber='{{@$order->orderNumber}}'
                                                            order='{{$order->id}}' statId=11 stat="PI"
                                                            class="btn btn-wawooh passPass">Pass
                                                    </button>

                                                    <a href="#ModalFail" data-toggle="modal"
                                                       orderNumer={{$order->orderNumber}} customer="{{$order->customerId}}"
                                                       order="{{$order->id}}" class="btn btn-order"
                                                       id="inspection-failed" stat="RI">Fail</a><span
                                                            class="mySpinner"></span>

                                                @elseif($order->status == 'RS')


                                                    <a href="#TracingNumber" data-toggle="modal"
                                                       customer='{{$order->customerId}}'
                                                       orderNumber='{{@$order->orderNumber}}' order='{{$order->id}}'
                                                       stat='OS' statId=8 class="btn btn-order shipPassed">Ship</a><span
                                                            class="mySpinner"></span>
                                                    {{--
                                                                                                        <button data-toggle="modal" style="color: #ffffff; background: black;border-bottom-width: 3px;border-color: #cd9933;" data-target = "#myModal" class="btn btn-doneReject pull-right">Send Image</button>
                                                    --}}
                                                    {{--
                                                                                                        <a href="#TracingNumber" data-toggle="modal"  class="btn btn-warning">Ship</a><span class="mySpinner"></span>
                                                    --}}


                                                @elseif($order->status == 'OS')
                                                    <button customer='{{$order->customerId}}'
                                                            orderNumber='{{@$order->orderNumber}}'
                                                            order='{{$order->id}}' stat='D' statId=9
                                                            class="btn btn-order accept">Deliver
                                                    </button><span class="mySpinner"></span>
                                                @elseif($order->status == 'D')
                                                    Delivered
                                                @endif

                                            </td>
                                            <td>
                                                @if(isset($order->pictures))
                                                    <div class="wow animated tada infinite"><i
                                                                class="fa fa-picture-o"></i></div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endif

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


            <!--Reason for reject modal-->
            <div class="modal fade" data-backdrop="static" id="ModalFail" tabindex="-1" role="dialog"
                 aria-labelledby="ModalFail" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content qaUpload">
                        <div class="modal-body qaFailReason">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="ModalFail">
                                <h5>Submit your Reason</h5>
                                <div class="form-group">
                                    <label for="reason-text"></label>
                                    <textarea type="text" class="form-control" placeholder="Give your reason "
                                              id="reason-text"></textarea>
                                </div>
                                <div class="form-group">
                                    {{--@if(isset($totalOrder))
                                        @if(count($totalOrder))
                                            @foreach($totalOrder as $items => $order)--}}

                                    <input type="submit" value="Submit Reason" id="reasonBtn"
                                           class="btn reason-btn"{{-- customer="{{$order->customerId}}" orderNumber="{{$order->orderNumber}}" order="{{$order->id}}"--}}>
                                    {{-- @endforeach
                                                 @endif
                                         @endif--}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Shipping modal-->
            <div style="margin-top: 3em;" class="modal fade" data-backdrop="static" id="TracingNumber" tabindex="-1"
                 role="dialog"
                 aria-labelledby="TracingNumber" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body TracingNumber">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="ModalFail">
                                <h5>Enter Tracking Number</h5>
                                <div class="form-group">
                                    <label for="reason-text"></label>
                                    <input type="text" class="form-control" placeholder="Enter Tracking Number"
                                           id="trackingNumberValue">
                                </div>
                                <div class="form-group">
                                    {{--@if(isset($totalOrder))
                                        @if(count($totalOrder))
                                            @foreach($totalOrder as $items => $order)--}}

                                    <input type="button" value="Submit" id="tracking"
                                           class="btn btn-wawooh"{{-- customer="{{$order->customerId}}" orderNumber="{{$order->orderNumber}}" order="{{$order->id}}"--}}>
                                    {{-- @endforeach
                                                 @endif
                                         @endif--}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal fade" data-backdrop="static" id="myModal" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-dialog-preview" role="document">
                <div class="modal-content productSampleUpload">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel"><label>Upload Product Pictures</label></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form action="">
                            <div class="row">
                                <div class="col-md-5 col-md-offset-4">
                                    <span>Front Sample</span>
                                    <label for="samplePicture">
                                        <img class="img designerProduct img-responsive"
                                             src="{{ asset('img/material1.jpg')}}" height="150" alt="">
                                        <input type="file" id="samplePicture" class="hide-input addSampleProduct"/>
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        <button type="button" class="btn passUploadPicture"
                                style="color: #ffffff; background: black;border-bottom-width: 3px;border-color: #cd9933;">
                            <i class="fa fa-check-circle-o"></i> Upload and Send
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#orderT').DataTable();
        })
        let orderNum = '';
        $(document).ready(function () {

            <?php if(!is_null(session('qaToken'))): ?>
                tok = <?= session('qaToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>

            $("#shipPassed").click(function () {
                $('#TracingNumber').modal({
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                })
            });


            $('.accept').on('click', function (e) {
                e.preventDefault();
                $(this).attr('disabled', true);
                <?php if(!is_null(session('qaToken'))): ?>
                    tok = <?= session('qaToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                        {{-- if($(this).attr('stat', 'RI')){
                             $('.btn-display').html('<div><button  class="btn btn-info" customer="{{$order->customerId}}" orderNumber="{{@$order->orderNumber}}" order="{{$order->id}}" statId=11 stat="PI" id="inspection-passed">Pass</button><a href="#ModalFail" data-toggle="modal" class="btn btn-warning" id="inspection-failed" stat="RI">Fail</a></div>');
                                    }else {--}}
                let data = {
                        "id": $(this).attr('order'),
                        "orderNumber": $(this).attr('orderNumber'),
                        "status": $(this).attr('stat'),
                        "customerId": $(this).attr('customer'),
                        "statusId": $(this).attr('statId')
                    }
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click ok to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $(this).siblings('span').addClass('lds-dual-ring');
                            $.ajax({
                                url: "{{env('GET_BASE_URL')}}/fashion/secure/qa/order/updateorderitem",
                                type: "POST",
                                dataType: 'json',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                data: JSON.stringify(data),
                                success: function (result) {
                                    // console.log(result);
                                    $('.mySpinner').removeClass('lds-dual-ring');
                                    //  window.location.reload();
                                    $('.accept').attr('disabled', false);

                                    if (result.status == 0) {
                                        swal('Order Upddate successfully', '', 'success');
                                        $(this).attr('disabled', false);
                                        location.reload();
                                    } else {
                                        swal(result.message, '', 'error');
                                        $('.accept').attr('disabled', false);
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                    $('.accept').attr('disabled', false);

                                    console.log(e);
                                }
                            });
                        }
                    });
                console.log(data);

                /* }*/

            });

            $(document).on('click', '.passPass', function () {
                var customerId = $(this).attr('customer');
                var order = $(this).attr('orderNumber');
                var status = $(this).attr('stat');
                var statusId = $(this).attr('statId');
                var orderId = $(this).attr('order');

                $('.passUploadPicture').attr("order", orderId);
                $('.passUploadPicture').attr("customer", customerId);
                $('.passUploadPicture').attr("orderNumber", order);
                $('.passUploadPicture').attr("status", status);
                $('.passUploadPicture').attr('statusId', statusId);


            });

            $(document).on('click', '.shipPassed', function () {
                var customerId = $(this).attr('customer');
                var order = $(this).attr('orderNumber');
                var status = $(this).attr('stat');
                var statusId = $(this).attr('statId');
                var orderId = $(this).attr('order');


                $('#tracking').attr("order", orderId);
                $('#tracking').attr("customer", customerId);
                $('#tracking').attr("orderNumber", order);
                $('#tracking').attr("status", status);
                $('#tracking').attr('statusId', statusId);
            });

            $(document).on('click', '#inspection-failed', function () {
                orderNum = $(this).attr('ordernumer');
                var customer = $(this).attr('customer');
                var order = $(this).attr('order');
                $('.reason-btn').attr("orderNumber", orderNum);
                $('.reason-btn').attr("customer", customer);
                $('.reason-btn').attr("order", order);
            });

            $(document).on('click', '#inspection-passed', function () {
                <?php if(!is_null(session('qanToken'))): ?>
                    tok = <?= session('qaToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        "id": $(this).attr('order'),
                        "orderNumber": $(this).attr('orderNumber'),
                        "status": $(this).attr('stat'),
                        "customerId": $(this).attr('customer'),
                        "statusId": $(this).attr('statId')

                    }

                // console.log(data);

                $.ajax({
                    url: "{{env('GET_BASE_URL')}}{{env('QA_ORDER')}}updateorderitem",
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (result) {
                        // $('#loaderModal').modal('hide');
                        // window.location.reload();

                        // $('div.loader').hide();

                        /*  if(result.status == 0){
                              $.notify('status changed successfully', 'success');
                              location.reload();
                          }else{
                              $.notify(result.message);
                          }*/
                    },
                    error: function (e) {
                        $('div.loader').hide();
                        $('#loaderModal').modal('hide');

                        console.log(e);
                    }
                });
            });

            $(document).on('click', '.reason-btn', function () {
                $('#reasonBtn').val('Processing...').attr('disabled', 'disabled');
                <?php if(!is_null(session('qaToken'))): ?>
                    tok = <?= session('qaToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        "id": $(this).attr('order'),
                        "status": 'FI',
                        "statusId": 12,
                        "orderNumber": $(this).attr('orderNumber'),
                        "customerId": $(this).attr('customer'),
                        "action": $('#reason-text').val()
                    }
                //console.log(data);
                $.ajax({
                    url: "{{env('GET_BASE_URL')}}{{env('QA_ORDER')}}updateorderitem",
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.status === "00") {
                            $('#reasonBtn').val('Processed').attr('disabled', 'disabled');
                            swal('Message Sent Successfully', '', 'success');
                            window.location.href = "{{route('qa.orders.all')}}"
                        }
                        else {
                            $('#reasonBtn').val('Submit Reason').removeAttr('disabled', 'disabled');
                            swal('Something went wrong. Try Again', '', 'error');
                        }
                    },
                    error: function (e) {
                        $('div.loader').hide();
                        $('#loaderModal').modal('hide');
                        swal(e, '','error');
                    }
                });
            });

            $(document).on('click', '.passUploadPicture', function () {
                <?php if(!is_null(session('qaToken'))): ?>
                    tok = <?= session('qaToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>

                var data = {
                        "id": $('.passUploadPicture').attr('order'),
                        "orderNumber": $('.passUploadPicture').attr('orderNumber'),
                        "status": $('.passUploadPicture').attr('status'),
                        "customerId": $('.passUploadPicture').attr('customer'),
                        "statusId": $('.passUploadPicture').attr('statusid'),
                        "productPicture": $('.designerProduct').attr('src')
                    };
                console.log(data);

                $.ajax({
                    url: '{{env('GET_BASE_URL')}}{{env('QA_ORDER')}}updateorderitem',
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (res) {
                        console.log(res);
                        if (res.status === '00') {
                            $('#loaderModal').hide();
                            swal("Order Updated Successfully", "", "success");
                            window.location.reload();
                        }
                    },
                    error: function (e) {
                        $('#loaderModal').hide();
                        $('#done').removeAttr("disabled", "disabled");
                        swal(e, '','error');
                    }
                })

            });

            $(document).on('click', '#tracking', function () {
                $('#tracking').val('Processing...').attr('disabled', 'disabled');
                <?php if(!is_null(session('qaToken'))): ?>
                    tok = <?= session('qaToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var data = {
                        id: $(this).attr('order'),
                        trackingNumber: $('#trackingNumberValue').val()
                    };
                // console.log(data);
                $.ajax({
                    url: "{{env('GET_BASE_URL')}}{{env('QA_ORDER')}}updatetrackingnumber",
                    type: "POST",
                    dataType: 'json',
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (result) {
                         console.log(result);
                        if (result.status === '00') {
                            var datas = {
                                "id": $('#tracking').attr('order'),
                                "orderNumber": $('#tracking').attr('orderNumber'),
                                "status": $('#tracking').attr('status'),
                                "customerId": $('#tracking').attr('customer'),
                                "statusId": $('#tracking').attr('statusid')
                            };

                            $.ajax({
                                url: '{{env('GET_BASE_URL')}}{{env('QA_ORDER')}}updateorderitem',
                                type: "POST",
                                dataType: 'json',
                                headers: {
                                    'Authorization': tok,
                                    'Content-Type': 'application/json; charset=utf-8',
                                    'accept': 'application/json'
                                },
                                data: JSON.stringify(datas),
                                success: function (res) {
                                    console.log(res);
                                    if (res.status === '00') {
                                        $('#loaderModal').hide();
                                        swal("Order Updated Successfully", "", "success");
                                        window.location.reload();
                                    }
                                },
                                error: function (e) {
                                    swal(e, '', 'error');
                                    $('#tracking').val('Submit').removeAttr('disabled', 'disabled');
                                    $('#loaderModal').hide();
                                    $('#done').removeAttr("disabled", "disabled");
                                    swal(e, '','error');
                                }
                            })
                        }
                        else {
                        }
                    },
                    error: function (e) {
                        $('#tracking').val('Submit').removeAttr('disabled', 'disabled');
                        $('div.loader').hide();
                        swal(e, '','error');
                        $('#loaderModal').modal('hide');

                        console.log(e);
                    }
                })

            });

        });

        $('.addSampleProduct').on('change', function () {
            var file = $(this).get(0).files[0];
            if (file.size / 1024 > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                convertMoreToBase64(this, '.designerProduct');
                $(this).siblings('img').addClass('uploadedImage');
            }
        });

    </script>

@endpush