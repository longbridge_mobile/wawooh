@extends('layouts.qa')

@push('styles')

@endpush

@section('pageTitle','All Deliverd Orders')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Delivered Orders</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-home"></i> Order</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> Delivered order</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">Delivered Orders: <label class="badge badge-danger">{{count($allOrders)}}</label></h3>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation">
                        <a href="#pending" aria-controls="pending" role="tab" data-toggle="tab">Delivered <label
                                    class="badge badge-primary"></label></a>
                    </li>
                </ul>

                <div class="tab-content m-top-2">
                    <div role="tabpanel" class="tab-pane active" id="pending">
                        <div class="all-order row">
                            <div class="table-responsive">
                                <table class="table table-striped" id="pendingOrders">
                                    <thead>
                                    <tr>
                                        <th scope="col">Order No</th>
                                        <th scope="col">Customer Name</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Time</th>
                                        <th scope="col">Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($allOrders))
                                        @if(count($allOrders))
                                            @foreach($allOrders as $order)
                                                @if($order->deliveryStatus === 'P')
                                                    <a href='{{route('qa.order-list-details', $order->id)}}'>
                                                        <tr class='clickable-row'>
                                                            <td>{{$order->orderNumber}}</td>
                                                            <td>{{$order->customerName}}</td>
                                                            <td>{{$order->deliveryPhoneNumber}}</td>
                                                            <td>₦{{number_format($order->paidAmount)}}</td>
                                                            <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}
                                                                <br>
                                                                <small class="text-success text-centerC">
                                                                    ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                    )
                                                                </small>
                                                            </td>
                                                            <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($order->orderDate))->format('h:m:i A')}}</td>
                                                            <td>
                                                                @if($order->deliveryStatus == 'PC')
                                                                    <label class="label label-completed">Payment
                                                                        Confirmed</label>
                                                                @elseif($order->deliveryStatus == 'P')
                                                                    <label class="label label-pending">Pending</label>
                                                                @elseif($order->deliveryStatus == 'OP')
                                                                    <label class="label label-processing">Order
                                                                        Processing</label>
                                                                @elseif($order->deliveryStatus == 'OR')
                                                                    <label class="label label-rejected">Order
                                                                        Rejected</label>
                                                                @elseif($order->deliveryStatus == 'CO')
                                                                    <label class="label label-completed">Completed by
                                                                        designer</label>
                                                                @elseif($order->deliveryStatus == 'RI')
                                                                    <label class="label label-inspection">Ready for
                                                                        Inspection</label>
                                                                @elseif($order->deliveryStatus == 'RS')
                                                                    <label class="label label-processing">Ready for
                                                                        Shipping</label>
                                                                @elseif($order->deliveryStatus == 'OS')
                                                                    <label class="label label-completed">Order
                                                                        Shipped</label>
                                                                @elseif($order->deliveryStatus == 'NV')
                                                                    <label class="label label-rejected">Not
                                                                        Verified</label>
                                                                @elseif($order->deliveryStatus == 'OD')
                                                                    <label class="label label-completed">Order
                                                                        Delivered</label>
                                                                @endif
                                                            </td>
                                                            <td>
                                                                <a href='{{route('qa.order-list-details', $order->id)}}'
                                                                   style="color: grey;" class="">View Order
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </a>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection

@push('scripts')

    <script>
        $(document).ready(function () {
            <?php if(!is_null(session('qaToken'))): ?>
                tok = <?= session('qaToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>
            console.log(tok);
        })
    </script>

    <script>

        $(document).ready(function () {
            $('#delIncompleteOrder').on('click', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var id = $(this).attr('del');
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click yes to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                }).then(confirm => {
                    if (confirm) {
                        $('#loaderModal').modal();
                        $.ajax({
                            url: `{{env('GET_BASE_URL')}}/fashion/secure/admin/order/${id}/deleteorder`,
                            headers: {
                                'Authorization': tok,
                                'Content-Type': 'application/json; charset=utf-8',
                                'accept': 'application/json'
                            },
                            dataType: 'json',
                            success: function (result) {
                                console.log(result);
                                if (result.status == 0) {
                                    $('#loaderModal').modal('hide');
                                    $(this).closest('.clickable-row').hide();
                                    // console.log($(this).closest('.clickable-row').hide());
                                    swal('Successfully Deleted', '', 'success');
                                    location.reload();
                                    //$(this).closest('.clickable-row').hide();
                                    $('#incompleted').addClass('active');
                                }
                            }
                        });
                    }
                })
            });
            $('.accept').on('click', function () {
                var data = {
                    "id": $(this).attr('order'),
                    "orderNumber": $(this).attr('orderNumber'),
                    "deliveryStatus": $(this).attr('stat'),
                    "userId": $(this).attr('customer'),
                    "paidAmount": $(this).attr('payment')
                };
                console.log(data);
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click yes to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $('#loaderModal').modal();
                            $.ajax({
                                url: '{{route('admin.comfirmPayments')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (result) {
                                    console.log(result);
                                    $('div.loader').hide();
                                    if (result.status == 0) {
                                        $('#loaderModal').modal('hide');
                                        swal('Order Confirmed Successfully', '', 'success');
                                        location.reload();
                                    } else if (result.status == 56) {
                                        swal({
                                            title: "Cannot confirm Order",
                                            text: result.data,
                                            icon: "warning",
                                            buttons: ["No", "Yes"],
                                            dangerMode: true,
                                        })
                                        location.reload();
                                    } else {
                                        $('#loaderModal').modal('hide');
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                    swal(e, '', 'error');
                                }
                            });
                        }
                    })
            });
        });

    </script>

@endpush