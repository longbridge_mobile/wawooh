@extends('layouts.qa')

@push('styles')

@endpush

@section('pageTitle','Order Details')
@section('content')
    <div class="content-wrapper">

        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">Orders Details</h3>
                <div class="all-order">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Total number of entries</p>
                                </div>
                                <div class="col-md-5">
                                    <button class="null-btn">{{count($order->itemsList)}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="giveMarginTen">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Order status:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>
                                            @if(isset($order->itemsList))
                                                @if(count($order->itemsList))
                                                    @foreach($order->itemsList as $list)
                                                        @if($list->status == 'PC')
                                                            <label class="label label-completed">Payment
                                                                Confirmed</label>
                                                        @elseif($list->status == 'P')
                                                            <label class="label label-pending">Pending</label>
                                                        @elseif($list->status == 'C')
                                                            <label class="label label-rejected">Cancelled</label>
                                                        @elseif($list->status == 'OP')
                                                            <label class="label label-processing">Order Processing</label>
                                                        @elseif($list->status == 'A')
                                                            <label class="label label-processing">Accepted</label>
                                                        @elseif($list->status == 'WR')
                                                            <label class="label label-rejected">Wawooh Rejected</label>
                                                        @elseif($list->status == 'WC')
                                                            <label class="label label-completed">Wawooh Collected</label>
                                                        @elseif($list->status == 'CO')
                                                            <label class="label label-completed">Completed</label>
                                                        @elseif($list->status == 'RS')
                                                            <label class="label label-processing">Ready for
                                                                Shipping</label>
                                                        @elseif($list->status == 'RI')
                                                            <label class="label label-inspection">Ready for
                                                                Inspection</label>
                                                        @elseif($list->status == 'OS')
                                                            <label class="label label-completed">Order Shipped</label>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Delivery Address:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>{{title_case($order->deliveryAddress)}}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Method of Payment:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>{{title_case(str_replace('_',' ',$order->paymentType))}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Customer:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>@if(isset($order->customerName)) {{$order->customerName}} @endif</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Total Amount:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>₦{{number_format($order->totalAmount)}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Paid Amount:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>₦{{number_format($order->paidAmount)}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Order Number:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        {{$order->orderNumber}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Customer Phone Number:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        {{$order->customerPhoneNumber}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Delivery Phone Number:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        {{$order->deliveryPhoneNumber}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Delivery Address:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <p>{{title_case($order->deliveryAddress) . '. ' . $order->deliveryCountry}}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <p><span class="makeBold brand-color">Delivery Type:</span></p>
                                    </div>
                                    <div class="col-md-8">
                                        <label class = "label label-{{$order->deliveryAddress === null ? 'processing':'inspection'}}">
                                            {{$order->deliveryAddress === null ? 'Pick Up':'Standard Delivery'}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                @if(isset($order->itemsList))
                                    @foreach($order->itemsList as $orderItem)
                                        <div class="row">
                                            <div class="col-md-12" style="padding: 0; margin-bottom: 10px;">
                                                <img src="{{str_replace('http','https',$orderItem->productPicture)}}" height="200" style="border: 1px solid rgba(0,0,0,.2);">
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p><span class="makeBold brand-color">Product Size:</span></p>
                                                </div>
                                                <div class="col-md-8">
                                                    {{$orderItem->size}}
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p><span class="makeBold brand-color">Designer:</span></p>
                                                </div>
                                                <div class="col-md-8">

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p><span class="makeBold brand-color">Time of Order:</span></p>
                                                </div>
                                                <div class="col-md-8">
                                                    {{\Carbon\Carbon::createFromTimeStamp(strtotime($orderItem->orderDate))->format('h:m:i A')}}
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <p><span class="makeBold brand-color">Designer Status:</span>
                                                    </p>
                                                </div>
                                                @if($orderItem->status !== 'P')
                                                    <div class="col-md-8">
                                                        <label class="label label-processing">Accepted by
                                                            Designer</label>
                                                    </div>
                                                    @else
                                                    <div class="col-md-8">
                                                        <label class="label label-pending">Waiting for Designer to Accept</label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Order No</th>
                                <th scope="col">Product</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($order->itemsList))
                                @if(count($order->itemsList))
                                    @foreach($order->itemsList as $item)
                                        <tr class='clickable-row' data-href='order-details.html'>
                                            <td>{{$item->orderNumber}}</td>
                                            <td>{{$item->productName}}</td>
                                            <td>{{$item->quantity}}</td>
                                            <td>₦{{number_format($item->amount)}}</td>
                                            <td>{{\Carbon\Carbon::parse($item->orderDate)->format('d-M-Y')}} <br>
                                                <small>
                                                    ({{\Carbon\Carbon::createFromTimeStamp(strtotime($orderItem->orderDate))->diffForHumans()}}
                                                    )
                                                </small>
                                            </td>
                                            <td>
                                                @if($item->status == 'PC')
                                                    <label class="label label-completed">Payment Confirmed</label>
                                                @elseif($item->status == 'P')
                                                    <label class="label label-pending">Pending</label>
                                                @elseif($item->status == 'OP')
                                                    <label class="label label-processing">Order Processing</label>
                                                @elseif($item->status == 'OR')
                                                    <label class="label label-rejected">Order Rejected</label>
                                                @elseif($item->status == 'CO')
                                                    <label class="label label-completed">Completed by designer</label>
                                                @elseif($item->status == 'RI')
                                                    <label class="label label-inspection">Ready for Inspection</label>
                                                @elseif($item->status == 'RS')
                                                    <label class="label label-processing">Ready for Shipping</label>
                                                @elseif($item->status == 'OS')
                                                    <label class="label label-completed">Order Shipped</label>
                                                @elseif($item->status == 'NV')
                                                    <label class="label label-rejected">Not Verified</label>
                                                @elseif($item->status == 'D')
                                                    <label class="label label-completed">Order Delivered</label>
                                                @elseif($item->status == 'C')
                                                    <label class="label label-rejected">Order Cancelled</label>
                                                @elseif($item->status == 'WR')
                                                    <label class="label label-rejected">Wawooh Rejected</label>
                                                @elseif($item->status == 'WC')
                                                    <label class="label label-completed">Wawooh Collected</label>
                                                @elseif($item->status == 'A')
                                                    <label class="label label-processing">Accepted</label>
                                                @elseif($item->status == 'PC')
                                                    <label class="label label-completed">Payment Confirmed</label>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endif

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="modal fade" data-backdrop="static" id="ModalFail" tabindex="-1" role="dialog"
                 aria-labelledby="ModalFail" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="ModalFail">
                                <h5>Reason</h5>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="reason-text">
                                </div>
                                <div>
                                    {{--{{dd($order)}}--}}
                                    <input type="submit" class="btn btn-primary reason-btn"
                                           customer="{{$order->userId}}" orderNumber="{{@$order->orderNumber}}"
                                           order="{{$order->id}}">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="margin-top: 5em;" class="modal fade" data-backdrop="static" id="ModalComplain" tabindex="-1"
                 role="dialog"
                 aria-labelledby="ModalComplain" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="ModalFail">

                                @if(isset($order->itemsList))
                                    @if(count($order->itemsList))
                                        @foreach($order->itemsList as $item)
                                            <div class="productComplaint">
                                                <h5>Complains for <strong>{{$item->productName}}</strong></h5>
                                                @if(isset($item->userComplain))
                                                    <span>{{$item->userComplain}}</span>
                                                @else
                                                    <p>No Complaint found!</p>
                                                @endif
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    </div>

@endsection

@push('scripts')


@endpush