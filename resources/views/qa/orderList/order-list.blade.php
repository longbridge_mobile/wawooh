@extends('layouts.qa.default')

@push('styles')
    <style>
        .f-14 {
            font-size: 14px;
        }
    </style>
@endpush

@section('pageTitle','All Orders')
@section('content')

    <div class="pcoded-content">
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="feather icon-user bg-c-blue"></i>
                        <div class="d-inline">
                            <h5>All Orders: <span class="badge badge-primary">{{count($allOrders)}}</span></h5>
                            <span>Orders ready for QA action</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{route('qa.dashboard')}}"><i class="feather icon-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">All Orders</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="pcoded-inner-content">
            <div class="main-body">
                <div class="page-wrapper">
                    <div class="page-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="allproducts-tab" data-toggle="tab"
                                           href="#all" role="tab" aria-controls="home" aria-selected="true">All
                                            Orders</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="verified-tab" data-toggle="tab" href="#pending"
                                           role="tab" aria-controls="profile" aria-selected="false">Pending</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="unverified-tab" data-toggle="tab" href="#active"
                                           role="tab" aria-controls="contact" aria-selected="false">Active</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="all" role="tabpanel"
                                         aria-labelledby="home-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>All Orders</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped table-bordered nowrap table-hover f-14" id="allOrderTable">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Order No</th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Total</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Status</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(isset($allOrders))
                                                            @if(count($allOrders))
                                                                @foreach($allOrders as $order)
                                                                    <a href='{{route('qa.order-list-details', $order->id)}}'>
                                                                        <tr>
                                                                            <td>{{$order->orderNumber}}</td>
                                                                            <td>{{$order->customerName}}</td>
                                                                            <td>₦{{number_format($order->totalAmount)}}</td>
                                                                            <td>{{\Carbon\Carbon::parse($order->orderDate)->format('Y-m-d')}}
                                                                                <br>
                                                                                <small class="text-success text-centerC">
                                                                                    ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                                    )
                                                                                </small>
                                                                            </td>
                                                                            <td>
                                                                                @if($order->deliveryStatus == 'PC')
                                                                                    <label class="label label-completed">Payment
                                                                                        Confirmed</label>
                                                                                @elseif($order->deliveryStatus == 'P')
                                                                                    <label class="label label-pending">Pending</label>
                                                                                @elseif($order->deliveryStatus == 'OP')
                                                                                    <label class="label label-processing">Order
                                                                                        Processing</label>
                                                                                @elseif($order->deliveryStatus == 'OR')
                                                                                    <label class="label label-rejected">Order
                                                                                        Rejected</label>
                                                                                @elseif($order->deliveryStatus == 'CO')
                                                                                    <label class="label label-completed">Completed by
                                                                                        designer</label>
                                                                                @elseif($order->deliveryStatus == 'RI')
                                                                                    <label class="label label-inspection">Ready for
                                                                                        Inspection</label>
                                                                                @elseif($order->deliveryStatus == 'RS')
                                                                                    <label class="label label-processing">Ready for
                                                                                        Shipping</label>
                                                                                @elseif($order->deliveryStatus == 'OS')
                                                                                    <label class="label label-completed">Order
                                                                                        Shipped</label>
                                                                                @elseif($order->deliveryStatus == 'NV')
                                                                                    <label class="label label-rejected">Not
                                                                                        Verified</label>
                                                                                @elseif($order->deliveryStatus == 'OD')
                                                                                    <label class="label label-completed">Order
                                                                                        Delivered</label>
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                <a href='{{route('qa.order-list-details', $order->id)}}'
                                                                                   style="color: grey;" class="">View Order
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </a>
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @if(isset($totalOrder))
                                                    <div>
                                                        {!! $totalOrder->render() !!}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pending" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Pending Orders</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table class="table table-striped table-bordered nowrap f-14" id="pendingOrders">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Order No</th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Phone</th>
                                                            <th scope="col">Total</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Time</th>
                                                            <th scope="col">Status</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(isset($allOrders))
                                                            @if(count($allOrders))
                                                                @foreach($allOrders as $order)
                                                                    @if($order->deliveryStatus === 'P')
                                                                        <a href='{{route('qa.order-list-details', $order->id)}}'>
                                                                            <tr>
                                                                                <td>{{$order->orderNumber}}</td>
                                                                                <td>{{$order->customerName}}</td>
                                                                                <td>{{$order->deliveryPhoneNumber}}</td>
                                                                                <td>₦{{number_format($order->paidAmount)}}</td>
                                                                                <td>{{\Carbon\Carbon::parse($order->orderDate)->format('Y-m-d')}}
                                                                                    <br>
                                                                                    <small class="text-success text-centerC">
                                                                                        ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                                        )
                                                                                    </small>
                                                                                </td>
                                                                                <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($order->orderDate))->format('h:m:i A')}}</td>
                                                                                <td>
                                                                                    @if($order->deliveryStatus == 'PC')
                                                                                        <label class="label label-completed">Payment
                                                                                            Confirmed</label>
                                                                                    @elseif($order->deliveryStatus == 'P')
                                                                                        <label class="label label-pending">Pending</label>
                                                                                    @elseif($order->deliveryStatus == 'OP')
                                                                                        <label class="label label-processing">Order
                                                                                            Processing</label>
                                                                                    @elseif($order->deliveryStatus == 'OR')
                                                                                        <label class="label label-rejected">Order
                                                                                            Rejected</label>
                                                                                    @elseif($order->deliveryStatus == 'CO')
                                                                                        <label class="label label-completed">Completed by
                                                                                            designer</label>
                                                                                    @elseif($order->deliveryStatus == 'RI')
                                                                                        <label class="label label-inspection">Ready for
                                                                                            Inspection</label>
                                                                                    @elseif($order->deliveryStatus == 'RS')
                                                                                        <label class="label label-processing">Ready for
                                                                                            Shipping</label>
                                                                                    @elseif($order->deliveryStatus == 'OS')
                                                                                        <label class="label label-completed">Order
                                                                                            Shipped</label>
                                                                                    @elseif($order->deliveryStatus == 'NV')
                                                                                        <label class="label label-rejected">Not
                                                                                            Verified</label>
                                                                                    @elseif($order->deliveryStatus == 'OD')
                                                                                        <label class="label label-completed">Order
                                                                                            Delivered</label>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    <a href='{{route('qa.order-list-details', $order->id)}}'
                                                                                       style="color: grey;" class="">View Order
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </a>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="active" role="tabpanel"
                                         aria-labelledby="contact-tab">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Active Orders</h5>
                                            </div>
                                            <div class="card-block">
                                                <div class="dt-responsive table-responsive">
                                                    <table id="unverified-table"
                                                           class="table table-striped table-bordered nowrap f-14">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Order</th>
                                                            <th scope="col">Name</th>
                                                            <th scope="col">Phone</th>
                                                            <th scope="col">Amount</th>
                                                            <th scope="col">Date</th>
                                                            <th scope="col">Status</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(isset($allOrders))
                                                            @if(count($allOrders))
                                                                @foreach($allOrders as $order)
                                                                    @if($order->deliveryStatus === 'PC' || $order->deliveryStatus === 'OP' || $order->deliveryStatus === 'OR' || $order->deliveryStatus === 'CO' || $order->deliveryStatus === 'RS')

                                                                        <a href='{{route('qa.order-list-details', $order->id)}}'>
                                                                            <tr>
                                                                                <td>{{$order->orderNumber}}</td>
                                                                                <td>{{$order->customerName}}</td>
                                                                                <td>{{$order->deliveryPhoneNumber}}</td>
                                                                                <td>₦{{number_format($order->paidAmount)}}</td>
                                                                                <td>{{\Carbon\Carbon::parse($order->orderDate)->format('Y-m-d')}}</td>
                                                                                <td>
                                                                                    @if($order->deliveryStatus == 'PC')
                                                                                        <label class="label label-completed">Payment
                                                                                            Confirmed</label>
                                                                                    @elseif($order->deliveryStatus == 'P')
                                                                                        <label class="label label-pending">Pending</label>
                                                                                    @elseif($order->deliveryStatus == 'OP')
                                                                                        <label class="label label-processing">Order
                                                                                            Processing</label>
                                                                                    @elseif($order->deliveryStatus == 'OR')
                                                                                        <label class="label label-rejected">Order
                                                                                            Rejected</label>
                                                                                    @elseif($order->deliveryStatus == 'CO')
                                                                                        <label class="label label-completed">Completed by
                                                                                            designer</label>
                                                                                    @elseif($order->deliveryStatus == 'RI')
                                                                                        <label class="label label-inspection">Ready for
                                                                                            Inspection</label>
                                                                                    @elseif($order->deliveryStatus == 'RS')
                                                                                        <label class="label label-processing">Ready for
                                                                                            Shipping</label>
                                                                                    @elseif($order->deliveryStatus == 'OS')
                                                                                        <label class="label label-completed">Order
                                                                                            Shipped</label>
                                                                                    @elseif($order->deliveryStatus == 'NV')
                                                                                        <label class="label label-rejected">Not
                                                                                            Verified</label>
                                                                                    @elseif($order->deliveryStatus == 'OD')
                                                                                        <label class="label label-completed">Order
                                                                                            Delivered</label>
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    <a href='{{route('qa.order-list-details', $order->id)}}'
                                                                                       style="color: grey;" class="">View Order
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        </a>
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="styleSelector">
            </div>
        </div>
    </div>

@endsection

@push('scripts')


@endpush