<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice For: {{isset($invoice->orderNumber) ? $invoice->orderNumber : ''}} | Wawooh &mdash; Express
        yourself</title>

{{--    <link rel="stylesheet" href="{{asset('css/bootstrap/bootstrap.min.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/invoice.css')}}">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <!-- Title Icon -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    <style type="text/css">
        .toUpper {
            text-transform: capitalize;
        }
    </style>
</head>
<body>
<div class="container">
    @if(isset($invoice))
        <div class="invoice-container">
            <div class="rowHolder">
                <div class="Col7">
                    <img src="{{asset('img/logo.png')}}" class="img-invoice" alt="imga">
                    <h3>sales invoice</h3>
                </div>
                <div class="Col5">
                    <div class="order-details">
                        <h4>Skywater Tower 6, Otunba Oshikoya drive, Mobolaji Bank Anthony way, Ikeja. Lagos.
                            Nigeria</h4>

                        <table class="table">
                            <tr>
                                <td style="vertical-align: middle">Order No</td>
                                <td><span class="orderNum">{{$invoice->orderNumber}}</span></td>
                            </tr>
                            <tr>
                                <td>Order Date</td>
                                <td>{{\Carbon\Carbon::createFromTimestamp($invoice->orderDate / 1000)->format('d-M-Y')}}</td>
                            </tr>
                            <tr>
                                <td>Deliver To:</td>
                                <td>{{$invoice->lastName}} {{$invoice->firstName}}</td>
                            </tr>
                            <tr>
                                <td>Delivery Method</td>
                                <td>{{str_replace('_', ' ', $invoice->deliveryType)}}</td>
                            </tr>
                            <tr>
                                <td>Invoice No:</td>
                                <td>{{$invoice->orderNumber}}-{{invoiceRefID('3')}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="header">
                <div class="rowHolder">
                    <div class="Col6"><h5>Delivery details:</h5></div>
                    <div class="Col6"><h5>Delivery address</h5></div>
                </div>
            </div>
            <div class="rowHolder">
                <div class="Col6">
                    <table class="table">
                        <tr>
                            <td width="150px">Dispatch date:</td>
                            <td>{{\Carbon\Carbon::createFromTimestamp($invoice->orderDate / 1000)->format('d-M-Y')}}</td>
                        </tr>
                        <tr>
                            <td>Order type:</td>
                            <td>Ready made</td>
                        </tr>
                        <tr>
                            <td>Amount Paid</td>
                            <td>{{env('NAIRA_SIGN')}}{{number_format($invoice->paidAmount)}}</td>
                        </tr>
                        <tr>
                            <td>Return Tracking ID:</td>
                            <td>RTW-{{$invoice->orderNumber}}</td>
                        </tr>
                    </table>
                </div>
                <div class="Col6">
                    <table class="table">
                        <tr>
                            <td width="100px">Address</td>
                            <td style="vertical-align: middle;">{{$invoice->deliveryAddress}}</td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>{{$invoice->city}}</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>{{$invoice->country}}</td>
                        </tr>
                        <tr>
                            <td>Contact:</td>
                            <td>{{$invoice->phoneNumber}}</td>
                        </tr>
                    </table>
                </div>
            </div>

            <h5 class="d-title">Order details</h5>
            <div class="orderItemDetails">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Item No.</th>
                        <th>Item Name</th>
                        <th>Qty</th>
                        <th>Item Price</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($invoice->item))
                        @if(count($invoice->item))
                            @foreach($invoice->item as $item)
                                <tr style="text-align: center;">
                                    <td>{{$loop->iteration}}</td>
                                    <td><span class="toUpper">{{strtolower($item->productName)}}</span></td>
                                    <td>{{$item->quantity}}</td>
                                    <td>{{env('NAIRA_SIGN')}}{{getQtyPrice($item->amount ,$item->quantity)}}</td>
                                    <td>{{env('NAIRA_SIGN')}}{{number_format($item->amount)}}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="rowHolder" style="justify-content: flex-end">
                <div class="Col5">
                    <table class="table underlineTable">
                        <tr>
                            <td width="250px">Promo Code Value</td>
                            <td>
                                @foreach($invoice->item as $item)
                                    {{$item->promoCode}}
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Discount</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td>Subtotal Less Discount</td>
                            <td>N/A</td>
                        </tr>
                        <tr>
                            <td>Shipping</td>
                            <td>{{env('NAIRA_SIGN')}}{{number_format($invoice->shippingAmount)}}</td>
                        </tr>
                        <tr>
                            <td>Total Paid</td>
                            <td><strong>{{env('NAIRA_SIGN')}}{{number_format($invoice->paidAmount)}}</strong></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="rowHolder">
                <div class="Col12">
                    <div class="content-user">
                        <h4>Dear {{$invoice->lastName}} {{$invoice->firstName}},</h4>

                        <p>Thank you for shopping with us. We are delighted to have you as a customer</p>
                        <p>
                            Please note, we aim to deliver your items as fast as possible and some of your order may
                            come in
                            multiple deliveries.
                            <br>
                            If there is a problem with your product. Please call 0700WAWOOH (0700929664) or visit
                            https://www.wawooh.com
                            <br>
                            Thank you again for shopping with us.

                            <br><br>
                            Sincerely. <br>
                            Wawooh Team.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="invoice-container">
            <div class="rowHolder">
                <div class="Col5"><a target="_blank" href="{{route('qaGetDeliveryNote', $invoice->orderNumber)}}" class="btn-next">Print Delivery Note</a></div>
                <div class="Col5"><a onclick="window.print()" href="#" class="btn-next">Print</a></div>
            </div>
        </div>
    @else
        <div class="invoice-container" style="text-align: center;">
            <div class="rowHolder">
                <div class="Col6 offset-3">
                    <div class="alert alert-warning"><strong>Invoice Not Available</strong></div>
                </div>
            </div>
        </div>
    @endif
</div>
</body>
</html>