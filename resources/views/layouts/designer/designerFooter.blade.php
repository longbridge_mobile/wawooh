<footer class="footer">
    <div class="container-fluid clearfix">
       <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">
                         &copy; <span id="year"></span> <a href="">Wawooh HQ</a>. All rights reserved.</span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- row-offcanvas ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- Start of LiveChat (www.livechatinc.com) code -->
{{--<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 10183982;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<noscript>
    <a href="https://www.livechatinc.com/chat-with/10183982/">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
</noscript>--}}
<!-- End of LiveChat code -->
@include('scripts.designer.allScript')

<script>
    $(document).ready(function () {
        $.noConflict();
        $('#desOrderTable, #conPendingOrders, #canOrders, #desOrderActive').DataTable();
    })
</script>
</body>
</html>