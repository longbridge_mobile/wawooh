<nav class="sidebar sidebar-offcanvas sideFix" id="sidebar">

    <style>
        /*summary::-webkit-details-marker {
            display: none
        }

        summary:before {
            float: left;
            height: 20px;
            width: 20px;
            content: "+";
        }

        details[open] summary:after {
            background: url(other-picture);
        }*/

    </style>
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    @if(isset($designer->logo))
                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'], @$designer->logo)}}"
                             class="img-fluid">
                        <span class="online-status online"></span>
                    @else
                        <img src="{{ asset('img/logo-sm.png') }}" class="img-fluid">
                @endif
                <!--change class online to offline or busy as needed-->
                </div>
                <div class="profile-name">
                    <p class="name">
                        {{@$designer->storeName}}
                    </p>
                    <p class="designation">
                        {{@$designer->firstName}} {{@$designer->lastName}}
                    </p>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('designer.dashboard') }}">
                <i class="icon-home menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ route('designer.profile') }}">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">My Profile</span>
                <span class="badge badge-danger"></span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('designer.updateprofile') }}">
                <i class="icon-pencil menu-icon"></i>
                <span class="menu-title">Update Profile</span>
            </a>
        </li>
        @if(isset($storeName))
            <li class="nav-item">
                <a class="nav-link" href="{{  route('designer.orders', generateCorrectUrl(strtolower($storeName))) }}">
                    <i class="icon-support menu-icon"></i>
                    <span class="menu-title">My Orders</span>
                    <span class="badge badge-primary"></span>
                </a>
            </li>
        @endif

        {{-- @if(isset($storeName))
             <li class="nav-item">
                 <a class="nav-link"
                    href="{{  route('designerBespokeRequest', generateCorrectUrl(strtolower($storeName))) }}">
                     <i class="icon-film  menu-icon"></i>
                     <span class="menu-title">Bespoke Requests</span>
                     --}}{{--<span class="badge badge-success animated swing infinite"><i class="fa fa-envelope-o"></i></span>--}}{{--
                 </a>
             </li>
         @endif--}}
        @if(isset($storeName))
            {{-- <li class="nav-item">
                 <a class="nav-link"
                    href="{{  route('addBespokeProduct', generateCorrectUrl(strtolower($storeName))) }}">
                     <i class="icon-film  menu-icon"></i>
                     <span class="menu-title">Add Bespoke Product</span>
                     --}}{{--<span class="badge badge-success animated swing infinite"><i class="fa fa-envelope-o"></i></span>--}}{{--
                 </a>
             </li>--}}
            <li class="nav-item nav-details">
                <details class=nav-item">
                    <summary class="nav-link">Bespoke</summary>
                    <a class="nav-link"
                       href="{{  route('designerBespokeRequest', generateCorrectUrl(strtolower($storeName))) }}">
                        <i class="icon-scissor  menu-icon"></i>
                        <span class="menu-title">Bespoke Requests</span>
                        {{--<span class="badge badge-success animated swing infinite"><i class="fa fa-envelope-o"></i></span>--}}
                    </a>
                    <a class="nav-link"
                       href="{{  route('addBespokeProduct', generateCorrectUrl(strtolower($storeName))) }}">
                        <i class="icon-scissor  menu-icon"></i>
                        <span class="menu-title">Add Bespoke Product</span>
                        {{--<span class="badge badge-success animated swing infinite"><i class="fa fa-envelope-o"></i></span>--}}
                    </a>
                    <a class="nav-link"
                       href="{{  route('getBespokeProduct', generateCorrectUrl(strtolower($storeName))) }}">
                        <i class="icon-film  menu-icon"></i>
                        <span class="menu-title">Bespoke Products</span>
                        {{--<span class="badge badge-success animated swing infinite"><i class="fa fa-envelope-o"></i></span>--}}
                    </a>
                    <a class="nav-link"
                        href="{{route('getUserUploadStylesForDesigner', generateCorrectUrl(strtolower($storeName)))}}">
                        <i class="icon-film menu-icon"></i>
                        <span class="menu-title">User Uploaded Styles</span>
                    </a>

                </details>
            </li>
        @endif
        @if(isset($storeName))
            <li class="nav-item">
                <a class="nav-link"
                   href="{!! route('designer.product', generateCorrectUrl(strtolower($storeName))) !!}">
                    <i class="icon-briefcase menu-icon"></i>
                    <span class="menu-title">My Products</span>
                    <span class="badge badge-info"></span>
                </a>
            </li>
        @endif
        @if(isset($storeName))
            <li class="nav-item">
                <a class="nav-link"
                   href="{!! route('designer.uploadImage', generateCorrectUrl(strtolower($storeName))) !!}">
                    <i class="icon-briefcase menu-icon"></i>
                    <span class="menu-title">My Style Catalogue</span>
                    <span class="badge badge-info"></span>
                </a>
            </li>
        @endif
        @if(isset($storeName))
             <li class="nav-item">
                 <a class="nav-link" href="{!! route('designer.bespokeRequest', generateCorrectUrl(strtolower($storeName))) !!}">
                     <i class="icon-briefcase menu-icon"></i>
                     <span class="menu-title">Bespoke Request</span>
                     <span class="badge badge-info"></span>
                 </a>
             </li>
         @endif
        @if(isset($storeName))
            <li class="nav-item">
                <a href="{!! route('designer.availableManual', generateCorrectUrl(strtolower($storeName))) !!}" class="nav-link">
                    <i class="icon-briefcase menu-icon"></i>
                    <span class="menu-title">Available Manual</span>
                    <span class="badge badge-info"></span>
                </a>
            </li>
        @endif
       {{--     <li class="nav-item">
                <a class="nav-link"
                   href="{!! route('designer.bespokeRequest', generateCorrectUrl(strtolower($storeName))) !!}">
                    <i class="icon-briefcase menu-icon"></i>
                    <span class="menu-title">Bespoke Request</span>
                    <span class="badge badge-info"></span>
                </a>
            </li>
        @endif--}}
        {{--<li class="nav-item">
            <a class="nav-link" href="">
                <i class="icon-briefcase menu-icon"></i>
                <span class="menu-title">Discount Price</span>
                <span class="badge badge-info"></span>
            </a>
        </li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{ route('designer.accountSettings') }}">
                <i class="icon-settings menu-icon"></i>
                <span class="menu-title">Settings</span>
            </a>
        </li>
        {{--<li class="nav-item">
            <a class="nav-link" href="">
                <i class="icon-speech menu-icon"></i>
                <span class="menu-title">Mail Box</span>
            </a>
        </li>--}}
    </ul>
</nav>