<nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href=""><img src="{{ asset('img/logo.png') }}" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href=""><img src="{{ asset('img/logo-sm.png') }}" alt="logo"/></a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav">
            <li class="nav-item dropdown d-none d-lg-flex">
                @if(!empty($storeName))
                    <a class="nav-link dropdown-toggle nav-btn"
                       href="{{route('designer.product.new', str_slug(strtolower(@$storeName)))}}">
                        <span class="btn btn-wawooh">+ Add Product </span>
                    </a>
                @else
                    <div>Please update your profile to add product</div>
                @endif
            </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
            {{--<li class="nav-item dropdown">
                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#"
                   data-toggle="dropdown">
                    <i class="icon-bell mx-0"></i>
                    <span class="count"></span>
                </a>
            </li>--}}
            <li class="nav-item nav-settings d-lg-block">
                <a class="nav-link" href="{{ route('designer.logout') }}">
                    <i class="fa fa-power-off"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>