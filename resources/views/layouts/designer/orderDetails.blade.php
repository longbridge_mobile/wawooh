<div class="row">
    {{--<div class="col-md-12"><h4>Orders Details (total)</h4></div>--}}
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <div class="ml-3 orderDetails">
                        <p class="mb-0">Pending</p>
                        @if(isset($designer))
                            <h5>{{@$designer->noOfPendingOders}}</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-2 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <div class="ml-3 orderDetails">
                        <p class="mb-0">Active/Confirmed</p>
                        @if(isset($designer))
                            <h5>{{@$designer->noOfConfirmedOrders}}</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="col-md-6 col-lg-2 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <div class="ml-3 orderDetails">
                        <p class="mb-0">Completed</p>
                        <h6>0</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    {{--<div class="col-md-6 col-lg-2 grid-margin stretch-card">--}}
        {{--<div class="card">--}}
            {{--<div class="card-body">--}}
                {{--<div class="d-flex align-items-center justify-content-md-center">--}}
                    {{--<div class="ml-3 orderDetails">--}}
                        {{--<p class="mb-0">Ready to Ship</p>--}}
                        {{--@if(isset($designer))--}}
                            {{--<h5>{{@$designer->noOfReadyToShipOrders}}</h5>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-6 col-lg-2 grid-margin stretch-card">
        @if(isset($storeName))<a class="card-effect" href="{{route('designer.orders', strtolower(str_slug($storeName)))}}"> @endif
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center justify-content-md-center">
                        <div class="ml-3 orderDetails">
                            <p class="mb-0">Shipped</p>
                            @if(isset($designer))
                                <h5>{{@$designer->noOfShippedOrders}}</h5>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-md-6 col-lg-2 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <div class="ml-3 orderDetails">
                        <p class="mb-0">Cancelled</p>
                        @if(isset($designer))
                            <h5>{{@$designer->noOfCancelledOrders}}</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-3 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-md-center">
                    <div class="ml-3 orderDetails">
                        <p class="mb-0">Delivered</p>
                        @if(isset($designer))
                            <h5>{{@$designer->noOfDeliveredOrders}}</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>