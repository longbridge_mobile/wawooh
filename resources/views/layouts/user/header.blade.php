<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords"
          content="fashion, designers, clothes, cloth, shoe, accessories, ecommerce, events, image, showroom">
    <meta property="og:url" content="@if(url()->full() !== null){{url()->full()}} @else https://www.wawooh.com @endif"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="@yield('pageTitle') | Wawooh &mdash; Express you"/>
    <meta name="robots" content="index, follow">
    <meta name="google-signin-client_id"
          content="414255311001-n200l9pqdsb0osogtche5g8s4nu2jol5.apps.googleusercontent.com">
    <meta property="og:description"
          content="{{isset($desc) ? $desc : 'Beat the Clock Sale with our New Year Shopping Discount! Wawooh is Africa’s foremost online shopping haven for Quality Bespoke & Ready to Wear fashion pieces from Top African Fashion Designers at affordable prices.
Enjoy secured payment through our escrow payment feature, on time delivery, premium brands at the best market price in Africa.
Female fashion, Male fashion and Kids fashion. Get all you need to be fashionably expressive on wawooh.com'}}
                  "/>
    <meta property="description"
          content="{{isset($desc) ? $desc : 'Beat the Clock Sale with our New Year Shopping Discount! Wawooh is Africa’s foremost online shopping haven for Quality Bespoke & Ready to Wear fashion pieces from Top African Fashion Designers at affordable prices.
Enjoy secured payment through our escrow payment feature, on time delivery, premium brands at the best market price in Africa.
Female fashion, Male fashion and Kids fashion. Get all you need to be fashionably expressive on wawooh.com'}}
                  "/>
    <meta property="og:image" content="@if (isset($eventThumb))
    {{$eventThumb}} @elseif (isset($thumb)) {{$thumb}} @else {{asset('img/logo-sm.png')}}
    @endif"/>
    {{--<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">--}}
    {{--Link for style--}}
    <title>@yield('pageTitle') | Wawooh &mdash; Express you</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css') }}">
    {{--    <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap-reboot.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('css/wawooh.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/others.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/inspire/inspire.css') }}">

    {{--Events Styles--}}
    <link rel="stylesheet" href="{{ asset('css/events.css') }}">
    <link rel="stylesheet" href="{{ asset('cubeportfolio/css/cubeportfolio.min.css') }}">
    <link href="{{ asset('css/utility.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tag.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/dash/designerShop.css') }}">


    {{--External Links (3rd Party)--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('css/lightbox.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnify/2.1.0/css/magnify.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    {{--Fonts--}}
    <link href="https://fonts.googleapis.com/css?family=Parisienne" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600,700" rel="stylesheet">
    {{--<link href="https://fonts.googleapis.com/css?family=Allerta" rel="stylesheet">--}}

    {{--Title Icon--}}
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">

@stack('styles')
<!-- Hotjar Tracking Code for www.wawooh.com -->
    {{--<script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1219799,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>--}}
<!-- Global site tag (gtag.js) - Google Analytics -->
{{--<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126735794-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-126735794-1');
</script>--}}

<!-- Google Tag Manager -->
{{--<script>
    (function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start':
                new Date().getTime(), event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-NV4C7SQ');
</script>--}}
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
{{--<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NV4C7SQ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>--}}
<!-- End Google Tag Manager (noscript) -->
<a name="top" class="home"></a>
<button onclick="topFunction()" id="icon-top" title="Top">
    <i class="fa fa-fw fa-chevron-up"></i>
</button>
<div class="top-nav">
    <div class="row">
        <div class="col-md-8">

        </div>
        <div class="col-md-4 pl-0">
            <ul>
                <li><a title="Open A Store" target="_blank" href="{{ route('designersLandingPage') }}">Open your
                        shop</a></li>
                <li><a href="{{ route('faq') }}">Need Help?</a></li>
                {{--<li><a data-toggle="tooltip" href="#" data-placement="bottom"
                       title="Skywater Tower, 6, Otunba Oshikoya, Mobolaji Bank Anthony way, Ikeja"><i
                                class="fa fa-map-marker"></i></a></li>--}}
                <li><a title="" href="tel:0700-0-929664"><i class="fa fa-phone"></i> 0700-0-929664</a></li>
                <li>
                    <label for="countryCurrency" class="sr-only">Currency</label>
                    <select name="" id="countryCurrency" class="curr-style">
                        <option value="NGN" hidden>NGN</option>
                    </select>
                </li>
            </ul>
        </div>
    </div>
</div>
<header class="header">
    <nav>
        <div class="row">
            <div class="col-md-2">
                <div class="logo-brand">
                    @if(request()->route()->getName() == 'fashionStore')
                        <a href="{{route('fashionStore')}}">
                            <img class="logo-cont" src="{{ asset('img/logo.png') }}" alt="">
                        </a>
                    @elseif(request()->route()->getName() == 'eventsPage' || request()->route()->getName() == 'viewEvent')
                        <a href="{{route('eventsPage')}}">
                            <img class="logo-cont" src="{{ asset('img/logo.png') }}" alt="">
                        </a>
                    @else
                        <a href="{{route('fashionStore')}}">
                            <img class="logo-cont" src="{{ asset('img/logo.png') }}" alt="">
                        </a>
                    @endif
                </div>
            </div>
            @if(request()->route()->getName() == 'fashionStore')
                <div class="col-md-7 mx-auto">
                    <div class="product-search">
                        <form id="form2" method='get' class="row">
                            {{csrf_field()}}
                            <div class="col-lg-12">
                                <div class="input-group mb-3">
                                    <input id='query2' type="search" name="query"
                                           value='@if(!empty(request('query'))) {{request('query')}} @endif'
                                           class="form-control" placeholder="Search for products, brands">
                                    <div class="input-group-addon">
                                        <button type="submit" class="btn btn-search"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @elseif(request()->route()->getName() == 'eventsPage' || request()->route()->getName() == 'viewEvent')
                <div class="col-md-7 mx-auto">
                    <div class="product-search">
                        <form id='form' class="row" action="{{route('eventsPage')}}" method='get'>
                            {{csrf_field()}}
                            <div class="col-lg-12">
                                <div class="input-group mb-3">
                                    <input id='query2' type="search" name="query"
                                           value='@if(!empty(request('query'))) {{request('query')}} @endif'
                                           class="form-control" placeholder="Search for Events">
                                    <div class="input-group-addon">
                                        <button type="submit" class="btn btn-search"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @else
                <div class="col-md-7 mx-auto">
                    <div class="product-search">
                        <form id="form2" action="{{route('productPageSearch', 'search')}}" method='get' class="row">
                            {{csrf_field()}}
                            <div class="col-lg-12">
                                <div class="input-group mb-3">
                                    <input id='query2' type="search" name="query"
                                           value='@if(!empty(request('query'))) {{request('query')}} @endif'
                                           class="form-control" placeholder="Search for products, brands">
                                    <div class="input-group-addon">
                                        <button type="submit" class="btn btn-search"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
            <div class="col-md-3 ml-auto" style="padding: 0;">
                @include('inc.navbar')
            </div>
        </div>
    </nav>
</header>
@if(request()->route())
    @if(request()->route()->getName() == 'eventsPage' || request()->route()->getName() == 'viewEvent')
        <section class="event-category">
            <ul>
                <li><a class="@if(request()->route()->getName() =='fashionStore') active @endif"
                       href="{{route('eventsPage')}}">Home</a>
                </li>
                <li>
                    <a class="nav-link @if(request('eventType') == 'T') active @endif"
                       href="{{route('eventsPage')}}?eventType=T">Trending</a>
                </li>
                <li>
                    <a class="nav-link @if(request('eventType') == 'S') active @endif"
                       href="{{route('eventsPage')}}?eventType=S">Sponsored</a>
                </li>
            </ul>
        </section>
    @else
        <section class="shop-categories" style="">
{{--            @include('inc.category-menu')--}}
        </section>
    @endif
@endif
@include('inc.user-login-reg')
@yield('content')

<script>

</script>

