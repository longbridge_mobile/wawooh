<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <ul class="pcoded-item pcoded-left-item">
                <li class="pcoded-hasmenu active">
                    <a href="{{route('admin.dashboard')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu ">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-calendar"></i>
                        </span>
                        <span class="pcoded-mtext">Events</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('admin.events.create')}}" class="waves-effect waves-dark">
                                <span class="pcoded-micon">
                                    <i class="feather icon-calendar"></i>
                                </span>
                                <span class="pcoded-mtext">Create Events</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.events')}}" class="waves-effect waves-dark">
                                <span class="pcoded-micon">
                                <i class="feather icon-menu"></i>
                                </span>
                                <span class="pcoded-mtext">View All Events</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.tags')}}" class="waves-effect waves-dark">
                                <span class="pcoded-micon">
                                <i class="feather icon-location-pin"></i>
                                </span>
                                <span class="pcoded-mtext">Event Tags</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="{{ route('admin.products') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-menu"></i>
                        </span>
                        <span class="pcoded-mtext">Products</span>
                    </a>
                </li>

                <li class="">
                    <a href="{{route('admin.uploaded.style')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-menu"></i>
                        </span>
                        <span class="pcoded-mtext">User Uploaded Style</span>
                    </a>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-user"></i>
                        </span>
                        <span class="pcoded-mtext">Users</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('admin.users')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Users Only</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.designers')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Designers</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.role')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Admin Roles</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.allusers')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">All Users Roles</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="{{route('admin.abandonCart')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-menu"></i>
                        </span>
                        <span class="pcoded-mtext">Abandon Cart</span>
                    </a>
                </li>

               {{-- <li class="">
                    <a href="{{route('admin.eventTicket')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-award"></i>
                        </span>
                        <span class="pcoded-mtext">Ticket</span>
                    </a>
                </li>--}}

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-award"></i>
                        </span>
                        <span class="pcoded-mtext">Ticket</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('admin.eventTicket')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Ticket Management</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.ticket.order')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Order</span>
                            </a>
                        </li>
                    </ul>

                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-scissors"></i>
                        </span>
                        <span class="pcoded-mtext">Bespoke</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('admin.bespoke.apply')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bespoke Applications</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.image.bank')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Styles Catalogue</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('adminBespokeRequest')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bespoke Requests</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('adminBespokeProduct')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bespoke Products</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('adminBespokeOrder')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">All Bespoke Order</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-briefcase"></i>
                        </span>
                        <span class="pcoded-mtext">Order Management</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('admin.orders.all')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">All Orders</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('getAllDeliveredOrders')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">All Delivered Orders</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.transfer')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Bank Transfer Information</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.refunds')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Refunds</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="pcoded-hasmenu">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-edit"></i>
                        </span>
                        <span class="pcoded-mtext">Categories</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('admin.category')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Category</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.subcategory')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Sub Categories</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="{{ route('admin.promocode') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-codepen"></i>
                        </span>
                        <span class="pcoded-mtext">Promo Codes</span>
                    </a>
                </li>



                <li class="">
                    <a href="{{ route('admin.newsletter') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-paperclip"></i>
                        </span>
                        <span class="pcoded-mtext">Subscribers</span>
                    </a>
                </li>

                <li class="">
                    <a href="{{ route('admin.feedbacks') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-mail"></i>
                        </span>
                        <span class="pcoded-mtext">Feedback</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin.logout')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-power"></i>
                        </span>
                        <span class="pcoded-mtext">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>