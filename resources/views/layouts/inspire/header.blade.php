<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('pageTitle') | Wawooh &mdash; Express you</title>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wawooh is a fashion dedicated marketplace to shop Bespoke and Ready to Wear quality fashion apparels from African fashion designers.">
    <meta name="keywords"
          content="fashion, designers, clothes, cloth, shoe, accessories, ecommerce, events, image, showroom">
    <meta property="og:url" content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content=""/>
    <meta property="og:description"
          content="Wawooh is a fashion dedicated marketplace to shop Bespoke and ReadytoWear quality fashion apparels from African fashion designers."/>
    <meta property="og:image" content="{{asset('img/logo-sm.png')}}"/>
    <link href="{{ asset('bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap/bootstrap-grid.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap-reboot.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/wawooh.css') }}">
    <link rel="stylesheet" href="{{ asset('css/inspire/inspire.css') }}">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126735794-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126735794-1');
    </script>

</head>
<body>
<a name="top" class="home"></a>
<button onclick="topFunction()" id="icon-top" title="Top">
    <i class="fa fa-fw fa-chevron-up"></i>
</button>
<header class="header">
    <nav>
        <div class="logo-brand">
            <a href="{{route('inspireLanding')}}"><img class="logo-cont" src="{{ asset('img/logo.png') }}" alt=""></a>
            <!--            <a href=""><img class="logo-cont" src="assets/images/waw-white.png" alt=""></a>-->
            <!--            <a href="home"><img class="logo-cont" src="assets/images/logo.png" alt=""></a>-->
        </div>
        <ul>
            <li><a href="{{ route('eventsPage') }}" class="underline">Show Case</a></li>
            <li><a href="{{ route('fashionStore') }}" class="underline">Shop</a></li>
            <li><a href="{{ route('cart') }}" class="underline">
                    <sup class='badge brand-bgcolor cart-sup'>2</sup>
                    <img src="{{asset('img/cart2.png')}}"> Cart
                </a>
            </li>
            {{--<li class="nav-item" style="width: 116px">
                <a class="nav-link edit-cart3" style='position:relative' href="{{route('cart')}}"><img
                            src="{{asset('img/cart2.png')}}"> Cart <sup
                            class='badge brand-bgcolor cart-sup' style='border-radius:50%'>2</sup></a>
            </li>--}}
            @if(!@$resp2)
                <li><a class="" href="#" data-toggle="modal"
                       data-target="#ModalSignIn"><i style="vertical-align: middle;" class="fa fa-fw fa-user-circle fa-2x"></i>Sign in</a></li>
            @else
                <li class="drop-down"><a href="">Temitope&nbsp;<i class="fa fa-fw fa-caret-down"></i></a>
                    <div class="dropmenu">
                        <ul>
                            <li><a href="{{route('userProfile', $resp2->firstName)}}">Profile</a></li>
                            <li><a href="{{route('wishlist')}}">Wishlist</a></li>
                            <li><a href="">My Wallet Bal:<br>
                                    <strong>₦{{number_format($resp2->wallet->pocketBalance)}}</strong></a></li>
                            <li><a href="{{route('logout')}}">Logout</a></li>
                        </ul>
                    </div>
                </li>
            @endif
        </ul>
    </nav>
</header>
@include('inc.user-login-reg')
@yield('content')

