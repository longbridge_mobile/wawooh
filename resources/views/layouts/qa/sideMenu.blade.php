<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <ul class="pcoded-item pcoded-left-item">
                <li class="{{request()->route()->getName() == 'qa.dashboard' ? 'active': ''}}">
                    <a href="{{route('qa.dashboard')}}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>

                <li class="pcoded-hasmenu {{request()->route()->getName() == 'qa.users' ? 'active': ''}}">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-user"></i>
                        </span>
                        <span class="pcoded-mtext">Users</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="{{request()->route()->getName() == 'qa.users' ? 'active': ''}}">
                            <a href="{{route('qa.users')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Users Only</span>
                            </a>
                        </li>
                        <li class="{{request()->route()->getName() == 'qa.designers' ? 'active': ''}}">
                            <a href="{{route('qa.designers')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Designers</span>
                            </a>
                        </li>
                        {{--<li class="">
                            <a href="{{route('admin.role')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Admin Roles</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('admin.allusers')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">All Users Roles</span>
                            </a>
                        </li>--}}
                    </ul>
                </li>


                <li class="pcoded-hasmenu {{request()->route()->getName() == 'qa.orders.all' ? 'active': ''}}">
                    <a href="javascript:void(0)" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-briefcase"></i>
                        </span>
                        <span class="pcoded-mtext">Order Management</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="">
                            <a href="{{route('qa.orders.all')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Submitted Orders</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('qa.order-list')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Order List</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="{{route('deliveredOrders')}}" class="waves-effect waves-dark">
                                <span class="pcoded-mtext">Delivered Orders</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="">
                    <a href="{{route('qa.bespoke-order')}}"  class="waves-effect waves-dark">
                    <span class="pcoded-micon">
                        <i class="feather icon-award"></i>
                    </span>
                        <span class="pcoded-mtext">Bespoke Order Management</span>
                    </a>
                </li>

                <li class="">
                    <a href="{{ route('qa.logout') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="feather icon-power"></i>
                        </span>
                        <span class="pcoded-mtext">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>