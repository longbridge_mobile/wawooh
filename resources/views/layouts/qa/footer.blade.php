


<script src="{{asset('js/admin/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/dash/bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/waves.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>
<script src="{{asset('js/admin/jquery.slimscroll.js')}}"></script>
<script src="{{asset('js/plugins/notify.min.js')}}"></script>
@include('scripts.utility')
<script src="{{asset('js/wow.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('cubeportfolio/jquery.cubeportfolio.min.js')}}"></script>
<script src="{{asset('cubeportfolio/main.js')}}"></script>

<script src="{{asset('js/admin/datatable/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

<script src="{{asset('js/admin/pcoded.min.js')}}"></script>
<script src="{{asset('js/admin/vertical-layout.min.js')}}"></script>
<script src="{{asset('js/admin/script.js')}}"></script>
@stack('scripts')
<script>
    $(document).ready(function () {
        $('#pendingOrders, #categoryTable, #subCatTable, #incompleteTable, #designerTable, #refundTable, #transferInfo, #usersTable').DataTable();
        $('#unverified-table, #verified-table, #designerList, #orderList, #allOrderTable, #orderT').DataTable();

        $('.promo-calender').flatpickr({
            minDate: "today",
            enableTime: true,
            dateFormat: "Y-m-d h:m:i",
            time_24hr: false
        });
        $('.calendar').flatpickr({
            minDate: "today",
            dateFormat: "Y-m-d",
            // mode: "range"
        });
    })
</script>
</body>
</html>