@component('mail::message')
# Introduction

Hello <strong>{{$user['firstName']}} {{$user['lastName']}}</strong>,
<br/>
Welcome to wawooh, please click on the link below to activate your profile.

@php
$url = route('fashionStore');
@endphp

@component('mail::button', ['url' => $url])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
