@php
// The function header by sending raw excel
//header("Content-type: application/vnd-ms-word");
header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

// Defines the name of the export file "codelution-export.xls"
header("Content-Disposition: attachment; filename=order-list-audit.xls");

// Add data table
@endphp
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Export Orders | Auditor &mdash; Express You</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>
<section class="export-orders">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th scope="col">Order No</th>
            <th scope="col">Store Name</th>
            <th scope="col">Designer Email</th>
            <th scope="col">Paid Amount</th>
            <th scope="col">Amount</th>
            <th scope="col">Due Date</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($orders))
            @if(count($orders))
                @foreach($orders as $order)
                    <tr>
                        <td>{{$order->orderNumber}}</td>
                        <td></td>
                        <td>{{$order->designer}}</td>
                        <td>₦{{number_format($order->item->paidAmount)}}</td>
                        <td>₦{{number_format($order->item->amount)}}</td>
                        <td>
                            {{\Carbon\Carbon::createFromTimestamp($order->dueDate / 1000)->format('dS F, Y')}}
                            <br>
                            <small class="text-success">({{\Carbon\Carbon::createFromTimestamp($order->dueDate / 1000)->diffForHumans()}})</small>
                        </td>
                    </tr>
                @endforeach
            @endif
        @endif
        </tbody>
    </table>

    {{--<div class="row">--}}
        {{--<div class="col-md-12" style="text-align: center; margin-bottom: 20px;">--}}
            {{--<a class="btn btn-success" href="{{route('admin.export.file')}}">Export Preview</a>--}}
        {{--</div>--}}
    {{--</div>--}}
</section>
</body>
</html>