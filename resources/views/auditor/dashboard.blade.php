@extends('layouts.auditor')

@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <style>
    #orderList, #designerList {
      text-align: center;
    }
    table th {
      text-align: center;
    }
  </style>
@endpush

@section('pageTitle','Dashboard')
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Dashboard</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="media-box bg-wawooh">
            <div class="media-icon pull-left"><i class="icon-bargraph"></i> </div>
            <div class="media-info">
              <h5>{{$order}} Due Payment</h5>
              <h3></h3>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- content -->
  </div>

@endsection

@push('scripts')
  <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
      $(document).ready(function () {
          $('#designerList, #orderList').DataTable();
      })
  </script>
@endpush