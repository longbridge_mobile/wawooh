@if(Session::has('success'))
    <div class="row">
        <div class="col-md-12 alert alert-success">
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <p class="text-center text-uppercase"><strong>{{Session::get('success')}}</strong></p>
        </div>
    </div>
@endif
@if(Session::has('error'))
    <div class="row">
        <div class="col-md-12 alert alert-success">
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <p class="text-center text-uppercase"><strong>{{Session::get('error')}}</strong></p>
        </div>
    </div>
@endif
@if(!empty($flasherror))
    <div class="row">
        <div class="col-md-12 alert alert-success">
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <p class="text-center text-uppercase"><strong>{{$flasherror}}</strong></p>
        </div>
    </div>
@endif
@if(count($errors) > 0)
    <div class="col-md-12 alert alert-danger">
        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif