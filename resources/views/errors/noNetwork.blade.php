<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>No Network | Wawooh &mdash; Express you</title>
    <link rel="stylesheet" href="{{asset('css/wawooh.css')}}">
</head>
<body>
    <section class="network">
        <div class="netoverlay"></div>
        <div class="inner-content">
            <h3>Gosh!</h3>
            <span>Check your Network Connection</span>

            <div class="btnHolder">
                <button id="reloadPage" type="button" class="btn btn-network">Reload the page</button>
                {{--<button onclick="goBack()">Go Back</button>--}}
            </div>
        </div>
    </section>
</body>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#reloadPage').on('click', function () {
            location.reload();
        })
    });

   /* function goBack(e) {
        e.preventDefault();
        window.history.back();
    }*/

</script>
</html>