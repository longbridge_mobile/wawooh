<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Error - Wawooh</title>

    <link href="{{ asset('bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap/bootstrap-grid.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap-reboot.min.css') }}" rel="stylesheet">

    <style>
        /* body{ margin: 0px;
              padding: 0px;
              height: 100vh; }
        #particles-js {
            height: 100%;
            background-color: #cd9933;
        }
        .network{position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            margin: 0 auto;
            text-align: center;
            color: white;
            margin-top: 17%;}
        .network h1{font-size: 150px;
                    margin-bottom: 0px;}
        .network p{font-size: 17px;}*/

        .btn-error {
            background-color: #cd9933;
            color: white;
            font-weight: bold;
        }

        body {
            position: relative;
        }

        h4 {
            color: white;
        }

        .Lost {
            background-image: url(../img/laundry1.png);
            background-blend-mode: color-burn;
            background-size: cover;
            position: absolute;
            background-color: rgba(0, 0, 0, 0.60);
            width: 100%;
            height: 100vh;
        }

        .lostContent {
            margin: 0 auto;
            position: absolute;
            top: 150px;
            right: 0;
            left: 0;
            text-align: center;
        }
    </style>
</head>
<body>

<div class="Lost">
    <div class="lostContent">
        <img src="{{ asset('img/shocked.png')}}">
        <br>
        <br>
        <h4>Oh no! You are lost, You are in our Laundry Room</h4>
        <br>

        <br>
        <a href="{{route('fashionStore')}}" class="btn btn-error">Take me to Shop</a>
    </div>
</div>


<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.bundle.js') }}"></script>


</body>
</html>