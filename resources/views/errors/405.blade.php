<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wawooh</title>

    <link href="{{ asset('bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap/bootstrap-grid.css') }}" rel="stylesheet">
    <link href="{{ asset('bootstrap-reboot.min.css') }}" rel="stylesheet">

    <style>
        body {
            margin: 0;
            padding: 0;
            height: 100vh;
        }

        #particles-js {
            height: 100%;
            background-color: #cd9933;
        }

        .network {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            text-align: center;
            color: white;
            margin: 17% auto 0;
        }

        .network h1 {
            font-size: 150px;
            margin-bottom: 0px;
        }

        .network p {
            font-size: 17px;
        }

        .btn-error {
            background-color: white;
            color: #cd9933;
            font-weight: bold;
        }
    </style>
</head>
<body>

<div id="particles-js">
    <div class="network">
        <h1>405</h1>
        <p>Method Not allowed.</p>
        <button class="btn btn-error" onclick='history.back()'>Back</button>
    </div>
</div>

<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.bundle.js') }}"></script>
<script src="{{ asset('js/particles.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>