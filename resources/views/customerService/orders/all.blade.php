@extends('layouts.cs')

@push('styles')

@endpush

@section('pageTitle','All Orders')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Orders</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#"><i class="fa fa-home"></i> Order</a></li>
                <li class="active"><i class="fa fa-dashboard"></i> All order</li>
            </ol>
        </section>
        <section class="content container-fluid">
            <div class="bg-white">
                <h3 class="sub-orders">Orders: <label class="badge badge-danger">{{count($orders)}}</label></h3>


                <div class="tab-content m-top-2">
                    <div role="tabpanel" class="tab-pane active" id="all">
                        <div class="all-order row">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <p>Total Number of entries:
                                            {{-- @if(isset($orders))
                                             <span class="badge badge-primary">{{count($orders)}}</span></p>
                                         @endif--}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="allOrderTable">
                                    <thead>
                                    <tr>
                                        <th scope="col">Order No</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($orders))
                                        @if(count($orders))
                                            @foreach($orders as $order)
                                                <a href='{{route('admin.order.items', $order->id)}}'>
                                                    <tr class='clickable-row'>
                                                        <td>{{$order->orderNumber}}</td>
                                                        <td>₦{{number_format($order->totalAmount)}}</td>
                                                        <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-Y')}}
                                                            <br> <small class="text-success text-centerC">({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}})</small></td>
                                                        <td>{{\Carbon\Carbon::createFromTimestamp(strtotime($order->orderDate))->format('h:m:i A')}}</td>

                                                    </tr>
                                                </a>
                                            @endforeach
                                        @endif
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{--@if(isset($totalOrder))--}}
                        {{--<div>--}}
                        {{--{!! $totalOrder->render() !!}--}}
                        {{--</div>--}}
                        {{--@endif--}}
                    </div>

                </div>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('admin.export.orders')}}" class="pull-right btn btn-primary">Export Data</a>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

@push('scripts')

    <script>

        $('.cancelOrder').click(function(){
            var data = {
                "id": $(this).attr('order'),
                "orderNumber": $(this).attr('orderNumber'),
                "deliveryStatus": $(this).attr('stat'),
                "userId": $(this).attr('customer'),
                "paidAmount": $(this).attr('payment')
            };
            console.log(data);
            swal({
                title: "Are you sure?",
                text: "This action is irreversible. Click yes to continue if you are sure",
                icon: "warning",
                buttons: ["No", "Yes"],
                dangerMode: true,
            })
                .then((confirm) => {
                    if(confirm){
                        $('#loaderModal').modal();
                        $.ajax({
                            url: '{{route('admin.cancelOrder')}}',
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function (result) {
                                console.log(result);
                                $('div.loader').hide();
                                if (result.status == 0) {
                                    $('#loaderModal').modal('hide');
                                    swal('Order Confirmed Successfully', '', 'success');
                                    location.reload();
                                } else if (result.status == 56) {
                                    swal({
                                        title: "Cannot confirm Order",
                                        text: result.data,
                                        icon: "warning",
                                        buttons: ["No", "Yes"],
                                        dangerMode: true,
                                    })
                                    location.reload();
                                } else {
                                    $('#loaderModal').modal('hide');
                                    swal(result.message, '', 'error');
                                }
                            },
                            error: function (e) {
                                $('div.loader').hide();
                                $('#loaderModal').modal('hide');
                                swal(e, '', 'error');
                            }
                        });
                    }
                })
        });


        $(document).ready(function () {
            $('#delIncompleteOrder').on('click', function () {
                <?php if(!is_null(session('adminToken'))): ?>
                    tok = <?= session('adminToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                var id = $(this).attr('del');
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click yes to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                }).then(confirm => {
                    if (confirm) {
                        $('#loaderModal').modal();
                        $.ajax({
                            url: `{{env('GET_BASE_URL')}}/fashion/secure/admin/order/${id}/deleteorder`,
                            headers: {
                                'Authorization': tok,
                                'Content-Type': 'application/json; charset=utf-8',
                                'accept': 'application/json'
                            },
                            dataType: 'json',
                            success: function (result) {
                                console.log(result);
                                if (result.status == 0) {
                                    $('#loaderModal').modal('hide');
                                    $(this).closest('.clickable-row').hide();
                                    // console.log($(this).closest('.clickable-row').hide());
                                    swal('Successfully Deleted', '', 'success');
                                    location.reload();
                                    //$(this).closest('.clickable-row').hide();
                                    $('#incompleted').addClass('active');
                                }
                            }
                        });
                    }
                })
            });
            $('.accept').on('click', function () {
                var data = {
                    "id": $(this).attr('order'),
                    "orderNumber": $(this).attr('orderNumber'),
                    "deliveryStatus": $(this).attr('stat'),
                    "userId": $(this).attr('customer'),
                    "paidAmount": $(this).attr('payment')
                };
                console.log(data);
                swal({
                    title: "Are you sure?",
                    text: "This action is irreversible. Click yes to continue if you are sure",
                    icon: "warning",
                    buttons: ["No", "Yes"],
                    dangerMode: true,
                })
                    .then((confirm) => {
                        if (confirm) {
                            $('#loaderModal').modal();
                            $.ajax({
                                url: '{{route('admin.comfirmPayments')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (result) {
                                    console.log(result);
                                    $('div.loader').hide();
                                    if (result.status == 0) {
                                        $('#loaderModal').modal('hide');
                                        swal('Order Confirmed Successfully', '', 'success');
                                        location.reload();
                                    } else if (result.status == 56) {
                                        swal({
                                            title: "Cannot confirm Order",
                                            text: result.data,
                                            icon: "warning",
                                            buttons: ["No", "Yes"],
                                            dangerMode: true,
                                        })
                                        location.reload();
                                    } else {
                                        $('#loaderModal').modal('hide');
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                    swal(e, '', 'error');
                                }
                            });
                        }
                    })
            });
        });

    </script>

@endpush