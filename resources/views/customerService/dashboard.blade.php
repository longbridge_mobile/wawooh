@extends('layouts.cs')

@push('styles')
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <style>
    #orderList, #designerList {
      text-align: center;
    }
    table th {
      text-align: center;
    }
  </style>
@endpush

@section('pageTitle','Dashboard')
@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Dashboard</h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="media-box bg-wawooh">
            <div class="media-icon pull-left"><i class="icon-bargraph"></i> </div>
            <div class="media-info">
              <h5>Total Product</h5>
              <h3></h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="media-box">
            <div class="media-icon pull-left"><i class="icon-wallet"></i> </div>
            <div class="media-info c-wawooh">
              <h5>Total Payment</h5>
              <h3></h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="media-box">
            <div class="media-icon pull-left"><i class="icon-basket"></i> </div>
            <div class="media-info c-wawooh">
              <h5>Total Sales</h5>
              <h3></h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="media-box bg-wawooh">
            <div class="media-icon pull-left"><i class="icon-layers"></i> </div>
            <div class="media-info">
              <h5>New Orders</h5>
              <h3></h3>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="chart-box">
            <h4>Recent Designers</h4>
            <hr>
            <div class="chart">
              <div id="container1">
                <table class="table table-responsive table-striped table-hover" id="designerList">
                  <thead>
                  <tr>
                    <th>SN</th>
                    <th>Store Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>No of Product</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
              <!--for values check "Sales Overview" chart on char-function.js-->
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="chart-box">
            <h4>Recent Orders</h4>
            <hr>
            <div class="table-block">
              <div class="table-responsive">
                <table class="table table-responsive table-striped table-hover" id="orderList">
                  <thead>
                  <tr>
                    <th>SN</th>
                    <th>OrderID</th>
                    <th>Customer Name</th>
                    <th>Status</th>
                    <th>Amount</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="chart-box">
            <div>
              <h5>Total orders placed</h5>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="chart-box">
            <div>
              <h5>Total Products</h5>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="chart-box">
            <div>
              <h5>Conversion Rate</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- content -->
  </div>

@endsection

@push('scripts')
  <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script>
      $(document).ready(function () {
          $('#designerList, #orderList').DataTable();
      })
  </script>
@endpush