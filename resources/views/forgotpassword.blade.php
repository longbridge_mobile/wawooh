<!doctype html>
<html lang="en">
<head>
    <title>Forgot Password &mdash; Open your online Store &mdash; Wawooh &mdash; Express you</title>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Shop Online for latest African fashion, trendy made in Nigeria clothes, work wear, party dresses,
          bridal dresses, Ankara fashion, tie-dye adire, jumpsuit, agbada, tuxedo, kaftans, tunic, athleisure wear,
          accessories, jewellery, shoes, bags and more from top fashion brands in Nigeria and across Africa at
          affordable rates, fast shipping, worldwide shipping and maximum satisfaction. Wawooh.">
    <meta name="keywords"
          content="All, top, categories, see, accessories, shop, black, top categories, see all, shop now, bag, latest fashion, new fashion
fashion show, online fashion, fashion style, fashion website, new fashion trend, fashion dresses, new fashion, latest fashion trend fro women,
online shopping fashion, fashion clothes, fashion stores, fashion wear, fall fashion, fashion sites, women fashion, men fashion, ankara, kids fashion,
shop here, tie-dye adire, tuxedo, kaftan, tunic, athleisure wear, worldwide, fast shipping, affordable, agbada">
    <meta property="og:url" content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:description"
          content="Shop Online for latest African fashion, trendy made in Nigeria clothes, work wear, party dresses,
          bridal dresses, Ankara fashion, tie-dye adire, jumpsuit, agbada, tuxedo, kaftans, tunic, athleisure wear,
          accessories, jewellery, shoes, bags and more from top fashion brands in Nigeria and across Africa at
          affordable rates, fast shipping, worldwide shipping and maximum satisfaction. Wawooh."/>
    <meta property="og:image" content="{{asset('img/logo-sm.png')}}"/>

    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/wawooh.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    {{--3rd Party Styles--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    @stack('styles')
    <style>
        .lds-dual-ring {
            display: inline-block;
            width: 18px;
            height: 18px;
            padding-left: 9px;
        }

        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 26px;
            height: 26px;
            margin: 1px;
            border-radius: 50%;
            border: 5px solid #cd9933;
            border-color: #cd9933 transparent #cd9933 transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }

        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .error {
            border: 1px solid red;
        }
        .passwordBox {
            background: #fff;
            width: 35%;
            margin: 8em auto;
            padding: 20px;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126735794-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-126735794-1');
    </script>
</head>
<body>

<div style="border-top: 1px solid rgba(191, 190, 190, 0.458823529411764);"></div>
<div>
    <div class="passwordBox passwordMargin">
        <div class="col-lg-12">
            <p class="forgottext">Please type in your email address. We will send you a link to change the
                password.</p>
            <input type="text" class="form-control" id='ema' placeholder="Enter Email address">
            <input type='hidden' id='pUrl' value='{{$previousUrl}}'/>
            <div id='loa' style='height:100px;display:none;' class='text-center'>
                <img class='center-block' src="{{asset('img/loader.gif')}}"/>
            </div>

            <input id="resetPwd" type="button" class="btn btn-wawooh btn-block send" value="Reset Password">

        </div>
    </div>
</div>

@include('scripts.designer.allScript')
<script src="{{ asset('js/validation.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.send').on('click', function () {
            $('#resetPwd').val('Please wait...').attr('disabled', 'disabled');
            let email = $('#ema').val();
            let url = $('#pUrl').val();
            // console.log(email+ ' '+ url);
            if (isEmail(email)) {
                $('#resetPwd').val('Please wait...').attr('disabled', 'disabled');
                $('#loa').show();
                $.ajax({
                    url: `{{route('resetPassword')}}/?email=${email}&url=${url}`,
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (result) {
                        //console.log(result);
                        $('#loa').hide();
                        if (result.status === "00") {
                            $('#resetPwd').val('Reset Password').attr('disabled', 'disabled');
                            //console.log(result);
                            //window.location.href = '{{route('user.reset.success')}}';
                            swal('A reset link has been sent to your email.', '', 'success');
                            window.location.href = '{{route('fashionStore')}}';
                            //$('.send').removeAttr('disabled');
                        } else {
                            swal(result.message, '', 'error');
                            $('#resetPwd').val('Reset Password').removeAttr('disabled', 'disabled');
                            $('#email').notify(result.message, 'error');
                        }
                    },
                    error: function (e) {
                        $('#loa').hide();
                        $('#resetPwd').val('Reset Password').removeAttr('disabled', 'disabled');
                        swal(e, '', 'error');
                        // console.log(e);
                    }
                });
            } else {
                $('#resetPwd').val('Reset Password').removeAttr('disabled', 'disabled');
                $('#ema').notify('Please enter a valid email', 'error');
            }
        });
    });
</script>
</body>
</html>
