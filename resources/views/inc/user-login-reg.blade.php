<div class="modal fade" data-backdrop="static" id="ModalSignIn" tabindex="-1" role="dialog"
     aria-labelledby="ModalSignInlLabel" aria-hidden="true" style="z-index: 1300; !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content login-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="Modalsign col-md-12">
                    <form role="form">
                        <div class="form-group">
                            <input type="email" id='email' class="form-control" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <input type="password" id='password' class="form-control" placeholder="Password"
                                   required>
                        </div>
                        <div class="form-group">
                            <input type="button" id="loginBtn" class="btn btn-block login sign-in" value="SIGN IN">
                        </div>
                    </form>
                    <div class="text-center">
                        <p class="forgot-login"><a href="{{route('forgotPassword')}}">Forgot Log in Details</a></p>
                        {{--  <p>Or Sign in with Social Media</p>  --}}
                        <div class="social-login">
                            {{--  <div class="social-login-facebook">
                                <a href='{{route('facebookRegister')}}'><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                            </div>  --}}
                            {{--  <div  class="social-login-google">
                                <i class="fa fa-google-plus  fa-lg" aria-hidden="true"></i>
                             </div>  --}}
                        </div>
                        <p class="no-account">Don't have an account?</p>
                        <p class="forgot-login ">
                            <a href="#" id='signUp' data-toggle="modal" data-target="#ModalSignUp">Signup Here</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="ModalSignIn2" tabindex="-1" role="dialog"
     aria-labelledby="ModalSignInlLabel" aria-hidden="true" style="z-index: 1300; !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content login-modal">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="container Modalsign">
                    <h5>SIGN IN</h5>
                    <form role="form">
                        <div class="input-details">
                            <input type="email" id='email2' class="form-control" placeholder="Email" required>
                        </div>
                        <div class="input-details">
                            <input type="password" id='password2' class="form-control" placeholder="Password"
                                   required>
                        </div>
                        {{--  <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="remember">
                            <label class="form-check-label" for="remember">Remember me</label>
                        </div>  --}}

                        <div id='loaderLog2' style='height:100px;display:none;' class='text-center'>
                            <img class='center-block' src="{{asset('img/loader.gif')}}"/>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-block login2">SIGN IN</button>
                        </div>
                    </form>
                    <div class="text-center">
                        <p class="forgot-login"><a href="{{route('forgotPassword')}}">Forgot Log in Details</a></p>
                        {{--  <p>Or Sign in with Social Media</p>  --}}
                        <div class="social-login">
                            {{--  <div class="social-login-facebook">
                                <a href='{{route('facebookRegister')}}'><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                            </div>  --}}
                            {{--  <div  class="social-login-google">
                                <i class="fa fa-google-plus  fa-lg" aria-hidden="true"></i>
                             </div>  --}}
                        </div>
                        <p class="no-account">Don't have an account?</p>
                        <p class="forgot-login "><a href="#" id='signUp' data-toggle="modal"
                                                    data-target="#ModalSignUp">Signup Here</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="static" id="ModalSignUp" tabindex="-1" role="dialog"
     aria-labelledby="ModalSignUplLabel" aria-hidden="true" style="z-index: 1300; !important;">
    <div class="modal-dialog" role="document">
        <div class="modal-content login-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create an Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form role="form" class="form-horizontal">
                    <p id='errorSignUp'></p>
                    {{--<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" id='fname' class="form-control signup2" placeholder="Firstname"
                                       required>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" id='lname' class="form-control signup2" placeholder="Lastname"
                                       required>
                            </div>
                        </div>
                    </div>--}}
                    <div class="form-group">
                        <input type="text" id='lname' class="form-control signup2" placeholder="Lastname"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="text" id='fname' class="form-control signup2" placeholder="Firstname"
                               required>
                    </div>
                    <div class="form-group">
                        <input type="email" id='emSign' class="form-control signup2"
                               placeholder="Enter a functional email address" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" id='userPhone' class="form-control signup2"
                               placeholder="Enter Phone Number" required>
                    </div>
                    <div class="form-group">
                        <input type="password" id='passwordSign' class="form-control signup2"
                               placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <input id='passwordSign2' type="password" class="form-control signup2"
                               placeholder="Repeat password" required>
                    </div>
                    {{--<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dateOfBirth" class="sr-only">Date of Birth</label>
                                <input type="text" id='dateOfBirth' class="form-control dofb signup2"
                                       placeholder="Select Date of Birth (yyyy/mm/dd)" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="gender" class="sr-only">Gender</label>
                                <select name="gender" id="gender" class="form-control signup2" required>
                                    <option value="null" hidden>--Select Gender--</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Others">Choose not to Specify</option>
                                </select>
                            </div>
                        </div>
                    </div>--}}

                    {{--<div id='loaderLog3' style='height:100px;display:none;' class='text-center'>
                        <img class='center-block' src="{{asset('img/loader.gif')}}"/>
                    </div>--}}
                    <div class="form-group">
                        <input type="button" id='signup' class="btn btn-block sign-in" value="CREATE AN ACCOUNT">
                    </div>
                    <div class="form-group">
                        {{--<button class="g-signin2" data-onsuccess="onSignIn">GOOGLE WITH SIGNUP</button>--}}
                        {{--<input type="button" id='signup' class="btn btn-block sign-in" value="CREATE AN ACCOUNT">--}}
                    </div>
                </form>
                <hr>
                <div class="text-center">
                    {{--<p>Or Sign up with Social Media</p>
                    <div class="social-login">
                            <a class="btn btn-facebook" href='{{route('facebookRegister')}}'>
                                <i class="fab fa-facebook fa-lg" aria-hidden="true"></i> Facebook
                            </a>
--}}
                    {{--  <div  class="social-login-google">
                        <i class="fa fa-google-plus  fa-lg" aria-hidden="true"></i>
                     </div>  --}}
                    {{--</div>--}}

                    {{-- <div class="row google-login">
                         <div class="col-md-12">
                             <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div>
                         </div>
                     </div>--}}
                    <p class="no-account">Already have an account?</p>
                    <p class="forgot-login "><a class='logina' href="#">Login Here</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

