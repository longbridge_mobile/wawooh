<ul class="category-style">
    <li><a href="{{route('designersLandingPage')}}" class="sell-link"><strong>SELL HERE</strong></a></li>
    <li><a href="{{route('allNewArrival')}}">New In</a></li>
    @if(isset($resp))
        @if(count($resp))
            @foreach($resp as $category)
                @if(isset($category->categoryName))
                    <li class="dropmenu">
                        <a href="{{route('fashionCategory', [strtolower($category->categoryName), $category->id])}}">{{title_case($category->categoryName)}}</a>
                        <div class="drop-mega">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Clothings</h4>
                                    <ul class="wrapMe">
                                        {{--<li><a>View All</a></li>--}}
                                        @if(is_array($category->subCategories))
                                            @foreach($category->subCategories as $subcat)
                                                @if($subcat->productType == 1)
                                                    @if($subcat->delFlag !== 'Y')
                                                        <li>
                                                            <a href="{{route('productPage', [$category->id, strtolower($category->categoryName), str_slug($subcat->subCategory), $subcat->id])}}">{{$subcat->subCategory}}</a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <h4>Accessories</h4>
                                    <ul>
                                        {{--<li><a>View All</a></li>--}}
                                        @if(is_array($category->subCategories))
                                            @foreach($category->subCategories as $subcat)
                                                @if($subcat->productType == 2)
                                                    @if($subcat->delFlag !== 'Y')
                                                        <li>
                                                            <a href="{{route('productPage', [$category->id, strtolower($category->categoryName), str_slug($subcat->subCategory), $subcat->id])}}">{{$subcat->subCategory}}</a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    <h4>Shoes</h4>
                                    <ul>
                                        {{--<li><a>View All</a></li>--}}
                                        @if(is_array($category->subCategories))
                                            @foreach($category->subCategories as $subcat)
                                                @if($subcat->productType == 3)
                                                    @if($subcat->delFlag !== 'Y')
                                                        <li>
                                                            <a href="{{route('productPage', [$category->id, strtolower($category->categoryName), str_slug($subcat->subCategory), $subcat->id])}}">{{$subcat->subCategory}}</a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                @else <label class="label label-danger">No Network</label>
                @endif
            @endforeach
        @endif
    @endif
    <li class="dropmenu">
        <a class="" href="{{route('getAllBespoke')}}">Bespoke</a>
        <div class="drop-mega">
            <div class="row">
                <div class="col-md-4">

                </div>
                <div class="col-md-4">
                    <ul>
                        <li> <a href="{{route('styleCatalogue')}}">Style Catalogue</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul>
                        <li> <a href="{{route('getAllBespokeCat', 'men')}}/?cat=1">Men</a></li>
                        <li> <a href="{{route('getAllBespokeCat', 'women')}}/?cat=11">Women</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </li>
</ul>