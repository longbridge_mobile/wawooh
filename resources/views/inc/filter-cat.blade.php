<section class="categoryNavList">
    @if(empty(request('_token')) || empty(request('query')))
        @if(count($response))
            <div class="btn-group">
                <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Price Range
                </button>
                <div class="dropdown-menu">
                    <h3 class="header-title">Filter by Price</h3>
                    <div class="body-cont">
                        <div class="range-slide">
                            <div id="slider-range"></div>
                            <div class="price-value">
                                <label for="amount" class="sr-only">Price</label>
                                <input type="text" id="amount" readonly
                                       style="border:0; color:#f6931f; font-weight:bold;text-align: center;margin-top: 8px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Product Rating
                </button>
                <div class="dropdown-menu">
                    <h3 class="header-title">Filter by Product Ratings</h3>
                    <div class="body-cont">
                        <div id="filtByProElastic" class="filterRange score-card">
                            <div>
                                <form action="" class="star-hidden-search">
                                    <input class="star star-5" id="star-5" type="radio" value='5'
                                           name="prodstar-elastic"/>
                                    <label class="star star-5" for="star-5"></label>
                                    <input class="star star-4" id="star-4" type="radio" value='4'
                                           name="prodstar-elastic"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-3" id="star-3" type="radio" value='3'
                                           name="prodstar-elastic"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-2" id="star-2" type="radio" value='2'
                                           name="prodstar-elastic"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-1" id="star-1" type="radio" value='1'
                                           name="prodstar-elastic"/>
                                    <label class="star star-1" for="star-1"></label>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @else
        @if(count($response))
            <div class="btn-group">
                <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Designer Name
                </button>
                <div class="dropdown-menu">
                    <h3 class="header-title">Designer Name</h3>
                    <div class="body-cont">
                        @if(isset($designerResponse))
                            @if(count($designerResponse->group_by_designerName->buckets))
                                <ul>
                                    @foreach($designerResponse->group_by_designerName->buckets as $aggregate)
                                        <li>
                                            <a href="{{route('productPageSearch', 'designerName' )}}?value={{$aggregate->key}}&searchTerm={{request('query')}}">{{$aggregate->key}}
                                                ({{$aggregate->doc_count}})</a>
                                            <i class="icon-ser fa fa-check"></i>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

            @php
                $subCat = array();
               $subCatName ='';
            @endphp
            <div class="btn-group">
                <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    Sub Categories
                </button>
                <div class="dropdown-menu">
                    <h3 class="header-title">Filter Sub Category</h3>
                    <div class="body-cont">
                        @if(isset($productAggregate))
                            @if(count($productAggregate->group_by_name->buckets))
                                @foreach($productAggregate->group_by_name->buckets as $aggregate)
                                    <ul>
                                        @if(count($aggregate->group_by_categoryName->buckets))
                                            @foreach($aggregate->group_by_categoryName->buckets as $aggregateDesigner)
                                                @if(count($aggregateDesigner->group_by_subCategoryName->buckets))
                                                    @foreach($aggregateDesigner->group_by_subCategoryName->buckets as $subcat)
                                                        @if($subcat->key)
                                                            <li>
                                                                @php
                                                                    $subCatName = $subcat->key;
                                                                    array_push($subCat, $subcat->doc_count)
                                                                @endphp
                                                                <i class="icon-ser fa fa-check"></i>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    </ul>
                                @endforeach
                                @php
                                    echo $subCatName . " (" . array_sum($subCat)   .  ")";
                                @endphp
                            @endif
                        @endif
                    </div>
                </div>
            </div>

            @if(isset($productAggregate))
                @if(count($productAggregate->group_by_name->buckets))
                    <div class="btn-group">
                        <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            Product Name
                        </button>
                        <div class="dropdown-menu">
                            <h3 class="header-title">Products Name</h3>
                            <div class="body-cont">
                                <ul>
                                    @foreach($designerResponse->group_by_designerName->buckets as $aggregate)
                                        <li>
                                            <a href="{{route('productPageSearch', 'designerName' )}}?value={{$aggregate->key}}&searchTerm={{request('query')}}">{{$aggregate->key}}
                                                ({{$aggregate->doc_count}})</a>
                                            <i class="icon-ser fa fa-check"></i>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        @endif
    @endif
</section>