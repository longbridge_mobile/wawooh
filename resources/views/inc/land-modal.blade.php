<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Launch demo modal
</button>

<!-- Modal -->


<script>


</script>

@if(empty(request('_token')) || empty(request('query')))
    @if(count($productsResponse))
        <div class="sidebarproduct">
            <div class="filter-price">
                <p>Filter by Price</p>
                <div class="range-slide">
                    <div id="slider-range-elastic"></div>
                    <div class="price-value">
                        <label for="amount-elastic" class="sr-only">Filter</label>
                        <input type="text" id="amount-elastic" readonly
                               style="  border:0; color:#f6931f; font-weight:bold;text-align: center;margin-top: 8px;">
                    </div>
                </div>
            </div>
            <div class="filter-price">
                <p>Filter by Product Ratings</p>
                <div id="filtByPro" class="filterRange score-card">
                    <div>
                        <form action="" class="star-hidden">
                            <input class="star star-5" id="star-5" type="radio" value='5'
                                   name="prodstar"/>
                            <label class="star star-5" for="star-5"></label>
                            <input class="star star-4" id="star-4" type="radio" value='4'
                                   name="prodstar"/>
                            <label class="star star-4" for="star-4"></label>
                            <input class="star star-3" id="star-3" type="radio" value='3'
                                   name="prodstar"/>
                            <label class="star star-3" for="star-3"></label>
                            <input class="star star-2" id="star-2" type="radio" value='2'
                                   name="prodstar"/>
                            <label class="star star-2" for="star-2"></label>
                            <input class="star star-1" id="star-1" type="radio" value='1'
                                   name="prodstar"/>
                            <label class="star star-1" for="star-1"></label>
                            {{-- <span style="float: right">(20200)</span> --}}
                        </form>
                    </div>
                </div>
            </div>
            {{--<div class="filter-price">
                <p>Filter by Designer Ratings</p>
                <div id="filtByDes" class="filterRange score-card">
                    <div>
                        <form action="" class="star-hidden">
                            <input class="star star-5" id="desstar-5" type="radio" value='5'
                                   name="desstar"/>
                            <label class="star star-5" for="desstar-5"></label>
                            <input class="star star-4" id="desstar-4" type="radio" value='4'
                                   name="desstar"/>
                            <label class="star star-4" for="desstar-4"></label>
                            <input class="star star-3" id="desstar-3" type="radio" value='3'
                                   name="desstar"/>
                            <label class="star star-3" for="desstar-3"></label>
                            <input class="star star-2" id="desstar-2" type="radio" value='2'
                                   name="desstar"/>
                            <label class="star star-2" for="desstar-2"></label>
                            <input class="star star-1" id="desstar-1" type="radio" value='1'
                                   name="desstar"/>
                            <label class="star star-1" for="desstar-1"></label>
                            --}}{{-- <span style="float: right">(20200)</span> --}}{{--
                        </form>
                    </div>
                </div>
            </div>--}}
        </div>
    @endif
@else
    @if(count($productsResponse))
        <div class="sidebarproduct">
            <div class="product-name">
                @php
                    $arrayMen = array();
                    $arrayWomen = array();
                @endphp

                @if(isset($productAggregate))
                    @if(count($productAggregate->group_by_name->buckets))
                        @foreach($productAggregate->group_by_name->buckets as $aggregate)
                            @if(count($aggregate->group_by_categoryName->buckets))
                                @foreach($aggregate->group_by_categoryName->buckets as $aggregateDesigner)
                                    @if($aggregateDesigner->key == 'Men')
                                        <div>

                                            @php
                                                array_push($arrayMen, $aggregateDesigner->doc_count);
                                            @endphp
                                        </div>
                                    @elseif($aggregateDesigner->key == 'Women')
                                        <div>
                                            @php
                                                array_push($arrayWomen, $aggregateDesigner->doc_count)
                                            @endphp
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        @php
                            echo "<div class = 'designer-name'><h3>Category Name</h3>";
                            echo "<ul>";
                            echo "<li> Men: " . array_sum($arrayMen) . "</li>";
                            echo "<li> Women: " . array_sum($arrayWomen) . "</li>";
                            echo "</ul></div>";
                        @endphp
                    @endif
                @endif
            </div>
            <hr>
            <div class="designer-search">
                @if(isset($designerResponse))
                    <h3>Designers</h3>
                    @if(count($designerResponse->group_by_designerName->buckets))
                        @foreach($designerResponse->group_by_designerName->buckets as $aggregate)
                            <ul>
                                <li>
                                    <a href="{{route('productPage', 'designerName' )}}?value={{$aggregate->key}}&searchTerm={{request('query')}}">{{strtolower($aggregate->key)}}
                                        ({{$aggregate->doc_count}})</a>
                                </li>
                            </ul>
                        @endforeach
                    @endif
                @endif
            </div>
            <hr>
            <div class="product-name">
                @if(isset($productAggregate))
                    @if(count($productAggregate->group_by_name->buckets))
                        <h3>Products</h3>
                        @foreach($productAggregate->group_by_name->buckets as $aggregate)
                            <ul>
                                <li>
                                    <a href="{{route('productPage', 'name' )}}?value={{$aggregate->key}}&searchTerm={{request('query')}}">{{$aggregate->key}}
                                        ({{$aggregate->doc_count}})</a>
                                </li>
                            </ul>
                        @endforeach
                    @endif
                @endif
            </div>

            @php
                $subCat = array();
               $subCatName ='';
            @endphp

            <hr>
            <div class="designer-search">
                @if(isset($productAggregate))
                    <h3>Sub Category</h3>
                    @if(count($productAggregate->group_by_name->buckets))
                        @foreach($productAggregate->group_by_name->buckets as $aggregate)
                            @if(count($aggregate->group_by_categoryName->buckets))
                                @foreach($aggregate->group_by_categoryName->buckets as $aggregateDesigner)
                                    @if(count($aggregateDesigner->group_by_subCategoryName->buckets))
                                        @foreach($aggregateDesigner->group_by_subCategoryName->buckets as $subcat)
                                            @if($subcat->key)
                                                @php
                                                    $subCatName = $subcat->key;
                                                    array_push($subCat, $subcat->doc_count)
                                                @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        @php
                            echo "<ul><li> " . $subCatName . " (" . array_sum($subCat)   .  ") " . "</li></ul>";
                        @endphp
                    @endif
                @endif
            </div>
            <hr>
            <div class="designer-search">
                P
                <hr>
                <div class="designer-search">
                    <div class="filter-rating">
                        <h3>Filter by Product Ratings</h3>
                        <div id="filtByProElastic" class="filterRange score-card">
                            <div>
                                <form action="" class="star-hidden-search">
                                    <input class="star star-5" id="star-5" type="radio" value='5'
                                           name="prodstar-elastic"/>
                                    <label class="star star-5" for="star-5"></label>
                                    <input class="star star-4" id="star-4" type="radio" value='4'
                                           name="prodstar-elastic"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-3" id="star-3" type="radio" value='3'
                                           name="prodstar-elastic"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-2" id="star-2" type="radio" value='2'
                                           name="prodstar-elastic"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-1" id="star-1" type="radio" value='1'
                                           name="prodstar-elastic"/>
                                    <label class="star star-1" for="star-1"></label>
                                    {{-- <span style="float: right">(20200)</span> --}}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endif
@endif
