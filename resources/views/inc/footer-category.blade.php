@if(isset($resp))
	@if(count($resp))
		@foreach($resp as $category)
			@if(isset($category->categoryName))
			<li>
				<a class=""  href="{{route('fashionCategory', [strtolower($category->categoryName), $category->id])}}">{{title_case($category->categoryName)}}</a>
			</li>
			@else
                <label class="label label-danger">No Internet</label>
			@endif
		@endforeach
	@endif
@else
	<p>No internet connection</p>
@endif