<div class="col-md-3 col-lg-3 effect">
    <div class="holder-container">
        <div class="wishlist-badge">
            @if($product->wishListFlag == 'N' || is_null($product->wishListFlag) || session()->has('userToken'))
                <a title="Save for later"
                   class="trans-0-4" id="add-wishlist"
                   href='{{route('addToWishList', $product->id)}}'>
                    <i class="fa fa-heart"></i>
                </a>
            @elseif(!session()->has('userToken'))
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#ModalSignIn').modal();
                    })
                </script>
                <i class="fa fa-heart" style='color:red'></i>
            @endif
        </div>
        <div class="type-badge">
            @if($product->acceptCustomSizes === 'Y')
                <img src="{{asset('img/bespoke.svg')}}" alt=""
                     class="bes-size">
            @endif
        </div>
        <div class="img-height">
            <a href='{{route('productDetails', str_slug($product->name).'-'.$product->id)}}'>
                @if(count($product->productColorStyleDTOS))
                    @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                        <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                    @endif
                    @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[1]))
                        <img class="under-img"
                             src="{{str_replace(['http','upload'],['https','upload/q_40'], $product->productColorStyleDTOS[0]->productPictureDTOS[1]->picture)}}">
                    @endif
                @endif
            </a>
        </div>
        @if($product->bespokeProductDTO === null)
            <div class="quick-shop">
                <h5>BUY NOW</h5>
                <div class="quick-size">
                    <ul>
                        @if(isset($product->productColorStyleDTOS) && is_array($product->productColorStyleDTOS))
                                @for($i = 0; $i < count($product->productColorStyleDTOS); $i++)

                                <li class="color-radio">
                                    <input type="radio" name="color"
                                           id="c-{{$product->productColorStyleDTOS[$i]->id}}">
                                    <label for="c-{{$product->productColorStyleDTOS[$i]->id}}">
                                        <img class="imgChecks {{$i === 0 ? 'borderDisplay' : ''}} {{$i}}" src="{{str_replace(['http','upload'],['https','upload/q_40'], $product->productColorStyleDTOS[$i]->colourPicture)}}" prodatr="{{$product->productColorStyleDTOS[$i]->id}}"
                                             alt="{{$product->productColorStyleDTOS[$i]->colourName}}">
                                    </label>
                                </li>
                            @endfor
                        @endif
                    </ul>
                    @if(isset($product->productColorStyleDTOS) && is_array($product->productColorStyleDTOS))

                       {{-- @foreach ($product->productColorStyleDTOS as $size)
                            @foreach($size->productSizes as $realSize)
                                <button class="btn btn-size">{{$realSize->name}}</button>
                            @endforeach
                        @endforeach--}}
                        @for($i = 0; $i < count($product->productColorStyleDTOS); $i++)
                            <div class="count-{{$i}} {{$i !== 0 ? 'hide' : ''}}">
                               @for($j = 0; $j < count($product->productColorStyleDTOS[$i]->productSizes); $j++)

                               <button productSize="{{$product->productColorStyleDTOS[$i]->productSizes[$j]->id}}" prodId="{{$product->id}}" designerId="{{$product->designerId}}" amount="{{$product->amount}}" prodatr="{{$product->productColorStyleDTOS[$i]->id}}" designerName="{{$product->designerName}}" slashedPrice="{{$product->slashedPrice}}" class="btn btn-size">{{$product->productColorStyleDTOS[$i]->productSizes[$j]->name}}</button>
                                   @endfor
                            </div>
                        @endfor
                    @endif
                </div>
            </div>
        @endif
        <div class="p-details">
            <span class="p-name display-type">
                <a href='{{route('productDetails', str_slug($product->name).'-'.$product->id)}}'>
                    {{strtolower($product->name)}}
                </a>
            </span>
            <a href="{{route('designer.view', str_slug(strtolower($product->designerName)))}}-{{$product->designerId}}"
               class="d-name">Designer: {{$product->designerName}}</a>
        </div>
        <div class="price">
            @if($product->slashedPrice > 0 )
                <ul>
                    <li>
                        ₦<span>{{number_format($product->slashedPrice)}}</span>
                    </li>
                    <li class="old text-danger">
                        ₦{{number_format($product->amount)}}
                    </li>
                </ul>
                <ul class="discount">
                    <li>
                        &dash;{{$product->percentageDiscount}}%
                    </li>
                </ul>
            @else
                <ul class="pPrice">
                    <li>
                        ₦{{number_format($product->amount)}}
                    </li>
                </ul>
            @endif
        </div>
        <div class="col-md-12 col-6 rating">
            @if(isset($product->productQualityRating) && $product->productQualityRating > 0)
                @for($i= 0; $i < $product->productQualityRating; $i++ )
                    <span class="fa fa-star checked"></span>
                @endfor
                @for($i= 0; $i < (5 - $product->productQualityRating); $i++ )
                    <span class="fa fa-star"></span>
                @endfor
            @endif
        </div>
    </div>
</div>