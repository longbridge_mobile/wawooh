<ul class="ml-auto">
    @if(request()->route()->getName() =='eventsPage' || request()->route()->getName() == 'viewEvent')
    <li>
        <a href="{{ route('fashionStore') }}" class="underline">Shop</a>
    </li>
    @elseif(request()->route()->getName() =='fashionStore')
        <li>
            <a href="{{ route('eventsPage') }}" class="underline">Show Case</a>
        </li>
        @else
        <li>
            <a href="{{ route('eventsPage') }}" class="underline">Show Case</a>
        </li>
    @endif
    {{--<li>
        <a href="{{ route('inspireLanding') }}" class="underline @if(request()->route()->getName() =='inspireLanding') active @endif">Inspire Me</a>
    </li>--}}
    <li>
        <a href="{{ route('cart') }}" class="">
            @if(count($cart)) <sup class='badge brand-bgcolor cart-sup'>{{count($cart)}}</sup> @endif
            <img src="{{asset('img/bag.svg')}}"> <span class="sr-only">Cart</span>
        </a>
    </li>

    @if(!$resp2)
        <li><a class="" href="#" data-toggle="modal"
               data-target="#ModalSignIn"><img src="{{asset('img/user.svg')}}"> <small class="sign-name"><strong>SIGN IN/<br>SIGN UP</strong></small></a></li>
    @else
        <li class="drop-down"><a href="#" id="no-link">{{$resp2->firstName}}&nbsp;<i class="fa fa-fw fa-caret-down"></i></a>
            <div class="dropmenu">
                <ul>
                    <li><a href="{{route('userDashboard')}}">My Account</a></li>
                    <li><a href="{{route('wishlist')}}">Wishlist</a></li>
                    <li><a href="#" id="wallet-stop">Escrow Account:<br>
                            <strong>₦{{number_format($resp2->pocketBalance)}}</strong></a></li>
                    <li><a href="{{route('logout')}}">Logout</a></li>
                </ul>
            </div>
        </li>
    @endif
</ul>