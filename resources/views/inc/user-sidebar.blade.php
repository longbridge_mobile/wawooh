<div class="col-md-1 userMenu" id="left-side">
    <div class="side-menu" id="left-inner">
        <ul>
            <li>
                <a class="{{request()->route()->getName() == 'userDashboard' ? 'active':''}}"
                   href="{{route('userDashboard')}}">
                    <span><i class="fa fa-home"></i></span>
                    <span class="m-text">My Info</span>
                </a>
            </li>
            <li>
                <a class="{{request()->route()->getName() == 'userOrders' ? 'active':''}}"
                   href="{{route('userOrders')}}">
                    <span><i class="fa fa-globe"></i></span>
                    <span class="m-text">Orders</span>
                </a>
            </li>
            <li>
                <a class="{{request()->route()->getName() == 'userBespokeRequest' ? 'active':''}}"
                   href="{{route('userBespokeRequest')}}">
                    <span><i class="fa fa-scissors"></i></span>
                    <span class="m-text">Bespoke Requests</span>
                </a>
            </li>
            @if(request()->route()->getName() == 'trackOrder' || request()->route()->getName() == 'paymentHistory')
                <li>
                    <a href="{{route('paymentHistory')}}" class="{{request()->route()->getName() == 'paymentHistory' ? 'active':''}}">
                        <span><i class="fa fa-dollar"></i></span>
                        <span class="m-text">Payments</span>
                    </a>
                </li>
            @endif
            <li>
                <a class="{{request()->route()->getName() == 'userMeasurement' ? 'active':''}}"
                   href="{{route('userMeasurement')}}">
                    <span><i class="fas fa-ruler"></i></span>
                    <span class="m-text">My Measurements</span>
                </a>
            </li>
            <li>
                <a class="{{request()->route()->getName() == 'addressBook' ? 'active':''}}"
                   href="{{route('addressBook')}}">
                    <span><i class="fa fa-book"></i></span>
                    <span class="m-text">Address Book</span>
                </a>
            </li>
            <li>
                <a class="{{request()->route()->getName() == 'settings' ? 'active':''}}"
                   href="{{route('settings')}}">
                    <span><i class="fa fa-user"></i></span>
                    <span class="m-text">Personal Information</span>
                </a>
            </li>
            {{--<li>
                <a href="{{route('logout')}}">
                    <span><i class="fa fa-power-off"></i></span>
                    <span class="m-text">Logout</span></a>
            </li>--}}
        </ul>
    </div>
</div>