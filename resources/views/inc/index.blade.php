@push('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        .card-header {
            cursor: pointer;
        }

        a, a:hover {
            color: inherit !important;
            text-decoration: none;
        }

        #products {
            min-height: 500px;
        }

        .side {
            position: relative;
        }

        #sidebar {
            position: absolute;
            height: 466px;
            overflow-y: auto;
        }

    </style>
@endpush

@extends('layouts.user.default')

@section('pageTitle', isset($sub_title) ? $sub_title : '' . ' | ' . isset($cat_title) ? $cat_title : $cat_title . ' | Category')
@section('content')

    <div>
        <div class="category-men">
            @if(strtolower(request('query')) == 'men')
                <img src="https://res.cloudinary.com/har9qnw3d/image/upload/v1538660912/banners/shop/cat_men.jpg">
            @elseif(strtolower(request('query')) == 'women')
                <img src="https://res.cloudinary.com/har9qnw3d/image/upload/v1538660912/banners/shop/cat_women.jpg">
            @elseif(strtolower(request('query')) == 'kids')
                <img src="https://res.cloudinary.com/har9qnw3d/image/upload/v1538660910/banners/shop/cat_kid.jpg">
            @else
                <img src="https://res.cloudinary.com/har9qnw3d/image/upload/v1538488030/banners/shop/an4_banner.jpg">
            @endif
        </div>
    </div>

    <section class="products-cat-shop">
        @if(empty(request('_token')) || empty(request('query')))
            @if(count($productsResponse))
                <div class="sidebarproduct">
                    <div class="filter-price">
                        <p>Filter by Price</p>
                        <div class="range-slide">
                            <div id="slider-range-elastic"></div>
                            <div class="price-value">
                                <label for="amount-elastic" class="sr-only">Filter</label>
                                <input type="text" id="amount-elastic" readonly
                                       style="  border:0; color:#f6931f; font-weight:bold;text-align: center;margin-top: 8px;">
                            </div>
                        </div>
                    </div>
                    <div class="filter-price">
                        <p>Filter by Product Ratings</p>
                        <div id="filtByPro" class="filterRange score-card">
                            <div>
                                <form action="" class="star-hidden">
                                    <input class="star star-5" id="star-5" type="radio" value='5'
                                           name="prodstar"/>
                                    <label class="star star-5" for="star-5"></label>
                                    <input class="star star-4" id="star-4" type="radio" value='4'
                                           name="prodstar"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-3" id="star-3" type="radio" value='3'
                                           name="prodstar"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-2" id="star-2" type="radio" value='2'
                                           name="prodstar"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-1" id="star-1" type="radio" value='1'
                                           name="prodstar"/>
                                    <label class="star star-1" for="star-1"></label>
                                    {{-- <span style="float: right">(20200)</span> --}}
                                </form>
                            </div>
                        </div>
                    </div>
                    {{--<div class="filter-price">
                        <p>Filter by Designer Ratings</p>
                        <div id="filtByDes" class="filterRange score-card">
                            <div>
                                <form action="" class="star-hidden">
                                    <input class="star star-5" id="desstar-5" type="radio" value='5'
                                           name="desstar"/>
                                    <label class="star star-5" for="desstar-5"></label>
                                    <input class="star star-4" id="desstar-4" type="radio" value='4'
                                           name="desstar"/>
                                    <label class="star star-4" for="desstar-4"></label>
                                    <input class="star star-3" id="desstar-3" type="radio" value='3'
                                           name="desstar"/>
                                    <label class="star star-3" for="desstar-3"></label>
                                    <input class="star star-2" id="desstar-2" type="radio" value='2'
                                           name="desstar"/>
                                    <label class="star star-2" for="desstar-2"></label>
                                    <input class="star star-1" id="desstar-1" type="radio" value='1'
                                           name="desstar"/>
                                    <label class="star star-1" for="desstar-1"></label>
                                    --}}{{-- <span style="float: right">(20200)</span> --}}{{--
                                </form>
                            </div>
                        </div>
                    </div>--}}
                </div>
            @endif
        @else
            @if(count($productsResponse))
                <div class="sidebarproduct">
                    <div class="product-name">
                        @php
                            $arrayMen = array();
                            $arrayWomen = array();
                        @endphp

                        @if(isset($productAggregate))
                            @if(count($productAggregate->group_by_name->buckets))
                                @foreach($productAggregate->group_by_name->buckets as $aggregate)
                                    @if(count($aggregate->group_by_categoryName->buckets))
                                        @foreach($aggregate->group_by_categoryName->buckets as $aggregateDesigner)
                                            @if($aggregateDesigner->key == 'Men')
                                                <div>

                                                    @php
                                                        array_push($arrayMen, $aggregateDesigner->doc_count);
                                                    @endphp
                                                </div>
                                            @elseif($aggregateDesigner->key == 'Women')
                                                <div>
                                                    @php
                                                        array_push($arrayWomen, $aggregateDesigner->doc_count)
                                                    @endphp
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                                @php
                                    echo "<div class = 'designer-name'><h3>Category Name</h3>";
                                    echo "<ul>";
                                    echo "<li> Men: " . array_sum($arrayMen) . "</li>";
                                    echo "<li> Women: " . array_sum($arrayWomen) . "</li>";
                                    echo "</ul></div>";
                                @endphp
                            @endif
                        @endif
                    </div>
                    <hr>
                    <div class="designer-search">
                        @if(isset($designerResponse))
                            <h3>Designers</h3>
                            @if(count($designerResponse->group_by_designerName->buckets))
                                @foreach($designerResponse->group_by_designerName->buckets as $aggregate)
                                    <ul>
                                        <li>
                                            <a href="{{route('productPage', 'designerName' )}}?value={{$aggregate->key}}&searchTerm={{request('query')}}">{{strtolower($aggregate->key)}}
                                                ({{$aggregate->doc_count}})</a>
                                        </li>
                                    </ul>
                                @endforeach
                            @endif
                        @endif
                    </div>
                    <hr>
                    <div class="product-name">
                        @if(isset($productAggregate))
                            @if(count($productAggregate->group_by_name->buckets))
                                <h3>Products</h3>
                                @foreach($productAggregate->group_by_name->buckets as $aggregate)
                                    <ul>
                                        <li>
                                            <a href="{{route('productPage', 'name' )}}?value={{$aggregate->key}}&searchTerm={{request('query')}}">{{$aggregate->key}}
                                                ({{$aggregate->doc_count}})</a>
                                        </li>
                                    </ul>
                                @endforeach
                            @endif
                        @endif
                    </div>

                    @php
                        $subCat = array();
                       $subCatName ='';
                    @endphp

                    <hr>
                    <div class="designer-search">
                        @if(isset($productAggregate))
                            <h3>Sub Category</h3>
                            @if(count($productAggregate->group_by_name->buckets))
                                @foreach($productAggregate->group_by_name->buckets as $aggregate)
                                    @if(count($aggregate->group_by_categoryName->buckets))
                                        @foreach($aggregate->group_by_categoryName->buckets as $aggregateDesigner)
                                            @if(count($aggregateDesigner->group_by_subCategoryName->buckets))
                                                @foreach($aggregateDesigner->group_by_subCategoryName->buckets as $subcat)
                                                    @if($subcat->key)
                                                        @php
                                                            $subCatName = $subcat->key;
                                                            array_push($subCat, $subcat->doc_count)
                                                        @endphp
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                                @php
                                    echo "<ul><li> " . $subCatName . " (" . array_sum($subCat)   .  ") " . "</li></ul>";
                                @endphp
                            @endif
                        @endif
                    </div>
                    <hr>
                    <div class="designer-search">
                        <div class="filter-price-elastic">
                            <h3>Filter by Price</h3>
                            <div class="range-slide">
                                <div id="slider-range" hidden></div>
                                <div class="price-value" hidden>
                                    <input type="text" id="amount" readonly
                                           style="border:0; color:#f6931f; font-weight:bold;text-align: center;margin-top: 8px;">
                                </div>
                            </div>

                            <div class="range-slide">
                                <div id="slider-range-elastic"></div>
                                <div class="price-value">
                                    <input type="text" id="amount-elastic" readonly
                                           style="border:0; color:#f6931f; font-weight:bold;text-align: center;margin-top: 8px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="designer-search">
                        <div class="filter-rating">
                            <h3>Filter by Product Ratings</h3>
                            <div id="filtByProElastic" class="filterRange score-card">
                                <div>
                                    <form action="" class="star-hidden-search">
                                        <input class="star star-5" id="star-5" type="radio" value='5'
                                               name="prodstar-elastic"/>
                                        <label class="star star-5" for="star-5"></label>
                                        <input class="star star-4" id="star-4" type="radio" value='4'
                                               name="prodstar-elastic"/>
                                        <label class="star star-4" for="star-4"></label>
                                        <input class="star star-3" id="star-3" type="radio" value='3'
                                               name="prodstar-elastic"/>
                                        <label class="star star-3" for="star-3"></label>
                                        <input class="star star-2" id="star-2" type="radio" value='2'
                                               name="prodstar-elastic"/>
                                        <label class="star star-2" for="star-2"></label>
                                        <input class="star star-1" id="star-1" type="radio" value='1'
                                               name="prodstar-elastic"/>
                                        <label class="star star-1" for="star-1"></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        <div class="productshow">
            @if(count($productsResponse))
                <div id="products" class="container product-pics">
                    @foreach(array_chunk($productsResponse, 4) as $products)
                        <div class="row-holder">
                            @foreach($products as $product)
                                <div class="row-3 white effect">
                                    <div class="holder-container">
                                        <div class="wishlist-badge">
                                            @if($product->wishListFlag == 'N' || is_null($product->wishListFlag) || session()->has('userToken'))
                                                <a title="Save for later"
                                                   class="trans-0-4" id="add-wishlist"
                                                   href='{{route('addToWishList', $product->id)}}'>
                                                    <i class="fa fa-heart"></i>
                                                </a>
                                            @elseif(!session()->has('userToken'))
                                                <script type="text/javascript">
                                                    $(document).ready(function () {
                                                        $('#ModalSignIn').modal();
                                                    })
                                                </script>
                                                <i class="fa fa-heart" style='color:red'></i>
                                            @endif
                                        </div>
                                        <div class="type-badge">
                                            @if($product->acceptCustomSizes === 'Y')
                                                <img src="{{asset('img/bespoke.svg')}}" alt="" class="bes-size">
                                            @endif
                                        </div>
                                        <div class="img-height">
                                            <a href='{{route('productDetails', str_slug($product->name).'-'.$product->id)}}'>
                                                @if(count($product->productColorStyleDTOS))
                                                    @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                        <img src="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                    @endif
                                                    @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[1]))
                                                        <img class="under-img"
                                                             src="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[1]->picture)}}">
                                                    @endif
                                                @endif
                                            </a>
                                        </div>
                                        <div class="p-details">
                                            <span class="p-name display-type">
                                                <a href='{{route('productDetails', str_slug($product->name).'-'.$product->id)}}'>
                                                    {{$product->name}}
                                                </a>
                                            </span>
                                            <a href="{{route('designer.view', str_slug(strtolower($product->designerName)))}}-{{$product->designerId}}"
                                               class="d-name">Designer: {{$product->designerName}}</a>
                                        </div>
                                        <div class="price">
                                            @if($product->slashedPrice > 0 )
                                                <ul>
                                                    <li>
                                                        ₦<span>{{number_format($product->slashedPrice)}}</span>
                                                    </li>
                                                    <li class="old text-danger">
                                                        ₦{{number_format($product->amount)}}
                                                    </li>
                                                </ul>
                                                <ul class="discount">
                                                    <li>
                                                        &dash;{{$product->percentageDiscount}}
                                                        %
                                                    </li>
                                                </ul>
                                            @else
                                                <ul class="pPrice">
                                                    <li>
                                                        ₦ {{number_format($product->amount)}}
                                                    </li>
                                                </ul>
                                            @endif
                                        </div>
                                        <div class="col-md-12 col-6 rating">
                                            @if(isset($product->productQualityRating) && $product->productQualityRating > 0)
                                                @for($i= 0; $i < $product->productQualityRating; $i++ )
                                                    <span class="fa fa-star checked"></span>
                                                @endfor
                                                @for($i= 0; $i < (5 - $product->productQualityRating); $i++ )
                                                    <span class="fa fa-star"></span>
                                                @endfor
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            @else
                <div class="no-product">
                    <span class='no-result-head'>No Product Found</span>
                    <div class='no-result-body'>
                        Try searching for a product or a keyword
                        <h4>Still can't find your desired product, <a href="{{route('userFeedBack')}}">contact us</a>
                        </h4>
                    </div>
                </div>
            @endif
            <div id='loaderf' style='display:none' class='text-center'>
                <img style='width:100px' class='center-block' src='{{asset('img/loader.gif')}}'/>
            </div>
            <div class="no-product">

            </div>
            <div class="no-p"></div>
        </div>
    </section>

    <div class="clearboth"></div>
    <div style="clear: both;">
        @include('inc.subscribe')
    </div>

@endsection

@push('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/products.js') }}"></script>
    <script>
        $(document).ready(function () {

            /*$('.productNameSub').click(function(){
               var data = {
                   indexName: "products",
{{--                   searchTerm: "{{request('query')}}",--}}
            pageNumber: 0,
            size:9,
            terms: [
                {
                    fieldName: "name",
                    values: [
                        $(this).attr('prodCat')
                    ]
                }
            ]
        }
        console.log(data);
        $.ajax({
{{--                   url: "{{route('get.productcat')}}",--}}
            type: 'POST',
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(result){
                console.log(result);
                $('#products').html('');
                result.forEach(function(value){
                $('#products').append(`

                   <div class="col-md-3">
                   <div class="holder-container">
                   <div class="img-height">
                   <a href="">
                   <img src="${value.productAttributeDTOS[0].productPictureDTOS[0].picture}">

                   </a>
                   </div>
                   </div>

                   </div>`);
                });

            },
            error: function(error){
                console.log(error);
            }

        })
     });*/

            flag = 1;
            page = 0;
            size = 6;
            pageNumber = 1;
            ajax = true;
            var navHeight = $('.header').height() * 2;
            var check = true;

            var filter = {
                "fromPrice": '{{(!empty(request('price')))?explode('-',request('price'))[0]:''}}',
                "toPrice": '{{(!empty(request('price')))?explode('-',request('price'))[1]:' '}}',
                "subCategoryId": '{{(!empty(request('subcatId')))?request('subcatId'):" "}}',
                "productQualityRating": '{{(!empty(request('rating')))?request('rating'):" "}}',
                "productName": '{{(!empty(request('name')))?request('name'):''}}',
                "designerRating": '{{(!empty(request('desrat')))?request('desrat'):''}}'
            };

            @if(!empty(request('prodrat')))
            $('input:radio[name="prodstar"][value="{{request('prodrat')}}"]').prop("checked", true);
            @endif

            @if(!empty(request('desrat')))
            $('input:radio[name="desstar"][value="{{request('desrat')}}"]').prop("checked", true);
            @endif

            $(window).scroll(function () {

                let noProduct = `
                            <span class = 'no-result-head'>No Result Found</span>
                            <div class = 'no-result-body'>
                                Try searching for a product or a keyword
                                <h4>Still can't find your desired product, <a href="tel:07000-0-WAWOOH">contact us</a></h4>
                            </div>`;


                if ($("#products").height() <= (window.scrollY + navHeight)) {

                    if (check) {
                        check = false;
                        page++;
                                @if(!empty(request('price')) || !empty(request('name')) || !empty(request('rating')) || !empty(request('prodrat')) || !empty(request('desrat')))
                        var data = {
                                "page": page,
                                "size": 6,
                                "subCategoryId": '{{(!empty(request('subcatId')))?request('subcatId'):" "}}',
                                "fromPrice": '{{(!empty(request('price')))?explode('-',request('price'))[0]:''}}',
                                "toPrice": '{{(!empty(request('price')))?explode('-',request('price'))[1]:' '}}',
                                "productQualityRating": '{{(!empty(request('rating')))?request('rating'):" "}}',
                                "productName": '{{(!empty(request('name')))?request('name'):''}}',
                                "designerRating": '{{(!empty(request('desrat')))?request('desrat'):''}}'
                            }

                        var url = '{{route('getMoreFilterProducts')}}';
                                @elseif(!empty(request('fieldName')))
                        var data = {
                                indexName: 'products',
                                pageNumber: pageNumber++,
                                size: 9,
                                searchTerm: '{{request('subcategory')}}',
                                min: parseInt({{request('min')}}),
                                max: parseInt({{request('max')}})
                            }
                        var url = '{{route('getMoreSliderElasticProduct')}}';
                                @elseif(!empty(request('query')))
                        var data = {
                                indexName: 'products',
                                pageNumber: pageNumber++,
                                size: 10,
                                searchTerm: '{{request('query')}}',
                            };

                        var url = '{{route('getMoreElasticProducts')}}';

                                @else
                        var data = {
                                "page": page,
                                "size": size,
                                "subcategoryId": '{{request('subcatId')}}'
                            };

                        var url = '{{route('getMoreProducts')}}';
                        @endif
                        $.ajax({
                            type: 'POST',
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            url: url,
                            beforeSend: function () {
                                $('#loaderf').show();
                            },
                            success: function (result) {
                                ajax = true;
                                $('#loaderf').hide();
                                if (result.status) {
                                    // $.notify(result.message, 'error');
                                    if (result.status === 200) {
                                        $.when(appendAllElasticProductsToDocument(result.data, '#products')).done(check = true);
                                    }
                                }
                                else {
                                    if (result.length > 0) {
                                        $.when(appendAllProductsToDocument(result, '#products')).done(check = true);
                                    } else {
                                        // alert('Nothing');
                                        // $('.no-product').html(noProduct);
                                        check = false;
                                    }
                                }
                            },
                            error: function (e) {
                                check = false;
                                $('#loaderf').hide();
                            }
                        });
                    }
                }
            });


            $('input[name="prodstar-elastic"]').click(function () {
                if (filter.fromPrice !== '') {
                    price = '&price=' + filter.fromPrice + '-' + filter.toPrice;
                } else {
                    price = '&price=0-100000';
                }
                if (filter.productName !== '') {
                    prodNam = filter.productName;
                } else {
                    prodNam = '';
                }
                if (typeof filter.productName === 'undefined') {
                    prodNam = '';
                }
                var desfit = '@if(!empty(request('desrat')))&desrat={{request('desrat')}}@endif';
                window.location.href = '{{route('productPage', request('subcategory'))}}?elasticSearch={{request('query')}}&prodelasticrat=' + $(this).val() + desfit;
                //console.log($(this).val());
            });


            $('input[name="prodstar"]').click(function () {
                if (filter.fromPrice !== '') {
                    price = '&price=' + filter.fromPrice + '-' + filter.toPrice;
                } else {
                    price = '&price=0-100000';
                }
                if (filter.productName !== '') {
                    prodNam = filter.productName;
                } else {
                    prodNam = '';
                }
                if (typeof filter.productName === 'undefined') {
                    prodNam = '';
                }
                var desfit = '@if(!empty(request('desrat')))&desrat={{request('desrat')}}@endif';
                {{--window.location.href = '{{route('productPage', request('subcategory'))}}?subcatId={{request('subcatId')}}' + price + '&c={{request('c')}}&name=' + prodNam + '&prodrat=' + $(this).val() + desfit;--}}
                    window.location.href = '{{route('productPage', request('subcategory'))}}?subcatId={{request('subcatId')}}&cat={{request('subcatId')}}&prodrat=' + $(this).val();
            });

            $('input[name="desstar"]').click(function () {
                if (filter.fromPrice !== '') {
                    price = '&price=' + filter.fromPrice + '-' + filter.toPrice;
                } else {
                    price = '&price=0-100000';
                }
                if (filter.productName !== '') {
                    prodNam = filter.productName;
                } else {
                    prodNam = '';
                }
                if (typeof filter.productName === 'undefined') {
                    prodNam = '';
                }
                var prdfit = '@if(!empty(request('prodrat')))&prodrat={{request('prodrat')}}@endif';
                window.location.href = '{{route('productPage', request('subcategory'))}}?subcatId={{request('subcatId')}}' + price + '&c={{request('c')}}&name=' + prodNam + '&desrat=' + $(this).val() + prdfit;
                //console.log($(this).val());
            });
        });

        $(function () {
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: 100000,
                values: @if(!empty(request('price')))

                [{{str_replace('-',',',request('price'))}}]
                        @else [100, 100000]
                @endif,
                slide: function (event, ui) {
                    $("#amount").val("₦" + ui.values[0] + " - ₦" + ui.values[1]);
                },
                change: function () {
                    var prices = $("#slider-range").slider("values");
                    prices = prices.join('-');
                    var q = '@if(!empty(request('query')))&query={{request('query')}}@else(!empty(request('name')))&name={{request('name')}}  @endif';
                    var r = '@if(!empty(request('rating')))&rating={{request('rating')}}  @endif';
                    var prdfit = '@if(!empty(request('prodrat')))&prodrat={{request('prodrat')}}@endif';
                    var desfit = '@if(!empty(request('desrat')))&desrat={{request('desrat')}}@endif';
                    window.location.href = '{{route('productPage', request('subcategory'))}}?subcatId={{request('subcatId')}}&c={{request('c')}}&price=' + prices + q + r + prdfit + desfit;
                }
            });
            $("#amount").val("₦" + $("#slider-range").slider("values", 0) +
                " - ₦" + $("#slider-range").slider("values", 1));
        });

        $(function () {
            $("#slider-range-elastic").slider({
                range: true,
                min: 0,
                max: 100000,
                values: @if(!empty(request('amount')))

                [{{str_replace('-',',',request('amount'))}}]
                        @elseif(!empty(request('fieldName')) && !empty(request('min')) && !empty(request('max')))
                    ['{{request('min')}}', '{{request('max')}}']
                        @else [100, 100000]
                @endif,
                slide: function (event, ui) {
                    $("#amount-elastic").val("₦" + ui.values[0] + " - ₦" + ui.values[1]);
                },
                change: function () {
                    var searchTerm = '{{request('query')}}' ? '{{request('query')}}' : '{{request('searchTerm')}}';
                    var prices = $("#slider-range-elastic").slider("values");
                    prices = prices.join('-');
                    var priceRange = prices.split('-');
                    var q = '@if(!empty(request('amount'))) &amount={{request('amount')}}@else(!empty(request('amount')))&amount={{request('amount')}}  @endif';
                    var min = parseInt(priceRange[0]);
                    var max = parseInt(priceRange[1]);
                    window.location.href = '{{route('productPage', request('subcategory'))}}?cat={{request('cat')}}&subcatId={{request('subcatId')}}&price=' + min + '-' + max + '&searchTerm=' + searchTerm;

                }
            });

            @if(!empty(request('min')) && !empty(request('max')))
            $("#amount-elastic").val('₦{{request('min')}} - ₦{{request('max')}}');
            @else
            {{--$("#amount-elastic").val('₦{{request('min')}} - ₦{{request('max')}}');--}}
            $("#amount-elastic").val("₦" + $("#slider-range-elastic").slider("values", 0) +
                " - ₦" + $("#slider-range-elastic").slider("values", 1));
            @endif

        });

        $(document).ready(function () {

            $("#myRange").on("input change", function () {
                var amt = $(this).val() * 1000;
                $('.price-value').html('&#8358;' + amt.toLocaleString());
            });

            $('#myRange').on('change', function () {
                // console.log($("#slider-range").slider("values"));
            });
        });


        {{-- $('.add').on('click', function(){

            var data = {
                "productId": "{{$resp2->id}}",
                "quantity": $('#q').val(),
                "designerId": "{{$resp2->designerId}}",
                "size": $('#s').val(),
                "amount": "{{$resp2->amount}}",
                "artWorkPictureId": null,
                "materialPictureId": null,
                "color": $('[name=colorMain]:checked').val(),
                "materialStatus": "N",
                "materialLocation": "",
                "materialPickupDate": "",
                "expiryDate": ""
            }
            //console.log(data);
            addToCart(data);
        }); --}}

        $(function () {
            var $sidebar = $("#sidebar"),
                $window = $(window),
                offset = $sidebar.offset(),
                topPadding = 15;

            $window.scroll(function () {
                /*if ($window.scrollTop() > offset.top) {
                    $sidebar.stop().animate({
                        marginTop: $window.scrollTop() - offset.top + topPadding
                    });
                } else {
                    $sidebar.stop().animate({
                        marginTop: 0
                    });
                }*/

                if (window.scrollY + 80 >= $("#products").offset().top && window.scrollY + 80 < $("#products").height()) {
                    $("#sidebar").css("bottom", $("#products").height() - (window.scrollY));
                    $("#sidebar").css("top", "auto");
                } else if (window.scrollY + 80 >= $("#products").height()) {
                    $("#sidebar").css("bottom", 40);
                    $("#sidebar").css("top", "auto");
                } else {
                    $("#sidebar").css("bottom", "auto");
                    $("#sidebar").css("top", "0");
                }

            });
        });

        /*$('#products').infiniteScroll({
            // options
            path: 'www.google.com',
            append: '.post',
            history: false,
        });*/
    </script>
@endpush

