<script>
    $(document).ready(function () {
        $(".dofb").datepicker({
            dateFormat: 'yy/m/d',
            changeYear: true,
            changeMonth: true,
            yearRange: '1960:2000'
        });
        errors = 0;

        $('#form').on('submit', function (event) {
            event.preventDefault();
            let query = $('#query').val();
            if (query === '') {
                $('#form').notify('Enter a query', 'error');
            } else {
                $(this).attr('action', '{{route('eventsPage')}}?query=' + query);

                $(this).unbind('submit').submit()
            }
        });

        $('#form2').on('submit', function (event) {
            event.preventDefault();
            let query = $('#query2').val();
            if (query === '') {
                $('#form2').notify('Enter a query', 'error');
            } else {
                $(this).attr('action', '{{route('productPageSearch','search')}}?query=' + query);

                $(this).unbind('submit').submit()
            }
        });

        $('#signUp').on('click', function () {
            $('#ModalSignIn').modal('hide');
        });

        $('#signIn').on('click', function () {
            $('#ModalSignUp').modal('hide');
        });

        $('a.logina').on('click', function () {
            $('#ModalSignIn').modal();
            $('#ModalSignUp').modal('hide');
        });

        $('.login').on('click', function (e) {
            e.preventDefault();
            $('#loginBtn').val('Logging in...').attr("disabled", "disabled");
            $('#loaderLog').show();
            $(this).removeClass('no-error');
            $('.dataField').each(function () {
                if (fieldIsEmpty($(this))) {
                    errors++;
                    $(this).notify(
                        "This field is empty",
                        {position: "right"}
                    );
                    highlight($(this), 'has-error');
                }
                else {
                    highlight($(this), 'no-error');
                    errors = 0
                }
            });

            if (errors === 0) {
                $('.login-field').attr('disabled', 'disabled');
                let em = $('#email').val();
                let pw = $('#password').val();
                let data = {
                    "email": em,
                    "password": pw,
                    "socialFlag": 'N'
                };
                let encodeEmail = btoa(data.email);
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    //processData: false,
                    url: '{{route('login')}}',
                    success: function (result) {
                        $('#loaderLog').hide();
                        if (result.status === '00') {
                            $.notify('Login Successful', 'success');
                            $('#ModalSignIn').modal('hide');
                            location.reload();
                        } else if (result.status === '290') {
                            $.notify('Login Successful', 'success');
                            $('#ModalSignIn').modal('hide');
                        }
                        else {
                            $('#loginBtn').val('SIGN IN').removeAttr("disabled", "disabled");
                            // $('#ModalSignIn').modal('hide');
                            swal(result.message, '', 'error');
                        }
                    },
                    error: function (e) {
                        $('#loginBtn').val('SIGN IN').removeAttr("disabled", "disabled");
                        swal(e, '', 'error');
                    },
                })

            }
        });

        $('.login2').on('click', function () {
            $('#loaderLog2').show();
            let em = $('#email2').val();
            let pw = $('#password2').val();
            if (em.length > 0 && pw.length > 0) {
                if (!isEmail(em)) {
                    $('#email2').notify('Not a Valid email', 'error');
                    errors++;
                }

                let data = {
                    "email": em,
                    "password": pw
                };
                if (errors === 0) {
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        url: '{{route('login')}}',
                        success: function (result) {
                            $('#loaderLog2').hide();
                            if (result.status === '00') {
                                // localStorage.setItem('userToken', JSON.stringify(result.data.token));
                                $.notify('Login Successful', 'success');
                                $('#ModalSignIn2').modal('hide');
                                location.reload();

                            } else if (result.status === '57') {
                                $('#ModalSignIn').modal('hide');
                                window.location.href = '{{route('user.accountStatus')}}?email=' + encodeEmail;
                            }
                            else {
                                // $('#email2').notify(result.message, 'error');
                                swal(result.message, '', 'error');
                            }
                        },
                        error: function (e) {
                            $('#loaderLog2').hide();
                            swal(e, '', 'error');
                        },
                    });
                }
            } else {
                swal('Please check your inputs', '', 'error');
            }
        });

        $('#signup').click(function () {
            $('#signup').val('Please wait...').attr('disabled', 'disabled');
            let emailVal = $('#emSign').val();
            let valueForm = $('.signup2').val();
            if ($('#lname').val() === '' || $('#fname').val() === '' || $('#emSign').val() === '' || $('#passwordSign').val() === '') {
                swal('All fields are required', '', 'warning');
                $(this).val('CREATE AN ACCOUNT').removeAttr('disabled', 'disabled');
            } else if (!isEmail(emailVal)) {
                $('#signup').val('CREATE AN ACCOUNT').removeAttr('disabled', 'disabled');
                swal('Enter valid email', '', 'warning');
            }
            else {
                let data = {
                    "lastName": $('#lname').val(),
                    "firstName": $('#fname').val(),
                    "email": $('#emSign').val(),
                    "phoneNo": $('#userPhone').val(),
                    "password": $('#passwordSign').val(),
                    "role": '{{env('USER_ROLE')}}',
                    "socialFlag": 'N'
                };
                console.log(data);
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: `{{route('userRegistration')}}`,
                    success: function (result) {
                        console.log(result);
                        if (result.status === "00") {
                            $('#signup').val('Account Created Successfully').attr('disabled', 'disabled');
                            swal('Account created successfully, update your account details', '', 'success');
                            location.reload();
                        } else {
                            // swal(result.message, '', 'error');
                            $('#signup').val('CREATE AN ACCOUNT').removeAttr('disabled', 'disabled');
                        }
                    },
                    error: function (e) {
                        $('#signup').val('CREATE AN ACCOUNT').removeAttr('disabled', 'disabled');
                        console.log(e);
                    },
                });
            }
        });

        $('.subMe').on('click', function () {
            $('#subMeAction').val('Adding email').attr('disabled', 'disabled');
            let subEmail = $('#subEmail').val();
            if (subEmail === '') {
                swal('Hey!, you can\'t submit empty field', '', 'warning');
                $('#subMeAction').val('Subscribe').removeAttr('disabled', 'disabled');
            } else if (!isEmail(subEmail)) {
                swal('Hey!, Wrong Data', '', 'error');
                $('#subMeAction').val('Subscribe').removeAttr('disabled', 'disabled');
            }
            else {
                let data = {
                    "email": subEmail
                };
                // console.log(data);
                $.ajax({
                    url: '{{env('GET_BASE_URL')}}{{env('USER_ACCOUNT_API')}}newsletter/add',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (result) {
                        // console.log(result);
                        if (result.status === "00") {
                            $('#subMeAction').val('Subscribed').attr('disabled', 'disabled');
                            swal('Successfully Subscribed', '', 'success');
                            //location.reload();
                        }
                        else {
                            swal('Something went wrong. Try Again', '', 'error');
                            $('#subMeAction').val('Subscribe').removeAttr('disabled', 'disabled');
                        }
                    },
                    error: function (e) {
                        $('#subMeAction').val('Subscribe').removeAttr('disabled', 'disabled');
                        swal(e, '', 'error');
                    }
                })
            }
        });

        $('#actionSub').on('click', function () {
            $('#actionSub').val('Please wait...').attr('disabled', 'disabled');
            let subEmail = $('#subEmail2').val(),
                firstName = $('#firstName').val();
            if (subEmail === '' || firstName === '') {
                $('.req').addClass('error');
                // swal('Hey!, you can\'t submit empty field', '', 'warning');
                $('#actionSub').val('Shop Here').removeAttr('disabled', 'disabled');
            } else if (!isEmail(subEmail)) {
                swal('Hey!, Wrong Data', '', 'error');
                $('#actionSub').val('Shop Here').removeAttr('disabled', 'disabled');
            }
            else {
                let data = {
                    "firstName": firstName,
                    "email": subEmail
                };
                // console.log(data);
                $.ajax({
                    url: '{{env('GET_BASE_URL')}}{{env('USER_ACCOUNT_API')}}newsletter/add',
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (result) {
                        // console.log(result);
                        if (result.status === "00") {
                            $('#actionSub').val('Hold on... Taking you to our val collections').attr('disabled', 'disabled');
                            // swal('Successfully', '', 'success');
                            $('#resultMsg').removeClass('hide');
                            $('#resultBtn').addClass('hide');
                            window.location.href = "{{route('valCollection')}}";
                        }
                        else {
                            $('#resultMsg').addClass('hide');
                            $('#resultBtn').removeClass('hide');
                            swal('Something went wrong. Try Again', '', 'error');
                            $('#actionSub').val('Shop Here').removeAttr('disabled', 'disabled');
                        }
                    },
                    error: function (e) {
                        $('#resultMsg').addClass('hide');
                        $('#resultBtn').removeClass('hide');
                        $('#actionSub').val('Shop Here').removeAttr('disabled', 'disabled');
                        swal(e, '', 'error');
                    }
                })
            }
        });

        @if(session()->has('success'))

        swal('{{session('success')}}', '', 'success');

        @endif

        @if(!empty($flasherror))
        swal('{{$flasherror}}', '', 'error');
        // console.log($flasherror);
        @endif

    });

    function renderButton() {
        gapi.signin2.render('gSignIn', {
            'scope': 'profile email',
            'width': 240,
            'height': 50,
            'longtitle': true,
            'theme': 'dark',
            'onsuccess': onSuccess,
            'onfailure': onFailure
        })
    }

    function onSuccess(googleUser) {
        let profile = googleUser.getBasicProfile();
        console.log(profile);
    }

    function onFailure(error) {
        console.log(error)
    }
</script>