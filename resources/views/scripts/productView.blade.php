<script>
    $($('.productColor ul li')[0]).click(function () {
        <?php if(!is_null(session('designerToken'))): ?>
            tok = <?= session('designerToken') ?>;
        <?php else: ?>
            tok = '';
        <?php endif; ?>
        $.ajax({
            url: `https://wawoohapi.herokuapp.com/fashion/product/${productId}/designer/getproductbyid`,
            dataType: 'json',
            headers: {
                'Authorization': tok,
                'Content-Type': 'application/json; charset=utf-8',
                'accept': 'application/json'
            },
            success: function (res) {
                //console.log(res);
                if (res.status === "00") {
                    for (var i = 0; i < res.data.productAttributeDTOS[0].productPictureDTOS.length; i++) {
                        console.log(res.data.productAttributeDTOS[0].productPictureDTOS[i].picture);
                        $('.product-small-img').hide();
                        $('.product-small-img-added1').show();
                        $('.product-small-img-added2').hide();
                        $('.product-small-img-added3').hide();
                        $('.product-small-img-added4').hide();
                        $('.product-small-img-added5').hide();
                        $('#mainPicture').attr('src', res.data.productAttributeDTOS[0].productPictureDTOS[0].picture);
                        $('.product-small-img-added1').append(`
                           <div>
                           <img src=${res.data.productAttributeDTOS[0].productPictureDTOS[i].picture} class="img-fluid">
                           </div>`);
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }

        });

        $('.productColor ul li').removeClass('disabledClick');
        $(this).addClass('disabledClick');

    });

    $($('.productColor ul li')[1]).click(function () {
        <?php if(!is_null(session('designerToken'))): ?>
            tok = <?= session('designerToken') ?>;
        <?php else: ?>
            tok = '';
        <?php endif; ?>
        $.ajax({
            url: `https://wawoohapi.herokuapp.com/fashion/product/${productId}/designer/getproductbyid`,
            dataType: 'json',
            headers: {
                'Authorization': tok,
                'Content-Type': 'application/json; charset=utf-8',
                'accept': 'application/json'
            },
            success: function (res) {
                //console.log(res);
                if (res.status === "00") {
                    for (var i = 0; i < res.data.productAttributeDTOS[1].productPictureDTOS.length; i++) {
                        $('.product-small-img').hide();
                        $('.product-small-img-added1').hide();
                        $('.product-small-img-added3').hide();
                        $('.product-small-img-added4').hide();
                        $('.product-small-img-added5').hide();
                        $('.product-small-img-added2').show();
                        $('#mainPicture').attr('src', res.data.productAttributeDTOS[1].productPictureDTOS[0].picture);
                        $('.product-small-img-added2').append(`
                           <div>
                            <img src=${res.data.productAttributeDTOS[1].productPictureDTOS[i].picture} class="img-fluid">
                           </div>`);
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });

        $('.productColor ul li').removeClass('disabledClick');
        $(this).addClass('disabledClick');


    });

    $($('.productColor ul li')[2]).click(function () {
        <?php if(!is_null(session('designerToken'))): ?>
            tok = <?= session('designerToken') ?>;
        <?php else: ?>
            tok = '';
        <?php endif; ?>
        $.ajax({
            url: `https://wawoohapi.herokuapp.com/fashion/product/${productId}/designer/getproductbyid`,
            dataType: 'json',
            headers: {
                'Authorization': tok,
                'Content-Type': 'application/json; charset=utf-8',
                'accept': 'application/json'
            },
            success: function (res) {
                //console.log(res);
                if (res.status === "00") {
                    for (var i = 0; i < res.data.productAttributeDTOS[2].productPictureDTOS.length; i++) {
                        console.log(res.data.productAttributeDTOS[2].productPictureDTOS[i].picture);
                        $('.product-small-img').hide();
                        $('.product-small-img-added3').show();
                        $('.product-small-img-added2').hide();
                        $('.product-small-img-added1').hide();
                        $('.product-small-img-added4').hide();
                        $('.product-small-img-added5').hide();
                        $('#mainPicture').attr('src', res.data.productAttributeDTOS[2].productPictureDTOS[0].picture);
                        $('.product-small-img-added3').append(`
                           <div>
                           <img src=${res.data.productAttributeDTOS[2].productPictureDTOS[i].picture} class="img-fluid">
                           </div>`);
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });

        $('.productColor ul li').removeClass('disabledClick');
        $(this).addClass('disabledClick');

    });

    $($('.productColor ul li')[3]).click(function () {
        <?php if(!is_null(session('designerToken'))): ?>
            tok = <?= session('designerToken') ?>;
        <?php else: ?>
            tok = '';
        <?php endif; ?>
        $.ajax({
            url: `https://wawoohapi.herokuapp.com/fashion/product/${productId}/designer/getproductbyid`,
            dataType: 'json',
            headers: {
                'Authorization': tok,
                'Content-Type': 'application/json; charset=utf-8',
                'accept': 'application/json'
            },
            success: function (res) {
                //console.log(res);
                if (res.status === "00") {
                    for (var i = 0; i < res.data.productAttributeDTOS[3].productPictureDTOS.length; i++) {
                        console.log(res.data.productAttributeDTOS[3].productPictureDTOS[i].picture);
                        $('.product-small-img').hide();
                        $('.product-small-img-added3').hide();
                        $('.product-small-img-added2').hide();
                        $('.product-small-img-added1').hide();
                        $('.product-small-img-added4').show();
                        $('.product-small-img-added5').hide();
                        $('#mainPicture').attr('src', res.data.productAttributeDTOS[3].productPictureDTOS[0].picture);
                        $('.product-small-img-added4').append(`
                           <div>
                           <img src=${res.data.productAttributeDTOS[3].productPictureDTOS[i].picture} class="img-fluid">
                           </div>`);
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });

        $('.productColor ul li').removeClass('disabledClick');
        $(this).addClass('disabledClick');

    });

    $($('.productColor ul li')[4]).click(function () {
        <?php if(!is_null(session('designerToken'))): ?>
            tok = <?= session('designerToken') ?>;
        <?php else: ?>
            tok = '';
        <?php endif; ?>
        $.ajax({
            url: `https://wawoohapi.herokuapp.com/fashion/product/${productId}/designer/getproductbyid`,
            dataType: 'json',
            headers: {
                'Authorization': tok,
                'Content-Type': 'application/json; charset=utf-8',
                'accept': 'application/json'
            },
            success: function (res) {
                //console.log(res);
                if (res.status === "00") {
                    for (var i = 0; i < res.data.productAttributeDTOS[4].productPictureDTOS.length; i++) {
                        console.log(res.data.productAttributeDTOS[4].productPictureDTOS[i].picture);
                        $('.product-small-img').hide();
                        $('.product-small-img-added3').hide();
                        $('.product-small-img-added2').hide();
                        $('.product-small-img-added1').hide();
                        $('.product-small-img-added4').hide();
                        $('.product-small-img-added5').show();
                        $('#mainPicture').attr('src', res.data.productAttributeDTOS[4].productPictureDTOS[0].picture);
                        $('.product-small-img-added5').append(`
                           <div>
                           <img src=${res.data.productAttributeDTOS[4].productPictureDTOS[i].picture} class="img-fluid">
                           </div>`);
                    }

                }
            },
            error: function (err) {
                console.log(err);
            }
        });

        $('.productColor ul li').removeClass('disabledClick');
        $(this).addClass('disabledClick');

    });

    $(document).on('click', '.product-small-img-added2 div img, .product-small-img-added3 div img, .product-small-img-added4 div img, .product-small-img-added5 div img', function () {
        console.log($(this).attr('src'));
        let pictureSwap = $(this).attr('src');
        $('#mainPicture').attr('src', pictureSwap);
    });
</script>