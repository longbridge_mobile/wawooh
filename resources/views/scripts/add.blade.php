@extends('layouts.designer.designer')

@push('styles')
    <link href="{{ asset('css/jquery.minicolors.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/rta.min.css') }}">
    <style>

        .img#mainFabric {
            height: 200px !important;
        }

        .tristyle {
            border: 1px solid red !important;
        }

        .addMorePictures {
            border: 0 solid #fff !important;
        }

        .otherAdd {
            border: 1px solid rgba(205, 153, 51, 0.2);
        }

        .imageAddDiv, .imageFabricDiv {
            float: left;
            margin: 0 4px;
            position: relative;
            border-radius: 7px;
        }

        .imageAddDiv span, .imageFabricDiv span {
            position: absolute;
            right: 3px;
            padding: 2px 8px 0;
            color: black;
            background: rgba(255, 255, 255, .4);
        }

        .pp {
            color: #999;
        }

        .imgleft {
            float: left;
        }

        .divClassArt {
            margin-top: 10px;
        }

        .arrange-artImg .proImg {
            margin: 0 7px;
        }

        .imgLeftStyle {
            border: 1px solid rgba(51, 51, 51, 0.6);
            color: #fff;
            background: #cd9933;
        }

        .imgleft input[type=number]::-webkit-inner-spin-button,
        .imgleft input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .artWorkNumber {
            height: 35px;
            width: 73px;
        }

        .cropper-crop-box {
            position: absolute !important;
            float: left !important;
        }

        .cropper-container {
            position: relative !important;
        }

        #showMoreContainer {
            width: 73vw;
        }

        .removeAllFabric {
            position: relative;
            z-index: 9999;
            margin-top: -15px;

        }

        .showFabricType .fa-trash:before {
            color: red !important;
        }

        .img-magnifier-glass {
            position: absolute;
            border: 3px solid #000;
            border-radius: 50%;
            cursor: none;
            width: 100px;
            height: 100px;
        }

        .removeBg {
            background-image: url({{asset("img/material1.jpg")}}) !important;
        }

        .displayCropper {
            display: block !important;
        }

        .hideCropper {
            display: none !important;
        }

        .proImg {

        }

        .validationCheck {
            border: 1px solid red;
        }

        .hidex {
            display: none !important;
        }

        .avai {
            display: flex;
        }

        .yip {
            display: block;
        }

        .proImg, .moreProImg {
            border: 0px solid !important;
        }

        /*.imgcass {
            height: 150px !important;
        }*/

        .proImg > img {
            /*//height: auto !important;*/
            height: 150px !important;
        }

        .notifyIcon {
            background: #cd9933;
        }

        #fabricstock {
            /*width: 50px;*/
        }

        .materialImage {
            height: 30px !important;
            float: right;
            width: 30px !important;
            margin-top: -34px;
        }

        .makeMaterial {
            cursor: pointer;
        }

        .colorPickers {
            padding: 10px;
            width: 10px;
            margin: 10px;
        }

        #styleHolder {
            position: absolute;
            z-index: 1000;
            max-height: 100px;
            width: 100%;
            overflow: auto;
            color: white;
            background: black;
        }

        .input-group-prepend {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .delete-size:hover {
            cursor: pointer;
        }

        #fabricsize, #fabricsize {
            /*width: 20%;*/
        }

        #searchres {
            background: #ffffff;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.1);
        }

        .sizesearchres {
            padding: 0.5rem 1rem;
            border-bottom: 0.5px solid rgba(0, 0, 0, 0.1);
            margin-bottom: 0;
        }

        .sizesearchres:hover {
            cursor: pointer;
            background: #f8f8f8;
        }

        .size {
            margin-bottom: 1rem;
        }

        .removeIMage {
            color: Red;
            cursor: pointer
        }

        .minicolors-swatch-color {
            margin: 2px 4px !important;

        }

        .minicolors-swatch {
            background: transparent;
            border: none;
        }

        .proImg img, .resize-art img, .upload-mat img {
            cursor: pointer
        }

        .row {
            width: 100%;
        }

        .designerAddProduct .productColor {
            position: relative;
            top: 0;
            left: 0;
        }
    </style>

@endpush

@section('pageTitle', 'Add Product')
@section('content')

    <div class="content-wrapper new-prod">
        <div class="row grid-margin">
            <div class="col-12 ">
                <h3 style="margin-bottom: 40px;" class="makeBold">ADD NEW PRODUCT</h3>
                <div class="in-stock">
                    <div class="pretty p-icon p-round p-smooth">
                        <input value='1' type="radio" name='producttype' class="form-check-input noLeft"
                               id="outfit" checked>
                        <div class="state p-warning-o">
                            <i class="icon fa fa-check"></i>
                            <label class="form-check-label" for="outfit">Clothings</label>
                        </div>
                    </div>
                    <div class="pretty p-icon p-round p-smooth">
                        <input value='2' type="radio" name='producttype' class="form-check-input noLeft"
                               id="accessories">
                        <div class="state p-warning-o">
                            <i class="icon fa fa-check"></i>
                            <label class="form-check-label" for="accessories">Accessories</label>
                        </div>
                    </div>
                    <div class="pretty p-icon p-round p-smooth">
                        <input value='3' type="radio" name='producttype' class="form-check-input noLeft"
                               id="shoes">
                        <div class="state p-warning-o">
                            <i class="icon fa fa-check"></i>
                            <label class="form-check-label" for="shoes">Shoes</label>
                        </div>
                    </div>
                </div>

                <div class="row spaceTop">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="productName">Product Name*</label>
                            <input type="text" class="form-control impt-control required-product" id="productName"
                                   placeholder="Not More than 30 characters"
                                   maxlength="30">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="productPrice">Product Price*</label>
                            <input type="number" class="form-control impt-control required-product" id="productPrice"
                                   placeholder="Input Product Cost">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group discountGroup" style="margin-bottom: 0 !important;">
                            <label for="productPrice">Discounted Product Price</label>
                            <input type="number" class="form-control discountField" id="DiscountproductPrice"
                                   placeholder="Input Discounted Price">
                        </div>

                        <div class="form-group hide percentGroup" style="margin-bottom: 0 !important;">
                            <label for="productPrice">Percentage</label>
                            <input type="number" class="form-control discountField" id="percent"
                                   placeholder="Input Percentage">
                        </div>
                        <div id="selectPercentage"><label class="label label-warning">Select <i
                                        class="fa fa-fw fa-percent"
                                        aria-hidden="true"></i></label>
                        </div>
                        <div id="selectDiscount" class="hide"><label class="label label-rejected">Select
                                Discount</label></div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="productDetails">Product Summary*</label>
                    <textarea class="form-control impt-control required-product" id="productSummary"
                              placeholder="Not more than 100 characters" rows="3"
                              maxlength="100"></textarea>
                </div>

                <div class="form-group">
                    <label for="productDescription">Product Full Description*</label>
                    <textarea class="form-control editor impt-control" id="productDescription"
                              placeholder="Write full details of your product describing the basic information (fabric, color, how to wash etc.). Each description should be in a paragraph."
                              rows="6"></textarea>

                </div>

                <div class="sortid" style="display:flex; margin: 0 0 15px 0;">
                    <div class="col-md-3" style="padding-left: 0; padding-top: 19px;">
                        <div class="pretty p-icon p-round p-smooth">
                            <input class="form-check-input" type="checkbox" name="availa" id="instocks" value="instocks"
                                   checked>
                            <div class="state p-warning-o">
                                <i class="icon fa fa-check"></i>
                                <label class="form-check-label" for="instocks">Ready-To-Wear Orders Accepted</label>
                            </div>
                        </div>
                    </div>

                </div>

                <div style="margin-bottom: 10px;" class="form-group designerAddProduct">
                    {{--
                                        <button id="addMoreFabricType" class="btn btn-wawooh">New Fabric Type</button>
                    --}}

                    <div id="showFabricType-1" class="row otherAdd showFabricType">
                        <div class="row">
                            <div class="col checkAllFabric">
                                <label for="mainFabricType-1" class="proImg">
                                    <p>Main</p>
                                    <img id="mainFabric" src="{{ asset('img/material1.jpg')}}"
                                         class="mainFabric proFabricImg firstImage">
                                    <input multiple type="file" id="mainFabricType-1"
                                           class="hide-input mainFabricType proFabricImgInput"/>
                                    <div class="mainFabricAction proFabricImgAction"></div>
                                </label>
                            </div>

                            <div class="col">
                                <label for="sideFabricType-1" class="proImg">
                                    <p>Side</p>
                                    <img id="sideFabric" src="{{ asset('img/material1.jpg')}}"
                                         class="sideFabric proFabricImg">
                                    <input multiple type='file' id='sideFabricType-1'
                                           class='hide-input sideFabricType proFabricImgInput'/>
                                    <div class="sideFabricAction proFabricImgAction"></div>
                                </label>
                            </div>

                            <div class="col">
                                <label for='backFabricType-1' class="proImg">
                                    <p>Back</p>
                                    <img id="backFabric" src="{{ asset('img/material1.jpg')}}"
                                         class="backFabric proFabricImg">
                                    <input multiple type='file' id='backFabricType-1'
                                           class='hide-input backFabricType proFabricImgInput'/>
                                    <div class="backFabricAction proFabricImgAction"></div>
                                </label>
                            </div>

                            <div class="col">
                                <label for='frontFabricType-1' class="proImg">
                                    <p>Front</p>
                                    <img id="frontFabric" src="{{ asset('img/material1.jpg')}}"
                                         class="frontFabric proFabricImg">
                                    <input multiple type='file' id='frontFabricType-1'
                                           class='hide-input frontFabricType proFabricImgInput'/>
                                    <div class="frontFabricAction proFabricImgAction"></div>
                                </label>
                            </div>

                            <div class="col">
                                <label for='topFabricType-1' class="proImg">
                                    <p>Top</p>
                                    <img id="topFabric" src="{{ asset('img/material1.jpg')}}"
                                         class="topFabric proFabricImg top">
                                    <input multiple type='file' id='topFabricType-1'
                                           class='hide-input topFabricType proFabricImgInput'/>
                                    <div class="topFabricAction proFabricImgAction"></div>
                                </label>
                            </div>
                        </div>
                        <div class="row mt-5 attribute-details">
                            <div class="col-md-4" style="border-right: 1px solid rgba(0,0,0,.3);">
                                <div id="material-name-cover" style="padding-left: 10px">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend" style="border: none !important;">
                                            <div class="productColor">
                                                <img src="{{ asset('img/material1.jpg')}}" class="displayMat"
                                                     height="40" width="40">
                                            </div>
                                        </div>
                                        <div class="form-group ml-5 mb-0">
                                            <label for="colorMaterialName-1">Fabric Color Name</label>
                                            <input class="form-control colorMaterialName" type="text"
                                                   id="colorMaterialName-1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div id="material-sizes-cover">
                                    <div class="mains">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label for="fabricsize-1"> Select Stock Sizes</label>
                                                {{--<input type="text" size="6" class="form-control impt-control fabricsize"
                                                       id="fabricsize-1">
                                                <div class="fabricsearch" data-result="#measureAttachment"></div>--}}
                                                <select name="" class="form-control fabricsiz impt-control"
                                                        id="fabricsize">
                                                    @if(count($sizes))
                                                        @foreach ($sizes as $size)
                                                            <option value="{{$size->name}}">{{$size->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="fabricstock-1">Stock Count</label>
                                                <input type="number" size="4"
                                                       class="fabricstock form-control impt-control"
                                                       id="fabricstock-1">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button class="btn btn-wawooh addFabricMeasurement">Add Stock</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 measureCorner pt-0" style="border: none;overflow: hidden;">
                                <p style="margin-bottom: 0.5rem">Product Stock</p>
                                <div class="measureAttachment"
                                     style="border: 1px solid #000000;height: 8rem;overflow-y: auto">
                                    <table style="text-align: center" class="table table-stripped">
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <p class="pp">To add a new set of fabric color options, click the "Add" button below</p>
                    <button id="addMoreFabricType" class="btn btn-wawooh" style="margin-bottom: 20px;">Add
                    </button>
                    <div id="showMoreContainer" class="addMorePictures" style="border: 0px solid #fff;"></div>
                </div>


                <div class="form-group">
                    <!-- <label for="productGroup">Product Grouping*</label> -->
                    <div class="row">
                        <div class="col-md-4">
                            <label for="Category">Category*</label>
                            <select class="form-control ct impt-control required-product" id="productGroup">
                                <option value="" hidden> -- Select Category --</option>
                                @if(count($categories))
                                    @foreach($categories as $category)
                                        <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="subCategory">Sub-Category*</label>
                            <select class="form-control ct impt-control required-product" disabled='disabled'
                                    id="subcat">
                                <option value='null' hidden>Select Sub Category</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="style">Style*</label>
                            <select class="form-control ct discountField impt-control required-product"
                                    disabled='disabled' id="style">
                                <option value='null' hidden>Select Style</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row" style="margin: 0">
                    <div class="col-md-3" style="padding-left: 0; padding-top: 19px;">
                        <div class="pretty p-icon p-round p-smooth">
                            <input class="form-check-input" type="checkbox" name="availa" id="ondemands"
                                   value="ondemands">
                            <div class="state p-warning-o">
                                <i class="icon fa fa-check"></i>
                                <label class="form-check-label" for="ondemands">Bespoke Orders Accepted</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 animated fadeInLeft" style="margin:0 5px;">
                        <input placeholder='Enter production time in days for each bespoke order'
                               class="form-control discountField hide"

                               style='margin: 10px 0 10px 0;' type='number' id='time'/>
                    </div>
                </div>


                <div class="form-group hide" id="material-div">

                    <hr>
                    <h5 class="customLabel">Artwork Options For Bespoke*</h5>
                    <p class="least-img makeBold">Click on each box to upload an artworks (maximum of 5 fabrics can be
                        selected)</p>
                    <div class="row artWWork" style="margin-bottom: 30px;">
                        <div class="col-md-2">
                            <label for="productImage">
                                <div class="arrange-artImg">
                                    <label for='artworkImg' class="proImg">
                                        <img class="artworkImages" src="{{ asset('img/landscape.png')}}">
                                        <input multiple type='file' id='artworkImg' class='hide-input'/>
                                    </label>
                                    {{--<div id="artworkprice"></div>--}}
                                </div>
                            </label>
                        </div>
                        <div class="col-md-10 artPW"></div>
                    </div>

                    <h5 class="customLabel">Fabric Options For Bespoke*</h5>
                    <p class="least-img makeBold">Add all available fabrics</p>
                    <div class="row materialBox">
                        <div class="col-md-2">
                            <label for='mat1' class="upload-mat">
                                <img style="width: 150px" class='materialImages' src="{{ asset('img/material1.jpg')}}">
                                <input type='file' id='mat1' class='hide-input'/>
                            </label>
                        </div>
                        <div class="col-md-10 prodd"></div>
                    </div>
                    <div class="row" id="materialPrice" style="display: none; margin-top:9px;">
                        <div class="col-md-3">
                            {{-- <div class="form-group">
                                 <label for="materialPrice">
                                     <small>Fabric Price</small>
                                 </label>
                                 <input type="number" class="form-control discountField" name="materialPrice"
                                        id="materialPrice"
                                        placeholder="Enter Fabric Price">
                             </div>--}}
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="sewingPrice">Enter Sewing Price</label>
                            <input type="text" id="sewingPrice" class="form-control">
                        </div>
                    </div>

                </div>


                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group hide" id="requiredmeasurement-div">
                            <label for="my-measurement">Required Measurement*</label>
                            <div>
                                <input type="text" id="my-measurement" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">

                    </div>

                    {{-- <div class="form-group">
                         <select multiple class="form-control selectMeasure">
                             @if(isset($reqM))
                                 @if(count($reqM))
                                     @foreach($reqM as $req)
                             <option value="{{$req->id}}">{{$req->name}}</option>
                                     @endforeach
                                 @endif
                             @endif
                         </select>
                     </div>--}}
                </div>

                <div class="pretty p-icon p-square p-smooth">
                    <input type="checkbox" class="form-check-input noLeft" id="exampleCheck1">
                    <div class="state p-warning-o">
                        <i class="icon fa fa-check"></i>
                        <label class="form-check-label" for="exampleCheck1">
                            I agree that all details filled are
                            correct and all Description of the product is correct.
                        </label>
                    </div>
                </div>

            </div>

            <div class="col-12">
                <div class="buttons" style="margin-top: 10px;">
                    <div>
                        {{--<button class="btn btn-wawooh add-product">Submit</button>--}}
                        <input type="button" class="btn btn-addProduct add-product" id="addBtn" value="Submit">
                        <span class="mySpinner"></span>
                    </div>
                    <div>
                        <button class="btn btn-update" id="cancel"
                                style="background: gray; color: white; border: 1px solid #111;">Cancel
                        </button>
                    </div>
                </div>
            </div>

            <div class="modal fade" data-backdrop="static" id="artworkModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="art-close">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-artwork" id="art-display"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                            </button>
                            <button type="button" class="btn btn-primary modalSaveBtn" data-dismiss="modal">Save
                                changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <!--Artwork Append Modal-->
            <div class="modal fade" data-backdrop="static" id="artworkModal2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="art-close">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-artwork" id="art-display-2"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                            </button>
                            <button type="button" class="btn btn-primary modalSaveFabricBtn" data-dismiss="modal">Save
                                changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="artworkEditModal2" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="art-close">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="modal-artwork" id="art-display-2"></div>
                        </div>
                        <div class="modal-footer">
                            {{--   <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                               </button>--}}
                            <button type="button" class="btn btn-primary modalSaveBtn" data-dismiss="modal">Save
                                changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{ asset('js/admin/jquery.richtext.js') }}"></script>

    <script>
        var imgClick = false;
        $('#productDescription').richText();
        $(document).on('click', '.imageFabricDiv', function () {
            imgClick = true;
            $(this).addClass('tempFabrice');
            var Img = $(this).find('.pImg').attr("src");
            var fPrice = $(this).find('.proImg .fPrice').html();
            console.log(fPrice);

            $('#artworkModal2').modal();
            $('#artworkModal2 #art-display-2').append(`
            <label id="1" class="">
                <div class="row artWf">
                    <div class="col-md-12">
                        <div class="img-holder-display">
                        <img src="${Img}" class="imgFaB">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="padding: 0">
                        <div class="form-group">
                            <label for="price">Enter Price</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-dollar"></i></span>
                                </div>
                                <input type="number" id="price" placeholder="Enter Price for Artwork" class="form-control" value="${fPrice}">
                            </div>
                        </div>
                    </div>
                 </div>
             </label>
            `);
        });

        $(document).on('click', '.imageAddDiv', function () {
            imgClick = true;
            $(this).addClass('tempArtWork');
            var Img = $(this).find('.pImg').attr('src');
            var fPrice = $(this).find('.fPrice').html();
            var fName = $(this).find('.fName').html();

            $('#artworkModal').modal();
            $('#artworkModal #art-display').append(`
            <div  style="margin-bottom:10px">
                        <div class="row">
                            <div class="col-md-5 offset-3">
                            <img src="${Img}" class="art-modal-img materialImg">
                            </div>
                        </div>
                        <div class="row">
                            <form class="form-inline">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <div class="input-group-addon">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="number" placeholder="Enter Fabric Price" class="form-control fabricPrice" value="${fPrice}">
                                    </div>
                                </div>
                                <div class="form-group mx-sm-3 mb-2">
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="text" class="form-control fabricName" id="fabricName" placeholder="Enter Fabric Name" value="${fName}">
                                </div>
                            </form>
                        </div>
                    </div>
            `);
        });

        $(document).on('click', '.art-close', function () {
            $('#art-display').html('');
            $('#art-display-2').html('');
            $('.imageFabricDiv').each(function () {
                if ($('.imageFabricDiv').hasClass('tempFabrice')) {
                    $(this).removeClass('tempFabrice');
                }
            });

            $('.imageAddDiv').each(function () {
                if ($('.imageFabricDiv').hasClass('tempArtWork')) {
                    $(this).removeClass('tempArtWork');
                }
            })
            //.prodd
            //  $(this).closest('.modal-header').siblings('.modal-body').html('');
        });

        $(document).on('click', '.modalSaveBtn', function () {
            var artW = $('.art-modal-img ').attr('src');
            var fabricPrice = $('.fabricPrice').val();
            var fabricName = $('.fabricName').val();

            //console.log(fabricPrice);
            // tempArtWork
            $('.imageAddDiv').each(function () {
                if ($('.imageAddDiv').hasClass('tempArtWork')) {
                    $(this).remove();
                }
            });

            $(".prodd").append(`
            <div class="imageAddDiv">
            <span class="imageAddDivDel"><i class="fa fa-trash"></i></span>
           <label class="proImg">
          <img class="pImg materialImg" src=${artW}>
          <p class="fPriceDiv">Price: <span class="fPrice">${fabricPrice}</span></p>
          <p class="fNameDiv">Name:  <span class="fName">${fabricName}</span></p>
          </label></div>`);

            $('#artworkModal').removeClass('show');
            $('#artworkModal').css('display', 'none');
            //$(".modalSaveBtn").attr('data-dismiss', 'modal');
            $('#art-display').html('');

        });

        $(document).on('click', '.imageAddDivDel', function (e) {
            e.preventDefault();
            $(this).closest('.imageFabricDiv').remove();
            $(this).closest('.imageAddDiv').remove();
        });

        $(document).on('click', '.modalSaveFabricBtn', function () {

            var Image = $(this).closest('.modal-footer').siblings('.modal-body').find('.imgFaB').attr('src');
            var Price = $(this).closest('.modal-footer').siblings('.modal-body').find('#price').val();

            // $('.imageFabricDiv').has('.tempFabrice').remove();
            $('.imageFabricDiv').each(function () {
                if ($('.imageFabricDiv').hasClass('tempFabrice')) {
                    // $('.imageFabricDiv').remove();
                    $(this).remove();
                }
            });

            $('.artPW').append(`
           <div class="imageFabricDiv">
             <span class="imageAddDivDel"><i class="fa fa-trash"></i></span>
           <label class="proImg">
          <img class="pImg artWorkImage" src=${Image}>
          <div>Price: <span class="fPrice">${Price}</span></div>
          </label></div>
          `);
            $('#art-display-2').html('');

        });

        var elemToStayFocused = ['proFabricImgAction', 'fabricCrop', 'addFabricCrop', 'removeFabricImg', 'removeFabricCrop', 'cropper-crop', 'cropper-move'];
        var activeDiscount = 'price';

        var sizes = [];
        var selectedSizes = [];
        var prodType = 1;
        var attrcun = 1;
        var keyPressed = false;


        //Gets current sizes from api
        $.ajax({
            url: '{{env('GET_BASE_URL')}}/fashion/size/getsizes',
            type: 'GET',
            success: function (res) {
                if (res.status = '00') {
                    tempSizes = res.data.data;
                    $.each(tempSizes, function () {
                        sizes.push(this.name.toUpperCase());
                    })
                }
            }
        });

        $.each(sizes.slice(0, 4), function () {
            $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
        });

        //When size search is focused
        $(document).on('focus', '.fabricsize', function () {
            selectedSizes = [];
            var fabricSize = $(this).val().toUpperCase();
            var fabricParent = $(this).parents('.attribute-details');
            var fabricSearch = fabricParent.find(".fabricsearch");

            $.each(fabricParent.find('tr'), function () {
                selectedSizes.push($(this).find('.selected-size').html());
            });

            fabricSearch.show();
            var currarr = $.grep(sizes, function (a) {
                return (a.includes(fabricSize) && !selectedSizes.includes(a));
            });

            fabricSearch.html('');
            $.each(currarr.slice(0, 4), function () {
                $(fabricSearch).append('<p class="sizesearchres" >' + this + '</p>');
            });
            fabricClickListeners(this, fabricSearch);
        });

        //Handles key press events for size search
        $(document).on('keydown', '.fabricsize', function () {
            keyPressed = true
        });

        $(document).on('keyup', '.fabricsize', function () {
            if (keyPressed) {
                searchRess($(this).parents('.attribute-details'));
                keyPressed = false;
            }
        });

        //Calls function to add size to selected sizes
        $(document).on('click', '.addFabricMeasurement', function () {
            var fabricParent = $(this).parents('.attribute-details');
            selectedSizes = [];
            var measurement = $(fabricParent).find('.fabricsiz').val().toUpperCase();
            var stock = $(fabricParent).find('.fabricstock').val();
            $.each(fabricParent.find('tr'), function () {
                selectedSizes.push($(this).find('.selected-size').html().toUpperCase());
            });

            if (!sizes.includes(measurement)) {
                $.notify("Size does not exist");
                return false;
            }

            if (selectedSizes.includes(measurement)) {
                $.notify("Size Has already been selected");
                return false;
            }

            if (parseInt(stock) < 0 || stock.trim() === "") {
                $.notify("Stock cannot be less than 0");
                return false;
            }

            var data = {
                measurement: measurement,
                stock: stock
            };

            showSizes(data, fabricParent);
            $(fabricParent).find('.fabricsize').val('');
            $(fabricParent).find('.fabricstock').val('');
        });

        $('#productGroup').on('change', function () {
            var id = $(this).val();
            styles = [];
            var data = {
                categoryId: id,
                productType: prodType
            };

            $.ajax({
                url: '{{route('getSubCatBycat')}}',
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    var result = response.data;
                    var html = `<option value='' hidden>Select</option>`;
                    if (result.length) {
                        $('#subcat').removeAttr('disabled');
                        for (i = 0; i < result.length; i++) {
                            html += `<option value='${result[i].id}'>${result[i].subCategory}</option>`;
                        }
                        $('#subcat').html(html);
                    } else {
                        $('#subcat').html(`<option value=''>No Sub Category</option>`);
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('#subcat').on('change', function () {
            var id = $(this).val();
            styles = [];
            var data = {
                id: id
            }

            $.ajax({
                url: `{{route('getStyleBySubCat')}}`,
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (result) {
                    var result = result.data;
                    $('#style').prop('disabled', false);
                    var html = `<option value=''>Select</option>`;
                    if (result.length) {
                        if (result instanceof Array) {
                            $('#style').removeAttr('disabled');

                            for (i = 0; i < result.length; i++) {
                                html += `<option value='${result[i].id}' >${result[i].style}</option>`;
                            }
                        } else {
                            $.notify('error', 'error');
                        }

                    } else {
                        html += `<option value=''>No style</option>`;
                    }
                    $('#style').html(html);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('#cancel').on('click', function () {
            location.reload();
        });

        //Search for available sizes
        function searchRess(fabricParent) {

            selectedSizes = [];
            $.each(fabricParent.find('tr'), function () {
                selectedSizes.push($(this).find('.selected-size').html());
            });

            var currarr = $.grep(sizes, function (a) {
                return (a.includes($(fabricParent).find('.fabricsize').val().toUpperCase()) && !selectedSizes.includes(a));
            });

            $(fabricParent).find('.fabricsearch').html('');
            $.each(currarr.slice(0, 4), function () {
                $(fabricParent).find('.fabricsearch').append('<p class="sizesearchres" >' + this + '</p>');
            });

            fabricClickListeners($(fabricParent).find('.fabricsize'), $(fabricParent).find('.fabricsearch'));
        }

        //Sets listener for size click
        function fabricClickListeners(fabricSize, fabricSearch) {
            $('.sizesearchres').click(function () {

                var size = $(this).html();
                $(fabricSize).val(size);
                $(fabricSearch).toggle();
            });
        }

        //Adds size to selected sizes
        function showSizes(data, fabricParent) {

            var searchResBody = $(fabricParent).find('tbody');
            searchResBody.append('<tr>\n' +
                '                    <td class="selected-size" >' + data.measurement + '</td>\n' +
                '                    <td class="selected-stock" >' + data.stock + '</td>\n' +
                '                    <td><i class="fa fa-trash removeMoreAttachedMeasurement"></i></td>\n' +
                '                  </tr>');
            deleteClickListener();
            $('.measureAttachment').removeClass('tristyle');
        }

        function deleteClickListener() {
            $(".removeMoreAttachedMeasurement").click(function () {
                parent = $(this).parents('tr');
                $(parent).remove();
            });
        }

        $(document).on('click', '#selectPercentage', function () {
            $('.discountGroup').toggle();
            $('.percentGroup').toggle();
            $(this).toggle();
            $('#selectDiscount').toggle();
            activeDiscount = 'percentage';
        });

        $(document).on('click', '#selectDiscount', function () {
            $('.discountGroup').toggle();
            $('.percentGroup').toggle();
            $(this).toggle();
            $('#selectPercentage').show();
            activeDiscount = 'price';
        });

        myMeasure = sellect("#my-measurement", {
            originList: [
                @if(isset($reqM))
                        @if(count($reqM))
                        @foreach($reqM as $req)
                    '{{$req->name}}',
                @endforeach
                @endif
                @endif
            ],
            destinationList: []
        });

        myMeasure.init();

        $(document).ready(function () {

            // $('.selectMeasure').select2();

            var cun = 0;

            $(document).on('click', '#addMoreFabricType', function () {
                attrcun++;

                if (attrcun === 5) {
                    $('#addMoreFabricType').attr('disabled', true);
                }

                if (attrcun > 0) {
                    if ($('[name=availa]:checked').val() === 'ondemands') {
                        // console.log($('[name=availa]:checked').val());
                        $('.mains').css('display', 'none');
                    }
                }


                $('#showMoreContainer').append(`
                    <div id="showFabricType-${attrcun}" class="row otherAdd showFabricType">
                    <i class="fa fa-trash removeAllFabric"></i>
                        <div class="row">
                            <div class="col" >
                                <label for="mainFabricType-${attrcun}" class="proImg">
                                    <p>Main</p>
                                    <img id="mainFabric" src="{{ asset('img/material1.jpg')}}" class="mainFabric proFabricImg firstImage">
                                    <input multiple type="file" id="mainFabricType-${attrcun}" class="hide-input mainFabricType proFabricImgInput"/>
                                    <div class="mainFabricAction proFabricImgAction"></div>
                                </label>
                            </div>

                            <div class="col" >
                                <label for="sideFabricType-${attrcun}" class="proImg">
                                    <p>Side</p>
                                    <img id="sideFabric" src="{{ asset('img/material1.jpg')}}" class="sideFabric proFabricImg">
                                    <input multiple type='file' id='sideFabricType-${attrcun}' class='hide-input sideFabricType proFabricImgInput'/>
                                    <div class="sideFabricAction proFabricImgAction" ></div>
                                </label>
                            </div>

                            <div class="col" >
                                <label for='backFabricType-${attrcun}' class="proImg">
                                    <p>Back</p>
                                    <img id="backFabric" src="{{ asset('img/material1.jpg')}}" class="backFabric proFabricImg">
                                    <input multiple type='file' id='backFabricType-${attrcun}' class='hide-input backFabricType proFabricImgInput'/>
                                    <div class="backFabricAction proFabricImgAction" ></div>
                                </label>
                            </div>

                            <div class="col" >
                                <label for='frontFabricType-${attrcun}' class="proImg">
                                    <p>Front</p>
                                    <img id="frontFabric" src="{{ asset('img/material1.jpg')}}" class="frontFabric proFabricImg">
                                    <input multiple type='file' id='frontFabricType-${attrcun}' class='hide-input frontFabricType proFabricImgInput'/>
                                    <div class="frontFabricAction proFabricImgAction" ></div>
                                </label>
                            </div>

                            <div class="col" >
                                <label for='topFabricType-${attrcun}' class="proImg">
                                    <p>Top</p>
                                    <img id="topFabricx" src="{{ asset('img/material1.jpg')}}" class="topFabric proFabricImg top-${attrcun}">
                                    <input multiple type='file' id='topFabricType-${attrcun}' class='hide-input topFabricType proFabricImgInput'/>
                                    <div class="topFabricAction proFabricImgAction" ></div>
                                </label>
                            </div>
                        </div>
                        <div class="row mt-5 attribute-details" >
                            <div class="col-md-4" >
                                <div id="material-name-cover" style="padding-left: 10px" >
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend" style = "border:none !important;">
                                            <div class="productColor"><img src="{{ asset('img/material1.jpg')}}" class="displayMat" height="40" width="40"></div>
                                        </div>
                                        <div class="form-group ml-5 mb-0">
                                            <label for="colorMaterialName-${attrcun}">Fabric Color Name</label>
                                            <input class="form-control colorMaterialName" type="text" id="colorMaterialName-${attrcun}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <div id="material-sizes-cover">
                                    <div class="mains">
                                        <div class="row" >
                                            <div class="col-md-6">
                                                <label for="fabricsize-${attrcun}">Select Stock Sizes</label>
                                                <select name="" class="form-control fabricsiz impt-control"
                                                        id="fabricsize">
                                                    @if(count($sizes))
                        @foreach ($sizes as $size)
                    <option value="{{$size->name}}">{{$size->name}}</option>
                                                        @endforeach
                        @endif
                    </select>
                    </div>
                    <div class="col-md-6">
                        <label for="fabricstock-${attrcun}">Stock Count</label>
                                                <input type="number" size="4"
                                                       class="fabricstock form-control impt-control" id="fabricstock-${attrcun}">
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button class="btn btn-wawooh addFabricMeasurement">Add Stock</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 measureCorner pt-0" style="border: none;overflow: hidden;" >
                                <p style="margin-bottom: 0.5rem" >Product Stock</p>
                                <div class="measureAttachment" style="border: 1px solid #000000;height: 8rem;overflow-y: auto" >
                                    <table style="text-align: center" class="table table-stripped" >
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
            });

            $(document).on('change', '.proFabricImgInput', function () {

                var file = $(this).get(0).files[0];
                if (typeof file === 'undefined') {
                    return false;
                }

                if (file.size / 1024 > 1200000) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    /*$(this).siblings('.proFabricImgAction').html(`
                             <span><i class="pull-right fa fa-trash removeFabricImg"></i></span>

                         `);*/


                    convertMoreToBase64(this, '.proFabricImg');

                    convertCropperToBase64(this, 'img');

                    var imageSiblings = $(this).siblings('.proFabricImg');
                    var cropperSiblings = $(this).siblings('.cropper-container');


                    convertCropperToBase64(this, 'img');

                    var imageSiblings = $(this).siblings('.firstImage');
                    var cropperSiblings = $(this).siblings('.cropper-container');

                    setTimeout(function () {
                        imageSiblings.cropper({
                            autoCropArea: 0.2,
                            cropBoxResizable: false,
                            zoomable: false
                        });
                    }, 500);

                    setTimeout(function () {
                        var img = imageSiblings.cropper('getCroppedCanvas').toDataURL();
                        imageSiblings.parents('.showFabricType').find('.displayMat').attr('src', img);
                    }, 540);

                    setTimeout(function () {
                        imageSiblings.parents('.showFabricType').find('.displayMat').addClass('prodAttr');
                    }, 800);

                    $(this).siblings('img').addClass('submitImg');

                    setTimeout(function () {
                        $('.proFabricImg').cropper('destroy');
                    }, 550);


                    $(this).siblings('img').removeClass('validationCheck');
                }
            });

            $(document).on('mouseover', '.submitImg', function () {
                var that = $(this);

                that.cropper({
                    autoCropArea: 0.2,
                    cropBoxResizable: false,
                    zoomable: false
                });
            });

            $(document).on('click', '.cropper-crop-box', function () {
                var that = $(this);
                var img = that.parents('.cropper-container').siblings('.submitImg').cropper('getCroppedCanvas').toDataURL();
                that.parents('.showFabricType').find('.displayMat').attr('src', img);

            });

            $(document).on('mouseleave', '.cropper-container', function () {
                $('.proFabricImg').cropper('destroy');
                $('.proFabricImgInput').attr('disabled', false);
            });

            $(document).on('mouseenter', '.cropper-crop-box', function () {
                $('.proFabricImgInput').attr('disabled', true);

            });

            $(document).on('click', '.removeAllFabric', function (e) {
                e.preventDefault();
                //console.log('dddff');
                var that = $(this);
                that.parents('.showFabricType').remove();
                attrcun += 1;
                // console.log(attrcun);
                // $('#addMoreFabricType').attr('disabled', false);
                if (attrcun === 5) {
                    $('#addMoreFabricType').attr('disabled', false);
                }
            });

            $(document).on('click', '.addFabricCrop', function (e) {

                e.preventDefault();
                var that = $(this);
                that.closest('.proFabricImgAction').siblings('.cropper-container').toggle();
                that.closest('.proFabricImgAction').siblings('.proFabricImg').toggleClass('cropper-hidden');
                var $image = $(this).closest('.proFabricImgAction').siblings('.proFabricImg');
                $image.cropper({
                    autoCropArea: 0.2,
                    cropBoxResizable: false
                });
                that.closest('.proFabricImgAction').addClass('yip');
                that.closest('.proFabricImgAction').siblings('input').prop('disabled', true);


            });

            $(document).on('click', '.fabricCrop', function (e) {
                e.preventDefault();
                var cropImage = $(this).closest('.proFabricImgAction').siblings('.proFabricImg').cropper('getCroppedCanvas').toDataURL();
                $(this).parents('.showFabricType').find('.displayMat').attr('src', cropImage);
                $(this).parents('.showFabricType').find('.displayMat').addClass('prodAttr');
                $(this).parents('.showFabricType').find('.displayMat').removeClass('validationCheck');
            });

            $(document).on('click', '.removeFabricCrop', function (e) {

                $('.proFabricImgAction').siblings('input').prop('disabled', false);
                $('.proFabricImgAction').removeClass('yip');

                $.each($('.proFabricImgAction').siblings('.cropper-container'), function () {
                    if ($(this).css('display').toLowerCase() !== 'none') {
                        $(this).toggle();
                    }
                    $('.proFabricImgAction').siblings('.proFabricImg').removeClass('cropper-hidden');
                });
                e.preventDefault();
            });

            $(document).on('click', '.removeFabricImg', function (e) {

                e.preventDefault();
                $(this).parents('.proFabricImgAction').siblings('.cropper-container').find('img').attr('src', '');
                $(this).parents('.proImg').find('.proFabricImg').attr('src', '{{ asset('img/material1.jpg')}}');
                $(this).parents('.proImg').find('.proFabricImgInput').val('');
                $(this).parents('.proImg').find('.proFabricImg').removeClass('submitImg');
                $(this).parents('.proFabricImgAction').html('');
            });

            $(document).click(function () {

                var stayFocused = false;
                var cropIsActive = false;

                if ($('.yip').length > 0) {
                    cropIsActive = true;
                }

                if (cropIsActive) {
                    $.each(elemToStayFocused, function () {
                        if ($(event.target).hasClass(this)) {
                            stayFocused = true;
                            return false;
                        }
                    });

                    if (!stayFocused) {
                        $('.proFabricImgAction').siblings('input').prop('disabled', false);
                        $('.proFabricImgAction').removeClass('yip');

                        $.each($('.proFabricImgAction').siblings('.cropper-container'), function () {
                            if ($(this).css('display').toLowerCase() !== 'none') {
                                $(this).toggle();
                            }
                            $('.proFabricImgAction').siblings('.proFabricImg').removeClass('cropper-hidden');
                        });
                    }
                }
            });

            $(document).on('click', '.addMoreFabricMeasurement', function () {
                var pix = [];
                var data = {
                    moreMeasurement: $(this).closest('.morethree').siblings('.moreone').find('.morefabricsize').val(),
                    moreStock: $(this).closest('.morethree').siblings('.moretwo').find('.morefabricstock').val()
                }
                pix.push(data);
                displayMoreMeasureSize(pix, this);
                $(this).closest('.morethree').siblings('.moreone').find('.morefabricsize').val('')
                $(this).closest('.morethree').siblings('.moretwo').find('.morefabricstock').val('')
            });

            $(document).on('click', '.removeMoreAttachedMeasurement', function () {
                var that = $(this);
                that.closest('.moreRemoveItems').remove();
                console.log(that);
            });

            counter = 0;
            var styles = [];
            tagId = 0;

            $('body').on('click', function () {
                $('#styleHolder').hide();
            });
            $(document).on('click', '.rem', function () {
                var target = $(this).attr('tag');
                $(target).remove();
            });

            $('.edit-plus').on('click', function () {
                var html = `<input type="hidden"  class="colorPicker" value="#ff6161">`;

                $(this).before(html);
            });

        });

        $('.removeArt').on('click', function () {
            //alert('remove');
            $(this).parent().remove();
        });

        $('#artwork1').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 500kb")
                } else {
                    $('#del').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                    convertToBase64(this, '#img1');
                }
            }

        });

        $('#artwork2').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 500kb")
                } else {
                    $('#del2').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                    convertToBase64(this, '#img2');
                }
            }
        });

        $('#artwork3').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 500kb")
                } else {
                    $('#del3').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                    convertToBase64(this, '#img3');
                }
            }
        });

        $('#artwork4').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 500kb")
                } else {
                    $('#del4').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                    convertToBase64(this, '#img4');
                }
            }
        });

        var counters = 0;
        $('#addMoreImage').on('click', function () {

            $('#addMoreImagesCol').append(`

           <div class="addedMoreImage" counter=${counters++}><i class="fa fa-times removeMoreImage"></i>
            <label for='productImgs' class="proImg">
                        <img src="{{ asset('img/landscape.png')}}">
                        <input multiple type='file' id='productImgs' class='hide-input productImgs'/>
                        <div class="moreAddedImmages"></div>
                    </label>
                    </div>
           `)
        });

        $(document).on('click', '.removeMoreImage', function () {
            $(this).closest('div').remove();
        });

        $('#artwork5').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 1M")
                } else {
                    $('#del5').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                    convertToBase64(this, '#img5');
                }
            }
        });

        $('#mat1').on('change', async function () {
            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 1M")
                } else {
                    $('#artworkModal').modal();
                    pix.push(await toBase64($(this).get(0).files[i]));
                    $('#materialPrice').show();
                }
            }
            showMaterials(pix);
        });

        var moreSizes;
        $(document).on('change', '.productImgs', function () {
            var pix = [];
            var that = $(this);
            for (var i = 0; i < $(this).get(0).files.length; i++) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 1M");
                } else {
                    pix.push(toBase64($(this).get(0).files[i]));
                }
            }

            /*for(var i = 0; i < $('.addedMoreImage').length; i++){
                console.log(i);
            }*/

            showMoreProductImages(pix, that);
            addDelListener();
        });

        var resizes;
        var counter = 0;

        $('.productImg').on('change', async function () {

            if ($('#instockopts').prop('checked')) {
                $('.moreImageDiv').html(`
                 <div id="sizess">
                            </div>
                            <div id="searchsizes" class="my-3">
                                <div class="form-group">
                                    <label for="searchsizeinputs">Select Sizes*</label>
                                    <input type="text" id="searchsizeinputs" class="form-control"
                                           placeholder="Search size" size="6">
                                    <div id="searchress">

                                    </div>
                                </div>
                            </div>
                            <div class="form-check" id="customsizes">
                                <div class="pretty p-icon p-square p-smooth">
                                    <input type="checkbox" name="acceptCustomSizes" class="form-check-input noLeft"
                                           id="customsizetoggles">
                                    <div class="state p-warning-o">
                                        <i class="icon fa fa-check"></i>
                                        <label class="form-check-label" for="exampleCheck2">Accept custom sizes</label>
                                    </div>
                                </div>
                            </div>
                `);
            }
            var pixs = [];

            for (var i = 0; i < $(this).get(0).files.length; ++i) {

                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 500kb")
                } else {
                    if ($('.pImg').length <= 4) {
                        pixs.push(await toBase64($(this).get(0).files[i]));
                        counter++;
                    } else {
                        $.notify("You cannot upload more than 5 images", "", "warning");
                    }
                    /*
                                         setTimeout(function(){
                                            $('.pImg').each(function(j, obj){
                                            });
                                           for(var j = 0; j < $('.pImg').length; j++){
                                                    resizes = new Croppie($('.pImg')[j], {
                                                        viewport: { width: 50, height: 50 },
                                                        boundary: { width: 100, height: 100 },
                                                        showZoomer: false,
                                                        enableResize: true,
                                                        enableOrientation: true
                                                    });
                                                    console.log(resizes);
                                                    //resizes.reverse();

                                            }
                                            $('.croppie-container .croppie-container').css('opacity', 0);
                                         }, 500);*/
                }
            }
            $(this).val("");
            showProductImages(pixs);
            addDelListener();


        });

        $(document).on('click', '.makeMaterial', function () {
            var that = $(this);
            var cropImage = $('.pImg').cropper('getCroppedCanvas').toDataURL();
            $('.materialImagePicture').attr('src', cropImage);

        });


        $('#artworkImg').on('change', async function () {
            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {

                //console.log($(this).get(0).files[i].size /1024);
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 120000) {
                    $.notify("Cannot upload image size more than 500kb")
                } else {
                    $('#artworkModal2').modal({
                        keyboard: false
                    });
                    pix.push(await toBase64($(this).get(0).files[i]));
                    $('.artworkprice').html(`
                    <input type="text" placeholder="Enter Material Price">
                    `);
                }

            }
            $(this).val("");
            showArtworkmages(pix);
            addDelListener();
        });

        function addDelListener() {
            $(document).on('click', '.removeIMage', function () {
                var piximg = $(this).attr('pix');
                $(piximg).remove();
            });
        }

        function find(arr, string) {
            var result = [];

            for (i = 0; i < arr.length; i++) {
                if (arr[i].toLowerCase().indexOf(string.toLowerCase()) > -1) {
                    console.log("hey " + string);
                    result.push(arr[i]);
                }
            }

            return result;
        }

        $(document).on('dblclick', '.minicolors', function () {
            $(this).remove();
        });

        function showMaterials(arr) {
            if (arr.length) {
                var material = '';
                for (i = 0; i < arr.length; i++) {
                    // console.log(arr[i]);
                    material += `<label id=${counter} class="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="img-holder-display">
                                            <img src="${arr[i]}" alt="" class="art-modal-img materialImg">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" style="padding: 0">
                                        <div class="form-group">
                                            <label for="price">Enter Price</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><i
                                                                class="fa fa-dollar"></i></span>
                                                </div>
                                                <input type="number" placeholder="Enter Fabric Price" class="form-control fabricPrice">
                                            </div>
                                        </div>
                                        </div>
                                        <div class = "col-md-6">
                                        <div class="form-group">
                                            <label for="fabricName">Enter Artwork Name</label>
                                                <input type="text" class="form-control fabricName" id="fabricName" placeholder="Enter Fabric Name">
                                        </div>
                                    </div>
                                 </div>
                                 </label>`;
                    counter++;
                }

                //$('.materialBox').append(material);
                $('#art-display').append(material);
            }
        }

        function showProductImages(arr) {
            if (arr.length <= 5) {
                var productImage = '';
                for (i = 0; i < arr.length; i++) {
                    productImage += `<label id=${counter} class="proImg">
                  <img class='pImg' src=${arr[i]}>
                  <img class="materialImage" src="" pix='#${counter}'>
                  <i class='pull-right fa fa-trash removeIMage' pix='#${counter}'></i>
                   <i class='pull-right fa fa-check makeMaterial' pix='#${counter}'></i>
               </label>
               `;
                    counter++;

                }
                $('.arrange-proImg').append(productImage);

                var $image = $('.pImg');

                $image.cropper({
                    aspectRatio: 16 / 9,
                });

            } else {
                $.notify("You cannot upload more than 5 images", "", "warning");
            }
        }

        function showMoreProductImages(arr, that) {
            if (arr.length) {
                var productImage = '';
                for (var i = 0; i < arr.length; i++) {
                    productImage += `<label id=${counter} class="moreProImg">
                    <img class='morePImg' src=${arr[i]}>
                    <img class="materialImage" src="" pix='#${counter}'>
                  <i class='pull-right fa fa-trash removeIMage' pix='#${counter}'></i>
                   <i class='pull-right fa fa-check makeMaterial' pix='#${counter}'></i>
                   </label>
                    `;
                    counter++
                }
            }
        }

        function showArtworkmages(arr) {
            if (arr.length) {
                let artworkImage = '';
                for (i = 0; i < arr.length; i++) {
                    artworkImage += `<label id=${counter} class="">
                                <div class="row artWf">
                                    <div class="col-md-12">
                                        <div class="img-holder-display">
                                            <img src=${arr[i]} alt="" class="imgFaB">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="form-group">
                                            <label for="price">Enter Price</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1"><i
                                                                class="fa fa-dollar"></i></span>
                                                </div>
                                                <input type="number" id="price" placeholder="Enter Price for Artwork"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                 </label>`;
                    counter++;
                }
                //$('.arrange-artImg').append(artworkImage);
                $('#art-display-2').append(artworkImage);
            }
        }

        $(document).on('click', '.removeIMage', function () {
            //console.log('hello world');
            var pix = $(this).attr('pix');
            $(pix).remove();
            $('#materialPrice').hide();
        });

        $('[name=producttype]').click(function () {
            prodType = $(this).val();

            if (prodType == 1) {
                $("#material-div").show();
                $("#requiredmeasurement-div").show();
            } else if (prodType == 2 || prodType == 3) {
                $("#requiredmeasurement-div").hide();
                $("#material-div").hide();
            }
        });

        $('#instocks').click(function () {
            if ($(this).is(':checked') && $('#ondemands').is(':checked')) {
                $('.mains').css('display', 'inline-flex');
                $('.measureCorner').removeClass('hide');
                $('#time').removeClass('hide');

            } else if ($(this).is(':checked')) {
                $('.mains').css('display', 'inline-flex');
                $('.measureCorner').removeClass('hide');
            } else {
                $('.mains').css('display', 'none');
                $('.measureCorner').addClass('hide');
            }
        });


        $('#ondemands').click(function () {
            if ($(this).is(':checked') && $('#instocks').is(':checked')) {
                $('.mains').css('display', 'inline-flex');
                $('.measureCorner').removeClass('hide');
                $('#time').removeClass('hide');
                $('#requiredmeasurement-div').removeClass('hide');
                $('#material-div').removeClass('hide');
            } else if ($(this).is(':checked')) {
                $('.mains').css('display', 'none');
                $('.measureCorner').addClass('hide');
                $('#time').removeClass('hide');
                $('#requiredmeasurement-div').removeClass('hide');
                $('#material-div').removeClass('hide');
            } else {
                $('.mains').css('display', 'inline-flex');
                $('.measureCorner').removeClass('hide');
                $('#requiredmeasurement-div').addClass('hide');
                $('#material-div').addClass('hide');
                $('#time').addClass('hide');
            }
        })

        $('[name=availability]').on('click', function () {
            $('#time').toggle();
        });

        $("#customsizetoggle").click(function () {
            if (this.checked) {
                $("#stock-time").show();
            } else {
                $("#stock-time").hide();
            }
        });


        $('#addBtn').on('click', function () {
            $(this).val('Please wait...').attr("disabled", true);
            var productPrices = parseInt($('#productPrice').val());
            var discountPrices = parseInt($('#DiscountproductPrice').val());

            if ($('#accessories').is(':checked') || $('#shoes').is(':checked')) {
                var topLength = [];
                $('.topFabric').each(function () {
                    if (!$(this).is('.submitImg')) {
                        topLength.push($(this));
                        $(this).addClass('validationCheck');
                    }
                });
                // console.log(topLength.length);
                if (topLength.length > 0) {
                    $.notify('Top image must be uploaded', 'error');
                    $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
                } else {
                    addNewProduct(discountPrices, productPrices);
                }

            } else if ($('#outfit').is(':checked')) {
                var mainLength = [];
                $('.mainFabric').each(function () {
                    if (!$(this).is('.submitImg')) {
                        mainLength.push($(this));
                        $(this).addClass('validationCheck');
                    }
                });
                var sideLength = [];
                $('.sideFabric').each(function () {
                    if (!$(this).is('.submitImg')) {
                        sideLength.push($(this));
                        $(this).addClass('validationCheck');
                    }
                });

                if (mainLength.length > 0 || sideLength.length > 0) {
                    swal('Main and Side image must be uploaded', '', 'error');
                    $('#addBtn').val('Submit').removeAttr("disabled", "disabled");

                }
                else {
                    addNewProduct(discountPrices, productPrices);
                }
            }
        });

        $(document).on('keydown', '.required-product', function () {
            $(this).removeClass('tristyle');
        });


        /*  function isEmpty(obj) {
              for (var key in obj) {
                  if (obj.hasOwnProperty(key))
                      return false;
              }
              return true;
          }*/

        function addNewProduct(discountPrices, productPrices) {

            if (discountPrices >= productPrices) {
                swal('Discount price should not be greater than or equal to Product Price', '', 'warning');
                $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
            } else {
                if ($('.measureAttachment table tbody tr').length === 0 && $('#outfit').prop('checked')) {
                    $('.mains').notify('You must add fabric size and quantity');
                    $('.measureAttachment').addClass('tristyle');
                    $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
                } else {

                    if ($('.required-product').val() === '' || $('.mce-container iframe body p').html() === '' || $('#productSummary').val() === '' || $('#productDescription').val() === '' || $('#productPrice').val() === '' || $('#productGroup').val() === '' || $('#subcat').val() === '') {
                        /* $('.required-product, .mce-container').css({
                             'border': '1px solid red'
                         });*/
                        $('.required-product, .mce-container').addClass('tristyle');

                        swal('Some fields are required', '', 'warning');
                        $('#addBtn').val('Submit').removeAttr("disabled", "disabled");


                    } else {

                        $('.colorMaterialName').each(function () {
                            if ($(this).val() === '') {
                                swal('Fabric color name cannot be empty', '', 'error');

                            } else {
                                //$('#loaderModal').modal();
                                $('#loaderModal').modal('show');
                                <?php if(!is_null(session('designerToken'))): ?>
                                    tok = <?= session('designerToken') ?>;
                                <?php else: ?>
                                    tok = '';
                                    <?php endif; ?>

                                var productSizes = [];
                                var matt = [];
                                var art = getAllImagesSrc('.arrange-artImg .pImg');
                                // var mat = getAllFabricSrc('.materialImg');
                                $('.materialImg').each(function () {
                                    matt.push({
                                        materialPicture: $(this).attr('src'),
                                        materialName: $(this).siblings('.fNameDiv').find('.fName').html(),
                                        materialPrice: $(this).siblings('.fPriceDiv').find('.fPrice').html()
                                    })
                                });

                                // selectMeasure
                                //  myMeasure.getSelected()

                                let measurement = JSON.stringify($('.selectMeasure').val());
                                var a = [];
                                /// if (art.length) {
                                $('.artWorkImage').each(function () {
                                    a.push({
                                        picture: $(this).attr('src'),
                                        price: $(this).closest('.imageFabricDiv').find('.fPrice').html()
                                    })
                                })
                                //  }


                                var data = {
                                    "name": $('#productName').val(),
                                    "productPriceDTO": {
                                        "amount": $('#productPrice').val(),
                                        "slashedPrice": $('#DiscountproductPrice').val(),
                                        "percentageDiscount": $('#percent').val(),
                                        "sewingPrice": $('#sewingPrice').val()
                                    },
                                    "prodSummary": $('#productSummary').val(),
                                    "description": $('#productDescription').val(),
                                    "productColorStyleDTOS": [],
                                    "subCategoryId": $('#subcat').val(),
                                    "productType": prodType,
                                    "bespokeProductDTO": {}
                                };

                                if ($('#instocks').is(':checked') && $('#ondemands').is(':checked')) {
                                    data.inStock = "Y";
                                    data.acceptCustomSizes = "Y";
                                    data.numOfDaysToComplete = $('#time').val();
                                } else if ($('#instocks').is(':checked')) {
                                    data.inStock = "Y";
                                    data.acceptCustomSizes = "N";
                                    data.numOfDaysToComplete = 0;
                                } else if ($('#ondemands').is(':checked')) {
                                    data.inStock = "N";
                                    data.acceptCustomSizes = "Y";
                                    data.numOfDaysToComplete = $('#time').val();
                                }

                                $.each($('.showFabricType'), function () {

                                    var arrayPicture = [];
                                    var sizes = [];

                                    $(this).find('.submitImg').each(function () {
                                        arrayPicture.push($(this).attr('src'));
                                    });
                                    $(this).find('.measureAttachment tr').each(function () {
                                        sizes.push({
                                            "name": $(this).find('.selected-size').html(),
                                            "numberInStock": parseInt($(this).find('.selected-stock').html()),
                                            "inStock": 'Y'
                                        });
                                    });
                                    // data.picture = arrayPicture;

                                    data.productColorStyleDTOS.push({
                                        "colourName": $(this).find('.colorMaterialName').val(),
                                        "colourPicture": $(this).find('.displayMat').attr('src'),
                                        "picture": arrayPicture,
                                        "productSizes": sizes
                                    });
                                });

                                //  var bespokeProductDTO = {}

                                console.log('submitting from');

                                if (prodType === 1) {
                                    data.bespokeProductDTO.mandatoryMeasurements = JSON.stringify(measurement);
                                    data.bespokeProductDTO.artWorkPicture = a;
                                    data.bespokeProductDTO.materialPicture = matt;
                                    data.materialPrice = $('#materialPrice').val();
                                    data.materialName = $('#materialName').val();
                                    data.bespokeProductDTO.numOfDaysToComplete = $('#time').val()
                                } else if (prodType === 2 || prodType === 3) {
                                    data.bespokeProductDTO.mandatoryMeasurements = JSON.stringify(measurement);
                                }


                                if ($('#ondemands').prop('checked')) {
                                    console.log('aa');
                                } else {
                                    data.bespokeProductDTO = null;
                                }

                                console.log(data);
                                        $.ajax({
                                            url: "{{route('designer.addNewProduct')}}",
                                            type: "POST",
                                            dataType: "json",
                                            data: JSON.stringify(data),
                                            success: function (res) {
                                                 console.log(res);
                                                if (res.status === '00') {
                                                    //$('#loaderModal').hide();
                                                    //$('#loaderModal').modal('hide');
                                                    $(this).val('Please wait...').attr("disabled", false);
                                                    swal('Product added successfully', '', 'success');
                                                    window.location.href = '{{route('designer.product', str_slug(strtolower($storeName)))}}';
                                                } else {
                                                    $('#loaderModal').modal('hide');
                                                    $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
                                                    $(this).val('Submit').removeAttr('disabled', 'disabled');
                                                    swal(res.message, '', 'error');
                                                }

                                            },
                                            error: function (e) {
                                                //$('#loaderModal').modal('hide');
                                                $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
                                                swal(e, '', 'error');
                                            }
                                        })



                            }


                        });


                    }
                }
            }
        }

    </script>

@endpush