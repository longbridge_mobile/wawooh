<script>
    $(document).ready(function () {

        var measure = JSON.parse(sessionStorage.getItem('measurement')) ? JSON.parse(sessionStorage.getItem('measurement')) : {};
        $(".measurementName").val(measure.name );
        // console.log(measure);
        let a = [];

        $('.pic').each(function(){
            a.push((($(this).siblings('label').html()).toLowerCase()).replace(/ /g, ''));
        })
        // console.log(a);

        for (var key in measure) {
            if (measure.hasOwnProperty(key) && key !== 'name' && key !== 'unit' && key !== ' ') {
                $(".pic").each(function(){
                    if((($(this).siblings('label').html()).toLowerCase()).replace(/ /g, '') === (key.toLowerCase()).replace(/ /g, '')){
                        $(this).val(measure[key]);
                    }
                });
            }
        }
        sessionStorage.removeItem("measurement");

        var bespokeContent = JSON.parse(sessionStorage.getItem('bespokeContent')) ? JSON.parse(sessionStorage.getItem('bespokeContent')) : {};
        $('.sample-artwork').attr('src', bespokeContent.sideImage);
        $('#requestGender').val(bespokeContent.gender);
        $('#requestNote').val(bespokeContent.notes);
        $('#fabricAvailability').val(bespokeContent.hasFabric);
        $('#userHeight').val(bespokeContent.height);
        $('#dateOfDelivery').val(bespokeContent.timeOfDelivery);
        $('#userBudget').val(bespokeContent.budget);
        $('#fabricType').val(bespokeContent.fabricType);


        $('#u-sample').on('click', function (e) {
            e.preventDefault();
            $('#u-sample').hide(300);
            $('#d-sample').show(500);
            $('#dis-upload').removeClass('hide').fadeIn(300);
        });

        $('#d-sample').on('click', function (e) {
            e.preventDefault();
            $('#d-sample').hide(300);
            $('#u-sample').show(500);
            $('#dis-upload').addClass('hide').fadeOut(300);
        });

        let owl = $('#se-result');
        owl.owlCarousel({
            margin: 10,
            nav: false,
            loop: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 5
                }
            }
        })

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        //Dynamic Next
        $(".btn-next").on("click", function(e){
            e.preventDefault();
            nextStep = $("#" + $(this).parents(".slider-form").data("nextStep"));
            $(this).parents(".slider-form").attr("data-anim","hide-to--left");
            nextStep.attr("data-anim","show-from--right");
        });

        //Dynamic Back
        $(".btn-prev").on("click", function(e){
            e.preventDefault();
            backTo = $("#" + $(this).parents(".slider-form").data("backTo"));
            $(this).parents(".slider-form").attr("data-anim","hide-to--right");
            backTo.attr("data-anim","show-from--left");
        });

        var login = false;
        var tok = '';
                @if(!is_null(session('userToken')))
        var tok = @php echo stripslashes(session('userToken')) @endphp;
        @else
            login = true;
        var tok = '';
        @endif

            measurements = JSON.parse('<?= isset($_COOKIE['measurements']) ? $_COOKIE['measurements'] : '{}' ?>');
        <?php if (isset($_COOKIE['measurements'])) {
            unset($_COOKIE['measurements']);
            setcookie('measurements', null, -1);
        } ?>
        if (measurements.length > 0) {
            for (ids in measurements) {
                $("#" + ids).val(measurements[ids]);
            }

            $('.collapse').addClass('show');
            //$(".measurementBox").show();
            // $('input[value="'+measurements.unit+'"]').prop("checked", true);
            // $('input[value="'+measurements.unit+'"]').prop("checked", true);

        }

        <?php if (isset($_COOKIE['measurements'])) {
        } ?>

        $('[name = "fabricAvailable"]').change(function () {
            if (this.value === "Yes") {
                $('#preferred-fabric').hide();
                $('#name-fabric').show();
            }
            else if (this.value === "No") {
                $('#preferred-fabric').show();
                $('#name-fabric').hide();
            }
        });

        var activeFeet = 'feet';

        $('#selectInches').on('click', function () {
            $('.feetGroup').toggle();
            $('.InchesGroup').toggle();
            // $('.HFeet').reset();
            $(this).toggle();
            $('#selectFeet').toggle();
            activeFeet = 'inches';
        });

        $('#selectFeet').on('click', function () {
            $('.feetGroup').toggle();
            $('.InchesGroup').toggle();
            // $('.HInches').reset();
            $(this).toggle();
            $('#selectInches').show();
            activeFeet = 'feet';
        });

        $('#female').click(function () {
            if ($(this).val() === 'female') {
                $('.f').show();
                $('.m').hide();
                $('.f-v').val() === '';
            } else $('.m').hide();
        });

        $('[name = "fabricAvailable"]').change(function () {
            if (this.value === "Yes") {
                $('#preferred-fabric').hide();
                $('#name-fabric').show();
            }
            else if (this.value === "No") {
                $('#preferred-fabric').show();
                $('#name-fabric').hide();
            }
        });

        $('#male').click(function () {
            if ($(this).val() === 'male') {
                $('.m').show();
                $('.f').hide();
                $('.m-v').val() === ''
            } else ('.m').show();
        });

        $('.measure').keypress(function () {
            if (this.value.length > 4) {
                return false;
            }
        });

        $('.pic').on('focus', function () {
            let name = $(this).attr('id');
            let folder = '';
            if ($('[name=gend]:checked').val() === 'male') {
                folder = 'male';
            } else {
                folder = 'female';
            }
            $('#pictureHolder').attr('src', '/img/style/' + folder + '/' + name + '.jpg');
        });

        /*$('[name=inchesRadios]').on('click', function () {
            let value = $('[name=inchesRadios]:checked').val();
            $('#labelUnit').text(value);
        });*/

        $('.editMeasurement').on('click', function () {
            $('.m-load').show();
            $('.editMeasurement').css('color', '#cd9933');
            $('.addNew').hide(300);
            $('.collapse').addClass('show');
            $(this).css('color', 'black');
            $($(this).attr('data-target')).prop('checked', true);
            idx = $(this).attr('id');
            $.ajax({
                url: `{{route('editMeasurement')}}/?id=${idx}`,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (result) {
                    if (result.status === "00") {
                        $('.m-load').hide();
                        var keys = Object.keys(result.data);
                        for (let value of keys) {
                            var valText = value.toLowerCase();
                            $('#' + valText).val((result.data[value]));
                        }
                        $('.unitMeasurement').removeAttr('checked');
                        $('.unitMeasurement').each(function () {
                            if ($(this).val() === result.data.unit) {
                                $(this).attr('checked', 'checked');
                            }
                        });
                        $('.measurementBox').show();
                        $('.updateMeasurement').show().attr('idx', idx);
                        $('.cancelMeasurement').show();
                        $('.saveMeasurement').hide();
                    }

                },
                error: function (e) {
                    swal(e, '', 'error');
                }
            });
        });

        //Save Measurement
        $('.saveMeasurement, .updateMeasurement').on('click', function () {
            if ($('.measure').val() === '') {
                // $('.measure').notify('Cannot be empty');
                $('.measure').addClass('error');
            } else {
                if (isNotEmpty('.name', 'error')) {
                    if ($('[name=inchesRadios]').is(':checked')) {
                        var btn = $(this).attr('status');
                        var url = '{{route('addMeasurement')}}';
                        var unit = $('[name=inchesRadios]:checked').val();
                        var fitness = $('[name=fitness]:checked').val();
                        var dat = '';
                        var data = {
                            "name": $('#name').val(),
                            "ankle": $('#ankle').val(),
                            "armHole": $('#armHole').val(),
                            "backShirtLength": $('#backShirtLength').val(),
                            "biceps": $('#biceps').val(),
                            "blouseLength": $('#blouseLength').val(),
                            "bubaLength": $('#bubaLength').val(),
                            "bust": $('#bust').val(),
                            "crotch": $('#crotch').val(),
                            "chest": $('#fullChest').val(),
                            "elbowCircumference": $('#elbowCircumference').val(),
                            "fistCircumference": $('#fistCircumference').val(),
                            "fullLength": $('#fullLength').val(),
                            "halfLength": $('#halfLength').val(),
                            "hips": $('#hips').val(),
                            "kneeCircumference": $('#kneeCircumference').val(),
                            "kneeLength": $('#kneeLength').val(),
                            "lapCircumference": $('#lapCircumference').val(),
                            "longSleeve": $('#longSleeve').val(),
                            "neck": $('#neck').val(),
                            "senatorLength": $('#senatorLength').val(),
                            "shirtLength": $('#shirtLength').val(),
                            "shortSleeve": $('#shortSleeve').val(),
                            "shortsLength": $('#shortsLength').val(),
                            "shoulderWidth": $('#shoulderWidth').val(),
                            "thigh": $('#thigh').val(),
                            "trouserLength": $('#trouserLength').val(),
                            "threeQuarterSleeve": $('#threeQuarterSleeve').val(),
                            "waist": $('#waist').val(),
                            "wrist": $('#wrist').val(),
                            "unit": unit,
                            "fitness": fitness
                        }
                        console.log(data);
                        if (btn === '2') {
                            url = '{{route('updateMeasurement')}}';
                            dat = $(this).attr('idx');
                            data = {
                                "name": $('#name').val(),
                                "ankle": $('#ankle').val(),
                                "armHole": $('#armHole').val(),
                                "backShirtLength": $('#backShirtLength').val(),
                                "biceps": $('#biceps').val(),
                                "blouseLength": $('#blouseLength').val(),
                                "bubaLength": $('#bubaLength').val(),
                                "bust": $('#bust').val(),
                                "crotch": $('#crotch').val(),
                                "chest": $('#fullChest').val(),
                                "elbowCircumference": $('#elbowCircumference').val(),
                                "fistCircumference": $('#fistCircumference').val(),
                                "fullLength": $('#fullLength').val(),
                                "halfLength": $('#halfLength').val(),
                                "hips": $('#hips').val(),
                                "kneeCircumference": $('#kneeCircumference').val(),
                                "kneeLength": $('#kneeLength').val(),
                                "lapCircumference": $('#lapCircumference').val(),
                                "longSleeve": $('#longSleeve').val(),
                                "neck": $('#neck').val(),
                                "senatorLength": $('#senatorLength').val(),
                                "shirtLength": $('#shirtLength').val(),
                                "shortSleeve": $('#shortSleeve').val(),
                                "shortsLength": $('#shortsLength').val(),
                                "shoulderWidth": $('#shoulderWidth').val(),
                                "trouserLength": $('#trouserLength').val(),
                                "thigh": $('#thigh').val(),
                                "threeQuarterSleeve": $('#threeQuarterSleeve').val(),
                                "waist": $('#waist').val(),
                                "wrist": $('#wrist').val(),
                                "id": dat,
                                "unit": unit,
                                "fitness": fitness
                            }
                        }
                        if (login) {
                            let addNote = $('#requestNote').val(),
                                measurement = $('[name=mz]:checked').val(),
                                userGender = $('#requestGender').val(),
                                userHeight = $('#userHeight').val(),
                                dateOfDelivery = $('#dateOfDelivery').val(),
                                budget = $('#userBudget').val(),
                                fabricAvailability = $('#fabricAvailability').val(),
                                fabricType = $('#fabricType').val();

                            let datas = {
                                "budget": budget,
                                "notes": addNote,
                                measurement: {
                                    "id": measurement
                                },
                                "gender": userGender,
                                "timeOfDelivery": dateOfDelivery,
                                "height": userHeight,
                                "fabricType": fabricType,
                                "hasFabric":fabricAvailability,
                                "sideImage": $('.sample-artwork').attr('src') ? $('.sample-artwork').attr('src') : null,
                                "frontImage": $('.uploadedImage-one').attr('src') ? $('.uploadedImage-one').attr('src') : null,
                                "backImage": $('uploadedImage-three').attr('src') ? $('uploadedImage-three').attr('src') : null
                            };

                            console.log('two');
                            var d = new Date();
                            d.setTime(d.getTime() + (5 * 60 * 1000));
                            var expires = "expires=" + d.toUTCString();
                            var tempData = data;
                            // console.log(tempData);
                            sessionStorage.setItem('measurement', JSON.stringify(tempData));
                            sessionStorage.setItem('bespokeContent', JSON.stringify(datas));
                            tempData.q = $("#q").val();
                            // console.log($('#q').val());
                            document.cookie = 'measurements = ' + JSON.stringify(tempData) + '; ' + expires + ';';
                            $('#ModalSignIn').modal();
                        }
                        else {
                            // console.log('three');
                            $('#loaderModal').modal();
                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                url: url,
                                success: function (result) {
                                    $('#loaderModal').modal('hide');
                                    $('div.loader').hide();

                                    if (result.status === "00") {
                                        swal('Measurement Saved Successfully', '', 'success');
                                        location.reload();
                                    } else if (result.status === "56") {
                                        $.notify('Please log in to continue', 'error');
                                    }
                                    else if (result.status === "99") {
                                        swal(result.message, '', 'error');
                                    }
                                },
                                error: function (e) {
                                    $('#loaderModal').modal('hide');
                                    $('div.loader').hide();
                                    $('#loaderModal').modal('hide');
                                },
                            });
                        }
                    } else {
                        $.notify('Select a unit', 'error');
                    }
                } else {
                    $.notify('Enter a measurement name', 'error');
                }
            }
        });

        $('.cancelMeasurement').on('click', function () {
            $('.collapse').removeClass('show');
            $('.updateMeasurement').hide();
            $('.addNew').text('Add measurement').show();
            $('.custom').val('');
            $('.measurementBox').hide(300);
            $('.editMeasurement').css('color', '#cd9933');

        });

        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            });
        }

        setInputFilter(document.getElementById("userBudget"), function(value) {
            return /^\d*\.?\d*$/.test(value);
        });


        $('.addBespokeImage').on('change', function () {
            let file = $(this).get(0).files[0];
            if (file.size / 1024 > 120000) {
                $.notify("Cannot upload image size more than 2MB");
            } else {
                convertBespokeRequest(this, '.sample-artwork');
                $(this).closest('.col-md-6').siblings('.col-md-6').find('.sample-artwork').addClass('uploadedImage-one');
                $(this).closest('.col-md-6').siblings('.col-md-6').find('.sample-artwork').addClass('submitImg');
            }
        });

        $('#submitRequest').on('click', function () {
            var validateImage = [];
            $('.sample-artwork').each(function () {
                if ($(this).is('.submitImg')) {
                    validateImage.push($(this));
                }
            });

            let addNote = $('#requestNote').val(),
                measurement = parseInt($('[name=mz]:checked').val()),
                userGender = $('#requestGender').val(),
                userHeight = $('#userHeight').val(),
                dateOfDelivery = $('#dateOfDelivery').val(),
                budget = $('#userBudget').val(),
                fabricAvailability = $('#fabricAvailability').val(),
                fabricType = $('#fabricType').val();

            let data = {
                "budget": parseInt(budget),
                "acceptedBidId": 0,
                "notes": addNote,
                "measurement": {
                    id: measurement
                },
                "bespokeStyleId": parseInt($(".imageBespoke:checked").val()),
                "gender": userGender,
                "timeOfDelivery": dateOfDelivery,
                "height": userHeight,
                "categoryId": parseInt($('#requestGender').val()),
                "fabricType": fabricType,
                "hasFabric":fabricAvailability,
                "sideImage": $('.sample-artwork').hasClass('submitImg') ? $('.sample-artwork').attr('src') : null,
                "frontImage": null,
                "heightFeet": $('#userHeight').val(),
                "heightInches": $('#userHeightInch').val(),
                "backImage": null
            };

            console.log(data);

            $('#submitRequest').val('Processing...').attr('disabled', 'disabled');
            if ($('input[name=mz]:checked').length <= 0) {
                sessionStorage.setItem('bespokeContent', JSON.stringify(data));

                swal('Select your measurement', '', 'warning');
                $('#submitRequest').val('Place Request').removeAttr('disabled', 'disabled');
            } else if (dateOfDelivery === '') {
                swal('Fill all required fields','','warning');
                $('#submitRequest').val('Place Request').removeAttr('disabled', 'disabled');
                $('#userHeight, #dateOfDelivery').addClass('error');
            }
            else {
                if(dateOfDelivery !== '' || addNote !== '')
                {
                    $('#userHeight, #dateOfDelivery, #requestNote').removeClass('error') ;
                }

                var url = '{{route('addBespokeMeasurement')}}';

                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: url,
                    success: function (result) {
                        console.log(result);
                        $('#loaderModal').modal('hide');
                        $('div.loader').hide();

                        if (result.status === "00") {
                            $('#submitRequest').val('Request Placed Successfully. Redirecting...').attr('disabled', 'disabled');
                            // swal('Measurement Saved Successfully', '', 'success');
                            window.location.href = "{{route('bespokeSuccessful')}}"
                        } else if (result.status === "56") {
                            $('#submitRequest').val('Place Request').removeAttr('disabled', 'disabled');
                            $.notify('Please log in to continue', 'error');
                        }
                        else if (result.status === "99") {
                            $('#submitRequest').val('Place Request').removeAttr('disabled', 'disabled');
                            swal(result.message, '', 'error');
                        }
                    },
                    error: function (e) {
                        $('#submitRequest').val('Place Request').removeAttr('disabled', 'disabled');
                        swal(e, '','error');
                    },
                });
            }
        });

        $('#searchBespoke').click(function () {
            var data = $('#search-bespoke').val();
            var url = "{{route('searchForImage')}}";
            window.location.href = "{{route('bespokePreOrder')}}?searchTerm=" + data;

            /*$.ajax({
                type: 'POST',
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                url: url,
                success: function(result){
                    console.log(result);
                },
                error: function(error){
                    console.log(error);
                }
            })*/
        })
    })

    function convertBespokeRequest(input, holder){
        if(input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e){
                $(input).parents('.upload-sample').find(holder).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>