<script>
    function getToken() {
        var t = localStorage.getItem('UserToken');
        if (t) {
            token = { "Authorization": JSON.parse(t) };

        } else {
            token = 't';
        }
    }

    getToken();

    function mainUrl(){
        return "{{env('GET_BASE_URL')}}";
    }

    //convert image toBase64
    function toBase64(x) {
        var fileReader = new FileReader();
        var data;

        return new Promise(resolve => {
            fileReader.addEventListener('load', () => {
                resolve(fileReader.result);
            });
            fileReader.readAsDataURL(x);
        });
    }

    //get one image src
    function getImageSrc(className) {
        var pix = '';
        // $(className).each(function(i, obj) {
        //     pix += $(this).attr('src');
        // });
        pix += $(className).attr('src');

        return pix;
    }

    //get all values from same classes and return
    function getAll(className) {
        var result = [];
        $(className).each(function(i, obj) {
            result.push($(this).val());
        });

        return result;
    }

    //convert all uploaded pix to base64
    function getAllPicturesinBase64(className) {
        var pix = [];
        $(className).each(async function(i, obj) {
            pix.push(await toBase64($(this).get(0).files[0]));
        });

        return pix;
    }

    //get all fabric name and image src only
    function getAllFabricSrc(className) {
        var pix = [];
        $(className).each(function(i, obj) {
            pix.push({
                'name': $(this).parent('.fabricName').siblings('input').val(),
                'materialPicture': getImageSrc($(this))
            });
        });
        return pix;
    }

    //get all images src
    function getAllImagesSrc(className) {
        var pix = [];
        $(className).each(function(i, obj) {
            pix.push($(this).attr('src'));
        });
        return pix;
    }

    function getAllImagesSrcWithId(className){
        var pix = [];
        $(className).each(function(i, obj){
            pix.push({
                "id": $(this).attr('pictureId'),
                "picture": $(this).attr('src')
            });
        });
    }

    // convert img path and coonvert image to base64
    function readUrl(input, holder) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(holder).attr('src', e.target.result).addClass('img-thumbnail');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function convertToEditBase64(input, holder){
        if(input.files && input.files[0]){
            var reader = new FileReader();
            reader.onload = function (e) {
                $(input).siblings(holder).attr('src', e.target.result);
                // $(holder).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function convertToBase64(input, holder) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(holder).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function convertMoreToBase64(input, holder){
        if(input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e){
                //$(holder)
                $(input).siblings(holder).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function convertCropperToBase64(input, holder){
        if(input.files && input.files[0]){
            var reader = new FileReader();
            reader.onload = function(e){
                $(input).siblings('.cropper-container').find(holder).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function getEventImages(id, arr, arr2 = null) {
        var temp = [];

        $.ajax({
            /* url: "https://jsonplaceholder.typicode.com/photos?_page="+page+"&_limit="+limit,
            type: "GET",  */
            url: `${mainUrl()}/fashion/event/${id}/geteventbyId`,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            async: false,
            success: function(data) {

                for (i = 0; i < data.data.event.length; i++) {
                    temp.push(data.data.event[i]);
                    // arr2.push(data.data);
                    // console.log(data.data);
                }


            },
            error: function(e) {
                $.notify('A network error occurred. Please try again', 'error');
                console.log(e);

            }
        });

        return temp;
    }

    function getAllUntagged(data) {
        $.ajax({

            url: `${mainUrl()}/fashion/product/getuntagged`,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify(data),
            success: function(result) {
                console.log(result);
                var html = '';
                for (i = 0; i < result.data.eventpictures.length; i++) {
                    html += `<div class="card-image">
                                <img class='pix' picture-id='${result.data.eventpictures[i].id}' style='width:100%;height:100%' src='${mainUrl()}${result.data.eventpictures[i].picture}' alt="Avatar" >

                            </div>`;

                }

                $('.card-holder').html(html);

                console.log(result);
            },
            error: function() {
                $('.loader').addClass('hide');

            }
        });
    }

    function sendTaggedPicture(data, key) {
        console.log(JSON.stringify(data));

        $.ajax({
            url: `${mainUrl()}/fashion/product/addTag`,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            data: JSON.stringify(data),
            success: function(data) {
                $('.loader').addClass('hide');

                if (data.data.success == 'success') {
                    localStorage.removeItem(key);
                }
                console.log(data);
            },
            error: function(e) {
                $('.loader').addClass('hide');
                console.log(e);

            }
        });
    }

    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }



    }

    function getPictureById(id, holder) {
        $.ajax({
            url: `${mainUrl()}/fashion/event/${id}/geteventpicturebyid`,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(data) {
                var html = '';
                console.log(data);


                $(holder).attr('src', `${mainUrl()}${data.data.eventpicture.picture}`).attr('data-target', `pix${id}`);
                console.log(data.data.eventpicture.picture);
                $('#submitTag').attr('ls_var', `pix${id}`)


            },
            error: function(e) {
                console.log(e);

            }
        });
    }

    function updateProductImage(data) {
        console.log(JSON.stringify(data));
        $.ajax({
            url: `${mainUrl()}/fashion/product/updateproductimage`,
            type: "POST",
            headers: token,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(result) {
                console.log(result);
                //location.reload();
            },
            error: function(e) {
                console.log(e);
            }
        });
    }

    function getAllProductByDesigner(holder) {

        $.ajax({
            url: `${mainUrl()}/fashion/product/getdesignerproducts`,
            type: "GET",
            headers: token,


            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(result) {
                var html = '';
                var serial = 1


                if (result.data.products.length) {

                    for (i = 0; i < result.data.products.length; i++) {
                        if (result.data.products[i].verifiedFlag == 'N') {
                            var status = `<i class='text-danger'>Not yet approved</i>`;
                            disabled = 'disabled';
                        } else {
                            disabled = '';
                            var status = `<i class='text-success'>Approved</i>`;
                        }
                        var disabled;
                        var action = 'Show';
                        var bg = 'success';

                        if (result.data.products[i].status == 'A') {
                            action = 'Hide';
                            bg = 'danger';
                        }
                        console.log(result.data.products[i].color.length);
                        if (result.data.products[i].color.length) {
                            var color = '';

                            for (j = 0; j < result.data.products[i].color.length; j++) {
                                color += `<div class='inline-block circle' style='background:${result.data.products[i].color[j]};height:15px;width:15px;'></div>
                            `;

                            }
                        }
                        if (result.data.products[i].sizes.length) {
                            var size = '';
                            for (k = 0; k < result.data.products[i].sizes.length; k++) {
                                size += result.data.products[i].sizes[k] + ',';
                            }
                        }
                        html += `<tr>
                                <td>${serial}</td>
                                <td>
                                    <img class='product' productId=${result.data.products[i].id} style='width:100px;height:auto'src="${mainUrl()}${result.data.products[i].picture[0]}" alt="Avatar" >

                                </td>
                                <td>${result.data.products[i].description}</td>
                                <td>
                                   ${color}
                                </td>
                                <td>
                                ${size}
                                </td>
                                <td>
                                    #${result.data.products[i].amount}
                                </td>

                                <td>
                                ${result.data.products[i].stockNo}
                                </td>
                                <td>
                                        <div class='inline-block '  style='padding:5px'><i productId=${result.data.products[i].id} class='fa action edit-product fa-edit text-primary' title='Edit Product'></i></div>
                                        <div class='inline-block '  style='padding:5px'><i productId=${result.data.products[i].id} class='fa action delete-product fa-trash text-danger' title='Remove Product'></i></div>
                                        <div class='inline-block '  style='padding:5px'><button productStatus=${result.data.products[i].status} productId=${result.data.products[i].id} class='btn btn-xs btn-${bg} changeVisibility btn-fill ${disabled}'>${action}</button></div>

                                </td>
                                <td>
                                    <i class='text-danger'>${status}</i>
                                </td>
                        </tr>`;
                        serial++;

                    }

                    $(holder).html(html);

                } else {
                    html = `<tr>
                            <td colspan='9'>
                                <div class='alert text-center  alert-danger card'>You have not added a product yet. Click
                                <a style='font-size:9px' href="http://localhost:8000/designer/product" class="btn  btn-link no-bd-rad btn-xs">Here</a>
                                to add a product
                                </div>
                            </td>

                        </tr>`;
                    $(holder).html(html);

                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    }

    function deleteProduct(id) {
        var flag = '';
        $.ajax({
            url: `${mainUrl()}/fashion/product/${id}/deleteproduct`,
            type: "GET",
            // async: false,
            headers: token,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(result) {
                if (result.success == 'success') {
                    flag = true;
                } else {
                    flag = false;
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
        return flag;
    }

    function changeVisibility(id, status) {
        var flag = '';
        $.ajax({
            url: `${mainUrl()}/fashion/product/${id}/productvisibility/${status}`,
            type: "GET",
            // async: false,
            headers: token,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(result) {
                console.log(result.status);
                if (result.status == 'Success') {
                    flag = true;
                } else {
                    flag = false;
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
        return flag;
    }

    function getProductImages(id) {
        var images = '';
        $.ajax({
            url: `${mainUrl()}/fashion/product/${id}/getproductbyid`,
            type: "GET",
            // async: false,
            headers: token,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function(result) {
                console.log(result);

                images = result;

            },
            error: function(e) {
                console.log(e);
            }
        });

        return images;
    }

    function urlToBase64(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            var reader = new FileReader();
            reader.onloadend = function() {
                callback(reader.result);

            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }

    function validateUrl(str) {
        var tarea = str;
        if (tarea.indexOf("http://") == 0 || tarea.indexOf("https://") == 0) {
            return true;
        }
        return false;
    }

    function convertToSlug(Text) {
        return Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
    }

    function appendEventImages(images, holder, cover) {
        if (images.length) {
            var html = '';

            while (images.length) {
                b = images.splice(0, 6);
                html += `<div class="wrapper">
                                        `;

                for (j = 0; j < b.length; j++) {
                    k = j + 1;
                    html += `<div class="box box${k}" imageId='${b[j].id}' style='background-image:url(${mainUrl()}${b[j].picture}) !important' >

                </div>`;
                }

                html += `</div>`;
                $(cover).css('background-image', `url(${mainUrl()}${b[1].picture}`)
                $(cover + ' h2').text(`${b[1].eventName}`);

            }

            $(holder).append(html);
        }
    }

    function setImageInf(id, arr) {
        var temp = '';
        console.log(token);
        $.ajax({
            url: `${mainUrl()}/fashion/event/${id}/geteventpicturebyid`,
            type: "GET",

            headers: token,
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            async: false,
            success: function(result) {
                console.log(result);
                $('#eventName').text(result.data.eventpicture.eventName);
                var like_length = result.data.eventpicture.likes.length;
                $('#no_of_likes').text(like_length);
                $('#like-btn').attr('pictureid', `${result.data.eventpicture.id}`);
                $('#commentBox').attr('pictureid', `${result.data.eventpicture.id}`);
                var htm = `<img id='mainPicture' src=${mainUrl()}${result.data.eventpicture.picture} class='img-fluid center block full-height'/>

           <div class='alert  full-width hide tag-items-box'>
                hello
           </div>

           `;
                $('.img-tag').html(htm);
                var html = '';
                if (result.data.eventpicture.comments.length) {
                    for (i = 0; i < result.data.eventpicture.comments.length; i++) {
                        //    var date =  moment(result.data.eventpicture.comments[i].createdDate).format("ll").fromNow();
                        var date = moment(result.data.eventpicture.comments[i].createdDate).format("ll");

                        html += `<div class="personal-comment">
                                        <p><span>${result.data.eventpicture.comments[i].user.firstName} ${result.data.eventpicture.comments[i].user.lastName}</span>
                                        ${result.data.eventpicture.comments[i].comment}
                                        <i class='pull-right'>${date}</i>
                                        </p>
                                    </div>`;

                    }

                }
                $('.user-comments').html(html);
                temp = result.data.eventpicture.tags;

            },
            error: function(e) {
                $.notify('A network error occurred. Please try again later', 'error');
                console.log(e);
            }
        });

        return temp;

    }

    function autoExpand(element) {
        while ($(element).outerHeight() < element.scrollHeight + parseFloat($(element).css("borderTopWidth")) + parseFloat($(element).css("borderBottomWidth"))) {
            $(element).height($(element).height() + 1);
        };
    }

    var delay = (function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();
</script>