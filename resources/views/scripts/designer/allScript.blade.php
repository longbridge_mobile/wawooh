<!-- plugins:js -->
<script src="{{asset('js/dash/jquery-1.9.1.min.js')}}"></script>
{{--<script src="{{ asset('js/jquery.easing.min.js') }}"></script>--}}
{{-- <script src = "{{asset('js/dash/jquery.easing.min.js')}}"></script>--}}
{{--<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>--}}
<script src="{{ asset('js/dash/popper.min.js') }}"></script>
<script src="{{ asset('js/dash/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dash/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="{{ asset('js/dash/jquery.barrating.min.js') }}"></script>
<script src="{{ asset('js/dash/Chart.min.js') }}"></script>
<script src="{{ asset('js/dash/raphael.min.js') }}"></script>
<script src="{{ asset('js/dash/morris.min.js') }}"></script>
<script src="{{ asset('js/dash/jquery.sparkline.min.js') }}"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{ asset('js/dash/off-canvas.js') }}"></script>
<script src="{{ asset('js/dash/hoverable-collapse.js') }}"></script>
<script src="{{ asset('js/dash/misc.js') }}"></script>
<script src="{{ asset('js/dash/settings.js') }}"></script>
<script src="{{ asset('js/dash/todolist.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ asset('js/dash/dashboard.js') }}"></script>
<!-- End custom js for this page-->
{{--<script src="{{ asset('js/utility.js') }}"></script>--}}
@include('scripts.utility')
<script src="{{ asset('js/plugins/colorpicker.js') }}"></script>
<script src="{{ asset('js/validation.js') }}"></script>
<script src="{{ asset('js/commons.js') }}"></script>
<script src="{{ asset('js/plugins/jquery.initialize.min.js') }}"></script>
<script src="{{ asset('js/plugins/notify.min.js') }}"></script>
<script src="{{ asset('js/intlTelInput.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/sellect.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.js"></script>--}}
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="{{asset('js/cropper.min.js')}}"></script>
<script src="{{ asset('js/countries.js') }}"></script>
<script src="{{ asset('js/general.js') }}"></script>
<script src="{{ asset('js/dash/designer.js') }}"></script>