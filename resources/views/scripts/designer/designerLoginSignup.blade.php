<script type="text/javascript">
    $(document).ready(function () {
        $('#login').on('click', function () {
            if($('#signEmail').val() === '' || $('#signPassword').val() === '') {
                swal('Please, Enter your email & password','','warning');
                $('#login').val("Authenticate Me").removeAttr("disabled", "disabled");
                $('.l-image').hide();
            } else {
                let data = {
                    "email": $('#signEmail').val(),
                    "password": $('#signPassword').val(),
                    "socialFlag": 'N'
                };
                let encodeEmail = btoa(data.email);
                $('#login').val("Please wait...").attr("disabled", "disabled");
                $('.l-image').show();
                //console.log(data);

                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: `{{ route('designer.login') }}`,
                    success: function (result) {
                        console.log(result);
                        if (result.status == 0) {
                            if (result.data.role == 2) {
                                $('.l-image').hide();
                                $('#login').val("Logged in successfully").attr("disabled", "disabled");
                                window.location.href = '{{route('designer.dashboard')}}'
                            }
                            else {
                                swal('User is not a designer', '', 'error');
                                $('#login').val('Authenticate Me').removeAttr("disabled", "disabled");
                                $('.l-image').hide();
                            }
                        }
                        else if (result.status === '57') {
                            swal(result.message, 'Click Resend Token', 'error');
                            //console.log(result.message);
                            window.location.href = '{{route('designer.regComplete')}}?email=' + encodeEmail;
                            $('#login').val('Authenticate Me').removeAttr("disabled", "disabled");
                            $('.l-image').hide();
                        } else {
                            swal(result.message, '', 'error');
                            //console.log(result.message);
                            $('#login').val('Authenticate Me').removeAttr("disabled", "disabled");
                            $('.l-image').hide();
                        }
                    },
                    error: function (e) {
                        $('#login').val('Authenticate Me').removeAttr("disabled", "disabled");
                        $('.l-image').hide();
                        swal(e, '', 'error');
                    }
                });
            }
        });

        $('#register').on('click', function () {
            $('#register').val("Please wait...").attr("disabled", "disabled");
            $('.l-image').show();
            if (isNotEmpty('.reg')) {
                if($('#passwordReg').val() === $('#confpasswordReg').val()) {
                    // storeName = $('#storeName').val()
                    let vendorEmail = $('#emailReg').val(),
                        phoneNo = $('#phoneNo').val(),
                        pwd = $('#passwordReg').val();
                        conf = $('#confpasswordReg').val();

                    if (isNumeric(phoneNo) && phoneNo.length === 11) {
                        if (validateEmail(vendorEmail)) {
                            if ($('#terms').is(':checked')) {
                                let data = {
                                    "email": vendorEmail,
                                    "phoneNo": phoneNo,
                                    "password": pwd,
                                    "socialFlag": 'N',
                                    "role": "{{env('DES_ROLE')}}",
                                    "designer": {
                                        "storeName": ''
                                    }
                                };
                                let encodeEmail = btoa(data.email);
                                $.ajax({
                                    url: `{{route('designer.register')}}`,
                                    type: 'POST',
                                    data: JSON.stringify(data),
                                    contentType: "application/json; charset=utf-8",
                                    dataType: 'json',
                                    success: function (result) {
                                        console.log(result);
                                        if (result.status === '00') {
                                            $('.l-image').hide();
                                            $('#register').val("Registration successfully").attr("disabled", "disabled");
                                            // swal(result.message, '', 'success');
                                           // swal('Registration was successful. An activation code has been sent to the ' + phoneNo + ' and ' + vendorEmail + 'provided', '', 'success');
                                            swal('Registration was successful', '', 'success');
                                          //  window.location.href = '{{route('designer.regComplete')}}?email=' + encodeEmail;
                                            location.reload();
                                        }
                                        else {
                                            var a = (result.message).split(',');
                                            if (a.length == 1) {
                                                swal(a[0], '', 'error');
                                            } else {
                                                swal("Email and Phone Number already exist", '', 'error');
                                            }

                                            $('#register').val("Register Me").removeAttr("disabled", "disabled");
                                            $('.l-image').hide();
                                        }
                                    },
                                    error: function (e) {
                                        swal(e, '', 'error');
                                        $('#register').val("Register Me").removeAttr("disabled", "disabled");
                                        $('.l-image').hide();
                                    },
                                });
                            } else {
                                $('#terms').notify('You have to agree to our terms and condition before signing up', 'error');
                                $('#register').val("Register Me").removeAttr("disabled", "disabled");
                                $('.l-image').hide();
                            }
                        } else {
                            $('#emailReg').notify('Please enter a valid email address', 'error');
                            $('#register').val("Authenicate Me").attr("disabled", false);
                            $('.l-image').hide();
                        }

                    } else {
                        $('#phoneNo').notify('Please enter a valid phone number', 'error', {
                            position: 'left'
                        });
                        $('#register').val("Register Me").removeAttr("disabled", "disabled");
                        $('.l-image').hide();
                    }
                } else {
                    $('#register').val("Register Me").removeAttr("disabled", "disabled");
                    $('.l-image').hide();
                    $('#confpasswordReg, #passwordReg').notify('Password does not match','error',{
                        position: 'left'
                    });
                }
            } else {
                swal('Please fill all fields completely before submitting', '', 'error');
                $('#register').val("Register Me").removeAttr("disabled", "disabled");
                $('.l-image').hide();
            }
        });

        let email = window.location.hash;
        email = '{{request('email')}}';
        let plainEmail = atob(email);

        let message = '';

        $('#activateMe').on('click', function () {
            $('#activateMe').val("Please wait...").attr("disabled", "disabled");
            $('.l-image').show();

            if (isNotEmpty('.actCode')) {
                let actCode = $('#activateCode').val();
                if (isNumeric(actCode) && actCode > 5) {
                    /* let data = {
                         'email': plainEmail,
                         'token': actCode
                     }*/
                    let data = {
                        'email': plainEmail,
                        'token': actCode
                    }
                    //https://wawoohapi.herokuapp.com/fashion/validatetoken
                    console.log(data);
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        url: "{{route('verify.token')}}",
                        success: function (result) {
                            console.log(result);
                            if (result.status === '00') {
                                window.location.href = '{{route('designer.dashboard')}}';
                            } else if (result.status === '56') {
                                swal("Token verification failed, please click on the resend button", "", "warning")

                                $('#activateMe').val("Validate").removeAttr("disabled", "disabled");
                            }

                        },
                        error: function (e) {
                            // swal(e, '', 'error');
                            console.log(e);
                            //window.location.href="www.google.com";
                            //$('#activateMe').val("Validate").removeAttr("disabled", "disabled");
                        }
                    })
                } else {
                    $('#activateMe').val("Validate").removeAttr("disabled", "disabled");
                    swal('Must be numbers only', '', 'warning');
                }
            } else {
                $('#activateMe').val("Validate").removeAttr("disabled", "disabled");
                swal('Please, enter the token', '', 'warning');
            }
        });

        $('#resendCode').on('click', function (e) {
            //$('#activateMe').val("Please wait...").attr("disabled", "disabled");
            $('#codeAction').hide();
            $('.l-image').show();
            e.preventDefault();
            let data = {
                "email": plainEmail
            };
            // console.log(plainEmail);
            //$('#loaderModal').modal('show');
            $.ajax({
                url: '{{env('GET_BASE_URL')}}/fashion/resendtoken',
                method: "POST",
                dataType: "json",
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                data: JSON.stringify(data),
                success: function (res) {
                    $('.l-image').hide();
                    // console.log(res);
                    if (res.status === '00') {
                        $('#resMes').html('<span class="alert alert-success">Token has been resent</span>');
                        //swal('Code Successfully Sent', '', 'success');
                        {{--                        //window.location.href = '{{route('designersLandingPage')}}'--}}
                    }
                    else if (res.status === '99') {
                        $('#resMes').html('<span class="alert alert-warning">User not existing</span>');
                    }
                },
                error: function (e) {
                    //console.log('can\'t connect' + e);
                }
            })
        });

        $('.retrive-email').click(function () {
            let data = {
                phoneNo: $('#emailReset').val()
            };
            $.ajax({
                url: "{{env('GET_BASE_URL')}}/fashion/forgotemail",
                type: "POST",
                dataType: "json",
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                data: JSON.stringify(data),
                success: function (response) {
                    swal(response.message, '', 'success');
                },
                error: function (error) {
                    console.log("error" + JSON.stringify(error));
                }
            })
        });
    });
</script>