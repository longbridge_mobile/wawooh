<script>
    $('#cancel').on('click', function () {
        location.reload();
    });

    var activeDiscount = 'price';

    var sizes = [];
    var selectedSizes = [];
    var prodType = 1;
    var attrcun = 0;

    $.ajax({
        url: '{{getBaseUrl()}}/fashion/size/getsizes',
        type: 'GET',
        success: function (res) {
            if (res.status = '00') {
                tempSizes = res.data.data;
                $.each(tempSizes, function () {
                    sizes.push(this.name.toUpperCase());
                })
            }
        }
    });

    $.each(sizes.slice(0, 4), function () {
        $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
    });
    sizeClickListener();
    deleteClickListener();

    var keyPressed = false;

    $("#searchsizeinput").keydown(function () {
        keyPressed = true;
    });

    $(document).on('keydown', '#searchsizeinputs', function () {
        keyPressed = true
    });

    $('.imgcass').hover(function(){
        $(this).siblings('div').toggleClass('hide');
    });


    $(document).on('mouseenter', '.imgcassmore', function(){
        $(this).siblings('div').removeClass('hide');
    });

    $(document).on('mouseleave', '.imgcassmore', function(){
        $(this).siblings('div').addClass('hide');
    });


    $(document).on('mouseenter', '.sideMoreFabricAction, .backMoreFabricAction, .frontMoreFabricAction, .topMoreFabricAction, .mainMoreFabricAction', function(){
        $(this).removeClass('hide');
    })

    $(document).on('mouseleave', '.sideMoreFabricAction, .backMoreFabricAction, .frontMoreFabricAction, .topMoreFabricAction, .mainMoreFabricAction', function(){
        $(this).addClass('hide');
    })





    $("#searchsizeinput").focus(function () {
        console.log('lol');
        searchRes();
    });

    $(document).on('focus', '#searchsizeinputs', function () {
        console.log('oooo');
        searchRess();
    });

    $("#searchsizeinput").keyup(function () {
        if (keyPressed) {
            searchRes();
            keyPressed = false;
        }
    });

    $('#searchsizeinputs').keyup(function () {
        if (keyPressed) {
            searchRess();
            keyPressed = false;
        }
    });

    function searchRess() {
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($("#searchsizeinputs").val().toUpperCase()) && !selectedSizes.includes(a));
        });

        $("#searchress").html('');
        $.each(currarr.slice(0, 4), function () {
            $("#searchress").append('<p class="sizesearchres" >' + this + '</p>');
        });

        sizeClickListeners();

    }

    $(document).on('focus', '.morefabricsize', function () {
        console.log('this is a focus');
        $(this).siblings('.morefabricsearch').show();
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($(".morefabricsize").val().toUpperCase()) && !selectedSizes.includes(a));
        });
        $(this).html('');
        $.each(currarr.slice(0, 4), function () {
            $(".morefabricsearch").append('<p class="sizesearchres" >' + this + '</p>');
            morefabricClickListeners();
        })
    });

    $('#fabricsize').focus(function () {
        $('#fabricsearch').show();
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($("#fabricsize").val().toUpperCase()) && !selectedSizes.includes(a));
        });
        console.log('2');
        $("#fabricsearch").html('');
        $.each(currarr.slice(0, 4), function () {
            $("#fabricsearch").append('<p class="sizesearchres" >' + this + '</p>');
            fabricClickListeners();
        })
    });

    function morefabricClickListeners() {
        $('.sizesearchres').click(function () {
            var size = $(this).html();
            selectedSizes.push(size.toUpperCase());
            $(this).closest('div').siblings('.morefabricsize').val(size);
            $(this).closest('div').toggle();
        });
    }

    function fabricClickListeners() {
        $('.sizesearchres').click(function () {
            //console.log($(this).html());
            var size = $(this).html();
            selectedSizes.push(size.toUpperCase());
            $('#fabricsize').val(size);
            $('#fabricsearch').toggle();
        });
    }

    function searchRes() {
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($("#searchsizeinput").val().toUpperCase()) && !selectedSizes.includes(a));
        });

        $("#searchres").html('');
        $.each(currarr.slice(0, 4), function () {
            $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
        });
        sizeClickListener();
        deleteClickListener();
    }

    function deleteClickListeners() {
        $(".delete-size").click(function () {
            parent = $(this).closest('.size');
            pinb = $(parent).children('.input-group')
            delSize = pinb.children('.sizeinput').val();
            parent.remove();

            newSelectedSize = $.grep(selectedSizes, function (a) {
                return (a !== delSize);
            });

            selectedSizes = newSelectedSize;
        });
    }

    function deleteClickListener() {
        $(".delete-size").click(function () {
            parent = $(this).closest('.size');
            pinb = $(parent).children('.input-group')
            delSize = pinb.children('.sizeinput').val();
            parent.remove();

            newSelectedSize = $.grep(selectedSizes, function (a) {
                return (a !== delSize);
            });

            selectedSizes = newSelectedSize;
        });
    }

    function sizeClickListeners() {
        $(".sizesearchres").click(function () {
            console.log('testing');
            var size = $(this).html();
            selectedSizes.push(size.toUpperCase());

            $("#sizess").append('<div class="size" >\n' +
                '                                    <div class="input-group" >\n' +
                '                                        <div class="input-group-prepend" >\n' +
                '                                            <i class="fa fa-times mr-2 delete-size text-danger" ></i>\n' +
                '                                        </div>\n' +
                '                                        <input class="form-control sizeinput" size="1" type="text" readonly value="' + size + '" >\n' +
                '                                        <input class="form-control stock" size="1" type="number" placeholder="Stock" >\n' +
                '                                    </div>\n' +
                '                                </div>');

            $("#searchress").html("");
            $("#searchsizeinputs").val("");
            deleteClickListeners();
        });
    }

    function sizeClickListener() {
        $(".sizesearchres").click(function () {
            var size = $(this).html();
            selectedSizes.push(size.toUpperCase());

            $("#sizes").append('<div class="size" >\n' +
                '                                    <div class="input-group" >\n' +
                '                                        <div class="input-group-prepend" >\n' +
                '                                            <i class="fa fa-times mr-2 delete-size text-danger" ></i>\n' +
                '                                        </div>\n' +
                '                                        <input class="form-control sizeinput" type="text" readonly value="' + size + '" >\n' +
                '                                        <input class="form-control stock" type="number" placeholder="Stock" >\n' +
                '                                    </div>\n' +
                '                                </div>');

            $("#searchres").html("");
            $("#searchsizeinput").val("");
            deleteClickListener();
        });
    }

    $(document).on('click', '#selectPercentage', function () {
        $('.discountGroup').toggle();
        $('.percentGroup').toggle();
        $(this).toggle();
        $('#selectDiscount').toggle();
        activeDiscount = 'percentage';
    });

    $(document).on('click', '#selectDiscount', function () {
        $('.discountGroup').toggle();
        $('.percentGroup').toggle();
        $(this).toggle();
        $('#selectPercentage').show();
        activeDiscount = 'price';
    });

    myMeasure = sellect("#my-measurement", {
        originList: ["neck",
            "armHole",
            "biceps",
            "longSleeve",
            "shortSleeve",
            "fullShoulder",
            "halfShoulder",
            "elbow",
            "fullChest",
            "bust",
            "overBreast",
            "underChest",
            "underBust",
            "shirtLength",
            "stomach",
            "wrist",
            "fullLength",
            "trouserWaist",
            "seat",
            "outSeam",
            "inSeam",
            "crotch",
            "thigh",
            "knee",
            "ankle",
            "butt",],
        destinationList: []
    });

    myMeasure.init();

    $(document).ready(function () {


        $('#addFabricType').click(function () {
            $(this).hide();
            $('#addMoreFabricType').removeClass('hide');
            $('#showFabricType').show();
        });

        var cun = 0;

        $(document).on('click', '#addMoreFabricType', function () {
            attrcun++;

            if (attrcun === 4) {
                $('#addMoreFabricType').attr('disabled', true);
            }

            $('#showMoreContainer').append(`
               <div class="realMoreFind-${attrcun}">
                <div class="row tudmore">
                    <div class="col-md-9">
                        <label for='mainMoreFabricType-${attrcun}' class="proImg">
                            <p>Main</p>
                            <img class="mainMoreFabric imgcassmore next-${attrcun}" src="{{ asset('img/material1.jpg')}}">
                            <input multiple type='file' id='mainMoreFabricType-${attrcun}' class='hide-input mainMoreFabricType' num="${attrcun}"/>
                            <div class="mainMoreFabricAction hide"></div>
                        </label>

                        <label for='sideMoreFabricType-${attrcun}' class="proImg">
                            <p>Side</p>
                            <img class="sideMoreFabric imgcassmore next-${attrcun}" src="{{ asset('img/material1.jpg')}}">
                            <input multiple type='file' id='sideMoreFabricType-${attrcun}' class='hide-input sideMoreFabricType'/>
                            <div class="sideMoreFabricAction hide"></div>
                        </label>

                        <label for='backMoreFabricType-${attrcun}' class="proImg">
                            <p>Back</p>
                            <img class="backMoreFabric imgcassmore next-${attrcun}" src="{{ asset('img/material1.jpg')}}">
                            <input multiple type='file' id='backMoreFabricType-${attrcun}' class='hide-input backMoreFabricType'/>
                            <div class="backMoreFabricAction hide"></div>
                        </label>

                        <label for='frontMoreFabricType-${attrcun}' class="proImg">
                            <p>Front</p>
                            <img class="frontMoreFabric imgcassmore next-${attrcun}" src="{{ asset('img/material1.jpg')}}">
                            <input multiple type='file' id='frontMoreFabricType-${attrcun}' class='hide-input frontMoreFabricType'/>
                            <div class="frontMoreFabricAction hide"></div>
                        </label>

                        <label for='topMoreFabricType-${attrcun}' class="proImg">
                            <p>Top</p>
                            <img class="topMoreFabric imgcassmore next-${attrcun}" src="{{ asset('img/material1.jpg')}}">
                            <input multiple type='file' id='topMoreFabricType-${attrcun}' class='hide-input topMoreFabricType'/>
                            <div class="topMoreFabricAction hide"></div>
                        </label>
                    </div>

                    <div class="col-md-3 measureCorner">
                        Measurement
                        <div class="measureMoreAttachment">
                        </div>
                        <div>
                        </div>
                    </div>
                </div>

                <div class="addmoremain" style="margin-top:47px;">
                    <div class="row mains">
                        <div class="col-3 col-sm-3 col-md-3 moreone">
                            <label for="morefabricsize"> Select Sizes</label>
                            <input type="text" size="6" id="morefabricsize" class="form-control morefabricsize">
                            <div class="morefabricsearch"></div>
                        </div>
                        <div class="col-4 col-sm-4 col-md-4 moretwo">
                            <label for="morefabricstock">Stock No</label>
                            <input type="number" size="4" id="morefabricstock" class="form-control morefabricstock">
                        </div>
                        <div class="morethree" style="padding-top: 38px;">
                            <i class="fa fa-plus addMoreFabricMeasurement"></i>
                        </div>

                        <div class = "col-4 col-sm-4 col-md-4">
                        <label for = "colorMaterialName">Material Color:</label>
                            <span class="productColor"><img src="{{ asset('img/material1.jpg')}}" class="displayMat matCol-${attrcun}" height="40" width="40"></span>
                            <input class="form-control colorMaterialName-${attrcun}" type="text">
                        </div>
                    </div>
                </div>
            </div>
            </div>
               `)
        });


        $(document).on('change', '.mainMoreFabricType', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $(this).siblings('.mainMoreFabricAction').html(`
                       <span><i class="pull-right fa fa-times restoreMoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-check mainMoreCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMoreMainCrop"></i></span>
                        `);
                    convertMoreToBase64(this, '.mainMoreFabric');
                    $(this).siblings('img').addClass('imgmore');
                }
            }
        });

        $(document).on('click', '.restoreMoreMainImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.mainMoreFabricAction').siblings('input').prop('disabled', false);
            that.closest('.mainMoreFabricAction').removeClass('yip');
            that.closest('.mainMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.mainMoreFabricAction').siblings('.mainMoreFabric').removeClass('cropper-hidden');
        });

        $(document).on('click', '.restoreMoreSideImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.sideMoreFabricAction').siblings('input').prop('disabled', false);
            that.closest('.sideMoreFabricAction').removeClass('yip');
            that.closest('.sideMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.sideMoreFabricAction').siblings('.sideMoreFabric').removeClass('cropper-hidden');
        });

        $(document).on('click', '.restoreMoreTopImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.topMoreFabricAction').siblings('input').prop('disabled', false);
            that.closest('.topMoreFabricAction').removeClass('yip');
            that.closest('.topMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.topMoreFabricAction').siblings('.topMoreFabric').removeClass('cropper-hidden');

        });

        $(document).on('click', '.restoreMoreBackImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.backMoreFabricAction').siblings('input').prop('disabled', false);
            that.closest('.backMoreFabricAction').removeClass('yip');
            that.closest('.backMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.backMoreFabricAction').siblings('.backMoreFabric').removeClass('cropper-hidden');
        });

        $(document).on('click', '.restoreMoreFrontImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.frontMoreFabricAction').siblings('input').prop('disabled', false);
            that.closest('.frontMoreFabricAction').removeClass('yip');
            that.closest('.frontMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.frontMoreFabricAction').siblings('.frontMoreFabric').removeClass('cropper-hidden');

        });


        $(document).on('change', '.topMoreFabricType', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $(this).siblings('.topMoreFabricAction').html(`
                        <span><i class="pull-right fa fa-times restoreMoreTopImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-check topMoreCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMoreTopCrop"></i></span>
                        `);
                    convertMoreToBase64(this, '.topMoreFabric');
                    $(this).siblings('img').addClass('imgmore');
                }
            }
        });


        //back image
        $(document).on('change', '.backMoreFabricType', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $(this).siblings('.backMoreFabricAction').html(`
                     <span><i class="pull-right fa fa-times restoreMoreBackImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-check backMoreCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMoreBackCrop"></i></span>
                        `);
                    convertMoreToBase64(this, '.backMoreFabric');
                    $(this).siblings('img').addClass('imgmore');
                }
            }
        });

        //side image
        $(document).on('change', '.sideMoreFabricType', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $(this).siblings('.sideMoreFabricAction').html(`
                      <span><i class="pull-right fa fa-times restoreMoreSideImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-check sideMoreCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMoreSideCrop"></i></span>
                        `);
                    convertMoreToBase64(this, '.sideMoreFabric');
                    $(this).siblings('img').addClass('imgmore');
                }
            }
        });


        //front image
        $(document).on('change', '.frontMoreFabricType', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $(this).siblings('.frontMoreFabricAction').html(`
                      <span><i class="pull-right fa fa-times restoreMoreFrontImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-check frontMoreCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMoreFrontCrop"></i></span>
                        `);
                    convertMoreToBase64(this, '.frontMoreFabric');
                    $(this).siblings('img').addClass('imgmore');
                }
            }
        });


        $('#mainFabricType').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $('#mainFabricAction').html(`
                    <span><i class="pull-right fa fa-times restoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check mainCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMainCrop"></i></span>
                        `)
                    convertToBase64(this, '#mainFabric');
                    $(this).siblings('img').addClass('add-img');
                }
            }
        });

        $(document).on('click', '.restoreMainImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('#mainFabricAction').siblings('input').prop('disabled', false);
            //that.closest('#mainFabricAction').siblings('#mainFabricType').attr('disabled', false);
            that.closest('#mainFabricAction').siblings('.cropper-container').toggle();
            that.closest('#mainFabricAction').removeClass('yip');
            that.closest('#mainFabricAction').siblings('#mainFabric').removeClass('cropper-hidden');

        });

        $(document).on('click', '.restoreSideImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('#sideFabricAction').siblings('input').prop('disabled', false);
            that.closest('#sideFabricAction').removeClass('yip');

            that.closest('#sideFabricAction').siblings('.cropper-container').toggle();
            that.closest('#sideFabricAction').siblings('#sideFabric').removeClass('cropper-hidden');

        });

        $(document).on('click', '.restoreFrontImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('#frontFabricAction').siblings('input').prop('disabled', false);
            that.closest('#frontFabricAction').removeClass('yip');
            that.closest('#frontFabricAction').siblings('.cropper-container').toggle();
            that.closest('#frontFabricAction').siblings('#frontFabric').removeClass('cropper-hidden');
        });


        $(document).on('click', '.restoreBackImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('#backFabricAction').removeClass('yip');
            that.closest('#backFabricAction').siblings('input').prop('disabled', false);
            that.closest('#backFabricAction').siblings('.cropper-container').toggle();
            that.closest('#backFabricAction').siblings('#backFabric').removeClass('cropper-hidden');
        });

        $(document).on('click', '.restoreTopImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('#topFabricAction').removeClass('yip');
            that.closest('#topFabricAction').siblings('input').prop('disabled', false);
            that.closest('#topFabricAction').siblings('.cropper-container').toggle();
            that.closest('#topFabricAction').siblings('#topFabric').removeClass('cropper-hidden');
        });


        $(document).on('click', '.changeMainImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('div').siblings('#mainFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('div').siblings('#sideFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('div').siblings('#backFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('div').siblings('#frontFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('div').siblings('#topFabric').attr('src', '{{ asset('img/material1.jpg')}}');


            // $('#mainFabric').attr('src', '{{ asset('img/material1.jpg')}}');
        });


        $(document).on('click', '.changeMoreMainImage', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.mainMoreFabricAction').siblings('.mainMoreFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('.sideMoreFabricAction').siblings('.sideMoreFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('.topMoreFabricAction').siblings('.topMoreFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('.backMoreFabricAction').siblings('.backMoreFabric').attr('src', '{{ asset('img/material1.jpg')}}');
            that.closest('.frontMoreFabricAction').siblings('.frontMoreFabric').attr('src', '{{ asset('img/material1.jpg')}}');

        });

        $(document).on('click', '.frontMoreCrop', function (e) {
            e.preventDefault();
            var cropImage = $(this).closest('.frontMoreFabricAction').siblings('.frontMoreFabric').cropper('getCroppedCanvas').toDataURL();
            $(this).closest('.tudmore').siblings('.addmoremain').find('.displayMat').attr('src', cropImage);
        });


        $(document).on('click', '.sideMoreCrop', function (e) {
            e.preventDefault();
            var cropImage = $(this).closest('.sideMoreFabricAction').siblings('.sideMoreFabric').cropper('getCroppedCanvas').toDataURL();
            $(this).closest('.tudmore').siblings('.addmoremain').find('.displayMat').attr('src', cropImage);
        });

        $(document).on('click', '.backMoreCrop', function (e) {
            e.preventDefault();
            var cropImage = $(this).closest('.backMoreFabricAction').siblings('.backMoreFabric').cropper('getCroppedCanvas').toDataURL();
            $(this).closest('.tudmore').siblings('.addmoremain').find('.displayMat').attr('src', cropImage);
        });

        $(document).on('click', '.topMoreCrop', function (e) {
            e.preventDefault();
            var cropImage = $(this).closest('.topMoreFabricAction').siblings('.topMoreFabric').cropper('getCroppedCanvas').toDataURL();
            $(this).closest('.tudmore').siblings('.addmoremain').find('.displayMat').attr('src', cropImage);
        });


        $('#mainFabricAction, .cropper-modal, #sideFabricAction, #backFabricAction, #frontFabricAction, #topFabricAction, .sideMoreFabricAction, .mainMoreFabricAction, .frontMoreFabricAction, .backMoreFabricAction, .topMoreFabricAction').hover(function(){
            $(this).toggleClass('hide');
        });

        // $(".cropper-container").hover(function(){
        //   console.log('hellonworld');
        // $(this).siblings('div').toggleClass('hide');
        //})



        $(document).on('click', '.mainMoreCrop', function (e) {
            e.preventDefault();
            var cropImage = $(this).closest('.mainMoreFabricAction').siblings('.mainMoreFabric').cropper('getCroppedCanvas').toDataURL();
            $(this).closest('.tudmore').siblings('.addmoremain').find('.displayMat').attr('src', cropImage);
        });

        $(document).on('click', '.mainCrop', function (e) {
            e.preventDefault();
            var cropImage = $('#mainFabric').cropper('getCroppedCanvas').toDataURL();
            $('.displayMat').attr('src', cropImage);

        });

        $(document).on('click', '.addMoreMainCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.mainMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.mainMoreFabricAction').siblings('.mainMoreFabric').toggleClass('cropper-hidden');
            var $image = $(this).closest('.mainMoreFabricAction').siblings('.mainMoreFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            that.closest('.mainMoreFabricAction').addClass('yip');
            that.closest('.mainMoreFabricAction').siblings('input').prop('disabled', true);

        });


        $(document).on('click', '.addMainCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('#mainFabricAction').siblings('.cropper-container').toggle();
            that.closest('#mainFabricAction').siblings('#mainFabric').toggleClass('cropper-hidden');
            var $image = $('#mainFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            $(this).closest('#mainFabricAction').addClass('yip');
            $(this).closest('#mainFabricAction').siblings('input').prop('disabled', true);

        });

        $(document).on('click', '.addMoreFrontCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.frontMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.frontMoreFabricAction').siblings('.frontMoreFabric').toggleClass('cropper-hidden');
            var $image = $(this).closest('.frontMoreFabricAction').siblings('.frontMoreFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            that.closest('.frontMoreFabricAction').addClass('yip');
            that.closest('.frontMoreFabricAction').siblings('input').prop('disabled', true);
        });


        $(document).on('click', '.addMoreSideCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.sideMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.sideMoreFabricAction').siblings('.sideMoreFabric').toggleClass('cropper-hidden');
            var $image = $(this).closest('.sideMoreFabricAction').siblings('.sideMoreFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            that.closest('.sideMoreFabricAction').addClass('yip');
            that.closest('.sideMoreFabricAction').siblings('input').prop('disabled', true);
        });

        $(document).on('click', '.addMoreBackCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.backMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.backMoreFabricAction').siblings('.backMoreFabric').toggleClass('cropper-hidden');
            var $image = $(this).closest('.backMoreFabricAction').siblings('.backMoreFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            that.closest('.backMoreFabricAction').addClass('yip');
            that.closest('.backMoreFabricAction').siblings('input').prop('disabled', true);

        });

        $(document).on('click', '.addMoreTopCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.topMoreFabricAction').siblings('.cropper-container').toggle();
            that.closest('.topMoreFabricAction').siblings('.topMoreFabric').toggleClass('cropper-hidden');
            var $image = $(this).closest('.topMoreFabricAction').siblings('.topMoreFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            that.closest('.topMoreFabricAction').addClass('yip');
            that.closest('.topMoreFabricAction').siblings('input').prop('disabled', true);
        });


        $('#sideFabricType').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                console.log($(this).get(0).files[i].size);
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $('#sideFabricAction').html(`
                         <span><i class="pull-right fa fa-times restoreSideImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check sideCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addSideCrop"></i></span>
                        `);
                    convertToBase64(this, '#sideFabric');
                    $(this).siblings('img').addClass('add-img');
                }
            }
        });

        $(document).on('click', '.addSideCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            var $image = $('#sideFabric');
            that.closest('#sideFabricAction').siblings('.cropper-container').toggle();
            that.closest('#sideFabricAction').siblings('#sideFabric').toggleClass('cropper-hidden');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            $(this).closest('#sideFabricAction').addClass('yip');
            $(this).closest('#sideFabricAction').siblings('input').prop('disabled', true);
        });

        $(document).on('click', '.sideCrop', function (e) {
            e.preventDefault();
            var cropImage = $('#sideFabric').cropper('getCroppedCanvas').toDataURL();
            $('.displayMat').attr('src', cropImage);
        });


        $('#backFabricType').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                console.log($(this).get(0).files[i].size);
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $('#backFabricAction').html(`
                   <span><i class="pull-right fa fa-times restoreBackImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check backCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addBackCrop"></i></span>
                        `);
                    convertToBase64(this, '#backFabric');
                    $(this).siblings('img').addClass('add-img');
                }
            }
        });

        $(document).on('click', '.addBackCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            var $image = $('#backFabric');
            that.closest('#backFabricAction').siblings('.cropper-container').toggle();
            that.closest('#backFabricAction').siblings('#backFabric').toggleClass('cropper-hidden');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            $(this).closest('#backFabricAction').addClass('yip');
            $(this).closest('#backFabricAction').siblings('input').prop('disabled', true);
        });

        $(document).on('click', '.backCrop', function (e) {
            e.preventDefault();
            var cropImage = $('#backFabric').cropper('getCroppedCanvas').toDataURL();
            $('.displayMat').attr('src', cropImage);
        });


        $('#frontFabricType').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                console.log($(this).get(0).files[i].size);
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $('#frontFabricAction').html(`
                  <span><i class="pull-right fa fa-times restoreFrontImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check frontCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addFrontCrop"></i></span>
                        `);
                    convertToBase64(this, '#frontFabric');
                    $(this).siblings('img').addClass('add-img');
                }
            }
        });

        $(document).on('click', '.addFrontCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            var $image = $('#frontFabric');
            that.closest('#frontFabricAction').siblings('.cropper-container').toggle();
            that.closest('#frontFabricAction').siblings('#frontFabric').toggleClass('cropper-hidden');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            $(this).closest('#frontFabricAction').addClass('yip');
            $(this).closest('#frontFabricAction').siblings('input').prop('disabled', true);
        });

        $(document).on('click', '.frontCrop', function (e) {
            e.preventDefault();
            var cropImage = $('#frontFabric').cropper('getCroppedCanvas').toDataURL();
            $('.displayMat').attr('src', cropImage);
        });


        $('#topFabricType').on('change', function () {
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                console.log($(this).get(0).files[i].size);
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 500) {
                    $.notify("Cannot upload image size more than 500kb");
                } else {
                    $('#topFabricAction').html(`
              <span><i class="pull-right fa fa-times restoreTopImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check topCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addTopCrop"></i></span>
                        `);
                    convertToBase64(this, '#topFabric');
                    $(this).siblings('img').addClass('add-img');
                }
            }
        });

        $(document).on('click', '.addTopCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            var $image = $('#topFabric');
            that.closest('#topFabricAction').siblings('.cropper-container').toggle();
            that.closest('#topFabricAction').siblings('#topFabric').toggleClass('cropper-hidden');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false

            });
            $(this).closest('#topFabricAction').addClass('yip');
            $(this).closest('#topFabricAction').siblings('input').prop('disabled', true);
        });

        $(document).on('click', '.topCrop', function (e) {
            e.preventDefault();
            var cropImage = $('#topFabric').cropper('getCroppedCanvas').toDataURL();
            $('.displayMat').attr('src', cropImage);
        });


        $('.addFabricMeasurement').click(function () {
            var pix = [];
            var data = {
                measurement: $('#fabricsize').val(),
                stock: $('#fabricstock').val()
            };
            pix.push(data);
            displayMeasureSize(pix);
            $('#fabricsize').val('');
            $('#fabricstock').val('');
        });

        $(document).on('click', '.addMoreFabricMeasurement', function () {
            var pix = [];
            var data = {
                moreMeasurement: $(this).closest('.morethree').siblings('.moreone').find('.morefabricsize').val(),
                moreStock: $(this).closest('.morethree').siblings('.moretwo').find('.morefabricstock').val()
            }
            pix.push(data);
            displayMoreMeasureSize(pix, this);
            $(this).closest('.morethree').siblings('.moreone').find('.morefabricsize').val('')
            $(this).closest('.morethree').siblings('.moretwo').find('.morefabricstock').val('')
        });


        $(document).on('click', '.removeAttachedMeasurement', function () {
            var that = $(this);
            that.closest('.removeItems').remove();
            console.log(that);
        });

        $(document).on('click', '.removeMoreAttachedMeasurement', function () {
            var that = $(this);
            that.closest('.moreRemoveItems').remove();
            console.log(that);
        });

        function displayMoreMeasureSize(arr, that) {
            if (arr.length > 0) {
                for (var i = 0; i < arr.length; i++) {
                    $(that).closest('.addmoremain').siblings('.tudmore').find('.measureMoreAttachment').append(`
                       <div class="row moreRemoveItems moreNItem-${i}">
                         <div class="col-sm-5">
                        <div class="moreSMeasure">${arr[i].moreMeasurement}</div>
                    </div>
                        <div class="col-sm-5">
                            <div class="moreSStock">${arr[i].moreStock}</div>
                        </div>
                        <div class="col-sm-2">
                           <i class="fa fa-trash removeMoreAttachedMeasurement"></i>
                          </div>
                          </div>
                      `);
                }
            }
        }

        function displayMeasureSize(arr) {
            if (arr.length > 0) {
                for (var i = 0; i < arr.length; i++) {
                    $('#measureAttachment').append(`
                        <div class="row removeItems" id="removeItems">
                         <div class="col-sm-5">
                        <div class="sMeasure">${arr[i].measurement}</div>
                    </div>
                        <div class="col-sm-5">
                            <div class="sStock">${arr[i].stock}</div>
                        </div>
                        <div class="col-sm-2">
                           <i class="fa fa-trash removeAttachedMeasurement"></i>
                          </div>
                          </div>
                        `);
                }
            }
        }


        counter = 0;
        $('.colorPicker').minicolors();

        $.initialize('.colorPicker', function () {
            $(this).minicolors();
        });
        var styles = [];
        tagId = 0;
        $('#productGroup').on('change', function () {
            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            <?php else: ?>
                tok = '';
                <?php endif; ?>
            var id = $(this).val();
            styles = [];
            var data = {
                categoryId: id,
                productType: prodType
            }

            //console.log(data);

            $.ajax({
                url: '{{getBaseUrl()}}/fashion/product/getsubcatbyproducttype',
                /*url: `/subcategory?id=${id}`,*/
                type: "POST",
                dataType: "json",
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                data: JSON.stringify(data),
                success: function (result) {
                    var result = result.data;
                    //console.log(result.data);
                    var html = `<option value='' >Select</option>`;
                    // console.log(result);
                    if (result.length) {
                        $('#subcat').removeAttr('disabled');
                        for (i = 0; i < result.length; i++) {
                            html += `<option value='${result[i].id}'>${result[i].subCategory}</option>`;
                        }
                        $('#subcat').html(html);
                    } else {
                        $.notify('error', 'error');
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('#subcat').on('change', function () {
            var id = $(this).val();
            styles = [];

            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>

            $.ajax({
                url: `{{getBaseUrl()}}/fashion/product/${id}/getstyles`,
                /*url: `/styles?subCatId=${id}`,*/
                type: "GET",
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                success: function (result) {
                    //console.log(result.data);
                    var result = result.data;
                    $('#style').prop('disabled', false);
                    var html = `<option value=''>Select</option>`;
                    // console.log(result);
                    if (result.length) {
                        if (result instanceof Array) {
                            $('#style').removeAttr('disabled');

                            for (i = 0; i < result.length; i++) {
                                html += `<option value='${result[i].id}' >${result[i].style}</option>`;
                            }
                        } else {
                            $.notify('error', 'error');
                        }

                    } else {
                        html += `<option value=''>No style</option>`;
                    }


                    $('#style').html(html);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('body').on('click', function () {
            $('#styleHolder').hide();
        });
        $(document).on('click', '.rem', function () {
            var target = $(this).attr('tag');
            $(target).remove();
        });


        $('.edit-plus').on('click', function () {
            var html = `<input type="hidden"  class="colorPicker" value="#ff6161">`;

            $(this).before(html);
        });

    });

    $('.removeArt').on('click', function () {
        //alert('remove');
        $(this).parent().remove();
    });

    $('#artwork1').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                $('#del').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                convertToBase64(this, '#img1');
            }
        }

    });

    $('#artwork2').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                $('#del2').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                convertToBase64(this, '#img2');
            }
        }
    });

    $('#artwork3').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                $('#del3').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                convertToBase64(this, '#img3');
            }
        }
    });

    $('#artwork4').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                $('#del4').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                convertToBase64(this, '#img4');
            }
        }
    });

    var counters = 0;
    $('#addMoreImage').on('click', function () {

        $('#addMoreImagesCol').append(`

           <div class="addedMoreImage" counter=${counters++}><i class="fa fa-times removeMoreImage"></i>
            <label for='productImgs' class="proImg">
                        <img src="{{ asset('img/landscape.png')}}">
                        <input multiple type='file' id='productImgs' class='hide-input productImgs'/>
                        <div class="moreAddedImmages"></div>
                    </label>
                    </div>
           `)
    });


    $(document).on('click', '.removeMoreImage', function () {
        $(this).closest('div').remove();
    });

    $('#artwork5').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                $('#del5').html('<i class="pull-right fa fa-trash removeIMage"></i>');
                convertToBase64(this, '#img5');
            }
        }
    });

    $('#mat1').on('change', async function () {
        var pix = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 256) {
                $.notify("Cannot upload image size more than 256kb")
            } else {
                pix.push(await toBase64($(this).get(0).files[i]));
                $('#materialPrice').show();
            }
        }
        showMaterials(pix);
    });


    var moreSizes;
    $(document).on('change', '.productImgs', function () {
        var pix = [];
        var that = $(this);
        for (var i = 0; i < $(this).get(0).files.length; i++) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                pix.push(toBase64($(this).get(0).files[i]));
                /*  setTimeout(function(){
                      $('.pImg').each(function(j, obj){
                      });
                      for(var j = 0; j < $('.pImg').length; j++){
                          moreSizes = new Croppie($('.pImg')[j], {
                              viewport: { width: 50, height: 50 },
                              boundary: { width: 100, height: 100 },
                              showZoomer: false,
                              enableResize: true,
                              enableOrientation: true
                          });

                      }
                      $('.croppie-container .croppie-container').css('opacity', 0);
                  }, 500);*/
            }
        }

        /*for(var i = 0; i < $('.addedMoreImage').length; i++){
            console.log(i);
        }*/

        showMoreProductImages(pix, that);
        addDelListener();

    });


    var resizes;
    var counter = 0;

    $('.productImg').on('change', async function () {

        if ($('#instockopts').prop('checked')) {
            $('.moreImageDiv').html(`
                 <div id="sizess">
                            </div>
                            <div id="searchsizes" class="my-3">
                                <div class="form-group">
                                    <label for="searchsizeinputs">Select Sizes*</label>
                                    <input type="text" id="searchsizeinputs" class="form-control"
                                           placeholder="Search size" size="6">
                                    <div id="searchress">

                                    </div>
                                </div>
                            </div>
                            <div class="form-check" id="customsizes">
                                <div class="pretty p-icon p-square p-smooth">
                                    <input type="checkbox" name="acceptCustomSizes" class="form-check-input noLeft"
                                           id="customsizetoggles">
                                    <div class="state p-warning-o">
                                        <i class="icon fa fa-check"></i>
                                        <label class="form-check-label" for="exampleCheck2">Accept custom sizes</label>
                                    </div>
                                </div>
                            </div>
                `);
        }
        var pixs = [];

        for (var i = 0; i < $(this).get(0).files.length; ++i) {

            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                if ($('.pImg').length <= 4) {
                    pixs.push(await toBase64($(this).get(0).files[i]));
                    counter++;
                } else {
                    $.notify("You cannot upload more than 5 images", "", "warning");
                }
                /*
                                     setTimeout(function(){
                                        $('.pImg').each(function(j, obj){
                                        });
                                       for(var j = 0; j < $('.pImg').length; j++){
                                                resizes = new Croppie($('.pImg')[j], {
                                                    viewport: { width: 50, height: 50 },
                                                    boundary: { width: 100, height: 100 },
                                                    showZoomer: false,
                                                    enableResize: true,
                                                    enableOrientation: true
                                                });
                                                console.log(resizes);
                                                //resizes.reverse();

                                        }
                                        $('.croppie-container .croppie-container').css('opacity', 0);
                                     }, 500);*/
            }
        }
        $(this).val("");
        showProductImages(pixs);
        addDelListener();


    });

    $(document).on('click', '.makeMaterial', function () {
        var that = $(this);
        var cropImage = $('.pImg').cropper('getCroppedCanvas').toDataURL();
        $('.materialImagePicture').attr('src', cropImage);

    });


    $('#artworkImg').on('change', async function () {
        var pix = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {

            //console.log($(this).get(0).files[i].size /1024);
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb")
            } else {
                pix.push(await toBase64($(this).get(0).files[i]));
            }

        }
        $(this).val("");
        showArtworkmages(pix);
        addDelListener();
    });

    function addDelListener() {
        $(document).on('click', '.removeIMage', function () {
            var piximg = $(this).attr('pix');
            $(piximg).remove();
        });
    }

    function find(arr, string) {
        var result = [];

        for (i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().indexOf(string.toLowerCase()) > -1) {
                console.log("hey " + string);
                result.push(arr[i]);
            }
        }

        return result;
    }

    $(document).on('dblclick', '.minicolors', function () {
        $(this).remove();
    });

    function showMaterials(arr) {
        if (arr.length) {
            var material = '';
            for (i = 0; i < arr.length; i++) {
                // console.log(arr[i]);
                material += `<div id=${counter} style='margin-bottom:10px' class="col-md-2">
                  <div class="upload-mat">
                    <img class='materialImg' src="${arr[i]}">
                    <input type='text' class='form-control fabricName' id = 'fabricName' placeholder='Enter Fabric Name' /><i class='pull-right fa fa-trash removeIMage' pix='#${counter}'></i>
                  </div>
                </div>`;
                counter++;
            }

            $('.materialBox').append(material);
        }
    }

    function showProductImages(arr) {
        console.log($('.pImg').length);
        if (arr.length <= 5) {
            var productImage = '';
            for (i = 0; i < arr.length; i++) {
                productImage += `<label id=${counter} class="proImg">
                  <img class='pImg' src=${arr[i]}>
                  <img class="materialImage" src="" pix='#${counter}'>
                  <i class='pull-right fa fa-trash removeIMage' pix='#${counter}'></i>
                   <i class='pull-right fa fa-check makeMaterial' pix='#${counter}'></i>
               </label>
               `;
                counter++;

            }
            $('.arrange-proImg').append(productImage);

            var $image = $('.pImg');

            $image.cropper({
                aspectRatio: 16 / 9,
            });

        } else {
            $.notify("You cannot upload more than 5 images", "", "warning");
        }
    }

    function showMoreProductImages(arr, that) {
        if (arr.length) {
            var productImage = '';
            for (var i = 0; i < arr.length; i++) {
                productImage += `<label id=${counter} class="moreProImg">
                    <img class='morePImg' src=${arr[i]}>
                    <img class="materialImage" src="" pix='#${counter}'>
                  <i class='pull-right fa fa-trash removeIMage' pix='#${counter}'></i>
                   <i class='pull-right fa fa-check makeMaterial' pix='#${counter}'></i>
                   </label>
                    `;
                counter++
            }


        }
    }

    function showArtworkmages(arr) {
        if (arr.length) {
            let artworkImage = '';
            for (i = 0; i < arr.length; i++) {
                artworkImage += `<label id=${counter} class="proImg">
                  <img class='pImg' src=${arr[i]}>
                  <i class='pull-right fa fa-trash removeIMage' pix='#${counter}'></i>
               </label>`;
                counter++;
            }
            $('.arrange-artImg').append(artworkImage);
        }
    }

    $(document).on('click', '.removeIMage', function () {
        console.log('hello world');
        var pix = $(this).attr('pix');
        $(pix).remove();
        $('#materialPrice').hide();
    });

    $('[name=producttype]').click(function () {
        prodType = $(this).val();

        if (prodType == 1) {
            $("#material-div").show();
            $("#requiredmeasurement-div").show();
        } else if (prodType == 2 || prodType == 3) {
            $("#requiredmeasurement-div").hide();
            $("#material-div").hide();
        }
    });

    $('[name=availability]').on('change', function () {
        console.log('helllffdf');
        if ($('[name=availability]:checked').val().toLowerCase() == 'y') {
            $('#sizes').show();
            $("#searchsize").show();
            $('#customsize').show();
            if ($('#customsizetoggle:checked').length > 0) {
                $("#stock-time").show();
            }
            $('#time').hide();

        } else {
            $('#sizes').hide();
            $("#searchsize").hide();
            $('#time').show();
            $("#stock-time").hide();
            $('#customsize').hide();
        }
    });

    /*data = {
        "sizes": [
            {"size": 'xs', "stock": 5},
            {"size": 'xs', "stock": 5}
        ]
    }*/

    $("#customsizetoggle").click(function () {
        if (this.checked) {
            $("#stock-time").show();
        } else {
            $("#stock-time").hide();
        }
    });

    /*$('#addBtn').on('click', function () {
        $(this).val('Please wait...')
            .attr('disabled', 'disabled');
    });*/


    $('#addBtn').click(function () {
        $(this).val('Please wait...').attr("disabled", true);
        if($('.impt-control').val() === ''){
            $.notify("Some empty fields are required");
            $('#addBtn').val('Submit').attr("disabled", false);
        }else {
            //$('#loaderModal').modal();
            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            //console.log(tok);
            <?php else: ?>
                tok = '';
                <?php endif; ?>

            var productSizes = [];
            var art = getAllImagesSrc('.artworkImages');
            var mat = getAllFabricSrc('.materialImg');
            let measurement = JSON.stringify(myMeasure.getSelected());
            var a = [];
            if (art.length) {
                for (i = 0; i < art.length; i++) {
                    if (!validateUrl(art[i])) {
                        a.push(art[i]);
                    }
                }
            }



            arrayPicture = [];
            $('.add-img').each(function(){
                arrayPicture.push(getImageSrc('.add-img'))
            });



            arrayPicture1 = [];
            $('.realMoreFind-1 .imgmore').each(function(){
                if($(this).hasClass('next-1'))
                    arrayPicture1.push($(this).attr('src'));
            });

            arrayPicture2 = [];
            $('.realMoreFind-2 .next-2').each(function(){
                if($(this).hasClass('imgmore')) {
                    arrayPicture2.push($(this).attr('src'));
                }
            });



            arrayPicture3 = [];
            $('.realMoreFind-3 .next-3').each(function(){
                if($(this).hasClass('imgmore')) {
                    arrayPicture3.push($(this).attr('src'));
                }
            });

            arrayPicture4 = [];
            $('.realMoreFind-4 .imgmore').each(function(){
                if($(this).hasClass('next-4')) {
                    arrayPicture4.push($(this).attr('src'));
                }
            });



            var data = {
                "name": $('#productName').val(),
                "amount": $('#productPrice').val(),
                "prodSummary": $('#productSummary').val(),
                "description": $('#productDescription').val(),
                "slashedPrice": $('#DiscountproductPrice').val(),
                "percentageDiscount": $('#percent').val(),
                "productAttributes": [
                    {
                        "picture": arrayPicture,
                        "colourName": $('#colorMaterialName').val(),
                        "colourPicture": getImageSrc('.displayMat')
                    }
                ],
                "subCategoryId": $('#subcat').val(),
                "productType": prodType
            };

            var myListOne = [];
            $('.realMoreFind-1 .moreRemoveItems').each(function(){
                myListOne.push({
                    "name": $(this).find('.moreSMeasure').html(),
                    "stockNo": $(this).find('.moreSStock').html()
                })
            });

            var myListTwo = [];
            $('.realMoreFind-2 .moreRemoveItems').each(function(){
                myListTwo.push({
                    "name": $(this).find('.moreSMeasure').html(),
                    "stockNo": $(this).find('.moreSStock').html()
                })
            });

            var myListThree = [];
            $('.realMoreFind-3 .moreRemoveItems').each(function(){
                myListThree.push({
                    "name": $(this).find('.moreSMeasure').html(),
                    "stockNo": $(this).find('.moreSStock').html()
                })
            });


            var myListFour = [];
            $('.realMoreFind-4 .moreRemoveItems').each(function(){
                myListFour.push({
                    "name": $(this).find('.moreSMeasure').html(),
                    "stockNo": $(this).find('.moreSStock').html()
                })
            });

            if(attrcun == 1 ) {
                data.productAttributes[1] = {
                    "picture": arrayPicture1,
                    "colourName": $('.colorMaterialName-1').val(),
                    "colourPicture": getImageSrc('.matCol-1')
                };
                data.productAttributes[1].productSizes = myListOne;
            }

            if(attrcun == 2){
                data.productAttributes[1] = {
                    "picture": arrayPicture1,
                    "colourName": $('.colorMaterialName-1').val(),
                    "colourPicture": getImageSrc('.matCol-1')
                };
                data.productAttributes[1].productSizes = myListOne;

                data.productAttributes[2] = {
                    "picture": arrayPicture2,
                    "colourName": $('.colorMaterialName-2').val(),
                    "colourPicture": getImageSrc('.matCol-2')
                }
                data.productAttributes[2].productSizes = myListTwo;

            }



            if(attrcun === 3){
                data.productAttributes[1] = {
                    "picture": arrayPicture1,
                    "colourName": $('.colorMaterialName-1').val(),
                    "colourPicture": getImageSrc('.matCol-1')
                };
                data.productAttributes[1].productSizes = myListOne;
                data.productAttributes[2] = {
                    "picture": arrayPicture2,
                    "colourName": $('.colorMaterialName-2').val(),
                    "colourPicture": getImageSrc('.matCol-2')
                };
                data.productAttributes[2].productSizes = myListTwo;

                data.productAttributes[3] = {
                    "picture": arrayPicture3,
                    "colourName": $('.colorMaterialName-3').val(),
                    "colourPicture": getImageSrc('.matCol-3')
                };
                data.productAttributes[3].productSizes = myListThree;

            }


            if(attrcun === 4){
                data.productAttributes[1] = {
                    "picture": arrayPicture1,
                    "colourName": $('.colorMaterialName-1').val(),
                    "colourPicture": getImageSrc('.matCol-1')
                };
                data.productAttributes[1].productSizes = myListOne;

                data.productAttributes[2] = {
                    "picture": arrayPicture2,
                    "colourName": $('.colorMaterialName-2').val(),
                    "colourPicture": getImageSrc('.matCol-2')
                };
                data.productAttributes[2].productSizes = myListTwo;

                data.productAttributes[3] = {
                    "picture": arrayPicture3,
                    "colourName": $('.colorMaterialName-3').val(),
                    "colourPicture": getImageSrc('.matCol-3')
                };
                data.productAttributes[3].productSizes = myListThree;


                data.productAttributes[4] = {
                    "picture": arrayPicture4,
                    "colourName": $('.colorMaterialName-4').val(),
                    "colourPicture": getImageSrc('.matCol-4')
                }
                data.productAttributes[4].productSizes = myListFour;

            }



            if (prodType == 1) {
                data.mandatoryMeasurements = JSON.stringify(measurement);
                data.artWorkPicture = a;
                data.materialPicture = mat;
                data.materialPrice = $('#materialPrice').val();
                data.materialName = $('#materialName').val();
            } else if (prodType == 2 || prodType == 3) {
                data.mandatoryMeasurements = JSON.stringify(["neck", "wrist"]);
            }


            for (var i = 0; i < $('.removeItems').length; i++) {
                productSizes.push({
                    "name": $('.sMeasure').html(),
                    "stockNo": $('.sStock').html()
                });
            }


            myList = [];
            $('.removeItems').each(function () {
                myList.push({
                    "name": $(this).find('.sMeasure').html(),
                    "stockNo": $(this).find('.sStock').html()
                })
            });

            data.productAttributes[0].productSizes = myList;


            console.log(data);
            $.ajax({
                url: "{{getBaseUrl()}}/fashion/product/addproduct",
                type: "POST",
                dataType: "json",
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                data: JSON.stringify(data),
                success: function (res) {
                    console.log(res);
                    if(res.status == 0){
                        $('#loaderModal').hide();
                        $(this).val('Please wait...').attr("disabled", false);
                        swal('Product added successfully', '', 'success');
                        window.location.href = '{{route('designer.product', str_slug($storeName))}}';
                    }else {
                        $(this).val('Submit').removeAttr('disabled', 'disabled');
                        swal(res.message, '', 'error');
                    }


                },
                error: function(e){
                    console.log(e);
                    $('#loaderModal').hide();
                    $(this).val('submit').attr("disabled", false);
                    $.notify(e, 'error');
                }
            })
        }




    });


    /* $(document).on('click', '.add-product', function () {
 //$('#loaderModal').modal();
 $('#addBtn').val('Please wait...').attr("disabled", "disabled");

 $('.mySpinner').addClass('lds-dual-ring');

<?php if(!is_null(session('designerToken'))): ?>
    tok = <?= session('designerToken') ?>;
    //console.log(tok);
    <?php else: ?>
    tok = '';
<?php endif; ?>

    if (isNotEmpty('.form-control:not(.discountField,#searchsizeinput)', 'error')) {
        //let measurement = JSON.stringify(myMeasure.getSelected());
        var noval = false;
        if ($('#exampleCheck4:checked').length > 0) {
            if ($('#time').val() < 1) {
                noval = true;
            }
        }

        if (!noval) {
            if (isNumeric($('#productPrice').val())) {
                if ($('#productName').val().length < 30) {
                    let measurement = JSON.stringify(myMeasure.getSelected());
                    if ($('.ct').val() != '') {
                        let product = getAllImagesSrc('.pImg');
                        if (product.length) {
                            if ($('[name=availability]').is(':checked')) {
                                if ($('#exampleCheck1').is(':checked')) {
                                    /!*$(this).attr('disabled', 'disabled');*!/
                                    var art = getAllImagesSrc('.artworkImages');
                                    var mat = getAllFabricSrc('.materialImg');
                                    var a = [];
                                    if (art.length) {
                                        for (i = 0; i < art.length; i++) {
                                            if (!validateUrl(art[i])) {
                                                a.push(art[i]);
                                            }
                                        }
                                    }
                                    var data = {
                                        "name": $('#productName').val(),
                                        "amount": $('#productPrice').val(),
                                        "prodSummary": $('#productSummary').val(),
                                        "description": $('#productDescription').val(),
                                        "slashedPrice": $('#DiscountproductPrice').val(),
                                        "percentageDiscount": $('#percent').val(),
                                        "color": getAll('.colorPicker'),
                                        "styleId": $('#style').val(),
                                        "subCategoryId": $('#subcat').val(),
                                        "picture": product,
                                        "status": 'A',
                                        "inStock": $('[name=availability]:checked').val(),
                                        "productType": prodType
                                    };

                                    if (prodType == 1) {
                                        data.mandatoryMeasurements = JSON.stringify(measurement);
                                        data.artWorkPicture = a;
                                        data.materialPicture = mat;
                                        data.materialPrice = $('#materialPrice').val();
                                        data.materialName = $('#materialName').val();
                                    } else if (prodType == 2 || prodType == 3) {
                                        data.mandatoryMeasurements = JSON.stringify(["neck", "wrist"]);
                                    }

                                    priceFlag = false;
                                    stockFlag = false;

                                    if (data.inStock === 'Y') {
                                        if ($(".sizeinput").length > 0) {
                                            $.each($(".stock"), function () {
                                                if (typeof $(this).val() === 'undefined' || $(this).val() == '') {
                                                    stockFlag = true;
                                                }
                                            });

                                            if (!stockFlag) {
                                                var productSizes = [];
                                                totalStock = parseInt(0);

                                                $.each($(".size .input-group"), function () {

                                                    productSizes.push({
                                                        "name": $(this).children('.sizeinput').val(),
                                                        "stockNo": $(this).children('.stock').val()
                                                    });

                                                    totalStock += parseInt($(this).children('.stock').val());
                                                });

                                                data.productSizes = productSizes;
                                                data.stockNo = totalStock;
                                            } else {
                                                $.notify('All sizes must have stock');
                                            }
                                        } else {
                                            $.notify("You must select a size");
                                            $('.mySpinner').removeClass('lds-dual-ring');
                                            $(this).val('Submit').removeAttr('disabled', 'disabled');
                                        }

                                        if ($('#customsizetoggle:checked').length > 0) {
                                            data.acceptCustomSizes = 'Y';
                                            if ($("#stock-time").val() === '' || parseInt($("#stock-time").val()) <= 0) {
                                                stockFlag = true;
                                                $("#stock-time").notify("Can't be less than 0");
                                                $.notify("Delivery time Can't be less than 0");
                                                $('.mySpinner').removeClass('lds-dual-ring');
                                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                            } else {
                                                data.numOfDaysToComplete = parseInt($("#stock-time").val());
                                            }
                                        } else {
                                            data.acceptCustomSizes = 'N';
                                            data.numOfDaysToComplete = null;
                                        }

                                        data.availability = 'Y';
                                    } else {
                                        data.availability = 'N';
                                        data.stockNo = 0;
                                        data.productSizes = [];
                                        data.numOfDaysToComplete = $('#time').val();
                                        if (data.numOfDaysToComplete === '' || parseInt(data.numOfDaysToComplete) <= 0) {
                                            stockFlag = true;
                                            $.notify("Delivery time Can't be less than 0");
                                            $("#time").notify("Can't be less than 0");
                                            $('.mySpinner').removeClass('lds-dual-ring');
                                            $(this).val('Submit').removeAttr('disabled', 'disabled');
                                        }
                                    }

                                    if (activeDiscount === 'price') {
                                        if (typeof data.slashedPrice !== 'undefined' && data.slashedPrice !== '') {
                                            if (parseFloat(data.slashedPrice) > parseFloat(data.amount)) {
                                                priceFlag = true;
                                                $.notify("Discount can't be greater than actual price");
                                                $('.mySpinner').removeClass('lds-dual-ring');
                                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                            }
                                        }
                                        data.percentageDiscount = '';
                                    } else if (activeDiscount === 'percentage') {
                                        if (typeof data.percentageDiscount !== 'undefined' && data.percentageDiscount !== '') {
                                            if (parseFloat(data.percentageDiscount) <= 0 || parseFloat(data.percentageDiscount) > 100) {
                                                priceFlag = true;
                                                $.notify("Percentage must be between 1 and 100");
                                                $('.mySpinner').removeClass('lds-dual-ring');
                                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                            }
                                        }
                                        data.slashedPrice = '';
                                    }
                                    if (priceFlag || stockFlag) {
                                        $('.mySpinner').removeClass('lds-dual-ring');
                                    } else {
                                        $.ajax({
                                            url: "{{getBaseUrl()}}/fashion/product/addproduct",
                                            type: "POST",
                                            dataType: "json",
                                            headers: {
                                                'Authorization': tok,
                                                'Content-Type': 'application/json; charset=utf-8',
                                                'accept': 'application/json'
                                            },
                                            data: JSON.stringify(data),
                                            success: function (res) {
                                                //console.log(data);
                                                //console.log(res);
                                                if (res.status == 0) {
                                                    swal('Product Added successfully', '', 'success');
                                                    $(this).prop('disabled', true);
                                                    $('.mySpinner').removeClass('lds-dual-ring');
                                                    //$('#loaderModal').modal();
                                                    window.location.href = '{{route('designer.product', str_slug($storeName))}}';
                                                } else {
                                                    $(this).val('Submit').removeAttr('disabled', 'disabled');
                                                    swal(res.message, '', 'error');
                                                    console.log(res.message);
                                                    $('.mySpinner').removeClass('lds-dual-ring');
                                                }
                                            },
                                            error: function (e) {
                                                console.log(e);
                                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                                $.notify(e, 'error');
                                                $('#loaderModal').modal('hide');
                                                $('.mySpinner').removeClass('lds-dual-ring');
                                            }
                                        });
                                    }
                                } else {
                                    $('#exampleCheck1').notify('Please check to add', 'error');
                                    $(this).val('Submit').removeAttr('disabled', 'disabled');
                                    $('.mySpinner').removeClass('lds-dual-ring');
                                }
                            } else {
                                $('.avail').notify('Please select product availability', 'error');
                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                $('.mySpinner').removeClass('lds-dual-ring');
                            }
                        } else {
                            $('.proImg').notify('Upload at least one image', 'error');
                            $.notify('Upload at least one image', 'error');
                            $(this).val('Submit').removeAttr('disabled', 'disabled');
                            $('.mySpinner').removeClass('lds-dual-ring');
                        }
                    } else {
                        $('.ct').each(function () {
                            if ($(this).val() == '') {
                                $(this).addClass('error');
                                $(this).notify('Select an option', 'error');
                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                $('.mySpinner').removeClass('lds-dual-ring');
                            }
                        });
                    }
                } else {
                    $('#productName').notify('Product name must be less than 20 characters');
                    $.notify('Product name must be less than 20 characters');
                    $('#productName').addClass('error');
                    $(this).val('Submit').removeAttr('disabled', 'disabled');
                    $('.mySpinner').removeClass('lds-dual-ring');
                }
            } else {
                $('#productPrice').addClass('error');
                $('#productPrice').notify('Price must be a valid number');
                $.notify('Price must be a valid number');
                $(this).val('Submit').removeAttr('disabled', 'disabled');
                $('.mySpinner').removeClass('lds-dual-ring');
            }
        } else {
            $('#time').notify('Time cannot be less than 1 day');
            $(this).val('Submit').removeAttr('disabled', 'disabled');
            $('.mySpinner').removeClass('lds-dual-ring');
        }

    } else {
        $.notify('Some Fields were not filled', 'error');
        $(this).val('Submit').removeAttr('disabled', 'disabled');
        $('.mySpinner').removeClass('lds-dual-ring');
    }
})*/


</script>