<script>

    $('.mainFabricType').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                $('#mainFabricAction').html(`
                    <span><i class="pull-right fa fa-times restoreMainImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check mainCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addMainCrop"></i></span>
                        `)
                //convertToBase64(this, '#mainFabric');
                $(this).siblings('.imgcassx').addClass('valid-imgcassx');
                convertToEditBase64(this, 'img')
            }
        }
    });

    $('#topFabricType').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            console.log($(this).get(0).files[i].size);
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                $('#topFabricAction').html(`
              <span><i class="pull-right fa fa-times restoreTopImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check topCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addTopCrop"></i></span>
                        `);
                convertToBase64(this, '#topFabric');
            }
        }
    });

    $('#frontFabricType').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            console.log($(this).get(0).files[i].size);
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                $('#frontFabricAction').html(`
                        <span><i class="pull-right fa fa-times restoreFrontImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check frontCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addFrontCrop"></i></span>
                        `);
                convertToBase64(this, '#frontFabric');
            }
        }
    });


    $('#sideFabricType').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            console.log($(this).get(0).files[i].size);
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                $('#sideFabricAction').html(`
                         <span><i class="pull-right fa fa-times restoreSideImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check sideCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addSideCrop"></i></span>
                        `);
                convertToBase64(this, '#sideFabric');
            }
        }
    });

    $('#backFabricType').on('change', function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            console.log($(this).get(0).files[i].size);
            var fileSize = $(this).get(0).files[i].size / 1024;
            if (fileSize > 500) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                $('#backFabricAction').html(`
                   <span><i class="pull-right fa fa-times restoreBackImage"></i></span>
                        <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                        <span><i class="pull-right fa fa-check backCrop"></i></span>
                       <span><i class="pull-right fa fa-plus addBackCrop"></i></span>
                        `);
                convertToBase64(this, '#backFabric');
            }
        }
    });

    $(document).on('click', '.restoreMainImage', function (e) {
        console.log('main');
        e.preventDefault();
        var that = $(this);
        //that.closest('#mainFabricAction').siblings('#mainFabricType').attr('disabled', false);
        that.closest('#mainFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#mainFabricAction').siblings('label').find('#mainFabric').removeClass('cropper-hidden');

    });

    $(document).on('click', '.mainCrop', function (e) {
        e.preventDefault();
        var cropImage = $('#mainFabric').cropper('getCroppedCanvas').toDataURL();
        $('.displayMat').attr('src', cropImage);

    });


    $(document).on('click', '.restoreSideImage', function (e) {
        e.preventDefault();
        var that = $(this);
        that.closest('#sideFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#sideFabricAction').siblings('label').find('#sideFabric').removeClass('cropper-hidden');

    });

    $(document).on('click', '.restoreFrontImage', function (e) {
        e.preventDefault();
        var that = $(this);
        that.closest('#frontFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#frontFabricAction').siblings('label').find('#frontFabric').removeClass('cropper-hidden');
    });


    $(document).on('click', '.restoreBackImage', function (e) {
        e.preventDefault();
        var that = $(this);
        that.closest('#backFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#backFabricAction').siblings('label').find('#backFabric').removeClass('cropper-hidden');
    });

    $(document).on('click', '.restoreTopImage', function (e) {
        e.preventDefault();
        var that = $(this);
        that.closest('#topFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#topFabricAction').siblings('label').find('#topFabric').removeClass('cropper-hidden');
    });

    $(document).on('click', '.addMainCrop', function (e) {
        e.preventDefault();
        var that = $(this);
        //that.closest('#mainFabricAction').siblings('#mainFabricType').attr('disabled', true);
        that.closest('#mainFabricAction').siblings('.cropper-container').toggle();
        that.closest('#mainFabricAction').siblings('#mainFabric').toggleClass('cropper-hidden');
        var $image = $('#mainFabric');
        $image.cropper({
            autoCropArea: 0.5,
            aspectRatio: 16 / 9,
            dragCrop: false,
            responsive: true,
            strict: true,
            multiple: true,
        });
    });

    $(document).on('click', '.addSideCrop', function (e) {
        e.preventDefault();
        var that = $(this);
        var $image = $('#sideFabric');
        that.closest('#sideFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#sideFabricAction').siblings('label').find('#sideFabric').toggleClass('cropper-hidden');
        $image.cropper();
    });

    $(document).on('click', '.addFrontCrop', function (e) {
        e.preventDefault();
        var that = $(this);
        var $image = $('#frontFabric');
        that.closest('#frontFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#frontFabricAction').siblings('label').find('#frontFabric').toggleClass('cropper-hidden');
        $image.cropper();
    });

    $(document).on('click', '.addTopCrop', function (e) {
        e.preventDefault();
        var that = $(this);
        var $image = $('#topFabric');
        that.closest('#topFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#topFabricAction').siblings('label').find('#topFabric').toggleClass('cropper-hidden');
        $image.cropper();
    });

    $(document).on('click', '.addBackCrop', function (e) {
        e.preventDefault();
        var that = $(this);
        var $image = $('#backFabric');
        that.closest('#backFabricAction').siblings('label').find('.cropper-container').toggle();
        that.closest('#backFabricAction').siblings('label').find('#backFabric').toggleClass('cropper-hidden');
        $image.cropper();
    });


    $(document).on('click', '.topCrop', function (e) {
        e.preventDefault();
        var cropImage = $('#topFabric').cropper('getCroppedCanvas').toDataURL();
        $('.displayMat').attr('src', cropImage);
    });


    $(document).on('click', '.changeMainImage', function (e) {
        e.preventDefault();
        var that = $(this);
        that.closest('div').siblings('#mainFabric').attr('src', '{{ asset('img/material1.jpg')}}');
        that.closest('div').siblings('#sideFabric').attr('src', '{{ asset('img/material1.jpg')}}');
        that.closest('div').siblings('#backFabric').attr('src', '{{ asset('img/material1.jpg')}}');
        that.closest('div').siblings('#frontFabric').attr('src', '{{ asset('img/material1.jpg')}}');
        that.closest('div').siblings('#topFabric').attr('src', '{{ asset('img/material1.jpg')}}');


        // $('#mainFabric').attr('src', '{{ asset('img/material1.jpg')}}');
    });


    var activeDiscount;

    var sizes = [];
    var selectedSizes = [];

    @if(isset($product->productSizes))
    @foreach($product->productSizes as $productSize)
    selectedSizes.push('{{strtoupper($productSize->name)}}');
    @endforeach
    @endif

    $.ajax({
        url: '{{env('GET_BASE_URL')}}/fashion/size/getsizes',
        type: 'GET',
        success: function (res) {
            if (res.status = '00') {
                tempSizes = res.data.data;
                $.each(tempSizes, function () {
                    sizes.push(this.name.toUpperCase());
                })
            }
        }
    });

    $.each(sizes.slice(0, 4), function () {
        $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
    });
    sizeClickListener();
    deleteClickListener();

    var keyPressed = false;

    $("#searchsizeinput").keydown(function () {
        keyPressed = true;
    });

    $("#searchsizeinput").focus(function () {
        searchRes();
    });

    $("#searchsizeinput").keyup(function () {
        if (keyPressed) {
            searchRes();
            keyPressed = false;
        }
    });


    $('#fabricsize').focus(function () {
        $('#fabricsearch').show();
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($("#fabricsize").val().toUpperCase()) && !selectedSizes.includes(a));
        });
        console.log('2');
        $("#fabricsearch").html('');
        $.each(currarr.slice(0, 4), function () {
            $("#fabricsearch").append('<p class="sizesearchres" >' + this + '</p>');
            fabricClickListeners();
        })
    });

    function fabricClickListeners() {
        $('.sizesearchres').click(function () {
            //console.log($(this).html());
            var size = $(this).html();
            selectedSizes.push(size.toUpperCase());
            $('#fabricsize').val(size);
            $('#fabricsearch').toggle();
        });
    }

    function searchRes() {
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($("#searchsizeinput").val().toUpperCase()) && !selectedSizes.includes(a));
        });

        $("#searchres").html('');
        $.each(currarr.slice(0, 4), function () {
            $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
        });
        sizeClickListener();
        deleteClickListener();
    }

    function deleteClickListeners() {
        $(".delete-size").click(function () {
            parent = $(this).closest('.size');
            pinb = $(parent).children('.input-group')
            delSize = pinb.children('.sizeinput').val();
            parent.remove();

            newSelectedSize = $.grep(selectedSizes, function (a) {
                return (a !== delSize);
            });

            selectedSizes = newSelectedSize;
        });
    }

    function searchRes() {
        var currarr = $.grep(sizes, function (a) {
            return (a.includes($("#searchsizeinput").val().toUpperCase()) && !selectedSizes.includes(a));
        });

        $("#searchres").html('');
        $.each(currarr.slice(0, 4), function () {
            $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
        });
        sizeClickListener();
        deleteClickListener();
    }

    $('.addFabricMeasurement').click(function () {
        var pix = [];
        var data = {
            measurement: $('#fabricsize').val(),
            stock: $('#fabricstock').val()
        };
        pix.push(data);
        displayMeasureSize(pix);
        $('#fabricsize').val('');
        $('#fabricstock').val('');
    });

    function displayMeasureSize(arr) {
        if (arr.length > 0) {
            for (var i = 0; i < arr.length; i++) {
                $('#measureAttachment').append(`
                        <div class="row removeItems" id="removeItems">
                         <div class="col-sm-5">
                        <div class="sMeasure">${arr[i].measurement}</div>
                    </div>
                        <div class="col-sm-5">
                            <div class="sStock">${arr[i].stock}</div>
                        </div>
                        <div class="col-sm-2">
                           <i class="fa fa-trash removeAttachedMeasurement"></i>
                          </div>
                          </div>
                        `);
            }
        }
    }

    function deleteClickListener() {
        $(".delete-size").click(function () {
            parent = $(this).closest('.size');
            pinb = $(parent).children('.input-group')
            delSize = pinb.children('.sizeinput').val();
            parent.remove();

            newSelectedSize = $.grep(selectedSizes, function (a) {
                return (a !== delSize);
            });

            selectedSizes = newSelectedSize;
        });
    }

    function sizeClickListener() {
        $(".sizesearchres").click(function () {
            var size = $(this).html();
            selectedSizes.push(size.toUpperCase());

            $("#sizes").append('<div class="size" >\n' +
                '                                    <div class="input-group" >\n' +
                '                                        <div class="input-group-prepend" >\n' +
                '                                            <i class="fa fa-times mr-2 delete-size text-danger" ></i>\n' +
                '                                        </div>\n' +
                '                                        <input class="form-control sizeinput" type="text" readonly value="' + size + '" >\n' +
                '                                        <input class="form-control stock" type="number" placeholder="Stock" >\n' +
                '                                    </div>\n' +
                '                                </div>');

            $("#searchres").html("");
            $("#searchsizeinput").val("");
            deleteClickListener();
        });
    }

    @if(strtoupper($product->inStock) == 'Y')
    $("#exampleCheck3").prop("checked", true);
    $('#sizes').show();
    $("#searchsize").show();
    $('#customsize').show();
    @if(strtoupper($product->acceptCustomSizes) == 'Y')
    $("#customsizetoggle").prop('checked', true);
    $("#stock-time").show();
    @endif
    $('#time').hide();
    @else
    $("#exampleCheck4").prop("checked", true);
    $('#sizes').hide();
    $("#searchsize").hide();
    $('#time').show();
    $("#stock-time").hide();
    $('#customsize').hide();
    @endif

    $(document).on('click', '#selectEditPercentage', function () {
        $('.discountEditGroup').toggle();
        $('.percentGroup').toggle();
        $(this).toggle();
        $('#selectEditDiscount').toggle();
        $.notify('you can only submit on value');
        activeDiscount = 'percentage'
    });

    $(document).on('click', '#selectEditDiscount', function () {
        $('.discountEditGroup').toggle();
        $('.percentGroup').toggle();
        $(this).toggle();
        $('#selectEditPercentage').toggle();
        activeDiscount = 'price'
    });


    //var newMeasurement = JSON.parse(str);


    myMeasure = sellect("#my-editMeasurement", {
        originList: ["neck", "armHole", "biceps",
            "longSleeve",
            "shortSleeve",
            "fullShoulder",
            "halfShoulder",
            "elbow",
            "fullChest",
            "bust",
            "overBreast",
            "underChest",
            "underBust",
            "shirtLength",
            "stomach",
            "wrist",
            "fullLength",
            "trouserWaist",
            "seat",
            "outSeam",
            "inSeam",
            "crotch",
            "thigh",
            "knee:",
            "ankle",
            "butt"],
        destinationList: [
            @if(isset($productMeasure))
                    @if(count($productMeasure))
                    @foreach($productMeasure as $measuress)
                '{{$measuress}}',
            @endforeach
            @endif
            @endif
        ]
    });

    myMeasure.init();

    $(document).ready(function () {

        $('.productPic').on('change', function () {
            var siblingImg = $(this).siblings('img.pImg');
            convertToBase64(this, siblingImg);
        });
        //edit material
        $('.publish').on('click', function () {
            var mat = [];
            $('.materialImg').each(function (i, obj) {
                var id = ($(this).attr('matId')) ? $(this).attr('matId') : '';
                var pix64 = $(this).attr('src');
                if (!validateUrl(pix64)) {
                    mat.push({
                        'id': id,
                        'materialPicture': pix64
                    });
                }
            });
            var data = {
                "productId": "{{$product->id}}",
                "materialPicture": mat
            }
            console.log(data);
            $('#loaderModal').modal();
            $.ajax({
                url: `{{route('designer.product.editMaterial')}}`,
                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    console.log(result);
                    $('#loaderModal').modal('hide');
                    //$.notify('Updated successfully', 'success');
                    swal('Updated successfully', '', 'success');
                    window.location.href = '{{route('designer.product', str_slug($storeName))}}';
                },
                error: function (e) {
                    console.log(e);
                    $.notify(e, 'error');
                    $('#loaderModal').modal('hide');

                }
            });
        });
        //change material
        $(document).on('change', '.mater', function () {
            var siblingImg = $(this).siblings('img.materialImg');
            convertToBase64(this, siblingImg);
        });
        //remove artwork image
        $('.removeArtwork').on('click', function () {

            var a = $(this).siblings('label').children('img.artworkImages').attr('artworkId');
            console.log(a);
            var url = '{{route('designer.product.removeArtWorkImage')}}';
            removeImage(url, a);

            $(this).siblings('label').children('img.artworkImages').attr('src', '{{ asset('img/landscape.png')}}');
        });
        //edit artwork
        $('.edit-artwork').on('click', function () {
            var artwork = getAllImagesSrc('.artworkImages');
            var art = [];
            var delArt = [];
            if (artwork.length) {
                $('.artworkImages').each(function (i, obj) {
                    var id = ($(this).attr('artworkId')) ? $(this).attr('artworkId') : '';
                    var pix64 = $(this).attr('src');
                    if (!validateUrl(pix64)) {
                        art.push({
                            'id': id,
                            'artWorkPicture': pix64
                        });
                    }
                });
                var data = {
                    "productId": "{{$product->id}}",
                    "artWorkPicture": art
                }
                $('#loaderModal').modal();
                $.ajax({
                    url: `{{route('designer.product.editArtwork')}}`,
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (result) {
                        //console.log(result);
                        if (result.status == 0) {
                            $('#loaderModal').modal('hide');
                            swal('Updated successfully', '', 'success');
                            $('.p').hide();
                            $('#p4').show();
                        }
                    },
                    error: function (e) {
                        $('#loaderModal').modal('hide');
                        console.log(e);
                    }
                });
            }
        });


        //delete image
        $(document).on('click', '.deleteImage', function () {

            var target = $(this).attr('target');
            if (target == 'p2') {
                if ($('input[name=checkProduct]:checked').length > 0) {
                    var answer = confirm('Are you sure you want to continue. Click ok to continue');
                    if (answer) {
                        var checked = $('input[name=checkProduct]:checked');
                        var arr = [];
                        $(checked).each(function () {
                            arr.push($(this).attr('data-target'));
                        });
                        var data = {
                            "ids": arr,
                        }
                        console.log(data);
                        $('#loaderModal').modal();
                        $.ajax({
                            type: 'POST',
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            url: '{{route('designer.product.removeProductImage')}}',
                            success: function (result) {
                                console.log(result);
                                if (result.status == 0) {

                                    $('input[name=checkProduct]:checked').each(function () {
                                        var targ = $(this).attr('data-target');
                                        $('#' + targ).remove();

                                        console.log(targ);
                                    });
                                    //$.notify('Successfully deleted', 'error');
                                    swal('Successfully deleted', '', 'success');
                                } else {
                                    $.notify(result.message, 'error');
                                }
                                $('#loaderModal').modal('hide');

                            },
                            error: function (e) {
                                $('#loaderModal').modal('hide');
                                $.notify(e, 'error');
                                console.log(e)
                            },
                        });
                    }
                } else {
                    $('#loaderModal').modal('hide');
                    alert('Select at least one to delete');
                }

            } else if (target == 'p3') {
                if ($('input[name=checkArtwork]:checked').length > 0) {
                    var answer = confirm('Are you sure you want to continue. Click ok to continue');
                    if (answer) {
                        var checked = $('input[name=checkArtwork]:checked');
                        var arr = [];
                        $(checked).each(function () {
                            arr.push($(this).attr('data-target'));
                        });
                        var data = {
                            "ids": arr,
                        }
                        console.log(data);

                        $.ajax({
                            type: 'POST',
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            url: '{{route('designer.product.removeArtWorkImage')}}',
                            success: function (result) {
                                console.log(result);
                                $('#loaderModal').modal('hide');
                            },
                            error: function (e) {
                                $('#loader').hide();
                                console.log(e)
                            },
                        });
                    }
                } else {
                    alert('Select at least one to delete');
                }

            } else if (target == 'p4') {
                if ($('input[name=checkMaterial]:checked').length > 0) {
                    var answer = confirm('Are you sure you want to continue. Click ok to continue');
                    if (answer) {
                        var checked = $('input[name=checkMaterial]:checked');
                        var arr = [];
                        $(checked).each(function () {
                            arr.push($(this).attr('data-target'));
                        });
                        var data = {
                            "ids": arr,
                        }
                        console.log(data);

                        $.ajax({
                            type: 'POST',
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: 'json',
                            url: '{{route('designer.product.removeMaterialImage')}}',
                            success: function (result) {
                                console.log(result);
                                $('#loader').hide();

                            },
                            error: function (e) {
                                $('#loader').hide();
                                console.log(e)
                            },
                        });
                    }
                } else {
                    alert('Select at least one to delete');
                }

            }
        });

        $('.selectAll').on('click', function () {
            var target = $(this).attr('target');
            if (target == 'p2') {
                if ($('input[name=checkProduct]:checked').length == $('input[name=checkProduct]').length) {
                    $('input[name=checkProduct]').prop('checked', false);
                    $(this).text('Select All');
                    $('.pImg').css({
                            filter: 'none'
                        }
                    );
                } else {
                    $(this).text('Deselect');
                    $('input[name=checkProduct]').prop('checked', true);
                    $('.pImg').css({
                            filter: 'grayscale(100%)'
                        }
                    );

                }
            } else if (target == 'p3') {
                if ($('input[name=checkArtwork]:checked').length == $('input[name=checkArtwork]').length) {
                    $('input[name=checkArtwork]').prop('checked', false);
                    $(this).text('Select All');
                    $('.artworkImages1').css({
                            filter: 'none'
                        }
                    );
                } else {
                    $(this).text('Deselect');
                    $('input[name=checkArtwork]').prop('checked', true);
                    $('.artworkImages1').css({
                            filter: 'grayscale(100%)'
                        }
                    );

                }
            } else if (target == 'p4') {
                if ($('input[name=checkMaterial]:checked').length == $('input[name=checkMaterial]').length) {
                    $('input[name=checkMaterial]').prop('checked', false);
                    $(this).text('Select All');
                    $('.materialImages').css({
                            filter: 'none'
                        }
                    );
                } else {
                    $(this).text('Deselect');
                    $('input[name=checkMaterial]').prop('checked', true);
                    $('.materialImages').css({
                            filter: 'grayscale(100%)'
                        }
                    );

                }
            }


        });

        counter = 0;
        $('.colorPicker').minicolors();
        $.initialize('.colorPicker', function () {
            $(this).minicolors();
        });
        var styles = [];
        tagId = 0;


        $('#productGroup').on('change', function () {
            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            <?php else: ?>
                tok = '';
                <?php endif; ?>
            var id = $(this).val();
            styles = [];
            var data = {
                categoryId: id,
                productType: $("input[name='producttype']:checked").data('name')
            }

            console.log(data);

            $.ajax({
                url: `http://wawoohapi.herokuapp.com/fashion/product/getsubcatbyproducttype`,
                /*url: `/subcategory?id=${id}`,*/
                type: "POST",
                dataType: "json",
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                data: JSON.stringify(data),
                success: function (result) {
                    var result = result.data;
                    console.log(result.data);
                    var html = `<option value='' >Select</option>`;
                    // console.log(result);
                    if (result.length) {
                        $('#subcat').removeAttr('disabled');
                        for (i = 0; i < result.length; i++) {
                            html += `<option value='${result[i].id}'>${result[i].subCategory}</option>`;
                        }
                        $('#subcat').html(html);
                    } else {
                        $.notify('error', 'error');
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('#subcat').on('change', function () {
            var id = $(this).val();
            styles = [];


            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>

            $.ajax({
                url: `http://wawoohapi.herokuapp.com/fashion/product/${id}/getstyles`,
                /*url: `/styles?subCatId=${id}`,*/
                type: "GET",
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                success: function (result) {
                    console.log(result);
                    var result = result.data;
                    var html = `<option value='null'>Select</option>`;
                    console.log(result);

                    if (result instanceof Array) {
                        for (i = 0; i < result.length; i++) {


                            html += `<option value='${result[i].id}'>${result[i].style}</option>`;
                        }
                    } else {
                        $.notify('error', 'error');
                    }
                    $('#style').html(html);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
        //edit product images
        /*$('.edit-product-images').on('click', function () {
            var product = getAllImagesSrc('.pImg');
            if (product.length >= 1) {
                pImg = [];
                picture = [];
                $('.pImg').each(function (i, obj) {
                    var id = ($(this).attr('pictureId')) ? $(this).attr('pictureId') : '';
                    var pix64 = $(this).attr('src');
                    pImg.push({
                        'id': id,
                        'picture': pix64
                    });
                });

                for (i = 0; i < pImg.length; i++) {
                    if (!validateUrl(pImg[i].picture)) {
                        picture.push(pImg[i]);
                    }
                }

                var data = {
                    "productId": "{{$product->id}}",
                        "picture": picture
                    }
                    $('#loaderModal').modal();

                    //console.log(data);
                    $.ajax({
                        url: `{{route('designer.product.editImages')}}`,
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function (result) {
                            console.log(result);
                            if (result.status == 0) {
                                $('#loaderModal').modal('hide');
                                swal('Updated Successfully', '', 'success');
                                $('.p').hide();
                                $('#p3').show();
                            } else {
                                $('#loaderModal').modal('hide');
                                swal('Unexpected error occurred', '', 'error');
                            }
                        },
                        error: function (e) {
                            $('#loaderModal').modal('hide');
                            console.log(e);
                            $.notify(e, 'error');
                        }
                    });

                } else {
                    $.notify('please upload at least one image', 'error');
                    $('#uplo').hide();
                }

            });*/


        $('.edit-product-images').on('click', function () {
            var a = '';

            function convert() {
                let img = document.querySelector(".displayMat");
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                // console.log(dataURL);//replace(/^data:image\/(png|jpg);base64,/, "");
                a = dataURL;
            }

            convert();
            var picx = '';

            imageArray = [];
            picturex = [];
            $('.valid-imgcassx').each(function () {
                var id = ($(this).attr('pictureId')) ? $(this).attr('pictureId') : '';
                var pix64 = $(this).attr('src');

                let img = document.querySelector(".valid-imgcassx");
                var canvas = document.createElement("canvas");
                canvas.width = img.width;
                canvas.height = img.height;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(img, 0, 0);
                var dataURL = canvas.toDataURL("image/png");
                //console.log("check: " + dataURL);

                imageArray.push({
                    'id': id,
                    'picture': pix64.indexOf('data:image') > 1 ? pix64 : dataURL,
                    'pictureName': 'A'
                })
            });
            console.log(imageArray);

            for (i = 0; i < imageArray.length; i++) {
                if (!validateUrl(imageArray[i].picture)) {
                    picturex.push(imageArray[i]);
                    console.log(picturex);
                }
            }

            console.log(imageArray);
            var data = {
                "id": "{{$product->id}}",
                "productAttributes": [{
                    "productPictureDTOS": imageArray,
                    "colourName": "A",
                    "colourPicture": a
                }]
            };

            myList = [];
            $('.removeItems').each(function () {
                myList.push({
                    "name": $(this).find('.sMeasure').html(),
                    "stockNo": $(this).find('.sStock').html()
                })
            });

            data.productAttributes[0].productSizes = myList;
            // $('#loaderModal').modal();
            console.log(data);

            $.ajax({
                url: `{{route('designer.product.editImages')}}`,

                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    console.log(result);
                    if (result.status == 0) {
                        $('#loaderModal').modal('hide');
                        console.log('hello');
                        swal('Updated Successfully', '', 'success');
                        $('.p').hide();
                        $('#p3').show();


                    } else {
                        $('#loaderModal').modal('hide');
                        swal('Unexpected error occurred', '', 'error');
                    }
                },
                error: function (e) {
                    $('#loaderModal').modal('hide');
                    console.log(e);
                    $.notify(e, 'error');
                }
            });

            /* }*/
        });

        $(document).on('click', '.rem', function () {
            var target = $(this).attr('tag');
            $(target).remove();
        });


        $('.edit-plus').on('click', function () {
            var html = `<input type="hidden"  class="colorPicker" value="#ff6161">`;

            $(this).before(html);
        });

        $('.edit-product-info').on('click', function () {
                var priceFlag = false;
                var stockFlag = false;

                var measures = JSON.stringify(myMeasure.getSelected()) ? '' : '[]';
                var data = {
                    "name": $('#productName').val(),
                    "amount": $('#productPrice').val(),
                    "slashedPrice": $('#productDiscount').val(),
                    "prodSummary": $('#productSummary').val(),
                    "description": $('#productDescription').val(),
                    "percentageDiscount": $('#productEditPercent').val(),
                    "color": getAll('.colorPicker'),
                    "mandatoryMeasurements": JSON.stringify(measures),
                    "styleId": $('#style').val(),
                    "materialPrice": $('#materialPrice').val(),
                    "materialName": $('#materialName').val(),
                    "subCategoryId": $('#subcat').val(),
                    "inStock": $('[name=availability]:checked').val(),
                    "id": '{{$product->id}}',
                    "productType": $("input[name='producttype']:checked").data('name')
                }

                console.log(data);

                if (data.inStock === 'Y') {
                    if ($(".sizeinput").length > 0) {
                        $.each($(".stock"), function () {
                            console.log("StocNo => " + $(this).val());
                            if (typeof $(this).val() === 'undefined' || $(this).val() == '') {
                                stockFlag = true;
                            }
                        });

                        if (!stockFlag) {
                            var productSizes = [];
                            totalStock = parseInt(0);

                            $.each($(".size .input-group"), function () {
                                productSizes.push({
                                    "name": $(this).children('.sizeinput').val(),
                                    "stockNo": $(this).children('.stock').val()
                                });

                                //console.log(productSizes);
                                totalStock += parseInt($(this).children('.stock').val());
                            });

                            data.productSizes = productSizes;
                            console.log(totalStock);
                            stockFlag = false;
                            data.stockNo = totalStock;
                        } else {
                            $.notify('All sizes must have stock');
                        }
                    } else {
                        stockFlag = true;
                        $.notify("You must select a size");
                    }

                    if ($('#customsizetoggle:checked').length > 0) {
                        data.acceptCustomSizes = 'Y';
                        if ($("#stock-time").val() === '' || parseInt($("#stock-time").val() <= 0)) {
                            stockFlag = true;
                            $("#stock-time").notify("Can't be less than 0");
                        } else {
                            data.numOfDaysToComplete = $("#stock-time").val();
                            if (data.numOfDaysToComplete === '' || parseInt(data.numOfDaysToComplete) <= 0) {
                                stockFlag = true;
                                $.notify("Delivery time Can't be less than 0");
                                $("#stock-time").notify("Can't be less than 0");
                            }
                        }
                    } else {
                        data.acceptCustomSizes = 'N';
                        data.numOfDaysToComplete = null;
                    }

                    data.availability = 'Y';
                } else {
                    data.availability = 'N';
                    data.acceptCustomSizes = 'N';
                    data.stockNo = 0;
                    data.productSizes = [];
                    data.numOfDaysToComplete = $('#time').val();
                    if (data.numOfDaysToComplete === '' || parseInt(data.numOfDaysToComplete) <= 0) {
                        stockFlag = true;
                        $.notify("Delivery time Can't be less than 0");
                        $("#time").notify("Can't be less than 0");
                    }
                }

                if (activeDiscount === 'price') {
                    if (typeof data.slashedPrice !== 'undefined' && data.slashedPrice !== '') {
                        if (parseFloat(data.slashedPrice) > parseFloat(data.amount)) {
                            priceFlag = true;
                            $.notify("Discount can't be greater than actual price");
                            $('.mySpinner').removeClass('lds-dual-ring');
                        }
                    }
                    data.percentageDiscount = '';
                } else if (activeDiscount === 'percentage') {
                    if (typeof data.percentageDiscount !== 'undefined' && data.percentageDiscount !== '') {
                        if (parseFloat(data.percentageDiscount) <= 0 || parseFloat(data.percentageDiscount) > 100) {
                            priceFlag = true;
                            $.notify("Percentage must be between 1 and 100");
                            $('.mySpinner').removeClass('lds-dual-ring');
                        }
                    }
                    data.slashedPrice = '';
                }

                // console.log(priceFlag+' '+stockFlag);
                if (priceFlag || stockFlag) {
                    //console.log(data);
                } else {
                    // $('#loaderModal').modal();
                    $.ajax({
                        url: `{{route('designer.product.edit')}}`,
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(data),
                        success: function (result) {
                            if (result.status == 0) {
                                console.log('tttt');
                                $('#loaderModal').modal('hide');
                                $('.p').hide();
                                $('#p2').show();
                                swal('Updated Successfully', '', 'success');
                            } else {
                                $('#loaderModal').modal('hide');
                                swal(result.message, '', 'error');
                                //console.log('cant submit')
                            }
                        },
                        error: function (e) {
                            $('#loaderModal').modal('hide');
                            $.notify(e, 'error');
                        }
                    });
                }

            }
        );


        $('.control').on('click', function () {
            var target = $(this).attr('data');
            $('.p').hide();
            console.log(target);
            $(target).show();
        });


        $(document).on('click', '.suggest', function () {

            var tag = $(this).text();

            $('#styleHolder').toggle();
            var tag = `<div id='tag${tagId}' class="chip">
                      <span class='tagValue'>${tag}</span>
                <i tag='#tag${tagId}' class="close fa fa-times rem"></i>
              </div>`;
            $('#styleBox').before(tag);

        });
    });


    $('#artwork1').on('change', function () {
        convertToBase64(this, '#img1');
    });
    $('#artwork2').on('change', function () {
        convertToBase64(this, '#img2');
    });
    $('#artwork3').on('change', function () {
        convertToBase64(this, '#img3');
    });
    $('#artwork4').on('change', function () {
        convertToBase64(this, '#img4');
    });
    $('#artwork5').on('change', function () {
        convertToBase64(this, '#img5');
    });


    $('#mat1').on('change', async function () {
        var pix = [];
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            pix.push(await toBase64($(this).get(0).files[i]));
        }
        showMaterials(pix);
    });
    $('#productImg').on('change', async function () {
        var pix = [];
        $('#loader').show();

        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            pix.push(await toBase64($(this).get(0).files[i]));
        }
        showProductImages(pix);
        if ($('.pImg').length > 3) {
            $('#proImgLabel').hide();
            $('.pImg').each(function (index, obj) {
                if (index >= 4) {

                    $(this).parent().remove();
                }
            });
        }

    });

    function find(arr, string) {
        var result = [];

        for (i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().indexOf(string.toLowerCase()) > -1) {
                console.log("hey " + string);
                result.push(arr[i]);
            }
        }

        return result;
    }

    $(document).on('dblclick', '.minicolors', function () {
        $(this).remove();
    });

    function showMaterials(arr) {
        if (arr.length) {
            var material = '';
            for (i = 0; i < arr.length; i++) {
                material += `<label for='material${counter}' class="upload-mat">
               <div id=${counter} style='margin-top:10px;margin-bottom:10px' class="col-md-2">
                  <div class="upload-mat">
                    <img class='materialImg' src="${arr[i]}">
                  <i class='pull-right removeIMage' w-type='material' pix='#${counter}'>&times;</i>
                          <input  type='file' id='material${counter}' class='mater hide-input' />

                  </div>
                </div>
                </label>`;
                counter++;
            }
            $('.materialBox').append(material);
        }
    }

    function showProductImages(arr) {
        if (arr.length) {
            var productImage = '';
            for (i = 0; i < arr.length; i++) {
                productImage += `<label id=${counter} class="proImg">
                  <img class='pImg' src=${arr[i]}>
                  <i class='pull-right removeIMage' w-type='product' pix='#${counter}'>&times;</i>
               </label>`;
                counter++;
            }

            $('.arrange-proImg').append(productImage);
            $('#loader').hide();
            $('#uplo').show();
        }
    }


    $(document).on('click', '.removeIMage', function () {
        var pix = $(this).attr('pix');
        var type = $(this).siblings('img').attr('w-type');

        if (type == 'product') {
            var id = $(this).siblings('img').attr('pictureId');
            var url = '{{route('designer.product.removeProductImage')}}';
            removeImage(url, id);
        } else if (type == 'material') {
            var id = $(this).siblings('img').attr('matId');
            var url = '{{route('designer.product.removeMaterialImage')}}';
            removeImage(url, id);
        }
        $(pix).remove();

        if (parseInt($('.pImg').length) == 0) {
            $('.uplo').hide();
            console.log($('.pImg').length);
        }
        //api to delete image from db
        if ($('.pImg').length < 4) {
            $('#proImgLabel').show();
        }
    });

    $('[name=availability]').on('change', function () {
        if ($('[name=availability]:checked').val().toLowerCase() == 'y') {
            $('#sizes').show();
            $("#searchsize").show();
            $('#customsize').show();
            if ($('#customsizetoggle:checked').length > 0) {
                $("#stock-time").show();
            }
            $('#time').hide();

        } else {
            $('#sizes').hide();
            $("#searchsize").hide();
            $('#time').show();
            $("#stock-time").hide();
            $('#customsize').hide();
        }
    });

    $("#customsizetoggle").click(function () {
        if (this.checked) {
            $("#stock-time").show();
        } else {
            $("#stock-time").hide();
        }
    });

    $(document).on('click', '.add-product', function () {
        var product = getAllImagesSrc('.pImg');
        var art = getAllImagesSrc('.artworkImages');
        var mat = getAllImagesSrc('.materialImg');

        var data = {
            "name": $('#productName').val(),
            "amount": $('#productPrice').val(),

            "description": $('#productDescription').val(),
            "color": getAll('.colorPicker'),
            "sizes": [$('#size').val()],
            "styleId": $('#style').val(),
            "subCategoryId": $('#subcat').val(),
            "picture": product,
            "artWorkPicture": art,
            "materialPicture": mat,
            "status": 'A',

            "stockNo": $('#stockNo').val(),
            "inStock": $('[name=availability]:checked').val(),
        };

        //data = JSON.stringify(data);
        console.log(data);

        $.ajax({
            url: `{{route('designer.product.add')}}`,
            type: "POST",
            data: JSON.stringify(data),
            success: function (result) {
                console.log(result);
            },
            error: function (e) {
                console.log(e);
            }
        });

        //addProduct(data);
    })


</script>