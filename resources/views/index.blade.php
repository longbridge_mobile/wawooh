<!doctype html>
<html lang="en">
<head>
    <title>Designer &mdash; Open your online Store &mdash; Wawooh &mdash; Express you</title>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Shop Online for latest African fashion, trendy made in Nigeria clothes, work wear, party dresses,
          bridal dresses, Ankara fashion, tie-dye adire, jumpsuit, agbada, tuxedo, kaftans, tunic, athleisure wear,
          accessories, jewellery, shoes, bags and more from top fashion brands in Nigeria and across Africa at
          affordable rates, fast shipping, worldwide shipping and maximum satisfaction. Wawooh.">
    <meta name="keywords"
          content="All, top, categories, see, accessories, shop, black, top categories, see all, shop now, bag, latest fashion, new fashion
fashion show, online fashion, fashion style, fashion website, new fashion trend, fashion dresses, new fashion, latest fashion trend fro women,
online shopping fashion, fashion clothes, fashion stores, fashion wear, fall fashion, fashion sites, women fashion, men fashion, ankara, kids fashion,
shop here, tie-dye adire, tuxedo, kaftan, tunic, athleisure wear, worldwide, fast shipping, affordable, agbada">
    <meta property="og:url" content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:description"
          content="Shop Online for latest African fashion, trendy made in Nigeria clothes, work wear, party dresses,
          bridal dresses, Ankara fashion, tie-dye adire, jumpsuit, agbada, tuxedo, kaftans, tunic, athleisure wear,
          accessories, jewellery, shoes, bags and more from top fashion brands in Nigeria and across Africa at
          affordable rates, fast shipping, worldwide shipping and maximum satisfaction. Wawooh."/>
    <meta property="og:image" content="{{asset('img/logo-sm.png')}}"/>

    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/wawooh.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    {{--3rd Party Styles--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    @stack('styles')
    <style>
        .lds-dual-ring {
            display: inline-block;
            width: 18px;
            height: 18px;
            padding-left: 9px;
        }

        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 26px;
            height: 26px;
            margin: 1px;
            border-radius: 50%;
            border: 5px solid #cd9933;
            border-color: #cd9933 transparent #cd9933 transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }

        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .error {
            border: 1px solid red;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126735794-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-126735794-1');
    </script>
</head>
<body>
<a name="top" class="home"></a>
<button onclick="topFunction()" id="icon-top" title="Top">
    <i class="fa fa-fw fa-chevron-up"></i>
</button>
<section class="designerHome">
    <div class="rightContainer">
        <div class="cover-layer"></div>
        <div class="inner-content">
            <h3>Connect with buyers </h3>
            <h4>For your design</h4>
            <div class="infoCard">
                <div class="card">
                    <div class="front">
                        <h6>Hey! We Have Something For you</h6>
                        <span class="iconMes animated shake infinite">
                            <i class="openDefault fa fa-envelope fa-3x"></i>
                            <i class="open fa fa-envelope-open fa-3x"></i>
                        </span>
                    </div>
                    <div class="back">
                        <div class="content">
                            <h3>How it works</h3>
                            <ul>
                                <li>
                                    <span>Register</span>
                                    <img src="{{ asset('img/clipboard.png') }}" alt="">
                                </li>
                                <li>
                                    <span>Add Products</span>
                                    <img src="{{ asset('img/cloud.png') }}" alt="">
                                </li>
                                <li>
                                    <span>Earn</span>
                                    <img src="{{ asset('img/money.png') }}" alt="">
                                </li>
                            </ul>

                            <strong class="wow animated slideInUp" data-wow-delay="15s" data-wow-duration="1.5s">As
                                Simple As A,B,C</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="registerCorner">
        <div class="form-container">
            <div class="img-holder">
                <img src="{{ asset('img/logo.png') }}" alt="W">
            </div>

            <div class="login-form">
                <form action="" id="loginForm" style="margin-top: 8em;" autocomplete="off">
                    <div class="form-group">
                        <label for="email" class="label-action">Enter your email address</label>
                        <div class="input-group">
                            <input type="email" id="signEmail" class="form-control"
                                   placeholder="Enter your email address">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="label-action">Enter your password</label>
                        <div class="input-group">
                            <input type="password" id="signPassword" class="form-control"
                                   placeholder="Enter your password">
                            <span class="input-group-addon"><i toggle="#signPassword" class="fa fa-eye pwd-show"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="button" id="login" value="Authenticate Me" class="btn btn-login-wawooh signin">
                        <img src="{{ asset('img/AjaxLoader.gif') }}" class="l-image" alt="">

                        <a href="#" class="forgetPwd pull-right">Forgot Login Details?</a>
                    </div>
                    <hr>
                    <div class="form-group text-centerC">
                        <a href="{{route('forgotPassword')}}" class="">Forgot Password?</a>
                    </div>
                    <div class="regNotify">
                        <h3>Don't have an account? <a href="#" id="regFlip">Register Now</a></h3>
                    </div>
                </form>

                <form action="" id="forgotPassword" style="display: none; margin-top: 4em;">
                    <div class="form-group">
                        <label for="emailReset" class="label label-info">
                            Enter your registered Phone Number
                        </label>

                        <div class="input-group">
                            <input type="tel" class="form-control" id="emailReset"
                                   placeholder="Enter your phone number">
                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="button" value="Retrieve my Email" class="btn btn-login-wawooh retrive-email">
                        <img src="{{ asset('img/AjaxLoader.gif') }}" class="l-image" alt="">

                        <a href="#" class="forgetPwd pull-right">Forgot Password?</a>
                    </div>

                    <div class="regNotify" id="pwLogin">
                        <h3>Already have an already? <a href="" id="loginLink">Login Now</a></h3>
                    </div>
                </form>

                <form action="" id="regForm" style="display: none;">
                    {{--<div class="form-group">
                        <label for="email" class="label-action">Enter your Store Name</label>
                        <div class="input-group">
                            <input type="text" id="storeName" class="form-control reg"
                                   placeholder="We will love to address you by your Store Name">
                            <span class="input-group-addon"><i class="fa fa-home"></i></span>
                        </div>
                    </div>--}}
                    <div class="form-group">
                        <label for="phoneNo" class="label-action">Enter your phone number</label>
                        <div class="input-group">
                            <input type="tel" id="phoneNo" class="form-control reg"
                                   placeholder="Enter your phone number for activation">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="emailReg" class="label-action">Enter your email address</label>
                        <div class="input-group">
                            <input type="email" id="emailReg" class="form-control reg"
                                   placeholder="Let us have your email for updates">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="passwordReg" class="label-action">Enter your password</label>
                        <div class="input-group">
                            <input type="password" id="passwordReg" class="form-control reg"
                                   placeholder="Enter your password">
                            <span class="input-group-addon"><i toggle="#passwordReg" class="fa fa-eye pwdReg-show"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confpasswordReg" class="label-action">Confirm password</label>
                        <div class="input-group">
                            <input type="password" id="confpasswordReg" class="form-control reg"
                                   placeholder="Enter your password">
                            <span class="input-group-addon"><i toggle="#confpasswordReg" class="fa fa-eye pwdReg-show"></i></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="form-control reg"
                               id="terms" style="margin-left:0 !important;">
                        <label class="" for="terms">Tick to Indicate
                            that you have agreed to the terms presented
                            in the <a href="{{route('terms')}}" class="termsSign">Terms
                                and Conditions</a> agreement</label>

                    </div>
                    <div class="form-group">
                        <input type="button" id="register" value="Register Me" class="btn btn-login-wawooh">
                        <img src="{{ asset('img/AjaxLoader.gif') }}" class="l-image" alt="">
                        <small class="text-center pull-right" style="padding-top: 12px;">
                            <a style="color: #111;" href="#" id="loginReg">Login if already registered</a>
                        </small>
                    </div>

                    <hr>
                    <div class="regNotify">

                    </div>
                    {{--<div class="hr-divider">
                        <span>OR</span>
                        <h3>Sign up with your social account</h3>
                    </div>

                    <div class="quick-register">
                        <ul>
                            <li><a href="#" id="facebook-connect"> <span>Facebook</span></a></li>
                            <li><a href="#" id="google-connect"> <span>Google</span></a></li>
                        </ul>
                    </div>--}}
                </form>
            </div>

            <div class="footer">
                <span>&copy; <span id="year"></span> Wawooh HQ. All Right Reserved.</span>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                Please, Login to continue
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Okay</button>
            </div>
        </div>
    </div>
</div>
@include('scripts.designer.allScript')
@include('scripts.designer.designerLoginSignup')
</body>
</html>
