@extends('layouts.designer.designer')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css" rel="stylesheet">
    <style>
        .bespoke-form {
            padding: 10px;
        }

        .imageSelect {
            margin: 0 5px;
        }
    </style>
@endpush

@section('pageTitle','My Bespoke Image Bank')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin bespoke-bank animated fadeInUp">
            <div class="col-md-12">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                    Add sample bespoke images
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <form action="">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Upload Image</label>
                                                <div class="row imageSelect">
                                                    <div class="ogaEdit">
                                                        <label>
                                                            <img class="nonFabric imgcass imgDisplay"
                                                                 src="{{ asset('img/material1.jpg')}}"
                                                                 height="150" width="150">
                                                            <input accept="image/jpeg" multiple type='file'
                                                                   id='nonFabricType'
                                                                   class='hide-input mainFabricType'/>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="productGroup">Category</label>
                                                <select class="form-control reqCat" id="productGroup">
                                                    <option value="0" hidden>---Select Category--</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}">
                                                            {{$category->categoryName}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="subcat">Subcategory</label>
                                                <select class="form-control reqCat" id="subcat"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Do you want this image patented?</label>
                                                <div class="row">
                                                    <div class="col-lg-3 col-md-3">
                                                        <div class="pretty p-icon p-round">
                                                            <input class="form-check-input" type="radio" id="true"
                                                                   name="patent-type" value="true">
                                                            <div class="state p-warning-o">
                                                                <i class="icon fa fa-check"></i>
                                                                <label for="true"
                                                                       style="font-size: inherit !important;">Yes</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3">
                                                        <div class="pretty p-icon p-round">
                                                            <input class="form-check-input" type="radio"
                                                                   name="patent-type"
                                                                   id="false" value="false" checked>
                                                            <div class="state p-warning-o">
                                                                <i class="icon fa fa-check"></i>
                                                                <label for="false"
                                                                       style="font-size: inherit !important;">No</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group patent-message">
                                                <div class="yes-message" id="Yes" style="display: none;">
                                                    <blockquote>
                                                <span>
                                                    <strong style="text-transform: uppercase; font-size: 16px">Purchase patent</strong> <br>
                                                    Make payment to the account below <br>
                                                        Account Number: 0423345725 <br>
                                                        Account Name: Wawooh Integrated Services Ltd. <br>
                                                        Bank Name: Guarantee Trust Bank <br>
                                                        Total Amount Payable: N1,000.00
                                                    </span>
                                                    </blockquote>
                                                </div>
                                                <div class="false-message" id="No" style="display: block;">
                                                <span>
                                                    Note: Wawooh has right to publish this photo to other designers on the platform
                                                </span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="mpp">Minimum Production Price</label>
                                                <input type="number" class="form-control mmpp reqCat"
                                                       placeholder="Enter your price"
                                                       id="mpp">
                                            </div>

                                            <div class="form-group">
                                                <label for="expectedDate">Production Duration</label>
                                                <input type="number" class="form-control reqCat expDate"
                                                       placeholder="Enter Production Duration"
                                                       id="expectedDate">
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="selectImageTags">Keywords</label>
                                                        <input type="text" class="form-control selectImageTags"
                                                               id="selectImageTags" maxlength="2">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input id="upBut" type="button" value="Upload Image"
                                                   class="btn btn-wawooh submitImage">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                My Style Catalogue
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                         data-parent="#accordion">
                        <div class="card-body">
                            <div class="row">
                                @if(isset($styleBanks))
                                    @if(count($styleBanks))
                                        @foreach($styleBanks as $styleBank)
                                            <div class="col-md-3 animated fadeInUp mb-3">
                                                <div class="holder">
                                                    <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$styleBank->picture)}}">
                                                    <div class="bes-overlay">
                                                        <h4>Sub Category: {{$styleBank->subCategory}}</h4>
                                                        <span>
                                                                Keywords: #@foreach($styleBank->keyWords as $keyword)
                                                                {{($keyword .',')}}
                                                            @endforeach
                                                            </span>
                                                        <a data-fancybox
                                                           href="{{str_replace(['http','upload'],['https','upload/q_40'],$styleBank->picture)}}"><i
                                                                    class="fa fa-eye"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="col-md-4 offset-4 text-center animated fadeInDown">
                                            <label class="label label-warning">No Sample Images In your bank</label>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px 23px;">
                    <h5 class="modal-title" id="exampleModalLabel" style="font-size: 1.2em; color: #555">Important
                        Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding: 1.2em 1.5em; line-height: 30px;">
                    <div class="bespoke-message">
                        Kindly upload pictures of your previous and current BESPOKE STYLES that does not meet our
                        <a target="_new"
                           href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541368518/manuals/Wawooh_Style_Guide_-min.pdf">photo
                            guidelines</a> here.
                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 0">
                    <div class="form-group">
                        <div class="col-lg-12 col-md-12 pl-0">
                            <button type="button" class="btn btn-secondary" id="offdisplay" data-dismiss="modal">Okay.
                            </button>
                            {{--<div class="pretty p-icon p-square">
                                <input class="form-check-input offMe" type="checkbox" id="offdisplay"
                                       name="notificationOff" value="true">
                                <div class="state p-warning-o">
                                    <i class="icon fa fa-check"></i>
                                    <label for="offdisplay" style="font-size: inherit !important;">Do not show message
                                        again</label>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <script>
        $(document).ready(function () {

            $('#offdisplay').click(function () {
                setCookie('Notification', true, '30');
            });

            if (!getCookie('Notification')) {
                $('#exampleModal').modal({
                    show: true,
                    backdrop: 'static',
                    Keyboard: false
                });
            }

            // limitDigit(2,'.expDate');
            $('.expDate').keypress(function () {
                if (this.value.length > 1) {
                    return false;
                }
            });

            $('.mmpp').keypress(function () {
                if (this.value.length > 10) {
                    return false;
                }
            });
        })
        var delimiter1 = ',';
        var delimiter2 = ',';

        $('#selectImageTags').selectize({
            plugins: ['restore_on_backspace', 'remove_button'],
            delimiter: delimiter1,
            persist: false,
            onType: function (str) {
                if (str.slice(-1) === delimiter2) {
                    this.createItem(str.slice(0, -1));
                    this.removeItem(delimiter2)
                }
            },
            create: function (input) {
                return {
                    value: input,
                    text: input
                }
            }
        });

        $(document).on('change', '.mainFabricType', function () {
            var file = $(this).get(0).files[0];

            if (file.size / 1024 > 1200000) {
                $.notify("Cannot upload image size more than 500kb");
            } else {
                convertMoreToBase64(this, '.nonFabric');
                $(this).siblings('img').addClass('readBankImage');
                $(this).siblings('img').css('border', '0px solid red');

            }
        });

        $('#productGroup').on('change', function () {
            let id = $(this).val();
            let data = {
                id: id
            };
            $.ajax({
                url: "{{route('getSubCatbyCatId')}}",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (result) {
                    if (result.status === '00') {
                        var subCat = result.data;
                        var html = `<option value='0' hidden>Select</option>`;
                        if (subCat.length > 0) {
                            for (var i = 0; i < subCat.length; i++) {
                                html += `<option value=${subCat[i].id}>${subCat[i].subCategory}</option>`
                            }
                            $('#subcat').html(html);
                        } else {
                            $('#subcat').html(`<option value='' hidden>No Sub Category</option>`);
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })

        });

        $('.form-control').keypress(function () {
            $(this).removeClass('error');
        })

        $('.submitImage').on('click', function () {
            $('#upBut').val('Uploading In Progress...').attr('disabled', 'disabled');

            var validationCheck = [];
            $('.reqCat').each(function () {
                if ($(this).val() === '' || $(this).val() === '0') {
                    $(this).addClass('error');
                    validationCheck.push('error');
                } else {

                }
            });

            var imgCheck = [];
            if ($('.imgDisplay').hasClass('readBankImage')) {
                $('.imgDisplay').css('border', '0px solid red');
            } else {
                imgCheck.push('imageError');
                $('.imgDisplay').css('border', '1px solid red');
            }
            if (validationCheck.length > 0 || imgCheck.length > 0) {
                $(this).addClass('error');
                $('#upBut').val('Upload Image').removeAttr('disabled', 'disabled');

            } else {
                let data = {
                    "subCategoryId": parseInt($('#subcat').val()),
                    "picture": $('.readBankImage').attr('src'),
                    "keyWords": ($('#selectImageTags').val()).split(','),
                    "estimatedPrice": parseInt($('#mpp').val()),
                    "estimatedDeliveryTime": $('#expectedDate').val(),
                    "patented": JSON.parse($('[name=patent-type]:checked').val())
                };
                // console.log(data);
                var url = '{{route('designer.uploadBespokeImageToBank')}}';
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: url,
                    success: function (result) {
                        console.log(result);
                        if (result.status === '00') {
                            swal(result.message, '', 'success');
                            $('#upBut').val('Uploaded Successfully').attr('disabled', 'disabled');
                            window.location.href = "{!! route('designer.uploadImage', generateCorrectUrl(strtolower($storeName))) !!}"
                        } else {
                            swal(result.message, '', 'warning');
                            //  window.location.reload();
                            $('#upBut').val('Upload Image').removeAttr('disabled', 'disabled');
                        }
                    }
                });
            }

        });
    </script>
@endpush
