@extends('layouts.designer.designer')

@push('styles')

@endpush

@section('pageTitle', 'Available Manuals')


@section('content')
    <div class="content-wrapper" style="background: #ffffff;">
        <div class="row grid-margin">
            <div class="col-12">

                <div id="" class="" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541075228/manuals/VENDOR_FAQ.pdf">Vendor FAQ</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541075327/manuals/Vendor_OPs_Manual_v-1.0.pdf">Operation Manual</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541368518/manuals/Wawooh_Style_Guide_-min.pdf">Photo Guide</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection