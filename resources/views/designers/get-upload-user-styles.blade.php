@extends('layouts.designer.designer')

@push('styles')
    <style>
        .proImg > img {
            height: 150px !important;
        }
    </style>
@endpush


@section('pageTitle', 'User\'s Uploaded Styles Products')

@section('content')

    <div class="content-wrapper">
        <div class="row grid-margin">
            <div class="row">
                <div class="col-md-12 product-pics allproducts">
                    <div id="products">
                        @if(is_array($resp))
                            @if(count($resp))
                                <div class="row-holder">
                                    @foreach($resp as $res)
                                        <div class="col-md-3 effect mb-5">
                                            <div class="holder-container">
                                                <div class="img-height">
                                                    <a href="{{route('designer.uploadStyle.details', [str_slug($storeName), $res->userBespokeStyleUploadId])}}">
                                                        <img src="{{@$res->bespokePicturesDTO->viewOne}}">
                                                        <img class="under-img"
                                                             src="{{@$res->bespokePicturesDTO->viewTwo}}">
                                                    </a>
                                                </div>
                                                <span class="verify-status verified">VERIFIED</span>
                                                <div class="p-details">
                                                     <span class="p-name font-reduce display-type">
                                                         <a style="color: gray !important;"
                                                            href="{{route('designer.bespokeproduct.details', [str_slug($storeName), $res->userBespokeStyleUploadId])}}">{{@$res->styleName}}</a>
                                                     </span>
                                                </div>
                                                <div class="price">
                                                    <ul>
                                                        <li>
                                                            ₦<span>{{number_format(@$res->budget)}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        @else
                            <div class="row-holder">
                                <div class="col-md-5 col-md-offset-5">
                                    <label class="label label-warning">
                                        <span>No Styles Found</span>
                                    </label>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>

    <script>


    </script>


@endpush