<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/dash/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/perfect-scrollbar.min.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/fontawesome-stars.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/style.css') }}">
    <link rel="stylesheet" href="{{asset('css/utility.css')}}">
    {{--<link rel="stylesheet" href="{{asset('css/style.css')}}">--}}
    <link rel="stylesheet" href="{{ asset('css/wawooh.css') }}">
    <link rel="stylesheet" href="{{ asset('css/category.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/designerdash.css') }}">
    <link rel="stylesheet" href="{{asset('css/sellect.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap/rta.min.css') }}">

    {{--3rd Party Styles--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">
    <title>Bespoke Application</title>
</head>
<body>
<div class="col-md-8 bespoke-form animated fadeInUp">
    <h3>Fill the following</h3>
    <hr>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">What type of fashion pieces do you
                    make?</label>
            </div>
            <div class="col-md-12 label-styled error-notify-sel">
                <div class="pretty p-icon p-square p-tada">
                    <input value="Clothing" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="bespoke-type">Clothing</label>
                    </div>
                </div>

                <div class="pretty p-icon p-square p-tada">
                    <input value="Footwear" type="checkbox" name="bespoke-piece" id="bespoke-type   "/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="bespoke-type">Footwear</label>
                    </div>
                </div>

                <div class="pretty p-icon p-square p-tada">
                    <input value="Accessories" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="bespoke-type">Accessories</label>
                    </div>
                </div>

                <div class="pretty p-icon p-square p-tada">
                    <input value="Bag" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="bespoke-type">Bag</label>
                    </div>
                </div>

                <div class="pretty p-icon p-square p-tada">
                    <input value="Others" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="bespoke-type">Others</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">What category do you cater to?</label>
            </div>
            <div class="col-md-12 label-styled error-notify-sel">
                <div class="pretty p-icon p-square p-tada">
                    <input type="checkbox" class="form-control" value="Male" name="cater-cat"
                           id="cater-cat"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="cater-cat">Male</label>
                    </div>
                </div>

                <div class="pretty p-icon p-square p-tada">
                    <input type="checkbox" class="form-control" value="Female" name="cater-cat"
                           id="cater-cat"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="cater-cat">Female</label>
                    </div>
                </div>

                {{--<div class="pretty p-icon p-square">
                    <input type="checkbox" class="form-control" value="Unisex" name="cater-cat"
                           id="cater-cat"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="cater-cat">Unisex</label>
                    </div>
                </div>--}}

                <div class="pretty p-icon p-square p-tada">
                    <input type="checkbox" name="cater-cat" value="Kid" id="cater-cat"/>
                    <div class="state p-warning">
                        <i class="icon mdi mdi-check"></i>
                        <label for="cater-cat">Kids</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 13px;">
        <div class="col-md-5">
            <div class="form-group">
                <label>What is the size of your entire workforce?</label>
                <div class="col-md-12 label-styled error-notify-sel" id="error-notify">
                    <input type="text" class="form-control" placeholder="Enter the size of your workforce"
                           id="bes-size">
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="form-group">
                <label for="weekly-orders">How many orders can you have under processing at any point in
                    time?</label>
                {{-- <div class="col-md-6">
                     <select required aria-required="true" name="" id="weekly-orders"
                             class="form-control error-notify">
                         <option value="0" hidden>--Select--</option>
                         <option value="Daily">Daily</option>
                         <option value="Weekly">Weekly</option>
                         <option value="Monthly">Monthly</option>
                     </select>
                 </div>--}}
                <input required aria-required="true" type="number" id="noOfOrders"
                       placeholder="Quantity in number"
                       class="form-control error-notify">
            </div>
        </div>
    </div>

    <h3>Location/Shop Space Verification</h3>
    <hr>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="verify-method">Select Verification Method</label>
                <select required aria-required="true" name="verify" id="verify-method"
                        class="form-control error-notify">
                    <option value="0" hidden>--Select--</option>
                    <option value="Video Call">Video Call</option>
                    <option value="Physical Visit">Physical Visit</option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6 pv">
                    <div class="form-group">
                        <label for="visit-day">Select Day for Visitation</label>
                        <select class="form-control" name="" id="visit-day">
                            <option value="0" hidden>--Select Day--</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wed">Wednesday</option>
                            <option value="Thurs">Thursday</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6 vm" style="display: none;">
                    <div class="form-group">
                        <label for="visit-day-cal">Create an Appointment</label>
                        <input placeholder="Select Date" type="text" id="visit-day-cal"
                               class="form-control calendar">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="visit-time">Select Time</label>
                        <select class="form-control" name="visit-time" id="visit-time">
                            <option value="0" hidden>--Select--</option>
                            <option value="10:00am">10:00am</option>
                            <option value="11:00am">11:00am</option>
                            <option value="12noon">12noon</option>
                            <option value="1:00pm">1:00pm</option>
                            <option value="2:00pm">2:00pm</option>
                            <option value="3:00pm">3:00pm</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group vm" style="display: none; padding-top: 6px; padding-left: 8px;">
                <label>Select one of the following</label>
                <div class="row">
                    <div class="col-md-6 label-styled">
                        <div class="pretty p-icon p-round">
                            <input value="Skype" type="radio" id="video-detail" class="form-control"
                                   name="video-connect"/>
                            <div class="state p-warning">
                                <i class="icon mdi mdi-check"></i>
                                <label for="video-detail">Skype ID</label>
                            </div>
                        </div>

                        {{--<div class="pretty p-icon p-round">
                            <input value="Hangout" type="radio" id="video-detail" class="form-control"
                                   name="video-connect"/>
                            <div class="state p-warning">
                                <i class="icon mdi mdi-check"></i>
                                <label for="video-detail">Hangout ID</label>
                            </div>
                        </div>--}}

                        <div class="pretty p-icon p-round">
                            <input value="Whatsapp" type="radio" id="video-detail" class="form-control"
                                   name="video-connect"/>
                            <div class="state p-warning">
                                <i class="icon mdi mdi-check"></i>
                                <label for="video-detail">Whatsapp</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input id="connect-detail" type="text" class="form-control"
                                   placeholder="Enter your contact ID">
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group pv">
                <label for="location">Verify your location Address</label>
                <input required aria-required="true" class="form-control" name="location" id="location"
                       value="{{isset($designer->address) ? $designer->address : ''}}">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label><strong><i class="fa fa-hand-point-right"></i> Please not, you must have a good camera
                    and internet enabled phone. (Calls are only within work-hours 9am -
                    5pm weekdays)</strong>
            </label>
        </div>
    </div>


    {{--<h3>Provide your social media ID</h3>
    <hr>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text facebook-text"><i class="fa fa-facebook"></i></div>
                    </div>
                    <input aria-required="true" id="facebook-id" placeholder="Enter your facebook ID"
                           type="text"
                           class="form-control error-notify">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text website-text"><i class="fa fa-globe"></i></div>
                    </div>
                    <input required aria-required="true" id="website-id" placeholder="Enter your Website ID"
                           type="url"
                           pattern="^(http[s]?:\/\/)?([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2}(\/([-~%\.\(\)\w\d]*\/*)*(#[-\w\d]+)?)?$"
                           class="form-control error-notify">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text instagram-text"><i class="fa fa-instagram"></i></div>
                    </div>
                    <input required aria-required="true" id="instagram-id"
                           placeholder="Enter your Instagram ID" type="text"
                           class="form-control error-notify">
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text twitter-text"><i class="fa fa-twitter"></i></div>
                    </div>
                    <input required aria-required="true" id="twitter-id" placeholder="Enter your Twitter ID"
                           type="text"
                           class="form-control error-notify">
                </div>
            </div>
        </div>
    </div>--}}

    <div class="form-group text-centerC">
        <img src="{{ asset('img/AjaxLoader.gif') }}" class="f-load" alt="">
        <input type="button" value="Apply Now" id="apply_now" class="btn btn-bes-apply">
    </div>
</div>
</body>
</html>
