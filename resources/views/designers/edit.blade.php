@extends('layouts.designer.designer')

@push('styles')
    <link href="{{ asset('css/jquery.minicolors.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.css" rel="stylesheet">

    <style>
        .artWorkEditPrice {
            width: 112px;
        }

        .check-icon-2 {
            bottom: 73px !important;
        }

        .tristyle {
            border: 1px solid red !important;
        }

        .measureEdited {
            height: 150px;
            overflow: scroll;
        }

        .control {
            cursor: pointer;
        }

        .artworkImages, .artworkImages1, .materialImg, .materialImages {
            width: 110px;
            height: 120px;
            object-fit: cover;
            object-position: top center;
        }

        .button-disabled {
            pointer-events: none;
            color: #808080;
        }

        .edit-col {
            float: left;
        }

        #styleHolder {
            position: absolute;
            z-index: 1000;
            max-height: 100px;
            width: 100%;
            overflow: auto;
            color: white;
            background: black;
        }

        .input-group-prepend {
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .delete-size:hover {
            cursor: pointer;
        }

        .fabricAction {
            position: absolute;
            bottom: -21px;
            background: white;
            padding: 5px;
        }

        #searchres {
            background: #ffffff;
            box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.1);
        }

        .sizesearchres {
            padding: 0.5rem 1rem;
            border-bottom: 0.5px solid rgba(0, 0, 0, 0.1);
            margin-bottom: 0;
        }

        .sizesearchres:hover {
            cursor: pointer;
            background: #f8f8f8;
        }

        .sizeE, .stockNo, .editT, .measureEdited, .matColorImage {
            margin-top: 9px;
        }

        .size {
            margin-bottom: 1rem;
        }

        .removeIMage, .removeArtwork {
            color: Red;
            cursor: pointer
        }

        .minicolors-swatch-color {
            margin: 2px 4px !important;

        }

        .mains {
            width: 40%;
        }

        .selectedSize ul {
            list-style: none;
            padding-left: 0;
            overflow: hidden;
        }

        .selectedSize li {
            float: left;
            padding: 5px 10px 5px;
            border: 1px solid #111;
        }

        .selectedSize li:nth-child(2) {
            margin-left: 20%;
        }

        .selectedSize li:nth-child(3) {
            margin-left: 25%;
        }
    </style>

@endpush

@section('pageTitle', 'Edit Product | ' . $product->name)
@section('content')

    <div class="content-wrapper new-prod">
        <div class="row grid-margin">
            <div class="col-lg-12 wow animated fadeInDown" data-wow-delay=".6s">
                <div class="alert alert-warning">
                    <strong><i class="fa fa-bell-o"></i> Important Information, your product goes offline immediately
                        you make a request for update until approved</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div id="accordion" class="product-edit">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne">
                                        Product Information and Category
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="">
                                                <label class="for-label">Product Grouping</label>
                                                <div class="in-stock">
                                                    <div class="pretty p-icon p-round p-smooth">
                                                        <input value='1' type="radio" name='producttype'
                                                               class="form-check-input noLeft"
                                                               data-name="1"
                                                               id="outfit" {{$product->productType == 1 ? "checked" : " "}}>
                                                        <div class="state p-warning-o">
                                                            <i class="icon fa fa-check"></i>
                                                            <label class="form-check-label"
                                                                   for="outfit">Clothings</label>
                                                        </div>
                                                    </div>
                                                    <div class="pretty p-icon p-round p-smooth">
                                                        <input value='2' type="radio" name='producttype'
                                                               class="form-check-input noLeft"
                                                               data-name="2"
                                                               id="accessories" {{$product->productType == 2 ? "checked" : " "}}>
                                                        <div class="state p-warning-o">
                                                            <i class="icon fa fa-check"></i>
                                                            <label class="form-check-label" for="accessories">Accessories</label>
                                                        </div>
                                                    </div>
                                                    <div class="pretty p-icon p-round p-smooth">
                                                        <input value='3' type="radio" name='producttype'
                                                               class="form-check-input noLeft"
                                                               data-name="3"
                                                               id="shoes" {{$product->productType == 3 ? "checked" : " "}}>
                                                        <div class="state p-warning-o">
                                                            <i class="icon fa fa-check"></i>
                                                            <label class="form-check-label" for="shoes">Shoes</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12 mt-3">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="for-label" for="productGroup">Category</label>
                                                        <select class="form-control required-product" id="productGroup">
                                                            @if(count($categories))
                                                                @foreach($categories as $category)
                                                                    <option @if($category->id == $product->categoryId ) selected
                                                                            @endif value='{{$category->id}}'>{{$category->categoryName}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="for-label" for="subcat">Sub Category</label>
                                                        <select class="form-control required-product" id="subcat">
                                                            <option value='null'>Select Sub Category</option>
                                                            @if(count($categories))
                                                                @foreach($categories as $category)

                                                                    @if($category->id == $product->categoryId )
                                                                        @if(count($category->subCategories))
                                                                            @foreach($category->subCategories as $subcategory)
                                                                                <option @if($subcategory->id == $product->subCategoryId ) selected
                                                                                        @endif value='{{$subcategory->id}}'>{{$subcategory->subCategory}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="for-label" for="style">Style</label>
                                                        <select class="form-control required-product" id="style">
                                                            <option value='null'>Select Style</option>
                                                            @if(count($categories))
                                                                @foreach($categories as $category)
                                                                    @if($category->id == $product->categoryId )
                                                                        @if(count($category->subCategories))
                                                                            @foreach($category->subCategories as $subcategory)
                                                                                @if($subcategory->id == $product->subCategoryId )
                                                                                    @if(count($subcategory->styles))
                                                                                        @foreach($subcategory->styles as $style)
                                                                                            <option @if($style->id == $product->styleId ) selected
                                                                                                    @endif value='{{$style->id}}'>{{$style->style}}</option>
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endif
                                                                            @endforeach
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            {{--    --}}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="for-label" for="productName">Product Name*</label>
                                                <input type="text" value='{{@$product->name}}'
                                                       class="form-control required-product"
                                                       id="productName"
                                                       placeholder="No More than 20 characters">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="for-label" for="productPrice">Product Cost*</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₦</span>
                                                    </div>
                                                    <input type="text" value='{{@$product->amount}}'
                                                           class="form-control required-product"
                                                           id="productPrice" placeholder="Update Product Price">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="for-label" for="productPrice">Sewing Price*</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">₦</span>
                                                    </div>
                                                    <input type="text" value='{{@$product->sewingPrice}}'
                                                           class="form-control required-product"
                                                           id="sewingPrice" placeholder="Update Sewing Price">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group discountEditGroup"
                                                 style="margin-bottom: 0 !important;">
                                                <label class="for-label" for="productPrice">Discounted Product
                                                    Price*</label>
                                                <input type="text" value="{{$product->slashedPrice}}"
                                                       class="form-control "
                                                       id="productDiscount"
                                                       placeholder="Input Discounted Price">
                                            </div>

                                            <div class="form-group hide percentGroup"
                                                 style="margin-bottom: 0 !important;">
                                                <label class="for-label" for="productPrice">Percentage Discount
                                                    Price*</label>
                                                <input type="text" value="{{$product->percentageDiscount}}"
                                                       class="form-control"
                                                       id="productEditPercent" placeholder="Input Percentage Discount">
                                            </div>
                                            <div id="selectEditPercentage"><label class="label label-warning">Select <i
                                                            class="fa fa-fw fa-percent" aria-hidden="true"></i></label>
                                            </div>
                                            <div id="selectEditDiscount" class="hide"><label
                                                        class="label label-rejected">Select
                                                    Discount</label></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="for-label" for="productDescription">Product
                                                    Summary*</label>
                                                <textarea class="form-control required-product"
                                                          value='{{$product->prodSummary}}'
                                                          id="productSummary"
                                                          placeholder="The summary of your product"
                                                          rows="6">{{$product->prodSummary}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="for-label" for="productDescription">Product
                                                    Description*</label>
                                                <textarea class="form-control required-product"
                                                          id="productDescription"
                                                          placeholder="Write full details of your product describing the basic information"
                                                          rows="6">{!! nl2br($product->description) !!}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                   {{-- <div class="row">
                                        <div class="col-md-12">
                                            <label class="for-label">Update Availability</label> <br>
                                            @if($product->availability === 'Y' && $product->acceptCustomSizes === 'N')
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y">
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @elseif($product->availability === 'N' && $product->acceptCustomSizes === 'Y')
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @elseif($product->availability === 'N' && $product->acceptCustomSizes == 'N')
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y">
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y">
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 mt-3">
                                            <div class="form-group">
                                                <label for="timeEdit" class="for-label">Update production time in
                                                    days for each bespoke order</label>
                                                <input placeholder='Enter delivery time in days'
                                                       class="discountField form-control timeEdit {{$product->acceptCustomSizes == 'Y' ?  '' : 'hide'}}"
                                                       style='width:50%;' type='number' id='timeEdit'
                                                       value="{{@$product->bespokeProductDTO->numOfDaysToComplete}}"/>
                                            </div>
                                        </div>
                                    </div>--}}
                                    {{--<div class="row mb-3 col-md-12">
                                        <div class="pretty p-icon p-square p-smooth">
                                            <input type="checkbox" class="form-check-input noLeft" id="exampleCheck1">
                                            <div class="state p-warning-o">
                                                <i class="icon fa fa-check"></i>
                                                <label class="form-check-label" for="exampleCheck1">
                                                    I agree that all details filled are
                                                    correct and all Description of the product is correct.
                                                </label>
                                            </div>
                                        </div>
                                    </div>--}}


                                    <div class="row buttons">
                                        <div class="col-md-12 pl-0">
                                            <button class="btn btn-product edit-product-info">Update & Continue</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                            data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                        Product Type and Images
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                 data-parent="#accordion">
                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="for-label">Update Availability</label> <br>
                                            @if($product->availability === 'Y' || $product->availability === null && $product->acceptCustomSizes === 'N')
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y">
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @elseif($product->availability === 'N' || $product->availability === null  && $product->acceptCustomSizes === 'Y')
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @elseif($product->availability === 'N' || $product->availability === null  && $product->acceptCustomSizes == 'N')
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y">
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y">
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="availability"
                                                           id="instocks"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="instocks">
                                                            Ready-To-Wear Orders Accepted</label>
                                                    </div>
                                                </div>
                                                <div class="pretty p-icon p-square p-smooth">
                                                    <input class="form-check-input" type="checkbox" name="acceptCustomSizes"
                                                           id="ondemands"
                                                           value="Y" checked>
                                                    <div class="state p-warning-o">
                                                        <i class="icon fa fa-check"></i>
                                                        <label class="form-check-label" for="ondemands">
                                                            Bespoke Orders Accepted</label>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 mt-3">
                                            <div class="form-group">
                                                <label for="timeEdit" class="for-label">Update production time in
                                                    days for each bespoke order</label>
                                                <input placeholder='Enter delivery time in days'
                                                       class="discountField form-control timeEdit {{$product->acceptCustomSizes == 'Y' ?  '' : 'hide'}}"
                                                       style='width:50%;' type='number' id='timeEdit'
                                                       value="{{@$product->bespokeProductDTO->numOfDaysToComplete}}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="editFabricTypeNew">
                                                <div class="div imageGroup">
                                                    @if(count($product->productColorStyleDTOS))
                                                        @php
                                                            $g = 1;
                                                            $y = 0;
                                                        @endphp
                                                        @for($i = 0; $i < sizeof($product->productColorStyleDTOS); $i++)
                                                            <div class="ola">
                                                                <div class="row rowGroup pp">
                                                                    <div class="col-md-12 mainEdit-{{$y}} imageToEdit">
                                                                        @for($j = 0; $j < sizeof($product->productColorStyleDTOS[$i]->productPictureDTOS); $j++)
                                                                            <div class="editEdit">
                                                                                <div hidden>{{$y++}}</div>
                                                                                <div class="editView"
                                                                                     style="float: left; margin-right:20px;">
                                                                                    <label for="matType-{{$y}}">
                                                                                        @php
                                                                                            $path = $product->productColorStyleDTOS[$i]->productPictureDTOS[$j]->picture;
                                                                                            $type = pathinfo($path, PATHINFO_EXTENSION);
                                                                                            $data = file_get_contents($path);
                                                                                            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                                                        @endphp
                                                                                        <div class="ogaEdit">
                                                                                            <img productId="{{$product->productColorStyleDTOS[$i]->productPictureDTOS[$j]->id}}"
                                                                                                 src="{{$base64}}"
                                                                                                 height="150"
                                                                                                 width="150"
                                                                                                 class="nonFabric editImagess imgDisplay sendImageEdit mainimage">
                                                                                            <input type="file"
                                                                                                   id="matType-{{$y}}"
                                                                                                   class="matType hide-input mainFabricType"/>
                                                                                        </div>
                                                                                        <div class="mainEditFabricAction">
                                                                                            <span><i class="pull-right fa fa-times deleteEditMainImage"></i></span>
                                                                                            {{--
                                                                                                <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                                                                                            --}}
                                                                                            {{-- <span><i class="pull-right fa fa-check mainEditCrop button-disabled"></i></span>
                                                                                             <span><i class="pull-right fa fa-plus addEditMainCrop"></i></span>--}}
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                            </div>
                                                                        @endfor
                                                                        @for($x = 0; $x < (5 - sizeof($product->productColorStyleDTOS[$i]->productPictureDTOS)); $x++)
                                                                            <div hidden>{{$g++}}</div>
                                                                            <div class=""
                                                                                 style="float: left; margin-right:20px;">
                                                                                <label for='nonFabricType-{{$g}}'
                                                                                       class="proImgx">
                                                                                    <div class="ogaEdit">
                                                                                        <img class="nonFabric imgcass imgDisplay"
                                                                                             src="{{ asset('img/material1.jpg')}}"
                                                                                             height="150" width="150">
                                                                                        <input multiple type='file'
                                                                                               id='nonFabricType-{{$g}}'
                                                                                               class='hide-input mainFabricType'/>
                                                                                    </div>
                                                                                    <div class="mainEditFabricAction"></div>
                                                                                </label>
                                                                            </div>
                                                                        @endfor
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-1 imageEditDisplay">
                                                                        @php
                                                                            $path = $product->productColorStyleDTOS[$i]->colourPicture;
                                                                            $type = pathinfo($path, PATHINFO_EXTENSION);
                                                                            $data = file_get_contents($path);
                                                                            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                                        @endphp
                                                                        <img src="{{$base64}}" height="40"
                                                                             class="imgD-{{$y}} imgCropEdit">
                                                                    </div>
                                                                    <div class="col-md-2 matColorImage"
                                                                         style="border-right: 1px solid rgba(0,0,0,.2);">
                                                                        <label for="matColor" class="for-label">Fabric
                                                                            Color
                                                                            Name:</label>
                                                                        <input type="text" id="matColor"
                                                                               class="form-control require-mat"
                                                                               value="{{$product->productColorStyleDTOS[$i]->colourName}}">
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="row" id="sizeReadyToWear">
                                                                            <div class="col-md-6 sizeE">
                                                                                <label for="fabricsize"
                                                                                       class="for-label">Select Stock
                                                                                    Sizes</label>
                                                                                {{--<input type="text" size="6"
                                                                                       class="form-control impt-control fabricsize">
                                                                                <div class="fabricsearch" data-result="#measureAttachment"></div>--}}
                                                                                <select name=""
                                                                                        class="form-control fabricsiz impt-control"
                                                                                        id="fabricsize">
                                                                                    @if(is_array($sizes))
                                                                                        @if(count($sizes))
                                                                                            @foreach ($sizes as $size)
                                                                                                <option value="{{$size->name}}">{{$size->name}}</option>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endif
                                                                                    @foreach($product->productColorStyleDTOS[$i]->productSizes as $sizes)
                                                                                        <option value="{{$sizes->name}}"
                                                                                                selected>{{$sizes->name}}</option>
                                                                                        @endforeach--}}
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-md-6 stockNo">
                                                                                <label for="fabricstock"
                                                                                       class="for-label">Stock
                                                                                    Count</label>
                                                                                <input type="number" size="4"
                                                                                       class="form-control impt-control fabricstock">
                                                                            </div>
                                                                            <div class="col-md-6 offset-3 editT">
                                                                                <button class="btn btn-product addEditFabricMeasurement">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    Add Stock
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 measureEdited">
                                                                        <label class="for-label">Product Stock</label>
                                                                        <table class="table table-stripped table-bordered measureTable">
                                                                            <tbody>
                                                                            @foreach($product->productColorStyleDTOS[$i]->productSizes as $sizes)
                                                                                <tr>
                                                                                    <td class="v-stoc-{{$y}} v-name">{{$sizes->name}}</td>
                                                                                    <td class="v-size-{{$y}} v-stock">{{$sizes->numberInStock}}</td>
                                                                                    <td class=""><i
                                                                                                class="fa fa-trash removeAttachedEditMeasurement"></i>
                                                                                    </td>
                                                                                </tr>
                                                                            @endforeach
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endfor
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="col-md-12 mb-3 mt-3" style="padding: 2px;">
                                            <strong>
                                                <small>To add a new set of fabric color options, click the "Add" button
                                                    below
                                                </small>
                                            </strong> <br>
                                            <button class="btn btn-product addEFabricType">Add</button>
                                        </div>
                                    </div>

                                    <div class="row col-md-12" style="margin-top: 15px; padding-left: 0;">
                                        <button class="btn btn-product edit-product-images">
                                            Update Images & Continue
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card productArtworksEd {{$product->acceptCustomSizes == 'Y' ?  '' : 'hide'}}">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                            data-target="#collapseThree" aria-expanded="false"
                                            aria-controls="collapseThree">
                                        Product Artwork(s)
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="label-margin">
                                                <label class="for-label">Add all available ArtWork*</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="margin-bottom: 10px;" class="artWorkPage">
                                        <div class="pretty p-icon p-square p-smooth">
                                            <input class="form-check-input selectAll" type="checkbox" name=""
                                                   id="checkall" value="" target='p3'>
                                            <div class="state p-warning-o">
                                                <i class="icon fa fa-check"></i>
                                                <label class="form-check-label" for="checkall">
                                                    Check all</label>
                                            </div>
                                        </div>
                                        {{--<button target='p4' class="btn btn-product selectAll">Select All</button>--}}
                                        <button target='p4' class="label label-rejected deleteImage"
                                                style="padding: 4px 8px; text-align: center;"><i
                                                    class="fa fa-trash"></i></button>
                                    </div>
                                    <div class="row upload-art art-col">
                                        @if(!is_null($product->bespokeProductDTO))
                                            @if(is_array($product->bespokeProductDTO->artPictureDTOS))
                                                @if(count($product->bespokeProductDTO->artPictureDTOS))
                                                    @foreach($product->bespokeProductDTO->artPictureDTOS as $artWorkPicture)
                                                        <div class="col-md-2">
                                                            <label for='artwork{{$loop->iteration}}' class="resize-art">
                                                                @php
                                                                    $path = $artWorkPicture->artWorkPicture;
                                                                    $type = pathinfo($path, PATHINFO_EXTENSION);
                                                                    $data = file_get_contents($path);
                                                                    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                                @endphp

                                                                <img class='artworkImages1 artworkImages artcheckreal'
                                                                     artworkId='{{$artWorkPicture->id}}'
                                                                     id='img{{$loop->iteration}}'
                                                                     src="{{$base64}}">
                                                                <input data-target='{{$artWorkPicture->id}}' type='file'
                                                                       id='artwork{{$loop->iteration}}'
                                                                       class='hide-input'/>
                                                                <div class="check-icon">
                                                                    <input id="artwork{{$loop->iteration}}"
                                                                           data-target='{{$artWorkPicture->id}}'
                                                                           name='checkArtwork' class='form-contro'
                                                                           type="checkbox"/>
                                                                </div>
                                                            </label>
                                                            <input type="number" placeholder="Price (₦)"
                                                                   class="form-control artWorkEditPrice"
                                                                   value="{{@$artWorkPicture->price}}">
                                                            <!-- <i class='pull-right removeArtwork' >&times;</i> -->
                                                        </div>
                                                    @endforeach

                                                    @php
                                                        $x = 5 - count($product->bespokeProductDTO->artPictureDTOS);
                                                    @endphp
                                                    @if($x)

                                                        @for($i = 5; $i > count($product->bespokeProductDTO->artPictureDTOS); $i--)
                                                            <div class="col-md-2">
                                                                <label for='artwork{{$i}}' class='resize-art'>

                                                                    <img class='artworkImages' id='img{{$i}}'
                                                                         src="{{ asset('img/landscape.png')}}">
                                                                    <input type='file' id='artwork{{$i}}'
                                                                           class='hide-input'/>
                                                                    <div>
                                                                        {{--  <input name='check' class='form-contro' type="checkbox" />     --}}
                                                                    </div>
                                                                </label>
                                                                <!-- <i class='pull-right removeArtwork' >&times;</i> -->

                                                            </div>
                                                        @endfor
                                                    @endif
                                                @else
                                                    @php
                                                        $x = 5 - count($product->bespokeProductDTO->artPictureDTOS);
                                                    @endphp
                                                    @if($x)

                                                        @for($i = 5; $i > count($product->bespokeProductDTO->artPictureDTOS); $i--)
                                                            <div class="col-md-2">
                                                                <label for='artwork{{$i}}' class='resize-art'>

                                                                    <img class='artworkImages' id='img{{$i}}'
                                                                         src="{{ asset('img/landscape.png')}}">
                                                                    <input type='file' id='artwork{{$i}}'
                                                                           class='hide-input'/>
                                                                    <div>
                                                                        {{--  <input name='checkArtwork' class='form-contro' type="checkbox" />     --}}
                                                                    </div>
                                                                </label>
                                                                <!-- <i class='pull-right removeArtwork' >&times;</i> -->
                                                            </div>
                                                        @endfor
                                                    @endif
                                                @endif
                                            @endif
                                        @else
                                            @for($i = 0; $i < 5; $i++)

                                                <div class="col-md-2">
                                                    <label for='artwork{{$i}}' class='resize-art'>

                                                        <img class='artworkImages' id='img{{$i}}'
                                                             src="{{ asset('img/landscape.png')}}">
                                                        <input type='file' id='artwork{{$i}}' class='hide-input'/>
                                                        <div>
                                                            {{--  <input name='checkArtwork' class='form-contro' type="checkbox" />     --}}
                                                        </div>
                                                    </label>
                                                {{--   <input type="text" placeholder="Name"
                                                          class="form-control materialEditPrice"> --}}
                                                {{-- <input type="number" placeholder="Price (₦)"
                                                        class="form-control materialEditPrice">--}}
                                                <!-- <i class='pull-right removeArtwork' >&times;</i> -->
                                                </div>
                                            @endfor
                                        @endif


                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-12 buttons pl-0">
                                            <button class="btn btn-product edit-artwork"
                                                    beSpokeId="{{@$product->bespokeProductDTO->id}}">Update &
                                                Continue
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card productFabricsEd {{$product->acceptCustomSizes == 'Y' ?  '' : 'hide'}}">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse"
                                            data-target="#collapseFour" aria-expanded="false"
                                            aria-controls="collapseFour">
                                        Product Fabric(s) & Required Measurements
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="label-margin">
                                            <label class="for-label">Add all available materials*</label>
                                        </div>
                                    </div>

                                    <div class="row materialHide">
                                        <div style="margin-bottom: 20px;">
                                            <div class="pretty p-icon p-square p-smooth">
                                                <input class="form-check-input selectAll" type="checkbox" name=""
                                                       id="checkall" value="" target='p4'>
                                                <div class="state p-warning-o">
                                                    <i class="icon fa fa-check"></i>
                                                    <label class="form-check-label" for="checkall">
                                                        Check all</label>
                                                </div>
                                            </div>
                                            {{--<button target='p4' class="btn btn-product selectAll">Select All</button>--}}
                                            <button target='p4' class="label label-rejected deleteImageFabric"
                                                    style="padding: 4px 8px; text-align: center;"><i
                                                        class="fa fa-trash"></i></button>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="row materialBox col-md-12 pl-0">
                                            <div class="col-md-2">
                                                <label for='mat1' class="upload-mat">
                                                    <img class='materialImages' src="{{ asset('img/landscape.png')}}">
                                                    <input multiple type='file' id='mat1' class='hide-input'/>
                                                </label>
                                            </div>
                                            @if(!is_null($product->bespokeProductDTO))
                                                @if(is_array($product->bespokeProductDTO->materialPicture))
                                                    @if(count($product->bespokeProductDTO->materialPicture))
                                                        @foreach($product->bespokeProductDTO->materialPicture as $materialPicture)
                                                            <div id="maty{{$loop->iteration}}"
                                                                 style='margin-bottom:10px'
                                                                 class="col-md-2 fabricEditing">
                                                                <label for='material{{$materialPicture->id}}'
                                                                       class="upload-mat">
                                                                    @php
                                                                        $path = $materialPicture->materialPicture;
                                                                        $type = pathinfo($path, PATHINFO_EXTENSION);
                                                                        $data = file_get_contents($path);
                                                                        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                                    @endphp
                                                                    <div class="upload-mat">
                                                                        <img class='materialImg matChecKImg'
                                                                             matId='{{$materialPicture->id}}'
                                                                             src="{{$base64}}">
                                                                        {{--  <i class='pull-right removeIMage' w-type='material' pix='#maty{{$loop->iteration}}'>&times;</i>  --}}
                                                                        <div class="check-icon-2">
                                                                            <div class="pretty p-icon p-square p-smooth">
                                                                                <input data-target='{{$materialPicture->id}}'
                                                                                       name='checkMaterial'
                                                                                       class='form-contro'
                                                                                       type="checkbox"/>
                                                                                <div class="state p-default-o">
                                                                                    <i class="icon fa fa-check"></i>
                                                                                    <label class="form-check-label"> </label>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <input type='file'
                                                                               id='material{{$materialPicture->id}}'
                                                                               class='mater hide-input'/>
                                                                    </div>
                                                                </label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">₦</span>
                                                                    </div>
                                                                    <input type="number" placeholder="Price (₦)"
                                                                           class="form-control materialEditPrice"
                                                                           value="{{@$materialPicture->materialPrice}}">
                                                                </div>

                                                                <input type="text" placeholder="Fabric Name"
                                                                       class="form-control materialEditName"
                                                                       value="{{@$materialPicture->materialName}}">
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row mt-5 col-md-12 pl-0">
                                        <div class="form-group">
                                            <label class="for-label" for="productSizes">Update Required
                                                Measurement*</label>
                                            <div class="col-md-12 pl-0">
                                                <label for="my-editMeasurement"></label>
                                                <input type="text" class="form-control" id="my-editMeasurement">
                                            </div>
                                        </div>

                                        {{-- <div class="form-group">
                                             <select multiple class="form-control selectMeasure">
                                                 @if(isset($reqM))
                                                     @if(count($reqM))
                                                         @foreach($reqM as $req)
                                                         @foreach($resM as $ress)

                                                             <option {{$ress->id === $req->id ? '' : 'selected' }}selected value="{{$ress->id}}">{{$ress->name}}</option>
                                                         @endforeach
                                                         @endforeach
                                                     @endif
                                                 @endif
                                             </select>
                                         </div>--}}
                                    </div>

                                    <div class="row col-md-12 pl-0">
                                        <div class="buttons">
                                            <div>
                                                <button class="btn btn-product publish ml-0"
                                                        beSpokeId="{{@$product->bespokeProductDTO->id}}">Update &
                                                    Publish
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="updateFabricModal" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel"
                                         aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true" class="art-close">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-artwork" id="updateFabric"></div>
                                                </div>
                                                <div class="modal-footer">
                                                    {{--   <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                                                       </button>--}}
                                                    <button type="button" class="btn btn-default modalSaveBtn"
                                                            data-dismiss="modal">Save
                                                        changes
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script src="{{ asset('js/plugins/jquery.initialize.min.js') }}"></script>
    <script src="{{ asset('js/plugins/notify.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/products.js') }}"></script>
    <script src="{{ asset('js/admin/jquery.richtext.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/4.0.0/cropper.min.js"></script>

    <script>
        $('#productDescription').richText();
        var editProductId = {{$product->id}};
        var newEditCount = $('.imageGroup').length;

        var matMore = 0;

        $('.addEFabricType').attr('count', newEditCount);

        var a = $('.addEFabricType').attr('count');

        $(document).on('click', '.deleteEditMainImage', function () {
            $(document).find('.mainFabricType').attr('disabled', true);
            var that = $(this);
            that.parents('.mainEditFabricAction').siblings('.ogaEdit').find('img').attr('src', '{{ asset('img/material1.jpg')}}');
            that.parents('.mainEditFabricAction').siblings('.ogaEdit').find('img').removeClass('sendImageEdit');
            $(document).find('.mainFabricType').attr('disabled', false);
        });

        $('.addEFabricType').click(function () {
            a++;
            matMore++;

            if (a === 5) {
                $(this).attr('disabled', true);
            }

            $('.editFabricTypeNew').append(`
            <hr>
            <div class="div imageGroup">
                <div class="row rowGroup ola">
                    <div class="deleteCont"><i class="fa fa-trash removeMoreEditContainer"></i></div>
                    <div class="col-md-12 mainEdit-${matMore} imageToEdit">
                        <div class="editEdit">
                            <div class="editView" style="float: left; margin-right:20px;">
                                <label for="matTypeMoreMain-${matMore}">
                                    <div class="ogaEdit">
                                        <img productId="" src="{{ asset('img/material1.jpg')}}" height="150" width="150" class="nonFabric editImagess imgDisplay mainimage">
                                        <input type="file" id="matTypeMoreMain-${matMore}" class="matType hide-input mainFabricType"/>
                                    </div>
                                    <div class="mainEditFabricAction"></div>
                                 </label>
                            </div>
                        </div>

                         <div class="editEdit">
                            <div class="editView" style="float: left; margin-right:20px;">
                                <label for="matTypeMoreSide-${matMore}">
                                    <div class="ogaEdit">
                                        <img productId="" src="{{ asset('img/material1.jpg')}}" height="150" width="150" class="nonFabric editImagess imgDisplay">
                                        <input type="file" id="matTypeMoreSide-${matMore}" class="matType hide-input mainFabricType"/>
                                     </div>
                                     <div class="mainEditFabricAction"></div>
                                 </label>
                             </div>
                         </div>

                         <div class="editEdit">
                            <div class="editView" style="float: left; margin-right:20px;">
                                <label for="matTypeMoreBack-${matMore}">
                                     <div class="ogaEdit">
                                        <img productId="" src="{{ asset('img/material1.jpg')}}" height="150" width="150" class="nonFabric editImagess imgDisplay">
                                        <input type="file" id="matTypeMoreBack-${matMore}" class="matType hide-input mainFabricType"/>
                                     </div>
                                     <div class="mainEditFabricAction"></div>
                                 </label>
                             </div>
                        </div>

                         <div class="editEdit">
                            <div class="editView" style="float: left; margin-right:20px;">
                                <label for="matTypeMoreFront-${matMore}">
                                    <div class="ogaEdit">
                                        <img productId="" src="{{ asset('img/material1.jpg')}}" height="150" width="150" class="nonFabric editImagess imgDisplay">
                                        <input type="file" id="matTypeMoreFront-${matMore}" class="matType hide-input mainFabricType"/>
                                    </div>
                                    <div class="mainEditFabricAction"></div>
                                 </label>
                             </div>
                        </div>

                         <div class="editEdit">
                            <div class="editView" style="float: left; margin-right:20px;">
                                <label for="matTypeMoreTop-${matMore}">
                                    <div class="ogaEdit">
                                        <img productId="" src="{{ asset('img/material1.jpg')}}" height="150" width="150" class="nonFabric editImagess imgDisplay topFabric">
                                        <input type="file" id="matTypeMoreTop-${matMore}" class="matType hide-input mainFabricType"/>
                                   </div>
                                    <div class="mainEditFabricAction"></div>
                                 </label>
                             </div>
                        </div>
                    </div>
                    <div class = "row col-md-12">
                        <div class="col-md-1 imageEditDisplay">
                           <img src="{{ asset('img/material1.jpg')}}" height="40" class="imgD-${matMore} imgCropEdit">
                        </div>

                        <div class="col-md-2 matColorImage" style="border-right: 1px solid rgba(0,0,0,.2);">
                             <label for="matColor" class="for-label">Fabric Color Name:</label>
                                  <input type="text" id="matColor" placeholder = "Enter Fabric Color Name" class="form-control require-mat">
                         </div>

                         <div class ="col-md-4">
                            <div class = "row" id = "sizeReadyToWear">
                                <div class="col-md-6 sizeE">
                                    <label for="fabricsize" class="for-label">Select Stock Sizes</label>
                                    <input type="text" size="6" class="form-control impt-control fabricsize fabricsiz">
                                    <div class="fabricsearch" data-result="#measureAttachment"></div>
                                </div>
                                <div class="col-md-6 stockNo">
                                    <label for="fabricstock" class="for-label">Stock Count</label>
                                    <input type="number" size="4" class="form-control impt-control fabricstock">
                                </div>
                                <div class="col-md-6 offset-3 editT">
                                    <button class="btn btn-product addEditFabricMeasurement">
                                        <i class="fa fa-plus"></i>Add Stock
                                    </button>
                                </div>
                             </div>
                         </div>

                                <div class="col-md-5 measureEdited" style = "border:1px dashed rgba(0,0,0,.2);">
                                <label class="for-label">Product Stock</label>
                                <table class="table table-stripped table-bordered measureTable oo">
                                <tbody></tbody>
                                </table>
                                </div>
                                </div>
                                </div>
                                </div>`);
        });

        $(document).on('click', '.removeMoreEditContainer', function () {
            var that = $(this);
            that.parents('.imageGroup').remove();
        });

        $(document).on('change', '.mainFabricType', function () {
            var that = $(this);

            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;
                if (fileSize > 12000) {
                    $.notify("Cannot upload image size more than 1.2MB");
                } else {
                    /* $(this).closest('.ogaEdit').siblings('.mainEditFabricAction').html(`
                         <span><i class="pull-right fa fa-trash changeMainImage"></i></span>
                          <span><i class="pull-right fa fa-times restoreEditMainImage"></i></span>
                         <span><i class="pull-right fa fa-check mainEditCrop"></i></span>
                        <span><i class="pull-right fa fa-plus addEditMainCrop"></i></span>
                     `);*/
                    $(this).siblings('.nonFabric').addClass('sendImageEdit');
                    convertMoreToBase64(this, '.imgDisplay');

                    var imageSiblings = $(this).siblings('.nonFabric');

                    setTimeout(function () {
                        imageSiblings.cropper({
                            autoCropArea: 0.2,
                            cropBoxResizable: false,
                            zoomable: false
                        });
                    }, 500);

                    setTimeout(function () {
                        var img = imageSiblings.cropper('getCroppedCanvas').toDataURL();
                        imageSiblings.parents('.imageGroup').find('.imgCropEdit').attr('src', img);
                    }, 900);

                    setTimeout(function () {
                        imageSiblings.parents('.rowGroup').find('.displayMat').addClass('prodEditAttr');
                    }, 1000);

                    setTimeout(function () {
                        $('.nonFabric').cropper('destroy');
                    }, 1200);

                    //$('.nonFabric').removeClass('cropper-hidden');
                    $('.cropper-container').hide();


                }
            }
        });

        $(document).on('mouseover', '.sendImageEdit', function () {
            var that = $(this);

            that.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false,
                zoomable: false
            });
        });

        $(document).on('click', '.cropper-crop-box', function () {
            var that = $(this);
            var img = that.parents('.cropper-container').siblings('.sendImageEdit').cropper('getCroppedCanvas').toDataURL();
            that.parents('.imageGroup').find('.imgCropEdit').attr('src', img);

        });

        $(document).on('mouseleave', '.cropper-container', function () {
            $('.sendImageEdit').cropper('destroy');
            $('.mainFabricType').attr('disabled', false);
        });

        $(document).on('mouseenter', '.cropper-crop-box', function () {
            $('.mainFabricType').attr('disabled', true);

        });

        $(document).on('click', '.addEditMainCrop', function (e) {
            e.preventDefault();
            var that = $(this);
            that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('.cropper-container').toggle();
            that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('img').toggleClass('cropper-hidden');
            var $image = that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('.nonFabric');
            $image.cropper({
                autoCropArea: 0.2,
                cropBoxResizable: false
            });
            that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('input').prop('disabled', true);
            that.closest('span').prev().find('.mainEditCrop').removeClass('button-disabled');


        });

        $(document).on('click', '.mainEditCrop', function () {
            var that = $(this);
            var cropImage = that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('.sendImageEdit').cropper('getCroppedCanvas').toDataURL();
            var cropDiv = that.closest('.mainEditFabricAction').closest('.imageToEdit').siblings('.imageEditDisplay').find('img');
            cropDiv.attr('src', cropImage);
            $.notify("Material created", "", "success");
        });

        $(document).on('click', '.addEditFabricMeasurement', function () {
            var fabricParent = $(this).parents('.ola');
            selectedSizes = [];
            var measurement = fabricParent.find('.sizeE').find('.fabricsiz').val();
            var stock = fabricParent.find('.fabricstock').val();

            $.each(fabricParent.find('tr'), function () {
                selectedSizes.push($(this).find('.v-name').html().toUpperCase());
            });

            console.log(sizes);
            console.log(measurement);


            if (!sizes.includes(measurement)) {
                $.notify("Size does not exist");
                return false;
            }

            if (selectedSizes.includes(measurement)) {
                $.notify("Size has already been selected");
                return false;
            }

            if (parseInt(stock) < 0 || stock.trim() === "") {
                $.notify("Stock cannot be less than 0");
                return false;
            }

            var data = {
                measurement: measurement,
                stock: stock
            };


            showSizes(data, fabricParent);

            $(fabricParent).find('.fabricsize').val('');
            $(fabricParent).find('.fabricstock').val('');

        });

        //Adds size to selected sizes
        function showSizes(data, fabricParent) {
            var searchResBody = $(fabricParent).find('tbody');
            searchResBody.append('<tr>\n' +
                '                    <td class="v-name" >' + data.measurement + '</td>\n' +
                '                    <td class="v-stock" >' + data.stock + '</td>\n' +
                '                    <td><i class="fa fa-trash removeMoreAttachedMeasurement"></i></td>\n' +
                '                  </tr>');
            deleteClickListener();
        }

        $(document).on('click', '.restoreEditMainImage', function (e) {
            /*e.preventDefault();
           var that = $(this);
            that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('input').prop('disabled', false);
            that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('img').removeClass('cropper-hidden');
            that.closest('.mainEditFabricAction').siblings('.ogaEdit').find('.cropper-container').remove();
            $(this).addClass('button-disabled');*/
            $.each($('.mainEditFabricAction').siblings('.ogaEdit').find('.cropper-container'), function () {
                if ($(this).css('display').toLowerCase() !== 'none') {
                    $(this).toggle();
                }
                $('.mainEditFabricAction').siblings('.ogaEdit').find('.nonFabric').removeClass('cropper-hidden');
            });
            e.preventDefault();
        });

        var activeDiscount;

        var sizes = [];
        var selectedSizes = [];
        var keyPressed = false;

        @if(isset($product->productSizes))
        @foreach($product->productSizes as $productSize)
        selectedSizes.push('{{strtoupper($productSize->name)}}');
        @endforeach
        @endif

        $.ajax({
            url: '{{env('GET_BASE_URL')}}/fashion/size/getsizes',
            type: 'GET',
            success: function (res) {
                if (res.status = '00') {
                    tempSizes = res.data.data;
                    $.each(tempSizes, function () {
                        sizes.push(this.name.toUpperCase());
                    })
                }
            }
        });

        $.each(sizes.slice(0, 4), function () {
            $("#searchres").append('<p class="sizesearchres" >' + this + '</p>');
        });

        //When size search is focused
        $(document).on('focus', '.fabricsize', function () {
            selectedSizes = [];
            var fabricSize = $(this).val().toUpperCase();
            var fabricParent = $(this).closest('.imageGroup');
            var fabricSearch = fabricParent.find(".fabricsearch");


            $.each(fabricParent.find('tr'), function () {
                selectedSizes.push($(this).find('.selected-size').html());
            });

            fabricSearch.show();
            var currarr = $.grep(sizes, function (a) {
                return (a.includes(fabricSize) && !selectedSizes.includes(a));
            });

            fabricSearch.html('');
            $.each(currarr.slice(0, 4), function () {
                $(fabricSearch).append('<p class="sizesearchres" >' + this + '</p>');
            });
            fabricClickListeners(this, fabricSearch);
        });

        //Sets listener for size click
        function fabricClickListeners(fabricSize, fabricSearch) {
            $('.sizesearchres').click(function () {

                var size = $(this).html();
                $(fabricSize).val(size);
                $(fabricSearch).toggle();
            });
        }

        //Search for available sizes
        function searchRess(fabricParent) {

            selectedSizes = [];
            $.each(fabricParent.find('tr'), function () {
                selectedSizes.push($(this).find('.selected-size').html());
            });

            var currarr = $.grep(sizes, function (a) {
                return (a.includes($(fabricParent).find('.fabricsize').val().toUpperCase()) && !selectedSizes.includes(a));
            });

            $(fabricParent).find('.fabricsearch').html('');
            $.each(currarr.slice(0, 4), function () {
                $(fabricParent).find('.fabricsearch').append('<p class="sizesearchres" >' + this + '</p>');
            });

            fabricClickListeners($(fabricParent).find('.fabricsize'), $(fabricParent).find('.fabricsearch'));
        }

        function deleteClickListener() {
            $(".removeAttachedEditMeasurement").click(function () {
                parent = $(this).parents('tr');
                $(parent).remove();
            });
        }

        $(document).on('click', '.removeAttachedEditMeasurement', function () {
            parent = $(this).parents('tr');
            $(parent).remove();
        });

        $(document).on('click', '.removeMoreAttachedMeasurement', function () {
            parent = $(this).parents('tr');
            $(parent).remove();
        });

        //Handles Key press events for size search
        $(document).on('keydown', '.fabricsize', function () {
            keyPressed = true
        });

        $(document).on('keyup', '.fabricsize', function () {
            if (keyPressed) {
                searchRess($(this).closest('.imageGroup'));
                keyPressed = false;
            }
        });


        function sizeClickListener() {
            $(".sizesearchres").click(function () {
                var size = $(this).html();
                selectedSizes.push(size.toUpperCase());

                $("#sizes").append('<div class="size" >\n' +
                    '                                    <div class="input-group" >\n' +
                    '                                        <div class="input-group-prepend" >\n' +
                    '                                            <i class="fa fa-times mr-2 delete-size text-danger" ></i>\n' +
                    '                                        </div>\n' +
                    '                                        <input class="form-control sizeinput" type="text" readonly value="' + size + '" >\n' +
                    '                                        <input class="form-control stock" type="number" placeholder="Stock" >\n' +
                    '                                    </div>\n' +
                    '                                </div>');

                $("#searchres").html("");
                $("#searchsizeinput").val("");
                deleteClickListener();
            });
        }

        @if(strtoupper($product->inStock) == 'Y')
        $("#exampleCheck3").prop("checked", true);
        $('#sizes').show();
        $("#searchsize").show();
        $('#customsize').show();
        @if(strtoupper($product->acceptCustomSizes) == 'Y')
        $("#customsizetoggle").prop('checked', true);
        $("#stock-time").show();
        @endif
        $('#time').hide();
        @else
        $("#exampleCheck4").prop("checked", true);
        $('#sizes').hide();
        $("#searchsize").hide();
        $('#time').show();
        $("#stock-time").hide();
        $('#customsize').hide();
        @endif

        $(document).on('click', '#selectEditPercentage', function () {
            $('.discountEditGroup').toggle();
            $('.percentGroup').toggle();
            $(this).toggle();
            $('#selectEditDiscount').toggle();
            $.notify('you can only submit one value');
            activeDiscount = 'percentage'
        });

        $(document).on('click', '#selectEditDiscount', function () {
            $('.discountEditGroup').toggle();
            $('.percentGroup').toggle();
            $(this).toggle();
            $('#selectEditPercentage').toggle();
            activeDiscount = 'price'
        });

        myMeasure = sellect("#my-editMeasurement", {
            originList: [
                "ANKLE",
                "ARMHOLE",
                "BACKSHIRTLENGTH",
                "BICEPS",
                "BLOUSELENGTH",
                "BUBALENGTH",
                "BUST",
                "CROTCH",
                "ELBOWCIRCUMFERENCE",
                "FISTCIRCUMFERENCE",
                "FULLLENGTH",
                "HALFLENGTH",
                "HIPS",
                "KNEECIRCUMFERENCE",
                "KNEELENGTH",
                "LAPCIRCUMFERENCE",
                "LONGSLEEVE",
                "LEFTFOOT",
                "RIGHTFOOT",
                "NECK",
                "SENATORLENGTH",
                "SHIRTLENGTH",
                "SHORTSLEEVE",
                "SHORTLENGTH",
                "SHOULDERWIDTH",
                "THIGH",
                "TROUSERLENGTH",
                "TROUSERWAIST",
                "THREEQUARTERSLEEVELENGTH",
                "WAIST",
                "WRIST"
            ],
            // destinationList: []
            destinationList: [
                @if(isset($productMeasure))
                    @if(count($productMeasure))
                        @foreach($productMeasure as $measuress)
                        '{{$measuress}}',
                        @endforeach
                    @endif
                @endif
            ]
        });

        myMeasure.init();

        $(document).ready(function () {

            var a = [];

            @foreach($arraySize as $size)
            a.push('{{$size}}')
            @endforeach
            $('.fabricsiz').each(function () {
                for (let i = 0; i < a.length; i++) {
                    $(this).append(`<option>${a[i]}</option>`);
                }

            });

            //To update artwork images (if Bespoke)
            $('.edit-artwork').on('click', function () {
                var artWork = [];
                $('.artworkImages').each(function () {
                    if ($(this).is('.artcheckreal')) {
                        artWork.push($(this));
                    }
                });

                var artPrice = [];
                $('.artWorkEditPrice').each(function () {
                    if ($(this).val() === '') {
                        artPrice.push($(this));
                    }
                })


                $('#loaderModal').modal();
                var artwork = getAllImagesSrc('.artworkImages');

                var art = [];
                var delArt = [];

                if (artwork.length) {
                    $('.artcheckreal').each(function (i, obj) {
                        var id = ($(this).attr('artworkId')) ? $(this).attr('artworkId') : null;
                        var pix64 = $(this).attr('src');
                        var price = parseInt($(this).closest('label').siblings('.artWorkEditPrice').val());
                        /* if (!validateUrl(pix64)) {*/
                        art.push({
                            'id': id,
                            'artWorkPicture': pix64,
                            'price': price
                        });
                        //}
                    });

                    var data = {
                        "productId": "{{$product->id}}",
                        // "bespokeProductDTO": {
                            "id": $(this).attr('beSpokeId') ? $(this).attr('bespokeId') : null,
                            "artPictureDTOS": art,
                        // }
                    };
                    console.log(data);
                    $.ajax({
                        url: `{{route('designer.product.editArtwork')}}`,
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function (result) {
                            console.log(result);
                            if (result.status === '00') {
                                $('#loaderModal').modal('hide');
                                swal('Updated successfully', '', 'success');
                                $('#loaderModal').modal('hide');
                                window.location.reload();
                            } else {
                                swal(result.message, '', 'warning');
                                $('#loaderModal').modal('hide');
                            }
                        },
                        error: function (e) {
                            $('#loaderModal').modal('hide');
                            $.notify(e, 'error');
                        }
                    });
                }

                /* if (artWork.length === 0 || artPrice.length > 0) {
                     swal('Some field required', '', 'warning')
                 } else {

                 }*/

            });

            $('.productPic').on('change', function () {
                var siblingImg = $(this).siblings('img.pImg');
                convertToBase64(this, siblingImg);
            });
            //edit material
            //  var test = JSON.stringify(myMeasure.getSelected()) ? JSON.stringify(myMeasure.getSelected()) : '[]';
            function getMeasure() {
                return JSON.stringify(myMeasure.getSelected())
            }
            // let measurements = JSON.stringify(myMeasure.getSelected());
            // console.log(measurements);

            $('.publish').on('click', function () {
                if (/*$('.fabricEditing').length < 1 || */myMeasure.getSelected().length < 1) {
                    swal('Please, select measurement', '', 'warning');
                } else {
                    var mat = [];
                    $('.matChecKImg').each(function (i, obj) {
                        var id = ($(this).attr('matId')) ? $(this).attr('matId') : '';
                        var pix64 = $(this).attr('src');
                        var price = $(this).closest('label').siblings('.input-group').find('.materialEditPrice').val();
                        var name = $(this).closest('label').siblings('.materialEditName').val()
                        if (!validateUrl(pix64)) {
                            mat.push({
                                'id': id,
                                'materialPicture': pix64,
                                'materialName': name,
                                'materialPrice': price
                            });
                        }
                    });
                    var data = {
                        "productId":parseInt({{$product->id}}),
                        // "bespokeProductDTO": {
                            "id": parseInt($(this).attr('beSpokeId')),
                            "materialPicture": mat,
                            "mandatoryMeasurements": JSON.stringify(getMeasure()),
                            "keyWords": ["0","buo"],
                            "estimatedPrice": 2.50,
                            "numOfDaysToComplete": $('.timeEdit').val(),
                            "estimatedDeliveryTime": 0
                        // }
                    };
                    console.log(data);
                    $('#loaderModal').modal();
                    $.ajax({
                        url: `{{route('designer.product.editMaterial')}}`,
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function (result) {
                            console.log(result);
                            if(result.status === "00") {
                                // $.notify('Updated successfully', 'success');
                                swal('Updated successfully', '', 'success');
                            } else {
                                swal(result.message, '','error');
                            }
                            $('#loaderModal').modal('hide');
                            {{--window.location.href = '{{route('designer.product', str_slug(strtolower($storeName)))}}';--}}
                        },
                        error: function (e) {
                            $.notify(e, 'error');
                            $('#loaderModal').modal('hide');
                        }
                    });
                }
            });

            //change material
            $(document).on('change', '.mater', function () {
                var siblingImg = $(this).siblings('img.materialImg');
                convertToBase64(this, siblingImg);
            });
            //remove artwork image
            $('.removeArtwork').on('click', function () {

                var a = $(this).siblings('label').children('img.artworkImages').attr('artworkId');
                var url = '{{route('designer.product.removeArtWorkImage')}}';
                removeImage(url, a);

                $(this).siblings('label').children('img.artworkImages').attr('src', '{{ asset('img/landscape.png')}}');
            });
            //edit artwork


            $('#instocks').click(function () {
                if ($(this).is(':checked') && $('#ondemands').is(':checked')) {
                    $(this).prop('checked', true);
                    // $('.sizeE').show();
                    // $('.stockNo').show();
                    // $('.editT').show();
                    $('#sizeReadyToWear').show(400);
                    $('.measureEdited').show(400);
                    //$('#timeEdit').removeClass('hide');
                    //  $('.productFabricsEd').show();
                    //  $('.productArtworksEd').show();
                } else if ($(this).is(':checked')) {
                    $(this).prop('checked', true);
                    /*$('.sizeE').show();
                    $('.stockNo').show();
                    $('.editT').show();*/
                    $('#sizeReadyToWear').show(400);
                    $('.measureEdited').show(400);
                    // $('#timeEdit').addClass('hide');
                    // $('.productFabricsEd').hide();
                    //  $('.productArtworksEd').hide();
                } else {
                    $('#sizeReadyToWear').hide(300);
                    $('.measureEdited').hide(300);
                    // $(this).prop('checked', true);
                    /*$('.sizeE').hide();
                    $('.stockNo').hide();
                    $('.editT').hide();
                    $('.measureEdited').hide();*/
                    //  $('#timeEdit').addClass('hide');
                    //  $('.productFabricsEd').hide();
                    //$('.productArtworksEd').hide();
                }
            });

            $('#ondemands').click(function () {
                if ($(this).is(':checked') && $('#instocks').is(':checked')) {
                    //$('.sizeE').show();
                    // $('.stockNo').show();
                    // $('.editT').show();
                    // $('.measureEdited').show();
                    $('#timeEdit').removeClass('hide');
                    $('.productFabricsEd').show();
                    $('.productArtworksEd').show();
                } else if ($(this).is(':checked')) {
                    /*$('.sizeE').hide();
                    $('.stockNo').hide();
                    $('.editT').hide();*/
                    // $('.measureEdited').hide();
                    $('#timeEdit').removeClass('hide');
                    $('.productArtworksEd').show();
                    $('.productFabricsEd').show();
                } else {
                    // $('.sizeE').hide();
                    // $('.stockNo').hide();
                    // $('.editT').hide();
                    // $('.measureEdited').hide();
                    $('#timeEdit').addClass('hide');
                    $('.productArtworksEd').hide();
                    $('.productFabricsEd').hide();
                }
            });

            $(document).on('click', '.deleteImageFabric', function () {
                var target = $(this).attr('target');
                if (target === 'p4') {
                    if ($('input[name=checkMaterial]:checked').length > 0) {
                        var answer = confirm('Are you sure you want to continue. Click ok to continue');
                        if (answer) {
                            var checked = $('input[name=checkMaterial]:checked');
                            var arr = [];
                            $(checked).each(function () {
                                arr.push($(this).attr('data-target'));
                            });
                            var data = {
                                "ids": arr,
                            }
                            $('#loaderModal').modal();
                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                url: '{{route('designer.product.removeFabricEditImage')}}',
                                success: function (result) {
                                    if (result.status === '00') {
                                        $('input[name=checkProduct]:checked').each(function () {
                                            var targ = $(this).attr('data-target');
                                            $('#' + targ).remove();
                                        });
                                        $.notify('Successfully deleted', 'error');
                                        swal('Successfully deleted', '', 'success');
                                        window.location.reload();
                                    } else {
                                        $.notify(result.message, 'success');
                                        window.location.reload();
                                    }
                                    $('#loaderModal').modal('hide');

                                },
                                error: function (e) {
                                    $('#loaderModal').modal('hide');
                                    $.notify(e, 'error');
                                },
                            });
                        }

                    } else {
                        $('#loaderModal').modal('hide');
                        alert('Select at least one to delete');
                    }
                }
            })

            //delete image
            $(document).on('click', '.deleteImage', function () {
                var target = $(this).attr('target');
                if (target === 'p4') {
                    if ($('input[name=checkArtwork]:checked').length > 0) {
                        var answer = confirm('Are you sure you want to continue. Click ok to continue');
                        if (answer) {
                            var checked = $('input[name=checkArtwork]:checked');
                            var arr = [];
                            $(checked).each(function () {
                                arr.push($(this).attr('data-target'));
                            });
                            var data = {
                                "ids": arr,
                            }
                            $('#loaderModal').modal();
                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                url: '{{route('designer.product.removeProductImage')}}',
                                success: function (result) {
                                    if (result.status === 0) {
                                        $('input[name=checkProduct]:checked').each(function () {
                                            var targ = $(this).attr('data-target');
                                            $('#' + targ).remove();

                                        });
                                        //$.notify('Successfully deleted', 'error');
                                        swal('Successfully deleted', '', 'success');
                                        window.location.reload();
                                    } else {
                                        $.notify(result.message, 'success');
                                        window.location.reload();
                                    }
                                    $('#loaderModal').modal('hide');

                                },
                                error: function (e) {
                                    $('#loaderModal').modal('hide');
                                    $.notify(e, 'error');
                                },
                            });
                        }
                    } else {
                        $('#loaderModal').modal('hide');
                        alert('Select at least one to delete');
                    }

                } else if (target == 'p3') {
                    if ($('input[name=checkArtwork]:checked').length > 0) {
                        var answer = confirm('Are you sure you want to continue. Click ok to continue');
                        if (answer) {
                            var checked = $('input[name=checkArtwork]:checked');
                            var arr = [];
                            $(checked).each(function () {
                                arr.push($(this).attr('data-target'));
                            });
                            var data = {
                                "ids": arr,
                            }

                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                url: '{{route('designer.product.removeArtWorkImage')}}',
                                success: function (result) {
                                    $('#loaderModal').modal('hide');
                                },
                                error: function (e) {
                                    $('#loader').hide();
                                    $.notify(e, 'error');
                                },
                            });
                        }
                    } else {
                        alert('Select at least one to delete');
                    }

                } else if (target == 'p4') {
                    if ($('input[name=checkMaterial]:checked').length > 0) {
                        var answer = confirm('Are you sure you want to continue. Click ok to continue');
                        if (answer) {
                            var checked = $('input[name=checkMaterial]:checked');
                            var arr = [];
                            $(checked).each(function () {
                                arr.push($(this).attr('data-target'));
                            });
                            var data = {
                                "ids": arr,
                            }
                            $.ajax({
                                type: 'POST',
                                data: JSON.stringify(data),
                                contentType: "application/json; charset=utf-8",
                                dataType: 'json',
                                url: '{{route('designer.product.removeMaterialImage')}}',
                                success: function (result) {
                                    $('#loader').hide();

                                },
                                error: function (e) {
                                    $('#loader').hide();
                                    $.notify(e, 'error');
                                },
                            });
                        }
                    } else {
                        alert('Select at least one to delete');
                    }

                }
            });

            $('.selectAll').on('click', function () {
                var target = $(this).attr('target');
                if (target == 'p2') {
                    if ($('input[name=checkProduct]:checked').length == $('input[name=checkProduct]').length) {
                        $('input[name=checkProduct]').prop('checked', false);
                        $(this).text('Select All');
                        $('.pImg').css({
                                filter: 'none'
                            }
                        );
                    } else {
                        $(this).text('Deselect');
                        $('input[name=checkProduct]').prop('checked', true);
                        $('.pImg').css({
                                filter: 'grayscale(100%)'
                            }
                        );

                    }
                } else if (target == 'p3') {
                    if ($('input[name=checkArtwork]:checked').length == $('input[name=checkArtwork]').length) {
                        $('input[name=checkArtwork]').prop('checked', false);
                        $(this).text('Select All');
                        $('.artworkImages1').css({
                                filter: 'none'
                            }
                        );
                    } else {
                        $(this).text('Uncheck all');
                        $('input[name=checkArtwork]').prop('checked', true);
                        $('.artworkImages1').css({
                                filter: 'grayscale(100%)'
                            }
                        );

                    }
                } else if (target == 'p4') {
                    if ($('input[name=checkMaterial]:checked').length == $('input[name=checkMaterial]').length) {
                        $('input[name=checkMaterial]').prop('checked', false);
                        $(this).text('Select All');
                        $('.materialImages').css({
                                filter: 'none'
                            }
                        );
                    } else {
                        $(this).text('Uncheck all');
                        $('input[name=checkMaterial]').prop('checked', true);
                        $('.materialImages').css({
                                filter: 'grayscale(100%)'
                            }
                        );

                    }
                }

            });

            counter = 0;
            $('.colorPicker').minicolors();
            $.initialize('.colorPicker', function () {
                $(this).minicolors();
            });
            var styles = [];
            tagId = 0;


            $('#productGroup').on('change', function () {
                var id = $(this).val();
                styles = [];
                var data = {
                    categoryId: id,
                    productType: $("input[name='producttype']:checked").data('name')
                }
                $.ajax({
                    url: `{{route('getSubCatBycat')}}`,
                    /*url: `/subcategory?id=${id}`,*/
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        var result = result.data;
                        var html = `<option value='' >Select</option>`;
                        if (result.length) {
                            $('#subcat').removeAttr('disabled');
                            for (i = 0; i < result.length; i++) {
                                html += `<option value='${result[i].id}'>${result[i].subCategory}</option>`;
                            }
                            $('#subcat').html(html);
                        } else {
                            $.notify('error', 'error');
                        }
                    },
                    error: function (e) {
                        $.notify(e, 'error');
                    }
                });
            });

            $('#subcat').on('change', function () {
                var id = $(this).val();
                styles = [];

                var data = {
                    id: id
                }

                $.ajax({
                    url: `{{route('getStyleBySubCat')}}`,
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        var result = result.data;
                        var html = `<option value='null'>Select</option>`;
                        if (result instanceof Array) {
                            for (i = 0; i < result.length; i++) {
                                html += `<option value='${result[i].id}'>${result[i].style}</option>`;
                            }
                        } else {
                            $.notify('error', 'error');
                        }
                        $('#style').html(html);
                    },
                    error: function (e) {
                        $.notify(e, 'error');
                    }
                });
            });
            //edit product images

            $('.edit-product-images').on('click', function () {
                var colorName = [];
                $('.require-mat').each(function () {
                    if ($(this).val() === '') {
                        colorName.push($(this));
                    }
                });

                var measure = [];
                if ($('.measureTable tbody tr').length < 1) {
                    measure.push('error');
                }

                var mainLength = [];
                $('.nonFabric').each(function () {
                    if ($(this).is('.sendImageEdit')) {
                        mainLength.push($(this));
                    }
                });


                var time = [];
                if ($('#ondemands').is(':checked')) {
                    if ($('.timeEdit').val() === '') {
                        time.push('time error')
                    } else {
                        time = [];
                    }
                } else {
                    time = [];
                }

                if (mainLength.length < 2) {
                    swal("At least two images are required, Main and any other", '', 'warning');
                } else if (colorName.length > 0 /*|| measure.length > 0 || time.length > 0*/) {
                    /*if (measure.length > 0) {
                        $('.measureTable').notify('Product size and stock must be added', 'error');
                    }*/
                    swal('Some field are required', '', 'warning');

                } else if (!($('[name=acceptCustomSizes]').is(':checked') || $('[name=availability]').is(':checked'))) {
                    swal('Select Product Availability','','warning');
                } else {
                    if (colorName.length > 0 /*|| measure.length > 0 || time.length > 0*/) {
                        swal('Some field are required', '', 'warning');
                    } else {
                        $('#loaderModal').modal();
                        editProductImage();
                    }
                }

            });

            function editProductImage() {
                var data = {
                    "id": "{{$product->id}}",
                    // inStock: $('#instocks').is(':checked') ? 'Y' : 'N',
                    "availability": $('#instocks').is(':checked') ? 'Y' : 'N',
                    "productColorStyleDTOS": [],
                    "acceptCustomSizes": $('#ondemands').is(':checked') ? 'Y' : 'N',
                    "bespokeProductDTO": {
                        "numOfDaysToComplete": parseInt($('.timeEdit').val()),
                    },
                };
                console.log(data);
                $.each($('.imageGroup'), function () {
                    var arrayPicture = [];
                    var sizes = [];

                    $(this).find('.sendImageEdit').each(function () {
                        arrayPicture.push($(this).attr('src'));
                    });

                    $(this).find('.measureEdited tr').each(function () {
                        sizes.push({
                            "name": $(this).find('.v-name').html(),
                            "numberInStock": $(this).find('.v-stock').html()
                        });
                    });

                    data.productColorStyleDTOS.push({
                        "colourName": $(this).find('.matColorImage #matColor').val(),
                        "colourPicture": $(this).find('.imageEditDisplay .imgCropEdit').attr('src'),
                        "picture": arrayPicture,
                        "productSizes": sizes

                    });
                });

                $.ajax({
                    url: `{{route('designer.product.editImages')}}`,
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.status === "00") {
                            $('#loaderModal').modal('hide');
                            swal('Updated Successfully', '', 'success');
                            window.location.reload();
                            $('.p').hide();
                            $('#p3').show();
                        } else {
                            $('#loaderModal').modal('hide');
                            swal('Unexpected error occurred', '', 'error');
                        }
                    },
                    error: function (e) {
                        $('#loaderModal').modal('hide');
                        $.notify(e, 'error');
                    }
                });
            }


            $(document).on('click', '.rem', function () {
                var target = $(this).attr('tag');
                $(target).remove();
            });

            $('.edit-plus').on('click', function () {
                var html = `<input type="hidden"  class="colorPicker" value="#ff6161">`;

                $(this).before(html);
            });
            var measures = JSON.stringify(myMeasure.getSelected()) ? JSON.stringify(myMeasure.getSelected()) : '[]';

            $('.required-product').keypress(function () {
                $(this).removeClass('tristyle');
            });


            $('.edit-product-info').on('click', function () {
                if ($('#productGroup').val() === "" || $('#subcat').val() === '' || $('#style').val() === '' || $('#productName').val() === '' || $('#productPrice').val() === '' || $('#productSummary').val() === '' || $('#productDescription').val() === '') {
                    swal('Some field required', '', 'warning');
                    $('.required-product').each(function () {
                        if ($(this).val() === '') {
                            $(this).addClass('tristyle');
                        } else {
                            $(this).removeClass('tristyle');
                        }
                    });

                    //  $('.required-product').addClass('tristyle');
                } else {

                    if (parseInt($('#productDiscount').val()) > parseInt($('#productPrice').val()) ) {
                        swal('Product Discount should not be greater than Product Price', '', 'warning');
                    }
                    else if (!($('[name=acceptCustomSizes]').is(':checked') || $('[name=availability]').is(':checked'))) {
                        swal('Select Product Availability','','warning');
                    } else {
                        $('#loaderModal').modal('show');
                        var priceFlag = false;
                        var stockFlag = false;
                        var data = {
                            "id": '{{$product->id}}',
                            "name": $('#productName').val(),
                            "productPriceDTO": {
                                "amount": $('#productPrice').val(),
                                "slashedPrice": $('#productDiscount').val(),
                                "percentageDiscount": $('#productEditPercent').val(),
                                "sewingAmount": $('#sewingPrice').val() ? $('#sewingPrice').val() : null
                            },
                            "prodSummary": $('#productSummary').val(),
                            "description": $('#productDescription').val(),
                            "numOfDaysToComplete": $('.timeEdit').val(),
                            // "mandatoryMeasurements": JSON.stringify(measurements),
                            "styleId": $('#style').val(),
                            /* "materialPrice": $('#materialPrice').val(),
                             "materialName": $('#materialName').val(),*/
                            "subCategoryId": $('#subcat').val(),
                            "availability": $('#instocks').is(':checked') ? 'Y' : 'N', /*$('[name=availability]:checked').val(),*/
                            "acceptCustomSizes": $('#ondemands').is(':checked') ? 'Y' : 'N',
                            "productType": $("input[name='producttype']:checked").data('name'),
                            "bespokeProductDTO": null
                        };

                        //  $('#loaderModal').modal();
                        console.log(data);
                        $.ajax({
                            url: `{{route('designer.product.edit')}}`,
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify(data),
                            success: function (result) {
                                if (result.status === "00") {
                                    $('#loaderModal').modal('hide');
                                    swal('Updated Successfully', '', 'success');
                                    $('#loaderModal').modal('hide');
                                } else {
                                    $('#loaderModal').modal('hide');
                                    swal(result.message, '', 'error');
                                }
                            },
                            error: function (e) {
                                $('#loaderModal').modal('hide');
                                $.notify(e, 'error');
                            }
                        });
                    }
                }
            });

            $('.control').on('click', function () {
                var target = $(this).attr('data');
                $('.p').hide();
                $(target).show();
            });

            $(document).on('click', '.suggest', function () {

                var tag = $(this).text();

                $('#styleHolder').toggle();
                var tag = `<div id='tag${tagId}' class="chip">
                      <span class='tagValue'>${tag}</span>
                <i tag='#tag${tagId}' class="close fa fa-times rem"></i>
              </div>`;
                $('#styleBox').before(tag);

            });
        });


        $('#artwork0').on('change', function () {
            convertToBase64(this, '#img0');
            $("#img0").addClass('artcheckreal');
            $(this).closest('.col-md-2').append(`

                  <input type="number" class="artWorkEditPrice" placeholder="Price (₦)">
                     `);
            $('.artWorkPage').removeClass('hide');
            //  $(this).siblings('img')
        });

        $('#artwork1').on('change', function () {
            convertToBase64(this, '#img1');
            $("#img1").addClass('artcheckreal');
            $(this).closest('.col-md-2').append(`

                  <input type="number" class="artWorkEditPrice" placeholder="Price (₦)">
                     `);
            $('.artWorkPage').removeClass('hide');
            //  $(this).siblings('img')
        });
        $('#artwork2').on('change', function () {
            convertToBase64(this, '#img2');
            $("#img2").addClass('artcheckreal');
            $(this).closest('.col-md-2').append(`

                  <input type="number" class="artWorkEditPrice" placeholder="Price (₦)">
                     `);
            $('.artWorkPage').removeClass('hide');
        });
        $('#artwork3').on('change', function () {
            convertToBase64(this, '#img3');
            $("#img3").addClass('artcheckreal');
            $(this).closest('.col-md-2').append(`
                  <input type="number" class="artWorkEditPrice" placeholder="Price (₦)">
                   `);
            $('.artWorkPage').removeClass('hide');
        });
        $('#artwork4').on('change', function () {
            convertToBase64(this, '#img4');
            $("#img4").addClass('artcheckreal');
            $(this).closest('.col-md-2').append(`
               <input type="number" class="artWorkEditPrice" placeholder="Price (₦)">
                   `)
            $('.artWorkPage').removeClass('hide');
        });
        $('#artwork5').on('change', function () {
            convertToBase64(this, '#img5');
            $("#img5").addClass('artcheckreal');
            $(this).closest('.col-md-2').append(`
                    <input type="number" class="artWorkEditPrice" placeholder="Price (₦)">
                     `);
            $('.artWorkPage').removeClass('hide');
        });

        $('#mat1').on('change', async function () {
            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                pix.push(await toBase64($(this).get(0).files[i]));
            }
            showMaterials(pix);
            $('.materialHide').removeClass('hide');

        });

        $('#productImg').on('change', async function () {
            var pix = [];
            $('#loader').show();

            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                pix.push(await toBase64($(this).get(0).files[i]));
            }
            showProductImages(pix);
            if ($('.pImg').length > 3) {
                $('#proImgLabel').hide();
                $('.pImg').each(function (index, obj) {
                    if (index >= 4) {

                        $(this).parent().remove();
                    }
                });
            }

        });

        function find(arr, string) {
            var result = [];

            for (i = 0; i < arr.length; i++) {
                if (arr[i].toLowerCase().indexOf(string.toLowerCase()) > -1) {
                    result.push(arr[i]);
                }
            }

            return result;
        }

        $(document).on('dblclick', '.minicolors', function () {
            $(this).remove();
        });

        function showMaterials(arr) {
            if (arr.length) {
                var material = '';
                for (i = 0; i < arr.length; i++) {
                    material += `<div class="fabricEditing"><label for='material${counter}' class="upload-mat">
               <div id=${counter} style='margin-top:10px;margin-bottom:10px' class="col-md-2">
                  <div class="upload-mat">
                    <img class='materialImg matChecKImg' src="${arr[i]}">
                  <i class='pull-right removeIMage' w-type='material' pix='#${counter}'>&times;</i>
                          <input  type='file' id='material${counter}' class='mater hide-input' />

                  </div>
                </div>
                </label>
              <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text">₦</span>
                     </div>
    <input type="number" placeholder="Price (₦)" class="form-control materialEditPrice">
                                       </div>
                                       <input type="text" placeholder="Fabric Name" class="form-control materialEditName"></div>



`;
                    counter++;
                }

                $('.materialBox').append(material);
                //$('#art-display').append(material);
            }
        }

        function showProductImages(arr) {
            if (arr.length) {
                var productImage = '';
                for (i = 0; i < arr.length; i++) {
                    productImage += `<label id=${counter} class="proImg">
                  <img class='pImg' src=${arr[i]}>
                  <i class='pull-right removeIMage' w-type='product' pix='#${counter}'>&times;</i>
               </label>`;
                    counter++;
                }

                $('.arrange-proImg').append(productImage);
                $('#loader').hide();
                $('#uplo').show();
            }
        }

        $(document).on('click', '.removeIMage', function () {
            var pix = $(this).attr('pix');
            var type = $(this).siblings('img').attr('w-type');

            if (type == 'product') {
                var id = $(this).siblings('img').attr('pictureId');
                var url = '{{route('designer.product.removeProductImage')}}';
                removeImage(url, id);
            } else if (type == 'material') {
                var id = $(this).siblings('img').attr('matId');
                var url = '{{route('designer.product.removeMaterialImage')}}';
                removeImage(url, id);
            }
            $(pix).remove();

            if (parseInt($('.pImg').length) == 0) {
                $('.uplo').hide();
            }
            //api to delete image from db
            if ($('.pImg').length < 4) {
                $('#proImgLabel').show();
            }
        });

        $('[name=availability]').on('change', function () {
            if ($('[name=availability]:checked').val().toLowerCase() == 'y') {
                $('#sizes').show();
                $("#searchsize").show();
                $('#customsize').show();
                if ($('#customsizetoggle:checked').length > 0) {
                    $("#stock-time").show();
                }
                $('#time').hide();

            } else {
                $('#sizes').hide();
                $("#searchsize").hide();
                $('#time').show();
                $("#stock-time").hide();
                $('#customsize').hide();
            }
        });

        $("#customsizetoggle").click(function () {
            if (this.checked) {
                $("#stock-time").show();
            } else {
                $("#stock-time").hide();
            }
        });

        $(document).on('click', '.add-product', function () {
            var product = getAllImagesSrc('.pImg');
            var art = getAllImagesSrc('.artworkImages');
            var mat = getAllImagesSrc('.materialImg');

            var data = {
                "name": $('#productName').val(),
                "amount": $('#productPrice').val(),

                "description": $('#productDescription').val(),
                "color": getAll('.colorPicker'),
                "sizes": [$('#size').val()],
                "styleId": $('#style').val(),
                "subCategoryId": $('#subcat').val(),
                "picture": product,
                "artWorkPicture": art,
                "materialPicture": mat,
                "status": 'A',

                "stockNo": $('#stockNo').val(),
                "inStock": $('[name=availability]:checked').val(),
            };
            //data = JSON.stringify(data);

            $.ajax({
                url: `{{route('designer.product.add')}}`,
                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    swal(result, '', 'success');
                },
                error: function (e) {
                    swal(e, '', 'error');
                }
            });

            //addProduct(data);
        })
    </script>

@endpush