@extends('layouts.designer.designer')

@push('styles')

   <style>
       .bespokeDiv div {
           margin: 30px 0;
       }

       #editButton {
           cursor: pointer;
       }
   </style>

@endpush


@section('pageTitle' | 'Bespoke Product Details')
@section('content')
   <div class="content-wrapper white">
       <div class="row grid-margin">
           <div class="col-12">
               <div class="row">
                   <div class="col-md-6 col-12">
                       @if(isset($data->bespokeFabricPictures))
                           <div style="width: 300px;">
                          <img style="width: 100%" id='mainPicture' src="{{$data->bespokeFabricPictures}}"
                               class="img-fluid fitDiv">
                           </div>
                           @elseif(isset($data->bespokeArtworkPictures))
                           <div style="width: 300px;">
                               <img style="width: 100%" id='mainPicture' src="{{$data->bespokeArtworkPictures}}"
                                    class="img-fluid fitDiv">
                           </div>
                           @else
                           <div>
                               <div>No fabric available</div>
                           </div>
                       @endif
                   </div>
                   <div class="col-md-6 col-12 bespokeDiv">

                       <div>
                           Product Name:  {{@$data->productName }}
                       </div>
                       <div>
                       Gender: {{@$data->gender}}
                       </div>
                       <div>
                           Fabric Available: {{$data->hasFabric ? 'Yes' : 'No'}}
                       </div>
                       @if(isset($data->lengthInYards))
                       <div>
                           Length in yard: {{$data->lengthInYards}}
                       </div>
                           @endif
                       <div>
                           Notes: {{$data->notes}}
                       </div>

                       <div>
                           @if($data->designerDecision === 'ND')
                                 Status: Waiting For Designer Decision
                           @endif
                           @if($data->designerDecision !== 'ND' && $data->userDecision === 'ND')
                                Status: Waiting For User Decision
                           @endif
                           @if($data->designerDecision !== 'ND' && $data->userDecision === 'ACCEPTED')
                               Status: Accepted by the User
                           @endif
                           @if($data->designerDecision !== 'ND' && $data->userDecision === 'REJECTED')
                               Status: Rejected by the User
                           @endif
                       </div>

                       <div id="divform" class="{{isset($data->fullCost) ? 'hide' : ''}}">
                           <div>
                               <label>Full Cost</label>
                               <input type="text" class="form-control" id="fullCost">
                           </div>
                           <div style="margin: 20px 0;">
                               <label>Notes</label>
                               <textarea id="notes" class="form-control"></textarea>
                           </div>
                       </div>

                       <div>
                           <button class="btn btn-wawooh action" attr="accept">Accept</button>
                           <button class="btn btn-danger action" attr="reject">Reject</button>
                           @if(isset($data->fullCost))
                           <span id="editButton">Edit</span>
                           @endif
                       </div>
                   </div>
               </div>

               <div>ArtWork Picture</div>
               <div class="row">
                   <div class="col-12">
                       @if(isset($data->bespokeArtworkPictures))
                       <div style="width: 100px;">
                           <img style="width: 100%;" src="{{$data->bespokeArtworkPictures}}">
                       </div>
                           @endif
                   </div>
               </div>
           </div>
       </div>
   </div>
@endsection

@push('scripts')
    <script>

        $('#editButton').click(function(){
            $('#divform').removeClass('hide');
            $("#fullCost").val({{$data->fullCost}});
            $("#notes").val(`{{$data->notes}}`);
        })

        var data = {
            bespokeRequestId: "{{$data->id}}"
        }

        $('.action').click(function(){
            console.log($(this).attr('attr'));

            var attribute = $(this).attr('attr');
            if(attribute === 'reject'){
              data.designerDecision = 'REJECTED';
              if(data.fullCost || data.notes){
                  data.fullCost = '';
                  data.notes = '';
              }

              console.log(data);
              makeQuote(data)
            } else if (attribute === 'accept'){
                data.designerDecision = 'ACCEPTED';
                data.fullCost = $('#fullCost').val();
                data.notes = $('#notes').val();
                console.log(data);
                makeQuote(data);
            }

        })


        function makeQuote(data){
            $.ajax({
                url: "{{route('makeQuote')}}",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    console.log(response);
                    if(response.status === '00'){
                        swal(response.message, "", "success");
                        location.href= "{{route('designer.bespokeRequest', str_slug(strtolower($storeName)))}}";
                    }else{
                        swal(response.message, "", "warning");

                    }
                },
                error: function(error){
                    console.log(error)
                }

            })
        }
    </script>

 @endpush