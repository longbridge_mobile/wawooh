<script>
    function addNewProduct(discountPrices, productPrices) {
        if (discountPrices >= productPrices) {
            $.notify('Discount price should not be greater than or equal to Product Price');
            $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
        } else {
            if ($('.measureAttachment table tbody tr').length === 0 && $('#outfit').prop('checked')) {
                $('.mains').notify('You must add fabric size and quantity');
                $('.measureAttachment').addClass('tristyle');
                $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
            } else {

                if ($('.required-product').val() === '' || $('.mce-container iframe body p').html() === '' || $('#productSummary').val() === '' || $('#productPrice').val() === '' || $('#productGroup').val() === '' || $('#subcat').val() === '' || $(".colorMaterialName").val() === '') {

                    $('.required-product, .mce-container').css({
                        'border': '1px solid red'
                    });
                    $('.required-product, .mce-container').addClass('tristyle');

                    swal('Some fields are required', '', 'warning');

                    $('.required-product').each(function(){
                        if($(this).val() === ''){
                            $(this).css({
                                'border': '1px solid red'
                            })
                            $(this).addClass('tristyle');
                        }else {
                            $(this).css({
                                'border': '1px solid rgba(51,51,51,0.6)'
                            })
                            $(this).removeClass('tristyle');
                        }
                    });

                    $('#addBtn').val('Submit').removeAttr("disabled", "disabled");


                    swal('Some fields are required', '', 'warning');
                    $('#addBtn').val('Submit').removeAttr("disabled", "disabled");


                } else {
                    //$('#loaderModal').modal();
                    $('#loaderModal').modal('show');

                    var productSizes = [];
                    var matt = [];
                    var art = getAllImagesSrc('.arrange-artImg .pImg');
                    // var mat = getAllFabricSrc('.materialImg');
                    $('.materialImg').each(function () {
                        matt.push({
                            materialPicture: $(this).attr('src'),
                            materialName: $(this).siblings('.fNameDiv').find('.fName').html(),
                            materialPrice: $(this).siblings('.fPriceDiv').find('.fPrice').html()
                        })
                    });

                    // selectMeasure
                    //  myMeasure.getSelected()

                    let measurement = JSON.stringify($('.selectMeasure').val());
                    var a = [];
                    /// if (art.length) {
                    $('.artWorkImage').each(function () {
                        a.push({
                            picture: $(this).attr('src'),
                            price: $(this).closest('.imageFabricDiv').find('.fPrice').html()
                        })
                    })
                    //  }


                    var data = {
                        "name": $('#productName').val(),
                        "productPriceDTO": {
                            "amount": $('#productPrice').val(),
                            "slashedPrice": $('#DiscountproductPrice').val(),
                            "percentageDiscount": $('#percent').val(),
                            "sewingAmount": $('#sewingPrice').val()
                        },
                        "prodSummary": $('#productSummary').val(),
                        "description": $('#productDescription').val(),
                        "productColorStyleDTOS": [],
                        "subCategoryId": $('#subcat').val(),
                        "productType": prodType,
                        "bespokeProductDTO": {}
                    };

                    if ($('#instocks').is(':checked') && $('#ondemands').is(':checked')) {
                        data.inStock = "Y";
                        data.acceptCustomSizes = "Y";
                        data.numOfDaysToComplete = $('#time').val();
                    } else if ($('#instocks').is(':checked')) {
                        data.inStock = "Y";
                        data.acceptCustomSizes = "N";
                        data.numOfDaysToComplete = 0;
                    } else if ($('#ondemands').is(':checked')) {
                        data.inStock = "N";
                        data.acceptCustomSizes = "Y";
                        data.numOfDaysToComplete = $('#time').val();
                    }

                    $.each($('.showFabricType'), function () {

                        var arrayPicture = [];
                        var sizes = [];

                        $(this).find('.submitImg').each(function () {
                            arrayPicture.push($(this).attr('src'));
                        });
                        $(this).find('.measureAttachment tr').each(function () {
                            sizes.push({
                                "name": $(this).find('.selected-size').html(),
                                "numberInStock": parseInt($(this).find('.selected-stock').html()),
                                "inStock": 'Y'
                            });
                        });
                        // data.picture = arrayPicture;

                        data.productColorStyleDTOS.push({
                            "colourName": $(this).find('.colorMaterialName').val(),
                            "colourPicture": $(this).find('.displayMat').attr('src'),
                            "picture": arrayPicture,
                            "productSizes": sizes
                        });
                    });

                    //  var bespokeProductDTO = {}

                    if (prodType == 1) {
                        data.bespokeProductDTO.mandatoryMeasurements = JSON.stringify(measurement);
                        data.bespokeProductDTO.artWorkPicture = a;
                        data.bespokeProductDTO.materialPicture = matt;
                        data.materialPrice = $('#materialPrice').val();
                        data.materialName = $('#materialName').val();
                        data.bespokeProductDTO.numOfDaysToComplete = $('#time').val()
                    } else if (prodType == 2 || prodType == 3) {
                        data.bespokeProductDTO.mandatoryMeasurements = JSON.stringify(measurement);
                    }


                    if ($('#ondemands').prop('checked')) {
                        console.log('aa');
                    } else {
                        data.bespokeProductDTO = null;
                    }

                    console.log(data);
                    $.ajax({
                        url: "{{route('designer.addNewProduct')}}",
                        type: "POST",
                        dataType: "json",
                        data: JSON.stringify(data),
                        success: function (res) {
                            console.log(res);
                            if (res.status === '00') {
                                //$('#loaderModal').hide();
                                //$('#loaderModal').modal('hide');
                                $(this).val('Please wait...').attr("disabled", false);
                                swal('Product added successfully', '', 'success');
                                window.location.href = '{{route('designer.product', str_slug(strtolower($storeName)))}}';
                            } else {
                                $('#loaderModal').modal('hide');
                                $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
                                $(this).val('Submit').removeAttr('disabled', 'disabled');
                                swal(res.message, '', 'error');
                            }

                        },
                        error: function (e) {
                            console.log(e);
                            //$('#loaderModal').modal('hide');
                            $('#addBtn').val('Submit').removeAttr("disabled", "disabled");
                            swal(e, '', 'error');
                        }
                    })
                }
            }
        }
    }
</script>