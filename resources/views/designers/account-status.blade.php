@extends('layouts.designer.designer')

@section('pageTitle','Account Status')
@section('content')
    <div class="noActivation">
        <div class="img-holder animated slideInDown">
            <img src="{{asset('img/orange-envelope-icon-28.png')}}" alt="" class="inner-img">
        </div>
        <div class="message">
            <span>Account not verified, Kindly click the link sent to your email to verify your account.</span>
            <div class="other"><span>OR</span></div>
            <a href="#" id="requestActivation" class="btn btn-request">Request Activation Link</a>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        <?php if(!is_null(session('userToken'))): ?>
            tok = <?= session('userToken') ?>;
        <?php else: ?>
            tok = '';
        <?php endif; ?>
                let email = window.location.hash;
                email = '{{request('email')}}';
                let plainEmail = atob(email);

        $(function () {
            $('#requestActivation').on('click', function (e) {
                e.preventDefault();
                let data = {
                    "email": plainEmail
                };
                $('#loaderModal').modal('show');
                $.ajax({
                    url: '{{getBaseUrl()}}/fashion/requestlink',
                    method: "POST",
                    dataType: "json",
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    data: JSON.stringify(data),
                    success: function (res) {
                        //console.log(res);
                        if(res.status === '00') {
                            swal('Link Successfully Sent', '', 'success');
                            window.location.href = '{{route('designersLandingPage')}}'
                        }
                        else if(res.status === '66') {
                            swal('User does not exist','','error')
                        }
                    },
                    error: function (e) {
                        //console.log('can\'t connect' + e);
                    }
                })
            })
        })
    </script>
@endpush