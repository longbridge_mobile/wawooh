@extends('layouts.designer.designer')

@push('styles')
    <style>

        .overlay {
            background: rgba(0, 0, 0, 0.6);
            height: 100%;
            top: 0;
            width: 100%;
            left: 0;
            display: none;
            transition: ease all 0.5s;
        }

        .owner-store-img:hover .overlay {
            display: block;
            transition: ease all 0.5s;
        }

    </style>
@endpush

@section('pageTitle', 'My Profile')
@section('content')

    <div class="content-wrapper vendorProfile white">
        <div class="sellerCover">
            <div class="coverBanner"></div>
            {{--<button class="btn btn-faint">Upload cover photo</button>--}}
            <input type="file" class="inputBanner" name="" id="coverPicture">
            <label id="logoBanner" for="coverPicture">
                <span>Click to Update Banner</span>
                @if(isset($designer->banner))
                    <img id="coverPicture" src="{{str_replace(['http','upload'],['https','upload/q_40'],$designer->banner)}}" alt="Banner" class="banner">
                @else
                    <img id="coverPicture" src="{{asset('img/designerBanner.jpeg')}}" alt="">
                @endif
                <input type="hidden" value='{{@$designer->banner}}' id='hiddenBanner'/>
            </label>
            <div class="overlay"><span><i class="fa fa-plus fa-4x"></i></span></div>
            <div class="bottomDetail">
                <h3>{{@$designer->storeName}}</h3>
                <h4>{{(@$designer->firstName . ' '. @$designer->lastName )}}</h4>
            </div>
        </div>
        <div class="designerDp">
            <label id='logoImage' style="height: inherit;" class="relative block owner-store-img">
                @if(isset($designer->logo))
                    <img id='brand' class="brand" src="{{str_replace(['http','upload'],['https','upload/q_40'], @$designer->logo)}}">
                @else
                    <img id="brand" style="background-color: #fff;" src="{{ asset('img/logo-sm.png') }}" alt="">
                @endif
                <input type="hidden" value='{{@$designer->logo}}' id='hiddenLogo'/>
                <div class='absolute overlay'>
                    <button class='btn btn-wawooh overBtn'><label for='inputLogo'>Change</label></button>
                </div>
            </label>
            <input type='file' id='inputLogo' class='hide-input'/>
        </div>
        <div class="profileDetail">
            <div class="others">
                <h3>Other Info</h3>
                <table class="table table-bordered" style=" margin-top: 15px;">
                    <tbody>
                    <tr>
                        <th>Successful Sales</th>
                        <td>{{@$designer->noOfDeliveredOrders}}</td>
                    </tr>
                    <tr>
                        <th>Member Since</th>
                        <td>{{\Carbon\Carbon::parse(@$designer->createdDate)->format('M-Y')}}
                            <small class="text-success">
                                ({{\Carbon\Carbon::parse(@$designer->createdDate)->diffForHumans()}})
                            </small>
                        </td>
                    </tr>
                   {{-- <tr>
                        <th>About Me</th>
                        <td>{{@$designer->aboutMe}}</td>
                    </tr>--}}
                    @if (isset($designer->storeName))
                        <tr>
                            {{--<th>Store Link</th>--}}
                            <td colspan="2">
                                <strong>Store Link</strong> <br> <br>
                                {{env('DESIGNER_URL')}}{{str_slug(strtolower(@$designer->storeName))}}-{{@$designer->id}}</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <hr>
            </div>
            <div class="basicInfo">
                <h3>Basic Information</h3>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th>Store's Name</th>
                        <td>
                            <span class='profile-details'>{{@$designer->storeName}}</span></td>
                    </tr>
                    <tr>
                        <th>Owner's Name</th>
                        <td>
                            <span class='profile-details'>{{@$designer->firstName}} {{@$designer->lastName}}</span>
                        </td>

                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><span class='profile-details'>{{@$designer->email}} </span></td>
                    </tr>
                    <tr>
                        <th>Phone No</th>
                        <td>
                            <span class='profile-details'>{{@$designer->phoneNo}}</span></td>
                    </tr>
                    <tr>
                        <th>Gender</th>
                        <td>
                            <span class='profile-details'>{{ucfirst(@$designer->gender)}}</span>
                        </td>
                    </tr>
                    <tr>
                        <th>Account number</th>
                        <td>
                            <span class='profile-details'>{{strtoupper(@$designer->accountNumber)}}</span>
                        </td>
                    </tr>
                    <tr>
                        <th>Threshold</th>
                        <td><span class='profile-details'>{{--{{@$designer->threshold}}--}}1</span></td>
                    </tr>
                    </tbody>
                </table>
                <div class="action">
                    <a href="{{route('designer.updateprofile')}}" class="btn btn-update">Update Profile</a>
                    @if(isset($designer->storeName))<a
                            href="{{route('designer.view', str_slug(strtolower(@$designer->storeName)))}}-{{@$designer->id}}"
                            class="btn btn-shop">Visit My Shop</a> @endif
                </div>
            </div>
        </div>
        <hr>

    </div>

    <!-- Modal -->
    <div class="modal fade" data-backdrop="static" id="confirm" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title makeBold brand-color" id="exampleModalLabel">Confirm Upload</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class='confirmUpload'>
                        <img style="width: 100%; border: 1px solid grey;" class='brand img-fluid' src=''
                        />
                    </div>
                </div>
                <div id='loaderLog' style='height:100px;display:none;' class='text-center'>
                    <img class='center-block' src="{{asset('img/loader.gif')}}"/>
                </div>
                <div class="modal-footer">
                    <p>Do you wish to continue?</p>
                    <button type="button" class="btn btn-danger no">No</button>
                    <button type="button" class="btn btn-wawooh uploadLogo">Yes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Banner Modal -->
    <div class="modal fade" data-backdrop="static" id="confirmBanner" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title makeBold brand-color" id="exampleModalLabel">Confirm Banner Upload</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class='confirmUpload'>
                        <img style="width: 100%; border: 1px solid grey;" class='banner img-fluid' src=''
                        />
                    </div>
                </div>
                <div id='loaderLog' style='height:100px;display:none;' class='text-center'>
                    <img class='center-block' src="{{asset('img/loader.gif')}}"/>
                </div>
                <div class="modal-footer">
                    <p>Do you wish to continue?</p>
                    <button type="button" class="btn btn-danger noBanner">No</button>
                    <button type="button" class="btn btn-wawooh uploadBanner">Yes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#profile').on('click', function () {
                var value = $(this).text();
                if (value.toLowerCase() == 'edit') {
                    $('.profile-input').show();
                    $('#close').show();
                    $('span.profile-details').hide('hide');
                    $(this).text('Save Changes');
                } else {
                    let data = {
                        "firstName": $('#fn').val(),
                        "lastName": $('#ln').val(),
                        "phoneNo": $('#pnum').val(),
                        "gender": $('#gender').val(),
                        "designer": {
                            "storeName": $('#store').val(),
                            "accountNumber": $('#accountNumber').val(),
                            "threshold": $('#threshold').val()
                        }
                    }
                    $('div.loader').show();
                    editMyProfile(data);
                    $('.profile-input').hide();

                    $('span.profile-details').show('hide');
                    $(this).text('Edit');
                }
            });

            $('#close').on('click', function () {
                location.reload();
            })
        });

        function editMyProfile(data) {
            $.ajax({
                url: '{{route('designer.editProfile')}}',
                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    console.log(result);
                    $('div.loader').hide();

                    if (result.status == 0) {
                        swal('Profile Updated Successfully', '', 'success');
                        location.reload();
                    } else {
                        swal('An Error Occured While Updating. Please Try Again', '', 'error');
                    }
                },
                error: function (e) {
                    $('div.loader').hide();
                    console.log(e);
                }
            });
        }

        $('#inputLogo').on('change', function () {
            convertToBase64(this, 'img.brand');
            $('#confirm').modal();
        });

        $('#coverPicture').on('change', function () {
            convertToBase64(this, 'img.banner');
            $('#confirmBanner').modal();
        });


        $('button.no').on('click', function () {
            let src = $('input#hiddenLogo').val();
            // console.log(src);
            $('img#brand').attr('src', src);
            $('#confirm').modal('hide');
        });

        $('button.noBanner').on('click', function () {
            let src = $('input#hiddenBanner').val();
            // console.log(src);
            $('img#coverPicture').attr('src', src);
            $('#confirmBanner').modal('hide');
        });

        $('.uploadLogo').on('click', function () {
            let data = {
                "id": "{{$designer->id}}",
                "logo": getImageSrc('.brand')
            }

            console.log(data);
            $('#loaderLog').show();
            $.ajax({
                url: '{{route('designer.updateLogo')}}',
                type: "POST",
                data: JSON.stringify(data),

                success: function (result) {
                    console.log(result);
                    $('#loaderLog').hide();

                    if (result.status === "00") {
                        // $.notify('Logo Successfully Uploaded', 'success');
                        swal('Logo Successfully Uploaded', '', 'success');
                        $('#confirm').modal('hide');
                        location.reload();
                    } else {
                        $('#loaderLog').hide();
                        swal(result.message, '', 'error');
                    }
                },
                error: function (e) {
                    $('#loaderLog').hide();
                    console.log(e);
                    swal(e, '', 'error');
                }
            });
        });

        $('.uploadBanner').on('click', function () {

            let data = {
                "id": "{{$designer->id}}",
                "banner": getImageSrc('.banner')
            };
            console.log(data);
            $('#loaderLog').show();
            $.ajax({
                url: '{{route('designer.updateBanner')}}',
                type: "POST",
                data: JSON.stringify(data),

                success: function (result) {
                    console.log(result);
                    $('#loaderLog').hide();
                    if (result.status === "00") {
                        // $.notify('Logo Successfully Uploaded', 'success');
                        swal('Cover Banner Successfully Uploaded', '', 'success');
                        location.reload();
                        $('#confirm').modal('hide');
                    } else {
                        $('#loaderLog').hide();
                        swal(result.message, '', 'error');
                    }
                },
                error: function (e) {
                    $('#loaderLog').hide();
                    // console.log(e);
                    swal(e, '', 'error');
                }
            });
        });

    </script>

@endpush