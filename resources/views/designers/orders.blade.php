@extends('layouts.designer.designer')

@push('styles')
    <style>
        .orderNum {
            color: #4a90e2;
        }

        .orderNum:hover {
            color: #337ab7;
        }
    </style>
@endpush

@section('pageTitle', 'My Orders')
@section('content')
    <div class="content-wrapper">
        @include('layouts.designer.orderDetails')
        <div class="row grid-margin">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Order status</h6>
                        <div class="table-responsive">
                            <ul class="nav nav-pills nav-fill mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item arrow_box">
                                    <a class="nav-link active" id="all-orders-tab" data-toggle="pill" href="#all-orders"
                                       role="tab"
                                       aria-controls="all-orders" aria-selected="true">All Orders <span
                                                class="badge badge-wawooh">@if(isset($order)){{count($orders)}}@endif</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pending-orders-tab" data-toggle="pill"
                                       href="#pending-orders" role="tab"
                                       aria-controls="pending-orders" aria-selected="false">Pending Orders <span
                                                class="badge badge-wawooh"><!--@if(count($activeOrders)) {{count($activeOrders)}} @endif</span>--></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="active-orders-tab" data-toggle="pill" href="#active-orders"
                                       role="tab"
                                       aria-controls="active-orders" aria-selected="false">Active Orders <span
                                                class="badge badge-wawooh">@if(count($activeOrders)) {{count($activeOrders)}} @endif</span></a>
                                </li>
                                {{--<li class="nav-item">
                                    <a class="nav-link" id="completed-orders-tab" data-toggle="pill"
                                       href="#completed-orders" role="tab"
                                       aria-controls="completed-orders" aria-selected="false">Completed Orders <span
                                                class="badge badge-wawooh">@if(count($completedOrders)) {{count($completedOrders)}} @endif</span></a>
                                </li>--}}
                                <li class="nav-item">
                                    <a class="nav-link" id="cancelled-orders-tab" data-toggle="pill"
                                       href="#cancelled-orders" role="tab"
                                       aria-controls="cancelled-orders" aria-selected="false">Cancelled Orders <span
                                                class="badge badge-wawooh"><!--@if(count($completedOrders)) {{count($completedOrders)}} @endif--></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="bespoke-orders-tab" data-toggle="pill"
                                       href="#bespoke-orders" role="tab"
                                       aria-controls="bespoke-orders" aria-selected="false">Bespoke Orders <span
                                                class="badge badge-wawooh"></span></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="all-orders" role="tabpanel"
                                     aria-labelledby="all-orders-tab">

                                    <div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <p>Total Number of entries</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-hovered" id="desOrderTable">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order No</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($orders) && is_array($orders))
                                                    @if(count($orders))
                                                        @foreach($orders as $order)
                                                            <tr>
                                                                <td class="orderNum">{{$order->orderNumber}} &nbsp;
                                                                    &nbsp;
                                                                    @if($order->failedInspectionReason != null)<i
                                                                            class="fa fa-fw fa-envelope animated swing infinite"></i>
                                                                    @endif
                                                                </td>
                                                                <td>{{$order->productName}}</td>
                                                                {{--  <td></td>  --}}
                                                                <td>₦{{number_format($order->amount)}}</td>
                                                                <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                    <small class="text-success">
                                                                        ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                        )
                                                                    </small>
                                                                </td>
                                                                <td>
                                                                    @if($order->status == 'PC')
                                                                        <label class="label label-pending">Payment
                                                                            Confirmed</label>
                                                                    @elseif($order->status == 'P')
                                                                        <label class="label label-pending">Pending</label>
                                                                    @elseif($order->status == 'OR')
                                                                        <label class="label label-rejected">Order
                                                                            rejected</label>
                                                                    @elseif($order->status == 'C')
                                                                        <label class="label label-rejected">Cancelled</label>
                                                                    @elseif($order->status == 'OP')
                                                                        <label class="label label-processing">order
                                                                            processing</label>
                                                                    @elseif($order->status == 'CO')
                                                                        <label class="label label-completed">Completed
                                                                            by you</label>
                                                                    @elseif($order->status == 'RI')
                                                                        <label class="label label-inspection">Ready for
                                                                            Inspection</label>
                                                                    @elseif($order->status == 'RS')
                                                                        <label class="label label-ins">Ready for
                                                                            shipping</label>
                                                                    @elseif($order->status == 'WC')
                                                                        <label class="label label-completed">Wawooh
                                                                            Collected</label>
                                                                    @elseif($order->status == 'WR')
                                                                        <label class="label label-rejected">Wawooh
                                                                            Rejected</label>
                                                                    @elseif($order->status == 'A')
                                                                        <label class="label label-processing">Accepted</label>
                                                                    @elseif($order->status == 'OS')
                                                                        <label class="label label-completed">Order
                                                                            Shipped</label>
                                                                    @elseif($order->status == 'D')
                                                                        <label class="label label-completed">Delivered</label>
                                                                    @endif
                                                                </td>
                                                                <td><a href="{{route('designer.orders.details', [str_slug($storeName), $order->id])}}">view details</a></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan='7' class='text-center'>No Orders Yet.</td>
                                                        </tr>
                                                    @endif
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="pending-orders" role="tabpanel"
                                     aria-labelledby="pending-orders-tab">
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <p>Total number of entries: <button class="null-btn">{{(count($pendingOrders))}}</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-hovered" id="conPendingOrders">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order No</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Customer</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(isset($pendingOrders) && is_array($pendingOrders))
                                                    @if(count($pendingOrders))
                                                        @foreach($pendingOrders as $order)
                                                            <tr>
                                                                <td>{{$order->orderNumber}}</td>
                                                                <td>{{$order->productName}}</td>
                                                                <td>{{$order->customerName}}</td>
                                                                <td>₦{{number_format($order->amount)}}</td>
                                                                <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                    <small>
                                                                        ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                        )
                                                                    </small>
                                                                </td>
                                                                <td>
                                                                    @if($order->status == 'PC')
                                                                        <label class="label label-pending">Pending</label>
                                                                    @elseif($order->status == 'P')
                                                                        <label class="label label-pending">Pending</label>
                                                                    @elseif($order->status == 'OR')
                                                                        <label class="label label-rejected">Order
                                                                            rejected</label>
                                                                    @elseif($order->status == 'C')
                                                                        <label class="label label-rejected">Cancelled</label>
                                                                    @elseif($order->status == 'OP')
                                                                        <label class="label label-processing">order
                                                                            processing</label>
                                                                    @elseif($order->status == 'CO')
                                                                        <label class="label label-completed">completed
                                                                            by
                                                                            you</label>
                                                                    @elseif($order->status == 'RI')
                                                                        <label class="label label-inspection">Ready for
                                                                            Inspection</label>
                                                                    @elseif($order->status == 'RS')
                                                                        <label class="label label-processing">Ready for
                                                                            shipping</label>
                                                                    @elseif($order->status == 'OS')
                                                                        <label class="label label-completed">Order
                                                                            Shipped</label>
                                                                    @elseif($order->status == 'D')
                                                                        <label class="label label-completed">Order
                                                                            Delivered</label>
                                                                    @endif
                                                                </td>
                                                                <td><a href="{{route('designer.orders.details', [str_slug($storeName), $order->id])}}">view details</a></td>
                                                            </tr>
                                                        @endforeach
                                                </tbody>
                                                <tbody>
                                                @elseif(isset($orders) && is_array($orders))
                                                    @if(count($orders))
                                                        @foreach($orders as $order)
                                                            @if($order->status == 'P')
                                                                <tr class='clickable-row'
                                                                    @if($order->deliveryStatus != 'OR') data-href='{{route('designer.orders.details', [str_slug($storeName), $order->id])}}' @endif>
                                                                    <td>{{$order->orderNumber}}</td>
                                                                    <td>{{$order->productName}}</td>
                                                                    <td>{{$order->customerName}}</td>
                                                                    <td>₦{{number_format($order->amount)}}</td>
                                                                    <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                        <small>
                                                                            ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}})
                                                                        </small>
                                                                    </td>
                                                                    <td>
                                                                        @if($order->status == 'PC')
                                                                            <label class="label label-pending">Pending</label>
                                                                        @elseif($order->status == 'P')
                                                                            <label class="label label-label-waiting-c">Waiting
                                                                                for
                                                                                Confirmation</label>
                                                                        @elseif($order->status == 'OR')
                                                                            <label class="label label-rejected">Order
                                                                                rejected</label>
                                                                        @elseif($order->status == 'C')
                                                                            <label class="label label-rejected">Cancelled</label>
                                                                        @elseif($order->status == 'OP')
                                                                            <label class="label label-processing">order
                                                                                processing</label>
                                                                        @elseif($order->status == 'CO')
                                                                            <label class="label label-completed">completed
                                                                                by you</label>
                                                                        @elseif($order->status == 'RI')
                                                                            <label class="label label-inspection">Ready
                                                                                for
                                                                                Inspection</label>
                                                                        @elseif($order->status == 'RS')
                                                                            <label class="label label-processing">Ready
                                                                                for
                                                                                shipping</label>
                                                                        @elseif($order->status == 'OS')
                                                                            <label class="label label-completed">Order
                                                                                Shipped</label>
                                                                        @elseif($order->status == 'D')
                                                                            <label class="label label-completed">Order
                                                                                Delivered</label>
                                                                        @endif
                                                                    </td>
                                                                    <td><a href="{{route('designer.orders.details', [str_slug($storeName), $order->id])}}">view details</a></td>
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @else
                                                    <tr>
                                                        <td colspan='7' class='text-center'>No pending Orders Yet.</td>
                                                    </tr>
                                                @endif
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="active-orders" role="tabpanel"
                                     aria-labelledby="active-orders-tab">

                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <p>Total number of entries: <button class="null-btn">{{(count($activesOrders))}}</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-hovered" id="desOrderActive">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order No</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Customer</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($activesOrders))
                                                    @foreach($activesOrders as $order)
                                                        <tr>
                                                            <td>{{$order->orderNumber}}</td>
                                                            <td>{{$order->productName}}</td>
                                                            <td>{{$order->customerName}}</td>
                                                            <td>₦{{number_format($order->amount)}}</td>
                                                            <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                <small>
                                                                    ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                    )
                                                                </small>
                                                            </td>
                                                            <td>
                                                                @if($order->status == 'PC')
                                                                    <label class="label label-pending">Pending</label>
                                                                @elseif($order->status == 'P')
                                                                    <label class="label label-pending">Pending</label>
                                                                @elseif($order->status == 'OR')
                                                                    <label class="label label-rejected">Order
                                                                        rejected</label>
                                                                @elseif($order->status == 'C')
                                                                    <label class="label label-rejected">Cancelled</label>
                                                                @elseif($order->status == 'OP')
                                                                    <label class="label label-processing">order
                                                                        processing</label>
                                                                @elseif($order->status == 'A')
                                                                    <label class="label label-processing">Accepted</label>
                                                                @elseif($order->status == 'CO')
                                                                    <label class="label label-completed">completed by
                                                                        you</label>
                                                                @elseif($order->status == 'RI')
                                                                    <label class="label label-inspection">Ready for
                                                                        Inspection</label>
                                                                @elseif($order->status == 'RS')
                                                                    <label class="label label-processing">Ready for
                                                                        shipping</label>
                                                                @elseif($order->status == 'OS')
                                                                    <label class="label label-completed">Order
                                                                        Shipped</label>
                                                                @elseif($order->status == 'D')
                                                                    <label class="label label-completed">Order
                                                                        Delivered</label>
                                                                @endif
                                                            </td>
                                                            <td><a href="{{route('designer.orders.details', [str_slug($storeName), $order->id])}}">view details</a></td>
                                                        </tr>
                                                    @endforeach

                                                @else
                                                    <tr>
                                                        <td colspan='7' class='text-center'>No Active Orders Yet.</td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                {{--<div class="tab-pane fade" id="completed-orders" role="tabpanel"
                                     aria-labelledby="completed-orders-tab">

                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <p>Total number of entries: <button class="null-btn">{{(count($completedsOrders))}}</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped" id="desOrderTable">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order No</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Customer</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($completedsOrders))
                                                    @foreach($completedsOrders as $order)
                                                        <tr class='clickable-row'
                                                            @if($order->deliveryStatus != 'OR') data-href='{{route('designer.orders.details', [str_slug($storeName), $order->id])}}' @endif>
                                                            <td>{{$order->orderNumber}}</td>
                                                            <td>{{$order->productName}}</td>
                                                            <td>{{$order->customerName}}</td>
                                                            <td>{{number_format($order->amount)}}</td>
                                                            <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                <small>
                                                                    ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                    )
                                                                </small>
                                                            </td>
                                                            <td>

                                                                @if($order->status == 'PC')
                                                                    <label class="label label-pending">Pending</label>
                                                                @elseif($order->status == 'P')
                                                                    <label class="label label-pending">Pending</label>
                                                                @elseif($order->status == 'OR')
                                                                    <label class="label label-rejected">Order
                                                                        rejected</label>
                                                                @elseif($order->status == 'C')
                                                                    <label class="label label-rejected">Cancelled</label>
                                                                @elseif($order->status == 'OP')
                                                                    <label class="label label-processing">order
                                                                        processing</label>
                                                                @elseif($order->status == 'CO')
                                                                    <label class="label label-completed">completed by
                                                                        you</label>
                                                                @elseif($order->status == 'RI')
                                                                    <label class="label label-inspection">Ready for
                                                                        Inspection</label>
                                                                @elseif($order->status == 'RS')
                                                                    <label class="label label-processing">Ready for
                                                                        shipping</label>
                                                                @elseif($order->status == 'OS')
                                                                    <label class="label label-completed">Order
                                                                        Shipped</label>
                                                                @elseif($order->status == 'D')
                                                                    <label class="label label-completed">Order
                                                                        Delivered</label>
                                                                @endif

                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif

                                                </tbody>
                                                <tbody>
                                                @if(isset($orders) && is_array($orders))
                                                    @if(count($orders))
                                                        @foreach($orders as $order)
                                                            @if($order->status == 'RS' || $order->status == 'OS' || $order->status == 'D')
                                                                <tr class='clickable-row'
                                                                    @if($order->deliveryStatus != 'OR') data-href='{{route('designer.orders.details', [str_slug($storeName), $order->id])}}' @endif>
                                                                    <td>{{$order->orderNumber}}</td>
                                                                    <td>{{$order->productName}}</td>
                                                                    <td>{{$order->customerName}}</td>
                                                                    <td>₦{{number_format($order->amount)}}</td>
                                                                    <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                        <small>
                                                                            ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                            )
                                                                        </small>
                                                                    </td>
                                                                    <td>
                                                                        @if($order->status == 'PC')
                                                                            <label class="label label-pending">Pending</label>
                                                                        @elseif($order->status == 'P')
                                                                            <label class="label label-label-waiting-c">Waiting
                                                                                for
                                                                                Confirmation</label>
                                                                        @elseif($order->status == 'OR')
                                                                            <label class="label label-rejected">Order
                                                                                rejected</label>
                                                                        @elseif($order->status == 'C')
                                                                            <label class="label label-rejected">Cancelled</label>
                                                                        @elseif($order->status == 'OP')
                                                                            <label class="label label-processing">order
                                                                                processing</label>
                                                                        @elseif($order->status == 'CO')
                                                                            <label class="label label-completed">completed
                                                                                by you</label>
                                                                        @elseif($order->status == 'RI')
                                                                            <label class="label label-inspection">Ready
                                                                                for
                                                                                Inspection</label>
                                                                        @elseif($order->status == 'RS')
                                                                            <label class="label label-processing">Ready
                                                                                for
                                                                                shipping</label>
                                                                        @elseif($order->status == 'OS')
                                                                            <label class="label label-completed">Order
                                                                                Shipped</label>
                                                                        @elseif($order->status == 'D')
                                                                            <label class="label label-completed">Order
                                                                                Delivered</label>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        @endforeach

                                                    @else
                                                    @endif
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>--}}
                                <div class="tab-pane fade" id="cancelled-orders" role="tabpanel"
                                     aria-labelledby="cancelled-orders-tab">
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <p>Total number of entries: <button class="null-btn">{{count($cancelOrders)}}</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped" id="desOrderActive">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order No</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Customer</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($cancelOrders))
                                                    @foreach($cancelOrders as $order)
                                                        <tr>
                                                            <td>{{$order->orderNumber}}</td>
                                                            <td>{{$order->productName}}</td>
                                                            <td>{{$order->customerName}}</td>
                                                            <td>₦{{number_format($order->amount)}}</td>
                                                            <td>{{\Carbon\Carbon::parse($order->orderDate)->format('d-M-y')}}
                                                                <small>
                                                                    ({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}}
                                                                    )
                                                                </small>
                                                            </td>
                                                            <td>
                                                                @if($order->status == 'C')
                                                                    <label class="label label-rejected">Cancelled</label>
                                                                @endif
                                                            </td>
                                                            <td><a href="{{route('designer.orders.details', [str_slug($storeName), $order->id])}}">view details</a></td>
                                                        </tr>
                                                    @endforeach

                                                @else
                                                    <tr>
                                                        <td colspan='7' class='text-center'>No cancelled Orders Yet.
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="bespoke-orders" role="tabpanel"
                                     aria-labelledby="bespoke-orders-tab">
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <p>Total number of entries: <button class="null-btn">{{count($bespokeorders)}}</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped" id="desOrderActive">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Order No</th>
                                                    <th scope="col">Product</th>
                                                    <th scope="col">Customer</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Status</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($bespokeorders))
                                                    @foreach($bespokeorders as $bespokeorder)
                                                        <tr>
                                                            <td>{{$bespokeorder->orderNum}}</td>
                                                            <td>{{$bespokeorder->productName}}</td>
                                                            <td>{{@$bespokeorder->customerName}}</td>
                                                            <td>₦{{number_format($bespokeorder->price)}}</td>
                                                            <td>{{--{{\Carbon\Carbon::parse(@$bespokeorder->orderDate)->format('d-M-y')}}
                                                                <small>
                                                                    ({{\Carbon\Carbon::parse(@$bespokeorder->orderDate)->diffForHumans()}}
                                                                    )
                                                                </small>--}}
                                                            </td>
                                                            {{--<td>
                                                                @if($order->status == 'C')
                                                                    <label class="label label-rejected">Cancelled</label>
                                                                @endif
                                                            </td>--}}
                                                            <td><a href="{{route('designer.bespokeorders.details', [str_slug($storeName), @$bespokeorder->orderNum])}}">view details</a></td>
                                                        </tr>
                                                    @endforeach

                                                @else
                                                    <tr>
                                                        <td colspan='7' class='text-center'>No cancelled Orders Yet.
                                                        </td>
                                                    </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        // jQuery(document).ready(function ($) {
        //     $(".clickable-row").click(function () {
        //         window.location = $(this).data("href");
        //     });
        //
        //     $(".nav-item").on("click", function () {
        //         $(".nav-item").removeClass("arrow_box");
        //         $(this).addClass("arrow_box");
        //     });
        // });
    </script>

@endpush