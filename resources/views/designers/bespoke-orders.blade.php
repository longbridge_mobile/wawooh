@extends('layouts.designer.designer')

@push('styles')
    <style>

    </style>
@endpush

@section('pageTitle', 'Bespoke Requests')
@section('content')
    <div class="content-wrapper">
        <div class="row grid-margin">
            <div class="col-12">
                <h3 style="margin-bottom: 40px;" class="makeBold">Bespoke Request Bids</h3>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hovered" id="desOrderTable">
                                <thead>
                                <tr>
                                    <th>s/n</th>
                                    <th>Gender</th>
                                    <th>Budget</th>
                                    <th>Expected Delivery Date</th>
                                    <th>Date Ordered</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(is_array($listBid))
                                    @if(count($listBid))
                                        @foreach($listBid as $listBids)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$listBids->bespokeOrderRequestDTO->gender}}</td>
                                                <td>
                                                    <strong>₦{{number_format($listBids->bespokeOrderRequestDTO->budget)}}</strong>
                                                </td>
                                                <td>{{\Carbon\Carbon::createFromTimestamp($listBids->bespokeOrderRequestDTO->timeOfDelivery / 1000)->format('dS F, Y')}}
                                                    <br>
                                                    <small class="text-info">
                                                        ({{\Carbon\Carbon::createFromTimestamp($listBids->bespokeOrderRequestDTO->timeOfDelivery / 1000)->diffForHumans()}}
                                                        )
                                                    </small>
                                                </td>
                                                <td>{{\Carbon\Carbon::createFromTimestamp($listBids->bespokeOrderRequestDTO->orderDate / 1000)->format('dS M, Y')}}
                                                    <br>
                                                    <small class="text-success">
                                                        ({{\Carbon\Carbon::createFromTimestamp($listBids->bespokeOrderRequestDTO->orderDate / 1000)->diffForHumans()}}
                                                        )
                                                    </small>
                                                </td>
                                                <td>
                                                    <a href="{{route('designerViewRequestDetails',[generateCorrectUrl(strtolower($storeName)),$listBids->bespokeOrderRequestDTO->id])}}">view
                                                        details</a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".clickable-row").click(function () {
                window.location = $(this).data("href");
            });

            $(".nav-item").on("click", function () {
                $(".nav-item").removeClass("arrow_box");
                $(this).addClass("arrow_box");
            });
        });
    </script>

@endpush