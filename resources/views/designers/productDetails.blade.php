@extends('layouts.designer.designer')

@push('styles')
    <style>
        .fitDiv {
            height: 100%;
            width: 100%;
        }

        .img-fluid {
            cursor: pointer;
        }

        .disabledClick {
            pointer-events: none;
        }

        .product-big-img {
            height: 100%;
        }

        .product-small-img .img-fluid {
            width: 100%;
            height: 100%;
        }

        .product-small-img > div {
            height: 120px;
            margin-bottom: 5px;
            width: 100%;
        }

        .size-inline {
            display: inline-flex;
        }

        .size-inline .form-inline {
            margin-top: 0.5rem;
            margin-right: 7px;
        }

        .size-inline .form-inline label {

        }

        .size-inline .form-inline input {
            width: 2rem;
        }

        .size-inline .input-group-prepend, .size-inline .input-group-append {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-left: 0.5rem;
            margin-right: 0.5rem;
        }

        .size-inline .input-group-prepend:hover, .size-inline .input-group-append:hover {
            cursor: pointer;
        }

        .btn-wawooh {
            font-size: 0.9rem;
        }

        .btn-wawooh:hover {
            cursor: pointer;
        }
    </style>
@endpush

@section('pageTitle', title_case($product->name) . ' | Product Details')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="row product-img">
                            <div class="col-md-9 product-big-img productDisplay product-one productDisplayOne">
                                <img id='mainPicture'
                                     src="{{str_replace(['http','upload'],['https','upload/q_40'],@$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3 product-small-img productThumb product-one productThumbOne">
                                @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS))
                                    @if(count($product->productColorStyleDTOS[0]->productPictureDTOS))
                                        @foreach($product->productColorStyleDTOS[0]->productPictureDTOS as $smallProductPicture)
                                            <div>
                                                <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                     class="img-fluid">
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>

                            <div class="col-md-9 hide product-big-img productDisplay product-two productDisplayTwo">
                                <img id='mainPicture'
                                     src="{{str_replace(['http','upload'],['https','upload/q_40'],@$product->productColorStyleDTOS[1]->productPictureDTOS[1]->picture)}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3  hide product-small-img productThumb product-two productThumbTwo">
                                @if(isset($product->productColorStyleDTOS[1]->productPictureDTOS))
                                    @if(count($product->productColorStyleDTOS[1]->productPictureDTOS))
                                        @foreach($product->productColorStyleDTOS[1]->productPictureDTOS as $smallProductPicture)
                                            <div>
                                                <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                     class="img-fluid">
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>

                            <div class="col-md-9 hide product-big-img productDisplay product-three productDisplayThree">
                                <img id='mainPicture'
                                     src="{{str_replace(['http','upload'],['https','upload/q_40'],@$product->productColorStyleDTOS[2]->productPictureDTOS[2]->picture)}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3 hide product-small-img productThumb product-three productThumbThree">
                                @if(isset($product->productColorStyleDTOS[2]->productPictureDTOS))
                                    @if(count($product->productColorStyleDTOS[2]->productPictureDTOS))
                                        @foreach($product->productColorStyleDTOS[2]->productPictureDTOS as $smallProductPicture)
                                            <div>
                                                <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                     class="img-fluid">
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>

                            <div class="col-md-9 hide product-big-img productDisplay product-four productDisplayFour">
                                <img id='mainPicture'
                                     src="{{str_replace(['http','upload'],['https','upload/q_40'],@$product->productColorStyleDTOS[4]->productPictureDTOS[4]->picture)}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3 hide product-small-img productThumb product-four productThumbFour">
                                @if(isset($product->productColorStyleDTOS[4]->productPictureDTOS))
                                    @if(count($product->productColorStyleDTOS[4]->productPictureDTOS))
                                        @foreach($product->productColorStyleDTOS[4]->productPictureDTOS as $smallProductPicture)
                                            <div>
                                                <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                     class="img-fluid">
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>

                            <div class="col-md-9 hide product-big-img productDisplay product-five productDisplayFive">
                                <img id='mainPicture'
                                     src="{{str_replace('http','https',@$product->productColorStyleDTOS[5]->productPictureDTOS[5]->picture)}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3 hide product-small-img productThumb product-five productThumbFive">
                                @if(isset($product->productColorStyleDTOS[5]->productPictureDTOS))
                                    @if(count($product->productColorStyleDTOS[5]->productPictureDTOS))
                                        @foreach($product->productColorStyleDTOS[5]->productPictureDTOS as $smallProductPicture)
                                            <div>
                                                <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$smallProductPicture->picture)}}"
                                                     class="img-fluid">
                                            </div>
                                        @endforeach
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 product-des wow animated fadeInRight" data-wow-delay=".3s">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="product-name">
                                    <strong>Product Name: {{$product->name}}</strong>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="product-name">Product Status:
                                    <label class="label label-{{$product->verifiedFlag === 'Y' ? 'completed' : 'rejected'}}">{{$product->verifiedFlag === 'Y' ? 'Verified' : 'Not Verified'}}</label>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="product-name">{{($product->slashedPrice > 0) ? 'Old Price:' : 'Price: ' }}
                                    <span>₦{{number_format($product->amount)}}</span></p>
                            </div>
                            @if($product->slashedPrice > 0)
                                <div class="col-md-6">
                                    <p class="product-name">New Price:
                                        <span>₦{{number_format($product->slashedPrice)}}</span>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="product-name">Percentage Discount:
                                        <span> &dash;{{$product->percentageDiscount}}%</span></p>
                                </div>
                            @endif
                            @if($product->slashedPrice > 0)
                                <div class="col-md-6">
                                    <p class="product-name">Discount/Difference:
                                        <span>₦{{getNewPrice($product->amount, $product->slashedPrice)}}</span></p>
                                </div>
                            @endif
                            <div class="col-md-6">
                                <p class="product-name">Availability:
                                    <span>
                                        @if ($product->availability === 'Y' && $product->acceptCustomSizes === 'N')
                                            <label class="label label-inspection">Ready To Wear</label>
                                        @elseif($product->availability === 'Y' && $product->acceptCustomSizes === 'Y')
                                            <label class="label label-processing">Ready To Wear</label>  and <label
                                                    class="label label-inspection">Bespoke</label>
                                        @elseif($product->bespokeProductDTO !== null && $product->acceptCustomSizes === 'N' && $product->availability === 'N')
                                            <label class="label label-processing">Bespoke Only</label>
                                        @else
                                            <label class="label label-info">Availability Unknown</label>
                                        @endif
                                    </span>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p class="product-name">Category:
                                    <span>{{$product->categoryName}}</span></p>
                            </div>
                            <div class="col-md-6">
                                <p class="product-name">Sub Category:
                                    <span>{{$product->subCategoryName}}</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="product-name">Total Sales: <span>{{$product->salesInQueue}}</span></p>
                            </div>
                            <div class="col-md-6">
                                <p class="product-name">Sales in Queue: <span> {{$product->totalSales}}</span></p>
                            </div>

                        </div>
                        <h6><strong><u>Summary</u></strong></h6>
                        <p class="product-des-long">{{$product->prodSummary}}</p>
                        <h6><strong><u>Product Description</u></strong></h6>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="prod-desc prod-message wow animated fadeInUp" data-wow-delay=".2s">
                                    {!! nl2br($product->description) !!}
                                </div>
                            </div>
                        </div>
                        <h6><strong><u>Production time in days</u></strong></h6>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="prod-desc prod-message wow animated fadeInUp" data-wow-delay=".2s">
                                    {{@$product->bespokeProductDTO->numOfDaysToComplete}}
                                </div>
                            </div>
                        </div>

                        @if($product->unVerifiedReason != null)
                            <h6 style="margin-bottom: 2px;"><strong><u>Unverify Reason</u></strong></h6>
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <div class="prod-desc prod-message-failed wow animated fadeInUp"
                                         data-wow-delay=".4s">
                                        <i class="fa fa-fw fa-bell animated swing infinite"></i>
                                        {!! nl2br($product->unVerifiedReason) !!}
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-md-12 productColor">
                            <h5><strong><u>Colors</u></strong></h5>
                            <ul style="padding-left: 0;">
                                @if(isset($product->productColorStyleDTOS))
                                    @if(count($product->productColorStyleDTOS))
                                        @php
                                            $var = 0;
                                        @endphp
                                        @foreach($product->productColorStyleDTOS as $colorPicture)
                                            <div class="c-label">{{$colorPicture->colourName}}</div>
                                            <li class="pColor oo">
                                                <img src="{{str_replace(['http','upload'],['https','upload/q_40'],$colorPicture->colourPicture)}}"
                                                     alt="1"
                                                     productCount="{{$var++}}"
                                                     class="imgproductmores addProductAttribute"
                                                     imageId="{{$colorPicture->id}}">
                                            </li>
                                        @endforeach
                                    @endif
                                @endif
                            </ul>
                        </div>

                        <div class="col-md-12 product-size seller-sizes">
                            <h6><strong><u>Size</u></strong></h6>
                            <div class="size-inline">
                                @if(isset($product->productColorStyleDTOS) && is_array($product->productColorStyleDTOS))
                                    @if(count($product->productColorStyleDTOS))
                                        @foreach($product->productColorStyleDTOS as $size)
                                            @foreach($size->productSizes as $realSize)
                                                <div class="form-inline">
                                                    <label for="">{{$realSize->name}}</label>&nbsp;
                                                    <div class="input-group stock">
                                                        {{-- <div class="input-group-prepend">
                                                             <i class="fa fa-minus minus-stock" aria-hidden="true"></i>
                                                         </div>--}}
                                                        <input type="text" disabled
                                                               class="form-control sizeInput stockNo"
                                                               value="{{$realSize->numberInStock}}"
                                                               name="{{$realSize->name}}"
                                                               id="{{$realSize->name}}">
                                                        {{--<div class="input-group-append">
                                                            <i class="fa fa-plus add-stock" aria-hidden="true"></i>
                                                        </div>--}}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endforeach
                                        {{-- <div class="form-group py-3">
                                           <input type="button" class="btn btn-default btn-wawooh" id="update-quantity"
                                                  value="UPDATE STOCK">
                                        </div>--}}
                                    @else
                                        <label class="label label-rejected">No Sizes found</label>
                                    @endif
                                @endif
                                {{--  @if(isset($product->productSizes) && is_array($product->productSizes))
                                @if(count($product->productSizes))
                                @foreach($product->productSizes as $size)
                                    <div class="form-inline">
                                        <label for="">{{$size->name}}</label>
                                        <div class="input-group stock">
                                            <div class="input-group-prepend">
                                                <i class="fa fa-minus minus-stock" aria-hidden="true"></i>
                                            </div>
                                            <input type="text" readonly class="form-control stockNo"
                                                   value="{{$size->stockNo}}" name="{{$size->name}}"
                                                   id="{{$size->name}}">
                                            <div class="input-group-append">
                                                <i class="fa fa-plus add-stock" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <div class="form-group mx-3 py-3">
                                    <input type="button" class="btn btn-default btn-wawooh" id="update-quantity"
                                           value="UPDATE STOCK">
                                </div>
                                @endif
                                @endif--}}
                                @if(isset($product->productAttributeDTOS[0]->productSizes))
                                    @foreach($product->productAttributeDTOS[0]->productSizes as $productSize)
                                        <div class="thumbD thumbOneSize">
                                            <span>{{$productSize->name}}</span>
                                            <span>{{$productSize->numberInStock}}</span>
                                        </div>
                                    @endforeach
                                @endif

                                @if(isset($product->productAttributeDTOS[1]->productSizes))
                                    @foreach($product->productAttributeDTOS[1]->productSizes as $productSize)
                                        <div class="hide thumbD thumbTwoSize">
                                            <span>{{$productSize->name}}</span>
                                            <span>{{$productSize->numberInStock}}</span>
                                        </div>
                                    @endforeach
                                @endif

                                @if(isset($product->productAttributeDTOS[2]->productSizes))
                                    @foreach($product->productAttributeDTOS[2]->productSizes as $productSize)
                                        <div class="hide thumbD thumbThreeSize">
                                            <span>{{$productSize->name}}</span>
                                            <span>{{$productSize->numberInStock}}</span>
                                        </div>
                                    @endforeach
                                @endif

                                @if(isset($product->productAttributeDTOS[3]->productSizes))
                                    @foreach($product->productAttributeDTOS[3]->productSizes as $productSize)
                                        <div class="hide thumbD thumbFourSize">
                                            <span>{{$productSize->name}}</span>
                                            <span>{{$productSize->numberInStock}}</span>
                                        </div>
                                    @endforeach
                                @endif

                                @if(isset($product->productAttributeDTOS[4]->productSizes))
                                    @foreach($product->productAttributeDTOS[4]->productSizes as $productSize)
                                        <div class="hide thumbD thumbFiveSize">
                                            <span>{{$productSize->name}}</span>
                                            <span>{{$productSize->numberInStock}}</span>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12 product-size selected-measurement">
                            <h6><strong><u>Selected Measurements</u></strong></h6>
                            @if(isset($reqMes))
                                @if(count($reqMes) > 0)
                                    <ul>
                                        @foreach($reqMes as $measure)
                                            <li>{{strtolower($measure)}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <label class="label label-rejected">No Selected Measurement</label>
                                @endif
                            @endif
                        </div>

                        <div class="col-md-12">


                            <a href="{{route('designer.convert.bespoke', [str_slug($storeName),str_slug($product->name), $product->id])}}">Convert To Bespoke</a>


                        </div>


                        <div class="">

                        </div>
                    </div>
                </div>

                <div class="row available-mat wow animated fadeInRight" data-wow-delay=".4s">
                    <div class="col-md-6 col-12" style="border-right: 1px solid rgba(0,0,0,.4);">
                        <div class="available">
                            <h5 class="productDetailsLabel text-center">Available Artworks</h5>
                            @if(isset($product->bespokeProductDTO->artPictureDTOS) && is_array($product->bespokeProductDTO->artPictureDTOS))
                                @if(count($product->bespokeProductDTO->artPictureDTOS))
                                    <div class="row" style="margin-bottom: 5px;">
                                        @foreach($product->bespokeProductDTO->artPictureDTOS as $artwork)
                                            <div class="col-md-3">
                                                <div><img class='img-fluid'
                                                          src='{{str_replace('http','https', @$artwork->artWorkPicture)}}'/>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @else
                                <label class="label label-inspection text-center">No Artwork Found</label>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="available">
                            <h5 class="productDetailsLabel text-center">Available Fabric</h5>
                            @if(isset($product->bespokeProductDTO->materialPicture) && is_array($product->bespokeProductDTO->materialPicture))
                                @if(count($product->bespokeProductDTO->materialPicture))
                                    <div class="row">
                                        @foreach($product->bespokeProductDTO->materialPicture as $material)
                                            <div class="col-md-3">
                                                <div>
                                                    <img class='img-fluid'
                                                         src='{{str_replace('http','https',@$material->materialPicture)}}'/>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @else
                                <label class="label label-inspection text-center">No Fabric Found</label>
                            @endif
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container seller-edit pull-right wow animated fadeInRight" data-wow-delay=".5s">
                    <a href='{{route('designer.edit', [str_slug(strtolower($storeName)),str_slug($product->name),$product->id])}}'
                       class="btn btn-wawooh">Edit Product</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(".add-stock").click(function () {
            stock = $(this).closest('.stock ').children('.stockNo');
            currentStock = parseInt(stock.val());
            newStock = ++currentStock;

            stock.val(newStock);
        });

        $('.addProductAttribute').click(function () {
            if ($(this).attr('productcount') === '0') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbOne img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-one').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbOneSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '1') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbTwo img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-two').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbTwoSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '2') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbThree img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-three').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbThreeSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '3') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbFour img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-four').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbFourSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '4') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbFive img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-five').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbFiveSize').removeClass('hide');
            }

        });

        $(document).on('click', '.product-small-img-added2 div img, .product-small-img-added3 div img, .product-small-img-added4 div img, .product-small-img-added5 div img', function () {
            console.log($(this).attr('src'));
            var pictureSwap = $(this).attr('src');
            $('#mainPicture').attr('src', pictureSwap);
        });

        $('.product-small-img >div >img').on('click', function () {
            console.log('fjsjjs');
            let src = $(this).attr('src');
            let url = '' + src;
            $('.product-small-img >div >img').css('border', 'none').removeClass('brand-bd-color');

            $(this).css({
                borderWidth: 1,
                borderStyle: "solid"
            }).addClass('thumbBorder');
            $(this).parents('.productThumb').siblings('.productDisplay').find('a').attr('href', src);
            $(this).parents('.productThumb').siblings('.productDisplay').find('img').attr('src', src);

        });

        $(".minus-stock").click(function () {
            stock = $(this).closest('.stock ').children('.stockNo');
            currentStock = parseInt(stock.val());
            newStock = --currentStock;

            if (newStock >= 0) {
                stock.val(newStock);
            } else {
                stock.notify("Stock can't be less than 0");
            }
        });

        var productId = parseInt('{{$product->id}}');
        var updateFlag = false;

        $("#update-quantity").click(function () {
            $('#update-quantity').val('Updating Stock...').attr('disabled', 'disabled');
            if (updateFlag) {
                $(this).notify("Please wait ...... don't click again", "primary");
                $.notify("Please wait ...... don't click again", "primary");
            } else {
                updateFlag = true;
                var productSizes = [];
                $.each($(".stock"), function () {
                    productSizes.push({
                        "name": $(this).children('.stockNo').attr('id'),
                        "stockNo": parseInt($(this).children('.stockNo').val())
                    });
                });

                data = {
                    'id': productId,
                    'productSizes': productSizes
                }
                var tok = decodeURI(<?= session('designerToken') ?>);
//console.log();
                if (typeof tok === 'undefined') {
                    var tok = '';
                }
//console.log(tok);

                $.ajax({
                    url: '{{env('GET_BASE_URL')}}/fashion/product/updateproductstock',
                    method: 'POST',
                    data: JSON.stringify(data),
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    success: function () {
                        $('#update-quantity').val('UPDATE STOCK').removeAttr('disabled', 'disabled');
                        $.notify("Stock has been updated", "success");
                        updateFlag = false;
                    },
                    error: function () {
                        $('#update-quantity').val('UPDATE STOCK').removeAttr('disabled', 'disabled');
                    }
                });
            }
        });
    </script>
@endpush