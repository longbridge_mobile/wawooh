@extends('layouts.designer.designer')

@push('styles')
    <link rel="stylesheet" href="{{asset('css/dash/ekko-lightbox.css')}}">
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }

        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 51px;
            height: 51px;
            margin: 6px;
            border: 6px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }

        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }

        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }

        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }

        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .ld-spinners {
            background: #cd9933;
            color: #cd9933;
        }
    </style>

@endpush

@section('pageTitle' , $bespokeOrderDetail->productName .' | Order Details')
@section('content')
    <div class="content-wrapper white orderStatusVendor">
        <div class="row grid-margin">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Order status</h6>
                        <div class="product-image">
                            {{--<h3>Product Picture</h3>--}}
                            <div class="main-product-img">
                                <a href="{{@$bespokeOrderDetail->bespokePicture[0]}}" data-toggle="lightbox">
                                    <img class="prodImg" src="{{@$bespokeOrderDetail->bespokePicture[0]}}" alt="">
                                </a>
                            </div>
                        </div>

                        <div class="col-md-7 orderStatusDetails">
                            <table class="table">
                                <tr>
                                    @if(@$bespokePicture->status == 'P')
                                        <td>Order Status</td>
                                        <td><label class="label label-pending">Pending</label></td>
                                    @elseif(@$bespokePicture->status == 'PC')
                                        <td>Order Status</td>
                                        <td><label class="label label-completed">Payment Confirmed</label></td>
                                    @elseif(@$bespokePicture->status == 'OP')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Processing</label></td>
                                    @elseif(@$bespokePicture->status == 'A')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Accepted</label></td>
                                    @elseif(@$bespokePicture->status == 'WR')
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Wawooh Rejected</label></td>
                                    @elseif(@$bespokePicture->status == 'WC')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Wawooh Collected</label></td>
                                    @elseif(@$bespokePicture->status == 'CO')
                                        <td>Order Status</td>
                                        <td><label class="label label-completed">Completed</label></td>
                                    @elseif(@$bespokePicture->status == 'RS')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Ready for Shipping</label></td>
                                    @elseif(@$bespokePicture->status == 'RI')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Ready for Inspection</label></td>
                                    @elseif(@$bespokePicture->status == 'OS')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Order Shipped</label></td>
                                    @elseif(@$bespokePicture->status == 'C')
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Cancelled</label></td>
                                        @else
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Bespoke Product</label></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Order Number</td>
                                    <td>{{@$bespokeOrderDetail->orderNum}}</td>
                                </tr>
                                <tr>
                                    <td>Product Details/Name</td>
                                    <td>{{@$bespokeOrderDetail->productName}}</td>
                                </tr>
                               {{-- <tr>
                                    <td>Order Date</td>
                                    <td>{{Carbon\Carbon::parse(@$bespokeOrderDetail->orderDate)->format('d-m-Y')}}
                                        <small class="text-success">({{\Carbon\Carbon::parse(@$bespokeOrderDetail->orderDate)->diffForHumans()}})</small>
                                    </td>
                                </tr>--}}
                                <tr>
                                    <td>Amount</td>
                                    <td>₦{{number_format(@$bespokeOrderDetail->price)}}</td>
                                </tr>
                                <tr>
                                    <td>Size</td>
                                    <td>{{'Bespoke'}}</td>
                                </tr>
                               {{-- <tr>
                                    <td>Measurement</td>
                                    @if(!is_null($order->measurement))
                                        <td><a href="#" data-toggle="modal" data-target="#measurementModal">Measurement
                                                Available</a></td>
                                    @else
                                        <td><a href="#" data-toggle="modal" data-target="#">No Measurement
                                                Available</a>
                                        </td>
                                    @endif
                                </tr>--}}
                               {{-- <tr>
                                    <td>Notes</td>
                                    @if(!is_null($order->notes))
                                        <td>{{$order->notes}}</td>
                                    @else
                                        <td>No Notes</td>
                                    @endif
                                </tr>--}}
                            </table>
                        </div>

                        <div class="artwork-image">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3>Artwork Image</h3>
                                    @if(isset($bespokeOrderDetail->arkworkPicture))
                                        @foreach($bespokeOrderDetail->arkworkPicture as $artW)
                                        <a href="{{asset('img/gallery6.jpg')}}" data-toggle="lightbox">
                                            <img class="img-class" src="{{$artW}}" alt="">
                                        </a>
                                        @endforeach
                                    @else
                                        <p class="text-centerC">No ArtWork Available</p>
                                    @endif
                                </div>
                                <div class="offset-2 col-md-4">
                                    <h3>Fabric Image</h3>
                                    @if(isset($bespokeOrderDetail->fabricDetailsDTO))
                                        @foreach($bespokeOrderDetail->fabricDetailsDTO as $fabricImg)
                                        <a href="{{asset('img/gallery4.jpg')}}" data-toggle="lightbox">
                                            <img class="img-class" src="{{$fabricImg->fabricPicture}}" alt="">
                                        </a>
                                        @endforeach
                                    @else
                                        <p class="text-centerC">No Fabric Available</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            @if($bespokeOrderDetail->status === 'Processing')
                                <div>
                                <p>Status: Waiting on designer decision</p>
                                <button class="btn btn-wawooh bespokeDone">Done</button>
                                </div>
                                @else
                                <div>
                                <p>Status: Completed by designer</p>
                                <button class="btn btn-wawooh bespokeDone">Cancel</button>
                                </div>
                                @endif
                        </div>



                       {{-- <div class="col-md-7 orderStatusDetails">


                            --}}{{-- <div class="actions">
                                 <input type="button" data-toggle="modal" data-target="#myModal" class="btn btn-doneAccept pull-right" value="Design Completed">
                             </div>--}}{{--
                            --}}{{-- <div class="actions">
                                 <button data-toggle="modal" data-target="#myModal" class="btn btn-doneAccept pull-right">Design Completed</button>
                             </div>--}}{{--
                            <div class="row">
                                @if($order->status == 'P')
                                    <div class="col-md-9 actions">
                                        <button designer="A" statusid="22" class="btn btn-doneAccept pull-right">
                                            Accept
                                        </button>
                                    </div>
                                    <div class="col-md-3 actions">
                                        <button designer='OR' statusid="4" class="btn btn-doneAccept">Decline</button>
                                    </div>
                                @elseif($order->status == 'PC')
                                    <div class="actions">
                                        <button designer="OP" statusid="3" class="btn btn-doneAccept pull-right">
                                            Start Processing
                                        </button>
                                    </div>
                                @elseif($order->status == 'WR')
                                    <div class="actions">
                                        <button designer="OP" statusid="3" class="btn btn-doneAccept pull-right">
                                            Start Processing
                                        </button>
                                    </div>
                                @elseif($order->status == 'OP')
                                    <div class="actions">
                                        <button data-toggle="modal" data-target="#myModal" designer="" statusid=""
                                                style="color: #ffffff; background: black;border-bottom-width: 3px;border-color: #cd9933;"
                                                class="btn btn-doneAcceptx pull-right">Design Completed
                                        </button>
                                    </div>

                                @elseif($order->status == 'CO')
                                    <div class="hide" hidden>
                                        Status:<label class="label">Completed</label>
                                    </div>
                                @endif
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>



        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/dash/lightbox-ekko.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.btn-doneAccept').click(function () {

                $('#done').attr("disabled", "disabled");
                let data = {
                    'id': '{{@$order->id}}',
                    'status': $(this).attr('designer'),
                    'productName': '{{@$order->productName}}',
                    'productId': '{{@$order->productId}}',
                    'statusId': $(this).attr('statusid'),
                    'orderNumber': '{{@$order->orderNumber}}',
                    'customerId': '{{@$order->customerId}}'
                };
                // console.log(data);
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No", "Yes"]
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                url: '{{route('designer.acceptOrder')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (res) {
                                    // $('#loaderModal').modal('show');
                                    // console.log(res);
                                    if (res.status === '00') {
                                        $('#loaderModal').hide();
                                        swal("Order Updated Successfully", "", "success");
                                        window.location.reload();
                                    } else if(res.status === '99') {
                                        swal(res.message,'','error');
                                        $('#loaderModal').hide();
                                    }
                                },
                                error: function (e) {
                                    swal(e, '','error');
                                    $('#loaderModal').hide();
                                    $('#done').removeAttr("disabled", "disabled");
                                }
                            })
                        }
                        else {
                            $('#done').removeAttr("disabled", "disabled");
                        }
                    })
            });

            $('.addDesignerProduct').on('change', function () {
                var file = $(this).get(0).files[0];
                if (file.size / 1024 > 120000) {
                    $.notify("Cannot upload image size more than 2MB");
                } else {
                    convertMoreToBase64(this, '.designerProduct');
                    $(this).siblings('img').addClass('uploadedImage');
                }
            });

            $('.btn-doneReject').on('click', function () {
                var data = {
                    'id': '{{@$order->id}}',
                    'productName': '{{@$order->productName}}',
                    'productId': '{{@$order->productId}}',
                    'statusId': '4',
                    'orderNumber': '{{@$order->orderNumber}}',
                    'status': 'OR',
                    'message': '',
                    'messageText': '',
                    'messageId': '',
                    'waitTime': '',
                    'customerId': '{{@$order->customerId}}'
                };
                $.ajax({
                    url: '{{route('designer.acceptOrder')}}',
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (res) {
                        // console.log(res);
                        if (res.status === "00") {
                            $('#orderDeclineBtn').val('Decline').removeAttr('disabled', 'disabled');
                            swal('Order Decline Successfully', '', 'success');
                            location.reload();

                        } else {
                            $('#orderDeclineBtn').val('Decline').removeAttr('disabled', 'disabled');
                            swal('Something went wrong', '', 'error');
                            //$(this).attr('disabled', false);
                        }
                    },
                    error: function (e) {
                        swal('Unable to complete this operation, please try again later', '', 'error');
                        $('#orderDeclineBtn').val('Decline').removeAttr('disabled', 'disabled');
                        //$(this).attr('disabled', false);
                    }

                })
            });


            $(document).on('click', '.bespokeDone', function(){

               var data = {
                   bespokeRequestId: '{{$bespokeOrderDetail->bespokeRequestId}}'
               };

               if('{{$bespokeOrderDetail->status}}' === 'Processing'){
                   data.completedBespokeOrder = true;
                }else{
                   data.completedBespokeOrder = false;
               }

                console.log(data);

               $.ajax({
                   url: '{{route('designer.accept.bespokeOrder')}}',
                   type: 'POST',
                   data: JSON.stringify(data),
                   success: function(res) {
                       console.log(res);
                       if (res.status === '00') {
                           swal("Order Updated Successfully", "", "success");
                           window.location.reload();
                       }
                   },
                   error: function(err){
                       console.log(err);
                   }
               })


            });


            $(document).on('click', '.doneAccepted', function () {
                $('.doneAccepted').attr('disabled', true);
                $('.lds-ring').removeClass('hide');


                var picture = [];
                $('.uploadedImage').each(function (value) {
                    picture.push($(this).attr('src'));
                });

                $('#done').attr("disabled", "disabled");
                let data = {
                    'id': '{{@$order->id}}',
                    'status': $(this).attr('designer'),
                    'productName': '{{@$order->productName}}',
                    'productId': '{{@$order->productId}}',
                    'statusId': $(this).attr('statusid'),
                    'orderNumber': '{{@$order->orderNumber}}',
                    'customerId': '{{@$order->customerId}}',
                    'pictures': picture
                };
                // console.log(data)
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No", "Yes"]
                })
                    .then((confirm) => {
                        if (confirm) {

                            $.ajax({
                                url: '{{route('designer.acceptOrder')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (res) {
                                    $('.doneAccepted').attr('disabled', false);
                                    $('.lds-ring').addClass('hide');
                                    if (res.status === '00') {
                                        swal("Order Updated Successfully", "", "success");
                                        window.location.reload();
                                    }
                                },
                                error: function () {
                                    $('#done').removeAttr("disabled", "disabled");
                                    $('#loader').hide();
                                    $('.doneAccepted').attr('disabled', false);
                                    $('.lds-ring').addClass('hide');
                                }
                            })

                        } else {
                            $('#done').removeAttr("disabled", "disabled");
                        }
                    })
            });

            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });

            $('#convert').click(function () {
                $('.measurementTable tbody tr .measureValue').each(function () {
                    if ($(this).html() !== '') {
                        $val = $(this).html() / 2.54;
                        $(this).html($val.toFixed(2));
                    }


                });
                /*if ($('.custom').val()) {
                    oldValue = $('.custom').val();
                    $('.custom').val($('.custom').val() * 5);
                }*/
                $(this).addClass('hide');
                $('#convertBack').removeClass('hide');
                $('#default').html('inches');
            });

            $('#convertBack').click(function () {
                $('.measurementTable tbody tr .measureValue').each(function () {
                    if ($(this).html() !== '') {
                        $val = $(this).html() * 2.54;
                        $(this).html($val.toFixed());
                    }
                });
                $(this).addClass('hide');
                $('#convert').removeClass('hide');
                $('#default').html('centimeters');
            });


            /*let measurement = JSON.parse(measurement);
              console.log(measurement);

            if (measurement != null) {
                unit = measurement.unit;
                var keys = Object.keys(measurement);
                for (let value of keys) {
                    $('#' + value).val(measurement[value]);
                }
                $('#unit').text(measurement.unit);
            }*/
        });
    </script>
@endpush
