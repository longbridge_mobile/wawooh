@extends('layouts.designer.designer')

@push('styles')

@endpush

@section('pageTitle','Account Settings')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin">
            <h3>Account Settings</h3>
            <hr>
            <div class="col-md-12 update-form-account">
                <form class="">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Update your Password</h3>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="oldPassword" class="">Old Password</label>
                                <input type="password" id='oldPassword' placeholder="Old password"
                                       class="form-control" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="newPassword" class="">New Password</label>
                                <input type="password" id='newPassword' placeholder="New password"
                                       class="form-control" required="required">
                            </div>
                        </div>

                        <div class="col-md-12 text-centerC">
                            <div id='loader2' style='height:50px' class='text-center hide '>
                                <img style='width:auto;height:100%' class='center-block'
                                     src='{{asset('img/loader.gif')}}'/>
                            </div>
                            <div class="form-group">
                                <input id="changePwd" type="button" class="btn btn-update save" value="Update Password">
                            </div>
                        </div>

                    </div>
                </form>
            </div>

            <hr>

            <div class="col-md-12 update-form-account">
                <form class="">
                    <div class="row">
                        <div class="col-md-6 offset-3">
                            <div class="form-group">
                                <label for="new-email" class="">Enter your new email Address</label>
                                <input type="email" id='new-email' placeholder="Enter new email address"
                                       class="form-control" value="{{$designer->email}}" required="required">
                            </div>
                        </div>

                        <div class="col-md-12 text-centerC">
                            <div class="form-group">
                                <input type="button" id='confirmChange' value="Confirm Change"
                                       class="btn btn-update changeEmail">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="modal fade" data-backdrop="static" id="tokenModal" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="basicInfo" style="clear:both; padding: 10px; text-align: center;">
                        <form action="" class="row form-inline">
                            <div class="col-lg-7">
                                <div class="form-group">
                                    <label for="">Enter the token sent to your phone</label>
                                    <input type="number" id="confirmToken" value="" class="form-control">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                {{--<div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Submit</button>
                </div>--}}
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $(document).ready(function () {
            let tok = decodeURI(<?= session()->has('designerToken') ?>);
            $('.save').on('click', function (e) {
                $('#changePwd').val('Processing...').attr('disabled', 'disabled');
                let oldPassword = $('#oldPassword').val();
                let newPassword = $('#newPassword').val();
                e.preventDefault();
                if (oldPassword === '' || newPassword === '') {
                    swal('Field is required', '', 'warning');
                    $('#changePwd').val('Update Password').removeAttr('disabled', 'disabled');
                } else {
                    let data = {
                        "email": "{{$designer->email}}",
                        "oldPassword": oldPassword,
                        "newPassword": newPassword,
                    };
                    console.log(data);
                    $.ajax({
                        url: '{{route('change.password')}}',
                        method: 'POST',
                        data: JSON.stringify(data),
                        headers: {
                            'Authorization': tok,
                            'Content-Type': 'application/json; charset=utf-8',
                            'accept': 'application/json'
                        },
                        success: function (result) {
                            if (result.status === "00") {
                                $(this).val('Password Changed').attr('disabled', 'disabled');
                                window.location.href = '{{route('designersLandingPage')}}';
                                location.reload();
                            } else if (result.status === '99') {
                                swal('Something went wrong', '', 'error');
                                $('#changePwd').val('Update Password').removeAttr('disabled', 'disabled');
                            }
                        },
                        error: function (e) {
                            $('#changePwd').val('Update Password').removeAttr('disabled', 'disabled');
                            console.log(e);
                        }
                    });
                }

            });

            $('.changeEmail').on('click', function () {
                $('#confirmChange').val('Processing...').attr('disabled', 'disabled');
                let userEmail = $('#new-email').val();
                if (userEmail === '') {
                    swal("You can't submit empty field", "", "warning");
                    $('#confirmChange').val('Confirm Change').removeAttr('disabled', 'disabled');
                } else {
                    let data = {
                        "email": userEmail
                    };
                    console.log(data);
                    $.ajax({
                        url: `{{route('changeDesignerEmail')}}`,
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        data: JSON.stringify(data),
                        type: "POST",
                        success: function (result) {
                            console.log(result);
                           if(result.status === "50") {
                                $('#tokenModal').modal('show');
                            }
                            else {
                                $('#confirmChange').val('Confirm Change').removeAttr('disabled', 'disabled');
                            }
                        },
                        error: function (e) {
                            $('#confirmChange').val('Confirm Change').removeAttr('disabled', 'disabled');
                            console.log(e);
                        }
                    })
                }
            })
        });
    </script>
@endpush