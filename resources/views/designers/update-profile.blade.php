@extends('layouts.designer.designer')

@push('styles')

@endpush

@section('pageTitle', 'Update Profile')
@section('content')

    <div class="content-wrapper white">
        <div class="row grid-margin">
            <h3>Update Profile</h3>
            {{--<span class="label label-info">Here you can update your profile details</span>--}}
            <div class="col-md-12 update-form">
                <!-- multistep form -->
                <form>
                    <!-- progressbar -->
                    <ul class="progress-title">
                        <li class="active">Personal Information</li>
                        <li>Business Information</li>
                        <li>Account Details</li>
                    </ul>
                    <!-- fieldsets -->
                    <fieldset id="personalsInfo">
                        <div class="section-title">
                            <span>Step 1: Personal Information</span>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">LastName</label>
                                    <input placeholder='Last Name' id='ln' type='text'
                                           class='form-control profile-input' value='{{@$designer->lastName}}'/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">FirstName</label>
                                    <input placeholder='First Name' id='fn' type='text'
                                           class='form-control profile-input'
                                           value='{{@$designer->firstName}}'/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="dateOfBirth">Date of Birth (dd/mm)</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?php
                                                $day = 0;
                                                for ($days = 01; $days <= 31; $days++) {
                                                    $day .= "<option value='$days'>" . $days . "</option>";
                                                }
                                                ?>
                                                <label for="day" class="sr-only">Day</label>
                                                <select name="day" id="day" class="form-control">
                                                    <option value="0" hidden> Day</option>
                                                    <?= $day; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="month" id="month" class="form-control">
                                                <option value="" hidden>Month</option>
                                                <option value="01">Jan</option>
                                                <option value="02" id="febHide">Feb</option>
                                                <option value="03">Mar</option>
                                                <option value="04" id="man30">Apr</option>
                                                <option value="05">May</option>
                                                <option value="06" id="man30">Jun</option>
                                                <option value="07">Jul</option>
                                                <option value="08">Aug</option>
                                                <option value="09" id="mon30">Sept</option>
                                                <option value="10">Oct</option>
                                                <option value="11" id="mon30">Nov</option>
                                                <option value="12">Dec</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select name="year" id="birthYear" class="form-control">
                                                <option value="" hidden>Year</option>
                                                <?php
                                                $year = '';                                              $yearsOption = '';
                                                $yearFrom = 1960;
                                                $endYear = 2002;
                                                $years = range($yearFrom, $endYear);
                                                foreach ($years as $year) {
                                                    //echo $year;
                                                    echo "<option value='$year'>" . $year . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{--<input placeholder='Date of Birth (dd/mm)' id='dateOfBirth'
                                       type='text' class='form-control profile-input' value=''/>--}}
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select name="gender" id="gender" class="form-control profile-input">
                                        @if(isset($designer->gender) == 'Male')
                                            <option value="Male" selected>Male</option>
                                        @elseif(isset($designer->gender) == 'Female')
                                            <option value="Female" selected>Female</option>
                                        @elseif(isset($designer->gender) == 'Other')
                                            <option value="Other" selected>Choose not to specify</option>
                                        @else
                                            <option value="" hidden>--Select Gender--</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                            <option value="Other">Choose not to specify</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Phone Number</label>
                                    <input id='pnum' placeholder='Phone Number' readonly type='tel'
                                           class='form-control profile-input' value='{{@$designer->phoneNo}}'/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Email Address</label>
                                    <input placeholder='email' id='email' readonly type='email'
                                           class='form-control profile-input' value='{{@$designer->email}}'/>
                                </div>
                            </div>
                            {{--<div class="col-md-8">
                                <div class="form-group">
                                    <label for="about-you">Tell us about yourself</label>
                                    <textarea name="" class="form-control" id="about-you" cols="20" rows="5"
                                              placeholder=""></textarea>
                                </div>
                            </div>--}}
                        </div>
                        <input type="button" id="SubmitPersonal" name="next" class="btn next btn-prev pull-right"
                               value="Save and Continue"/>
                        <input type="button" name="next" class="next btn btn-update personalUpdate" value="Next"/>

                    </fieldset>
                    <fieldset id="businessesInfo">
                        <div class="section-title">
                            <span>Step 2: Business Information</span>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Business/Brand Name <span
                                                class="req">*</span></label>
                                    <input placeholder='Store Name' id='sN' type='text'
                                           class='form-control toUpper profile-input'
                                           value='{{@$designer->storeName}}' onkeydown="changeToUpper(this)"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Business Email Address</label>
                                    <input placeholder='Business Email Address' id='businessEmail' type='email'
                                           class='form-control profile-input' readonly value='{{@$designer->email}}'
                                           required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Business Phone Number </label>
                                    <input placeholder='Enter Business Phone Number' id='businessPhone' type='tel'
                                           class='form-control profile-input' readonly value='{{@$designer->phoneNo}}'
                                           required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="businessAddress">Business Physical Address <span
                                                class="req">*</span></label>
                                    <input type="text" id="businessAddress" class="form-control"
                                           placeholder="Enter Business physical address"
                                           value="{{@$designer->address}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="country">Country <span class="req">*</span></label>
                                    <select name="" id="country" class="form-control" required>
                                        <option value="" hidden>--Select Country--</option>
                                        <option value="{{@$designer->country }}"
                                                selected>{{@$designer->country}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="state">State/Region <span class="req">*</span></label>

                                    <select name="" id="state" class="form-control" required>
                                        @if(isset($designer->state))
                                            <option value="{{$designer->state}}">{{$designer->state}}</option>
                                        @else
                                            <option value="" hidden>--Select State--</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="lga">Local Government <span class="req">*</span></label>
                                    <select name="" id="lga" class="form-control" required>
                                        @if(isset($designer->localGovt))
                                            <option value="{{$designer->localGovt}}">{{$designer->localGovt}}</option>
                                        @else
                                            <option value="" hidden>--Select Local Government--</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="threshold">Bespoke Production Threshold <span
                                                class="req">*</span></label>
                                    <input type="number" class="form-control" id="threshold" disabled
                                           placeholder="Enter Threshold"
                                           value="1">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="businessState" id="status" class="form-control" required>
                                        @if(@$designer->registeredFlag == 'Y')
                                            <option value="Y" selected>Registered</option>
                                            <option value="N">Not Registered</option>
                                        @elseif(@$designer->registeredFlag == 'N')
                                            <option value="Y">Registered</option>
                                            <option value="N" selected>Not Registered</option>
                                        @else
                                            <option value="" hidden>--Status--</option>
                                            <option value="N">Not Registered</option>
                                            <option value="Y">Registered</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4" id="businessID"
                                 style="display: {{@$designer->registeredFlag == 'Y' ? 'none' : 'block'}}">
                                <div class="form-group">
                                    <label for="businessDoc">Upload Business Proof (ID Card)<span
                                                class="req"></span>
                                        <img id="businessDocPix" hidden
                                             src="{{str_replace('http','https', @$designer->registrationDocument) && @$designer->registeredFlag == 'N' ? str_replace('http','https', @$designer->registrationDocument) : asset('img/material1.jpg')}}"
                                             class="img-control">
                                        @if(@$designer->registrationDocument && @$designer->registeredFlag == 'N')
                                            <p class="alert alert-primary" role="alert">Document Uploaded<i
                                                        class="fa fa-check"></i></p>
                                        @endif
                                        <p class="alert alert-primary hide" role="alert">Document Uploaded<i
                                                    class="fa fa-check"></i></p>

                                        <input type="file" style="margin-top: 1em;" class="form-control businessDocPix"
                                               id="businessDoc" name="businessDoc">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row animated fadeInLeft" id="businessProof"
                             style="display: {{@$designer->registeredFlag == 'Y' ? 'flex' : 'none'}}">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Business Registration Number </label>
                                    <input placeholder='Business Registration Number' id='businessRegNumber' type='text'
                                           class='form-control profile-input'
                                           value='{{@$designer->registrationNumber}}'>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="businessDocSec">{{@$designer->registeredFlag && @$designer->registrationDocument ? 'View':''}}
                                        Upload Certificate of Incorporation<span class="req">*</span>
                                        <input type="file" class="form-control IncorpoDocument">
                                        <img id="businessDocSecImage" hidden
                                             src="{{str_replace('http','https', @$designer->registrationDocument) && @$designer->registeredFlag == 'Y' ? str_replace('http','https',@$designer->registrationDocument) : asset('img/material1.jpg')}}"
                                             class="img-control">
                                        @if(@$designer->registrationDocument && @$designer->registeredFlag == 'Y')
                                            <p class="alert alert-primary bd {{@$designer->registrationDocument && @$designer->registeredFlag == 'Y' ? ' ' : 'hide'}}"
                                               role="alert">Document uploaded<i class="fa fa-check"></i></p>
                                        @endif
                                        <p class="alert alert-primary bd hide" role="alert">Document uploaded <i
                                                    class="fa fa-check"></i></p>
                                        <input style="display: none;" type="file"
                                               class="form-control businessDocSecImage"
                                               id="businessDocSec" name="businessDocSec">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="sizeChart">
                                        <small>Do you have a size guide or chart? <span class="req">*</span>
                                        </small>
                                    </label>
                                    <select name="size-chart" id="sizeChart" class="form-control" required>
                                        @if(@$designer->sizeGuideFlag == 'Y')
                                            <option value="Y" selected>Yes</option>
                                            <option value="N">No</option>
                                        @elseif(@$designer->sizeGuideFlag == 'N')
                                            <option value="Y">Yes</option>
                                            <option value="N" selected>No</option>
                                        @else
                                            <option value="" hidden>--Select--</option>
                                            <option value="Y">Yes</option>
                                            <option value="N">No</option>
                                        @endif

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8" id="actionBtn" style="padding-top: 21px; font-size: 13px;">
                               {{-- <strong>
                                    <a href="#" class="btn btn-primary btn-lg" data-toggle="modal"
                                       data-target="#sizeModal">Select</a>
                                </strong>--}}
                                <span>By not uploading your size chart, you agree to  make all products in accordance to wawooh size chart,
                                    <a target="_new" href="{{route('sizeChart')}}">click here to view</a>
                                </span>
                            </div>

                            @if(@$designer->sizeGuideFlag === 'Y')
                                <div class="col-md-4" id="sizeForm" style="padding-top: 27px;">
                                    <strong>
                                        <a href="#" class="btn btn-primary btn-lg" data-toggle="modal"
                                           data-target="#sizeUploadModal">Click to Upload</a>
                                    </strong>
                                </div>
                            @else
                                <div class="col-md-4" id="sizeForm" style="display: none;  padding-top: 27px;">
                                    <strong>
                                        <a href="#" class="btn btn-primary btn-lg" data-toggle="modal"
                                           data-target="#sizeUploadModal">Click to Upload</a>
                                    </strong>
                                </div>
                            @endif

                            <div class="col-md-12 ala">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="mc hide">Male Chart</h3>
                                        <img src="{{@$designer->maleSizeGuide ? str_replace('upload','upload/q_40',@$designer->maleSizeGuide) : ''}}"
                                             id="malChartPark" class="maleCharts" height="200">
                                    </div>
                                    <div class="col-md-6">
                                        <h3 class="mf hide">Female Chart</h3>
                                        <img src="{{@$designer->femaleSizeGuide ? str_replace('upload','upload/q_40',@$designer->femaleSizeGuide) : ''}}"
                                             id="femChartPark" class="femaleCharts" height="200">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <input style="margin-top:20px;" type="button" name="previous"
                               class="previous btn btn-prev pull-left" value="Previous"/>
                        <input style="margin-top:20px; margin-left: 10px;" type="button" name="next"
                               class="next btn btn-update pull-left businessInfo"
                               value="Next"/>

                        <input style="margin-top:20px;" type="button" name="next"
                               class="btn btn-update pull-right businessInfoReal"
                               value="Save and Continue"/>
                    </fieldset>
                    <fieldset id="accountsInfo">
                        <div class="section-title">
                            <span>Step 3: Account Information</span>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Bank Account Number<span class="req">*</span></label>
                                    <input placeholder='Bank Account Number' id='bankAccNum' type='text'
                                           class='form-control profile-input' value='{{@$designer->accountNumber}}'
                                           maxlength="10" minlength="10"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Bank Account Name <span class="req">*</span></label>
                                    <input placeholder='Bank Account Name' id='bankAccName' type='text'
                                           class='form-control profile-input'
                                           value='{{@$designer->accountName}}'/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="desBank">Select Bank <span class="req">*</span></label>
                                    <select name="desBank" id="desBank" class="form-control profile-input">
                                        <option value="" hidden> --Select Bank--</option>
                                        <option value="Access Bank" {{@$designer->bankName == 'Access Bank'? 'selected': '' }}>
                                            Access Bank
                                        </option>
                                        <option value="CitiBank" {{@$designer->bankName == 'CitiBank'? 'selected': '' }}>
                                            Citibank
                                        </option>
                                        <option value="Diamond Bank" {{@$designer->bankName == 'Diamond Bank'? 'selected': '' }}>
                                            Diamond Bank
                                        </option>
                                        <option value="Ecobank" {{@$designer->bankName == 'Ecoank'? 'selected': '' }}>
                                            Ecobank
                                        </option>
                                        <option value="Fidelity" {{@$designer->bankName == 'Fidelity Bank'? 'selected': '' }}>
                                            Fidelity Bank
                                        </option>
                                        <option value="Fidelity" {{@$designer->bankName == 'First Bank'? 'selected': '' }}>
                                            First Bank
                                        </option>
                                        <option value="FCMB" {{@$designer->bankName == 'FCMB'? 'selected': '' }}>First
                                            City Monument Bank (FCMB)
                                        </option>
                                        <option value="FSDH Merchant Bank" {{@$designer->bankName == 'FSDH Merchant Bank'? 'selected': '' }}>
                                            FSDH Merchant Bank
                                        </option>
                                        <option value="Guarantee Trust Bank" {{@$designer->bankName == 'Guarantee Trust Bank'? 'selected': '' }}>
                                            Guarantee Trust Bank
                                            (GTB)
                                        </option>
                                        <option value="Heritage Bank" {{@$designer->bankName == 'Heritage Bank'? 'selected': '' }}>
                                            Heritage Bank
                                        </option>
                                        <option value="Keystone Bank" {{@$designer->bankName == 'Keystone Bank'? 'selected': '' }}>
                                            Keystone Bank
                                        </option>
                                        <option value="Rand Merchant Bank" {{@$designer->bankName == 'Rand Merchant Bank'? 'selected': '' }}>
                                            Rand Merchant Bank
                                        </option>
                                        <option value="Polaris Bank" {{@$designer->bankName == 'Polaris Bank'? 'selected': '' }}>
                                            Polaris Bank
                                        </option>
                                        <option value="Stanbic IBTC Bank" {{@$designer->bankName == 'Stanbic IBTC Bank'? 'selected': '' }}>
                                            Stanbic IBTC Bank
                                        </option>
                                        <option value="Standard Chartered Bank">Standard Chartered
                                            Bank
                                        </option>
                                        <option value="Sterling Bank" {{@$designer->bankName == 'Sterling Bank'? 'selected': '' }}>
                                            Sterling Bank
                                        </option>
                                        <option value="Suntrust Bank" {{@$designer->bankName == 'Suntrust Bank'? 'selected': '' }}>
                                            Suntrust Bank
                                        </option>
                                        <option value="Union Bank" {{@$designer->bankName == 'Union Bank'? 'selected': '' }}>
                                            Union Bank
                                        </option>
                                        <option value="United Bank for Africa (UBA)" {{@$designer->bankName == 'United Bank for Africa (UBA)'? 'selected': '' }}>
                                            United Bank for Africa (UBA)
                                        </option>
                                        <option value="Unity Bank" {{@$designer->bankName == 'Unity Bank'? 'selected': '' }}>
                                            Unity Bank
                                        </option>
                                        <option value="Wema Bank" {{@$designer->bankName == 'Wema Bank'? 'selected': '' }}>
                                            Wema Bank
                                        </option>
                                        <option value="Zenith Bank" {{@$designer->bankName == 'Zenith Bank'? 'selected': '' }}>
                                            Zenith Bank
                                        </option>
                                        <option value="Others" {{@$designer->bankName == 'Others' ? 'selected': ''}}>
                                            Others
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4" id="otherBank">
                                <div class="form-group">
                                    <label for="other_bank">Enter the Bank Name</label>
                                    <input type="text" name="otherBank" id="other_bank"
                                           placeholder="Enter the Bank Name" class="form-control"
                                           value="{{$designer->bankName}}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="currency">Select Currency</label>
                                    <select name="currency" id="currency" class="form-control profile-input">
                                        <option value="" hidden>--Select Currency--</option>
                                        <option value="NGR" {{@$designer->currency == 'NGR'? 'selected': '' }}>
                                            ₦ Naira
                                        </option>
                                        {{--<option value="USD" {{@$designer->currency == 'USD'? 'selected': '' }}>
                                            &dollar; US Dollar
                                        </option>--}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4" id="swiftCode" style="display:none;">
                                <div class="form-group">
                                    <label for="gender">SWIFT Code <span class="req">*</span></label>
                                    <input placeholder='Enter Swift Code' id='swiftCode' type='text'
                                           class='form-control profile-input text-uppercase'
                                           value='{{@$designer->swiftCode}}'/>
                                    <small data-toggle="tooltip" data-placement="bottom"
                                           title="A SWIFT code is an international bank code that identifies particular banks worldwide. It’s also known as a Bank Identifier Code (BIC). CommBank uses SWIFT codes to send money to overseas banks.">
                                        What is SWIFT Code <i class="fa fa-question-circle"></i></small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="countryCode">Country <span class="req">*</span></label>
                                    <select name="" id="countryCode" class="form-control">
                                        <option value="" hidden>--Select Country--</option>
                                        <option value="{{@$designer->countryCode }}"
                                                selected>{{@$designer->countryCode}}</option>
                                    </select>
                                    {{--<label for="gender">Country<span class="req">*</span></label>
                                    <input placeholder='Enter Country Code' id='countryCode' type='text'
                                           class='form-control profile-input'
                                           value='{{$designer->countryCode}}'/>--}}
                                </div>
                            </div>
                        </div>
                        <input type="button" name="previous" class="previous btn btn-prev pull-left" value="Previous"/>
                        <input id="updateAccount" type="submit" name="submit"
                               class="submit btn btn-update pull-right update-profile"
                               value="Submit"/>
                    </fieldset>
                </form>
            </div>
        </div>
        <hr>
    </div>

    <!-- Modal -->
    <div class="modal hide fade" id="sizeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content size-style">
                <div class="modal-header">
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                    <h4>Select a Size Chart</h4>
                </div>
                <div class="modal-body">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                            data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Male Size Chart
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="material-img hide-radio">
                                                <input class="form-check-input" type="radio" name="sizeMChart"
                                                       id="sizeChartMale-1" value="">
                                                <label for="sizeChartMale-1"><img
                                                            class="imgMChart"
                                                            src="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1536146110/size%20chart/mensizechart1.jpg"></label>

                                            </div>
                                        </div>
                                        <div class="col-md-4" style="margin-left: 8em;">
                                            <div class="material-img hide-radio">
                                                <input class="form-check-input" type="radio" name="sizeMChart"
                                                       id="sizeChartMale-2" value="">
                                                <label for="sizeChartMale-2"><img class="imgMChart"
                                                                                  src="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1536146143/size%20chart/Msize2.jpg"></label>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                            data-target="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                        Female Size Chart
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                 data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="material-img hide-radio">
                                                <input class="form-check-input" type="radio" name="sizeFChart"
                                                       id="sizeChartFemale-1" value="">
                                                <label for="sizeChartFemale-1"><img
                                                            class="imgMChart"
                                                            src="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1536146267/size%20chart/Femalesizechart1.jpg"></label>

                                            </div>
                                        </div>
                                        <div class="col-md-4" style="margin-left: 8em;">
                                            <div class="material-img hide-radio">
                                                <input class="form-check-input" type="radio" name="sizeFChart"
                                                       id="sizeChartFemale-2" value="">
                                                <label for="sizeChartFemale-2"><img class="imgMChart"
                                                                                    src="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1536146268/size%20chart/Fsize2.jpg"></label>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-update addSizeChart" data-dismiss="modal">Upload</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Size Chart Upload Modal -->
    <div class="modal hide fade" id="sizeUploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content size-style">
                <div class="modal-header">
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                    <h4>Upload Your Size Chart/Guide</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <label for="uploadGender1"><span>Male *</span>
                                <img id="maleSizePic"
                                     src="{{ asset('img/material1.jpg')}}"
                                     class="img-control">
                                <input type="file" id="uploadGender1" class="hide-input form-control maleSizePic">
                            </label>
                        </div>
                        <div class="col-md-5">
                            <label for="uploadGender2" style="margin-left: 25px;"><span>Female *</span>
                                <img id="femaleSizePic"
                                     src="{{ asset('img/material1.jpg')}}"
                                     class="img-control">
                                <input type="file" id="uploadGender2" class="hide-input form-control femaleSizePic">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="upload" class="btn btn-update" data-dismiss="modal">Upload</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" data-backdrop="static" id="confirm" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title makeBold brand-color" id="exampleModalLabel">Confirm Upload</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class='confirmUpload'>
                        <img style="width: 100%; border-radius: 50%; border: 1px solid grey;" class='brand img-fluid'
                             src=''/>
                    </div>
                    <p>Do you wish to continue?</p>
                </div>
                <div id='loaderLog' style='height:100px;display:none;' class='text-center'>
                    <img class='center-block' src="{{asset('img/loader.gif')}}"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger no">No</button>
                    <button type="button" class="btn btn-wawooh uploadLogo">Yes</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    {{-- <script src="{{asset('js/dash/multi.js')}}"></script> --}}
    <script src="{{asset('js/dash/jquery.easing.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var documentFlag = false;

            /* if($('#sizeChart').val !== ''){
                 documentFlag = true;
             }*/

            /*if ($('#malChartPark').attr('src') === '' || $('#femChartPark').attr('src') === '') {
                documentFlag = false;
            } else {
                documentFlag = true;
            }*/

            var date = '';
            @if(@$designer->dateOfBirth !== null)
                date = '{{$designer->dateOfBirth}}'
            @endif

            if (date !== '') {
                let dat = date.indexOf('/');
                let lstdate = date.lastIndexOf('/');

                let b = date.substr(lstdate + 1, 4);
                let c = date.substr(dat + 1, 2);
                let a = date.substr(0, dat);

                $('#month').val(c);
                $('#day').val(a);
                $('#birthYear').val(b);
            }

            var state = $('#state').val();

            $('#country').val("{{@$designer->country}}").attr('selected', 'selected');

            $('#countryCode').val("{{@$designer->countryCode}}");

            $('#state').val("{{@$designer->state}}").attr('selected', 'selected');

            $('#lga').val("{{@$designer->localGovt}}").attr('selected', 'selected');

            $('#inputLogo').on('change', function () {
                convertToBase64(this, 'img.brand');
                $('#confirm').modal();
            });

            $('#SubmitPersonal').click(function () {
                //$('#loaderModal').modal('show');
                var data = {
                    lastName: $('#ln').val(),
                    firstName: $('#fn').val(),
                    gender: $('#gender').val(),
                    dateOfBirth: $('#day').val() + '/' + $('#month').val() + '/' + $('#birthYear').val(),
                    email: $('#email').val(),
                    aboutMe: $('#about-you').val()
                };

                console.log(data);
                $.ajax({
                    url: "{{route('updatepersonalinformation')}}",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (response) {
                        // console.log(response);
                        if (response.status === '00') {
                            $('#personalsInfo').css({
                                'display': 'none',
                                'left': 50 + '%',
                                'opacity': 0
                            });

                            $('#businessesInfo').css({
                                'display': 'block',
                                'left': 0 + '%',
                                'opacity': 1
                            });
                            $.notify("Profile successfully updated", "success");
                        }
                    },
                    error: function (err) {
                        swal(err, '', 'error');
                    }
                })
            });

            $('.businessInfoReal').click(function () {
                $('#countryCode').val("{{@$designer->countryCode}}");
                var businessLength = [];
                console.log(documentFlag);

                if ($('#sN').val() === '' || $('#country').val() === '' || $('#state').val() === '' || $('#lga').val() === '' || $('#businessAddress').val() === '' || $('#sizeChart').val() === '') {
                    // $.notify('Invalid Store Name');
                    swal('All fields with (*) are required', '', 'warning');

                } else {
                    console.log('passed');
                    var data = {
                        designer: {
                            storeName: $('#sN').val(),
                            address: $('#businessAddress').val(),
                            country: $('#country').val(),
                            state: $('#state').val(),
                            localGovt: $('#lga').val(),
                            city: $('#city').val(),
                            sizeGuideFlag: $('#sizeChart').val(),
                            registeredFlag: $('#status').val(),
                            registrationNumber: $('#businessRegNumber').val(),
                            registrationDocument: ($('#businessDocPix').attr('src')).includes('/img/material1.jpg') ? $('#businessDocSecImage').attr('src') : $('#businessDocPix').attr('src'),
                            threshold: $('#threshold').val()
                        }
                    };

                    if (data.designer.sizeGuideFlag === "Y") {
                        data.designer.maleSizeGuide = $('#malChartPark').attr('src');
                        data.designer.femaleSizeGuide = $('#femChartPark').attr('src');

                    } else {
                        data.designer.maleSizeGuide = null;
                        data.designer.femaleSizeGuide = null;
                    }
                    console.log(data);

                    if ((data.designer.sizeGuideFlag === 'Y') && (data.designer.sizeGuide === '' || data.designer.sizeGuideFlag === '')) {
                        if (data.designer.sizeGuideFlag === 'Y') {
                            $.notify('You must upload a size guide to continue', 'error');
                        }
                        /*else if (data.designer.sizeGuideFlag === 'N') {
                                                   $.notify('You must select a size guide to continue', 'error');
                                               }*/
                    } else {
                        updateBusinessInfo(data);
                    }

                    if (data.designer.sizeGuideFlag === '') {
                        updateBusinessInfo(data);
                    }
                }
            });

            function updateBusinessInfo(data) {
                $.ajax({
                    url: "{{route('updatebusinessinformation')}}",
                    type: "POST",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (response) {
                        console.log(response);
                        if (response.status === '00') {
                            $('#businessesInfo').css({
                                'display': 'none',
                                'left': 50 + '%',
                                'opacity': 0
                            });
                            $('#accountsInfo').css({
                                'display': 'block',
                                'left': 0 + '%',
                                'opacity': 1
                            })
                            $.notify("Business profile successfully updated", "success");
                            $('#countryCode').val("{{@$designer->countryCode}}");
                        }
                    },
                    error: function (err) {
                        // console.log(err);
                        $('#loaderModal').modal('hide');
                        swal(err, '', 'error');
                    }
                });
            }


            $('#updateAccount').on('click', function (e) {
                e.preventDefault();
                if ($('#sN').val() === '' || $('#country').val() === '' || $('#state').val() === '' || $('#lga').val() === '' || $('#businessAddress').val() === '' || $('#bankAccNum').val() === '' || $('#bankAccName').val() === '' || $('#desBank').val() === '') {
                    swal('Some field are required', '', 'warning');
                } else {
                    var data = {
                        designer: {
                            accountNumber: $('#bankAccNum').val(),
                            bankName: $('#desBank').val() === 'Others' ? $('#other_bank').val() : $('#desBank').val(),
                            accountName: $('#bankAccName').val(),
                            otherBankName: $('#other_bank').val(),
                            swiftCode: $('#swiftCode').val(),
                            countryCode: $('#countryCode').val(),
                            currency: $('#currency').val()
                        }
                    };
                    $.ajax({
                        url: "{{route('updateaccountinformation')}}",
                        type: "POST",
                        dataType: "json",
                        data: JSON.stringify(data),
                        success: function (response) {
                            if (response.status === '00') {
                                $.notify("Account successfully updated", "success");
                            }

                        },
                        error: function (error) {
                            console.log(error);
                        }
                    })
                }

            })

            $('.personalUpdate').click(function () {
                $(this).addClass('personalUp');
            });

            $('.businessInfo').click(function () {
                $(this).addClass('businessIn');
                $('#countryCode').val("{{@$designer->countryCode}}");
            });

            $('.update-profile').click(function (e) {
                e.preventDefault();
                if ($('#sN').val() === '' || $('#country').val() === '' || $('#state').val() === '' || $('#lga').val() === '' || $('#businessAddress').val() === '' || $('#bankAccNum').val() === '' || $('#bankAccName').val() === '' || $('#desBank').val() === '') {
                    swal('Some field are required', '', 'warning');
                } else {

                    $('#loaderModal').modal('show');
                    if ($('.personalUpdate').hasClass('personalUp') || $('.businessInfo').hasClass('businessIn')) {
                        // console.log('wo');
                        let stName = $('#sN').val();
                        let data = {
                            lastName: $('#ln').val(),
                            firstName: $('#fn').val(),
                            gender: $('#gender').val(),
                            dateOfBirth: $('#day').val() + '/' + $('#month').val() + '/' + $('#birthYear').val(),
                            email: $('#email').val(),
                            aboutMe: $('#about-you').val(),
                            designer: {
                                storeName: removeSpace(stName),
                                address: $('#businessAddress').val(),
                                country: $('#country').val(),
                                state: $('#state').val(),
                                localGovt: $('#lga').val(),
                                city: $('#city').val(),
                                sizeGuideFlag: $('#sizeChart').val(),
                                registeredFlag: $('#status').val(),
                                registrationNumber: $('#businessRegNumber').val(),
                                registrationDocument: $('#businessDocSecImage').attr('src'),
                                accountNumber: $('#bankAccNum').val(),
                                bankName: $('#desBank').val() === 'Others' ? $('#other_bank').val() : $('#desBank').val(),
                                otherBankName: $('#other_bank').val(),
                                accountName: $('#bankAccName').val(),
                                swiftCode: $('#swiftCode').val(),
                                countryCode: $('#countryCode').val(),
                                currency: $('#currency').val(),
                                threshold: $('#threshold').val()
                            }
                        }
                        // console.log(data);
                        if (data.designer.sizeGuideFlag === "Y") {
                            data.designer.maleSizeGuide = $('#malChartPark').attr('src');
                            data.designer.femaleSizeGuide = $('#femChartPark').attr('src');

                        } else {
                            data.designer.maleSizeGuide = null;
                            data.designer.femaleSizeGuide = null;
                        }
                        console.log(data);
                        $.ajax({
                            url: "{{route('updatepersonalinformation')}}",
                            type: "POST",
                            dataType: "json",
                            data: JSON.stringify(data),
                            success: function (response) {
                                // console.log(response);
                                $('#loaderModal').modal('hide');
                                swal("Profile Information Updated", '', 'success');
                                window.location.href = "{{route('designer.profile')}}"
                            },
                            error: function (err) {
                                $('#loaderModal').modal('hide');
                                // console.log(err);
                                swal(err, '', 'error');
                            }

                        })
                    } else {
                        var data = {
                            designer: {
                                accountNumber: $('#bankAccNum').val(),
                                bankName: $('#desBank').val() === 'Others' ? $('#other_bank').val() : $('#desBank').val(),
                                accountName: $('#bankAccName').val(),
                                swiftCode: $('#swiftCode').val(),
                                otherBankName: $('#other_bank').val(),
                                countryCode: $('#countryCode').val(),
                                currency: $('#currency').val()
                            }

                        };
                        console.log(data);
                        $.ajax({
                            url: '{{route('updateaccountinformation')}}',
                            type: "POST",
                            dataType: "json",
                            data: JSON.stringify(data),
                            success: function (response) {
                                // console.log(response);
                                $('#loaderModal').modal('show');
                                swal("Profile Information Updated Successfully", '', 'success');
                                window.location.href = "{{route('designer.profile')}}"
                            },
                            error: function (err) {
                                $('#loaderModal').modal('hide');
                                // console.log(err);
                                swal(err, '', 'error');
                            }
                        })
                    }
                }

            });

            $('.maleSizePic').change(function () {
                for (var i = 0; i < $(this).get(0).files.length; i++) {
                    var fileSize = $(this).get(0).files[i].size / 1024;
                    if (fileSize > 500) {
                        $.notify("Cannot upload image size more than 500kb");
                    } else {
                        convertToBase64(this, '#maleSizePic');
                    }
                }
            });

            $('.IncorpoDocument').change(function () {
                for (var i = 0; i < $(this).get(0).files.length; i++) {
                    var fileSize = $(this).get(0).files[i].size / 1024;
                    if (fileSize > 500) {
                        $.notify('Cannot upload document size more than 500kb');
                    } else {
                        convertToBase64(this, '#businessDocSecImage');
                    }
                }
            });

            $('.femaleSizePic').change(function () {
                for (var i = 0; i < $(this).get(0).files.length; i++) {
                    var fileSize = $(this).get(0).files[i].size / 1024;
                    if (fileSize > 500) {
                        $.notify("Cannot upload image size more than 500kb");
                    } else {
                        convertToBase64(this, '#femaleSizePic');
                    }
                }
            });

            $('.businessDocPix').change(function () {
                for (var i = 0; i < $(this).get(0).files.length; i++) {
                    var fileSize = $(this).get(0).files[i].size / 1024;
                    if (fileSize > 500) {
                        $.notify("Cannot upload image size more than 500kb");
                    } else {
                        convertToBase64(this, '#businessDocPix');
                        $(this).siblings('p').removeClass('hide');
                    }
                }
            });

            $('.businessDocSecImage').change(function () {
                for (var i = 0; i < $(this).get(0).files.length; i++) {
                    var fileSize = $(this).get(0).files[i].size / 1024;
                    if (fileSize > 500) {
                        $.notify("Cannot upload image size more than 500kb");
                    } else {
                        convertToBase64(this, '#businessDocSecImage');
                        $(this).siblings('p').removeClass('hide');
                    }
                }
            });

            $('#upload').click(function () {
                var m = $('#maleSizePic').attr('src');
                var f = $('#femaleSizePic').attr('src');

                if (m.includes("/material1.jpg")) {
                    $('#malChartPark').attr('src', '#');
                } else {
                    $('#malChartPark').attr('src', m);
                    documentFlag = true;
                }

                if (f.includes("/material1.jpg")) {
                    $('#femChartPark').attr('src', '#');
                } else {
                    $('#femChartPark').attr('src', f);
                    documentFlag = true;
                }
            });

            $('input[name=sizeMChart]:checked').siblings('label').find('img');

            $('.imgMChart').click(function () {
                $(this).addClass('imgRealChart');
            });

            $('.addSizeChart').click(function () {
                var m = $('input[name=sizeMChart]:checked').siblings('label').find('img').attr('src');
                var f = $('input[name=sizeFChart]:checked').siblings('label').find('img').attr('src');
                $('.maleCharts').attr('src', m);
                $('.femaleCharts').attr('src', f);
                documentFlag = true;
            });

            $(document).on('keyup', '#sN', function () {
                /*if ($(this).val().search('-') !== -1) {
                    $.notify('dash character(-) is not allowed', 'error');
                    // $(this).val().replace('_', '');
                }*/
                /*$.ajax({
                    url: '',

                })*/
                console.log('this is a keyp');
            });

            populateCountries("countryCode");

        });

    </script>
@endpush