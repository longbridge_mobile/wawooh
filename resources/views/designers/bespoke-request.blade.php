@extends('layouts.designer.designer')

@push('styles')
    @endpush


@section('pageTitle', 'Bespoke Request')

@section('content')
    <div class="content-wrapper new-prod">
        <div class="row grid-margin">
            <div class="col-12">
                <h3 style="margin-bottom: 40px;" class="makeBold">Bespoke Request Bids</h3>
                  <div class="card">
                      <div class="card-body">
                          <div class="table-responsive">
                              <table class="table table-striped table-hovered" id="desOrderTable">
                                  <thead>
                                  <tr>
                                      <th>s/n</th>
                                      <th>Gender</th>
                                      <th>Budget</th>
                                      <th>Expected Delivery Date</th>
                                      <th>Date Ordered</th>
                                      <th>action</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      @if(is_array($data))
                                          @if(count($data))
                                              @foreach($data as $product)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$product->gender}}</td>
                                                    <td>{{$product->estimatedBudget}}</td>
                                                    <td>{{$product->hasFabric ? 'Has Fabric' : 'No Fabric'}}</td>
                                                    <td>({{\Carbon\Carbon::createFromTimestamp($product->orderDate / 1000)->diffForHumans()}})</td>
                                                    <td>
                                                        <a href="{{route('getDesignerBespokeDetail', [generateCorrectUrl(strtolower($storeName)), $product->id])}}">
                                                        detail
                                                        </a>
                                                    </td>
                                                </tr>
                                              @endforeach
                                         @endif
                                      @endif
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>

@endsection
