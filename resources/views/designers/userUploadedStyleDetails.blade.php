@extends('layouts.designer.designer')

@push('styles')
    <style>
        .fitDiv {
            height: 100%;
            width: 100%;
        }

        .img-fluid {
            cursor: pointer;
        }

        .disabledClick {
            pointer-events: none;
        }

        .product-big-img {
            height: 100%;
        }

        .product-small-img .img-fluid {
            width: 100%;
            height: 100%;
        }

        .product-small-img > div {
            height: 120px;
            margin-bottom: 5px;
            width: 100%;
        }

        .size-inline {
            display: inline-flex;
        }

        .size-inline .form-inline {
            margin-top: 0.5rem;
            margin-right: 7px;
        }

        .size-inline .form-inline label {

        }

        .size-inline .form-inline input {
            width: 2rem;
        }

        .size-inline .input-group-prepend, .size-inline .input-group-append {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-left: 0.5rem;
            margin-right: 0.5rem;
        }

        .size-inline .input-group-prepend:hover, .size-inline .input-group-append:hover {
            cursor: pointer;
        }

        .btn-wawooh {
            font-size: 0.9rem;
        }

        .btn-wawooh:hover {
            cursor: pointer;
        }
    </style>
@endpush

@section('pageTitle', title_case(@$resp->styleName) . ' | Product Details')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="row product-img">
                            <div class="col-md-9 product-big-img productDisplay product-one productDisplayOne">
                                <img id='mainPicture'
                                     src="{{@$resp->bespokePicturesDTO->viewOne}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3 product-small-img productThumb product-one productThumbOne">
                                            <div>
                                                <img src="{{@$resp->bespokePicturesDTO->viewFour}}"
                                                     class="img-fluid">
                                            </div>
                                <div>
                                    <img src="{{@$resp->bespokePicturesDTO->viewThree}}"
                                         class="img-fluid">
                                </div>

                            </div>


                            </div>
                        </div>


                    <div class="col-md-6 product-des wow animated fadeInRight" data-wow-delay=".3s">
                        <div class="row">
                            <div class="col-md-6">
                                {{--  <p class="product-name">Product Status:
                                      <label class="label label-{{$product->verifiedFlag === 'Y' ? 'completed' : 'rejected'}}">{{$product->verifiedFlag === 'Y' ? 'Verified' : 'Not Verified'}}</label>
                                  </p>--}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="product-name">{{(@$resp->budget > 0) ? 'Old Price:' : 'Price: ' }}
                                    <span>₦{{number_format(@$resp->budget)}}</span></p>
                            </div>
                            <div class="col-md-6">
                                <p class="product-name">Product:
                                    <span>{{@$resp->styleName}}</span></p>
                            </div>
                        </div>
                        <h6><strong><u>Summary</u></strong></h6>
                        <p class="product-des-long">{{@$resp->additionalInfo}}</p>
                        <h6><strong><u>Product Description</u></strong></h6>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="prod-desc prod-message wow animated fadeInUp" data-wow-delay=".2s">
                                    {!! nl2br(@$resp->additionalInfo) !!}
                                </div>
                            </div>
                        </div>
                        <h6><strong><u>Estimated Delivery</u></strong></h6>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="prod-desc prod-message wow animated fadeInUp" data-wow-delay=".2s">
                                    {{@$resp->expectedDelivery}}
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <label for="expectedDelivery">Expected Delivery</label>
                                    <input type="date" id="expectedDelivery" class="form-control" value="{{@$resp->bespokeBidDTO->expectedDelivery}}">
                                </div>
                                <div>
                                    <label for="budget">Budget</label>
                                    <input type="text" id="budget" class="form-control" value="{{@$resp->bespokeBidDTO->budget}}">
                                </div>
                                <div>
                                    <label for="additionalInfo">Additional Information</label>
                                    <textarea id="additionalInfo" class="form-control">{{@$resp->bespokeBidDTO->additionalNotes}}</textarea>
                                </div>
                                <button class="btn-wawooh placeBid">{{$resp->bespokeBidDTO ? 'Replace Bids': 'Place Bids'}}</button>
                            </div>
                        </div>





                      {{--  <div class="col-md-12 product-size selected-measurement">
                            <h6><strong><u>Selected Measurements</u></strong></h6>
                            @if(isset($reqMes))
                                @if(count($reqMes) > 0)
                                    <ul>
                                        @foreach($reqMes as $measure)
                                            <li>{{strtolower($measure)}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <label class="label label-rejected">No Selected Measurement</label>
                                @endif
                            @endif
                        </div>--}}
                    </div>



                </div>


                </div>

            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>

        $('.placeBid').click(function(){
            console.log('hello world');
            $(this).attr('disabled', true);
            var data = {
                userBespokeStyleUploadId: "{{@$resp->userBespokeStyleUploadId}}",
                expectedDelivery: $('#expectedDelivery').val(),
                budget: $('#budget').val(),
                additionalNotes: $('#additionalInfo').val()
            }

            console.log(data);
           $.ajax({
                url: "{{route('placeBidForUserStyle')}}",
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function(res) {
                    if(res.status === '200'){
                        swal(res.message, '', 'success');
                        location.reload();
                    } else {
                        swal(res.message, '', 'error');
                        $(this).attr('disabled', false);
                    }
                },
                error: function(err){
                    console.log(err);
                }
            })
        })

    </script>
@endpush