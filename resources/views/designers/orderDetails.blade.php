@extends('layouts.designer.designer')

@push('styles')
    <link rel="stylesheet" href="{{asset('css/dash/ekko-lightbox.css')}}">
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }

        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 51px;
            height: 51px;
            margin: 6px;
            border: 6px solid #fff;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #fff transparent transparent transparent;
        }

        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }

        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }

        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }

        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

        .ld-spinners {
            background: #cd9933;
            color: #cd9933;
        }
    </style>

@endpush

@section('pageTitle' , $order->productName .' | Order Details')
@section('content')
    <div class="content-wrapper white orderStatusVendor">
        <div class="row grid-margin">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Order status</h6>
                        <div class="product-image">
                            {{--<h3>Product Picture</h3>--}}
                            <div class="main-product-img">
                                <a href="{{str_replace('http','https',$order->productPicture)}}" data-toggle="lightbox">
                                    <img class="prodImg" src="{{str_replace('http','https',$order->productPicture)}}" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-7 orderStatusDetails">
                            <table class="table">
                                <tr>
                                    @if($order->status == 'P')
                                        <td>Order Status</td>
                                        <td><label class="label label-pending">Pending</label></td>
                                    @elseif($order->status == 'PC')
                                        <td>Order Status</td>
                                        <td><label class="label label-completed">Payment Confirmed</label></td>
                                    @elseif($order->status == 'OP')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Processing</label></td>
                                    @elseif($order->status == 'A')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Accepted</label></td>
                                    @elseif($order->status == 'WR')
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Wawooh Rejected</label></td>
                                    @elseif($order->status == 'WC')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Wawooh Collected</label></td>
                                    @elseif($order->status == 'CO')
                                        <td>Order Status</td>
                                        <td><label class="label label-completed">Completed</label></td>
                                    @elseif($order->status == 'RS')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Ready for Shipping</label></td>
                                    @elseif($order->status == 'RI')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Ready for Inspection</label></td>
                                    @elseif($order->status == 'OS')
                                        <td>Order Status</td>
                                        <td><label class="label label-processing">Order Shipped</label></td>
                                    @elseif($order->status == 'C')
                                        <td>Order Status</td>
                                        <td><label class="label label-rejected">Cancelled</label></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Order Number</td>
                                    <td>{{$order->orderNumber}}</td>
                                </tr>
                                <tr>
                                    <td>Product Details/Name</td>
                                    <td>{{$order->productName}}</td>
                                </tr>
                                <tr>
                                    <td>Order Date</td>
                                    <td>{{Carbon\Carbon::parse($order->orderDate)->format('d-m-Y')}}
                                        <small class="text-success">({{\Carbon\Carbon::parse($order->orderDate)->diffForHumans()}})</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Amount</td>
                                    <td>₦{{number_format($order->amount)}}</td>
                                </tr>
                                <tr>
                                    <td>Size</td>
                                    <td>{{$order->size ? $order->size : 'Bespoke'}}</td>
                                </tr>
                                <tr>
                                    <td>Quantity</td>
                                    <td>{{$order->quantity}}</td>
                                </tr>
                                @if(isset($order->failedInspectionReason))
                                    <tr>
                                        <td>Has Message?</td>
                                        <td><a href="#myModalMessage" data-target="#myModalMessage" data-toggle="modal"><i
                                                        class="animated pulse infinite fa fa-fw fa-envelope-o"></i>Message</a>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>Measurement</td>
                                    @if(!is_null($order->measurement))
                                        <td><a href="#" data-toggle="modal" data-target="#measurementModal">Measurement
                                                Available</a></td>
                                    @else
                                        <td><a href="#" data-toggle="modal" data-target="#">No Measurement
                                                Available</a>
                                        </td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>Notes</td>
                                    @if(!is_null($order->notes))
                                        <td>{{$order->notes}}</td>
                                    @else
                                        <td>No Notes</td>
                                    @endif
                                </tr>
                            </table>
                            <div class="artwork-image">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3>Artwork Image</h3>
                                        @if(!is_null($order->artWorkPicture))
                                            <a href="{{asset('img/gallery6.jpg')}}" data-toggle="lightbox">
                                                <img class="img-class" src="{{str_replace('http','https',$order->artWorkPicture)}}" alt="">
                                            </a>
                                        @else
                                            <p class="text-centerC">No ArtWork Available</p>
                                        @endif
                                    </div>
                                    <div class="offset-2 col-md-4">
                                        <h3>Fabric Image</h3>
                                        @if(!is_null($order->materialPicture))
                                            <a href="{{asset('img/gallery4.jpg')}}" data-toggle="lightbox">
                                                <img class="img-class" src="{{str_replace('http','https',$order->materialPicture)}}" alt="">
                                            </a>
                                        @else
                                            <p class="text-centerC">No Fabric Available</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="actions">
                                 <input type="button" data-toggle="modal" data-target="#myModal" class="btn btn-doneAccept pull-right" value="Design Completed">
                             </div>--}}
                            {{-- <div class="actions">
                                 <button data-toggle="modal" data-target="#myModal" class="btn btn-doneAccept pull-right">Design Completed</button>
                             </div>--}}
                            <div class="row">
                                @if($order->status == 'P')
                                    <div class="col-md-9 actions">
                                        <button designer="A" statusid="22" class="btn btn-doneAccept pull-right">
                                            Accept
                                        </button>
                                    </div>
                                    <div class="col-md-3 actions">
                                        <button designer='OR' statusid="4" class="btn btn-doneAccept">Decline</button>
                                    </div>
                                @elseif($order->status == 'PC')
                                    <div class="actions">
                                        <button designer="OP" statusid="3" class="btn btn-doneAccept pull-right">
                                            Start Processing
                                        </button>
                                    </div>
                                @elseif($order->status == 'WR')
                                    <div class="actions">
                                        <button designer="OP" statusid="3" class="btn btn-doneAccept pull-right">
                                            Start Processing
                                        </button>
                                    </div>
                                @elseif($order->status == 'OP')
                                    <div class="actions">
                                        <button data-toggle="modal" data-target="#myModal" designer="" statusid=""
                                                style="color: #ffffff; background: black;border-bottom-width: 3px;border-color: #cd9933;"
                                                class="btn btn-doneAcceptx pull-right">Design Completed
                                        </button>
                                    </div>

                                @elseif($order->status == 'CO')
                                    <div class="hide" hidden>
                                        Status:<label class="label">Completed</label>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" data-backdrop="static" id="myModal" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-dialog-preview" role="document">
                    <div class="modal-content productSampleUpload">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><label>Upload Product Pictures From Different
                                    Angles For QA Inspection</label></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-3">
                                        <span>Front Sample</span>
                                        <label for="front">
                                            <img class="img designerProduct img-responsive"
                                                 src="{{ asset('img/material1.jpg')}}" height="150" alt="">
                                            <input type="file" id="front" class="hide-input addDesignerProduct"/>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <span>Back Sample</span>
                                        <label for="back">
                                            <img class="img designerProduct img-responsive"
                                                 src="{{ asset('img/material1.jpg')}}" height="150" alt="">
                                            <input type="file" id="back" class="hide-input addDesignerProduct"/>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <span>Side Sample</span>
                                        <label for="side">
                                            <img class="img designerProduct img-responsive"
                                                 src="{{ asset('img/material1.jpg')}}" height="150" alt="">
                                            <input type="file" id="side" class="hide-input addDesignerProduct"/>
                                        </label>
                                    </div>
                                    <div class="col-md-3">
                                        <span>Inner Sample</span>
                                        <label for="inner-sample">
                                            <img class="img designerProduct img-responsive"
                                                 src="{{ asset('img/material1.jpg')}}" height="150" alt="">
                                            <input type="file" id="inner-sample" class="hide-input addDesignerProduct"/>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                            <button type="button" designer="CO" statusid="5" class="btn doneAccepted"
                                    style="color: #ffffff; background: black;border-bottom-width: 3px;border-color: #cd9933;">
                                <i class="fa fa-check-circle-o"></i>Done
                            </button>
                            <div class="lds-ring hide">
                                <div class="ld-spinner"></div>
                                <div class="ld-spinners"></div>
                                <div class="ld-spinner"></div>
                                <div class="ld-spinner"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--Modal For Measurement-->
            @if(isset($orderMeasurement))
                <div class="modal fade" data-backdrop="static" id="measurementModal" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-measurement" role="document">
                        <div class="modal-content productSampleUpload">
                            <div class="modal-header">
                                <h5>Measurement Details in <span style="font-size: 10px" id="default">centimeters</span>
                                    <span style='font-size:10px' id='unit'></span></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <hr>
                                <label id='convert' class='btn btn-wawooh'>Convert Measurement</label>
                                <label id="convertBack" class="btn btn-wawooh hide">Convert Measurement</label>
                            </div>
                            <div class="modal-body">
                                <div class="row measurement-input">
                                    <table class="table table-stripped table-bordered measurementTable">
                                        <thead>
                                        <th class="heading" colspan="2">Measurement
                                            Name: @if(isset($orderMeasurement)) {{$orderMeasurement->name}} @endif</th>
                                        </thead>
                                        @if($orderMeasurement->measurements)
                                            <tbody>
                                            <tr>
                                                <td>Fitness Type</td>
                                                <td style="text-align: center;"
                                                    class="measureValue">@if(isset($orderMeasurement)) {{ucfirst(@$orderMeasurement->fitType)}} @endif</td>
                                            </tr>
                                            @if($orderMeasurement->neck != '' || $orderMeasurement->neck != null)
                                                <tr>
                                                    <td width="150px">Neck</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{@$orderMeasurement->measurement->neck}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->armHole != '' || $orderMeasurement->armHole != null)
                                                <tr>
                                                    <td width="150px">Arm Hole</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{@$orderMeasurement->armHole}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->biceps != '' || $orderMeasurement->biceps != null)
                                                <tr>
                                                    <td width="150px">Biceps</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{@$orderMeasurement->biceps}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->longSleeve != '' || $orderMeasurement->longSleeve != null)
                                                <tr>
                                                    <td width="150px">Long Sleeve</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{@$orderMeasurement->longSleeve}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->shortSleeve != '' || $orderMeasurement->shortSleeve != null)
                                                <tr>
                                                    <td width="150px">Short Sleeve</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->shortSleeve}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->fullShoulder != '' || $orderMeasurement->fullShoulder != null)
                                                <tr>
                                                    <td width="150px">Full Shoulder</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->fullShoulder}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->halfShoulder != '' || $orderMeasurement->fullShoulder != null)
                                                <tr>
                                                    <td width="150px">Short Sleeve</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->halfShoulder}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->elbow != '' || $orderMeasurement->elbow != null)
                                                <tr>
                                                    <td width="150px">Elbow</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->elbow}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->fullChest != '' || $orderMeasurement->fullChest != null)
                                                <tr>
                                                    <td width="150px">Full Chest</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->fullChest}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->bust != '' || $orderMeasurement->bust != null)
                                                <tr>
                                                    <td width="150px">Bust</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->bust}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->overBreast != '' || $orderMeasurement->overBreast != null)
                                                <tr>
                                                    <td width="150px">Over Breast</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->overBreast}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->underChest != '' || $orderMeasurement->underChest != null)
                                                <tr>
                                                    <td width="150px">Under Chest</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->underChest}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->underBust != '' || $orderMeasurement->underBust != null)
                                                <tr>
                                                    <td width="150px">Under Bust</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->underBust}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->shirtLength != '' || $orderMeasurement->shirtLength != null)
                                                <tr>
                                                    <td width="150px">Shirt Length</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->shirtLength}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->stomach != '' || $orderMeasurement->stomach != null)
                                                <tr>
                                                    <td width="150px">Stomach</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->stomach}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->wrist != '' || $orderMeasurement->wrist != null)
                                                <tr>
                                                    <td width="150px">Wrist</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->wrist}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->fullLength != '' || $orderMeasurement->fullLength != null)
                                                <tr>
                                                    <td width="150px">Full Length</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->fullLength}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->trouserWaist != '' || $orderMeasurement->trouserWaist != null)
                                                <tr>
                                                    <td width="150px">Trouser Waist</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->trouserWaist}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->seat != '' || $orderMeasurement->seat != null)
                                                <tr>
                                                    <td width="150px">Seat</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->seat}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->outSeam != '' || $orderMeasurement->outSeam != null)
                                                <tr>
                                                    <td width="150px">Out Seam</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->outSeam}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->inSeam != '' || $orderMeasurement->inSeam != null)
                                                <tr>
                                                    <td width="150px">In Seam</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->inSeam}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->crotch != '' || $orderMeasurement->crotch != null)
                                                <tr>
                                                    <td width="150px">Crotch</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->crotch}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->thigh != '' || $orderMeasurement->crotch != null)
                                                <tr>
                                                    <td width="150px">Thigh</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->thigh}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->knee != '' || $orderMeasurement->knee != null)
                                                <tr>
                                                    <td width="150px">Knee</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->knee}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->ankle != '' || $orderMeasurement->ankle != null)
                                                <tr>
                                                    <td width="150px">Ankle</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->ankle}}</td>
                                                </tr>
                                            @endif
                                            @if($orderMeasurement->butt != '' || $orderMeasurement->butt != null)
                                                <tr>
                                                    <td width="150px">Butt</td>
                                                    <td style="text-align: center;"
                                                        class="measureValue">{{$orderMeasurement->butt}}</td>
                                                </tr>
                                            @endif

                                            </tbody>
                                        @else
                                            <label class="label label-rejected">Measurement is empty</label>
                                        @endif
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="#" id="printMeasure" class="btn btn-primary"><i class="fa fa-print"></i> Print
                                    Out</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(isset($order->failedInspectionReason))
                <div id="myModalMessage" data-backdrop="static" class="modal fade" tabindex="-1" role="dialog"
                     aria-labelledby="myModalLabel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="inspection-message">
                                    <h3>Hey Designer, This item failed Inspection</h3>
                                    <hr>
                                    <article>
                                        <p><b>Reason: </b> <span> {{$order->failedInspectionReason}}</span></p>
                                    </article>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-accept" id="message-accept" data-dismiss="modal"
                                       aria-hidden="true">OKAY</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/dash/lightbox-ekko.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.btn-doneAccept').click(function () {

                $('#done').attr("disabled", "disabled");
                let data = {
                    'id': '{{@$order->id}}',
                    'status': $(this).attr('designer'),
                    'productName': '{{@$order->productName}}',
                    'productId': '{{@$order->productId}}',
                    'statusId': $(this).attr('statusid'),
                    'orderNumber': '{{@$order->orderNumber}}',
                    'customerId': '{{@$order->customerId}}'
                };
                // console.log(data);
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No", "Yes"]
                })
                    .then((confirm) => {
                        if (confirm) {
                            $.ajax({
                                url: '{{route('designer.acceptOrder')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (res) {
                                    // $('#loaderModal').modal('show');
                                    // console.log(res);
                                    if (res.status === '00') {
                                        $('#loaderModal').hide();
                                        swal("Order Updated Successfully", "", "success");
                                        window.location.reload();
                                    } else if(res.status === '99') {
                                        swal(res.message,'','error');
                                        $('#loaderModal').hide();
                                    }
                                },
                                error: function (e) {
                                    swal(e, '','error');
                                    $('#loaderModal').hide();
                                    $('#done').removeAttr("disabled", "disabled");
                                }
                            })
                        }
                        else {
                            $('#done').removeAttr("disabled", "disabled");
                        }
                    })
            });

            $('.addDesignerProduct').on('change', function () {
                var file = $(this).get(0).files[0];
                if (file.size / 1024 > 120000) {
                    $.notify("Cannot upload image size more than 2MB");
                } else {
                    convertMoreToBase64(this, '.designerProduct');
                    $(this).siblings('img').addClass('uploadedImage');
                }
            });

            $('.btn-doneReject').on('click', function () {
                var data = {
                    'id': '{{@$order->id}}',
                    'productName': '{{@$order->productName}}',
                    'productId': '{{@$order->productId}}',
                    'statusId': '4',
                    'orderNumber': '{{@$order->orderNumber}}',
                    'status': 'OR',
                    'message': '',
                    'messageText': '',
                    'messageId': '',
                    'waitTime': '',
                    'customerId': '{{@$order->customerId}}'
                };
                $.ajax({
                    url: '{{route('designer.acceptOrder')}}',
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (res) {
                        // console.log(res);
                        if (res.status === "00") {
                            $('#orderDeclineBtn').val('Decline').removeAttr('disabled', 'disabled');
                            swal('Order Decline Successfully', '', 'success');
                            location.reload();

                        } else {
                            $('#orderDeclineBtn').val('Decline').removeAttr('disabled', 'disabled');
                            swal('Something went wrong', '', 'error');
                            //$(this).attr('disabled', false);
                        }
                    },
                    error: function (e) {
                        swal('Unable to complete this operation, please try again later', '', 'error');
                        $('#orderDeclineBtn').val('Decline').removeAttr('disabled', 'disabled');
                        //$(this).attr('disabled', false);
                    }

                })
            });

            $(document).on('click', '.doneAccepted', function () {
                $('.doneAccepted').attr('disabled', true);
                $('.lds-ring').removeClass('hide');


                var picture = [];
                $('.uploadedImage').each(function (value) {
                    picture.push($(this).attr('src'));
                });

                $('#done').attr("disabled", "disabled");
                let data = {
                    'id': '{{@$order->id}}',
                    'status': $(this).attr('designer'),
                    'productName': '{{@$order->productName}}',
                    'productId': '{{@$order->productId}}',
                    'statusId': $(this).attr('statusid'),
                    'orderNumber': '{{@$order->orderNumber}}',
                    'customerId': '{{@$order->customerId}}',
                    'pictures': picture
                };
                // console.log(data)
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    dangerMode: true,
                    buttons: ["No", "Yes"]
                })
                    .then((confirm) => {
                        if (confirm) {

                            $.ajax({
                                url: '{{route('designer.acceptOrder')}}',
                                type: "POST",
                                data: JSON.stringify(data),
                                success: function (res) {
                                    $('.doneAccepted').attr('disabled', false);
                                    $('.lds-ring').addClass('hide');
                                    if (res.status === '00') {
                                        swal("Order Updated Successfully", "", "success");
                                        window.location.reload();
                                    }
                                },
                                error: function () {
                                    $('#done').removeAttr("disabled", "disabled");
                                    $('#loader').hide();
                                    $('.doneAccepted').attr('disabled', false);
                                    $('.lds-ring').addClass('hide');
                                }
                            })

                        } else {
                            $('#done').removeAttr("disabled", "disabled");
                        }
                    })
            });

            $(document).on('click', '[data-toggle="lightbox"]', function (event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });

            $('#convert').click(function () {
                $('.measurementTable tbody tr .measureValue').each(function () {
                    if ($(this).html() !== '') {
                        $val = $(this).html() / 2.54;
                        $(this).html($val.toFixed(2));
                    }


                });
                /*if ($('.custom').val()) {
                    oldValue = $('.custom').val();
                    $('.custom').val($('.custom').val() * 5);
                }*/
                $(this).addClass('hide');
                $('#convertBack').removeClass('hide');
                $('#default').html('inches');
            });

            $('#convertBack').click(function () {
                $('.measurementTable tbody tr .measureValue').each(function () {
                    if ($(this).html() !== '') {
                        $val = $(this).html() * 2.54;
                        $(this).html($val.toFixed());
                    }
                });
                $(this).addClass('hide');
                $('#convert').removeClass('hide');
                $('#default').html('centimeters');
            });


            /*let measurement = JSON.parse(measurement);
              console.log(measurement);

            if (measurement != null) {
                unit = measurement.unit;
                var keys = Object.keys(measurement);
                for (let value of keys) {
                    $('#' + value).val(measurement[value]);
                }
                $('#unit').text(measurement.unit);
            }*/
        });
    </script>
@endpush
