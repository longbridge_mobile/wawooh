@extends('layouts.user.default')

@push('styles')


@endpush
@section('pageTitle', 'FAQ')
@section('content')

    <div class="container">
        <div class="shipping">
        <h3 class="text-center brand-color makeBold">FREQUENTLY ASKED QUESTION</h3>

            <div class="accordion" id="accordionFaq">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <p class="mb-0">
                            <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                How do i join wawooh? 
                            </a>

                            <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                        </p>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>Simply register as a designer or merchant on our platform by filling all the necessary information. However, you will be subjected to confirmation checks by our admin. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <p class="mb-0">
                            <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            I just registered but my products are not live on the platform
                            </a>
                            <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                        </p>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>Wawooh seeks to maintain a high standard on its platform. Before your products can be live on our platform, you are required to fill our Know Your Customer form (KYC) by filling it on your dashboard and submit.</p>
                            <p>Furthermore, your product photographs are up to our photography specifications and standards before they can be approved on our platform (consult our photography guide here). We do this to ensure there is consistency/uniform look across all products on our site. </p>
                            <p>We review your products within 24 hours. However, we may and may not approve your products depending on your ability to meet our criteria. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Criteria for joining wawooh?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFaq">
                        <div class="card-body">
                        <p>Merchants are screened based on the following criteria: </p>
                        <ul>
                            <li>Quality</li>
                            <li>Creativity & Originality</li>
                            <li>Sustainability</li>
                            <li>International capability</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            What are the guidelines i need tot follow?   
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFaq">
                        <div class="card-body">
                            <ol>
                                <li>You must ensure the products you sell are yours, true to description, original and you have the legal right to sell such product or item.</li>
                                <li>You must list at least 10 products at all times and must have in stock or be able to produce any of such product listed. </li>
                                <li>Vendors must follow Wawooh photography guide in shooting and uploading products on the platform. Take a look at our <a href="">photography guide</a> to help you drive maximum sales.</li>
                                <li>You must always update your inventories (take off products not in stock or unavailable), attend to orders timely.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Do i have to pay to join wawooh?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>No, you do not have to pay any joining fee. However, we charge a 12% commission rate on sales. You are charge 1,000 naira flat per product per shipping .</p>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSix">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Can I market my Wawooh store to my social networks?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>We provide your store link upon registration this serves as your store front url which you can share to your social and business networks.</p>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            How many product can i list?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>We currently do not have a maximum number of products that you can list on the platform.  However we recommend a minimum of 5 product stock on the platform. </p>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            How do i price my product
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>You are free to fix your price. However, so that you get an idea of how you should calculate your price, we advise that you use the formula. </p>  
                            <p>Price = COGS + Markup = X + 12% of X + N1000 (delivery). </p>
                            <ul>
                                <li>If the cost of producing a gown is N15,000, add your profit of 3,000. It means your price is 18,000 for the product. </li>
                                <li>To list this product on Wawooh, simply calculate 12% of 18,000 which is 2160 to the product + N1,000 for delivery charge. This will mean the product will be listed on Wawooh as N 21,160</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                            When do i get payed?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>Your bank account will be credited directly after seven to 14 days of delivery to the customer as long as a return has not been initiated. A return may delay remittance of the money. You may choose from the different payment methods available on wawooh. Please find below the list of available payment methods:</p>
                            <ul>
                                <li>Debit or credit card</li>
                                <li>Bank transfer</li>
                            </ul>
                            <p>We do not do cash remittance</p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                            When do i ship my order?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>For readymade product, orders must be shipped within 24hours of confirming receipt of the order. </p>
                            <p>For customized or bespoke, Orders, you commence order processing upon confirming receipt of the order in accordance to the date stated for product to be ready. Customized orders must only be shipped upon confirmation of payment of balance by Wawooh admin. </p>
                            <p class="makeBold"><em>You are to ship your order to a Wawooh quality assurance representative nearest to you for quality assurance checks. Wawooh Quality Assurance representative package the product and sends to the buyer. </em></p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEleven">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                            Can I cancel an order?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwelve">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                            What happens if somebody wants to return my item for a refund?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>Buyers may return an item for a full refund, as long as the item is received in the same condition as it was sent to the buyer. We will examine the complaint of the buyer to determine if genuine or not. </p>
                            <p>A genuine complain will mean the vendor will bear the cost of return shipping back to the buyer. </p>
                            <p>A retuned item will be sent to you through our Quality Assurance representative in line with our return policies. If the item is custom made, cancel can be done before it is shipped provided that the garment hasn’t been altered or made yet. If the customer wishes to return it for any reason after delivery they will have to write to return@wawooh.com within 2 days of receiving the item provided the reason for returns in genuine (consult or return policy); item must be unworn and must be in its original condition except for the customer's right to inspect the goods and try on the item. It is the customer's responsibility to ensure that returned goods are returned as they were delivered to prevent damage. </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThirteen">
                    <p class="mb-0">
                        <a href="" class='clickMe' data-toggle="collapse" data-target="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                            Can a buyer return my otem for exchange?
                        </a>
                        <span style="float: right" class="brand-color"><i class="fa fa-plus sg"></i></span>
                    </p>
                    </div>
                    <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen" data-parent="#accordionFaq">
                        <div class="card-body">
                            <p>A buyer may return an item for replacement by the vendor. If a return by the buyer is the fault of the buyer, the vendor bears the cost for shipping such replacement, if vice versa, the buyer will bear the cost of shipping. Please consult our policy for more information.</p>
                            <p>Our Vendor compliance manual contains all necessary information you need to know. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('scripts')
<script>
    $('.clickMe').on('click', function(){
    
    $(this).siblings('span').find('i.sg').toggleClass('fa-plus fa-minus');
    });
</script>
@endpush