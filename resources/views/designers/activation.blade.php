<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registration Pending | Wawooh &mdash; Express you</title>
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/wawooh.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dash/designerdash.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
</head>
<body>
<a name="top" class="home"></a>
<button onclick="topFunction()" id="icon-top" title="Top">
    <i class="fa fa-fw fa-chevron-up"></i>
</button>
<div class="activationHolder">
    <div class="inner-holder">
        <div class="head"><img src="{{ asset('img/logo.png') }}" alt="W" class="img-h"></div>

        <div class="activationContent">
            <h4>Enter the activation code/token sent to your mobile number and email address</h4>
            <form class="form-action form-inline" autocomplete="off">
                <div class="form-group">
                    <input type="text" class="form-control actCode" id="activateCode" minlength="5" maxlength="5">
                    <input type="button" id="activateMe" value="Validate" class="btn btn-warning">
                </div>
            </form>
            <span id="codeAction"><a href="#" id="resendCode">Resend Code</a></span>
            <div id="resMes"></div>
        </div>
    </div>
</div>
@include('scripts.scripts')
@include('scripts.designer.designerLoginSignup')

</body>
</html>