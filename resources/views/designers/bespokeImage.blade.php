@extends('layouts.designer.designer')

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css" rel="stylesheet">
    <style>
        .bespoke-form {
            padding: 10px;
        }

        .imageSelect {
            margin: 0 5px;
        }
    </style>
@endpush

@section('pageTitle','Upload Image')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin">
            @foreach($styleBanks as $styleBank)
                <div class="col-md-3 animated fadeInUp">
                    <img src="{{$styleBank->picture}}" width="150">
                    <div>Sub Category: {{$styleBank->subCategory}}</div>
                    <div>Keywords:</div>
                </div>
            @endforeach
        </div>

    </div>
@stop

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
    <script>

    </script>
@endpush
