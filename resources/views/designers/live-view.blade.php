<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class DesignersController extends Controller
{
    /**
     * @param Request $req
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index(Request $req)
    {
        if (session()->has('designerToken')) {
            return redirect(route('designer.dashboard'));
        }
        return view('designers.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dashboard()
    {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];
            $url = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";


            $urlPro = env('GET_BASE_URL') . "/fashion/secure/designer/product/getdesignerproducts";

            $resTotal = designerConsume($urlPro, 'GET');

            if(isset($resTotal['data'])) {
                $prodTotal = $resTotal['data'];
            }
            else {
                return view('errors.noNetwork');
            }
            $res = designerConsume($url, 'GET');
            // dd($res);
            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
//                    dd($designer);
                    // dd(strtolower($designer->storeName));
//                   dd($designer);
                    $salesChart = $designer->salesChart;
                    $label = array();
                    $sales = array();
                    for ($i = 0; $i < count($salesChart); $i++) {
                        array_push($label, \DateTime::createFromFormat('!m', $salesChart[$i]->month)->format('F'));
                        array_push($sales, $salesChart[$i]->amount);
                    }

                    $storeName = $designer->storeName;
//                    dd($storeName);
                } else {
                    $designer = null;
                }
                //$storeName = getDesignerStoreName();

                return view('designers.dashboard', compact('storeName', 'designer', 'sales', 'label', 'prodTotal'));
            } else {
                return view('errors.noNetwork');
                // return displayError($res, 'designers.dashboard');
            }
            return view('designers.dashboard', compact('storeName', 'designer'));
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function profile()
    {
        if (session()->has('designerToken')) {


            $data = getLastSixMonths();
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
            $res = designerConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
                    $storeName = $designer->storeName;
                    return view('designers.profile', compact('designer', 'storeName'));
                } else {
                    $designer = null;
                }

                return view('designers.profile', compact('designer', 'storeName'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'designers.profile');
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editProfile()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/designer/updatedesigner";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * Update Profile View and functions
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateProfile()
    {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
            $res = designerConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
                    //dd($designer);
                    $storeName = $designer->storeName;

                    return view('designers.update-profile', compact('designer', 'storeName'));
                } else {
                    $designer = null;
                }

                return view('designers.update-profile', compact('designer', 'storeName'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'designers.update-profile');
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateLogo()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/updatedesignerlogo";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $request
     */
    public function view(Request $request)
    {
        //check if user is logged in as a designer
        $Agent = new Agent();

        $categories = getCategories();

        $designerInUrl = str_replace('-', ' ', request('profile'));

        //get designer by store name
        $url = env('GET_BASE_URL') . "/fashion/designer/getdesignerbystorename/$designerInUrl";
        $res = consumeWithoutToken($url, 'GET');
        //dd($url);
        if (isset($res['data'])) {
            $designer = $res['data'];
        } else {
            return view('errors.noNetwork');
        }

        $designerId = $designer->id;
//        dd($designerId);
        if (session()->has('designerToken')) {
            //get the logged in designer storename
            $storeName = str_replace("'", '', strtolower(getDesignerStoreName()));
            //dd(session('designerToken'));
            //get the store Name in the url
            $designerInUrl = str_replace("'", '', strtolower(request('profile')));
            //check if logged in designers store name and store name in url are the same
            //if the same, get the logged in designer products by token
            //else get the details by store Name
            if ($storeName == $designerInUrl) {
                $page = env('MOBILE_LINK').'/designer/' .$designerInUrl;
                // $url = env('GET_BASE_URL')."/fashion/designer/getdesigner";
                // dd($url);
                $url = env('GET_BASE_URL') . "/fashion/designer/{$designerId}/getdesignerbyid";
                $res = designerConsume($url, 'GET');
                //dd($res);

                if (isset($res['data'])) {
                    $designer = $res['data'];
                } else {
                    $designer = null;
                }
            } else {
                $designerInUrl = str_replace('-', ' ', request('profile'));
                //get the designer by store name.
            }

            return view('designers.view', compact('designer', 'designerInUrl'));

        } else {
            if (request('dg') != null && request('subcatId') != null) {
                //get subcat products of designer
                $designerId = request('dg');
                $subCatId = request('subcatId');
                $data = [
                    'page' => '0',
                    'size' => '5',
                    'subcategoryId' => $subCatId,
                    'designerId' => $designerId
                ];
                $data = ['json' => $data];
                $url = env('GET_BASE_URL') . "/fashion/product/getdesignerproductsbysub";
                $res = consumeWithoutToken($url, 'POST', $data);
                $page = env('MOBILE_LINK').'/designer/' .$designerInUrl;
                if ($res['status'] == 0) {
                    $products = $res['data'];
                    if ($Agent->isMobile()) {
                        return Redirect::to($page);
                    } else {
                        //dd($products);
                        return view('designers.view', compact('designer', 'designerInUrl', 'categories', 'products'));
                    }
                }
            }

        }

        $datad = getLastSixMonths();
        $datad = ['json' => $datad];

        $urld = env('GET_BASE_URL') . "/fashion/designer/getdesigner";
        $resd = designerConsume($urld, 'POST', $datad);

        $req = substr($request->path(), 9);
        $realReq = ucfirst($req);
        $page = env('MOBILE_LINK').'/designer/' .$designerInUrl;
        if ($resd['status'] == '00') {
            //dd($req);
            $storeRes = $resd['data']->storeName;
            if ($storeRes != $realReq) {
                return view('designers.view', compact('designer', 'categories', 'designerInUrl'));
            }
            //dd($resd['data']->storeName);

        }
        return view('designers.view', compact('designer', 'categories', 'designerInUrl', 'storeName'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orders(Request $request)
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('designerToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/getdesignerorders";
            $cancelUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getcancelledorders";
            $activeUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getactiveorders";
            $pendingUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getpendingorders";
            $completedUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getcompletedorders";

            $pendingRes = designerConsume($pendingUrl, 'GET');
            $completedRes = designerConsume($completedUrl, 'GET');
            $cancelRes = designerConsume($cancelUrl, 'GET');
            $res = designerConsume($url, 'GET');
            $activeRes = designerConsume($activeUrl, 'GET');
//
            if ($res['status'] == 0 || $pendingRes['status'] == 0 || $completedRes['status'] == 0 || $cancelRes['status'] == 0 || $activeRes['status'] == 0) {
                $activeOrders = array();
                $completedOrders = array();
                if (isset($res['data'])) {
                    $orders = $res['data'];
                } else {
                    return view('errors.noNetwork');
                }
                $pendingOrders = $pendingRes['data'];
//                dd(count($pendingOrders));
                $completedsOrders = $completedRes['data'];
                $cancelOrders = $cancelRes['data'];
                $activesOrders = $activeRes['data'];
//                dd($completedRes);
                foreach ($orders as $order) {
                    if (strtolower($order->deliveryStatus) == 'op') {
                        array_push($activeOrders, $order);
                    } else if (strtolower($order->deliveryStatus) == 'co') {
                        array_push($completedOrders, $order);
                    }
                }

                $completedsOrders;
                //dd($cancelOrders);

                $totalOrder = [];
                $currentPage = LengthAwarePaginator::resolveCurrentPage();

                $collection = new Collection($orders);

                $per_page = 20;

                $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();

                $totalOrder = new LengthAwarePaginator($currentPageResult, count($collection), $per_page);

                $totalOrder->setPath($request->url());

                //dd($totalOrder);
                $data = getLastSixMonths();
                $data = ['json' => $data];

                $urlProfile = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
                $resD = designerConsume($urlProfile, 'GET');

                $designer = $resD['data'];

                $storeName = getDesignerStoreName();

                return view('designers.orders', compact('orders', 'designer', 'storeName', 'activeOrders', 'completedOrders', 'totalOrder', 'pendingOrders', 'activesOrders', 'cancelOrders', 'completedsOrders'));
            } else {
                return $redirectView;
            }
        }

        return redirect(route('designersLandingPage'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orderDetails()
    {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];

            $urlProfile = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
            $resD = designerConsume($urlProfile, 'GET');

            $designer = $resD['data'];

            $storeName = $designer->storeName;

            $orderId = request('orderId');
            $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/{$orderId}/getorderitemdetails";
            $res = designerConsume($url, 'GET');
            if ($res['status'] == 0) {//return displayError($res);
                $order = $res['data'];
                //dd($order);
                $orderMeasurement = json_decode($order->measurement);
//                dd($orderMeasurement);
                return view('designers.orderDetails', compact('storeName', 'order', 'designer', 'orderMeasurement'));
                // return view('designers.orderDetails', compact('storeName', 'order'));
            }
        }

        return redirect(route('designersLandingPage'));

    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function acceptOrder()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/updateorderitem";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * this check if account is activate
     */
    public function accountStatus()
    {
        $email = request('email');
        $decodeEmail = base64_decode($email);
        return view('designers.account-status');
    }

    /**
     * this check if account is activate
     */
    public function registrationComplete()
    {
        $email = request('email');
        $decodeEmail = base64_decode($email);
        return view('designers.activation');
    }

    /**
     * Mail Configuration for Designer
     */
    public function mailConfigure()
    {

    }

    /**
     * Mail Configuration for Designer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function verifyDesignerToken()
    {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/validatetoken";
        $res = consume($url, 'POST', $data);

        if ($res['status'] == '00') {
            $designerToken = $res['data'];
            session(['designerToken' => $designerToken]);
            return json_encode(['status' => '00', 'Token verification successful', 'data' => $designerToken]);
        } else {
            return json_encode(['status' => '56', 'message' => 'Token verification failed, try again']);
        }
    }

    /**
     * Change Password and Logout
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updatePassword() {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL').env('USER_ACCOUNT_API')."/editpassword";
        $res = designerConsume($url,'POST', $data);

        if($res['status'] == '00') {
            session()->forget('designerToken');
            return response()->json(['status' => '00','message' =>'Password Change Successfully']);
        } else {
            return response()->json(['status' => '99', 'message' => 'Not successfully']);
        }
    }

    /**
     * Change Password and Logout
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changeEmail() {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL').env('DESIGNER_ACCOUNT_API')."/updateemailaddress";
        $res = designerConsume($url,'POST', $data);

        if($res['status'] == '00') {
            session()->forget('designerToken');
            return response()->json(['status' => '00','message' =>'Email Change Successfully']);
        } else {
            return response()->json(['status' => '99', 'message' => 'Something went wrong']);
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updatePersonalInformation(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL')."/fashion/secure/designer/updatepersonalinformation";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateBusinessInformation(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL')."/fashion/secure/designer/updatebusinessinformation";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateAccountInformation(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL')."/fashion/secure/designer/updateaccountinformation";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * Account Configuration for the designer
     * Strictly for logon designer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function accountSettings() {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') ."getdesigner";
            $res = designerConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
                    $storeName = $designer->storeName;
                    return view('designers.settings', compact('designer', 'storeName'));
                } else {
                    $designer = null;
                }

                return view('designers.settings', compact('designer', 'storeName'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'designers.profile');
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addDesignerNewProduct(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL')."/fashion/secure/designer/product/addproduct";
        return designerConsume($url, 'POST', $data);
    }
}
