@extends('layouts.designer.designer')

@push('styles')
    <style>
        .fitDiv {
            height: 100%;
            width: 100%;
        }

        .img-fluid {
            cursor: pointer;
        }

        .disabledClick {
            pointer-events: none;
        }

        .product-big-img {
            height: 100%;
        }

        .product-small-img .img-fluid {
            width: 100%;
            height: 100%;
        }

        .product-small-img > div {
            height: 120px;
            margin-bottom: 5px;
            width: 100%;
        }

        .size-inline {
            display: inline-flex;
        }

        .size-inline .form-inline {
            margin-top: 0.5rem;
            margin-right: 7px;
        }

        .size-inline .form-inline label {

        }

        .size-inline .form-inline input {
            width: 2rem;
        }

        .size-inline .input-group-prepend, .size-inline .input-group-append {
            display: flex;
            justify-content: center;
            align-items: center;
            margin-left: 0.5rem;
            margin-right: 0.5rem;
        }

        .size-inline .input-group-prepend:hover, .size-inline .input-group-append:hover {
            cursor: pointer;
        }

        .btn-wawooh {
            font-size: 0.9rem;
        }

        .btn-wawooh:hover {
            cursor: pointer;
        }
    </style>
@endpush

@section('pageTitle', title_case($product->productName) . ' | Product Details')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="row product-img">
                            <div class="col-md-9 product-big-img productDisplay product-one productDisplayOne">
                                <img id='mainPicture'
                                     src="{{@$product->bespokePicturesDTO->viewOne}}"
                                     class="img-fluid fitDiv">
                            </div>
                            <div class="col-md-3 product-small-img productThumb product-one productThumbOne">
                                            <div>
                                                <img src="{{@$product->bespokePicturesDTO->viewTwo}}"
                                                     class="img-fluid">
                                            </div>

                            </div>


                            </div>
                        </div>


                    <div class="col-md-6 product-des wow animated fadeInRight" data-wow-delay=".3s">
                        <div class="row">
                            <div class="col-md-6">
                                {{--  <p class="product-name">Product Status:
                                      <label class="label label-{{$product->verifiedFlag === 'Y' ? 'completed' : 'rejected'}}">{{$product->verifiedFlag === 'Y' ? 'Verified' : 'Not Verified'}}</label>
                                  </p>--}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p class="product-name">{{($product->estimatedSewingPrice > 0) ? 'Old Price:' : 'Price: ' }}
                                    <span>₦{{number_format($product->estimatedSewingPrice)}}</span></p>
                            </div>
                            <div class="col-md-6">
                                <p class="product-name">Product:
                                    <span>{{$product->productName}}</span></p>
                            </div>
                        </div>
                        <h6><strong><u>Summary</u></strong></h6>
                        <p class="product-des-long">{{$product->description}}</p>
                        <h6><strong><u>Product Description</u></strong></h6>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="prod-desc prod-message wow animated fadeInUp" data-wow-delay=".2s">
                                    {!! nl2br($product->description) !!}
                                </div>
                            </div>
                        </div>
                        <h6><strong><u>Estimated Duration</u></strong></h6>
                        <div class="row mb-3">
                            <div class="col-md-12">
                                <div class="prod-desc prod-message wow animated fadeInUp" data-wow-delay=".2s">
                                    {{@$product->estimatedDuration}}
                                </div>
                            </div>
                        </div>




                        <div class="col-md-12 product-size selected-measurement">
                            <h6><strong><u>Selected Measurements</u></strong></h6>
                            @if(isset($reqMes))
                                @if(count($reqMes) > 0)
                                    <ul>
                                        @foreach($reqMes as $measure)
                                            <li>{{strtolower($measure)}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <label class="label label-rejected">No Selected Measurement</label>
                                @endif
                            @endif
                        </div>


                        <div class="">

                        </div>
                    </div>



                </div>

                <div class="row available-mat wow animated fadeInRight" data-wow-delay=".4s">
                    <div class="col-md-6 col-12" style="border-right: 1px solid rgba(0,0,0,.4);">
                        <div class="available">
                            <h5 class="productDetailsLabel text-center">Available Artworks</h5>
                            @if(isset($product->bespokeArtworkPicturesDTO) && is_array($product->bespokeArtworkPicturesDTO))
                                @if(count($product->bespokeArtworkPicturesDTO))
                                    <div class="row" style="margin-bottom: 5px;">
                                        @foreach($product->bespokeArtworkPicturesDTO as $artwork)
                                            <div class="col-md-3">
                                                <div><img class='img-fluid'
                                                          src='{{@$artwork->viewOne}}'/>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @else
                                <label class="label label-inspection text-center">No Artwork Found</label>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="available">
                            <h5 class="productDetailsLabel text-center">Available Fabric</h5>
                            @if(isset($product->bespokeFabricPicturesDTO) && is_array($product->bespokeFabricPicturesDTO))
                                @if(count($product->bespokeFabricPicturesDTO))
                                    <div class="row">
                                        @foreach($product->bespokeFabricPicturesDTO as $material)
                                            <div class="col-md-3">
                                                <div>
                                                    <img class='img-fluid'
                                                         src='{{@$material->viewOne}}'/>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            @else
                                <label class="label label-inspection text-center">No Fabric Found</label>
                            @endif
                        </div>
                    </div>
                </div>

                </div>


                <hr>
                <div class="container seller-edit pull-right wow animated fadeInRight" data-wow-delay=".5s">
                    <a href='{{route('designer.bespoke.edit', [str_slug(strtolower($storeName)),str_slug($product->productName),$product->id])}}'
                       class="btn btn-wawooh">Edit Product</a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $(".add-stock").click(function () {
            stock = $(this).closest('.stock ').children('.stockNo');
            currentStock = parseInt(stock.val());
            newStock = ++currentStock;

            stock.val(newStock);
        });

        $('.addProductAttribute').click(function () {
            if ($(this).attr('productcount') === '0') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbOne img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-one').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbOneSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '1') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbTwo img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-two').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbTwoSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '2') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbThree img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-three').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbThreeSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '3') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbFour img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-four').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbFourSize').removeClass('hide');
            } else if ($(this).attr('productcount') === '4') {
                var img = $(this).parents('.product-des').siblings('.col-12').find('.productThumbFive img').attr('src');
                $(this).parents('.product-des').siblings('.col-12').find('.productDisplay img').attr('src', img);
                $('.productThumb').addClass('hide');
                $('.productDisplay').addClass('hide');
                $('.product-five').removeClass('hide');
                $('.thumbD').addClass('hide');
                $('.thumbFiveSize').removeClass('hide');
            }

        });

        $(document).on('click', '.product-small-img-added2 div img, .product-small-img-added3 div img, .product-small-img-added4 div img, .product-small-img-added5 div img', function () {
            console.log($(this).attr('src'));
            var pictureSwap = $(this).attr('src');
            $('#mainPicture').attr('src', pictureSwap);
        });

        $('.product-small-img >div >img').on('click', function () {
            console.log('fjsjjs');
            let src = $(this).attr('src');
            let url = '' + src;
            $('.product-small-img >div >img').css('border', 'none').removeClass('brand-bd-color');

            $(this).css({
                borderWidth: 1,
                borderStyle: "solid"
            }).addClass('thumbBorder');
            $(this).parents('.productThumb').siblings('.productDisplay').find('a').attr('href', src);
            $(this).parents('.productThumb').siblings('.productDisplay').find('img').attr('src', src);

        });

        $(".minus-stock").click(function () {
            stock = $(this).closest('.stock ').children('.stockNo');
            currentStock = parseInt(stock.val());
            newStock = --currentStock;

            if (newStock >= 0) {
                stock.val(newStock);
            } else {
                stock.notify("Stock can't be less than 0");
            }
        });

        var productId = parseInt('{{$product->id}}');
        var updateFlag = false;

        $("#update-quantity").click(function () {
            $('#update-quantity').val('Updating Stock...').attr('disabled', 'disabled');
            if (updateFlag) {
                $(this).notify("Please wait ...... don't click again", "primary");
                $.notify("Please wait ...... don't click again", "primary");
            } else {
                updateFlag = true;
                var productSizes = [];
                $.each($(".stock"), function () {
                    productSizes.push({
                        "name": $(this).children('.stockNo').attr('id'),
                        "stockNo": parseInt($(this).children('.stockNo').val())
                    });
                });

                data = {
                    'id': productId,
                    'productSizes': productSizes
                }
                var tok = decodeURI(<?= session('designerToken') ?>);
//console.log();
                if (typeof tok === 'undefined') {
                    var tok = '';
                }
//console.log(tok);

                $.ajax({
                    url: '{{env('GET_BASE_URL')}}/fashion/product/updateproductstock',
                    method: 'POST',
                    data: JSON.stringify(data),
                    headers: {
                        'Authorization': tok,
                        'Content-Type': 'application/json; charset=utf-8',
                        'accept': 'application/json'
                    },
                    success: function () {
                        $('#update-quantity').val('UPDATE STOCK').removeAttr('disabled', 'disabled');
                        $.notify("Stock has been updated", "success");
                        updateFlag = false;
                    },
                    error: function () {
                        $('#update-quantity').val('UPDATE STOCK').removeAttr('disabled', 'disabled');
                    }
                });
            }
        });
    </script>
@endpush