@extends('layouts.designer.designer')

@push('styles')
    <style>
        .proImg > img {
            height: 150px !important;
        }
    </style>
@endpush


@section('pageTitle', 'Bespoke Products')

@section('content')

    <div class="content-wrapper">
        <div class="row grid-margin">
            <div class="row">
                <div class="col-md-12 product-pics allproducts">
                    <div id="products">
                        @if(is_array($bespokeProducts))
                            @if(count($bespokeProducts))
                                <div class="row-holder">
                                    @foreach($bespokeProducts as $bespokeProduct)
                                        <div class="col-md-3 effect mb-5">
                                            <div class="holder-container">
                                                <div class="img-height">
                                                    <a href="{{route('designer.bespokeproduct.details', [str_slug($storeName), $bespokeProduct->id])}}">
                                                        <img src="{{@$bespokeProduct->bespokePicturesDTO->viewOne}}">
                                                        <img class="under-img"
                                                             src="{{@$bespokeProduct->bespokePicturesDTO->viewTwo}}">
                                                    </a>
                                                </div>
                                                <span class="verify-status verified">VERIFIED</span>
                                                <div class="p-details">
                                                     <span class="p-name font-reduce display-type">
                                                         <a style="color: gray !important;"
                                                            href="{{route('designer.bespokeproduct.details', [str_slug($storeName), $bespokeProduct->id])}}">{{@$bespokeProduct->productName}}</a>
                                                     </span>
                                                </div>
                                                <div class="price">
                                                    <ul>
                                                        <li>
                                                            ₦<span>{{number_format(@$bespokeProduct->estimatedSewingPrice)}}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            @else
                            <div class="row-holder">
                                <div class="col-md-5 col-md-offset-5">
                                    <label class="label label-warning">
                                        <span>No Bespoke Product Found</span>
                                    </label>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>


        </div>


    </div>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>


    <script>

        var cloudinaryDetails = {
            uploadPreset: 'c0tphuck',
            apiKey: '629146977531321',
            cloudName: 'har9qnw3d',
            secure_url: [],
            progress: 0
        };

        var fabricImageDetails = {
            uploadPreset: 'uo6e5glm',
            image_url: {},
            secure_url: [],
            progress: 0
        };

        var artWorkImageDetails = {
            uploadPreset: 'uo6e5glm',
            image_url: {},
            secure_url: [],
            progress: 0
        };

        var bespokePicturesDTO = {
            viewFive: "",
            viewFour: "",
            viewOne: "",
            viewThree: "",
            viewTwo: ""
        };

        var bespokeArtworkPicturesDTO = [];

        var bespokeFabricPicturesDTO = [];

        var counter = 0;


        myMeasure = sellect("#my-measurement", {
            originList: [
                "ANKLE",
                "ARMHOLE",
                "BACKSHIRTLENGTH",
                "BICEPS",
                "BLOUSELENGTH",
                "BUBALENGTH",
                "BUST",
                "CROTCH",
                "CHEST",
                "ELBOWCIRCUMFERENCE",
                "FISTCIRCUMFERENCE",
                "FULLLENGTH",
                "HALFLENGTH",
                "HIPCIRCUMFERENCE",
                "HIPS",
                "KNEECIRCUMFERENCE",
                "KNEELENGTH",
                "LAPCIRCUMFERENCE",
                "LONGSLEEVE",
                "LEFTFOOT",
                "RIGHTFOOT",
                "NECK",
                "SENATORLENGTH",
                "SHIRTLENGTH",
                "SHORTLENGTH",
                "SHORTSLEEVE",
                "SHOULDERWIDTH",
                "THIGH",
                "THREEQUARTERSLEEVELENGTH",
                "TROUSERLENGTH",
                "WAIST",
                "WRIST"
            ],
            destinationList: []
        });

        myMeasure.init();


        $(document).on('change', '.proFabricImgInput', function (e) {
            var myfiles = e.target.files;
            var file = $(this).get(0).files[0];
            console.log(file);
            if (typeof file === 'undefined') {
                return false;
            }

            var that = $(this);

            if (file.size / 1024 > 1200) {
                swal("Cannot upload image size more than 500kb", "", "warning");
            } else {
                convertMoreToBase64(this, '.proFabricImg');
                const formData = new FormData();
                console.log(file[0]);
                formData.append('file', myfiles[0]);
                formData.append('upload_preset', cloudinaryDetails.uploadPreset);
                formData.append('tags', 'upload');
                var request = new XMLHttpRequest();


                request.upload.onprogress = function (e) {
                    if (e.lengthComputable) {
                        console.log(Math.round(e.loaded / e.total * 100) + '%');
                        cloudinaryDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                    }
                };


                request.onreadystatechange = function () {
                    if (request.readyState === 4) {
                        console.log(JSON.parse(request.response));


                        cloudinaryDetails.secure_url.push(JSON.parse(request.response).secure_url);

                        console.log(that.attr('image-attr'));
                        var imageAttr = that.attr('image-attr');
                        bespokePicturesDTO[imageAttr] = JSON.parse(request.response).secure_url


                    }
                }

                request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
                request.send(formData);

            }
        });

        $('#mat1').on('change', async function (e) {
            console.log(e.target.files);
            fabricImageDetails.image_url = e.target.files;
            console.log(fabricImageDetails.image_url);

            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;

                $('#artworkModal').modal();
                pix.push(await toBase64($(this).get(0).files[i]));
                $('#materialPrice').show();
            }
            showMaterials(pix);
        });


        function showMaterials(arr) {
            if (arr.length) {
                var material = '';
                for (i = 0; i < arr.length; i++) {
                    // console.log(arr[i]);
                    material += `<label id=${counter} class="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="img-holder-display">
                                            <img src="${arr[i]}" alt="" class="art-modal-img materialImg">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" style="padding: 0">
                                        <div class="form-group">
                                            <label for="price">Enter Price</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">₦</span>
                                                </div>
                                                <input type="number" placeholder="Enter Fabric Price" class="form-control fabricPrice">
                                            </div>
                                        </div>
                                        </div>
                                        <div class = "col-md-6">
                                        <div class="form-group">
                                            <label for="fabricName">Enter Artwork Name</label>
                                                <input type="text" class="form-control fabricName" id="fabricName" placeholder="Enter Fabric Name">
                                        </div>
                                    </div>
                                 </div>
                                 </label>`;
                    counter++;
                }

                //$('.materialBox').append(material);
                $('#art-display').append(material);
            }
        }


        $(document).on('click', '.modalSaveBtn', function () {
            var artW = $('.art-modal-img ').attr('src');
            var fabricPrice = $('.fabricPrice').val();
            var fabricName = $('.fabricName').val();

            $('.imageAddDiv').each(function () {
                if ($('.imageAddDiv').hasClass('tempArtWork')) {
                    $(this).remove();
                }
            });

            $(".prodd").append(`
            <div class="imageAddDiv">
            <span class="imageAddDivDel"><i class="fa fa-trash"></i></span>
           <label class="proImg">
          <img class="pImg materialImg" src=${artW}>
          <p class="fPriceDiv">Price: <span class="fPrice">${fabricPrice}</span></p>
          <p class="fNameDiv">Name:  <span class="fName">${fabricName}</span></p>
          </label></div>`);

            $('#artworkModal').removeClass('show');
            $('#artworkModal').css('display', 'none');
            //$(".modalSaveBtn").attr('data-dismiss', 'modal');
            const formData = new FormData();
            console.log(fabricImageDetails.image_url);
            console.log(fabricImageDetails.image_url[0]);
            formData.append('file', fabricImageDetails.image_url[0]);
            formData.append('upload_preset', fabricImageDetails.uploadPreset);
            formData.append('tags', 'upload');

            var request = new XMLHttpRequest();


            request.upload.onprogress = function (e) {
                console.log(Math.round(e.loaded / e.total * 100) + '%');
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    fabricImageDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                }
            };


            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    console.log(JSON.parse(request.response).secure_url);
                    fabricImageDetails.secure_url.push(JSON.parse(request.response).secure_url)

                    bespokeFabricPicturesDTO.push({
                        viewOne: JSON.parse(request.response).secure_url
                    })

                }
            }

            request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
            request.send(formData);


            $('#art-display').html('');

        });


        $('#artworks').on('change', async function (e) {
            console.log(e.target.files[0]);
            artWorkImageDetails.image_url = e.target.files[0];

            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {

                //console.log($(this).get(0).files[i].size /1024);
                var fileSize = $(this).get(0).files[i].size / 1024;

                $('#artworkModal2').modal({
                    keyboard: false
                });
                pix.push(await toBase64($(this).get(0).files[i]));
                $('.artworkprice').html(`
                    <input type="text" placeholder="Enter Material Price">
                    `);


            }
            $(this).val("");
            showArtworkmages(pix);
            addDelListener();
        });

        function showArtworkmages(arr) {
            if (arr.length) {
                let artworkImage = '';
                for (i = 0; i < arr.length; i++) {
                    artworkImage += `<label id=${counter} class="">
                                <div class="row artWf">
                                    <div class="col-md-12">
                                        <div class="img-holder-display">
                                            <img src=${arr[i]} alt="" class="imgFaB">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="form-group">
                                            <label for="price">Enter Price</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">₦</span>
                                                </div>
                                                <input type="number" id="price" placeholder="Enter Price for Artwork"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                 </div>
                                 </label>`;
                    counter++;
                }
                //$('.arrange-artImg').append(artworkImage);
                $('#art-display-2').append(artworkImage);
            }
        }

        function addDelListener() {
            $(document).on('click', '.removeIMage', function () {
                var piximg = $(this).attr('pix');
                $(piximg).remove();
            });
        }


        $(document).on('click', '.modalSaveFabricBtn', function () {

            var Image = $(this).closest('.modal-footer').siblings('.modal-body').find('.imgFaB').attr('src');
            var Price = $(this).closest('.modal-footer').siblings('.modal-body').find('#price').val();

            // $('.imageFabricDiv').has('.tempFabrice').remove();
            $('.imageFabricDiv').each(function () {
                if ($('.imageFabricDiv').hasClass('tempFabrice')) {
                    // $('.imageFabricDiv').remove();
                    $(this).remove();
                }
            });

            $('.artPW').append(`
           <div class="imageFabricDiv">
             <span class="imageAddDivDel"><i class="fa fa-trash"></i></span>
           <label class="proImg">
          <img class="pImg artWorkImage" src=${Image}>
          <div>Price: <span class="fPrice">${Price}</span></div>
          </label></div>
          `);


            const formData = new FormData();
            console.log(artWorkImageDetails.image_url);
            // console.log(artWorkImageDetails.image_url[0]);
            formData.append('file', artWorkImageDetails.image_url);
            formData.append('upload_preset', artWorkImageDetails.uploadPreset);
            formData.append('tags', 'uploads');

            var request = new XMLHttpRequest();


            request.upload.onprogress = function (e) {
                console.log(Math.round(e.loaded / e.total * 100) + '%');
                if (e.lengthComputable) {
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    artWorkImageDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                }
            };


            request.onreadystatechange = function () {
                if (request.readyState === 4) {
                    console.log(JSON.parse(request.response).secure_url);
                    artWorkImageDetails.secure_url.push(JSON.parse(request.response).secure_url)
                    bespokeArtworkPicturesDTO.push({
                        viewOne: JSON.parse(request.response).secure_url
                    })

                }
            }

            request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
            request.send(formData);


            $('#art-display-2').html('');

        });


        $('#addBtn').click(function () {
            let measurement = JSON.stringify(myMeasure.getSelected());

            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>

            console.log(tok);

            /* var data = {
                productName: $('#productName').val(),
                estimatedSewingPrice: Number($('#estimatedSewingPrice').val()),
                estimatedDuration: Number($('#estimatedDuration').val()),
                description: $('#description').val(),
                bespokePicturesDTO: bespokePicturesDTO,
                bespokeFabricPicturesDTO: bespokeFabricPicturesDTO,
                bespokeArtworkPicturesDTO: bespokeArtworkPicturesDTO,
                requiredMeasurements: JSON.stringify(measurement),
                designerId: 1,
                categoryId: 21

            };*/


            var data = {
                "bespokeArtworkPicturesDTO": [
                    {
                        "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                    },
                    {
                        "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                    },
                    {
                        "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                    }
                ],
                "bespokeFabricPicturesDTO": [
                    {
                        "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                    },
                    {
                        "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                    },
                    {
                        "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                    }
                ],
                "bespokePicturesDTO": {
                    "viewFive": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg",
                    "viewFour": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg",
                    "viewOne": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg",
                    "viewThree": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg",
                    "viewTwo": "https://res.cloudinary.com/har9qnw3d/image/upload/…8/bespoke-wawooh-artwork/zyn5zwa4ibwjjc4gfz1y.jpg"
                },
                "categoryId": 21,
                "description": "Nice shirt",
                "designerId": 1,
                "estimatedDuration": 500,
                "estimatedSewingPrice": 8000,
                "id": "",
                "productName": "T-Lewin",
                "requiredMeasurements": "neck, waist, hands, belly, shoulders"

            }

            console.log(data);


            $.ajax({
                url: "https://wawooh.com:8094/fashion/secure/designer/bespoke/add",
                type: "POST",
                dataType: "json",
                headers: {
                    'Authorization': tok,
                    'Content-Type': 'application/json; charset=utf-8',
                    'accept': 'application/json'
                },
                data: JSON.stringify(data),
                success: function (result) {
                    console.log(result);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        });


    </script>


@endpush