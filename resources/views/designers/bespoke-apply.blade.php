@extends('layouts.designer.designer')

@section('pageTitle','Bespoke Application')
@section('content')
    <div class="content-wrapper white">
        <div class="row grid-margin">
            {{--<span class="label label-info">Here you can update your profile details</span>--}}
            <div class="col-md-10 bespoke-form animated fadeInUp">
                <h3>Fill the following</h3>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">What type of fashion pieces do you
                                make?</label>
                        </div>
                        <div class="col-md-12 label-styled error-notify-sel">
                            <div class="pretty p-icon p-square p-tada">
                                <input value="Clothing" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="bespoke-type">Clothing</label>
                                </div>
                            </div>

                            <div class="pretty p-icon p-square p-tada">
                                <input value="Footwear" type="checkbox" name="bespoke-piece" id="bespoke-type   "/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="bespoke-type">Footwear</label>
                                </div>
                            </div>

                            <div class="pretty p-icon p-square p-tada">
                                <input value="Accessories" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="bespoke-type">Accessories</label>
                                </div>
                            </div>

                            <div class="pretty p-icon p-square p-tada">
                                <input value="Bag" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="bespoke-type">Bag</label>
                                </div>
                            </div>

                            <div class="pretty p-icon p-square p-tada">
                                <input value="Others" type="checkbox" name="bespoke-piece" id="bespoke-type"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="bespoke-type">Others</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">What category do you cater to?</label>
                        </div>
                        <div class="col-md-12 label-styled error-notify-sel">
                            <div class="pretty p-icon p-square p-tada">
                                <input type="checkbox" class="form-control" value="Male" name="cater-cat"
                                       id="cater-cat"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="cater-cat">Male</label>
                                </div>
                            </div>

                            <div class="pretty p-icon p-square p-tada">
                                <input type="checkbox" class="form-control" value="Female" name="cater-cat"
                                       id="cater-cat"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="cater-cat">Female</label>
                                </div>
                            </div>

                            {{--<div class="pretty p-icon p-square">
                                <input type="checkbox" class="form-control" value="Unisex" name="cater-cat"
                                       id="cater-cat"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="cater-cat">Unisex</label>
                                </div>
                            </div>--}}

                            <div class="pretty p-icon p-square p-tada">
                                <input type="checkbox" name="cater-cat" value="Kid" id="cater-cat"/>
                                <div class="state p-warning">
                                    <i class="icon mdi mdi-check"></i>
                                    <label for="cater-cat">Kids</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="margin-top: 13px;">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>What is the size of your entire workforce?</label>
                            <div class="col-md-12 label-styled error-notify-sel" id="error-notify">
                                {{--<div class="pretty p-icon p-round p-tada">
                                    <input id="bes-size" value="1 - 3 Persons" name="work-size" class="form-control"
                                           type="radio"/>
                                    <div class="state p-warning">
                                        <i class="icon mdi mdi-check"></i>
                                        <label for="bes-size">1 - 3 Persons</label>
                                    </div>
                                </div>

                                <div class="pretty p-icon p-round p-tada">
                                    <input id="bes-size" value="3 - 5 Persons" name="work-size" class="form-control"
                                           type="radio"/>
                                    <div class="state p-warning">
                                        <i class="icon mdi mdi-check"></i>
                                        <label for="bes-size">3 - 5 Persons</label>
                                    </div>
                                </div>

                                <div class="pretty p-icon p-round p-tada">
                                    <input id="bes-size" value="5 - 10 Persons" name="work-size" class="form-control"
                                           type="radio"/>
                                    <div class="state p-warning">
                                        <i class="icon mdi mdi-check"></i>
                                        <label for="bes-size">5 - 10 Persons</label>
                                    </div>
                                </div>

                                <div class="pretty p-icon p-round p-tada">
                                    <input id="bes-size" value="10 - 20 Persons" name="work-size" class="form-control"
                                           type="radio"/>
                                    <div class="state p-warning">
                                        <i class="icon mdi mdi-check"></i>
                                        <label for="bes-size">10 - 20 Persons</label>
                                    </div>
                                </div>

                                <div class="pretty p-icon p-round p-tada">
                                    <input id="bes-size" value="20 Above" name="work-size" class="form-control"
                                           type="radio"/>
                                    <div class="state p-warning">
                                        <i class="icon mdi mdi-check"></i>
                                        <label for="bes-size">20 Above</label>
                                    </div>
                                </div>

                                <div class="pretty p-icon p-round p-tada">
                                    <input id="bes-size" value="Outsourced" name="work-size" class="form-control"
                                           type="radio"/>
                                    <div class="state p-warning">
                                        <i class="icon mdi mdi-check"></i>
                                        <label for="bes-size">Outsourced</label>
                                    </div>
                                </div>--}}
                                <input type="text" class="form-control" placeholder="Enter the size of your workforce"
                                       id="bes-size">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label for="weekly-orders">How many orders can you have under processing at any point in
                                time?</label>
                            {{-- <div class="col-md-6">
                                 <select required aria-required="true" name="" id="weekly-orders"
                                         class="form-control error-notify">
                                     <option value="0" hidden>--Select--</option>
                                     <option value="Daily">Daily</option>
                                     <option value="Weekly">Weekly</option>
                                     <option value="Monthly">Monthly</option>
                                 </select>
                             </div>--}}
                            <input required aria-required="true" type="number" id="noOfOrders"
                                   placeholder="Quantity in number"
                                   class="form-control error-notify">
                        </div>
                    </div>
                </div>

                <h3>Location/Shop Space Verification</h3>
                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="verify-method">Select Verification Method</label>
                            <select required aria-required="true" name="verify" id="verify-method"
                                    class="form-control error-notify">
                                <option value="0" hidden>--Select--</option>
                                <option value="Video Call">Video Call</option>
                                <option value="Physical Visit">Physical Visit</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6 pv" style="display: none;">
                                <div class="form-group">
                                    <label for="visit-day">Select Day for Visitation</label>
                                    <select class="form-control phyVisit" name="" id="visit-day">
                                        <option value="0" hidden>--Select Day--</option>
                                        <option value="Tuesday">Tuesday</option>
                                        <option value="Wed">Wednesday</option>
                                        <option value="Thurs">Thursday</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6 vm" style="display: none;">
                                <div class="form-group">
                                    <label for="visit-day-cal">Create an Appointment</label>
                                    <input placeholder="Select Date" type="text" id="visit-day-cal"
                                           class="form-control calendar videoCall">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="visit-time">Select Time</label>
                                    <select class="form-control" name="visit-time" id="visit-time">
                                        <option value="0" hidden>--Select--</option>
                                        <option value="10:00am">10:00am</option>
                                        <option value="11:00am">11:00am</option>
                                        <option value="12noon">12noon</option>
                                        <option value="1:00pm">1:00pm</option>
                                        <option value="2:00pm">2:00pm</option>
                                        <option value="3:00pm">3:00pm</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group vm" style="display: none; padding-top: 6px; padding-left: 8px;">
                            <label>Select one of the following</label>
                            <div class="row">
                                <div class="col-md-6 label-styled">
                                    <div class="pretty p-icon p-round">
                                        <input value="Skype" type="radio" id="video-detail" class="form-control"
                                               name="video-connect"/>
                                        <div class="state p-warning">
                                            <i class="icon mdi mdi-check"></i>
                                            <label for="video-detail">Skype ID</label>
                                        </div>
                                    </div>

                                    {{--<div class="pretty p-icon p-round">
                                        <input value="Hangout" type="radio" id="video-detail" class="form-control"
                                               name="video-connect"/>
                                        <div class="state p-warning">
                                            <i class="icon mdi mdi-check"></i>
                                            <label for="video-detail">Hangout ID</label>
                                        </div>
                                    </div>--}}

                                    <div class="pretty p-icon p-round">
                                        <input value="Whatsapp" type="radio" id="video-detail" class="form-control"
                                               name="video-connect"/>
                                        <div class="state p-warning">
                                            <i class="icon mdi mdi-check"></i>
                                            <label for="video-detail">Whatsapp</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input id="connect-detail" type="text" class="form-control videoCall"
                                               placeholder="Enter your contact ID">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group pv" style="display: none;">
                            <label for="location">Verify your location Address</label>
                            <input required aria-required="true" class="form-control" name="location" id="location"
                                   value="{{isset($designer->address) ? $designer->address : ''}}">
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label><strong><i class="fa fa-hand-point-right"></i> Please not, you must have a good camera
                                and internet enabled phone. (Calls are only within work-hours 9am -
                                5pm weekdays)</strong>
                        </label>
                    </div>
                </div>

                {{--<h3>Provide your social media ID</h3>
                <hr>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text facebook-text"><i class="fa fa-facebook"></i></div>
                                </div>
                                <input aria-required="true" id="facebook-id" placeholder="Enter your facebook ID"
                                       type="text"
                                       class="form-control error-notify">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text website-text"><i class="fa fa-globe"></i></div>
                                </div>
                                <input required aria-required="true" id="website-id" placeholder="Enter your Website ID"
                                       type="url"
                                       pattern="^(http[s]?:\/\/)?([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2}(\/([-~%\.\(\)\w\d]*\/*)*(#[-\w\d]+)?)?$"
                                       class="form-control error-notify">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text instagram-text"><i class="fa fa-instagram"></i></div>
                                </div>
                                <input required aria-required="true" id="instagram-id"
                                       placeholder="Enter your Instagram ID" type="text"
                                       class="form-control error-notify">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text twitter-text"><i class="fa fa-twitter"></i></div>
                                </div>
                                <input required aria-required="true" id="twitter-id" placeholder="Enter your Twitter ID"
                                       type="text"
                                       class="form-control error-notify">
                            </div>
                        </div>
                    </div>
                </div>--}}

                <div class="form-group text-centerC">
                    <img src="{{ asset('img/AjaxLoader.gif') }}" class="f-load" alt="">
                    <input type="button" value="Apply Now" id="apply_now" class="btn btn-bes-apply">
                </div>
            </div>
        </div>

    </div>
@stop

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#apply_now').on('click', function () {
                $(this).val('Processing....').attr("disabled", "disabled");
                $('.f-load').show();
                <?php if(!is_null(session('designerToken'))): ?>
                    tok = <?= session('designerToken') ?>;
                <?php else: ?>
                    tok = '';
                    <?php endif; ?>
                let bespokePiece = [];
                let categorySelection = [];
                let besSize = $('#bes-size').val(),
                    threadValue = $('#noOfOrders').val(),
                    verifyType = $('#verify-method').val();


                $.each($("input[name='bespoke-piece']:checked"), function () {
                    bespokePiece.push($(this).val());
                });
                $.each($("input[name='cater-cat']:checked"), function () {
                    categorySelection.push($(this).val());
                });

                var arrayLengthP = [];
                var arrayLengthV = [];

                if($('#verify-method').val() === 'Physical Visit'){
                    $('.phyVisit').each(function(){
                        if($(this).val() === '' ){
                            arrayLengthP.push($(this));
                            $(this).addClass('error')
                        }
                    });
                    if($('#visit-time').val() === ''){
                        arrayLengthP.push($('#visit-time'));
                        $(this).addClass('error')
                    }

                    if($('#visit-time').val() === '0'){
                        arrayLengthP.push($('#visit-time'));
                        $(this).addClass('error')
                    }


                }else if($('#verify-method').val() === 'Video Call'){
                    $('.videoCall').each(function () {
                       if($(this).val() === ''){
                           arrayLengthV.push($(this));
                           $(this).addClass('error')
                       }

                    });

                    if($('#visit-time').val() === ''){
                        arrayLengthV.push($('#visit-time'));
                        $(this).addClass('error')
                    }

                    if($('#visit-time').val() === '0'){
                        arrayLengthV.push($('#visit-time'));
                        $(this).addClass('error')
                    }

                    if($('[name="video-connect"]:checked').val() === ''){
                        arrayLengthV.push('error');
                        $(this).addClass('error')
                    }

                    if($('[name="video-connect"]:checked').val() === undefined){
                        arrayLengthV.push('error');
                        $(this).addClass('error')
                    }
                }

                var arrr = [4,6];

                if (bespokePiece.length <= 0 || categorySelection.length <= 0) {
                    swal('At least, one of the following must be selected', '', 'warning');
                    $('#apply_now').val('Apply Now').removeAttr("disabled", "disabled");
                    $('.f-load').hide();
                    $('.error-notify-sel').addClass('error');
                } else if (besSize === '' || threadValue === '' || verifyType === '' || verifyType === '0') {
                    swal('Fill all required fields', '', 'warning');
                    $('.error-notify').addClass('error');
                    $('#apply_now').val('Apply Now').removeAttr("disabled", "disabled");
                    $('.f-load').hide();
                    $('.error-notify-sel').removeClass('error');
                }else if(arrayLengthP.length > 0 || arrayLengthV.length > 0){
                    swal('Some required field', '', 'warning');
                  //  $('.imptField').addClass('error');
                    $('#apply_now').val('Apply Now').removeAttr("disabled", "disabled");
                    $('.f-load').hide();
                } else {
                     let data = {
      designerId: {{isset($designer->id)}} ? {{$designer->id}} : '',
     productType: bespokePiece,
     category: categorySelection,
     workForceSize: besSize,
     // thresholdType: threadType,
     thresholdValue: threadValue,
     locationVerificationType: verifyType,
     locationVerificationMethod: $("input[name='video-connect']:checked").val() ? $("input[name='video-connect']:checked").val() : '',
     locationVerificationMethodId: $('#connect-detail').val(),
     facebookId: $('#facebook-id').val(),
     twitterId: $('#twitter-id').val(),
     instagramId: $('#instagram-id').val(),
     websiteId: $('#website-id').val(),
     workshopAddress: $('#location').val(),
     time: $('#visit-time').val(),
     dayForVisitation: $('#verify-method').val() === 'Video Call' ? $('.calendar').val() : $('#visit-day').val()
 };
 //console.log(data);
$.ajax({
     type: 'POST',
     data: JSON.stringify(data),
     contentType: "application/json; charset=utf-8",
     dataType: "json",
     headers: {
         'Authorization': tok,
         'Content-Type': 'application/json; charset=utf-8',
         'accept': 'application/json'
     },

     url: '{{env('GET_BASE_URL')}}{{env('DESIGNER_ACCOUNT_API')}}bespoke/apply',

     success: function (result) {
         // console.log(result);
         if (result.status === '00') {
             swal('Application Successful', 'We will get back to you soon', 'success');
             $('#apply_now').val('Application Successful').attr("disabled", "disabled");
             $('.f-load').hide();
             window.location.href = "{{route('designer.product', str_slug(strtolower($storeName)))}}"
         } else {
             swal('Application Not Successful', 'Try Again Later', 'error');
             $('#apply_now').val('Apply Now').removeAttr("disabled", "disabled");
             $('.f-load').hide();
         }
     },
     error: function (e) {
         // console.log(e);
         swal(e, '', 'error');
         $('#apply_now').val('Apply Now').removeAttr("disabled", "disabled");
         $('.f-load').hide();
     },
 });
                }

            });
        })
    </script>
@endpush
