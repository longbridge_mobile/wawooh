@extends('layouts.designer.designer')

@push('styles')
    <style>

    </style>
@endpush

@section('pageTitle', 'Bespoke Requests')
@section('content')
    <div class="content-wrapper">
        <div class="row grid-margin">
            <div class="col-12">
                <h3 style="margin-bottom: 15px;" class="makeBold">Bespoke Request Bids</h3>
                <div class="card">
                    <div class="card-body">
                        <div class="all-order">
                            <div class="row">
                                <div class="col-md-8">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Category/Gender</td>
                                            <td>
                                                @if($listBid->gender == 1)
                                                    <span>Men</span>
                                                @elseif($listBid->gender == 11)
                                                    <span>Women</span>
                                                @elseif($listBid->gender == 41)
                                                    <span>Kids</span>
                                                @else
                                                    <span>Others</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Height</td>
                                            <td>{{$listBid->heightFeet.'\'' . $listBid->heightInches}}</td>
                                        </tr>
                                        <tr>
                                            <td>User Expected Delivery Date</td>
                                            <td colspan="2">
                                                {{\Carbon\Carbon::createFromTimestamp($listBid->timeOfDelivery / 1000)->format('dS F, Y')}}

                                                <small class="text-success">
                                                    ({{\Carbon\Carbon::createFromTimestamp($listBid->timeOfDelivery / 1000)->diffForHumans()}}
                                                    )
                                                </small>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>User Preferred Fabric</td>
                                            <td>{{$listBid->fabricType}}</td>
                                        </tr>
                                        <tr>
                                            <td>User Note</td>
                                            <td><span style="line-height: 23px;">{!! nl2br($listBid->notes) !!}</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>User Estimated Budget</td>
                                            <td>
                                                ₦{{number_format($listBid->budget)}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Date Ordered</td>
                                            <td>{{\Carbon\Carbon::createFromTimestamp($listBid->orderDate / 1000)->format('dS M, Y')}}

                                                <small class="text-success">
                                                    ({{\Carbon\Carbon::createFromTimestamp($listBid->orderDate / 1000)->diffForHumans()}}
                                                    )
                                                </small>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Bid Expiry Date</td>
                                            <td>
                                                @if(!is_null($listBid->bidEndDate))
                                                    {{\Carbon\Carbon::createFromTimestamp($listBid->bidEndDate / 1000)->format('dS M, Y')}}

                                                    <small class="text-success">
                                                        ({{\Carbon\Carbon::createFromTimestamp($listBid->bidEndDate / 1000)->diffForHumans()}}
                                                        )
                                                    </small>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                    <hr>
                                    <div class="">
                                        <h3 style="font-size: 1em; font-weight: bold;">Fill the form to respond</h3>
                                        <table class="table table-bordered">
                                            <tr>
                                                <td width="200px">
                                                    <label for="bidexpirydate">Set Your Delivery
                                                        Date</label>
                                                </td>
                                                <td>
                                                    <input type="text" class="req form-control {{is_null($listBidDes->timeOfDelivery) ? 'calendar' : ''}}"
                                                           id="bidexpirydate"
                                                           value="{{!is_null($listBidDes->timeOfDelivery) ? \Carbon\Carbon::createFromTimestamp(@$listBidDes->timeOfDelivery / 1000)->format('Y-m-d') : ''}}"
                                                           placeholder="Enter Delivery Date">
                                                    <small class="text text-info">Date format: yyyy-mm-dd</small>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <label for="designerPrice">Set Your
                                                        Price</label>
                                                </td>
                                                <td>
                                                    <input type="text" class="req form-control" id="designerPrice"
                                                           value="{{!is_null($listBidDes->estimatedAmount) ? number_format(@$listBidDes->estimatedAmount) : ''}}"
                                                           {{!is_null($listBidDes->estimatedAmount) ? '': ''}} placeholder="Enter your price">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Your response note (Optional)</td>
                                                <td>
                                                    <label for="responseNote" class="sr-only"><strong>Your Response
                                                            Note</strong></label>
                                                    <textarea name="" id="responseNote"
                                                              {{!is_null($listBidDes->notes) ? '': ''}}
                                                              placeholder="Enter your response note" cols="7" rows="4"
                                                              class="form-control">{!! nl2br(@$listBidDes->notes) !!}</textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="product-image">
                                        <h3>Sample Picture</h3>
                                        <div class="main-product-img">
                                            @if($listBid->sideImage !== null)
                                                <a href="{{str_replace(['http','upload'],['https','upload/q_40'],$listBid->sideImage)}}"
                                                   data-fancybox >
                                                    <img class="prodImg" width="100%" height="100%"
                                                         src="{{str_replace(['http','upload'],['https','upload/q_40'],$listBid->sideImage)}}"
                                                         alt="">
                                                </a>
                                            @else
                                                <a href="{{str_replace(['http','upload'],['https','upload/q_40'],$listBid->bespokeStyleBank->picture)}}"
                                                   data-fancybox>
                                                    <img class="prodImg" width="100%" height="100%"
                                                         src="{{str_replace(['http','upload'],['https','upload/q_40'],$listBid->bespokeStyleBank->picture)}}"
                                                         alt="">
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($listBid->status === "PENDING")
                                <div class="row col-md-6 mt-2 text-center">
                                    <input type="button" value="Respond to Request" id="assignBidToDesigners"
                                           class="btn btn-success">
                                </div>
                            @elseif($listBid->status === "OPEN")
                                <div class="row col-md-6 mt-2 text-center">
                                    <input type="button" value="Update Quote" id="assignBidToDesigners"
                                           class="btn btn-success">
                                </div>
                            @elseif($listBid->status === "CLOSED")
                                <label class="label label-rejected">Sorry! Bid Closed</label>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('#assignBidToDesigners').click(function () {
                $('#assignBidToDesigners').val('Sending Response...').attr('disabled', 'disabled');
                if ($('.req').val() === '') {
                    $('.req').addClass('error');
                    swal('Fill required fields', '', 'warning');
                    $('#assignBidToDesigners').val('Respond to Request').removeAttr('disabled', 'disabled');
                } else {
                    let data = {
                        estimatedAmount: $('#designerPrice').val(),
                        bespokeOrderId: parseInt("{{$listBid->id}}"),
                        notes: $('#responseNote').val(),
                        timeOfDelivery: $('#bidexpirydate').val()
                    };
                    console.log(data);
                    let url = "{{route("designer.bespokeSubmit")}}";
                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        url: url,
                        success: function (result) {
                            console.log(result);
                            if (result.status === '00') {
                                swal(result.message, '', 'success');
                                window.location.href = "{{route('designerBespokeRequest', $storeName)}}";
                            } else {
                                swal(result.message, '', 'error');
                                $('#assignBidToDesigners').val('Respond to Request').removeAttr('disabled', 'disabled');
                            }
                        },
                        error: function (error) {
                            console.log(error);
                            swal(error, '', 'error');
                            $('#assignBidToDesigners').val('Respond to Request').removeAttr('disabled', 'disabled');
                        }

                    })
                }
            });

            $(".nav-item").on("click", function () {
                $(".nav-item").removeClass("arrow_box");
                $(this).addClass("arrow_box");
            });
        });
    </script>

@endpush