@extends('layouts.designer.designer')

@push('styles')
    <style>
        .hiddenValue {
            opacity: 0;
            position: absolute;
            top: -1003%;
        }
    </style>
@endpush

@section('pageTitle','My Products')
@section('content')
    <div class="content-wrapper">
        <div class="row grid-margin">
            <div class="row">
                <div class="col-md-12 product-pics allproducts">
                    <div id='products'>
                        @if(count($totalOrder))
                            <div class="row-holder">
                                @foreach($totalOrder as $product)
                                    <div class="col-md-3 effect mb-5">
                                        <div class="holder-container">
                                            @if($product->verifiedFlag == 'Y')
                                                <div class="type-link">
                                                    <input type="text"
                                                           value="{{route('productDetails', str_slug($product->name).'-'.$product->id)}}"
                                                           class="hiddenValue">
                                                    <a href="#" title="Copy Link" class="copy-link"><i
                                                                class="fa fa-copy"></i> Copy</a>
                                                </div>
                                            @endif
                                            <div class="img-height">
                                                <a href="{{route('designer.product.details', [str_slug($storeName), $product->id])}}">
                                                    <img src="{{str_replace('http','https',@$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                    <img class="under-img"
                                                         src="{{str_replace('http','https',@$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                </a>
                                            </div>
                                            @if($product->verifiedFlag == 'Y')
                                                <span class="verify-status verified">VERIFIED</span>
                                            @else
                                                <span class="verify-status unverified">UNVERIFIED</span>
                                            @endif
                                            <div class="p-details">
                                                    <span class="p-name font-reduce display-type">
                                                        <a style="color: gray; !important;"
                                                           href="{{route('designer.product.details',[str_slug($storeName), $product->id])}}">{{$product->name}} </a>
                                                    </span>
                                            </div>
                                            <div class="price">
                                                @if($product->slashedPrice > 0 )
                                                    <ul>
                                                        <li>
                                                            ₦<span>{{number_format($product->slashedPrice)}}</span>
                                                        </li>
                                                        <li class="old text-danger">
                                                            ₦{{number_format($product->amount)}}
                                                        </li>
                                                    </ul>
                                                    <ul class="discount">
                                                        <li>
                                                            &dash;{{$product->percentageDiscount}}%
                                                        </li>
                                                    </ul>
                                                @else
                                                    <ul class="pPrice">
                                                        <li>
                                                            ₦{{number_format($product->amount)}}
                                                        </li>
                                                    </ul>
                                                @endif
                                            </div>
                                            <div class="col-md-12 col-6">
                                                @if(isset($product->productQualityRating) && $product->productQualityRating > 0)
                                                    @for($i= 0; $i < $product->productQualityRating; $i++ )
                                                        <span class="fa fa-star checked"></span>
                                                    @endfor
                                                    @for($i= 0; $i < (5 - $product->productQualityRating); $i++ )
                                                        <span class="fa fa-star"></span>
                                                    @endfor
                                                @endif
                                            </div>
                                            {{--@if($product->verifiedFlag == 'Y')
                                                <div class="col-md-12">
                                                    <input type="text"
                                                           value="{{route('productDetails', str_slug($product->name).'-'.$product->id)}}"
                                                           readonly>
                                                </div>
                                            @endif--}}
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <label class="label no-product-dash label-info text-center wow animated fadeInUp"
                                   data-wow-delay=".4s">There are no products.
                                @if(!empty($storeName))<a
                                        href="{{route('designer.product.new', str_slug(@$storeName))}}"> Click to
                                    Add Product</a>@endif</label>
                        @endif
                    </div>
                    @if(isset($totalOrder))
                        <div class="test">
                            {!! $totalOrder->render() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" data-backdrop = "static" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="padding: 15px 23px;">
                    <h5 class="modal-title" id="exampleModalLabel" style="font-size: 1.2em; color: #555">Important Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" style="padding: 1.2em 1.5em; line-height: 30px;">
                    <div class="bespoke-message">
                        <strong>READY TO WEAR and BESPOKE PRODUCTS</strong> that meet our <a target="_new"
                                                                                              href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541368518/manuals/Wawooh_Style_Guide_-min.pdf">photo guidelines</a> can be uploaded here.
                        <br><br>
                        Pictures that do not meet the Photo Guidelines should be uploaded in <a
                                href="{!! route('designer.uploadImage', generateCorrectUrl(strtolower($storeName))) !!}">My Styles Catalogue</a>
                        or they would not be verified by the system admin.

                    </div>
                </div>
                <div class="modal-footer" style="padding: 10px 0">
                    <div class="form-group">
                        <button type="button" class="btn btn-secondary" id="offdisplay" data-dismiss="modal">Okay.
                        </button>
                        {{--<div class="col-lg-12 col-md-12 pl-0">
                            <div class="pretty p-icon p-square">
                                <input class="form-check-input" type="checkbox" id="offdisplay"
                                       name="notificationOff" value="true">
                                <div class="state p-warning-o">
                                    <i class="icon fa fa-check"></i>
                                    <label for="offdisplay" style="font-size: inherit !important;">Do not show message again</label>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('js/products.js')}}"></script>
    <script>

        function appendDesignerProducts(products, holder) {
            var html = "";
            if (products.length) {
                for (i = 0; i < products.length; i++) {
                    html += `
                    <div class="row">
                    `;
                    if (x != null) {
                        while (x.length) {
                            b = x.splice(0, 3);

                            html += `
                            <div class="col-md-4 col-6">
                                    <div class="reduce back-change">
                                        <a href="{{route('designer.product.details', [str_slug($storeName),str_slug(@$product->name).@$product->id])}}"><img src="{{ env('GET_BASE_URL').'/'.@$product->picture[0]->picture}}"></a>
                                        <h3>{{@$product->name}} @if(@$product->verifiedFlag == 'Y')<span class='pull-right text-success'><i class='fa fa-check'></i></span> @else <span class='pull-right text-success'><i style='font-size:12px'>Unverified</i></span> @endif</h3>
                                        <div class="row font-small">
                                            <div class="col-md-6 col-6">
                                                <p>₦ {{number_format(@$product->amount)}}</p>
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <p class="discountRate"><del>₦<span>12,000</span></del></p>
                                                
                                            </div>
                                            <div class="col-md-12 col-12">
                                                        @if(isset($product->productQualityRating) && $product->productQualityRating > 0)
                                    @for($i= 0; $i < $product->productQualityRating; $i++ )
                                <span class="fa fa-star checked"></span>
@endfor
                                    @for($i= 0; $i < (5 - $product->productQualityRating); $i++ )
                                <span class="fa fa-star"></span>
@endfor
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>`;

                        }
                    }

                    html += '</div>';
                }
            }

            $(holder).html(html);

        }

        $(document).ready(function () {
            $('#offdisplay').click(function () {
                setCookie('Notification', true, '30');
            });

            if (!getCookie('Notification')) {
                $('#exampleModal').modal({
                    show: true,
                    backdrop: 'static',
                    Keyboard: false
                });
            }

            page = 0;
            size = $('#size').val();

            $('.next').on('click', function () {
                page = parseInt($(this).attr('page'));
                var data = {
                    "page": page,
                    "size": size,
                    "subcategoryid": 2
                };
                $('#loader').show();
                prev = page - 1;
                $('.previous').attr('page', prev);
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{route('designer.products.api')}}',
                    success: function (result) {
                        $('#loader').hide();
                        $('.previous').show();

                        {{--  if(result.status == 0){
                            if(result.data.events.length > 0){
                                appendDesignerProducts(result.data.events,'#products');
                            }else{
                                $('#products').html('There are no more events');
                                $('.next').attr('disabled', 'disabled');
                            }
                        }  --}}
                    },
                    error: function (e) {
                        $('#loader').hide();
                    },
                });
                page++;
                $(this).attr('page', page);


            });
            $('.previous').on('click', function () {
                page = parseInt($(this).attr('page'));
                $('#loader').show();
                var data = {
                    "page": page,
                    "size": size
                };
                page = page + 1;
                $('.next').removeAttr('disabled');
                $.ajax({
                    type: 'POST',
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    url: '{{route('admin.events.api')}}',
                    success: function (result) {
                        $('#loader').hide();
                        if (result.status == 0) {
                            if (result.data.events.length > 0) {
                                appendAllEventsToAdminDocument(result.data.events, '#products');
                            } else {

                            }
                        }
                    },
                    error: function (e) {
                        $('#loader').hide();
                    },
                });
                $('.next').attr('page', page);

                page = page - 2;

                if (page < 0) $(this).hide();
                $(this).attr('page', page);
            });


        });

        $('.copy-link').click(function (e) {
            e.preventDefault();
            $(this).siblings('.hiddenValue').select();
            document.execCommand("copy");
        });
    </script>
@endpush