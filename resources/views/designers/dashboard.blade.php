@extends('layouts.designer.designer')

@push('styles')

@endpush

@section('pageTitle', 'My Dashboard')
@section('content')
    <div class="content-wrapper">
        <div class="col-lg-12 p-0 wow animated fadeInUp" data-wow-delay=".3s">
            <div class="alert alert-primary">
                <strong>Information! Interested in free Product Photo Shoot?
                    <a target="_new"
                       href="https://wa.me/2349057030380?text=Please, I would like to book for fashion product photoshot"
                       class="label label-danger">Click
                        to Apply Now!!!</a>
                </strong>

                <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        <div class="row">
            @if(isset($designer))
                @if(@$designer->bespokeEligible !== 'Y' && isset($storeName))
                    <div class="col-lg-12 wow animated fadeInUp" data-wow-delay=".3s">
                        <div class="alert alert-info">
                            <strong>If you are interested in taking bespoke order(s)
                                <a href="#" data-toggle="modal" data-target="#exampleModal" class="label label-danger">Click
                                    to Apply Now!!!</a>
                            </strong>

                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @elseif(@$designer->bespokeEligible === null && isset($storeName))
                    <div class="col-lg-12 wow animated fadeInUp" data-wow-delay=".3s">
                        <div class="alert alert-info">
                            <strong>If you are interested in taking bespoke order(s)
                                <a href="#" data-toggle="modal" data-target="#exampleModal" class="label label-danger">Click
                                    to Apply Now!!!</a>
                            </strong>

                            <button type="button" class="close pull-right" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @else
                @endif
            @endif
            @if(isset($designer))
                @if(@$designer->registrationProgress < 10.0)
                    <div class="col-lg-12 wow animated fadeInDown" data-wow-delay=".6s">
                        <div class="alert alert-warning">
                            <strong>Please, update your profile details to have full access to the service</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif
            @endif

            {{--@if(isset($designer))
                @if($designer->emailVerificationFlag != 'Y')
                <!-- Button trigger modal -->
                    <div class="col-lg-12 wow animated fadeInUp" data-wow-delay=".3s">
                        <div class="alert alert-info">
                            <strong>Your email is yet to be verified! Did not get activation link?
                                <a href="#" class="label label-danger">Resend Activation Link</a></strong>
                        </div>
                    </div>
                @endif
            @endif--}}
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                @if(isset($storeName))
                    <a style="width: inherit" href="{{route('designer.orders', strtolower(str_slug($storeName)))}}">
                        @endif
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-md-center">
                                    <div class="ml-3 orderDetails">
                                        <p class="mb-0">All Orders</p>
                                        @if(isset($designer))
                                            <h5>{{$designer->noOfPendingOders + $designer->noOfConfirmedOrders +$designer->noOfReadyToShipOrders + $designer->noOfShippedOrders + $designer->noOfCancelledOrders + $designer->noOfDeliveredOrders}}</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                @if(isset($storeName)) <a class="card-effect"
                                          href="{{route('designer.product', str_slug($storeName))}}"> @endif
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex align-items-center justify-content-md-center">
                                <div class="ml-3 orderDetails">
                                    <p class="mb-0">Total Products</p>
                                    <h5>{{isset($prodTotal) ? count($prodTotal) : '0'}}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-md-center">
                            <div class="ml-3 orderDetails">
                                <p class="mb-0">Total Amount</p>
                                @if(isset($designer))
                                    @if(isset($designer))
                                        <h5>₦{{number_format($designer->amountOfOrders)}}</h5>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-md-center">
                            <div class="ml-3 orderDetails">
                                <p class="mb-0">Pending Amount</p>
                                <h5>
                                    @if(isset($designer))
                                        ₦@if(@$designer->amountOfPendingOrders) {{@number_format($designer->amountOfPendingOrders)}}  @else
                                            0 @endif</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                                aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="order-head">Click to view order Statistics</h4>
                        </button>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        @include ('layouts.designer.orderDetails')
                    </div>
                </div>
            </div>
            <hr>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h4 class="order-head">Click to See Available Manuals</h4>
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541075228/manuals/VENDOR_FAQ.pdf">Vendor FAQ</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541075327/manuals/Vendor_OPs_Manual_v-1.0.pdf">Operation Manual</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/g_auto/v1541368518/manuals/Wawooh_Style_Guide_-min.pdf">Photo Guide</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                                <a style="width: inherit" href="">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex align-items-center justify-content-md-center">
                                                <div class="ml-3 orderDetails">
                                                    <img class="user-manual-image"
                                                         src="{{asset('img/adobe-pdf-icon.png')}}" alt="">
                                                    <span><a target="_new"
                                                             href="https://res.cloudinary.com/har9qnw3d/image/upload/v1571323942/designersizeguides/Vendors_TC-converted.pdf">Terms and Condition</a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="card">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Collapsible Group Item #3
                        </button>
                    </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                        3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin
                        coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                        anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings
                        occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard
                        of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>--}}
        </div>

        {{--<div class="row">
            <div class="col-md-4 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div class="wrapper d-md-flex align-items-center justify-content-center text-center text-md-left">
                            <i class="mdi mdi-facebook icon-lg text-facebook"></i>
                            <div class="wrapper ml-md-3">
                                <p class="text-facebook mb-0 font-weight-medium">15k Likes</p>
                                <small class="text-muted mb-0">You main list growing !</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div class="wrapper d-md-flex align-items-center justify-content-center text-center text-md-left">
                            <i class="mdi mdi-twitter icon-lg text-twitter"></i>
                            <div class="wrapper ml-md-3">
                                <p class="text-twitter mb-0 font-weight-medium">18k followers</p>
                                <small class="text-muted mb-0">There you are !</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <div class="wrapper d-md-flex align-items-center justify-content-center text-center text-md-left">
                            <i class="mdi mdi-linkedin icon-lg text-linkedin"></i>
                            <div class="wrapper ml-md-3">
                                <p class="text-linkedin mb-0 font-weight-medium">5k connections</p>
                                <small class="text-muted mb-0">Going good !</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>--}}
        <div class="row">
            <div class="col-lg-4 d-flex flex-column">
                <div class="row flex-grow">
                    <div class="col-12 col-md-4 col-lg-12 grid-margin stretch-card">
                        <div class="card card-effect" style="height: 250px;">
                            <div class="card-body">
                                <h6 class="card-title">Orders Today</h6>
                                <div class="row">
                                    <div class="col-12 text-center" style="padding-top: 30px">
                                        <div class="row">
                                            <div class="col-12 border-right">
                                                @if(isset($designer))
                                                    <h4>{{@$designer->noOfPendingOders}}</h4>
                                                @endif
                                                <p>Pending Orders</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            {{--<div class="col-lg-8 d-none d-flex flex-column">
                <div class="row flex-grow">
                    <div class="col-12 col-md-8 col-lg-12 grid-margin stretch-card">
                        <div class="card" style="height: 250px; background: rgb(4,3,3); color: #cd9933">
                            <div class="card-body">
                                <h6 class="card-title"></h6>
                                <div class="row">
                                    <div class="col-12 text-center" style="padding-top: 30px">
                                        <div class="row">
                                            <div class="col-12 border-right">
                                                <h4></h4>
                                                <p>Something coming here soon!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>--}}
            {{--<div class="col-md-6 col-lg-4 grid-margin stretch-card">
                <div class="card card-effect">
                    <div class="card-body">
                        <h6 class="card-title">Daily Sales</h6>
                        <div class="w-75 mx-auto">
                            <div class="d-flex justify-content-between text-center">
                                <div class="wrapper">
                                    <h4>0</h4>
                                    <small class="text-muted">Total sales</small>
                                </div>
                                <div class="wrapper">
                                    <h4>0</h4>
                                    <small class="text-muted">Compaign</small>
                                </div>
                            </div>
                            <div id="dashboard-donut-chart" style="height:250px"></div>
                        </div>
                        <div id="legend" class="donut-legend"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 grid-margin stretch-card">
                <div class="card card-effect">
                    <div class="card-body">
                        <h6 class="card-title">Total Revenue</h6>
                        <div class="w-75 mx-auto">
                            <div class="d-flex justify-content-between text-center mb-5">
                                <div class="wrapper">
                                    <h4>0</h4>
                                    <small class="text-muted">Total sales</small>
                                </div>
                                <div class="wrapper">
                                    <h4>0</h4>
                                    <small class="text-muted">Open Compaign</small>
                                </div>
                            </div>
                        </div>
                        <div id="morris-line-example" style="height:250px;"></div>
                        <div class="w-75 mx-auto">
                            <div class="d-flex justify-content-between text-center mt-5">
                                <div class="wrapper">
                                    <h4>0</h4>
                                    <small class="text-muted">Online Sales</small>
                                </div>
                                <div class="wrapper">
                                    <h4>0</h4>
                                    <small class="text-muted">Store Sales</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" data-backdrop="static" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-bespoke">
                <div class="cover-bespoke"></div>
                <div class="modal-header bes-header">
                    <h4 class="modal-title" id="exampleModalLabel">Welcome
                        back, {{isset($designer->storeName) ? $designer->storeName : ''}}</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bes-body">
                    <h3>Are you interested in taking bespoke order(s)?</h3>
                </div>
                <div class="modal-footer bes-footer">
                    <button type="button" class="btn btn-secondary" id="setDismiss" data-dismiss="modal">No, Maybe
                        later
                    </button>
                    <a href="{{route('designer.bespoke')}}" id="accept-bespoke" type="button" class="btn btn-primary">Yes,
                        Apply now</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#accept-bespoke, #setDismiss').on('click', function (e) {
                // e.preventDefault();
                setCookie("bespokeModal", "Yes", "100");
            });

            if (getCookie('bespokeModal')) {
                $('#exampleModal').modal({
                    keyboard: false,
                    show: true,
                    backdrop: 'static'
                });
            }

            /*$("#myModal").modal("show").on("shown", function () {
                window.setTimeout(function () {
                    $("#myModal").modal("hide");
                }, 5000);
            });*/
        })
    </script>
@endpush