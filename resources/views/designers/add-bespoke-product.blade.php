@extends('layouts.designer.designer')

@push('styles')
    <style>
        .proImg > img {
            height: 150px !important;
        }

        .col-image {
            position: relative;
        }

        .progress {
            display: none;
            position: absolute;
            top: 0;
            right: 0;
            padding: 10px 4px 24px 4px;
        }
    </style>
@endpush


@section('pageTitle', 'Add Bespoke')

@section('content')

    <div class="content-wrapper" style="background: #ffffff;">
         <div class="row grid-margin">
             <div class="col-12">
                 <h3 style="margin-bottom: 40px;" class="makeBold">ADD NEW BESPOKE PRODUCT</h3>

                 <div class="form-group">
                     <label for="productName">Product Name</label>
                     <input type="text" class="form-control" id="productName">
                 </div>


                 <div class="row">
                     <div class="col-6">
                         <label for="estimatedSewingPrice">Estimated Sewing Price</label>
                         <input type="text" class="form-control" id="estimatedSewingPrice">
                     </div>

                     <div class="col-6">
                         <label for="estimatedDuration">Estimated Duration (in days)</label>
                         <input type="text" class="form-control" id="estimatedDuration">
                     </div>
                 </div>


                 <div class="form-group">
                     <label for="description">Description</label>
                     <textarea class="form-control" rows="4" id="description"></textarea>
                 </div>

                 <div class="row">
                     <div class="col col-image">
                         <label for="mainFabricType-1" class="proImg">
                             <p>Main</p>
                             <img id="mainFabric" src="{{ asset('img/material1.jpg')}}"
                                  class="mainFabric proFabricImg">
                             <input multiple type='file' accept="image/png, image/jpeg" id='mainFabricType-1'
                                    image-attr="viewOne" class='hide-input mainFabricType proFabricImgInput'/>
                             <div class="mainFabricAction proFabricImgAction"></div>
                         </label>
                         <div class="progress"></div>
                     </div>

                     <div class="col col-image">
                         <label for="sideFabricType-1" class="proImg">
                             <p>Side</p>
                             <img id="sideFabric" src="{{ asset('img/material1.jpg')}}"
                                  class="sideFabric proFabricImg">
                             <input multiple type='file' accept="image/png, image/jpeg" id='sideFabricType-1'
                                    image-attr="viewTwo" class='hide-input sideFabricType proFabricImgInput'/>
                             <div class="sideFabricAction proFabricImgAction"></div>
                         </label>
                         <div class="progress"></div>
                     </div>

                     <div class="col col-image">
                         <label for='backFabricType-1' class="proImg">
                             <p>Back</p>
                             <img id="backFabric" src="{{ asset('img/material1.jpg')}}"
                                  class="backFabric proFabricImg">
                             <input multiple type='file' accept="image/png, image/jpeg" id='backFabricType-1'
                                    image-attr="viewThree" class='hide-input backFabricType proFabricImgInput'/>
                             <div class="backFabricAction proFabricImgAction"></div>
                         </label>
                         <div class="progress"></div>
                     </div>

                     <div class="col col-image">
                         <label for='frontFabricType-1' class="proImg">
                             <p>Front</p>
                             <img id="frontFabric" src="{{ asset('img/material1.jpg')}}"
                                   class="frontFabric proFabricImg">
                             <input multiple type='file' accept="image/png, image/jpeg" id='frontFabricType-1'
                                    image-attr="viewFour" class='hide-input frontFabricType proFabricImgInput'/>
                             <div class="frontFabricAction proFabricImgAction"></div>
                         </label>
                         <div class="progress"></div>
                     </div>

                     <div class="col col-image">
                         <label for='topFabricType-1' class="proImg">
                             <p>Top</p>
                             <img id="topFabric" src="{{ asset('img/material1.jpg')}}"
                                  class="topFabric proFabricImg top">
                             <input multiple type='file' accept="image/png, image/jpeg" id='topFabricType-1'
                                    image-attr="viewFive"   class='hide-input topFabricType proFabricImgInput'/>
                             <div class="topFabricAction proFabricImgAction"></div>
                         </label>
                         <div class="progress"></div>
                     </div>
                 </div>

                 <hr>

                 <div class="form-group">
                     <!-- <label for="productGroup">Product Grouping*</label> -->
                     <div class="row">
                         <div class="col-md-4">
                             <label for="Category">Category*</label>
                             <select class="form-control ct impt-control required-product" id="productGroup">
                                 <option value="" hidden> -- Select Category --</option>
                                 @if(count($categories))
                                     @foreach($categories as $category)
                                         <option value='{{$category->id}}'>{{$category->categoryName}}</option>
                                     @endforeach
                                 @endif
                             </select>
                         </div>
                         <div class="col-md-4">
                             <label for="subCategory">Sub-Category*</label>
                             <select class="form-control ct impt-control required-product" disabled='disabled'
                                     id="subcat">
                                 <option value='null' hidden>Select Sub Category</option>
                             </select>
                         </div>
                         <div class="col-md-4">
                             <label for="style">Style*</label>
                             <select class="form-control ct discountField impt-control required-product"
                                     disabled='disabled' id="style">
                                 <option value='null' hidden>Select Style</option>
                             </select>
                         </div>
                     </div>
                 </div>
                 <hr>

                 <h5 class="customLabel mt-5">Fabric Options For Bespoke*</h5>
                 <p class="least-img makeBold">Add all available fabrics</p>
                 <div class="row">
                     <div class="col-md-2">
                         <label for='mat1' class="upload-mat">
                             <img style="width: 150px" class='materialImages' src="{{ asset('img/material1.jpg')}}">
                             <input type='file' id='mat1' class='hide-input'/>
                         </label>
                     </div>
                     <div class="col-md-10 prodd"></div>
                 </div>

                 <div class="form-group" id="material-div">
                     <hr>
                     <h5 class="customLabel">Artwork Options For Bespoke*</h5>
                     <p class="least-img makeBold">Click on each box to upload an artworks (maximum of 5 fabrics can be
                         selected)</p>

                     <div class="row artWWork" style="margin-bottom: 30px;">
                         <div class="col-md-2">
                             <label for="productImage">
                                 <div class="arrange-artImg">
                                     <label for='artworks' class="proImg">
                                         <img class="artworkImages" src="{{ asset('img/landscape.png')}}">
                                         <input type='file' id='artworks' class='hide-input'/>
                                     </label>
                                     {{--<div id="artworkprice"></div>--}}
                                 </div>
                             </label>
                         </div>
                         <div class="col-md-10 artPW"></div>
                     </div>
                 </div>

                 <div class="row">
                     <div class="col-md-6">
                         <div class="form-group" id="requiredmeasurement-div">
                             <label for="my-measurement">Required Measurement*</label>
                             <div>
                                 <input type="text" id="my-measurement" class="form-control">
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="new-prod col-12">
                     <div class="buttons" style="margin-top: 10px;">
                         <div>
                             {{--<button class="btn btn-wawooh add-product">Submit</button>--}}
                             <input type="button" class="btn btn-addProduct add-product" id="addBtn" value="Submit">
                             <span class="mySpinner"></span>
                         </div>
                         <div>
                             <button class="btn btn-update" id="cancel"
                                     style="background: gray; color: white; border: 1px solid #111;">Cancel
                             </button>
                         </div>
                     </div>
                 </div>

             </div>


         </div>




        <div class="modal fade" data-backdrop="static" id="artworkModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="art-close">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-artwork" id="art-display"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-primary modalSaveBtn" data-dismiss="modal">Save
                            changes
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" data-backdrop="static" id="artworkModal2" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                        <button type="button" class="close art-close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="art-close">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="modal-artwork" id="art-display-2"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary art-close" data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-primary modalSaveFabricBtn" data-dismiss="modal">Save
                            changes
                        </button>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>


    <script>

        var prodType = 1;

        var cloudinaryDetails = {
                uploadPreset: 'c0tphuck',
                apiKey: '629146977531321',
                cloudName: 'har9qnw3d',
                secure_url: [],
                progress: 0
        };

        var fabricImageDetails = {
            uploadPreset: 'uo6e5glm',
            image_url: {},
            secure_url: [],
            progress: 0
        };

        var artWorkImageDetails = {
            uploadPreset: 'uo6e5glm',
            image_url: {},
            secure_url: [],
            progress: 0
        };

        var bespokePicturesDTO = {
            viewFive: "",
            viewFour: "",
            viewOne: "",
            viewThree: "",
            viewTwo: ""
        };

        var bespokeArtworkPicturesDTO = [];

        var bespokeFabricPicturesDTO = [];

        var counter = 0;


        myMeasure = sellect("#my-measurement", {
            originList: [
                "ANKLE",
                "ARM HOLE",
                "BACK SHIRT LENGTH",
                "BICEPS",
                "BLOUSE LENGTH",
                "BUBA LENGTH",
                "BUST",
                "CROTCH",
                "CHEST",
                "ELBOW CIRCUMFERENCE",
                "FIST CIRCUMFERENCE",
                "FULL LENGTH",
                "HALF LENGTH",
                "HIP CIRCUMFERENCE",
                "HIPS",
                "KNEE CIRCUMFERENCE",
                "KNEELENGTH",
                "LAP CIRCUMFERENCE",
                "LONG SLEEVE",
                "LEFT FOOT",
                "RIGHT FOOT",
                "NECK",
                "SENATOR LENGTH",
                "SHIRT LENGTH",
                "SHORT LENGTH",
                "SHORT SLEEVE",
                "SHOULDER WIDTH",
                "THIGH",
                "THREE QUARTER SLEEVE LENGTH",
                "TROUSER LENGTH",
                "WAIST",
                "WRIST"
            ],
            destinationList: []
        });

        myMeasure.init();



        $(document).on('change', '.proFabricImgInput', function (e) {
            var myfiles = e.target.files;
            var file = $(this).get(0).files[0];
            if (typeof file === 'undefined') {
                return false;
            }

            var that = $(this);

            if (file.size / 1024 > 1200) {
                swal("Cannot upload image size more than 500kb","","warning");
            } else {
                convertMoreToBase64(this, '.proFabricImg');



                const formData = new FormData();
                formData.append('file', myfiles[0]);
                formData.append('upload_preset', cloudinaryDetails.uploadPreset);
                formData.append('tags', 'upload');
                var request = new XMLHttpRequest();


                request.upload.onprogress = function (e) {
                    if (e.lengthComputable){
                        console.log(Math.round(e.loaded / e.total * 100) + '%');
                        that.parents('.col-image').find('.progress').css('display', 'block');
                        that.parents('.col-image').find('.progress').html(Math.round(e.loaded / e.total * 100) + '%');
                        cloudinaryDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                    }
                };


                request.onreadystatechange = function() {
                    if (request.readyState === 4) {
                      console.log(JSON.parse(request.response));

                      console.log('one');
                        that.parents('.col-image').find('.progress').html('Completed');


                      cloudinaryDetails.secure_url.push(JSON.parse(request.response).secure_url);

                        var imageAttr = that.attr('image-attr');
                        bespokePicturesDTO[imageAttr] = JSON.parse(request.response).secure_url


                    }
                }

                request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
                request.send(formData);

            }
        });

        $('#mat1').on('change', async function (e) {
            console.log(e.target.files);
            fabricImageDetails.image_url = e.target.files;
            console.log(fabricImageDetails.image_url);

            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {
                var fileSize = $(this).get(0).files[i].size / 1024;

                    $('#artworkModal').modal();
                    pix.push(await toBase64($(this).get(0).files[i]));
                    $('#materialPrice').show();
            }
            showMaterials(pix);
        });


        function showMaterials(arr) {
            if (arr.length) {
                var material = '';
                for (i = 0; i < arr.length; i++) {
                    // console.log(arr[i]);
                    material += `<label id=${counter} class="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="img-holder-display">
                                            <img src="${arr[i]}" alt="" class="art-modal-img materialImg">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                        <div class = "col-md-12">
                                        <div class="form-group">
                                            <label for="fabricName">Enter Fabric Name</label>
                                                <input type="text" class="form-control fabricName" id="fabricName" placeholder="Enter Fabric Name">
                                        </div>
                                    </div>
                                 </div>
                                 </label>`;
                    counter++;
                }

                //$('.materialBox').append(material);
                $('#art-display').append(material);
            }
        }

        $('#productGroup').on('change', function () {
            var id = $(this).val();
            styles = [];
            var data = {
                categoryId: id,
                productType: prodType
            };

            $.ajax({
                url: '{{route('getSubCatBycat')}}',
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    var result = response.data;
                    var html = `<option value='' hidden>Select</option>`;
                    if (result.length) {
                        $('#subcat').removeAttr('disabled');
                        for (i = 0; i < result.length; i++) {
                            html += `<option value='${result[i].id}'>${result[i].subCategory}</option>`;
                        }
                        $('#subcat').html(html);
                    } else {
                        $('#subcat').html(`<option value=''>No Sub Category</option>`);
                    }
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });

        $('#subcat').on('change', function () {
            var id = $(this).val();
            styles = [];
            var data = {
                id: id
            }

            $.ajax({
                url: `{{route('getStyleBySubCat')}}`,
                type: "POST",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (result) {
                    var result = result.data;
                    $('#style').prop('disabled', false);
                    var html = `<option value=''>Select</option>`;
                    if (result.length) {
                        if (result instanceof Array) {
                            $('#style').removeAttr('disabled');

                            for (i = 0; i < result.length; i++) {
                                html += `<option value='${result[i].id}' >${result[i].style}</option>`;
                            }
                        } else {
                            $.notify('error', 'error');
                        }

                    } else {
                        html += `<option value=''>No style</option>`;
                    }
                    $('#style').html(html);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });


        $(document).on('click', '.modalSaveBtn', function () {
            var artW = $('.art-modal-img ').attr('src');
            var fabricPrice = $('.fabricPrice').val();
            var fabricName = $('.fabricName').val();

            $('.imageAddDiv').each(function () {
                if ($('.imageAddDiv').hasClass('tempArtWork')) {
                    $(this).remove();
                }
            });

            $(".prodd").append(`
            <div class="imageAddDiv" style="float: left;">
            <span class="imageAddDivDel"><i class="fa fa-trash"></i></span>
           <label class="proImg">
          <img class="pImg materialImg" src=${artW}>
          <p class="fNameDiv">Name:  <span class="fName">${fabricName}</span></p>
          </label>
           <div class="progress"></div>
          </div>`);

            $('#artworkModal').removeClass('show');
            $('#artworkModal').css('display', 'none');
            //$(".modalSaveBtn").attr('data-dismiss', 'modal');
            const formData = new FormData();
            console.log(fabricImageDetails.image_url);
            console.log(fabricImageDetails.image_url[0]);
            formData.append('file', fabricImageDetails.image_url[0]);
            formData.append('upload_preset', fabricImageDetails.uploadPreset);
            formData.append('tags', 'upload');

            var request = new XMLHttpRequest();


            request.upload.onprogress = function (e) {
                console.log(Math.round(e.loaded / e.total * 100) + '%');
                if (e.lengthComputable){
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    $('.imageAddDiv').parents('.col-image').find('.progress').css('display', 'block');
                    $('.imageAddDiv').parents('.col-image').find('.progress').html(Math.round(e.loaded / e.total * 100) + '%');
                    fabricImageDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                }
            };


            request.onreadystatechange = function() {
                if (request.readyState === 4) {
                    console.log(JSON.parse(request.response).secure_url);
                    $('.imageAddDiv').parents('.col-image').find('.progress').html('Completed');

                    fabricImageDetails.secure_url.push(JSON.parse(request.response).secure_url)

                    bespokeFabricPicturesDTO.push({
                        viewOne: JSON.parse(request.response).secure_url,
                        fabricName: fabricName
                    })

                }
            }

            request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
            request.send(formData);


            $('#art-display').html('');

            console.log(bespokeFabricPicturesDTO);

        });


        $('#artworks').on('change', async function (e) {
            console.log(e.target.files[0]);
           artWorkImageDetails.image_url = e.target.files[0];

            var pix = [];
            for (var i = 0; i < $(this).get(0).files.length; ++i) {

                //console.log($(this).get(0).files[i].size /1024);
                var fileSize = $(this).get(0).files[i].size / 1024;

                    $('#artworkModal2').modal({
                        keyboard: false
                    });
                    pix.push(await toBase64($(this).get(0).files[i]));
                    $('.artworkprice').html(`
                    <input type="text" placeholder="Enter Material Price">
                    `);


            }
            $(this).val("");
            showArtworkmages(pix);
            addDelListener();
        });

        function showArtworkmages(arr) {
            if (arr.length) {
                let artworkImage = '';
                for (i = 0; i < arr.length; i++) {
                    artworkImage += `<label id=${counter} class="">
                                <div class="row artWf">
                                    <div class="col-md-12">
                                        <div class="img-holder-display">
                                            <img src=${arr[i]} alt="" class="imgFaB">
                                        </div>
                                    </div>
                                </div>

                                 </label>`;
                    counter++;
                }
                //$('.arrange-artImg').append(artworkImage);
                $('#art-display-2').append(artworkImage);
            }
        }

        function addDelListener() {
            $(document).on('click', '.removeIMage', function () {
                var piximg = $(this).attr('pix');
                $(piximg).remove();
            });
        }


        $(document).on('click', '.modalSaveFabricBtn', function () {

            var Image = $(this).closest('.modal-footer').siblings('.modal-body').find('.imgFaB').attr('src');
            var Price = $(this).closest('.modal-footer').siblings('.modal-body').find('#price').val();

            // $('.imageFabricDiv').has('.tempFabrice').remove();
            $('.imageFabricDiv').each(function () {
                if ($('.imageFabricDiv').hasClass('tempFabrice')) {
                    // $('.imageFabricDiv').remove();
                    $(this).remove();
                }
            });

            $('.artPW').append(`
           <div class="imageFabricDiv" style="float: left;">
             <span class="imageAddDivDel"><i class="fa fa-trash"></i></span>
           <label class="proImg">
          <img class="pImg artWorkImage" src=${Image}>
          </label></div>
          `);






            const formData = new FormData();
            console.log(artWorkImageDetails.image_url);
           // console.log(artWorkImageDetails.image_url[0]);
            formData.append('file', artWorkImageDetails.image_url);
            formData.append('upload_preset', artWorkImageDetails.uploadPreset);
            formData.append('tags', 'uploads');

            var request = new XMLHttpRequest();


            request.upload.onprogress = function (e) {
                console.log(Math.round(e.loaded / e.total * 100) + '%');
                if (e.lengthComputable){
                    console.log(Math.round(e.loaded / e.total * 100) + '%');
                    artWorkImageDetails.progress = Math.round(e.loaded / e.total * 100) + '%';
                }
            };


            request.onreadystatechange = function() {
                if (request.readyState === 4) {
                    console.log(JSON.parse(request.response).secure_url);
                    artWorkImageDetails.secure_url.push(JSON.parse(request.response).secure_url)
                    bespokeArtworkPicturesDTO.push({
                        viewOne: JSON.parse(request.response).secure_url
                    })

                }
            }

            request.open("POST", `https://api.cloudinary.com/v1_1/${cloudinaryDetails.cloudName}/upload`);
            request.send(formData);


            $('#art-display-2').html('');

        });


        $('#addBtn').click(function(){
            let measurement = JSON.stringify(myMeasure.getSelected());

            <?php if(!is_null(session('designerToken'))): ?>
                tok = <?= session('designerToken') ?>;
            <?php else: ?>
                tok = '';
            <?php endif; ?>

            console.log(tok);

           var data = {
               productName: $('#productName').val(),
               estimatedSewingPrice: Number($('#estimatedSewingPrice').val()),
               estimatedDuration: Number($('#estimatedDuration').val()),
               description: $('#description').val(),
               bespokePicturesDTO: bespokePicturesDTO,
               bespokeFabricPicturesDTO: bespokeFabricPicturesDTO,
               bespokeArtworkPicturesDTO: bespokeArtworkPicturesDTO,
               requiredMeasurements: JSON.stringify(measurement),
               designerId: 1,
               categoryId: $('#productGroup').val()
           };

           console.log(data);



          $.ajax({
               url: "{{env('GET_BASE_URL')}}/fashion/secure/designer/bespoke/add",
               type: "POST",
               dataType: "json",
               headers: {
                   'Authorization': tok,
                   'Content-Type': 'application/json; charset=utf-8',
                   'accept': 'application/json'
               },
               data: JSON.stringify(data),
               success: function(result){
                   console.log(result);
                   if(result.status === '00'){
                       swal(result.message, '', 'success');
                       window.location.href = '{{route('getBespokeProduct')}}';

                   }else{
                       swal(result.message, '', 'info');
                   }
               },
               error: function(error){
                   console.log(error);
               }
           })
        });



    </script>


@endpush