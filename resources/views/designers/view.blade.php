@extends('layouts.user.default')
@section('pageTitle', @$designer->storeName .' | Designer')
@section('content')
    <section class="vendorProfile">
        @if(isset($designer->logo))
            <img src="{{str_replace('http','https',@$designer->banner)}}" alt="">
        @else
            <img src="{{asset('img/designerBanner.jpeg')}}" alt="">
        @endif
        <div class="Shopcover"></div>
        <div class="bottomDetail animated wow fadeInDown" data-wow-delay=".4s" data-wow-duration="2s">
            <h3>{{@$designer->storeName}}</h3>
        </div>
        <div class="short-details">
            <span>
                <h5>Successful Delivered Orders</h5>
                {{@$designer->noOfDeliveredOrders}}
            </span>

            <span>
                 <h5>Member Since</h5>
                {{\Carbon\Carbon::parse(@$designer->createdDate)->format('M-Y')}}
                <small class="text-success">
                    ({{\Carbon\Carbon::parse(@$designer->createdDate)->diffForHumans()}})
                </small>
            </span>
        </div>
    </section>
    <div class="designerDp animated wow fadeInLeft" data-wow-delay=".6s" data-wow-duration="2s">
        @if(isset($designer->logo))
            <img src="{{str_replace('http','https',@$designer->logo)}}" alt="">
        @else
            <img src="{{ asset('img/logo-sm.png') }}" alt="W">
        @endif
    </div>
    <section class="products">
        <div class="productshow">
            <div class="product-pics">
                @if(isset($products))
                    @if(count($products))
                        @foreach(array_chunk($products, 4) as $items)

                            <div class="row-holder">
                                @foreach($items as $item)
                                    @if($item->verifiedFlag === 'Y')
                                        <div class="row-3 effect">
                                            <div class="holder-container">
                                                @if(!isset($storeName))
                                                    <div class="type-badge">
                                                        @if($item->acceptCustomSizes === 'Y')
                                                            <img src="{{asset('img/bespoke.svg')}}" alt=""
                                                                 class="bes-size">
                                                        @endif
                                                    </div>
                                                    <div class="wishlist-badge">
                                                        @if($item->wishListFlag == 'N' || is_null($item->wishListFlag) || session()->has('userToken'))
                                                            <a title="Save for later"
                                                               class="trans-0-4" id="add-wishlist"
                                                               href='{{route('addToWishList', $item->id)}}'>
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        @elseif(!session()->has('userToken'))
                                                            <script type="text/javascript">
                                                                $(document).ready(function () {
                                                                    $('#ModalSignIn').modal();
                                                                })
                                                            </script>
                                                            <i class="fa fa-heart" style='color:red'></i>
                                                        @endif
                                                    </div>
                                                @endif
                                                <div class="img-height">
                                                    @if(isset($storeName))
                                                        <a href="{{route('designer.product.details', [str_slug($storeName),str_slug($product->name).'-'.$product->id])}}">
                                                            <img src="{{@$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture}}"></a>
                                                    @else
                                                        <a href="{{route('productDetails', str_slug($item->name).'-'.$item->id)}}">
                                                            @if(count($item->productColorStyleDTOS))
                                                                @if(isset($item->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                                    <img src="{{str_replace('http','https', @$item->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                                @endif
                                                                @if(isset($item->productColorStyleDTOS[0]->productPictureDTOS[1]))
                                                                    <img class="under-img"
                                                                         src="{{str_replace('http','https',$item->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                                @endif
                                                            @endif
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="p-details">
                                            <span class="p-name display-type">
                                                <a href='{{route('productDetails', str_slug($item->name).'-'.$item->id)}}'>
                                                    {{$item->name}}
                                                </a>
                                            </span>
                                                </div>
                                                <div class="price">
                                                    @if($item->slashedPrice > 0 )
                                                        <ul>
                                                            <li>
                                                                ₦<span>{{number_format($item->slashedPrice)}}</span>
                                                            </li>
                                                            <li class="old text-danger">
                                                                ₦{{number_format($item->amount)}}
                                                            </li>
                                                        </ul>
                                                        <ul class="discount">
                                                            <li>
                                                                &dash;{{$item->percentageDiscount}}%
                                                            </li>
                                                        </ul>
                                                    @else
                                                        <ul class="pPrice">
                                                            <li>
                                                                ₦{{number_format($item->amount)}}
                                                            </li>
                                                        </ul>
                                                    @endif
                                                </div>

                                                <div class="col-md-12 col-6">
                                                    @if(isset($item->productQualityRating) && $item->productQualityRating > 0)
                                                        @for($i= 0; $i < $item->productQualityRating; $i++ )
                                                            <span class="fa fa-star checked"></span>
                                                        @endfor
                                                        @for($i= 0; $i < (5 - $item->productQualityRating); $i++ )
                                                            <span class="fa fa-star"></span>
                                                        @endfor
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endforeach

                    @else
                        <div class='alert alert-danger'>NO Products</div>
                    @endif
                @elseif(isset($designer->products))
                    @if(count($designer->products))
                        @foreach(array_chunk(@$designer->products, 4) as $products)
                            <div class="more-products row-holder">
                                @foreach($products as $product)
                                    <div class="row-3 effect">
                                        <div class="holder-container">
                                            @if(isset($storeName))
                                                <div class="img-height">
                                                    <a href="{{route('designer.product.details', [str_slug(@$storeName),str_slug($product->name).$product->id])}}">
                                                        @if(count($product->productColorStyleDTOS))
                                                            @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                                <img src="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                            @endif
                                                            @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[1]))
                                                                <img class="under-img"
                                                                     src="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[1]->picture)}}">
                                                            @endif
                                                        @endif
                                                    </a>
                                                </div>
                                            @else
                                                <div class="img-height">
                                                    <div class="type-badge">
                                                        @if($product->acceptCustomSizes === 'Y')
                                                            <img src="{{asset('img/bespoke.svg')}}" alt=""
                                                                 class="bes-size">
                                                        @endif
                                                    </div>
                                                    <div class="wishlist-badge">
                                                        @if($product->wishListFlag == 'N' || is_null($product->wishListFlag) || session()->has('userToken'))
                                                            <a title="Save for later"
                                                               class="trans-0-4" id="add-wishlist"
                                                               href='{{route('addToWishList', $product->id)}}'>
                                                                <i class="fa fa-heart"></i>
                                                            </a>
                                                        @elseif(!session()->has('userToken'))
                                                            <script type="text/javascript">
                                                                $(document).ready(function () {
                                                                    $('#ModalSignIn').modal();
                                                                })
                                                            </script>
                                                            <i class="fa fa-heart" style='color:red'></i>
                                                        @endif
                                                    </div>
                                                    <a href='{{route('productDetails', str_slug($product->name).'-'.$product->id)}}'>
                                                        @if(count($product->productColorStyleDTOS))
                                                            @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[0]))
                                                                <img src="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[0]->picture)}}">
                                                            @endif
                                                            @if(isset($product->productColorStyleDTOS[0]->productPictureDTOS[1]))
                                                                <img class="under-img"
                                                                     src="{{str_replace('http','https',$product->productColorStyleDTOS[0]->productPictureDTOS[1]->picture)}}">
                                                            @endif
                                                        @endif
                                                    </a>
                                                </div>
                                            @endif
                                            <div class="p-details">
                                            <span class="p-name display-type">
                                                {{$product->name}}
                                            </span>
                                            </div>
                                            <div class="price">
                                                @if($product->slashedPrice > 0 )
                                                    <ul>
                                                        <li>
                                                            ₦<span>{{number_format($product->slashedPrice)}}</span>
                                                        </li>
                                                        <li class="old text-danger">
                                                            ₦{{number_format($product->amount)}}
                                                        </li>
                                                    </ul>
                                                    <ul class="discount">
                                                        <li>
                                                            &dash;{{$product->percentageDiscount}}%
                                                        </li>
                                                    </ul>
                                                @else
                                                    <ul class="pPrice">
                                                        <li>
                                                            ₦{{number_format($product->amount)}}
                                                        </li>
                                                    </ul>
                                                @endif
                                            </div>
                                            <div class="col-md-12 col-6">
                                                @if(isset($product->productQualityRating) && $product->productQualityRating > 0)
                                                    @for($i= 0; $i < $product->productQualityRating; $i++ )
                                                        <span class="fa fa-star checked"></span>
                                                    @endfor
                                                    @for($i= 0; $i < (5 - $product->productQualityRating); $i++ )
                                                        <span class="fa fa-star"></span>
                                                    @endfor
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                        <div class="row mt-2">
                            <div class="col-md-12 text-center">
                                <img class="load-gif" src="{{asset('img/loading.gif')}}" alt="">
                                <a href="#" id="loader" class="btn btn-load-more loadMore">LOAD MORE</a>
                            </div>
                        </div>
                    @endif
                @else
                    <div class='alert alert-danger'>NO Products</div>
                @endif

            </div>
        </div>
    </section>
@stop

@push('scripts')
    <script>
        var page = 1;

        $(document).on('click', '.loadMore', function (e) {
            e.preventDefault();
            var data = {
                "id": "{{$designer->id}}",
                "page": page++,
                "size": 10
            }

            console.log(data);
            $.ajax({
                url: '{{route('getMoreDesignerProduct')}}',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: JSON.stringify(data),
                success: function (result) {
                    if (result.status === '00') {
                        console.log(result);
                        if (result.data.length > 0) {
                            {{--var storeName = "{{$storeName}}";--}}
                            // var storeName = storeName.trim();
                            // var storeName = storeName.replace(/\s/g, '-');
                            // console.log(storeName);
                            for (var i = 0; i < result.data.length; i++) {
                                var productName = result.data[i].name;
                                var productId = result.data[i].id;

                                // console.log(`${storeName}/product-details/${productId}`);

                                $('.more-products').append(`

                               <div class="row-3 effect">
                                   <div class="holder-container">
                                       <div class="img-height">
                                       <a href="">
                                          <img src="${result.data[i].productColorStyleDTOS[0].productPictureDTOS[0].picture}">
                                          </a>
                                       </div>
                                   </div>
                               </div>
                            `);

                            }
                        }
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            })
        })
    </script>

@endpush