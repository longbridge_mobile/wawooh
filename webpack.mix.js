let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
    processCssUrls: false
});

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })
    .sass('resources/assets/sass/wawooh.scss', 'public/css', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    }).options({
    processCssUrls: false
})
    .sass('resources/assets/sass/designerMobile.scss', 'public/css', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    }).options({
    processCssUrls: false
})
    .sass('resources/assets/sass/category.scss', 'public/css', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    }).options({
    processCssUrls: false
})
    .sass('resources/assets/sass/home.scss', 'public/css', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    }).options({
    processCssUrls: false
})
    .sass('resources/assets/sass/designerdash.scss', 'public/css/dash', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    }).options({
    processCssUrls: false
})
    .sass('resources/assets/sass/designerShop.scss', 'public/css/dash', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    }).options({
    processCssUrls: false
})
    .sass('resources/assets/sass/inspire.scss', 'public/css/inspire', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/invoice.scss', 'public/css/', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/timeline.scss', 'public/css/inspire', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/admin-wawooh.scss', 'public/css/', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/navbar.scss', 'public/css/', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/events.scss', 'public/css/', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/user.scss', 'public/css/', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/others.scss', 'public/css/', {
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/coming-soon.scss', 'public/css/',{
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/land.scss', 'public/css/',{
    outputStyle: mix.inProduction ? 'compressed' : 'nested'
    })
    .sass('resources/assets/sass/richtext.scss', 'public/css/',{
        outputStyle: mix.inProduction ? 'compressed' : 'nested'
    });

mix.browserSync('https://wawooh.test');