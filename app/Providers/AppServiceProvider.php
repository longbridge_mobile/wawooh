<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function boot()
    {
        URL::forceScheme('https');

        view()->composer(['layouts.user.header'], function ($view) {
            $pr = request('product');
            $prPosition = strrpos($pr, '-');
            $currency = 'USD';
            $productId = substr($pr, $prPosition + 1);
            $url = env('GET_BASE_URL') . env('PRODUCT_API') . "$productId/$currency/getproductbyid/review";
            $res = consume($url, 'GET');
            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $resp = $res['data'];
                    $desc = $resp->prodSummary;
                    $thumb = isset($resp->productColorStyleDTOS[0]->productPictureDTOS[0]->picture) ? str_replace('http', 'https', $resp->productColorStyleDTOS[0]->productPictureDTOS[0]->picture) : '';
                    $view->with('thumb', $thumb);
                    $view->with('desc', $desc);
                }
            }

            $eventId = request()->route('eventId');
            $url = env('GET_BASE_URL') . env('EVENT_API') . "$eventId/geteventbyId";
            $eventres = consume($url, 'GET');
            if ($eventres['status'] == 0) {
                if (isset($eventres['data']->event)) {
                    $event = $eventres['data']->event;
                    $eventThumb = isset($event->eventPictures[0]->picture) ? str_replace('http', 'https', $event->eventPictures[0]->picture) : '';
                    $view->with('eventThumb', $eventThumb);
                }
            }

        });

        view()->composer(['index', 'inc.category-menu', 'inc.footer-category', 'inc.mobile-menu'], function ($view) {
            $resp = getCategories();
            $view->with('resp', $resp);
        });

        view()->composer(['inc.navbar', 'user.track-order'], function ($view) {
            $resp2 = null;

            $currency = 'USD';
            if (session()->has('userToken')) {
                $url4User = env('GET_BASE_URL') . "/fashion/getuserdetails";
                $url2 = env('GET_BASE_URL') . "/fashion/secure/customer/order/getcart/$currency";
                $cart = consume($url2, 'GET');
                $res = consume($url4User, 'GET');
                if (isset($res['data']->userDetails)) {
                    $resp2 = $res['data']->userDetails;
                }
                if ($cart['status'] == 0) {
                    $cart = $cart['data'];
                    $view->with('resp2', $resp2)->with('cart', $cart->cartItems);
                } else {
                    $cart = array();
                    $view->with('resp2', $resp2)->with('cart', $cart);
                }
            } else if (session()->has('userCart')) {
                $cart = session('userCart');
                $view->with('resp2', $resp2)->with('cart', $cart);
            } else {
                $cart = array();
                $view->with('resp2', $resp2)->with('cart', $cart);
            }
        });

        /* \Blade::setEchoFormat('nl2br(e(%s))');*/
    }

    /**
     * Register any application services.
     * @return void
     */
    public function register()
    {
        //
    }
}
