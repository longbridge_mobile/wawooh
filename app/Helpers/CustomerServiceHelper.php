<?php
/**
 * @author
 * The Helper function for Quality Assurance activities
 * Refactor Friday, 21st September, 2018
 * LongBridge Technologies Software Developers
 */

/**
 * The QA API Consume
 * @param $api
 * @param $type
 * @param array $data
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function customerConsume($api, $type, $data = [])
{
    if (session()->has('customertoken')) {
        $token = 'Bearer ' . str_replace('"', '', session('customertoken'));
        $auth = "authorization";
    } else {
        $token = '';
        $auth = '';
    }
    $client = new GuzzleHttp\Client([
        'headers' => [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            $auth => $token

        ],
    ]);

    try {
        $response = $client->request($type, $api, $data);
    } catch (GuzzleHttp\Exception\ClientErrorResponseException $e) {
        return array('status' => 400, 'message' => 'No internet');

    } catch (GuzzleHttp\Exception\ServerErrorResponseException $e) {
        return array('status' => 500, 'message' => 'An error occurred on the server. Please try again later.');
    } catch (GuzzleHttp\Exception\ConnectException $e) {
        return array('status' => 100, 'message' => 'Please Check your internet Connection');

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        return array('status' => 400, 'message' => 'Oops something is not  right!!!, Bad method call');
    } catch (Exception $e) {
        return array('status' => 101, 'message' => 'An unknown error occurred. Please retry');

    }
    return responseToJson($response->getBody());
}