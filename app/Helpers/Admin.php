<?php
/**
 * @author
 * The Helper function for Admin activities
 * Refactor Friday, 21st September, 2018
 * LongBridge Technologies Software Developers
 */

/**
 * @param $api
 * @param $type
 * @param array $data
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */

function adminConsume($api, $type, $data = [])
{
    if (session()->has('adminToken')) {

        $token = 'Bearer ' . str_replace('"', '', session('adminToken'));;
        $auth = "authorization";
    } else {
        $token = '';
        $auth = '';
    }
    $client = new GuzzleHttp\Client([
        'headers' => [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            $auth => $token

        ],
    ]);
    try {
        $response = $client->request($type, $api, $data);
    } catch (GuzzleHttp\Exception\ClientErrorResponseException $e) {
        return array('status' => 400, 'message' => 'No internet');
    } catch (GuzzleHttp\Exception\ServerErrorResponseException $e) {
        return array('status' => 500, 'message' => 'An error occurred on the server. Please try again later.');
    } catch (GuzzleHttp\Exception\ConnectException $e) {
        return array('status' => 100, 'message' => 'Please Check your internet Connection');
    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        return array('status' => 400, 'message' => 'Oops something is not  right!!!, Bad method call');
    } catch (Exception $e) {
        return array('status' => 101, 'message' => 'An unknown error occured. Please retry');

    }
    return responseToJson($response->getBody());
}

/**
 * @return mixed
 * Get A Particular Designer Store name
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getAllDesigners()
{
    $redirectView = view('errors.noNetwork');
    $data = getLastSixMonths();
    $data = ['json' => $data];
    $url = env('GET_BASE_URL') . "/fashion/designer/getdesigners";
    $res = adminConsume($url, 'GET');
    return $designers = $res;
}