<?php
/**
 * @author
 * The Helper function for Users activities
 * Refactor Friday, 21st September, 2018
 * LongBridge Technologies Software Developers
*/

/**
 * @param $api
 * @param $type
 * @param array $data
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function consume($api, $type, $data = [])
{
    if (session()->has('userToken')) {
        $token = 'Bearer ' . str_replace('"', '', session('userToken'));
        $auth = "Authorization";
    } else {
        $token = '';
        $auth = '';
    }
    $client = new GuzzleHttp\Client([
            'headers' => [
                'content-type' => 'application/json',
                'accept' => 'application/json',
                $auth => $token,
            ],

        ]

    );
    try {
        $response = $client->request($type, $api, $data);
    } catch (GuzzleHttp\Exception\ClientErrorResponseException $e) {
        return array('status' => 400, 'message' => 'No internet');

    } catch (GuzzleHttp\Exception\ServerErrorResponseException $e) {
        return array('status' => 500, 'message' => 'An error occurred on the server. Please try again later.');
    } catch (GuzzleHttp\Exception\ConnectException $e) {
        return array('status' => 100, 'message' => 'Please Check your internet Connection');

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        return array('status' => 400, 'message' => 'Oops something is not  right!!!, Bad method call');
    } catch (Exception $e) {
        return array('status' => 101, 'message' => 'An unknown error occured. Please retry');

    }
    return responseToJson($response->getBody());
}

/**
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getCartDetails()
{

    $currency = env('CURRENCY');
    $redirectView = view('errors.noNetwork');
    $url2 = env('GET_BASE_URL') . env('USER_ORDER_API') .'getcart/'.$currency;
    $carts = consume($url2, 'GET');
    if ($carts['status'] == 0) {
        if(isset($carts['data'])) {
            $carts = $carts['data'];
        }
        else {
            return $redirectView;
        }
//        dd($carts);
        /*$total = 0;
        foreach ($carts as $cart) {
            if($cart->slashedPrice > 0){
                $total += ($cart->slashedPrice)*$cart->quantity;
            }else{
                $total += ($cart->amount)*$cart->quantity;
            }
        }*/
        return array('cart' => $carts->cartItems, 'total' => $carts->totalPrice);
    } else {
        return $carts;
    }
}

/**
 * Empty cart item function for user
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function emptyCart()
{
    $url = env('GET_BASE_URL') . env('USER_ORDER_API') . "emptycart";
    return $res = consume($url, 'GET');
}

/**
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getAddresses()
{
    $url = env('GET_BASE_URL') . "/fashion/secure/address/getaddress";
    return $addresses = consume($url, 'GET');
}

/**
 * Get a particular user detail
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getUserDetails() {
    $url = env('GET_BASE_URL')."/fashion/getuserdetails";
    return $userDetails = consume($url, 'GET');
}

/**
 * @param $id
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getProductDetails($id)
{
    $url2 = env('GET_BASE_URL') . "/fashion/product/{$id}/getproductbyid";
    return $product = consume($url2, 'GET');
}

function convertEmail($email) {
    $val = base64_encode($email);
    return $val;
}

$t = convertEmail('	vicfidelis2004@yahoo.com');
//echo $t;