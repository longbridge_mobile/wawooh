<?php
/**
 * @author
 * The Helper function for Users activities
 * Refactor Friday, 21st September, 2018
 * LongBridge Technologies Software Developers
 */

/**
 * Designer API Consume Method
 * @param $api
 * @param $type
 * @param array $data
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function designerConsume($api, $type, $data = [])
{
    if (session()->has('designerToken')) {
        $token = 'Bearer ' . str_replace('"', '', session('designerToken'));;
        $auth = "authorization";
        //return $token;
    } else {
        $token = '';
        $auth = '';
    }
    $client = new GuzzleHttp\Client([
        'headers' => [
            'content-type' => 'application/json',
            'accept' => 'application/json',
            $auth => $token

        ],
    ]);


    try {
        $response = $client->request($type, $api, $data);
    } catch (GuzzleHttp\Exception\ClientErrorResponseException $e) {
        return array('status' => 400, 'message' => 'No internet');

    } catch (GuzzleHttp\Exception\ServerErrorResponseException $e) {
        return array('status' => 500, 'message' => 'An error occurred on the server. Please try again later.');
    } catch (GuzzleHttp\Exception\ConnectException $e) {
        return array('status' => 100, 'message' => 'Please Check your internet Connection');

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
       return array('status' => 400, 'message' => 'Oops something is not  right!!!, Bad method call');
    } catch (Exception $e) {
        return array('status' => 101, 'message' => 'An unknown error occurred. Please retry');

    }
    return responseToJson($response->getBody());
}

/**
 * @return mixed
 * Get A Particular Designer Store name
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getDesignerStoreName()
{
    $redirectView = view('errors.noNetwork');
    $data = getLastSixMonths();
    $data = ['json' => $data];
//    $urlS = env('GET_BASE_URL') . "/fashion/designer/getdesignerbystorename/{storename}";
    $url = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
    $res = designerConsume($url, 'GET');
    if (isset($res['data']->storeName)) {
        return $res['data']->storeName;
    } else {
        return $redirectView;
    }
//    return $res['data']->storeName;
}

/**
 * Get A Designer details
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getDesignerDetails() {
    $redirectView = view('errors.noNetwork');
    $url = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
    $res = designerConsume($url, 'GET');
    if (isset($res['status']) == "00") {
        $desDetail = $res['data'];
        return $desDetail;
    } else {
        return $redirectView;
    }
}
