<?php

/**
 * @param $response
 * @return array|mixed
 */
function responseToJson($response)
{
    $res = json_decode($response);
    $res = collect($res)->toArray();
    return $res;
}

if (!function_exists('env_api')) {
    /**
     * Get the value of the API in the env_file
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    /*function env_api($key, $default = null) {
        $value = getenv($key);

        if($value === false) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }

        if(strlen($value) > 1 && \Illuminate\Support\Str::startsWith($value, '"') && \Illuminate\Support\Str::endsWith($value, '"')) {
            return substr($value, 1, -1);
        }

        return $value;
    }*/
}

/**
 * General Function
 * @param \Illuminate\Http\Request $request
 * @param $parameter
 * @return bool
 */
function checkUserIdentity(\Illuminate\Http\Request $request, $parameter)
{
    if ($request->has($parameter)) {
        return true;
    }
}

/**
 * @param $obj
 * @param null $view
 * @return string
 */
function displayError($obj, $view = null)
{
    return "<div class='' style='text-align:center;color:red;margin-top:250px'><h1>Please try again. See Error Details =></h4><br/>" . json_encode($obj) . "</div>";
    //return view('errors.405');
}

/**
 * @param $api
 * @param $type
 * @param array $data
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 *
 */
function consumeWithoutToken($api, $type, $data = [])
{


    $client = new GuzzleHttp\Client([
        'headers' => [
            'content-type' => 'application/json',
            'accept' => 'application/json'
        ],
    ]);

   // return $client->request($type, $api, $data);
    try {
        $response = $client->request($type, $api, $data);
    } catch (GuzzleHttp\Exception\ClientErrorResponseException $e) {
        return array('status' => 400, 'message' => 'No internet');

    } catch (GuzzleHttp\Exception\ServerErrorResponseException $e) {
        return array('status' => 500, 'message' => 'An error occurred on the server. Please try again later.');
    } catch (GuzzleHttp\Exception\ConnectException $e) {
        return array('status' => 100, 'message' => 'Please Check your internet Connection');

    } catch (GuzzleHttp\Exception\BadResponseException $e) {
        return array('status' => 400, 'message' => 'Oops something is not  right!!!, Bad method call');
    } catch (Exception $e) {
       
        return array('status' => 101, 'message' => 'An unknown error occured. Please retry');

    }
    return responseToJson($response->getBody());
}

/**
 * General Function
 * @param $string
 * @return int
 */
function extractNumberOnly($string)
{
    return $int = intval(preg_replace('/[^0-9]+/', '', $string), 10);
}

function activeProducts($products)
{
    $arr = array();
    foreach (array_reverse($products) as $product) {
        if ($product->designerStatus == 'A') {
            array_push($arr, $product);
        }
    }
    return $arr;
}

/**
 * @param $failed
 * @param $success
 * @param null $var
 * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function checkIfAuthenticated($failed, $success, $var = null)
{
    if (!session('userToken')) {
        return redirect($failed);
    } else {
        return view($success, $var);
    }
}

/**
 * @return array|mixed
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getCategories()
{
    $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getcategories";
    $res = consume($url, 'GET');
    if (count($res) > 0) {
        if ($res['status'] == 0) {
            return $categories = $res['data'];
        } else {
            return $res;
        }
    }
}

/**
 * Get all sizes in system
 * @throws \GuzzleHttp\Exception\GuzzleException
 */
function getSizes()
{
    $urlSizes = env('GET_BASE_URL') . "/fashion/size/getsizes";
    $sizeRes = designerConsume($urlSizes, 'GET');
    if (isset($sizeRes['data'])) {
        return $sizes = $sizeRes['data']->data;
    } else {
        return $sizes = [];
    }
}

/**
 * General Function
 */
function getLastSixMonths()
{
    $first = strtotime('first day this month');
    $months = array();
    for ($i = 6; $i >= 1; $i--) {
        array_push($months, date('Y-m', strtotime("-$i month", $first)));
    }
    return $data = ['months' => $months];
}

/**
 * General Function
 * @param $dataArray
 * @param \Illuminate\Http\Request $request
 * @return array|\Illuminate\Pagination\LengthAwarePaginator
 */
function pagePagination($dataArray, \Illuminate\Http\Request $request)
{
    $totalOrder = [];

    $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage();

    $collection = new \Illuminate\Support\Collection($dataArray);

    $per_page = 20;

    $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();

    $totalOrder = new \Illuminate\Pagination\LengthAwarePaginator($currentPageResult, count($collection), $per_page);

    $totalOrder->setPath($request->url());

    return $totalOrder;

}

/**
 * General Function
 * function to perform discount percentage calculation
 * @param $disPrice
 * @param $originalPrice
 * @return float
 */
function getDiscountPercent($disPrice, $originalPrice)
{
    $dis = $disPrice / $originalPrice;
    $disInPer = ($dis) * 100;
    return round($disInPer);
}

/**
 * General Function
 * Get the new price from discount price
 * @param $originalPrice
 * @param $discPrice
 * @return string
 */
function getNewPrice($originalPrice, $discPrice)
{
    $new = $originalPrice - $discPrice;
    return number_format($new);
}

/**
 * General Function
 * Get the new price from discount price
 * @param $qty
 * @param $price
 * @return string
 */
function getQtyPrice($price, $qty)
{
    $new = $price / $qty;
    return number_format($new);
}

/**
 * General Function
 * Generate a random string, combination of strings
 * @param $length
 * @return string
 */
function invoiceRefID($length)
{
    $chars = array_merge(range('A', 'Z'), range(0, 9));
    shuffle($chars);
    if ($length > count($chars)) $length = count($chars);
    return implode(array_slice($chars, 0, $length));
}

/**
 * @param $result
 * @return string|string[]
 */
function replaceQuote($result)
{
    $pattern = '/(?<!:|: )"(?=[^"]*?"(( [^:])|([,}])))/';
    $string = preg_replace($pattern, '\\"', $result);
    return $string;
}

/**
 * @param $value
 * @return mixed
 */
function escapeJsonString($value)
{
    # list from www.json.org: (\b backspace, \f formfeed)
    $pattern = '/"((?:.|\n)*?)"\s*[:,}]\s*/';
    preg_match_all($pattern, $value, $matches);
    foreach ($matches[1] as $match) {
        $ans[] = str_replace('"', '\\"', $match);
    }
    print_r($ans);
}



function generateCorrectUrl($url)
{
    $string = preg_replace("/^(\d{1,2}[^0-9])/", "-", $url);
    return $string;
}

/**
 * Generate a unique slug from a string
 * @param string $string The string to slug
 * @return bool|string
 */
function generateSlug($string)
{

    $string = str_replace("\n","<br />",$string);
    $string = str_replace('\n',"<br />",$string);
    $string = stripslashes($string);
    $string = preg_replace('/^(\d{1,2}[^0-9])/','-', iconv('UTF-8','ASCII//TRANSLIT',$string));
    $string = trim($string,'-');
    $string = strtolower($string);
    $string = str_replace('%27','\'', $string);
    return $string;
}
/*function isMobile()
{
    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(tablet|ipad|amazon|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($useragent))) {
            return true;
        };

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return true;
        }
    }
    return 0;
}

if (isMobile()) {
    return view('mobile.index');

} else {
//    die('here');
    return view('view');
}*/