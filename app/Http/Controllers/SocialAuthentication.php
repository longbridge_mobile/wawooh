<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Socialite;

class SocialAuthentication extends Controller
{
    //
    /**
     * Redirect the user to the GitHub authentication page.
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function redirectToProviderForRegistration()
    {

        return Socialite::driver('facebook')->redirectUrl(route('facebookRegisterCallback'))->redirect();
    }

    /**
     * Obtain the user information from GitHub
     * @return \Illuminate\Http\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        $email = $user->getEmail();
        $password = $user->token;
        $data = ['email' => $email,
            'password' => $password,
            'socialFlag' => 'Y',
        ];
        $res = login($data);
        if ($res['status'] == 0) {
            return back();
        } else {
            return displayError($res);
        }

    }

    public function handleProviderCallbackForRegistration()
    {
        //get last url
        $previous = url()->previous();
        //check if its facebook
        if (strpos($previous, 'facebook') !== false) {
            return redirect(route('fashionStore'));
        }
        $user = Socialite::driver('facebook')->stateless()->user();
        $email = $user->getEmail();//get user email
        $name = explode(' ', $user->getName());//get name of the user
        $firstName = $name[0];
        $lastName = $name[1];
        return view('user.register', compact('firstName', 'lastName', 'email'));

    }
}
