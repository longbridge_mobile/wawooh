<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminExportController extends Controller
{
    /**
     * Create an export file for designers
     * and download as a specific file
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportDesigners()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/designer/getdesigners";
            $res = consumeWithoutToken($url, 'GET');
            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designers = $res['data'];
                } else {
                    return $redirectView;
                }
                return view('admin.exports.designers-export', compact('designers'));
            } else {
                return redirect(route('admin.login'));
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportTransferInfo()
    {
        $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . "getalltransferinfo";
        $res = adminConsume($url, 'GET');
        if ($res['status'] == 0) {
            $transferInfo = $res['data'];
            //dd($transferInfo);
            return view('admin.exports.export-transfer-info', compact('transferInfo'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportUsers(Request $request)
    {
        //get all users
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "getusers";
            $users = adminConsume($url, 'GET');
            return view('admin.exports.users-export', compact('users'));
        }

        return redirect(route('admin.login'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportAllProducts()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_PRODUCT') . "getproducts";
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $res = adminConsume($url, 'POST', $data);
            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $allProducts = $res['data'];
                } else {
                    return $redirectView;
                }
                return view('admin.exports.products-export', compact('allProducts'));
            }
        } else {
            return redirect(route('admin.login'));
        }
    }
}