<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AdminProductController extends Controller
{
    /**
     * Get all Products (both verify and unverify)
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function products(Request $request)
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_PRODUCT') . "getproducts";
            $verify = env('GET_BASE_URL') . env('ADMIN_PRODUCT') . "getverifiedproducts";
            $unverify = env('GET_BASE_URL') . env('ADMIN_PRODUCT') . "getunverifiedproducts";

            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];

            $res = adminConsume($url, 'POST', $data);
            $unverifyRes = adminConsume($unverify, 'POST', $data);
            $verifyRes = adminConsume($verify, 'POST', $data);

            if ($res['status'] == 0 || $verifyRes['status'] == 0 || $unverifyRes['status'] == 0) {
                $products = $res['data'];
                $verifyProducts = $verifyRes['data'];
                $unverifyProducts = $unverifyRes['data'];

                $totalOrder = [];

                $currentPage = LengthAwarePaginator::resolveCurrentPage();

                $collection = new Collection($products);

                $per_page = 20;

                $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();

                $totalOrder = new LengthAwarePaginator($currentPageResult, count($collection), $per_page);

                $totalOrder->setPath($request->url());

                return view('admin.products.products', compact('products', 'unverifyProducts', 'verifyProducts', 'totalOrder'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * RETURN ALL ABANDON CARTS
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function abandonCart()
    {
        if (session()->has('adminToken')) {
            $redirectView = view('errors.noNetwork');
            $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . "get_abandoned_cart";
            $data = ['page' => '0', 'size' => '100'];
            $data = ['json' => $data];
            $resp = adminConsume($url, 'POST', $data);
//            dd($resp);
            if ($resp['status'] === '00') {
                $res = $resp['data'];
                return view('admin.orders.abandonCart', compact('res'));
            }
            return view('admin.orders.abandonCart');
//            return $redirectView;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productDetails(Request $request)
    {
        if (session()->has('adminToken')) {
            $redirectView = view('errors.noNetwork');
            $id = $request->route('pId');
            $currency = env('CURRENCY');
            $url = env('GET_BASE_URL') . "/fashion/product/$id/$currency/getproductbyid";
            $resp = adminConsume($url, 'GET');
            if ($resp['status'] == '00') {
                $newresp = $resp['data'];
//                dd($newresp);
                if ($newresp->bespokeProductDTO !== null) {
                    $reqMes = json_decode(json_decode($newresp->bespokeProductDTO->mandatoryMeasurements));
                }
                return view('admin.products.productDetails', compact('newresp', 'reqMes'));
            } else {
                return $redirectView;
            }
        } else {
            return redirect(route('admin.login'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeProductDetail(Request $request)
    {
        if (session()->has('adminToken')) {
            $redirectView = view('errors.noNetwork');
            $id = $request->route('pId');
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/product/${id}/NGN/get_one_styles";
            $data = ['init' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $resp = adminConsume($url, 'POST', $data);
            $bespokeProductDetail = [];

            if ($resp['status'] === '00') {
                $bespokeProductDetail = $resp['data'];
             //   dd($bespokeProductDetail);

                return view('admin.products.bespokeProductDetails', compact('bespokeProductDetail'));

            } else {
                return view('admin.products.bespokeProductDetails', compact('bespokeProductDetail'));
            }
        }
    }


    public function approveUserUploadStyle() {
        if (session()->has('adminToken')) {
             $url = env('GET_BASE_URL').'/fashion/secure/admin/product/admin_approve_user_uploaded_style';
            $data = ['init' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $resp = adminConsume($url, 'POST', $data);
            dd($resp);
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sponsor()
    {
        $id = request('id');
        $status = request('status');
        $url = env('GET_BASE_URL') . "/fashion/product/$id/sponsor/$status";
        return $resp = adminConsume($url, 'GET');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productsVerify()
    {
        $id = request('id');
        $status = request('status');
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/product/$id/verifyproduct/$status";

        return $resp = adminConsume($url, 'GET');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDesignerProducts()
    {
        $designerId = request('id');
        //get products by designer id.
        $url = env('GET_BASE_URL') . "/fashion/product/$designerId/getdesignerproducts";
        return $res = consumeWithoutToken($url, 'GET');
    }
}
