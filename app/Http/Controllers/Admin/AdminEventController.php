<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AdminEventController extends Controller
{
    /**
     * Add events
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function addEvents()
    {
        if (session()->has('adminToken')) {
            return view('admin.events.create');
        }
        return redirect(route('admin.login'));

    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createEvents()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/createevent";
        return $res = adminConsume($url, 'POST', $data);//consume api
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function events()
    {
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/event/getevents";
            $data = ['page' => '0', 'size' => '10', 'eventType' => 'A'];
            $data = ['json' => $data];
            $events = adminConsume($url, 'POST', $data);
           // dd($events);
    
            if ($events['status'] == 0) {
                $events = $events['data']->events;
                return view('admin.events.events', compact('events'));
            } else {
                return view('errors.noNetwork');
//                return displayError($events);
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function preview($event)
    {
        if (session()->has('adminToken')) {
            //$event = request('event');
            $url = env('GET_BASE_URL') . "/fashion/event/$event/geteventbyId";
           // dd($url);
            $eventPictures = adminConsume($url, 'GET');
            //dd($eventPictures);

            //fashion/product/id/gettagged
            $url = env('GET_BASE_URL') . "/fashion/product/$event/gettagged";
            $TaggedPictures = adminConsume($url, 'GET');
            if ($TaggedPictures['status'] == 0) {
                $TaggedPictures = $TaggedPictures['data'];
            }
            //fashion/product/id/getuntagged
            $url = env('GET_BASE_URL') . "/fashion/product/$event/getuntagged";
            $unTaggedPictures = adminConsume($url, 'GET');
            if ($unTaggedPictures['status'] == 0) {
                $unTaggedPictures = $unTaggedPictures['data'];
            }

            if ($eventPictures['status'] == 00) {
                $eventPictures = $eventPictures['data']->event;
            // dd($eventPictures);
                return view('admin.events.events-preview', compact('eventPictures', 'TaggedPictures', 'unTaggedPictures'));
            } else {
                return $eventPictures;
            }
        }

        return redirect(route('admin.login'));

    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteEvent()
    {
        if (session()->has('adminToken')) {
            $id = request('id');
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/{$id}/delete";
            $response = adminConsume($url, 'GET');
           // dd($response);
            if ($response['status'] == 0) {
                return back();
            } else {
                return displayError($response, 'admin.events.events');
            }
        }
        return redirect(route('admin.login'));


    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function eventsApi()
    {
        if (session()->has('adminToken')) {
            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body];//request parameter
            $url = env('GET_BASE_URL') . "/fashion/event/getevents";

            return $events = adminConsume($url, 'POST', $data);
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateEventsInfo()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/updateevent";

        return $res = adminConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteEventsPicture()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/deleteeventpictures";

        return $res = adminConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateEventPicture()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/updateeventpictures";

        return $res = adminConsume($url, 'POST', $data);
    }
}
