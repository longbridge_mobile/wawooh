<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AdminController extends Controller
{
    //
    public function __construct()
    {

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dashboard()
    {
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/getdashboarddata";
            $res = adminConsume($url, 'GET');
            $url = env('GET_BASE_URL') . "/fashion/designer/getdesigners";
            $resDes = consumeWithoutToken($url, 'GET');
            $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "getusers";
            $users = adminConsume($url, 'GET');
            $urlAll = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "getallusers";
            $usersAll = adminConsume($urlAll, 'GET');
            $url = env('GET_BASE_URL') . env('USER_ACCOUNT_API') . "newsletter/getall";
            $result = adminConsume($url, 'GET');
            if ($res['status'] == '00' || $resDes['status'] == '00' || $result['status'] == '00') {
                if (isset($res['data']) || isset($resDes['data']) || isset($result['data'])) {
                    $resp2 = $res['data'];
                    $emailSub = count($result['data']);
//                    $designerCount = count($resDes['data']);
                    $designerCount = '0';
                    $userCount = count($users);
                    $allUserCount = count($usersAll);
                    return view('admin.dashboard', compact('resp2', 'designerCount', 'userCount', 'allUserCount', 'emailSub'));
                } else {
                    return view('admin.dashboard');
                }
            }
            return view('admin.dashboard');
        }
        return redirect(route('admin.login'));
    }

    public function userDetails()
    {
        return view('admin.users.userdetails');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function role()
    {
        $customer = 'customer_service';
        $qa = 'qa';
        $auditor = 'auditor';
        $urlForCustomer = env('GET_BASE_URL') . "/fashion/secure/admin/$customer/getusers";
        $urlForQa = env('GET_BASE_URL') . "/fashion/secure/admin/$qa/getusers";
        $urlForAuditor = env('GET_BASE_URL') . "/fashion/secure/admin/$auditor/getusers";
        $dataForCustomer = adminConsume($urlForCustomer, 'GET');
//        dd($dataForCustomer);
        $dataForQa = adminConsume($urlForQa, 'GET');
        $dataForAuditor = adminConsume($urlForAuditor, 'GET');
        return view('admin.settings.role', compact('dataForCustomer', 'dataForQa', 'dataForAuditor'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTagDetails()
    {
        $id = request('id');
        $url = env('GET_BASE_URL') . "/fashion/product/$id/gettag";
        return $unTaggedPictures = adminConsume($url, 'GET');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteTag()
    {
        $id = request('id');
        $url = env('GET_BASE_URL') . "/fashion/product/$id/deletetag";
        return $unTaggedPictures = adminConsume($url, 'GET');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function designers(Request $request)
    {
        //get all designers
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/designer/getdesigners";
            $res = consumeWithoutToken($url, 'GET');
            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designers = $res['data'];
//                    dd($designers);
                } else {
                    return $redirectView;
                }
                $totalDesigner = [];
//                $currentPage = LengthAwarePaginator::resolveCurrentPage();
//
//                $collection = new Collection($designers);
//
//                $per_page = 20;
//
//                $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();
//
//                $totalDesigner = new LengthAwarePaginator($currentPageResult, count($collection), $per_page);
//
//                $totalDesigner->setPath($request->url());
                // dd($totalDesigner);
                return view('admin.users.designers', compact('designers'));
            } else {
                return $redirectView;
            }
        }

        return redirect(route('admin.login'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function viewDesigner()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            // $designer = str_replace('-',' ',request('designerName'));
            $id = request('designerName');
            $url = env('GET_BASE_URL') . "/fashion/designer/$id/getdesignerbyid";
            $res = consumeWithoutToken($url, 'GET');
            if ($res['status'] == 00) {
                $designer = $res['data'];
                return view('admin.users.view-designer', compact('designer'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changeDesignerStatus()
    {
        $id = request('id');
        $status = request('status');
        $url = env('GET_BASE_URL') . "/fashion/designer/$id/$status/changestatus";
        return $res = consumeWithoutToken($url, 'GET');
    }

    /**
     * Get the categories in Admin
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function category()
    {
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getcategories";
            $res = adminConsume($url, 'GET');
            $categories = $res['data'];
            return view('admin.category.category', compact('categories'));
        }
        return redirect(route('admin.login'));
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeApplication(Request $request)
    {
        //get all designers
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "getbespokeapplications";
            $res = adminConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $bespokeApp = $res['data'];
                } else {
                    return $redirectView;
                }
                return view('admin.bespoke.bespoke-application', compact('bespokeApp'));
            } else {
                return $redirectView;
            }
        }

        return redirect(route('admin.login'));
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeApplicationDetails(Request $request)
    {
        //get all designers
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
//            $besId = request('besId');
            $besId = $request->route('baId');
            $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "{$besId}/getbespokeapplication";
            $res = adminConsume($url, 'GET');

            if ($res['status'] === "00") {
                if (isset($res['data'])) {
                    $bespokeApp = $res['data'];
                } else {
                    return $redirectView;
                }
                return view('admin.bespoke.bespoke-detail', compact('bespokeApp'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeRequests(Request $request)
    {
        //get all designers
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_BESPOKE') . "view";
            $res = adminConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $bespokeRequests = $res['data'];
//                    dd($bespokeRequests);
                } else {
                    return $redirectView;
                }
                return view('admin.bespoke.bespoke-requests', compact('bespokeRequests'));
            } else {
//                return $redirectView;
                return view('admin.bespoke.bespoke-requests', compact('bespokeRequests'));
            }
        }

        return redirect(route('admin.login'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function adminBespokeProduct()
    {
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/product/NGN/get_all_bespoke_styles";
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];

            $res = adminConsume($url, 'POST', $data);
            if ($res['status'] === '00') {
                $bespokeProducts = $res['data'];

                return view('admin.bespoke.bespoke-products', compact('bespokeProducts'));
            } else {
                return view('errors.noNetwork');
            }
        }
    }

    public function adminUserUploadedStyle() {
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL'). "/fashion/secure/admin/product/get_all_user_uploaded_styles";
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $res = adminConsume($url, 'POST', $data);
           // dd($res);
            if ($res['status'] === '00') {
                $uploadedStyles = $res['data'];
                return view('admin.bespoke.uploaded-style', compact('uploadedStyles'));
            }
        }
    }

    public function adminUserUploadedStyleDetail(Request $request) {
        if (session()->has('adminToken')) {
            $styleId = $request->id;
            $url = env('GET_BASE_URL'). "/fashion/secure/admin/product/{$styleId}/get_one_user_uploaded_styles";
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $res = adminConsume($url, 'POST', $data);
            if ($res['status'] === '00') {
                $uploadedSingleStyle = $res['data'];
               // dd($uploadedSingleStyle);
                return view('admin.products.uploadProductDetails', compact('uploadedSingleStyle'));
            }

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokePerRequest(Request $request)
    {
        //get all designers
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $reqId = $request->route('bId');
            $url = env('GET_BASE_URL') . env('ADMIN_BESPOKE') . "{$reqId}/view";
            $res = adminConsume($url, 'GET');
            $categories = getCategories();
            if ($res['status'] === "00") {
                if (isset($res['data'])) {
                    $preview = $res['data'];
//                    dd($preview);
                    $requestMeasurement = $preview->measurement;
                } else {
                    return $redirectView;
                }
                return view('admin.bespoke.view-bespoke-requests', compact('preview', 'requestMeasurement', 'categories'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function subCategoryByCatId()
    {
        if (session()->has('adminToken')) {
            $body = json_decode(request()->getContent(), true);
            $body = $body['id'];
            $url = env('GET_BASE_URL') . "/fashion/product/$body/getsubcategories";
            return adminConsume($url, 'GET');
        }
        /*$categories = getCategories();
        $cat = request('cat');
        if (isset($cat)) {
            $url = env('GET_BASE_URL') . "/fashion/product/$cat/getsubcategories";
            $subcategories = adminConsume($url, 'GET');
            if ($subcategories['status'] == 0) {
                $subcategories = $subcategories['data'];
                return view('admin.category.subcategory', compact('categories', 'subcategories'));

            } else {
                return $subcategories;
            }
        } else {
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/category/getallsubcategories";
            $subcategories = adminConsume($url, 'GET');
            if ($subcategories['status'] == 0) {
                $subcategories = $subcategories['data'];
//                    dd($subcategories);
                return view('admin.subcategory', compact('categories', 'subcategories'));
            } else {
                return $subcategories;
            }
        }
    }
    return redirect(route('admin.login'));*/
    }

    /**
     * Confirm the status of Bespoke Application
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function confirmBespokeApplication()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . 'updatebespokerequest';
        return $res = adminConsume($url, 'POST', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function users(Request $request)
    {
        //get all users
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "getusers";
            $users = adminConsume($url, 'GET');
            return view('admin.users.users', compact('users'));
        }
        return redirect(route('admin.login'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allUsers(Request $request)
    {
        //get all users
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_GENERAL') . "getallusers";
            $users = adminConsume($url, 'GET');
            return view('admin.users.allusers', compact('users'));
        }
        return redirect(route('admin.login'));
    }


    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function subCategory()
    {
        if (session()->has('adminToken')) {
            $categories = getCategories();
            $cat = request('cat');
            if (isset($cat)) {
                $url = env('GET_BASE_URL') . "/fashion/product/$cat/getsubcategories";
                $subcategories = adminConsume($url, 'GET');
                if ($subcategories['status'] == 0) {
                    $subcategories = $subcategories['data'];
                    return view('admin.category.subcategory', compact('categories', 'subcategories'));

                } else {
                    return $subcategories;
                }
            } else {
                $url = env('GET_BASE_URL') . "/fashion/secure/admin/category/getallsubcategories";
                $subcategories = adminConsume($url, 'GET');
                if ($subcategories['status'] == 0) {
                    $subcategories = $subcategories['data'];
//                    dd($subcategories);
                    return view('admin.subcategory', compact('categories', 'subcategories'));
                } else {
                    return $subcategories;
                }
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function categorystyles()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            if (is_numeric(request('subcat'))) {
                $id = request('subcat');
                $url = env('GET_BASE_URL') . "/fashion/product/$id/getstyles";//api
                $res = adminConsume($url, 'GET');
                if ($res['status'] == 0) {
                    $styles = $res['data'];
                    return view('admin.category.styles', compact('styles'));
                } else {
                    return $redirectView;
                }
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function tags(Request $request)
    {
        $redirectView = view('errors.noNetwork');
        //get all untagged
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/product/getuntagged";
            $taggedUrl = env('GET_BASE_URL') . "/fashion/product/gettagged";
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];

            $res = adminConsume($url, 'POST', $data);
            $taggedData = ['page' => '0', 'size' => '10'];
            $taggedData = ['json' => $taggedData];

            $taggedRes = adminConsume($taggedUrl, 'POST', $taggedData);

            if ($res['status'] == 0 || $taggedRes['status'] == 0) {
                $unTaggedPictures = $res['data'];
                $taggedPicture = $taggedRes['data'];
                $unTaggedPictures = pagePagination($unTaggedPictures, $request);

                $taggedPictures = pagePagination($taggedPicture, $request);
                // dd($taggedPicture);


                return view('admin.tags', compact('unTaggedPictures', 'taggedPictures'));
            } else {
                return $redirectView;
            }
        }

        return redirect(route('admin.login'));
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function tagsImages()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $id = request('images');

            $url = env('GET_BASE_URL') . "/fashion/event/$id/geteventpicturebyid";
            $res = adminConsume($url, 'GET');
            if ($res['status'] == 0) {
                $unTaggedPicture = $res['data']->eventpicture;
                $categories = getCategories();
                $designers = getAllDesigners();
                return view('admin.tags-images', compact('unTaggedPicture', 'categories', 'designers'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editTags()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $id = request('images');

            $url = env('GET_BASE_URL') . "/fashion/product/$id/gettags";
            $res = adminConsume($url, 'GET');
            if ($res['status'] == 0) {
                $TaggedPicture = $res['data']->picture;
                $categories = getCategories();
                $designers = getAllDesigners();
                $tags = $res['data']->tags;

                return view('admin.edittags', compact('TaggedPicture', 'tags', 'categories', 'designers'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));

    }

    public function adminLogin()
    {
        if (!session()->has('adminToken')) {
            return view('admin.index');
        } else
            return redirect(route('admin.dashboard'));
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/signin";//api

        $res = consumeWithoutToken($url, 'POST', $data);//consume api
        if ($res['status'] == 0) {

            if ($res['data']->role == 3) {
                $token = json_encode($res['data']->token);//get token
                session(['adminToken' => $token]);
            } elseif ($res['data']->role == 5) {
                $token = json_encode($res['data']->token);
                session(['qaToken' => $token]);
            } elseif ($res['data']->role == 6) {
                $token = json_encode($res['data']->token);
                session(['customertoken' => $token]);
            } elseif ($res['data']->role == 7) {
                $token = json_encode($res['data']->token);
                session(['auditortoken' => $token]);
            }

            return $res2 = collect($res)->toJson();//return response 
        } else {
            return collect(array('status' => $res['status'], 'message' => $res['message']))->toJson();
        }

        //set auth session
    }

    public function shippingDoc()
    {
        return view('admin.shipping');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function transferInfo()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/order/getalltransferinfo";
        $res = adminConsume($url, 'GET');
        if ($res['status'] == 0) {
            $transferInfo = $res['data'];
            return view('admin.transfer-info', compact('transferInfo'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allRefunds()
    {
        $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . "getrefundinfo";
        $res = adminConsume($url, 'GET');

        if ($res['status'] == 0) {
            $allRefunds = $res['data'];
            return view('admin.refunds', compact('allRefunds'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addInspireProducts()
    {
        return view('admin.add-inspire');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function viewAllInspireProducts()
    {
        return view('admin.view-inspire');
    }

    public function changeAdminPassword()
    {

        return view('admin.change-password');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changePassword()
    {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('USER_ACCOUNT_API') . "/editpassword";
        $res = adminConsume($url, 'POST', $data);


        if ($res['status'] == '00') {
            session()->forget('adminToken');
            return response()->json(['status' => '00', 'message' => 'Password Change Successfully']);
        } else {
            return response()->json(['status' => '99', 'message' => 'Not successfully']);
        }

    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function adminDeactiveDesigner()
    {
        $body = json_decode(request()->getContent(), true);

        // $data = ['json' => $body];
        $designerId = $body['id'];
        $status = $body['status'];

        $url = env('GET_BASE_URL') . "/fashion/secure/admin/" . $designerId . "/" . $status . "/update";

        $res = adminConsume($url, 'GET');
        return $res;

        //  return $data;
    }

    /**
     * Email Subscriber list
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function emailSubscribers()
    {
        $redirectView = view('errors.noNetwork');
        $url = env('GET_BASE_URL') . env('USER_ACCOUNT_API') . "newsletter/getall";
        $result = adminConsume($url, 'GET');

        if ($result['status'] == '00') {
            $subscribers = $result['data'];
            return view('admin.subscribers', compact('subscribers'));
        } else {
            return $redirectView;
        }
    }

    /**
     * Email Subscriber list
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userFeedback()
    {
        $redirectView = view('errors.noNetwork');
        $url = env('GET_BASE_URL') . env('USER_FEEDBACK') . "getall";
        $result = adminConsume($url, 'GET');

        if ($result['status'] == '00') {
            $feedbacks = $result['data'];
            return view('admin.users.user-feedback', compact('feedbacks'));
        } else {
            return $redirectView;
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createAdminRole()
    {
        $body = json_decode(request()->getContent(), true);
        $body = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/adduser";
        $result = adminConsume($url, 'POST', $body);
        return $result;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function imageBankForAdmin()
    {

        $url = env('GET_BASE_URL') . "/fashion/secure/admin/bespoke/styles/view";

        $data = ['page' => '0', 'size' => '10'];
        $data = ['json' => $data];
        $result = adminConsume($url, 'POST', $data);

        if ($result['status'] == '00') {
            $resp = $result['data'];
            //dd($resp);
            return view('admin.designer-bank', compact('resp'));
        } else {
            return view('admin.designer-bank', compact('error'));
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function verifyDesignerImage()
    {
        $body = json_decode(request()->getContent(), true);
        $bespokeStyleId = $body['bespokeStyleId'];
        $verified = $body['verified'];
        $data = ['bespokeStyleId' => $bespokeStyleId, 'verified' => $verified ? false : true];
        $data = ['json' => $data];
        $url = env('GET_BASE_URL') . '/fashion/secure/admin/bespoke/styles/updatebespokestyle';
        $result = adminConsume($url, 'POST', $data);
        return $result;
    }

//    public function verifyDesignerImage()
//    {
//        $body = json_decode(request()->getContent(), true);
//        //  $data = ['json' => $body];//request parameter
//        $bespokeStyleId = $body['bespokeStyleId'];
//
//        $url = env('GET_BASE_URL') . "/fashion/secure/admin/bespoke/styles/${bespokeStyleId}/verifybespokestyle";
//
//        $data = ['page' => '0', 'size' => '10'];
//        $data = ['json' => $data];
//        $url = env('GET_BASE_URL') . '/fashion/secure/admin/bespoke/styles/updatepatent';
//        $result = adminConsume($url, 'POST', $data);
//        return $result;
//    }

}
