<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Route;


class AdminOrderController extends Controller
{
    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allOrders(Request $request)
    {
        //get all orders
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . "getorders";
            $incomplereUrl = env('GET_BASE_URL') . env('ADMIN_ORDER') . "getincompleteorders";
            $res = adminConsume($url, 'GET');

            $incompleteRes = adminConsume($incomplereUrl, "GET");
            if ($res['status'] == 0 || $incompleteRes['status'] == 0) {
                $orders = $res['data'];

                $incompleteOrder = $incompleteRes['data'];
                // dd($incompleteOrder);
                return view('admin.orders.all', compact('orders', 'incompleteOrder'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    public function allBespokeOrders(){
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {

            $url = env('GET_BASE_URL')."/fashion/secure/admin/bespoke/order/get_all_bespoke_request_orders";

            $data = [
                "page" => 0,
                "size" => 10
            ];

            $data = ['json' => $data];


            $res = adminConsume($url, 'POST', $data);
           // dd($res);

            $orders = [];

            if($res['status'] === '00') {
                $orders = $res['data'];

                return view('admin.orders.allBespokeOrder', compact('orders'));
            } else {
                return view('admin.orders.allBespokeOrder', compact('orders'));
            }

        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function activeOrders()
    {
        //get active orders
        if (session()->has('adminToken')) {
            return view('admin.orders.active');
        }
        return redirect(route('admin.login'));
    }

    public function completedOrders()
    {
        //get completed orders
        if (session()->has('adminToken')) {
            return view('admin.orders.completed');
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function acceptOrder()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/order/admin/updateorderitem";
        return $res = adminConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateOrder()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/order/admin/updateorderitem";
        return $res = adminConsume($url, 'POST', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function viewOrderDetails()
    {
        if (session()->has('adminToken')) {
            $id = request('order');
            $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . "{$id}/getorder";
            $res = adminConsume($url, 'GET');
            if ($res['status'] == 0) {
                $order = $res['data'];
//                dd($order);
                return view('admin.orders.items', compact('order'));
            }
        } else {
            return redirect(route('admin.login'));
        }
    }


    public function viewBespokeOrderDetails(){
        if (session()->has('adminToken')) {
            $orderNum = request('orderNum');
            $url = env('GET_BASE_URL'). "/fashion/secure/admin/bespoke/order/${orderNum}/get_one_bespoke_request_orders";
            $data = ['page' => '0', 'size' => '100'];
            $data = ['json' => $data];
            $res = adminConsume($url, 'POST', $data);

            //dd('ffjf');


            if($res['status'] === '00'){
               $order = $res['data'];

               return view('admin.orders.bespokeItems', compact('order'));

            }


        }
    }



    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function confirmPayments()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . "updateorder";
        return $res = adminConsume($url, 'POST', $data);
    }

    public function confirmBespokePayment(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL'). "/fashion/secure/admin/bespoke/order/payment_for_bespoke_confirmed_2";
        return $res = adminConsume($url, 'POST', $data);

    }


    public function confirmTicketPayment(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL'). "/fashion/secure/admin/event/update_bank_transfer";
        return $res = adminConsume($url, 'POST', $data);
    }

    public function collectTicket() {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL')."/fashion/secure/admin/event/confirm_ticket_collection";
        return $res = adminConsume($url, 'POST', $data);
    }

    public function searchTicket(Request $request) {
        if (session()->has('adminToken')) {
            $orderNum = $request->input('search');
            $url = env('GET_BASE_URL'). "/fashion/secure/admin/event/{$orderNum}/confirm_ticket";
            $resp = adminConsume($url, 'GET');
            if ($resp['status'] === '200') {
                $res = $resp['data'];
                return view('admin.ticket.order', compact('res'));
            } else {
                return view('admin.ticket.order', compact('res'));

            }
            }

        }

    public function ticketOrder(){
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . '/fashion/secure/admin/event/view_all_ticket_orders';
            $data = ['init' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $resp = adminConsume($url, 'POST', $data);
            $res = [];
            if ($resp['status'] === '200') {
                $res = $resp['data'];
                return view('admin.ticket.order', compact('res'));
            } else {
                return view('admin.ticket.order', compact('res'));

            }
        }
    }

    public function eventTicket(){
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . '/fashion/secure/admin/event/view_all_tickets';
            $data = ['init' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $resp = adminConsume($url, 'POST', $data);
            if ($resp['status'] === '200') {
                $res = $resp['data'];
                return view('admin.ticket.event', compact('res'));
            }

        }
    }

    public function updateTicket(){
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/update_ticket";
            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body];
            return adminConsume($url, 'POST', $data);
        }
    }

    public function deleteTicket(){
        if (session()->has('adminToken')) {
           $data =  json_decode(request()->getContent(), true);
           $ticketId = $data['ticketId'];
           $url = env('GET_BASE_URL'). "/fashion/secure/admin/event/${ticketId}/delete_ticket";
           return adminConsume($url, 'GET');
        }
    }

    public function getTicket(){
        if (session()->has('adminToken')) {
            $data =  json_decode(request()->getContent(), true);
            $ticketId = $data['ticketId'];
            $url = env('GET_BASE_URL')."/fashion/secure/admin/event/${ticketId}/view_one_ticket";
            return adminConsume($url, 'GET');
        }
    }

    public function ticketCreate() {
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/event/create_ticket";
            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body];
            return adminConsume($url, 'POST', $data);
        }
    }

    /**
     * Display the full list of all orders for exporting
     * to excel
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportAllOrders()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/admin/order/getorders";
            $res = adminConsume($url, 'GET');
            if ($res['status'] == 0) {
                $orders = $res['data'];
//                dd($orders);
                return view('admin.orders.order-export', compact('orders'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * PROMO CODES Management
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function promoCodeManagement()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $url = env('GET_BASE_URL') . "/fashion/product/getcategories";
            $res = adminConsume($url, 'GET');

            $urlPromo = env('GET_BASE_URL') . "/fashion/secure/admin/promocode/getunexpiredpromocode";
            $resPromo = adminConsume($urlPromo, 'GET');
            // dd($resPromo);

            if ($res['status'] == 0 || $resPromo['status'] == 0) {

                $orders = isset($res['data']) ? $res['data']: [];
                $allPromoCode = $resPromo['data'];
//                dd($allPromoCode);
                return view('admin.promo-code', compact('orders', 'allPromoCode'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addPromoCode()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/promocode/addpromocode";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        return adminConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function applyPromoCode()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/applyPromoCode";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        return Consume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generatePromoCode()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/promocode/generatepromocode";
        return adminConsume($url, "GET");
    }


    /**
     * @param Request $request
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPromoCodeById(Request $request)
    {
        $id = $request->id;
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/promocode/getpromocode/{$id}";

        return adminConsume($url, 'GET');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function verifyPromoCode()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/promocode/verifypromocode";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];

        return adminConsume($url, 'POST', $data);
    }


    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancelOrder()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/order/cancelorder";

        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];

        return adminConsume($url, 'POST', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllDeliveredOrders() {
        $redirectView = view('errors.noNetwork');
        if (session()->has('adminToken')) {
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $url = env('GET_BASE_URL') . env('ADMIN_ORDER') ."get_all_delivered_orders";
            $res = adminConsume($url, 'POST', $data);
//            dd($res);
            if ($res['status'] == 200) {
                $orders = $res['data'];
//                dd($orders);
                return view('admin.orders.delivered-order', compact('orders'));
            } else {
                return $redirectView;
            }
        }
        return redirect(route('admin.login'));

    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateInvoice(Request $request)
    {

        if (session()->has('adminToken')) {
            $orderNum = $request->route('orderNum');
            $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . $orderNum . "/generateinvoice";
            $res = adminConsume($url, 'GET');
            if ($res['status'] == 0) {
                $invoice = $res['data'];
                return view('admin.invoice.invoice', compact('invoice'));
            }
        } else {
            return view('admin.login');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateDeliveryNote(Request $request)
    {

        if (session()->has('adminToken')) {
            $orderNum = $request->route('orderNum');
            $url = env('GET_BASE_URL') . env('ADMIN_ORDER') . $orderNum . "/generateinvoice";
            $res = adminConsume($url, 'GET');
            if ($res['status'] == 0) {
                $invoice = $res['data'];
                return view('admin.invoice.delivery-note', compact('invoice'));
            }
        } else {
            return view('admin.login');
        }
    }
}
