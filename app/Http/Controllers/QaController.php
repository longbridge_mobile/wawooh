<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class QaController extends Controller
{
    //
    public function __construct()
    {

    }

    /**
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dashboard(Request $request)
    {

        if (session()->has('qaToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/qa/order/getdashboarddata";
            $res = qaConsume($url, 'GET');
//            dd($res);
            if ($res['status'] == '00') {
                $resp2 = $res['data'];
                return view('qa.dashboard', compact('resp2'));
            } else
            {
                return view('qa.dashboard');
            }
        }
    }

    /**
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allOrders(Request $request)
    {
        if (session()->has('qaToken')) {
            $url = env('GET_BASE_URL') . env('QA_ORDER') . "getorders";
            $res = qaConsume($url, 'GET');
           /// dd($res);
         //  dd($url);
            if ($res['status'] == 0) {
                $allOrder = $res['data'];
                // dd($allOrder);
                $totalOrder = pagePagination($allOrder, $request);
                //dd($totalOrder);
                return view('qa.orders.all', compact('totalOrder'));
            }
        } else {
            return redirect(route('admin.login'));
        }

    }

    public function allBespokeOrders(Request $request)
    {
        if (session()->has('qaToken')) {
            $url = env('GET_BASE_URL'). env('QA_ORDER'). "get_all_bespoke_request_orders";
            $data = ['page' => '0', 'size' => '20'];
            $data = ['json' => $data];

            $res = qaConsume($url, 'POST', $data);
            if($res['status'] === '00') {
                $allBespokeOrder = $res['data'];
                $totalOrder = pagePagination($allBespokeOrder, $request);
              //  dd($totalOrder);

                return view('qa.orders.bespoke-all', compact('totalOrder'));
            }

        }
    }

    /**
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orderDetails(Request $request)
    {
        if (session()->has('qaToken')) {

            $orderId = $request->get('id');
            $url = env('GET_BASE_URL') . env('QA_ORDER') . "getorders";
            $res = qaConsume($url, 'GET');
            if ($res['status'] == 0) {
                $orderDetail = $res['data'];
                foreach ($orderDetail as $order) {
                    if ($order->id == $orderId) {
                        $newresp = $order;
                        $measurement = json_decode($newresp->measurement);
                        //dd($measurement);
                        return view('qa.orders.orderDetails', compact('newresp', 'measurement'));
                    }
                }
            }
        } else {
            return redirect(route('admin.login'));
        }
    }


    public function bespokeOrderDetails(Request $request) {
        if (session()->has('qaToken')) {
            $orderNumber = $request->orderNum;
            $url = env('GET_BASE_URL') . env('QA_ORDER') . "{$orderNumber}/get_one_bespoke_request_orders";

            $data = [
                'page' => '0',
                'size' => '20'
            ];

            $data = ['json' => $data];
            $res = qaConsume($url, 'POST', $data);
            if($res['status'] === '00') {
                $newresp = $res['data'];
              //  dd($newresp);

                return view('qa.orders.bespokeOrderDetails', compact('newresp'));
            }

        }
    }

    public function changeQaPassword()
    {
        return view('qa.users.changeQaPassword');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changePassword()
    {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('USER_ACCOUNT_API') . "/editpassword";
        $res = qaConsume($url, 'POST', $data);

        if ($res['status'] == '00') {
            session()->forget('qaToken');
            return response()->json(['status' => '00', 'message' => 'Password Change Successfully']);
        } else {
            return response()->json(['status' => '99', 'message' => 'Not successfully']);
        }

    }

    /**
     * View all Designers Details
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allDesigners()
    {

        if (session()->has('qaToken')) {
            $url = env('GET_BASE_URL') . env('QA_SECURE') . "getalldesigners";
            $res = qaConsume($url, 'GET');

            if ($res['status'] == 0) {
                $allDesigners = $res['data'];

            }
            return view('qa.users.designers', compact('allDesigners'));
        } else {
            return redirect(route('admin.login'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function viewDesigner()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('qaToken')) {
            // $designer = str_replace('-',' ',request('designerName'));
            $id = request('designerName');
            $url = env('GET_BASE_URL') . "/fashion/designer/$id/getdesignerbyid";
            $res = consumeWithoutToken($url, 'GET');
            if ($res['status'] == 00) {
                $designer = $res['data'];
                return view('qa.users.view-designer', compact('designer'));
            } else {
//                return $redirectView;
            }
        }
        return redirect(route('admin.login'));
    }

    /**
     * View all Designers Details
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allUsers()
    {
        if (session()->has('qaToken')) {
            $url = env('GET_BASE_URL') . env('QA_SECURE') . "getallusers";
            $allUsers = qaConsume($url, 'GET');
            return view('qa.users.users', compact('allUsers'));
        } else {
            return redirect(route('admin.login'));
        }
    }

    /**
     * @param Request $request
     * @return Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function allDeliveredOrders(Request $request)
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('qaToken')) {
            $data = ['page' => '0', 'size' => '1000'];
            $data = ['json' => $data];
            $url = env('GET_BASE_URL') . env('QA_ORDER') ."get_all_delivered_orders";
            $res = qaConsume($url, 'POST', $data);
//            dd($res);
            if ($res['status'] == 200) {
                $orders = $res['data'];
//                dd($orders);
                return view('qa.orderList.deliveredOrders', compact('orders'));
            } else {
                return $res;
            }
        } else {
            return redirect(route('admin.login'));
        }
    }

    /**
     * View all Orders
     * @param Request $request
     * @return Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function listOrders(Request $request)
    {
        if (session()->has('qaToken')) {
            $url = env('GET_BASE_URL') . env('QA_ORDER') . "getallorders";
            $res = qaConsume($url, 'GET');
            if ($res['status'] == 0) {
                $allOrders = $res['data'];
                return view('qa.orderList.order-list', compact('allOrders'));
            }
        } else {
            return redirect(route('admin.login'));
        }
    }

    /**
     * View all Order Details
     * @param Request $request
     * @return Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getListOrdersDetails(Request $request)
    {
        if (session()->has('qaToken')) {
            $id = \request('order');
            $url = env('GET_BASE_URL') . env('QA_ORDER') . "$id/getorder";

            $res = qaConsume($url, 'GET');
//            dd($res);
            if ($res['status'] == 0) {
                $order = $res['data'];
//                dd($order);
                return view('qa.orderList.list-order-details', compact('order'));
                /*foreach ($orderDetail as $order) {
                    if ($order->id == $id) {
                        $newresp = $order;
                        dd($newresp);
                        $measurement = json_decode($newresp->measurement);
                        //dd($measurement);
                        return view('qa.list-order-details', compact('newresp', 'measurement'));
                    }
                }*/
            }

        } else {
            return redirect(route('admin.login'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateInvoice(Request $request)
    {
        if (session()->has('qaToken')) {
            $orderNum = $request->route('orderNum');
            $url = env('GET_BASE_URL') . env('QA_ORDER') . $orderNum . "/generateinvoice";
            $res = qaConsume($url, 'GET');
            if ($res['status'] == 0) {
                $invoice = $res['data'];
                return view('qa.notes.invoice', compact('invoice'));
            }
        } else {
            return view('admin.login');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateDeliveryNote(Request $request)
    {

        if (session()->has('adminToken')) {
            $orderNum = $request->route('orderNum');
            $url = env('GET_BASE_URL') . env('QA_ORDER') . $orderNum . "/generateinvoice";
            $res = qaConsume($url, 'GET');
            if ($res['status'] == 0) {
                $invoice = $res['data'];
                return view('qa.notes.delivery-note', compact('invoice'));
            }
        } else {
            return view('admin.login');
        }
    }
}
