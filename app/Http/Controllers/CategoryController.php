<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse|Request|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function add(Request $request)
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/category/addcategory";
        $data = ['categoryName' => request('categoryName')];
        $validate = $request->validate(['categoryName' => 'required']);
        if (is_null($validate)) {
            $error = 'Please, enter category name';
            return view('admin.category')->with('error', $error);
        } else {
            $data = ['json' => $data];
            $res = adminConsume($url, 'POST', $data);
            if ($res['data'] == 0) {
                return back();
            }
        }

        return request('categoryName');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\RedirectResponse|Request|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addSub(Request $request)
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/category/addsubcategory";
        $subcat = explode(',', request('subcategories'));
        $subcat = array_map('trim', $subcat);
        $prodType = $request->input('producttype');
        $data = ['categoryId' => request('id'), 'subCategoryName' => $subcat, 'productType' => $prodType];
        $data = ['json' => $data];
        $res = adminConsume($url, 'POST', $data);
        if ($res['data'] == 0) {
            return back();
        }
        return request('categoryName');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editSub()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/category/editsubcategory";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];

        $res = adminConsume($url, 'POST', $data);

        return $res;
    }


    /**
     * @return array|\Illuminate\Http\RedirectResponse|Request|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addStyle()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/admin/category/addstyle";
        $styles = explode(',', request('styles'));
        $styles = array_map('trim', $styles);
        $data = ['subCategoryId' => request('subcat'), 'style' => $styles];
        $data = ['json' => $data];
        $res = adminConsume($url, 'POST', $data);
        if ($res['status'] == 0) {
            return back();
        }
        return request('categoryName');
    }


}
