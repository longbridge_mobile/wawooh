<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\UserRegistration;
use Mail;


class LoginController extends Controller
{
    /**
     * Login Model for users
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login()
    {

        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/signin";//api
        $res = consumeWithoutToken($url, 'POST', $data);//consume api

        if ($res['status'] == 0) {
            $token = json_encode($res['data']->token);//get token
            //set auth session

            session(['userToken' => $token]);
            //check if an item existed in the cart before logging in
            if (session()->has('userCart') && session()->has('userToken')) {
                $userCart = ['carts' => session('userCart')];
                $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/additemstocart";

                $body = $userCart;
                $data = ['json' => $body];//request parameter
                $cart = consume($url, 'POST', $data);//consume api
                if ($cart['status'] == 0) {
                    session()->forget('userCart');
                }
            }
            return $res;
        } else {
            return $res;
        }
    }

    /**
     * Registration Method for user only
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function register()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/register";//api
        $res = consumeWithoutToken($url, 'POST', $data);//consume api
        if ($res['status'] == 00) {
            Mail::to($body['email'])->send(new UserRegistration($body));
        } 
        return $res;
        // if(!Mail::failures()){
        //     return back()->with('success', 'Verification code was successfully resent');
        //  }
    }

    public function registerWithFacebook()
    {
        return view('user.register');
    }

    /**
     * Login Method for designer Only
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function loginDesigner()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/signin";//api

        $res = consumeWithoutToken($url, 'POST', $data);//consume api

        if ($res['status'] == 0) {
            if ($res['data']->role == 2) {
                $token = json_encode($res['data']->token);//get token
                session(['designerToken' => $token]);
            }
            return $res2 = collect($res)->toJson();//return response 
        } else {
            return collect(array('status' => $res['status'], 'message' => $res['message']))->toJson();
        }

        //set auth session
    }

    /**
     * Logout Method for Designer
     */
    public function logoutDesigner()
    {
        session()->forget('designerToken');
        return redirect(route('designersLandingPage'));
    }

    /**
     * Registration Method for Designer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function registerDesigner()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL')."/fashion/register";//api
        return $res = consumeWithoutToken($url, 'POST', $data);//consume api
    }


}
