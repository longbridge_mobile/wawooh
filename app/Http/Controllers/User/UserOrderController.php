<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class UserOrderController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cart()
    {
        $cart = null;
        $products = null;
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/cart";

        //$redirectView = view('errors.noNetwork');

        if (session()->has('userToken')) {
            $data = getCartDetails();
           // dd($data);
            if (isset($data['cart'])) {
                if ($Agent->isMobile()) {
                    return Redirect::to($page);
                } else {
                    return view('cart')->with('carts', $data['cart'])->with('total', $data['total']);
                }
            } else {
                return view('cart')->with('carts', $data['cart'])->with('total', $data['total']);
//                return $redirectView;
//                return redirect(route('fashionStore'));
            }

        } else if (session()->has('userCart')) {
            $page = env('MOBILE_LINK') . "/cart";

            $carts = session('userCart');
            $total = 0;
            $products = array();

            foreach ($carts as $cart) {
                $quantity = $cart['quantity'];
                $size = $cart['size'];
                $res = getProductDetails($cart['productId']);
                $myObj['id'] = $res['data']->id;
                $myObj['name'] = $res['data']->name;
                $myObj['picture'] = $res['data']->productColorStyleDTOS[0]->productPictureDTOS[0]->picture;
                $myObj['amount'] = @$res['data']->amount;

                foreach ($res['data']->productColorStyleDTOS as $attributes) {
                    if ($attributes->id == $cart['productColorStyleId']) {
                        foreach ($attributes->productSizes as $sizes) {
                            if ($sizes->name == $cart['size']) {
                                $myObj['stockNo'] = $sizes->numberInStock;
                                break;
                            }
                        }
                    }
                }

                $myObj['quantity'] = $quantity;
                $myObj['slashedPrice'] = $res['data']->slashedPrice;
                $myObj['designerName'] = $res['data']->designerName;
                $myObj['size'] = $size;

                if ($res['data']->slashedPrice > 0) {
                    $total += ($res['data']->slashedPrice * $cart['quantity']);
                } else {
                    $total += ($res['data']->amount * $cart['quantity']);
                }
                array_push($products, collect($myObj));
            }
            $products = str_replace('"', ' ', $products);

            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('cart')->with('products', $products)->with('total', $total);
            }
        } else {
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('cart');
            }
        }
    }

    /**
     * @return array|mixed|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addToCart()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/addtocart";
        $body = json_decode(request()->getContent(), true);
        if (session()->has('userToken')) {
            $data = ['json' => $body]; //request parameter
            return $res = consume($url, 'POST', $data); //consume api
            $resp2 = $res['data'];
        } else {
            //session()->push(['userCart' => $body]);
            session()->push('userCart', $body);
            if (in_array($body, session('userCart'))) {
                return json_encode(array('status' => '0'));
            } else {
                return json_encode(array('status' => 99));
            }
        }
    }

    /**
     * Anonymous Checkout
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function anonymousCheckout()
    {
        $url = env('GET_BASE_URL') . "/fashion/anonymoususer/create";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $res = consume($url, 'POST', $data);
        if ($res['status'] == '00') {
            $id = $res['data'];
            session(['anonymousCheckout' => $id]);
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addOrder()
    {
        $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/addorder";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        return $res = consume($url, 'POST', $data);//consume api
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orderDetails()
    {
        $orderId = request('orderId');
        $url = env('GET_BASE_URL') . "/fashion/order/{$orderId}/getorder";
        return $res = consume($url, 'GET');
    }

    /**
     * Customization page for the user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sizeInput()
    {
        //return view('customize');
        $redirectView = view('errors.noNetwork');

        $product = request('product');

        $urlCust = env('GET_BASE_URL') . "/fashion/customization/{$product}/getmandatorymeasurements";

        // dd($urlCust);
        //$meas = consumeWithoutToken($urlCust,'GET');//consume api
        //dd($meas);

        $urlCust = env('GET_BASE_URL') . "/fashion/secure/customization/getusermeasurements";
        $meas = consume($urlCust, 'GET');//consume api
        //dd($meas);
        $getUserMeasurement = env('GET_BASE_URL'). "/fashion/measurementtypes/";

        //dd($getUserMeasurement);
        $getUser = consume($getUserMeasurement, 'GET');
        //dd($getUser);

        if (is_numeric($product)) {
            $res = getProductDetails($product);
            //dd($res);

            if (isset($res['data']->id)) {
                $resId = $res['data']->id;
            } else {
                return view('errors.noNetwork');
            }

            if ($res['data']->bespokeProductDTO !== null) {
                $resMandantory = $res['data']->bespokeProductDTO->mandatoryMeasurements;
            } else {
                $resMandantory = '';
            }

            $realMan = json_decode(json_decode($resMandantory));

            if ($res['status'] == 0) {
                $product = $res['data'];
            } else {
                return $redirectView;
//                return displayError($res, 'customize');
            }
        }

        if (session()->has('userToken')) {

            $url = env('GET_BASE_URL') . "/fashion/secure/customization/getusermeasurements";
            $m = consume($url, 'GET');//consume api
            if ($m['status'] == 0) {
                $measurements = $m['data'];

                return view('customize', compact('product', 'measurements', 'realMan'));

            } else {
                //  return displayError($m);
                return view('customize', compact('product', 'measurements', 'realMan'));
            }

        } else {
            $measurements = array();
           // dd($product);
            return view('customize', compact('product', 'measurements', 'realMan'));
        }
    }

    /**
     * @return array|mixed|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateCart()
    {
        $body = json_decode(request()->getContent(), true);
        if (session()->has('userToken')) {
            $data = ['json' => $body];//request parameter
            $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/updatecart";
            return $res = consume($url, 'POST', $data);//consume api
        } else {
            if (session()->has('userCart')) {
                $item = session('userCart.' . $body['id']);
               // return $body['quantity'];
                $item['quantity'] = $body['quantity'];
                if( $item['quantity'] = $body['quantity']){
                    return json_encode(array('status' => 0));
                }

              /*  if (session(['userCart.' . $body['id'] => $item])) {
                    return json_encode(array('status' => 0));
                } else {
                    return json_encode(array('status' => 99));
                }*/
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkout()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('userToken')) {
            $returnView = view('errors.noNetwork');
            //get address details
            $addresses = getAddresses();
            $userDetail = getUserDetails();
            if (isset($userDetail['data'])) {
                $userData = $userDetail['data'];
            } else {
                return $returnView;
            }
            // dd($userData);
            if ($userDetail['status'] != 0) {
                return $redirectView;
            }
            if ($addresses['status'] != 0) {
                return $redirectView;
            }

            $addresses = $addresses['data'];
           // dd($addresses);

            $data = getCartDetails();
          // dd($data);
            $items = $data['cart'];

            if (empty($items)) {
                return redirect(route('cart'));
            } else {
                $total = $data['total'];
               // dd($items);
                return view('checkout', compact('addresses', 'userData', 'items', 'total'));
            }

        } else if (session()->has('userCart')) {
            $carts = session('userCart');
            $total = 0;
            $products = array();
            foreach ($carts as $cart) {

                $total += ($cart['amount'] * $cart['quantity']);
                $res = getProductDetails($cart['productId']);
                $myObj['name'] = $res['data']->name;
                $myObj['picture'] = $res['data']->productColorStyleDTOS[0]->productPictureDTOS[0]->picture;
                $myObj['amount'] = $res['data']->amount * $cart['quantity'];
//                dd($myObj);
                array_push($products, collect($myObj));
            }

            $products = str_replace('"', ' ', $products);
            return view('checkout', compact('products', 'total'));
        } else {
            return redirect(route('cart'));
        }
    }

    /**
     * Function to track order from email
     * Users' Order
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function trackOrder(Request $request)
    {
        $Agent = new Agent();
        $orderNum = extractNumberOnly(request('orderNum'));
        $email = request('email');
        $decodeEmail = base64_decode($email);
        $page = env('MOBILE_LINK') . '/bank-transfer?email=' . $email . '&orderNum=' . $orderNum;
        if (session()->has('userToken')) {
            $orderNum = extractNumberOnly(request('orderNum'));

            $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/$orderNum/getorderbynum";
            $payUrl = env('GET_BASE_URL') ."/fashion/order/payment/$orderNum/gettransferinfo";
//            dd($payUrl);
            $res = consume($url, 'GET');
//            dd($res);
            $resPay = consume($payUrl, 'GET');
            if($resPay['status'] === '00') {
                if(isset($resPay['data'])) {
                    $orderPayments = $resPay['data'];
//                    dd($orderPayments);
                } else {
                    $orderPayments = [];
                }
            }
            $email = request('email');
            $decodeEmail = base64_decode($email);
            $page = env('MOBILE_LINK') . '/bank-transfer?email=' . $email . '&orderNum=' . $orderNum;
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('users.track-order', compact('decodeEmail','orderPayments'));
            }
        }

        if($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('users.track-order', compact('decodeEmail'));
        }
    }

    /**
     * To update and Show payment History
     * User's order/payment
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function paymentHistory() {
        if(session()->has('userToken')) {
            $orderNum = extractNumberOnly(request('orderNum'));
            $email = request('email');
            $decodeEmail = base64_decode($email);

            $url = env('GET_BASE_URL') . env('USER_ORDER_API')."$orderNum/getorderbynum";
            $payUrl = env('GET_BASE_URL') . env('USER_PAYMENT_API'). "$orderNum/gettransferinfo";
            $res = consume($url, 'GET');
//            dd($res);
            $resPay = consume($payUrl, 'GET');
            if($resPay['status'] === '00') {
                if(isset($resPay['data'])) {
                    $orderPayments = $resPay['data'];
//                    dd($orderPayments);
                } else {
                    $orderPayments = [];
                }
            }
            $email = request('email');
            $decodeEmail = base64_decode($email);
            return view('users.payment-history',compact('decodeEmail','orderPayments'));
        } else {
            return redirect(route('userDashboard'));
        }
    }

    /**
     * Guest Checkout Method
     */
    public function guestCheckout()
    {
        if (session()->has('userCart')) {
            $cart = session('userCart');
            $total = 0;
            $product = [];
        }
        return view('guest-checkout');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userAcceptOrder(){
        if (session()->has('userToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/bespoke/order/bids/update";
            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body]; //request parameter
            $res = consume($url, 'POST', $data);
            return $res;
        }
    }


    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    function addBespokeToCart(){

         if (session()->has('userToken')) {
             $url = env('GET_BASE_URL')."/fashion/secure/customer/order/addtocart";
             $body = json_decode(request()->getContent(), true);
             $data = ['json' => $body]; //request parameter
             $res = consume($url, 'POST', $data);
             return $res;
         }

     }
}
