<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class UserController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function forgotPassword()
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/forgotpassword";

        $previousUrl = url()->previous();

//        return view('users.forgotpassword', compact('previousUrl'));

        if (session()->has('userToken')) {
            session()->forget('userToken');
            return view('users.forgotpassword', compact('previousUrl'));
//            return redirect(route('settings'));
        } else {
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('forgotpassword', compact('previousUrl'));
            }
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function resetPassword()
    {
        $email = request('email');
        $url = request('url');
        $data = ['email' => $email, 'currentUrl' => $url];
        $data = ['json' => $data];
        $url = env('GET_BASE_URL') . "/fashion/forgotpassword";
        return $res = consumeWithoutToken($url, 'POST', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function inputPassword()
    {
        $Agent = new Agent();

        $email = request('email');
        $decodeEmail = base64_decode($email);
        $previous = request('url');
        $newPass = request('newPassword');
        $email = base64_encode($email);
        $newPass = base64_decode($newPass);
        $page = env('MOBILE_LINK') . '/changepassword?email=' . $email . '&url=' . $previous . '&newPassword=' . $newPass;
        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('users.inputpassword', compact('email', 'decodeEmail', 'previous', 'newPass'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function paymentDetails(Request $request)
    {
        return view('payment-details');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function edit()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/edituser";

        return $res = consume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addAddress()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/address/addaddress";
        return $res = consume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateAddress()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/address/updateaddress";
        return $res = consume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteAddress()
    {
        $addressId = request('address');
        $url = env('GET_BASE_URL') . "/fashion/secure/address/$addressId/deleteaddress";
        return $res = consume($url, 'GET');
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function wishlist()
    {
        $mobile  = new Agent();
        if(session()->has('userToken')) {
            $url = env('GET_BASE_URL') . env('USER_WISHLIST_API') . "get";
            $data = [
                'page' => '0',
                'size' => '20',
            ];
            $data = ['json' => $data];
            $res = consume($url, 'POST', $data);
            $error = 'Oops! No item found';
            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $wishes = $res['data'];
                    return view('users.wishlist', compact('wishes'));
                } else {
                    return view('users.wishlist', compact('error'));
                }
            } elseif ($res['status'] == "99") {
                return view('users.wishlist', compact('error'));
            }
        } else {
            return redirect(route('fashionStore'));
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changePassword()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/editpassword";
        return $res = consumeWithoutToken($url, 'POST', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function activateMail()
    {
        $email = base64_decode(request('email'));
        $data = ['email' => $email];
        $data = ['json' => $data];
        $url = env('GET_BASE_URL') . "/fashion/activateaccount";
        $res = consumeWithoutToken($url, 'POST', $data);
        if ($res['status'] == 0) {
            $status = $res['data']->message;
        } else {
            $status = "Couldn't verify successfully. An unexpected error occurred.";
        }
        return view('users.activate', compact('status', 'res'));
    }

    public function postDecision()
    {
        return "true";
    }

    /**
     * Create Event by user, render the add event page
     */
    public function addEvent()
    {
        $userToken = session('userToken');
        return view('users.add-event');
    }

    /**
     * this check if account is activate
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function accountStatus()
    {
        $email = request('email');
        $decodeEmail = base64_decode($email);
        return view('account-status', compact('decodeEmail'));
    }

    /**
     * Render the size chart page
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sizeChart()
    {
        return view('size-chart');
    }

    /***
     * Payment method for payment confirmation
     */
    public function paymentSuccessful()
    {
       /* if(session()->has('userToken')) {
            return view('paymentsuccess');
        }
        else {
            return redirect(route('cart'));
        }*/
          return view('paymentsuccess');
    }

    /**
     * User feedback form and page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userFeedBack(Request $request)
    {
        $Agent = new Agent();

        $page = env('MOBILE_LINK') . '/feedback';

        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('feedback');
        }
    }

    /**
     * User Product Feedback
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productFeedback()
    {
        $Agent = new Agent();
        $email = request('email');
        $orderNum = request('orderNum');
        $encodeEmail = base64_encode($email);
        $page = env('MOBILE_LINK') . '/product-feedback?email=' . $encodeEmail . '&orderNum=' . $orderNum;

        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('users.product-feedback');
        }
    }

    public function decisionDetails()
    {
        return "hello world";
        /*$url = env('GET_BASE_URL')."/fashion/order/userrejectdecision";

        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];

        $res = consume($url,'POST', $data);
        dd($res);

        return view('user.decision', compact('res'));*/
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function decision(Request $request)
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/decision";

        $userToken = session('userToken');
        $email = request('email');
        $decodeEmail = base64_decode($email);
        $orderNumber = request('orderNum');
        $Id = request('itemId');
        if ($request->session()->has('userToken')) {
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('users.decision', compact('Id', 'decodeEmail', 'orderNumber', 'userToken'));
            }
        } else {
            return view('users.decision', compact('userToken', 'decodeEmail', 'orderNumber', 'Id'));
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function declineDecision(Request $request)
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/decline";

        $userToken = session('userToken');
        $email = request('email');
        $decodeEmail = base64_decode($email);
        if ($request->session()->has('userToken')) {
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('users.decline', compact('decodeEmail', 'userToken'));
            }
        } else {
            return view('users.decline', compact('decodeEmail', 'userToken'));
        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function complaint()
    {
        $email = request('email');
        $decodeEmail = base64_decode($email);
        $orderNumber = request('orderNum');
        $Id = request('itemId');
        return view('users.complaint', compact('decodeEmail', 'orderNumber', 'Id'));
    }


}

