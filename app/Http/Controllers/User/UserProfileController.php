<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class UserProfileController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request)
    {
        if (session()->has('userToken')) {
            $url = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($url, 'GET');
            //return total orders
            $url = env('GET_BASE_URL') . env('USER_ORDER_API') . "getuserorder";
            $orders = consume($url, 'GET');
            $totalOrders = count($orders['data']);
            $url = env('GET_BASE_URL') . env('USER_ADDRESS_API') . "getaddress";
            $addresses = consume($url, 'GET');

            if ($addresses['status'] == "00") {
                if (isset($addresses['data'])) {
                    $addresses = $addresses['data'];
//                    dd($addresses);
                    //return view('users.address-book', compact('addresses', 'user'));
                } else {
                    $addresses = [];
                }
            }

            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;
            } else {
                return redirect(route('fashionStore'));
            }
            return view('users.index', compact('user', 'totalOrders', 'addresses'));
        } else {
            return redirect(route('fashionStore'));
        }
    }

    /**
     * User Control Dashboard, the order page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orders(Request $request)
    {
        if (session()->has('userToken')) {
            $url = env('GET_BASE_URL') . env('USER_ORDER_API') . "getuserorder";
            $orders = consume($url, 'GET');
            //dd($orders);

            $url = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($url, 'GET');

            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;
            } else {
                return redirect(route('fashionStore'));
            }

            if ($orders['status'] == "00") {
                if (isset($orders['data'])) {
                    $orders = $orders['data'];
//                    dd($orders);
                } else {
                    $orders = [];
                }
            } else {
                return view('users.orders', compact('orders'));
            }
            return view('users.orders', compact('orders', 'user'));
        } else {
            return redirect(route('fashionStore'));
        }
    }

    /**
     * User Control Dashboard, the bespoke request page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeRequests(Request $request)
    {
        if (session()->has('userToken')) {
            $bespokeUrl = env('GET_BASE_URL') . env('USER_BESPOKE_ORDER') . "bespokes";
            $bespoke_req = consume($bespokeUrl, 'GET');

            $url = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($url, 'GET');

            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;
            } else {
                return redirect(route('fashionStore'));
            }

            if ($bespoke_req['status'] == "00") {
                if (isset($bespoke_req['data'])) {
                    $besRequests = $bespoke_req['data'];
                } else {
                    $besRequests = [];
                }
            } else {
                return view('users.bespoke-requests', compact('besRequests'));
            }
            return view('users.bespoke-requests', compact('besRequests', 'user'));
        } else {
            return redirect(route('fashionStore'));
        }
    }

    /**
     * User Control Dashboard, the bespoke request page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function viewBespokeQuotes(Request $request)
    {
        if (session()->has('userToken')) {
            $bidRequestId = $request->route('bidId');
            $urlQuote = env('GET_BASE_URL') . env('USER_BESPOKE_ORDER') . "{$bidRequestId}/bids";
            $bidQuote = consume($urlQuote, 'GET');

            $url = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($url, 'GET');


            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;

            } else {
                return redirect(route('fashionStore'));
            }

            if ($bidQuote['status'] == "00") {
                if (count($bidQuote['data'])) {
                    $besQuote = $bidQuote['data'];
//                    dd($besQuote);
                    return view('users.bespoke-quotes', compact('besQuote', 'user'));

                    /* foreach ($besQuote as $details) {
                         $besRequests = $details->bespokeOrderRequest;
                     }*/
                } else {
                    $besQuote = [];
                    return view('users.bespoke-quotes', compact('besQuote', 'user'));
                }
            }
        } else {
            return redirect(route('fashionStore'));
        }
    }

    /**
     * User Control Dashboard, the address page
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addressBook()
    {
        if (session()->has('userToken')) {
            $user = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($user, 'GET');

            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;
            } else {
                return redirect(route('fashionStore'));
            }

            $url = env('GET_BASE_URL') . env('USER_ADDRESS_API') . "getaddress";
            $addresses = consume($url, 'GET');
            //dd($addresses);

            if ($addresses['status'] == "00") {
                if (isset($addresses['data'])) {
                    $addresses = $addresses['data'];
//                    dd($addresses);
                    return view('users.address-book', compact('addresses', 'user'));
                } else {
                    $addresses = [];
                }
            }
            return view('users.address-book', compact('addresses', 'user'));
        } else {
            return redirect(route('fashionStore'));
        }
    }

    /**
     * User Control Dashboard, the user profile page
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateProfile()
    {
        if (session()->has('userToken')) {
            $user = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($user, 'GET');

            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;
            } else {
                return redirect(route('fashionStore'));
            }
            return view('users.update-profile', compact('user'));
        }
    }

    /**
     * User Control Dashboard, the user profile page
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function myMeasurements()
    {
        if (session()->has('userToken')) {
            $user = env('GET_BASE_URL') . "/fashion/getuserdetails";
            $res = consume($user, 'GET');

            $url = env('GET_BASE_URL') . env('USER_CUSTOMIZATION_API') . "getusermeasurements";
            $m = consume($url, 'GET');//consume api
//             dd($m);
            if (isset($res['data']->userDetails)) {
                $user = $res['data']->userDetails;
            } else {
                return redirect(route('fashionStore'));
            }

            if ($m['status'] == 0) {
                $measurements = $m['data'];
//                dd($measurements);
                return view('users.measurement', compact('user', 'measurements'));

            } else {
                //  return displayError($m);
                $measurements = array();
                return view('users.measurement', compact('user', 'measurements'));
            }

        } else {
            return \redirect(route('fashionStore'));
        }
    }

    /**
     * Fund wallet method
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fundWallet() {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/secure/wallet/fundwallet";

        return $res = consume($url, 'POST', $data);
    }
}
