<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeasurementController extends Controller
{
    //
    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function add(){
        $body = json_decode(request()->getContent(), true);     
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL'). env('USER_CUSTOMIZATION_API')."add"; //api

       // $url = env('GET_BASE_URL') . env('USER_BESPOKE_ORDER') . 'add';

        return $res = consume($url,'POST', $data);//consume api
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addBespokeMeasurement(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        // $url = env('GET_BASE_URL')."/fashion/secure/customization/add"; //api
        $url = env('GET_BASE_URL') . env('USER_BESPOKE_ORDER') . 'add';
        return $res = consume($url,'POST', $data);//consume api
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function  uploadBespokeImageToBank(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . env('DESIGNER_STYLES_API')."addbespokestyle";
        return $res = designerConsume($url,'POST', $data);

    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeSubmit(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') .  env('DESIGNER_BESPOKE_REQUEST') . "bids/update";
        return $res = designerConsume($url,'POST', $data);
    }


    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dispatchBespoke(){
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . env('ADMIN_BESPOKE')."bids/add";
        return $res = adminConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(){
        $body = json_decode(request()->getContent(), true);     
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL'). env('USER_CUSTOMIZATION_API')."update";//api
        return $res = consume($url,'POST', $data);//consume api
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editForm(){
        $id = request('id');
        if(is_numeric($id)){
            $url = env('GET_BASE_URL'). env('USER_CUSTOMIZATION_API')."{$id}/get"; //api
            return $res = consume($url,'GET');//consume api
        }
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(){
        $id = request('id');
        if(is_numeric($id)){
            $url = env('GET_BASE_URL'). env('USER_CUSTOMIZATION_API')."${id}/delete"; //api
            $res = consume($url,'GET');//consume api
            return back();
        }
    }
}
