<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Agent;
use Share;

class EventsController extends Controller
{
    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index()
    {
        $Agent = new Agent();

        $url = env('GET_BASE_URL') . "/fashion/event/getevents";
        //if user filters events
        $page = env('MOBILE_LINK') . '/events';

        if (is_numeric(request('month')) && is_numeric(request('year')) && !empty(request('month')) && !empty(request('year'))) {
            $month = request('month');
            $year = request('year');
            $url = env('GET_BASE_URL') . "/fashion/event/geteventbydate";
            $data = [
                'page' => '0',
                'size' => '5',
                'month' => $month,
                'year' => $year
            ];
            $data = ['json' => $data];

            $res = consumeWithoutToken($url, 'POST', $data);
            if ($res['status'] == 0) {
                $resp = $res['data']->events;
                if ($Agent->isMobile()) {
                    return Redirect::to($page);
                } else {
                    return view('events.index', compact('resp'));
                }
            } else {
                return view('errors.noNetwork');
            }

        }
        //search events
        if (!empty(request('query'))) {
            $query = request('query');
            $url = env('GET_BASE_URL') . "/fashion/event/$query/searchevent";
            $res = consume($url, 'GET');
            if ($res['status'] == 0) {
                $resp = $res['data']->result;
                return view('events.index', compact('resp'));
            } else {
                return view('errors.noNetwork');
            }
        }

        if (!empty(request('eventType'))) {
            $type = request('eventType');
            if ($type == 'S') {
                $title = 'Sponsor Events | Events';
            } elseif ($type == 'T') {
                $title = 'Trending Events | Events';
            } else {
                $title = 'Events';
            }
            $data = [
                'page' => '0',
                'size' => '5',
                'eventType' => $type
            ];
            $data = ['json' => $data];
            $res = consumeWithoutToken($url, 'POST', $data);
            if ($res['status'] == 0) {
                $resp = $res['data']->events;
                if ($Agent->isMobile()) {
                    return Redirect::to($page);
                } else {
                    return view('events.index', compact('resp', 'title'));
                }
            } else {
//                return $resp;
                return view('errors.noNetwork');
            }
        }
        //default
        $data = [
            'page' => '0',
            'size' => '5',
            'eventType' => 'A'
        ];
        $data = ['json' => $data];
        $res = consumeWithoutToken($url, 'POST', $data);
        if ($res['status'] == 0) {
            $resp = $res['data']->events;
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('events.index', compact('resp'));
            }
        } else {
            return view('errors.noNetwork');
        }
    }

    /**
     * If User filters events by month and year
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getEventsApi()
    {
        $redirectView = view('errors.noNetwork');
        //if user filters events by month and year
        if (is_numeric(request('month')) && is_numeric(request('year'))) {

            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body];//request parameter
            $url = env('GET_BASE_URL') . "/fashion/event/geteventbydate";

            $res = consumeWithoutToken($url, 'POST', $data);
            if ($res['status'] == 0) {
                return $resp = $res['data']->events;
            } else {
                return $redirectView;
//                return $res;
            }
        } //if user searches events
        else if (!empty(request('query'))) {
            $query = request('query');
            $url = env('GET_BASE_URL') . "/fashion/event/$query/searchevent";
            $res = consume($url, 'GET');
            if ($res['status'] == 0) {
                return $resp = $res['data']->result;
            } else {
                return $redirectView;
//                return $res;
            }
        } //if user wants more an event type
        else if (!empty(request('eventType'))) {
            $type = request('eventType');
//            $url = env('GET_BASE_URL') . "/fashion/event/$query/searchevent";
            $data = ['page' => request('page'),
                'size' => request('size'),
                'eventType' => $type];
            $data = ['json' => $data];
            $res = consumeWithoutToken($url, 'POST', $data);
            if ($res['status'] == 0) {
                return $resp = $res['data']->events;
            } else {
                return $redirectView;
//                return $res;
            }
        } else {
            $data = ['page' => request('page'),
                'size' => request('size'),
                'eventType' => 'A'];
            $data = ['json' => $data];
            $url = env('GET_BASE_URL') . "/fashion/event/getevents";
            $res = consumeWithoutToken($url, 'POST', $data);
            if ($res['status'] == 0) {
                return $resp = $res['data']->events;
            } else {
                return $redirectView;
//                return $res;
            }
        }
    }

    //method to call when user scrolls if he doesn't filter by month and year

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getEvents()
    {
        $redirectView = view('errors.noNetwork');
        $url = env('GET_BASE_URL') . "/fashion/event/getevents";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $res = consume($url, 'POST', $data);
        if ($res['status'] == 0) {
            return $resp = $res['data']->events;
        } else {
            return $redirectView;
//            return $res;
        }
    }

    /**
     * View Pictures in an event
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function showEvent(Request $request)
    {
//        $redirectView = view('errors.noNetwork');
        $Agent = new Agent();
//        $eventId = request('event');
        $eventId = $request->route('eventId');
        $url = env('GET_BASE_URL') . "/fashion/event/$eventId/geteventbyId";
        $getSideEvent = env('GET_BASE_URL') . "/fashion/event/getevents";
        $data = [
            'page' => '0',
            'size' => '4',
            'eventType' => 'A'
        ];
        $data = ['json' => $data];
        $res = consumeWithoutToken($getSideEvent, 'POST', $data);
        if ($res['status'] == 0) {
            $sideEvents = array_reverse($res['data']->events);
        }

        $res = consume($url, 'GET');
        if ($res['status'] == 0) {
            $event = $res['data']->event;
            $page = env('MOBILE_LINK') .'/events/'.$event->eventName.'/'.$event->id;
            $shareLink = Share::currentPage($event->eventName)
                    ->facebook()
                    ->twitter()
                    ->whatsapp();
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('events.view-event', compact('event', 'sideEvents','shareLink'));
            }
//            return view('events.view-event', compact('event', 'sideEvents','shareLink'));
        } else {
            return view('errors.noNetwork');
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getEventPictureDetails()
    {
        $pictureId = request('pictureId');
        $client = new Client([
            'headers' => ['content-type' => 'application/json', 'accept' => 'application/json'],
        ]);
        $url = env('GET_BASE_URL') . "/fashion/event/$pictureId/geteventpicturebyid";
        return $response = consume($url, 'GET');
        return $res = responseToJson($response->getBody());

    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search()
    {
        $query = request('query');
        $url = env('GET_BASE_URL') . "/fashion/event/$query/searchevent";
        return $res = consume($url, 'GET');
        return view('events.index', compact('resp'));
    }
}
