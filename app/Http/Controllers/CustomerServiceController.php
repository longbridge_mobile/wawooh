<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerServiceController extends Controller
{
    //

    public function dashboard(){
        return view('customerService.dashboard');
    }

    public function getAllOrder(){

        $redirectView = view('errors.noNetwork');
        if (session()->has('customertoken')) {
            $url = env('GET_BASE_URL') . env('CUSTOMER_SERVICE_ORDER') . "getorders";
            $res = customerConsume($url, 'GET');
            //  dd($res);
            if ($res['status'] == 0) {
                $orders = $res['data'];
                // dd($orders);
                return view('customerService.orders.all', compact('orders'));
            } else {
                return $redirectView;
            }
        }
       // dd('out');
    }
}
