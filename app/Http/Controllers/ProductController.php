<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class ProductController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request)
    {
        $Agent = new Agent();
        $categories = getCategories();
        $subcatId = $request->route('subCatId');
        if (!empty(request('query'))) {
            $query = request('query');
            $searchTerm = $request->query('query');

            $url = env('GET_BASE_URL') . "/fashion/se/search";
            $aggs = ['fieldName' => 'designerName', 'type' => 'terms'];

            $datagg = json_decode(json_encode($aggs), FALSE);

            $data = ['indexName' => 'products', 'pageNumber' => '0', 'size' => '12', 'searchTerm' => $searchTerm];
            $data = ['json' => $data];

            $res = consume($url, 'POST', $data);
            dd($res);

            $anotherData = ['indexName' => 'products', 'pageNumber' => '0', 'aggs' => $datagg, 'size' => '9', 'searchTerm' => $searchTerm];
            $anotherData = ['json' => $anotherData];
            $anotheres = consume($url, 'POST', $anotherData);

            $token = request()->get('_token');
            $mo = str_replace(' ','+', $searchTerm);
            $mobileLink = env('MOBILE_LINK') .'/product-search?_token='.$token.'/&query='.$mo;


            if ($res['status'] == 200 || $anotheres['status'] == 200) {

                $productsResponse = $res['data'];
                $designerResponse = $anotheres['aggregations'];
                $productAggregate = $res['aggregations'];
                $sub_title = isset($productsResponse[0]->subCategoryName) ? $productsResponse[0]->subCategoryName : '';
                $cat_title = isset($productsResponse[0]->categoryName) ? 'Search Results | ' . $productsResponse[0]->categoryName : '';
                if($Agent->isMobile()) {
                    return Redirect::to($mobileLink);
                } else {
                    return view('products.index', compact('productsResponse', 'categories', 'cat_title', 'sub_title', 'productAggregate', 'designerResponse','subcatId'));
                }

            } else {

                return view('errors.noNetwork');
            }
        }

        if (!empty(request('price')) && !empty(request('searchTerm'))) {

            $searchTerm = $request->query();
            $price = $searchTerm['price'];

            $fruits_ar = explode('-', $price);

            $min = $fruits_ar[0];

            $max = $fruits_ar[1];

            $category = request('searchTerm');

            $newdata = ["fieldName" => 'amount', "min" => (int)$min, "max" => (int)$max];

            $dataObj = json_decode(json_encode($newdata), FALSE);

            $url = env('GET_BASE_URL') . "/fashion/se/search";
            $datas = ['indexName' => 'products', 'pageNumber' => '0', 'size' => '12', 'searchTerm' => $category, 'ranges' => [$dataObj]];

            $newdatas = ['json' => $datas];

            $res = consume($url, 'POST', $newdatas);
            if ($res['status'] == 200) {
                $productsResponse = $res['data'];
                $sub_title = isset($productsResponse[0]->subCategoryName) ?  $productsResponse[0]->subCategoryName : '';
                $cat_title = isset($productsResponse[0]->categoryName) ?  $productsResponse[0]->categoryName : '';
                return view('products.index', compact('productsResponse', 'categories', 'sub_title', 'cat_title','subcatId'));
            }
        }

        if (!empty(request('value')) && !empty(request('searchTerm'))) {

            $search = $request->query();

            $searchValue = $search['searchTerm'];
            $name = request('subcategory');
            $searchTerm = $search['searchTerm'];

            $url = env('GET_BASE_URL') . "/fashion/se/search";

            $terms = ['fieldName' => $name, 'values' => [$searchValue]];

            $dataTerms = json_decode(json_encode($terms), FALSE);

            $data = ['indexName' => 'products', 'pageNumber' => '0', 'size' => '12', 'searchTerm' => $searchTerm, 'terms' => [$dataTerms]];

            $dataTerm = ['json' => $data];
//            dd($dataTerm);
            $res = consume($url, 'POST', $dataTerm);

            if ($res['status'] == 200) {
                $productsResponse = $res['data'];

                return view('products.index', compact('productsResponse', 'categories','subcatId'));
            }
//            return $redirectView;
        }

        if (!empty(request('prodelasticrat'))) {
            $allSearchTerm = $request->query();
            $searchTerm = $allSearchTerm['elasticSearch'];
            $rating = $allSearchTerm['prodelasticrat'];

            $newdata = ["fieldName" => 'productQualityRating', "min" => (int)$rating, "max" => (int)$rating];

            $dataObj = json_decode(json_encode($newdata), FALSE);

            $url = env('GET_BASE_URL') . "/fashion/se/search";
            $datas = ['indexName' => 'products', 'pageNumber' => '0', 'size' => '9', 'searchTerm' => $searchTerm, 'ranges' => [$dataObj]];

            $newdatas = ['json' => $datas];

            $res = consume($url, 'POST', $newdatas);

            if ($res['status'] == 200) {
                $productsResponse = $res['data'];
                $sub_title = isset($productsResponse[0]->subCategoryName) ?  $productsResponse[0]->subCategoryName : '';
                $cat_title = isset($productsResponse[0]->subCategoryName) ?  $productsResponse[0]->categoryName : '';
                return view('products.index', compact('productsResponse', 'categories', 'sub_title', 'cat_title','subcatId'));
            }
        }

        //old code here ---- don't remove
        /* if(!empty(request('query'))){
             $query = request('query');
             $url = env('GET_BASE_URL')."/fashion/product/$query/searchproduct";
             $res = consume($url,'GET');
              /*$url2 = getBaseUrl()."/fashion/product/$query/searchproductsbysub";
               $res2 = consume($url2, 'GET');*/

        /*  if($res['status'] == 0){
              $productsResponse = $res['data']->result;
              dd($productsResponse);
              return view('products.index', compact('productsResponse', 'categories'));

          }else{
              displayError($res, 'products.index');
          }
      }*/

        if (!empty(request('price')) || !empty(request('name')) || !empty(request('prodrat')) || !empty(request('desrat'))) {

            if (!empty(request('price'))) {
                $price = explode('-', request('price'));
            }

            $url = env('GET_BASE_URL') . "/fashion/product/filter";
            $data = [
                'page' => '0', 'size' => '16',
                'subCategoryId' => (!empty($subcatId)) ? $subcatId  : 0,
                'fromPrice' => (!empty(request('price'))) ? $price[0] : '',
                'toPrice' => (!empty(request('price'))) ? $price[1] : '',
                'categoryId' => (!empty(request('cat'))) ? request('cat') : 0,
                'productQualityRating' => (!empty(request('prodrat'))) ? request('prodrat') : '',
                'productName' => (!empty(request('name'))) ? request('name') : '',
                'designerRating' => (!empty(request('desrat'))) ? request('desrat') : ''
            ];

            $data = ['json' => $data];
//            dd($data);
            $res = consumeWithoutToken($url, 'POST', $data);

            if ($res['status'] == 0) {
                $productsResponse = $res['data'];
                $cat_title = '';
                $sub_title = '';
                return view('products.index', compact('productsResponse', 'categories', 'cat_title', 'sub_title'));
            } else {
                return $res;
            }
        }

        //products
        $subcatId = $request->route('subCatId');

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getproductsbysub";

        $data = ['page' => '0', 'size' => '16', 'subcategoryId' => $subcatId];
        $data = ['json' => $data];

        $res = consume($url, 'POST', $data);
        if ($res['status'] == 0) {
            $productsResponse = $res['data'];
            $sub_title = isset($productsResponse[0]->subCategoryName) ? $productsResponse[0]->subCategoryName : '';
            $cat_title = isset($productsResponse[0]->categoryName) ? $productsResponse[0]->categoryName : '';
            $mobile = env('MOBILE_LINK') . "/fashion/products?subCategoryId=".$subcatId."&categoryId=";
            if($Agent->isMobile()) {
                return Redirect::to($mobile);
            } else {
                return view('products.index', compact('productsResponse', 'categories', 'cat_title', 'sub_title', 'productAggregate', 'designerResponse', 'subcatId'));
            }
        } else {
            return view('products.index', compact('productsResponse', 'categories', 'cat_title', 'sub_title','subcatId'));
//            return $redirectView;
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMoreSliderElasticProduct()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter

        $url = env('GET_BASE_URL') . "/fashion/se/search";

        return consume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMoreElasticProducts()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/se/search";
        return consume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMoreProducts()
    {
        $redirectView = view('errors.noNetwork');
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/product/getproductsbysub";
        $res = consume($url, 'POST', $data);
        if ($res['status'] == 0) {
            return $productsResponse = $res['data'];
        } else {
            return $redirectView;
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMoreFilterProducts()
    {
        $redirectView = view('errors.noNetwork');
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/product/filter";
        $res = consume($url, 'POST', $data);
        if ($res['status'] == 0) {
            return $productsResponse = $res['data'];
        } else {
            return $redirectView;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function showDetails(Request $request)
    {
        $Agent = new Agent();

        $currency = env('CURRENCY');

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getfeaturedproducts";
        $data = ['size' => '30', 'page' => '0'];
        $data = ['json' => $data];
        $features = consume($url, 'POST', $data);

        $pr = request('product');

        $prPosition = strrpos($pr, '-');

        $productId = substr($pr, $prPosition + 1);

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "$productId/$currency/getproductbyid/review";
        $res = consume($url, 'GET');

        $url = env('GET_BASE_URL') . "/fashion/getuserdetails";
        $userDetail = consume($url, 'GET');

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "$productId/getproductbyid/getuserreview";
        $userRev = consume($url, 'GET');

        $url = env('GET_BASE_URL') . "/fashion/customization/$productId/getmandatorymeasurements";
        $userMandatoryMeasure = consume($url, 'GET');

        $value = $request->session()->get('userToken');
        //dd($res);
        if ($res['status'] == 0 && $features['status'] == 0) {
            $resp2 = $res['data'];
//            dd($resp2);
            $features = $features['data'];
            $userDetails = $userDetail['data'];
            $userRevres = $userRev['data'];
            //get mobile URL
            $link_m = str_slug($resp2->name) . '-' . $resp2->id;

            $page = env('MOBILE_LINK') . "/fashion/products/" . $link_m;

            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('products.show-details', compact('resp2', 'features', 'value', 'userDetails', 'userRevres'));
            }
        } else {
            return view('errors.noNetwork');
        }

    }

    /**
     * @var $allfeatures
     * Get all Features products Another Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllFeatureProducts()
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/fashion/featured";

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getfeaturedproducts";
        $data = ['size' => '280', 'page' => '0'];
        $data = ['json' => $data];
        $resp = consume($url, 'POST', $data);

        if ($resp['status'] == 0) {
            $allfeatures = $resp['data'];
        } else {
            return view('products.all-features', compact('resp'));
        }
        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('products.all-features', compact('allfeatures'));
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMoreFeatureProducts()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getfeaturedproducts";

        $data = ['json' => $data];
        $resp = consume($url, 'POST', $data);

        return $resp;
    }

    /**
     * @var $allNewArrival
     * Get all Features products Another Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllNewArrival()
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/fashion/new";

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getnewproducts";
        $data = ['size' => '280', 'page' => '0', 'currency' => 'GBP'];
        $data = ['json' => $data];

        $resp = consume($url, 'POST', $data);
        //dd($resp);

        if ($resp['status'] == 0) {
            $newArrival = $resp['data'];
        } else {
            return view('products.new-arrival', compact('resp'));
        }
        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('products.new-arrival', compact('newArrival'));
        }
    }

    /**
     * @var $allBespoke
     * Get all Features products Another Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllBespokeProducts()
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/bespoke";

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getbespokeproducts";

        $data = ['size' => '500', 'page' => '0'];
        $data = ['json' => $data];
        $resp = consume($url, 'POST', $data);
        if ($resp['status'] == 0) {
            $allBespoke = $resp['data'];
        } else {
            return view('products.all-bespoke', compact('allBespoke'));
        }
        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('products.all-bespoke', compact('allBespoke'));
        }
    }

    /**
     * @var $allBespoke
     * Get all Features products Another Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllBespokeProductsByCat()
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/fashion/bespoke";
        if (is_numeric(request('cat'))) {
            $catId = request('cat');
            if ($catId == '1') {
                $title = 'Men Bespoke';
            } else {
                $title = 'Women Bespoke';
            }
            $sub_url = env('GET_BASE_URL') . env('PRODUCT_API') . "getbespokeproductsbycat";
            $data = ['page' => '0', 'size' => '500', 'categoryId' => $catId];
            $data = ['json' => $data];
            $resp = consume($sub_url, 'POST', $data);
            if ($resp['status'] == 0) {
                $allBespokeCat = $resp['data'];
            } else {
//                return $allBespokeCat = '';
            }
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('products.bespoke-cat', compact('allBespokeCat', 'title'));
            }
        }
//        return view('products.bespoke-cat', compact('allBespokeCat'));
    }

    /**
     * Pre Order Bespoke Page
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokePreOrder()
    {
        $url = env('GET_BASE_URL') . env('USER_CUSTOMIZATION_API') . "getusermeasurements";
        $urls = env('GET_BASE_URL') . "/fashion/bespokestyle/getbespokestyles";
        $data = ['size' => '10', 'page' => '0'];
        $data = ['json' => $data];
        $bs = consume($urls, 'POST', $data);
        $m = consume($url, 'GET');//consume api
        // dd($m);
        $categories = getCategories();

        if (request('searchTerm')) {
            $req = request('searchTerm');
            $body = ['filterKey' => $req];
            $data = ['json' => $body];

            $url = env('GET_BASE_URL') . "/fashion/bespokestyle/filter";
            $resp = consume($url, 'POST', $data);

            if ($resp['status'] === '00') {
                $bespokeImageBanks = $resp['data'];
                return view('products.bespoke-preorder', compact('bespokeImageBanks', 'categories'));
            }
        }


        if (session()->has('userToken')) {
            if ($m['status'] == 0 || $bs['status'] == 0) {
                $measurements = $m['data'] ? $m['data'] : [];
                $bespokeImageBanks = $bs['data'];

                return view('products.bespoke-preorder', compact('measurements', 'bespokeImageBanks','categories'));
            } else {
                $measurements = array();
                $bespokeImageBanks = [];
                return view('products.bespoke-preorder', compact('measurements', 'bespokeImageBanks','categories'));
            }
        } else {
            $bespokeImageBanks = isset($bs['data']) ? $bs['data'] : [];
            return view('products.bespoke-preorder', compact('bespokeImageBanks', 'categories'));
        }


    }

    /**
     * Pre Order Bespoke Successful Page
     */
    public function bespokeOrderSuccess()
    {
        if (session()->has('userToken')) {
            return view('products.bespoke-successful');
        } else {
            return redirect(route('getAllBespoke'));
        }
    }

    /**
     * Process Bespoke request API
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function processRequest()
    {
        $body = json_decode(request()->getContent(), true);
        $url = env('GET_BASE_URL') . env('USER_BESPOKE_ORDER') . 'add';
        return $url;
        $data = ['json' => $body];
        return consume($url, 'POST', $data);
        //return $resp;
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function showMoreCategory()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $sub_url = env('GET_BASE_URL') . env('PRODUCT_API') . "getbespokeproductsbycat";
        $resp = consume($sub_url, 'POST', $data);
        return $resp;
    }

    /**
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchForImage()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/bespokestyle/filter";
        $resp = consume($url, 'POST', $data);
        if($resp['status'] === '0'){
            $bespokeImageBanks = $resp['data'];
            return view('products.bespoke-preorder', compact('bespokeImageBanks'));
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function categoryName(){
        $body = json_decode(request()->getContent(), true);
        $id =  $body['categoryId'];
        $url = env('GET_BASE_URL') . "/fashion/product/${id}/getcategory";
        $resp = consume($url, 'GET');
        return $resp;
    }

    /**
     * @var $allBespoke
     * Get all Features products Another Page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function stylesCatalogue()
    {
        $Agent = new Agent();
        $page = env('MOBILE_LINK') . "/bespoke";

        $url = env('GET_BASE_URL') . env('USER_BESPOKE_STYLE'). "getbespokestyles";

        $data = ['size' => '500', 'page' => '0'];
        $data = ['json' => $data];
        $resp = consume($url, 'POST', $data);
        if ($resp['status'] == 0) {
            $productsResponse = $resp['data'];
        } else {
            return view('products.style-catalogue', compact('productsResponse'));
        }
        if ($Agent->isMobile()) {
            return Redirect::to($page);
        } else {
            return view('products.style-catalogue', compact('productsResponse'));
        }
    }
}
