<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TagController extends Controller
{
    //
    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addTag(){
        $body = json_decode(request()->getContent(), true);     
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL')."/fashion/product/addTag";//api
        return $res = adminConsume($url,'POST', $data);
    }
}
