<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuditorController extends Controller
{
    //
    public function dashboard()
    {
        if (session()->has('auditortoken')) {
            $url = env('GET_BASE_URL') . env('AUDITOR_ORDER') . "getitemsdueforpayment";

            $data = [
                'page' => '0',
                'size' => '1000'
            ];
            $data = ['json' => $data];
            $res = auditorConsume($url, 'POST', $data);
            if ($res['status'] == '0') {
                $orders = $res['data'];
                $order = count($orders);
                //dd($order);
                return view('auditor.dashboard', compact('order'));
            }

        }

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllOrder()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('auditortoken')) {
            $url = env('GET_BASE_URL') . env('AUDITOR_ORDER') . "getitemsdueforpayment";
            // dd($url);
            $data = [
                'page' => '0',
                'size' => '20'
            ];
            $data = ['json' => $data];
            $res = auditorConsume($url, 'POST', $data);
            if ($res['status'] == '0') {
                $orders = $res['data'];
                return view('auditor.orders.all', compact('orders'));
            }
        } else {
            return $redirectView;
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function exportAuditOrder()
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('auditortoken')) {
            $url = env('GET_BASE_URL') . env('AUDITOR_ORDER') . "getitemsdueforpayment";
            // dd($url);
            $data = [
                'page' => '0',
                'size' => '20'
            ];
            $data = ['json' => $data];
            $res = auditorConsume($url, 'POST', $data);
            if ($res['status'] == '0') {
                $orders = $res['data'];
                return view('auditor.orders.order-audit-export', compact('orders'));
            }
        } else {
            return $redirectView;
        }
    }

}
