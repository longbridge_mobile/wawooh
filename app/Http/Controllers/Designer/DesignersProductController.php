<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Jerry\JWT\JWT;

class DesignersProductController extends Controller
{

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addDesignerNewProduct()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/product/addproduct";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addNewBespokeProduct()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/bespoke/add";

        $res = designerConsume($url, 'POST', $data);

        return $res;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBespokeProduct()
    {
        $storeName = getDesignerStoreName();
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/bespoke/NGN/get_designer_bespoke_styles";
        $data = ['init' => '0', 'size' => '30'];
        $data = ['json' => $data];
        $res = designerConsume($url, 'POST', $data);
        $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
        $resD = designerConsume($urlProfile, 'GET');
        $designer = $resD['data'];
        if ($res['status'] === '00') {
            $bespokeProducts = $res['data'];
            return view('designers.get-bespoke-product', compact('storeName', 'bespokeProducts', 'designer'));
        } else {
            return view('designers.get-bespoke-product', compact('storeName', 'designer'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUserUploadStylesForDesigner()
    {
        $storeName = getDesignerStoreName();
        $url = env('GET_BASE_URL') . "/fashion/designer/uploadedstyles/get_new_styles";
        $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
        $resD = designerConsume($urlProfile, 'GET');
        $designer = $resD['data'];
        $data = ['init' => '0', 'size' => '30'];
        $data = ['json' => $data];
        $res = designerConsume($url, 'POST', $data);

        if ($res['status'] === '200') {
            // dd($res['data']);
            $resp = $res['data'];

            return view('designers.get-upload-user-styles', compact('resp', 'storeName', 'designer'));

        }
    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUploadedStylesForDesigner()
    {
        if (session()->has('designerToken')) {
            $url = env('GET_BASE_URL') . "/fashion/designer/uploadedstyles/get_new_styles";
            $data = ['init' => '0', 'size' => '30'];
            $data = ['json' => $data];

            $res = designerConsume($url, 'POST', $data);
//             dd($res);
        }
    }


    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSingleUploadedStylesForDesigner()
    {
        if (session()->has('designerToken')) {
            $url = env('GET_BASE_URL') . "/fashion/designer/uploadedstyles /{userBespokeStyleUploadId}/get_one_styles";
            $data = ['init' => '0', 'size' => '30'];
            $data = ['json' => $data];

            $res = designerConsume($url, 'POST', $data);
//             dd($res);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function products(Request $request)
    {
        $categories = getCategories();
        //get the store Name in the url

        $designer = str_replace(['-', '_'], ' ', request('designer'));
        // $designer = strtolower(request('designer'));
//        dd($designer);

        //check if user is logged in as a designer
        if (session()->has('designerToken')) {

            //get the logged in designer store name
            $storeName = trim(str_replace("'", '', strtolower(getDesignerStoreName())));

            //check if logged in designers store name and store name in url are the same
            //if the same, get the logged in designer products by token
            //else get the details by store Name
            if ($storeName) {


                $data = getLastSixMonths();
                $data = ['json' => $data];
                $urlProfile = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
                $resD = designerConsume($urlProfile, 'GET');

                $designer = $resD['data'];

                $url = env('GET_BASE_URL') . "/fashion/secure/designer/product/getdesignerproducts";

                $res = designerConsume($url, 'GET');

                $designerProducts = [];

                if ($res['status'] === '00') {
                    $designerProducts = $res['data'];
                }


                $totalOrder = [];

                $currentPage = LengthAwarePaginator::resolveCurrentPage();

                $collection = new Collection($designerProducts);

                $per_page = 10;

                $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();

                $totalOrder = new LengthAwarePaginator($currentPageResult, count($collection), $per_page);

                $totalOrder->setPath($request->url());
                $subcat = request('sc');
                //$subcat = request('sc', 4);
                //check if designer wants a subcategory
                if (is_numeric($subcat)) {
                    //get designer products by subcategory
                    $url = env('GET_BASE_URL') . "/fashion/product/getdesignerproductsbysub";
                    $data = ['page' => '0', 'size' => '5', 'subcategoryId' => $subcat];
                    $data = ['json' => $data];
                    $res = designerConsume($url, 'POST', $data);
                    if ($res['status'] == 0) {
                        $designerProducts = $res['data'];
                        //dd($designerProducts);

                        $currentPage = LengthAwarePaginator::resolveCurrentPage();

                        $collection = new Collection($designerProducts);

                        $per_page = 20;

                        $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();

                        $totalOrder = new LengthAwarePaginator($currentPageResult, count($collection), $per_page);

                        $totalOrder->setPath($request->url());

                        //dd($designerProducts);

                        return view('designers.products', compact('designerProducts', 'categories', 'storeName', 'res', 'totalOrder'));

                    } else {
                        return $res;
                    }

                }

                return view('designers.products', compact('designerProducts', 'categories', 'designer', 'storeName', 'totalOrder'));

            } else {
                $designerId = str_replace('-', ' ', request('designer'));
                //get products by designer id.
                $url = env('GET_BASE_URL') . "/fashion/secure/product/$designerId/getdesignerproducts";
                $res = consumeWithoutToken($url, 'GET');
            }
        } else {
            return redirect(route('designersLandingPage'));

            //$designer = str_replace('-', ' ', request('profile'));
            //get designer products by id
        }
        //get designers product by category
        //category
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */


    public function productBespokeDetails(Request $request)
    {
        if (session()->has('designerToken')) {
            $productId = $request->route('pid');
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "bespoke/${productId}/NGN/getbespoke";

            $product = designerConsume($url, 'GET');
            if ($product['status'] === '00') {
                $storeName = getDesignerStoreName();
                $product = $product['data'];

                $reqMes = json_decode(json_decode($product->requiredMeasurements));

                return view('designers.bespokeProductDetails', compact('product', 'storeName', 'reqMes'));
            }
        }
    }

    public function uploadStyleDetail(Request $request)
    {
        if (session()->has('designerToken')) {
            $userBespokeStyleUploadId = $request->route('pid');
            $url = env('GET_BASE_URL') . "/fashion/designer/uploadedstyles/${userBespokeStyleUploadId}/get_one_style";
            $res = designerConsume($url, 'GET');
            if ($res['status'] === '200') {
                $resp = $res['data'];
                $storeName = getDesignerStoreName();

                return view('designers.userUploadedStyleDetails', compact('resp', 'storeName'));
            }
        }
    }

    public function placeBidForUserStyle(Request $request)
    {
        if (session()->has('designerToken')) {
            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body];
            $url = env('GET_BASE_URL') . "/fashion/designer/uploadedstyles/place_bid";
            return $res = designerConsume($url, 'POST', $data);
        }
    }


    public function convertToBespoke(Request $request)
    {
        if (session()->has('designerToken')) {
            $storeName = getDesignerStoreName();
            $productId = $request->route('pid');
            $productName = $request->route('productName');
            $prodName = str_replace('-', ' ', $productName);

            return view('designers.convert-rtw-to-bespoke-product', compact('storeName', 'productId', 'prodName'));
        }
    }


    public function productDetails(Request $request)
    {
        $currency = env('CURRENCY');
        if (session()->has('designerToken')) {
            $productId = $request->route('pid');
//            $url = env('GET_BASE_URL') . env('PRODUCT_API')."${productId}/$currency/getproductbyid";
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "product/${productId}/designer/getproductbyid";
            $product = designerConsume($url, 'GET');

            $data = getLastSixMonths();
            $data = ['json' => $data];


            $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $resD = designerConsume($urlProfile, 'GET');


            $designer = $resD['data'];
            if ($product['status'] == 0) {
                $product = $product['data'];
                if ($product->bespokeProductDTO !== null) {
                    $reqMes = json_decode(json_decode($product->bespokeProductDTO->mandatoryMeasurements));
                } else {
                    $reqMes = [];
                }
                $storeName = getDesignerStoreName();

                return view('designers.productDetails', compact('product', 'storeName', 'designer', 'reqMes'));
            } else {
                return $product;
            }
        }
        return redirect(route('designersLandingPage'));
    }

    public function editBespokeProduct(Request $request)
    {
        if (session()->has('designerToken')) {
            $productId = $request->route('pid');

            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "bespoke/${productId}/NGN/getbespoke";

            $res = designerConsume($url, 'GET');

            $categories = getCategories();

            if ($res['status'] === '00') {
                $product = $res['data'];
                $storeName = getDesignerStoreName();
                $productMeasure = json_decode(json_decode($product->requiredMeasurements));
                return view('designers.bespokeEdit', compact('product', 'categories', 'storeName', 'productMeasure'));

            }
        }
    }


    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editProduct(Request $request)
    {
        if (session()->has('designerToken')) {
            $productId = $request->route('pid');
            $currency = env('CURRENCY');
//            $url = env('GET_BASE_URL') . env('PRODUCT_API')."${productId}/$currency/getproductbyid";
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "product/${productId}/designer/getproductbyid";
            $res = designerConsume($url, 'GET');

            $data = getLastSixMonths();
            $data = ['json' => $data];

            $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $resD = designerConsume($urlProfile, 'GET');

            $designer = $resD['data'];
            if ($res['status'] == 0) {
                $product = $res['data'];
                if ($product->bespokeProductDTO !== null) {
                    $productMeasure = json_decode(json_decode($product->bespokeProductDTO->mandatoryMeasurements, true), true);
                } else {
                    $productMeasure = [];
                }

                $datamasure = ['json' => $productMeasure];

                $getMeasureFromValueUrl = env('GET_BASE_URL') . "/fashion/measurementtypes/converttotypes";
                $getMeasure = env('GET_BASE_URL') . "/fashion/measurementtypes/all";
                $reqM = designerConsume($getMeasureFromValueUrl, 'POST', $datamasure);
                $resM = designerConsume($getMeasure, 'GET');

                $resM = $resM['data'] ? $resM['data'] : [];

                $reqM = [];

                $storeName = getDesignerStoreName();
                $categories = getCategories();

                $sizes = getSizes();
                $arraySize = [];
                foreach ($sizes as $size) {
                    array_push($arraySize, $size->name);
                }

                if (isset($productMeasure) && is_array($productMeasure) && sizeof($productMeasure) > 0) {
                    $storeName = getDesignerStoreName();
                    $categories = getCategories();

                    return view('designers.edit', compact('categories', 'storeName', 'product', 'designer', 'productMeasure', 'sizes', 'arraySize', 'reqM', 'resM'));

                } else {
                    return view('designers.edit', compact('categories', 'storeName', 'product', 'designer', 'sizes', 'reqM', 'arraySize', 'resM'));

                }
            } else {
                return $res;
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editProductApi()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . 'updateproduct';
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editProductImagesApi()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "updateproductimage";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editArtworkApi()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "updateproductartwork";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeArtworkImage()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "deleteartworkimage";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeProductImage()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "deleteartworkimage";

        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeFabricImage()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "deletematerialimage";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * Remove a product material Image
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeMaterialImage()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "deletematerialimage";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editMaterialApi()
    {
        $url = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "updateproductmaterial";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function productsApi()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getdesignerproductsbysub";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * This function for a designer to add a new product with Store Name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addNewProduct()
    {
        $redirectView = view('errors.noNetwork');
        $data = getLastSixMonths();
        $data = ['json' => $data];
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
        $getMeasure = env('GET_BASE_URL') . "/fashion/measurementtypes/all";
        $resD = designerConsume($url, 'GET');
        //dd($resD);

        $resM = designerConsume($getMeasure, 'GET');
        // dd($resM);

        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getcategories";
        $res = designerConsume($url, 'GET');

        $urlSizes = env('GET_BASE_URL') . "/fashion/size/getsizes";
        $sizeRes = designerConsume($urlSizes, 'GET');

        if (isset($sizeRes['data'])) {
            $sizes = $sizeRes['data']->data;
        } else {
            $sizes = [];
        }

        if (isset($res['data']) || isset($resM['data'])) {
            $categories = $res['data'];
            $reqM = $resM['data'];
        } else {
            return $redirectView;
        }
        if (session()->has('designerToken')) {
            if (isset($resD['data'])) {
                $designer = $resD['data'];
            } else {
                return $redirectView;
            }
            $storeName = $designer->storeName;
        } else {
            return redirect(route('designersLandingPage'));
        }

        //   dd($reqM);
        return view('designers.add', compact('categories', 'storeName', 'sizes', 'designer', 'reqM'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSubCatBycat()
    {
        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getsubcatbyproducttype";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStyleBySubCat()
    {
        $body = json_decode(request()->getContent(), true);
        $id = $body['id'];
        $url = env('GET_BASE_URL') . env('PRODUCT_API') . "{$id}/getstyles";
        return designerConsume($url, 'GET');
    }
}
