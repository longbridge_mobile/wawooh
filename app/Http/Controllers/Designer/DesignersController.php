<?php

namespace App\Http\Controllers\Designer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class DesignersController extends Controller
{
    /**
     * @param Request $req
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $req)
    {
        if (session()->has('designerToken')) {
            return redirect(route('designer.dashboard'));
        }
        return view('index');
    }


    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMoreDesignerProduct()
    {
        $body = json_decode(request()->getContent(), true);
        $designerId = $body['id'];
        $url = env('GET_BASE_URL') . "/fashion/product/{$designerId}/getdesignerproducts";
        $data = ['json' => $body];
        $resp = consume($url, 'POST', $data);
        return $resp;

        //return $url;
        //$data = ['json' => $body];
        //return $data;

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dashboard()
    {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";

            $urlPro = env('GET_BASE_URL') . env('DESIGNER_PRODUCT_API') . "getdesignerproducts";

            $resTotal = designerConsume($urlPro, 'GET');

            if (isset($resTotal['data'])) {
                $prodTotal = $resTotal['data'];
            } else {
                return view('designers.dashboard', compact('storeName', 'prodTotal', 'designer'));
                //return view('errors.noNetwork');
            }
            $res = designerConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
                    $salesChart = $designer->salesChart;
                    $label = array();
                    $sales = array();
                    for ($i = 0; $i < count($salesChart); $i++) {
                        array_push($label, \DateTime::createFromFormat('!m', $salesChart[$i]->month)->format('F'));
                        array_push($sales, $salesChart[$i]->amount);
                    }

                    $storeName = $designer->storeName;
//                    dd($storeName);
                } else {
                    $designer = null;
                }
                //$storeName = getDesignerStoreName();

                return view('designers.dashboard', compact('storeName', 'designer', 'sales', 'label', 'prodTotal'));
            } else {
                return view('designers.dashboard');
//                return view('errors.noNetwork');
//                 return displayError($res, 'designers.dashboard');
            }
            return view('designers.dashboard', compact('storeName', 'designer'));
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $request
     */
    public function view(Request $request)
    {

        $Agent = new Agent();
        //check if user is logged in as a designer
        $categories = getCategories();
        $pp = strrpos(request('profile'), '-');

        $urlRequest = (substr(request('profile'), 0, $pp));

        $designerInUrl = str_replace('-', ' ', $urlRequest);

        //get designer by store name
        $url = env('GET_BASE_URL') . "/fashion/designer/getdesignerbystorename/$designerInUrl";
        $res = consumeWithoutToken($url, 'GET');

        if (isset($res['data'])) {
            $designer = $res['data'];
//            dd($designer);
        } else {
            return $designer = $res['data'];
        }


        // dd($designer);

        $mName = str_slug($designer->storeName);

        $desId = $designer->id;
        $combineName = $mName . '-' . $desId;

        $pr = request('designerId');

        $prPosition = strrpos($pr, '-');

        $designerId = substr($pr, $prPosition + 1);

        $page = env('MOBILE_LINK') . '/designer/' . $combineName;

        if ($Agent->isMobile()) {
            return Redirect::to($page);
        }

        if (session()->has('designerToken')) {

            //get the logged in designer storename
            $storeName = str_replace("'", '', strtolower(getDesignerStoreName()));
            //get the store Name in the url
            $designerInUrl = str_replace("'", '', strtolower(request('profile')));

            //check if logged in designers store name and store name in url are the same
            //if the same, get the logged in designer products by token
            //else get the details by store Name

            if ($storeName == $designerInUrl) {

                $url = env('GET_BASE_URL') . env('PRODUCT_API') . "$designerId/getdesignerproducts";
                $res = designerConsume($url, 'GET');

                $page = env('MOBILE_LINK') . '/designer/' . $designerInUrl;


                if (($res['status'] == '00')) {
                    $products = $res['data'];

                    if ($Agent->isMobile()) {
                        return Redirect::to($page);
                    } else {
                        return view('designers.view', compact('designer', 'designerInUrl', 'products'));
                    }
                } else {
                    $designer = null;
                }
            } else {
                $url = env('GET_BASE_URL') . "/fashion/product/$designerId/getdesignerproducts";
                $res = designerConsume($url, 'GET');

                if ($res['status'] == '00') {
                    $products = $res['data'];
                    if ($Agent->isMobile()) {
                        return Redirect::to($page);
                    } else {

                        return view('designers.view', compact('designer', 'designerInUrl', 'products'));
                    }
                }

                $designerInUrl = str_replace('-', ' ', request('profile'));
                //get the designer by store name.

            }


            //return view('designers.view', compact('designer', 'designerInUrl'));

        } else {

            if (request('dg') != null && request('subcatId') != null) {
                //get subcat products of designer
                $designerId = request('dg');
                $subCatId = request('subcatId');
                $data = [
                    'page' => '0',
                    'size' => '10',
                    'subcategoryId' => $subCatId,
                    'designerId' => $designerId
                ];
                $data = ['json' => $data];

                $url = env('GET_BASE_URL') . "/fashion/product/getdesignerproductsbysub";
                $res = consumeWithoutToken($url, 'POST', $data);
                if ($res['status'] == 0) {
                    $products = $res['data'];
//                    dd($products);
                    if ($Agent->isMobile()) {
                        return Redirect::to($page);
                    } else {
                        return view('designers.view', compact('designer', 'designerInUrl', 'categories', 'products'));
                    }
                }
            }
        }


        $datad = getLastSixMonths();
        $datad = ['json' => $datad];

        $urld = env('GET_BASE_URL') . env('PRODUCT_API') . "$designerId/getdesignerproducts";
        $resd = designerConsume($urld, 'GET');


        $req = substr($request->path(), 9);
        $realReq = ucfirst($req);


        if ($resd['status'] == '00') {
            $products = $resd['data'];
            if ($Agent->isMobile()) {
                return Redirect::to($page);
            } else {
                return view('designers.view', compact('designer', 'categories', 'designerInUrl', 'products'));
            }
        }
        //dd($designer);

        return view('designers.view', compact('designer', 'categories', 'designerInUrl', 'storeName'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orders(Request $request)
    {
        $redirectView = view('errors.noNetwork');
        if (session()->has('designerToken')) {
            $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/getdesignerorders";
            $cancelUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getcancelledorders";
            $activeUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getactiveorders";
            $pendingUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getpendingorders";
            $completedUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/getcompletedorders";
            $bespokeOrderUrl = env('GET_BASE_URL') . "/fashion/secure/designer/order/get_designer_bespoke_request_orders";

            $pendingRes = designerConsume($pendingUrl, 'GET');
            $completedRes = designerConsume($completedUrl, 'GET');
            $cancelRes = designerConsume($cancelUrl, 'GET');
            $res = designerConsume($url, 'GET');
            $data = [
                'init' => '0',
                'size' => '20'
            ];

            $data = ['json' => $data];
            $bespokeRes = designerConsume($bespokeOrderUrl, 'POST', $data);

            $activeRes = designerConsume($activeUrl, 'GET');
//
            if ($res['status'] == 0 || $pendingRes['status'] == 0 || $completedRes['status'] == 0 || $cancelRes['status'] == 0 || $activeRes['status'] == 0
                || $bespokeRes['status'] == 0) {
                $activeOrders = array();
                $completedOrders = array();
                if (isset($res['data'])) {
                    $orders = $res['data'];
                } else {
                    return view('errors.noNetwork');
                }
                $pendingOrders = $pendingRes['data'];
                $bespokeorders = $bespokeRes['data'];

                $completedsOrders = $completedRes['data'];
                $cancelOrders = $cancelRes['data'];
                $activesOrders = $activeRes['data'];
                // dd($bespokeorders);
                foreach ($orders as $order) {
                    if (strtolower($order->deliveryStatus) == 'op') {
                        array_push($activeOrders, $order);
                    } else if (strtolower($order->deliveryStatus) == 'co') {
                        array_push($completedOrders, $order);
                    }
                }

                $completedsOrders;
                //dd($cancelOrders);

                $totalOrder = [];
                $currentPage = LengthAwarePaginator::resolveCurrentPage();

                $collection = new Collection($orders);

                $per_page = 20;

                $currentPageResult = $collection->slice(($currentPage - 1) * $per_page, $per_page)->all();

                $totalOrder = new LengthAwarePaginator($currentPageResult, count($collection), $per_page);

                $totalOrder->setPath($request->url());

                //dd($totalOrder);
                $data = getLastSixMonths();
                $data = ['json' => $data];

                $urlProfile = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
                $resD = designerConsume($urlProfile, 'GET');

                $designer = $resD['data'];

                $storeName = getDesignerStoreName();

                return view('designers.orders', compact('orders', 'designer', 'storeName', 'activeOrders', 'completedOrders', 'totalOrder', 'bespokeorders', 'pendingOrders', 'activesOrders', 'cancelOrders', 'completedsOrders'));
            } else {
                return $redirectView;
            }
        }

        return redirect(route('designersLandingPage'));

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function orderDetails()
    {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];

            $urlProfile = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
            $resD = designerConsume($urlProfile, 'GET');

            $designer = $resD['data'];

            $storeName = $designer->storeName;

            $orderId = request('orderId');
            $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/{$orderId}/getorderitemdetails";
            $res = designerConsume($url, 'GET');
            if ($res['status'] == 0) {
                //return displayError($res);
                $order = $res['data'];
//                dd($order);
                $orderMeasurement = json_decode($order->measurement);
//                dd($orderMeasurement);
                return view('designers.orderDetails', compact('storeName', 'order', 'designer', 'orderMeasurement'));
                // return view('designers.orderDetails', compact('storeName', 'order'));
            }
        }
        return redirect(route('designersLandingPage'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeOrderDetails()
    {
        if (session()->has('designerToken')) {

            $orderNum = request('orderId');
            $storeName = request('designers');

            $data = [
                'init' => '0',
                'size' => '20'
            ];
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/{$orderNum}/get_one_bespoke_request_orders";
            $res = designerConsume($url, 'POST', $data);
            //  dd($res);

            if ($res['status'] === '00') {
                $bespokeOrderDetail = $res['data'];
                return view('designers.bespokeOrderDetails', compact('storeName', 'bespokeOrderDetail'));
            }
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function acceptOrder()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/order/updateorderitem";
        return $res = designerConsume($url, 'POST', $data);
    }

    public function acceptBespokeOrder()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/bespoke/order/completed_bespoke_order";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * this check if account is activate
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function accountStatus()
    {
        $email = request('email');
        $decodeEmail = base64_decode($email);
        return view('designers.account-status');
    }

    /**
     * this check if account is activate
     */
    public function registrationComplete()
    {
        $email = request('email');
        $decodeEmail = base64_decode($email);
        return view('designers.activation');
    }

    /**
     * Mail Configuration for Designer
     */
    public function mailConfigure()
    {

    }

    /**
     * Verify Token for Designer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function verifyDesignerToken()
    {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/validatetoken";
        $res = consume($url, 'POST', $data);
        if ($res['status'] == '00') {
            $designerToken = $res['data'];
            session(['designerToken' => $designerToken]);
            return json_encode(['status' => '00', 'Token verification successful', 'data' => $designerToken]);
        } else {
            return json_encode(['status' => '56', 'message' => 'Token verification failed, try again']);
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addDesignerNewProduct()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . "/fashion/secure/designer/product/addproduct";
        return designerConsume($url, 'POST', $data);
    }

    public function printBespoke()
    {
        return view('designers.bespoke-apply-print');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadImage()
    {
        if (session()->has('designerToken')) {
            $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $resD = designerConsume($urlProfile, 'GET');
            $designer = $resD['data'];
            $storeName = getDesignerStoreName();
            $categories = getCategories();

            $url = env('GET_BASE_URL') . env('DESIGNER_STYLES_API') . "getbespokestyles";
            $data = [
                'page' => '0',
                'size' => '20'
            ];

            $data = ['json' => $data];

            $styleBank = designerConsume($url, 'POST', $data);

            if ($styleBank['status'] === '00') {
                $styleBanks = $styleBank['data'];
//                dd($styleBanks);
                return view('designers.upload-image', compact('styleBanks', 'categories', 'designer', 'storeName'));

            }
        } else {
            return redirect(route('designersLandingPage'));
        }
    }

    /*  public function designerGetSizes(){
          $sizes = getSizes();
         return $sizes;
      }*/

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSubCatbyCatId()
    {
        $body = json_decode(request()->getContent(), true);
        $body = $body['id'];
        $url = env('GET_BASE_URL') . "/fashion/product/{$body}/getsubcategories";
        return designerConsume($url, 'GET');
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadImageToBank()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_STYLES_API') . "addbespokestyle";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeBiddingRequest(Request $request)
    {
        if (session()->has('designerToken')) {
            // dd(session('designerToken'));
            $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $resD = designerConsume($urlProfile, 'GET');
            $designer = $resD['data'];
            $storeName = getDesignerStoreName();
            $biddingApi = env('GET_BASE_URL') . env('DESIGNER_BESPOKE_REQUEST') . "bids";
            $bidding = designerConsume($biddingApi, 'GET');
            if ($bidding['status'] == '00') {
                $listBid = $bidding['data'];
                return view('designers.bespoke-orders', compact('designer', 'storeName', 'listBid'));
            } else {
                return view('designers.bespoke-orders', compact('designer', 'storeName'));
            }
        } else {
            return redirect(route('designersLandingPage'));
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addBespokeProduct()
    {
        if (session()->has('designerToken')) {
            $redirectView = view('errors.noNetwork');

            $storeName = getDesignerStoreName();
            $data = getLastSixMonths();
            $data = ['json' => $data];
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $getMeasure = env('GET_BASE_URL') . "/fashion/measurementtypes/all";
            $resD = designerConsume($url, 'GET');

            $resM = designerConsume($getMeasure, 'GET');


            $url = env('GET_BASE_URL') . env('PRODUCT_API') . "getcategories";
            $res = designerConsume($url, 'GET');

            $urlSizes = env('GET_BASE_URL') . "/fashion/size/getsizes";
            $sizeRes = designerConsume($urlSizes, 'GET');

            if (isset($sizeRes['data'])) {
                $sizes = $sizeRes['data']->data;
            } else {
                $sizes = [];
            }

            if (isset($res['data']) || isset($resM['data'])) {
                $categories = $res['data'];
                $reqM = $resM['data'];
            } else {
                return $redirectView;
            }

            if (isset($resD['data'])) {
                $designer = $resD['data'];
            } else {
                return $redirectView;
            }
            $storeName = $designer->storeName;


            return view('designers.add-bespoke-product', compact('categories', 'sizes', 'designer', 'reqM', 'storeName'));
        }
    }


    public function bespokeRequest()
    {
        if (session()->has('designerToken')) {
            $storeName = getDesignerStoreName();
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "bespoke/order/NGN/view_all_bespoke_style_request";
            $body = ['init' => 0, 'size' => 30];
            // dd($url);
            $data = ['json' => $body];
            $res = designerConsume($url, 'POST', $data);
            //dd($res);
            if ($res['status'] === '00') {
                $data = $res['data'];
                return view('designers.bespoke-request', compact('storeName', 'data'));
            }
        }

    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function availableManual()
    {
        $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
        $resD = designerConsume($urlProfile, 'GET');
        $designer = $resD['data'];
        $storeName = getDesignerStoreName();
        return view('designers.availableManual', compact('storeName','designer'));
    }

    /**
     * @param $designer
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDesignerBespokeDetail($designer, $id)
    {
        if (session()->has('designerToken')) {
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "bespoke/order/ngn/${id}/get_bespoke_request";
            $body = ['init' => 0, 'size' => 10];
            $data = ['json' => $body];
            $storeName = getDesignerStoreName();

            $res = designerConsume($url, 'POST', $data);

            if ($res['status'] === '00') {
                $data = $res['data'];
                //  dd($res);

                return view('designers.bespoke-request-detail', compact('storeName', 'data'));
            }

        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function makeQuote()
    {
        if (session()->has('designerToken')) {
            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "bespoke/order/make_quote";
            $body = json_decode(request()->getContent(), true);
            $data = ['json' => $body];//request parameter
            return $res = designerConsume($url, 'POST', $data);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function viewBespokeBidding(Request $request)
    {

        if (session()->has('designerToken')) {
            $urlProfile = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $resD = designerConsume($urlProfile, 'GET');
            $designer = $resD['data'];
            $storeName = getDesignerStoreName();
            $id = request('id');
            $biddingApi = env('GET_BASE_URL') . env('DESIGNER_BESPOKE_REQUEST') . $id . "/bids";
            $bidding = designerConsume($biddingApi, 'GET');
            if ($bidding['status'] == '00') {
                $listBidDes = $bidding['data'];
                $listBid = $listBidDes->bespokeOrderRequestDTO;
                return view('designers.bespoke-order-details', compact('designer', 'id', 'storeName', 'listBid', 'listBidDes'));
            } else {
                return view('designers.bespoke-order-details', compact('designer', 'storeName'));
            }
        } else {
            return redirect(route('designersLandingPage'));
        }
    }
}