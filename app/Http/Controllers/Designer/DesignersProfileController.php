<?php

namespace App\Http\Controllers\Designer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DesignersProfileController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function profile()
    {
        if (session()->has('designerToken')) {

            $data = getLastSixMonths();
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $res = designerConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
                    $storeName = $designer->storeName;
                    return view('designers.profile', compact('designer', 'storeName'));
                } else {
                    $designer = null;
                }
                return view('designers.profile', compact('designer', 'storeName'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'designers.profile');
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function editProfile()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];//request parameter
        $url = env('GET_BASE_URL') . "/fashion/designer/updatedesigner";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * Update Profile View and functions
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateProfile()
    {
        if (session()->has('designerToken')) {

            $data = getLastSixMonths();
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $res = designerConsume($url, 'GET');
            // dd($res);

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
//                    dd($designer);
                    $storeName = $designer->storeName;
                    return view('designers.update-profile', compact('designer', 'storeName'));
                } else {
                    $designer = null;
                }
                return view('designers.update-profile', compact('designer', 'storeName'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'designers.update-profile');
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateLogo()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body]; //request parameter
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "updatedesignerlogo";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateBanner()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body]; //request parameter
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "updatedesignerbanner";
        return $res = designerConsume($url, 'POST', $data);
    }

    /**
     * Change Password and Logout
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updatePassword()
    {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('USER_ACCOUNT_API') . "/editpassword";
        $res = designerConsume($url, 'POST', $data);

        if ($res['status'] == '00') {
            session()->forget('designerToken');
            return response()->json(['status' => '00', 'message' => 'Password Change Successfully']);
        } else {
            return response()->json(['status' => '99', 'message' => 'Not successfully']);
        }
    }

    /**
     * Change Password and Logout
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changeEmail()
    {
        $body = json_decode(request()->getContent(), true);

        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "/updateemailaddress";
        $res = designerConsume($url, 'POST', $data);

        if ($res['status'] == '00') {
            session()->forget('designerToken');
            return response()->json(['status' => '00', 'message' => 'Email Change Successfully']);
        } else {
            return response()->json(['status' => '99', 'message' => 'Something went wrong']);
        }
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updatePersonalInformation()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "updatepersonalinformation";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateBusinessInformation()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "updatebusinessinformation";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateAccountInformation()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "updateaccountinformation";
        return designerConsume($url, 'POST', $data);
    }

    /**
     * Controller for Bespoke application
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bespokeApplication()
    {
        if (session()->has('designerToken')) {
            $urlProfile = env('GET_BASE_URL') . "/fashion/secure/designer/getdesigner";
            $resD = designerConsume($urlProfile, 'GET');
            //$emailPlain = \request('email');
            $designer = $resD['data'];

//            $emailPlain = $designer->email;
//            $encodeEmail = base64_encode($emailPlain);
            $storeName = getDesignerStoreName();

            return view('designers.bespoke-apply', compact('designer', 'storeName'));
        } else {
            return redirect(route('designersLandingPage'));
        }
    }

    /**
     * Account Configuration for the designer
     * Strictly for logon designer
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function accountSettings()
    {
        if (session()->has('designerToken')) {
            $data = getLastSixMonths();
            $data = ['json' => $data];

            $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "getdesigner";
            $res = designerConsume($url, 'GET');

            if ($res['status'] == 0) {
                if (isset($res['data'])) {
                    $designer = $res['data'];
                    $storeName = $designer->storeName;
                    return view('designers.settings', compact('designer', 'storeName'));
                } else {
                    $designer = null;
                }

                return view('designers.settings', compact('designer', 'storeName'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'designers.profile');
            }
        }
        return redirect(route('designersLandingPage'));
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function changeDesignerEmail()
    {
        $url = env('GET_BASE_URL') . env('DESIGNER_ACCOUNT_API') . "updateemailaddress";
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        return designerConsume($url, 'POST', $data);
    }
}
