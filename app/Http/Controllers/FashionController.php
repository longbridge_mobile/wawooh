<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Agent\Agent;

class FashionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index()
    {
        if (!empty(request('price')) || !empty(request('name')) || !empty(request('prodrat')) || !empty(request('desrat'))) {

            if (!empty(request('price'))) {
                $price = explode('-', request('price'));

            }

            $url = env('GET_BASE_URL') . "/fashion/product/filter";
            $data = [
                'page' => '0', 'size' => '6',
                'subCategoryId' => (!empty(request('cat'))) ? request('cat') : '',
                'fromPrice' => (!empty(request('price'))) ? $price[0] : '',
                'toPrice' => (!empty(request('price'))) ? $price[1] : '',
                'productQualityRating' => (!empty(request('prodrat'))) ? request('prodrat') : '',
                'productName' => (!empty(request('name'))) ? request('name') : '',
                'designerRating' => (!empty(request('desrat'))) ? request('desrat') : ''
            ];

            $data = ['json' => $data];
            $res = consumeWithoutToken($url, 'POST', $data);
            if ($res['status'] == 0) {
                $productsResponse = $res['data'];
                return view('products.index', compact('productsResponse', 'categories'));
            } else {
                return view('errors.noNetwork');
//                return displayError($res, 'products.index');
            }
        }

        $Agent = new Agent();
        //mobile
        $page = env('MOBILE_LINK') . '/fashion';
        $currency = env('CURRENCY');
        //get frequent products
        $url = env('GET_BASE_URL') . "/fashion/product/getfreqboughtproducts/$currency";

        $frequents = consumeWithoutToken($url, 'GET');
        $url = env('GET_BASE_URL') . "/fashion/product/getfeaturedproducts/$currency";
        $data = ['size' => '12', 'page' => '0'];
        $data = ['json' => $data];
        //dd($data);
        $features = consume($url, 'GET', $data);

        $newProd = env('GET_BASE_URL') . "/fashion/product/getnewproducts";
        $data = ['size' => '12', 'page' => '0'];
        $data = ['json' => $data];
        $newProducts = consumeWithoutToken($newProd, 'POST', $data);

        //get top products
        $url = env('GET_BASE_URL') . "/fashion/product/gettopproducts/$currency";
        $topProducts = consumeWithoutToken($url, 'GET');

        if ($newProducts['status'] == 0 || $topProducts['status'] == 0 || $frequents['status'] == 0 || $features['status']) {

            $topProducts = count($topProducts['data']) > 0 ? $topProducts['data'] : [];
            $frequents = count($frequents['data']) > 0 ? $frequents['data'] : [];
            $features = count($features['data']) > 0 ? $features['data'] : [];
            $newProducts = isset($newProducts['data']) > 0 ? $newProducts['data'] : [];

            if ($Agent->isMobile()) {
                return Redirect::to($page);
            }

            return view('fashion.index', compact('topProducts', 'frequents', 'features', 'newProducts'));
        } else {

            //displayError($topProducts, 'fashion.index');
            return view('fashion.index', compact('resp'));
        }
    }

    /**
     * @param $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function showCategory(Request $category)
    {
        $Agent = new Agent();
//        $redirectView = view('errors.noNetwork');
        $catId = $category->route('catId');
        if ($catId == '1') {
            $title = 'Men\'s Products';
        } elseif ($catId == '11') {
            $title = 'Women\'s Products';
        } elseif ($catId == '41') {
            $title = 'Kids Product';
        } else {
            $title = 'Other Products';
        }

        $sub_url = env('GET_BASE_URL') . env('PRODUCT_API') . "getproductsbycat";
        $data = ['page' => '0', 'size' => '8', 'categoryId' => $catId];
        $data = ['json' => $data];

        $resp = consume($sub_url, 'POST', $data);
        if (isset($resp['data'])) {
            $response = $resp['data'];
            $mobile = env('MOBILE_LINK') . "/sub-category?categoryName=" . $response[0]->categoryName . "&categoryId=" . $catId;
            if ($Agent->isMobile()) {
                return Redirect::to($mobile);
            } else {
                return view('fashion.showcategory', compact('resp', 'response', 'title', 'catId'));
            }
        } else {
            return view('fashion.showcategory', compact('resp', 'response', 'title', 'catId'));
        }
    }


    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function showMoreCategory()
    {
        $body = json_decode(request()->getContent(), true);
        $data = ['json' => $body];
        $sub_url = env('GET_BASE_URL') . env('PRODUCT_API') . "getproductsbycat";
        $resp = consume($sub_url, 'POST', $data);
        return $resp;
    }
}
