<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InspireController extends Controller
{
    /**
     * The landing model and shop details
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request) {
        $gender = 'name';
        if(!empty($request)){
            return view('inspire.index', compact('gender'));
            dd("ff");
        }
    }

    /**
     * The landing model and shop details
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showInspireProducts(Request $request) {

        if(!empty(request('gender')) && !empty(request('event'))){

            $events = request('event');
            $gender = request('gender');
            $url = env('GET_BASE_URL') . "/fashion/inspireme/findoutfitbygenderandevent";
            $data = [
                "gender"=>$gender,
                "event"=>$events,
                "page"=>0,
                "size"=>10
            ];
            $data = ["json" => $data];
            $res = consume($url, 'POST', $data);
            $result = $res['data'];

            if($res['status'] == "00"){
                return view('inspire.category-products', compact('result'));
            }
        }
    }

    /**
     * The landing model and shop details
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inspireCategory(Request $request) {

        if(!empty(request('gender'))){
            $gender = request('gender');
            $url = env('GET_BASE_URL') . "/fashion/inspireme/findeventbygender/$gender";
            $res = consume($url, 'GET');
            $result = $res['data'];

            if($res['status'] == "00"){
                return view('inspire.category', compact('result'));
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDetails(Request $request) {

        if(!empty(request('outfitId'))){
            $id = request('outfitId');
            $url = env('GET_BASE_URL') . "/fashion/inspireme/findoutfitbyid/$id";

            $res = consume($url, 'GET');
            $details = $res['data'];

            if($res['status'] == "00"){
                return view('inspire.show-details', compact('details'));
            }
        }
    }

    public function timesRemaining(Request $request) {
        $details = "testing";
        return view('timeline', compact('details'));
    }
}
