<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    if (Cookie::get('firstTime') !== null)
        return redirect(route('fashionStore'));
    else
        Cookie::queue('firstTime', 'ft', 24*60*7);
    return view('index');
})->name('landingPage');*/

Route::get('/mobile', function () {
    return view('mobile_redirect');
});


Route::get('/step-bespoke', function () {
    return view('step-form');
})->name('comingSoon');

Route::get('/invoice', function () {
    return view('invoice');
})->name('printInvoice');

//Route::get('/', function () {
//    return redirect('fashion');
//});

Route::get('/', function () {
    return redirect('designers');
});


Route::get('/sitemap', function () {
    return view('sitemap.xml');
})->name('sitemap');

Route::get('/privacy-policy', function () {
    return view('return-policy');
})->name('returnPolicy');

Route::get('/faq', function () {
    return view('faq');
})->name('faq');

Route::get('/designerfaq', function () {
    return view('designerfaq');
})->name('designerfaq');

Route::get('/about-us', function () {
    return view('contact');
})->name('aboutUs');

Route::get('/shipping-and-delivery', function () {
    return view('shipping');
})->name('shipping');

Route::get('/terms-and-conditions', function () {
    return view('terms');
})->name('terms');

Route::get('/blog', function () {
    return view('blog.blog');
});

Route::get('/blog-full', function () {
    return view('blog.blog-full');
});

Route::get('about-us/who-we-are', function () {
    return view('about-us.who');
})->name('whoWeAre');

Route::get('about-us/events', function () {
    return view('about-us.event');
})->name('aboutEvent');

Route::get('about-us/brand', function () {
    return view('about-us.brand');
})->name('ourBrand');

Route::get('auth/manage/blog', function () {
    return view('admin.blog');
});

Route::get('auth/manage/add-blog', function () {
    return view('admin.add-blog');
});

Route::get('auth/manage/preview-blog', function () {
    return view('admin.preview-blog');
});
//events
Route::get('/events', 'EventsController@index')->name('eventsPage');
Route::get('/events/{event}/{eventId}', 'EventsController@showEvent')->name('viewEvent');
Route::post('/events/getevent', 'EventsController@getEvents')->name('getEvents');
// Route::post('/events/page/api', 'EventsController@getEventsByMonthAndYear')->name('eventsPageApi');
Route::post('/events/page/api', 'EventsController@getEventsApi')->name('eventsPageApi');
Route::get('/event/picture-details', 'EventsController@getEventPictureDetails')->name('getPictureDetails');

//fashion
Route::get('/fashion', 'FashionController@index')->name('fashionStore');
Route::get('/fashion/category/{category}/{catId}', 'FashionController@showCategory')->name('fashionCategory');
Route::post('/fashion/category/subcat', 'FashionController@showMoreCategory')->name('getMoreProductByCat');

//Inspire
Route::get('/inspire', 'InspireController@index')->name('inspireLanding');
Route::get('/inspire/outfit/{outfitId}', 'InspireController@showDetails')->name('inspireDetails');
Route::get('/inspire/{gender}/{event}', 'InspireController@showInspireProducts')->name('productsShow');
Route::get('/inspire/{gender}', 'InspireController@inspireCategory')->name('inspireCategory');
Route::get('/time-remaining', 'InspireController@timesRemaining')->name('timeRemaining');

//product
Route::get('/fashion/products/features', 'ProductController@getAllFeatureProducts')->name('allFeatureProducts');
Route::get('/fashion/products/new-arrival', 'ProductController@getAllNewArrival')->name('allNewArrival');
Route::get('/fashion/products/bespoke', 'ProductController@getAllBespokeProducts')->name('getAllBespoke');
Route::get('/fashion/bespoke/{category}', 'ProductController@getAllBespokeProductsByCat')->name('getAllBespokeCat');
Route::get('/fashion/bespoke-request/pre-order','ProductController@bespokePreOrder')->name('bespokePreOrder');
Route::get('/fashion/bespoke-request/pre-order/request-successful','ProductController@bespokeOrderSuccess')->name('bespokeSuccessful');
Route::post('/fashion/bespoke/subcat', 'ProductController@showMoreBespokeCat')->name('getMoreBespokeByCat');
Route::post('/fashion/bespoke/request', 'ProductController@processRequest')->name('bespokeRequest');
Route::get('/fashion/valentine-collections','ProductController@valentineCollections')->name('valCollection');
Route::get('/fashion/products/styles-catalogue', 'ProductController@stylesCatalogue')->name('styleCatalogue');


Route::post('/fashion/products/getmorefeatureproduct', 'ProductController@getMoreFeatureProducts')->name('allMoreFeatureProducts');
Route::get('/fashion/{catId}/{catName}/{subCat}/{subCatId}', 'ProductController@index')->name('productPage');
Route::get('/fashion/{subCategory}/product', 'ProductController@index')->name('productPageSearch');
Route::post('/fashion/products/getmore', 'ProductController@getMoreProducts')->name('getMoreProducts');
Route::post('/fashion/product/elasticsearch', 'ProductController@getMoreElasticProducts')->name('getMoreElasticProducts');
Route::post('/fashion/product/filterelasticsearch', 'ProductController@getMoreSliderElasticProduct')->name('getMoreSliderElasticProduct');
Route::post('/fashion/products/getmoreproducts', 'ProductController@getMoreFilterProducts')->name('getMoreFilterProducts');
Route::get('/fashion/products/{product}', 'ProductController@showDetails')->name('productDetails');
Route::post('/fashion/products/getproductname', 'ProductController@index')->name('get.productcat');
Route::post('/search/bespoke-images', 'ProductController@searchForImage')->name('searchForImage');
Route::post('/get/category-name', 'ProductController@categoryName')->name('categoryName');


//User's orders
Route::post('/orders/addtocart', 'User\UserOrderController@addToCart')->name('addToCart');
Route::post('/orders/update', 'User\UserOrderController@updateCart')->name('updateCart');
Route::post('/orders/addorder', 'User\UserOrderController@addOrder')->name('addOrder');
Route::get('/customize/{product}', 'User\UserOrderController@sizeInput')->name('customize');
Route::post('/order/bespoke-accept-order', 'User\UserOrderController@userAcceptOrder')->name('userAcceptOrder');
Route::post('/order/bespoke-add-cart', 'User\UserOrderController@addBespokeToCart')->name('addBespokeToCart');

Route::post('/orders/anoymouscheckout', 'User\UserOrderController@anonymousCheckout')->name('anonymousCheckoutCreate');

//user
Route::get('/order/track', 'User\UserOrderController@trackOrder')->name('trackOrder');
Route::get('/user/orders/payment-history', 'User\UserOrderController@paymentHistory')->name('paymentHistory');
Route::get('/network', 'User\UserController@noNetwork')->name('noNetwork');
Route::get('/feedback', 'User\UserController@userFeedBack')->name('userFeedBack');
Route::get('/product-feedback', 'User\UserController@productFeedBack')->name('productFeedBack');

Route::get('/user', function () {
    if (session()->has('userToken')) {
        return redirect('/user/{account}');
    } else {
        return view('user.index');
    }
});

Route::get('/user/{account}/event/add', 'User\UserController@addEvent')->name('createEvent');
Route::get('/decision', 'User\UserController@decision')->name('decision');
//Route::get('/user/{account}/{orderId}','UserController@trackOrder')->name('trackOrder');

//User Profile New Route
Route::get('/my-account','User\UserProfileController@index')->name('userDashboard');
Route::get('/my-account/bespoke-requests','User\UserProfileController@bespokeRequests')->name('userBespokeRequest');
Route::get('/my-account/bespoke-requests/{bidId}/view-quotes','User\UserProfileController@viewBespokeQuotes')->name('userBespokeQuotes');
Route::get('/my-account/orders','User\UserProfileController@orders')->name('userOrders');
Route::get('/my-account/address-book','User\UserProfileController@addressBook')->name('addressBook');
Route::get('/my-account/settings','User\UserProfileController@updateProfile')->name('settings');
Route::get('/my-account/measurements','User\UserProfileController@myMeasurements')->name('userMeasurement');
Route::post('/my-account/fundwallet', 'User\UserProfileController@fundWallet')->name('fundWallet');

//Additional User Order process
Route::get('/complain', 'User\UserController@complaint')->name('complain');
Route::get('/sizechart', 'User\UserController@sizeChart')->name('sizeChart');
Route::get('/account-status', 'User\UserController@accountStatus')->name('user.accountStatus');
Route::get('/declinedecision', 'User\UserController@declineDecision')->name('declineDecision');
Route::post('/decision', 'User\UserController@postDecision')->name('postDecision');

Route::post('/edit', 'User\UserController@edit')->name('editUser');
Route::get('/accountstatus', 'User\UserController@accountStatus')->name('accountStatus');
Route::post('/address/edit', 'User\UserController@addAddress')->name('addAddress');
Route::post('/address/update', 'User\UserController@updateAddress')->name('updateAddress');
Route::get('/address/delete', 'User\UserController@deleteAddress')->name('deleteAddress');
Route::get('/cart', 'User\UserOrderController@cart')->name('cart');
Route::get('/checkout', 'User\UserOrderController@checkout')->name('checkout');
Route::get('/checkout/payment', 'User\UserController@paymentDetails')->name('paymentDetails');
Route::get('/checkout/guest', 'User\UserOrderController@guestCheckout')->name('guestCheckout');
Route::get('/wishlist', 'User\UserController@wishlist')->name('wishlist');
Route::get('/wallet', 'User\UserController@wallet')->name('wallet');
Route::get('/forgotpassword', 'User\UserController@forgotPassword')->name('forgotPassword');
Route::get('/forgotpassword/sendmail', 'User\UserController@resetPassword')->name('resetPassword');
Route::get('/forgotpassword/sendmail/success', function () {
    return view('user.reset-success');
})->name('user.reset.success');
Route::get('/changepassword', 'User\UserController@inputPassword')->name('inputPassword');
Route::post('/newpassword', 'User\UserController@changePassword')->name('changePassword');
Route::post('/user/review', 'User\UserController@review')->name('user.review');
Route::get('/checkout/paymentsuccessful', 'User\UserController@paymentSuccessful')->name('paymentSuccessful');
Route::get('/user/address/getalladdress', function () {
    return getAddresses();
})->name('user.getAddresses');

Route::get('/user/order/details', 'User\UserOrderController@orderDetails')->name('user.getOrderDetails');
Route::get('/user/decision-now', 'User\UserController@decisionDetails')->name('decision-page');

Route::get('/apply/bespoke/print', 'Designer\DesignersController@printBespoke')->name('printBespoke');
//auth user
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/register', 'Auth\LoginController@register')->name('userRegistration');
Route::get('/logout', function () {
    session()->forget('userToken');
    return redirect(route('fashionStore'));
    //return back();
})->name('logout');

//auth designer
Route::post('/designer/register', 'Auth\LoginController@registerDesigner')->name('designer.register');
Route::post('/designer/login', 'Auth\LoginController@loginDesigner')->name('designer.login');
Route::get('/designer/logout', 'Auth\LoginController@logoutDesigner')->name('designer.logout');


//designers
Route::post('/designers/addNewProduct', 'Designer\DesignersProductController@addDesignerNewProduct')->name('designer.addNewProduct');
Route::post('/designers/addNewBespokeProduct', 'Designer\DesignersProductController@addNewBespokeProduct')->name('designer.addNewBespokeProduct');
Route::get('/designers/getBespokeProduct', 'Designer\DesignersProductController@getBespokeProduct')->name('getBespokeProduct');
Route::get('/{designer}/get-uploaded-style', 'Designer\DesignersProductController@getUserUploadStylesForDesigner')->name('getUserUploadStylesForDesigner');
Route::get('/designers', 'Designer\DesignersController@index')->name('designersLandingPage');
Route::get('/designers/profile', 'Designer\DesignersProfileController@profile')->name('designer.profile');
Route::get('/designers/profile/update', 'Designer\DesignersProfileController@updateProfile')->name('designer.updateprofile');
Route::get('/designers/account-status', 'Designer\DesignersController@accountStatus')->name('designer.accountStatus');
Route::get('/designers/registration/complete', 'Designer\DesignersController@registrationComplete')->name('designer.regComplete');
Route::post('/designers/orderaccept/accept', 'Designer\DesignersController@acceptOrder')->name('designer.acceptOrder');
Route::post('/designers/bespokeorder/accept', 'Designer\DesignersController@acceptBespokeOrder')->name('designer.accept.bespokeOrder');
Route::post('/designers/profile/edit', 'Designer\DesignersProfileController@editProfile')->name('designer.editProfile');
Route::get('/designers/account/settings', 'Designer\DesignersProfileController@accountSettings')->name('designer.accountSettings');
Route::post('/designers/profile/edit/logo', 'Designer\DesignersProfileController@updateLogo')->name('designer.updateLogo');
Route::post('/designers/profile/edit/banner', 'Designer\DesignersProfileController@updateBanner')->name('designer.updateBanner');
Route::get('/designer/{profile}', 'Designer\DesignersController@view')->name('designer.view');
Route::get('/{designer}/products', 'Designer\DesignersProductController@products')->name('designer.product');
Route::post('/api/products/api', 'Designer\DesignersProductController@productsApi')->name('designer.products.api');
Route::get('/{designers}/product-details/{pid}', 'Designer\DesignersProductController@productDetails')->name('designer.product.details');
Route::get('/{designers}/covert-to-bespoke/{productName}/{pid}', 'Designer\DesignersProductController@convertToBespoke')->name('designer.convert.bespoke');
Route::get('/{designers}/product-bespoke-details/{pid}', 'Designer\DesignersProductController@productBespokeDetails')->name('designer.bespokeproduct.details');
Route::get('/{designers}/upload-user-style-detail/{pid}', 'Designer\DesignersProductController@uploadStyleDetail')->name('designer.uploadStyle.details');
Route::post('/designers/placebids', 'Designer\DesignersProductController@placeBidForUserStyle')->name('placeBidForUserStyle');
Route::get('/{designers}/editproducts/{product}/{pid}', 'Designer\DesignersProductController@editProduct')->name('designer.edit');
Route::get('/{designers}/editbespokeproducts/{product}/{pid}', 'Designer\DesignersProductController@editBespokeProduct')->name('designer.bespoke.edit');
Route::post('/editproducts', 'Designer\DesignersProductController@editProductApi')->name('designer.product.edit');
Route::post('/editproductsimages', 'Designer\DesignersProductController@editProductImagesApi')->name('designer.product.editImages');
Route::post('/editartwork', 'Designer\DesignersProductController@editArtworkApi')->name('designer.product.editArtwork');
Route::post('/editmaterial', 'Designer\DesignersProductController@editMaterialApi')->name('designer.product.editMaterial');
Route::post('/designers/products/add', 'Designer\DesignersProductController@addProduct')->name('designer.product.add');
Route::get('/{designers}/product/new', 'Designer\DesignersProductController@addNewProduct')->name('designer.product.new');
Route::get('/{designers}/orders', 'Designer\DesignersController@orders')->name('designer.orders');
Route::get('/{designers}/orders/{orderId}', 'Designer\DesignersController@orderDetails')->name('designer.orders.details');
Route::get('/{designers}/bespoke-orders/{orderId}', 'Designer\DesignersController@bespokeOrderDetails')->name('designer.bespokeorders.details');
Route::get('/designers/dashboard', 'Designer\DesignersController@dashboard')->name('designer.dashboard');

Route::post('/designers/getmoredesignerproduct', 'Designer\DesignersController@getMoreDesignerProduct')->name('getMoreDesignerProduct');
Route::post('/designers/remove/product', 'Designer\DesignersProductController@removeProductImage')->name('designer.product.removeProductImage');
Route::post('/designers/remove/fabric/image', 'Designer\DesignersProductController@removeFabricImage')->name('designer.product.removeFabricEditImage');
Route::post('/designers/remove/artwork', 'Designer\DesignersProductController@removeArtworkImage')->name('designer.product.removeArtWorkImage');
Route::post('/designers/remove/material', 'Designer\DesignersProductController@removeMaterialImage')->name('designer.product.removeMaterialImage');
Route::get('/designers/get/pics', 'Designer\DesignersProductController@getMoreImage')->name('more.picture');
Route::post('/designers/verify/token', 'Designer\DesignersController@verifyDesignerToken')->name('verify.token');
Route::post('/designers/change/password', 'Designer\DesignersProfileController@updatePassword')->name('change.password');
Route::post('/designers/change/email', 'Designer\DesignersProfileController@changeEmail')->name('change.email');
Route::post('/designer/updatepersonalinformation', 'Designer\DesignersProfileController@updatePersonalInformation')->name('updatepersonalinformation');
Route::post('/designer/updatebusinessinformation', 'Designer\DesignersProfileController@updateBusinessInformation')->name('updatebusinessinformation');
Route::post('/designer/updateaccountinformation', 'Designer\DesignersProfileController@updateAccountInformation')->name('updateaccountinformation');
Route::get('/designer/bespoke/apply', 'Designer\DesignersProfileController@bespokeApplication')->name('designer.bespoke');
Route::post('/designer/change-designer-email', 'Designer\DesignersProfileController@changeDesignerEmail')->name('changeDesignerEmail');
Route::get('/{designers}/getstylefordesigner', 'Designer\DesignersProfileController@getUploadedStylesForDesigner')->name('getUploadedStylesForDesigner');
Route::post('/designer/get-product-sub-cat', 'Designer\DesignersProductController@getSubCatBycat')->name('getSubCatBycat');
Route::post('/designer/get-style-by-sub-cat', 'Designer\DesignersProductController@getStyleBySubCat')->name('getStyleBySubCat');
Route::get('/designer/designergetsizes', 'Designer\DesignersController@designerGetSizes')->name('designerGetSizes');
Route::post('/designer/getsubcatbycatid', 'Designer\DesignersController@getSubCatbyCatId')->name('getSubCatbyCatId');
Route::post('/designer/uploadImageToBank', 'Designer\DesignersController@uploadImageToBank')->name('uploadImageToBank');
Route::get('/{designers}/bespoke-request/bidding', 'Designer\DesignersController@bespokeBiddingRequest')->name('designerBespokeRequest');
Route::get('/{designers}/add-bespoke-product', 'Designer\DesignersController@addBespokeProduct')->name('addBespokeProduct');

Route::get('/{designers}/bespoke-request/{id}/details', 'Designer\DesignersController@viewBespokeBidding')->name('designerViewRequestDetails');
Route::get('/{designers}/bespoke-request-detail/{id}', 'Designer\DesignersController@getDesignerBespokeDetail')->name('getDesignerBespokeDetail');
Route::get('/{designers}/image-bank/sample-bespoke-image', 'Designer\DesignersController@uploadImage')->name('designer.uploadImage');
Route::get('/{designers}/bespoke-request', 'Designer\DesignersController@bespokeRequest')->name('designer.bespokeRequest');
Route::get('/{designers}/available-manual', 'Designer\DesignersController@availableManual')->name('designer.availableManual');
Route::post('/designers/makequote', 'Designer\DesignersController@makeQuote')->name('makeQuote');

//qa - Quality Assurance
Route::get('/auth/manage/qa/dashboard', 'QaController@dashboard')->name('qa.dashboard');
Route::get('/auth/manage/qa/getorders/all', 'QaController@allOrders')->name('qa.orders.all');
Route::get('/auth/manage/qa/bespoke-orders/all', 'QaController@allBespokeOrders')->name('qa.bespoke-order');
Route::get('/auth/manage/qa/getorders/{order}', 'QaController@viewOrderDetails')->name('qa.order.items');
Route::get('/auth/manage/qa/wawooh-orders/details', 'QaController@orderDetails')->name('qa.order.details');
Route::get('/auth/manage/qa/bespoke-orders/{orderNum}/details', 'QaController@bespokeOrderDetails')->name('qa.bespokeorder.details');
Route::get('/auth/manage/qa/change-qa-password', 'QaController@changeQaPassword')->name('qa.change.password');
Route::post('/auth/manage/qa/changepassword', 'QaController@changePassword')->name('change.qa.password');
Route::get('/auth/manage/qa/designers', 'QaController@allDesigners')->name('qa.designers');
Route::get('/auth/manage/qa/designers/{designerName}', 'QaController@viewDesigner')->name('qaViewDesigner');
Route::get('/auth/manage/qa/users', 'QaController@allUsers')->name('qa.users');
Route::get('/auth/manage/qa/orders/list', 'QaController@listOrders')->name('qa.order-list');
Route::get('/auth/manage/qa/orders-details/{order}', 'QaController@getListOrdersDetails')->name('qa.order-list-details');
Route::get('/auth/manage/qa/orders/invoice/{orderNum}', 'QaController@generateInvoice')->name('qaGetInvoice');
Route::get('/auth/manage/qa/orders/delivery-note/{orderNum}', 'QaController@generateInvoice')->name('qaGetDeliveryNote');
Route::get('/auth/manage/qa/orders/delivered', 'QaController@allDeliveredOrders')->name('deliveredOrders');

//cs - Customer Service
Route::get('/auth/manage/cs/dashboard', 'CustomerServiceController@dashboard')->name('customer.dashboard');
Route::get('/auth/manage/cs/orders', 'CustomerServiceController@getAllOrder')->name('customer.order');

//Auditor
Route::get('/auth/manage/auditor/dashboard', 'AuditorController@dashboard')->name('auditor.dashboard');
Route::get('/auth/manage/auditor/orders', 'AuditorController@getAllOrder')->name('auditor.order');
Route::get('/auth/manage/auditor/orders/export', 'AuditorController@exportAuditOrder')->name('auditorExport');

//Auth Admin
Route::get('/auth/manage/logout', function () {
    session()->forget('adminToken');
    return redirect(route('admin.login'));
})->name('admin.logout');

Route::get('/auth/manage/qa/logout', function () {
    session()->forget('qaToken');
    return redirect(route('admin.login'));
})->name('qa.logout');

Route::get('/auth/manage/auditor/logout', function (){
   session()->forget('auditortoken');
   return redirect(route('admin.login'));
})->name('logOutAuditor');

//admin
Route::get('/auth/manage/dashboard', 'Admin\AdminController@dashboard')->name('admin.dashboard');
Route::get('/auth/manage/events', 'Admin\AdminEventController@events')->name('admin.events');
Route::get('/auth/manage/events/delete', 'Admin\AdminEventController@deleteEvent')->name('admin.events.delete');
Route::post('/events/api', 'Admin\AdminEventController@eventsApi')->name('admin.events.api');
Route::post('/events/api/update', 'Admin\AdminEventController@updateEventsInfo')->name('admin.events.updateInfo');
Route::post('/events/api/deletepictures', 'Admin\AdminEventController@deleteEventsPicture')->name('admin.events.deletePictures');
Route::post('/events/api/updatepictures', 'Admin\AdminEventController@updateEventPicture')->name('admin.events.updateEventPicture');
Route::get('/auth/manage/events/add', 'Admin\AdminEventController@addEvents')->name('admin.events.create');
Route::post('/auth/manage/events/create', 'Admin\AdminEventController@createEvents')->name('admin.events.create.submit');
Route::get('/auth/manage/events/{preview}', 'Admin\AdminEventController@preview')->name('admin.events.preview');
Route::get('/auth/manage/category', 'Admin\AdminController@category')->name('admin.category');
Route::get('/auth/manage/subcategory', 'Admin\AdminController@subCategory')->name('admin.subcategory');
Route::get('/auth/manage/{subcategory}/styles', 'Admin\AdminController@categoryStyles')->name('admin.style');
Route::get('/auth/manage/orders/active', 'Admin\AdminOrderController@activeOrders')->name('admin.orders.active');
Route::get('/auth/manage/orders/details', 'Admin\AdminOrderController@orderDetails')->name('admin.orders.details');
Route::get('/auth/manage/getorders/all', 'Admin\AdminOrderController@allOrders')->name('admin.orders.all');
Route::get('/auth/manage/getbespokeorder/all', 'Admin\AdminOrderController@allBespokeOrders')->name('adminBespokeOrder');
Route::get('/auth/manage/orders/refunds', 'Admin\AdminController@allRefunds')->name('admin.refunds');
Route::get('/auth/manage/order/getorder/{order}', 'Admin\AdminOrderController@viewOrderDetails')->name('admin.order.items');
Route::get('/auth/manage/order/getbespoke/{orderNum}', 'Admin\AdminOrderController@viewBespokeOrderDetails')->name('admin.Bespokeorder.items');
Route::post('/auth/manage/getorders/order/accept', 'Admin\AdminOrderController@acceptOrder')->name('admin.acceptOrder');
Route::post('/auth/manage/getorders/order/confirm-payment', 'Admin\AdminOrderController@confirmPayments')->name('admin.comfirmPayments');
Route::post('/auth/manage/getbespoke/order/confirm-payment', 'Admin\AdminOrderController@confirmBespokePayment')->name('confirm.bespoke.order');
Route::post('/auth/manage/ticket/confirm-payment', 'Admin\AdminOrderController@confirmTicketPayment')->name('confirm.ticket.order');
Route::post('/auth/manage/cancel-orders', 'Admin\AdminOrderController@cancelOrder')->name('admin.cancelOrder');
Route::get('/auth/manage/event-ticket', 'Admin\AdminOrderController@eventTicket')->name('admin.eventTicket');
Route::post('/auth/mange/delete-ticket', 'Admin\AdminOrderController@deleteTicket')->name('admin.deleteTicket');
Route::post('/auth/manage/edit-ticket', 'Admin\AdminOrderController@getTicket')->name('admin.getTicket');
Route::post('/auth/manage/update-ticket', 'Admin\AdminOrderController@updateTicket')->name('admin.updateTicket');
Route::post('/auth/manage/create-ticket', 'Admin\AdminOrderController@ticketCreate')->name('admin.ticketCreate');
Route::get('/auth/manage/ticket-order', 'Admin\AdminOrderController@ticketOrder')->name('admin.ticket.order');
Route::post('/auth/manage/search-ticket-order', 'Admin\AdminOrderController@searchTicket')->name('admin.search.ticket');
Route::post('/auth/manage/collect-ticket', 'Admin\AdminOrderController@collectTicket')->name('admin.collect.ticket');
Route::post('/auth/manage/getorders/order/update', 'Admin\AdminController@updateOrder')->name('admin.updateOrder');
Route::get('/auth/manage/orders/completed', 'Admin\AdminOrderController@completedOrders')->name('admin.orders.completed');
Route::get('/auth/manage/designers', 'Admin\AdminController@designers')->name('admin.designers');
Route::get('/auth/manage/designers/{designerName}', 'Admin\AdminController@viewDesigner')->name('admin.designer.view');
Route::get('/auth/manage/uploaded-style', 'Admin\AdminController@adminUserUploadedStyle')->name('admin.uploaded.style');
Route::get('/auth/manage/uploaded-detail-style/{id}', 'Admin\AdminController@adminUserUploadedStyleDetail')->name('admin.uploadStyle.detail');
Route::get('/auth/manage/designers/products/getproducts', 'Admin\AdminProductController@getDesignerProducts')->name('admin.designer.getDesignerProducts');
Route::get('/auth/manage/users', 'Admin\AdminController@users')->name('admin.users');
Route::get('/auth/manage/allusers', 'Admin\AdminController@allUsers')->name('admin.allusers');
Route::get('/auth/manage/products', 'Admin\AdminProductController@products')->name('admin.products');
Route::get('/auth/manage/abandon-cart', 'Admin\AdminProductController@abandonCart')->name('admin.abandonCart');
Route::get('/auth/manage/products/details/{pId}', 'Admin\AdminProductController@productDetails')->name('admin.product.details');
Route::get('/auth/manage/bespoke-product/details/{pId}', 'Admin\AdminProductController@bespokeProductDetail')->name('admin.bespokeProduct.detail');
Route::get('/auth/manage/products/sponsor', 'Admin\AdminProductController@sponsor')->name('admin.products.sponsor');
Route::get('/auth/manage/user/details', 'Admin\AdminController@userDetails')->name('admin.user.details');
Route::get('/auth/manage/role', 'Admin\AdminController@role')->name('admin.role');
Route::get('/auth/manage/change-password', 'Admin\AdminController@changeAdminPassword')->name('admin.change.password');
Route::post('/auth/manage/admin-change-password', 'Admin\AdminController@changePassword')->name('change.admin.password');
Route::get('/auth/manage/subscribers', 'Admin\AdminController@emailSubscribers')->name('admin.newsletter');
Route::get('/auth/manage/feedback', 'Admin\AdminController@userFeedback')->name('admin.feedbacks');
Route::get('/auth/manage/promo-code/management', 'Admin\AdminOrderController@promoCodeManagement')->name('admin.promocode');
Route::post('/auth/manage/add-promo-code', 'Admin\AdminOrderController@addPromoCode')->name('addPromoCode');
Route::post('/auth/manage/apply-promo-code', 'Admin\AdminOrderController@applyPromoCode')->name('applyPromoCode');
Route::get('/auth/manage/designers/bespoke/application', 'Admin\AdminController@bespokeApplication')->name('admin.bespoke.apply');
Route::get('/auth/manage/style-catalogue', 'Admin\AdminController@imageBankForAdmin')->name('admin.image.bank');
Route::post('/auth/manage/verify-image-bank', 'Admin\AdminController@verifyDesignerImage')->name('verifyDesignerImage');
Route::post('/auth/manage/verify-patented-image', 'Admin\AdminController@verifyDesignerPatent')->name('verifyDesignerPatent');
Route::get('/auth/manage/order/invoice/{orderNum}','Admin\AdminOrderController@generateInvoice')->name('generateInvoice');
Route::get('/auth/manage/order/delivery-note/{orderNum}','Admin\AdminOrderController@generateDeliveryNote')->name('generateDeliveryNote');
Route::get('/auth/manage/order/delivered-orders','Admin\AdminOrderController@getAllDeliveredOrders')->name('getAllDeliveredOrders');

Route::get('/auth/manage/bespoke/order/pre-request', 'Admin\AdminController@bespokeRequests')->name('adminBespokeRequest');
Route::get('/auth/manage/bespoke/order/bespoke-product', 'Admin\AdminController@adminBespokeProduct')->name('adminBespokeProduct');
Route::get('/auth/manage/bespoke/{bId}/details', 'Admin\AdminController@bespokePerRequest')->name('adminBespokePerRequest');
Route::get('/auth/manage/bespoke-application/{baId}/details', 'Admin\AdminController@bespokeApplicationDetails')->name('admin.bespoke.detail');
Route::post('/auth/manage/designers/confirmbespoke', 'Admin\AdminController@confirmBespokeApplication')->name('admin.bespoke.confirm');
Route::get('/auth/manage/generate-promo-code', 'Admin\AdminOrderController@generatePromoCode')->name('admin.generate.promo.code');
Route::post('/auth/manage/verifypromocode', 'Admin\AdminOrderController@verifyPromoCode')->name('verifyPromoCode');
Route::post('/auth/manage/promo-code-by-id', 'Admin\AdminOrderController@getPromoCodeById')->name('getPromoCodeById');


//Export Data from Admin Route
Route::get('/auth/manage/export/orders', 'Admin\AdminOrderController@exportAllOrders')->name('admin.export.orders');
Route::get('/auth/manage/export/products', 'Admin\AdminExportController@exportAllProducts')->name('exportProducts');
Route::get('/auth/manage/export/transferInfo', 'Admin\AdminExportController@exportTransferInfo')->name('admin.export.transfer');
Route::get('/auth/manage/export/designer', 'Admin\AdminExportController@exportDesigners')->name('exportDesigner');
Route::get('/auth/manage/export/user', 'Admin\AdminExportController@exportUsers')->name('exportUsers');
Route::get('/auth/manage/export/action', function () {
    return view('admin.orders.export-file');
})->name('admin.export.file');

Route::get('/auth/manage/shipping', 'Admin\AdminController@shippingDoc')->name('admin.shipping');
Route::get('/auth/manage/wawooh-products/verify', 'Admin\AdminProductController@productsVerify')->name('admin.products.verify');
Route::get('/auth/manage/tags', 'Admin\AdminController@tags')->name('admin.tags');
Route::get('/auth/manage/tags/{images}', 'Admin\AdminController@tagsImages')->name('admin.tags.images');
Route::get('/auth/manage/tags/{images}/edit', 'Admin\AdminController@editTags')->name('admin.tags.edit');
Route::get('/auth/manage/tag/get/details', 'Admin\AdminController@getTagDetails')->name('getTagDetails');
Route::get('/auth/manage/tag/delete', 'Admin\AdminController@deleteTag')->name('deleteTag');
Route::get('/auth/manage', 'Admin\AdminController@adminLogin')->name('admin.login');
Route::post('/auth/manage', 'Admin\AdminController@Login')->name('admin.login.auth');
Route::get('/auth/manage/deactivatedesigner', 'Admin\AdminController@changeDesignerStatus')->name('admin.designer.deactivate');
Route::post('/auth/manage/createadminrole', 'Admin\AdminController@createAdminRole')->name('createAdminRole');
Route::post('/auth/manage/admindeactivedesigner', 'Admin\AdminController@adminDeactiveDesigner')->name('admin.deactivate.designer');
Route::get('/auth/manage/transferinfo', 'Admin\AdminController@transferInfo')->name('admin.transfer');
Route::get('/auth/manage/inspire/add', 'Admin\AdminController@addInspireProducts')->name('admin.inspire.add');
Route::get('/auth/manage/inspire/view', 'Admin\AdminController@viewAllInspireProducts')->name('admin.inspire.products');

Route::post('/auth//manage/getsubcatbycatid', 'Admin\AdminController@subCategoryByCatId')->name('getSubCatByCatId');
Route::post('/auth/manage/addCategory', 'CategoryController@add')->name('addCategory');
Route::post('/auth/manage/addSubCategory', 'CategoryController@addSub')->name('addSubCategory');
Route::post('/auth/manage/editSubCatgory', 'CategoryController@editSub')->name('edit.subcategories');
Route::post('/auth/manage/addStyles', 'CategoryController@addStyle')->name('addStyle');


Route::post('/addTag', 'TagController@addTag')->name('addTag');

//socials likes
Route::post('/likes', function () {
    $body = json_decode(request()->getContent(), true);
    $data = ['json' => $body];//request parameter
    $url = env('GET_BASE_URL') . env('USER_EVENT_API') . "addlike";//api
    return $res = consume($url, 'POST', $data);

});

//comments
Route::post('/comments', function () {
    $body = json_decode(request()->getContent(), true);
    $data = ['json' => $body];//request parameter
    $url = env('GET_BASE_URL') . env('USER_EVENT_API') . "addcomment";//api
    return $res = consume($url, 'POST', $data);

});

//get subcategories
Route::get('/subcategory', function () {
    $id = request('id');

    $url = env('GET_BASE_URL') . env('PRODUCT_API') . "$id/getsubcategories";//api
    $res = consumeWithoutToken($url, 'GET');
    if ($res['status'] == 0) {
        return $res['data'];

    } else {
        return $res;
    }

})->name('getSubCat');

//get substyles
Route::get('/styles', function () {
    $id = request('subCatId');
    if (is_numeric($id)) {
        $url = env('GET_BASE_URL') . "/fashion/product/$id/getstyles";//api
        $res = consumeWithoutToken($url, 'GET');
        return $res['data'];
    } else {
        return "'status':'99'";
    }
});

Route::get('/getDesigners', function () {
    $designer = request('designer');
    $url = env('GET_BASE_URL') . env('PRODUCT_API') . "$designer/getsubcategories";//api
    return $res = adminConsume($url, 'GET');
})->name('getDesigners');

Route::get('/getallDesigners', function () {
    return $designers = getAllDesigners();

})->name('getAllDesigners');

Route::get('/empty-cart', function () {
    if (session()->has('userToken')) {
        //empty the cart in the db
        $res = emptyCart();
        if ($res['status'] == 0) {
            return back();
        } else {
            return displayError($res);
        }
    } else if (session()->has('userCart')) {
        //empty the cart in the session
        session()->forget('userCart');
        return back();
    }
})->name('emptyCart');

Route::get('/removeItem', function () {
    if (session()->has('userToken')) {
        //remove item from the cart in the db

        $id = request('itemId');
        $url = env('GET_BASE_URL') . "/fashion/secure/customer/order/$id/deletecart";
        $res = consume($url, 'GET');
        if ($res['status'] == 0) {
            return back();
        } else {
            return $res;
        }

    } else if (session()->has('userCart')) {
        //remove the item from cart in the session
        $item = request('itemId');
        session()->forget('userCart.' . $item);
        return back();
    }
})->name('removeItemFromCart');

//Wishlist Function and routes definitions

Route::get('deleteWishListItem/{id?}', function ($id) {
    if (is_numeric($id)) {
        $url = env('GET_BASE_URL') . env('USER_WISHLIST_API') . "$id/delete";
        $res = consume($url, 'GET');

        if ($res['status'] == 0) {
            session()->flash('success', 'Successfully deleted from wishlist');

            return back();
        }
    }
})->name('deleteWishListItem');

Route::get('addtowishlist/{id?}', function ($id) {
    //
    if (is_numeric($id)) {
        $data = ['productId' => $id,];
        $data = ['json' => $data];
        $url = env('GET_BASE_URL') . env('USER_WISHLIST_API') . "add";
        $res = consume($url, 'POST', $data);
//        dd($res);
        if (session()->has('userToken') || $res['status'] == 0) {
            session()->flash('success', 'Successfully added to wishlist');
            return back();
        } else {
            return back();
        }
    }

})->name('addToWishList');

Route::get('notifyme/{id?}', function ($id) {
    //
    if (is_numeric($id)) {
        $data = ['productId' => $id,];
        $data = ['json' => $data];
        $url = env('GET_BASE_URL') . env('USER_WISHLIST_API') . "notifyme";
        $res = consume($url, 'POST', $data);

        if ($res['status'] == 0) {
            session()->flash('success', 'Notification will be sent to your mail when in stock');
            return back();
        }
    }
})->name('notifyme');

//Measurement Route and Controller
Route::post('/addMeasurement', 'MeasurementController@add')->name('addMeasurement');
Route::post('/updateMeasurement', 'MeasurementController@update')->name('updateMeasurement');
Route::get('/editMeasurement', 'MeasurementController@editForm')->name('editMeasurement');
Route::get('/deleteMeasurement', 'MeasurementController@delete')->name('deleteMeasurement');

Route::post('/add/bespoke/measurement', 'MeasurementController@addBespokeMeasurement')->name('addBespokeMeasurement');
Route::post('/add/image/to/bank', 'MeasurementController@uploadBespokeImageToBank')->name('designer.uploadBespokeImageToBank');
Route::post('/dispatch/bespoke-request', 'MeasurementController@dispatchBespoke')->name('dispatchBespoke');

Route::post('/submit/designer/bid', 'MeasurementController@bespokeSubmit')->name('designer.bespokeSubmit');


//social authentication

Route::get('/auth/facebook', 'SocialAuthentication@redirectToProvider')->name('facebookLogin');
Route::get('/auth/facebook/callback', 'SocialAuthentication@handleProviderCallback');
Route::get('/register', 'LoginController@registerWithFacebook')->name('facebookRegisterForm');

Route::get('/auth/register/facebook', 'SocialAuthentication@redirectToProviderForRegistration')->name('facebookRegister');
Route::get('/auth/register/facebook/callback', 'SocialAuthentication@handleProviderCallbackForRegistration')->name('facebookRegisterCallback');

Route::get('/activate', 'User\UserController@activateMail')->name('activateMail');

Route::get('/loginwithtoken', function () {
    $token = request('token');
    $url = request('url');
    session(['userToken' => $token]);
    return redirect($url);
})->name('loginWithToken');