function displayUploadedImages(upload, holder,counter){
    var html ='';

    for(var j =0; j < upload.length; j++){
        html += `
        <div id ='pix${counter}' class='col-md-3'>
        <input type='text' class='form-control pictureName' placeholder='Add Image Name' />
            <div class="uploadRes">
            
                <img  class='img-responsive  block pix' src='${upload[j]}' />
            </div>
                <button type='button' data-target='#pix${counter}' class='btn btn-block btn-xs btn-danger delete' title='Delete Image'>&times;</button>
            </div>`;

            counter++;
     }
    $('#loader').addClass('hide');
     $(holder).append(html);
     $('.addMoreBox').toggle();
     $('.uploadBox').toggle();
}
function appendAllEventsToAdminDocument(events,holder) {
    
    var template = '';
    if (events.length) {
            for(j = 0; j < events.length; j++){
                var title = convertToSlug(events[j].eventName);
                var active = 'active';
                var html = '';
                var type = (events[j].eventType == 'S')?'Sponsored':'';
                template += `	<div class=" learn19">
                <h3>${events[j].eventName} <small>${type}</small></h3>
                <hr>
                <span class='pull-right'>
                <a id=${events[j].id} class='btn btn-link delete'>Delete</a>
            </span>
            <div style='clear:both'></div>
                <div id="${title}${j}" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">`;
                    if(events[j].eventPictures != null){
                        while(events[j].eventPictures.length){
                            b = events[j].eventPictures.splice(0,3);
                            
                            html += `<div class="item ${active}">
                                        <div class="row learn20">`;
                            for(k = 0; k < b.length; k++){    
                                 html += `	<div class="col-md-4 col-xs-12">
                                                 <div><img class='img-fluid' src=${b[k].picture}></div>
                                             </div>`;
                             } 
                             html += `</div>
                                        </div>`;
                             active = '';                                    
    
                        }
                    }
                  

                    template += `${html}`;
                    
                template += `</div><a class="left carousel-control" href="#${title}${j}"  data-slide="prev">
                        <span class="fa fa-chevron-left "></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#${title}${j}"  data-slide="next">
                        <span class="fa fa-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>

                    <div class="containe learn21">
                        <p><a href="/auth/manage/events/${events[j].id}" class="view">View all</a></p>
                    </div>
                    </div>
                </div>`;
                
            }
        $(holder).html(template);
    }

}