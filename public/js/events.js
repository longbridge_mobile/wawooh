/**
 * @author
 * The Event JS Function
 * Refactor Friday, 21st September, 2018
 * LongBridge Technologies Software Developers
 */

function appendAllEventsToDocument(events, holder) {
    let template = '';
    if (events.length) {
        for (j = 0; j < events.length; j++) {
            let title = convertToSlug(events[j].eventName);
            let active = 'active';
            let html = '';
            let date = moment(events[j].eventDate).format('ll');
            template += `	<div class="evt-1 wow effect" data-wow-delay =".5s">
	            <div class="tag-div">
                    <img class="event-tag wow animated swing infinite" src="/img/tag4.png" alt="">
                    <span>` + events[j].totalTags + `</span>
                </div>
                <div class="event-heading wow animated fadeInUp" data-wow-delay=".4s">
                    <a href="/events/${title}/${events[j].id}" class="trans-0-4">
                        ${events[j].eventName} <br>
                        <small style='font-size: 14px; '>${date}</small>
                    </a>
                </div>
                <a href="/events/${title}/${events[j].id}" class="trans-0-4">
                <div class="image-holder">`;
            if (events[j].eventPictures != null) {
                while (events[j].eventPictures.length) {
                    b = events[j].eventPictures.splice(0, 3);
                    for (k = 0; k < b.length; k++) {
                        html += `<img class='img-control' src=${(b[k].picture).replace('http','https')}>`;
                    }
                }
            }

            template += `${html}`;

            template += `
                    </a></div>
                </div>`;
        }
        $(holder).append(template);
    }
}

function setImageInfo(data) {
    $('#eventName').text(data.eventName);
    var like_length = data.likes.length;
    $('#no_of_likes').text(like_length);
    $('#like-btn').attr('pictureid', `${data.id}`);
    $('.modal-des').text(data.pictureDesc);
    if (data.liked == 'true') {
        $('#like-btn i').removeClass('far fa-heart').addClass('fas fa-heart');
    } else {
        $('#like-btn i').removeClass('fas fa-heart').addClass('far fa-heart');
    }

    $('#commentBox').attr('pictureid', `${data.id}`);
    var htm = `<span class='arr fa fa-chevron-left'></span><span class='arr2 fa fa-chevron-right'></span>
    <img id='mainPicture' src=${data.picture.replace('http','https')} class='img-fluid center block full-height'/>
    <div id='loader' style="height:100px;display:none;position:Absolute;top:40%;z-index:1000"></div>
    
    <div class='alert full-width hide tag-items-box animated fadeInUp'>
         
    </div>`;
    $('.img-tag').html(htm);
    showComments(data);
    $('#mainPicture').on('load', function () {

        var elem = $(this);
        var offset = elem.offset().left - elem.parent().offset().left;
        $('#loader').hide();
        clickableArea('.img-tag', '#mainPicture', offset);
        displayTags('#map-area', data.tags);
    });
}

function showComments(data) {
    let html = '';
    if (data.comments.length) {
        for (i = 0; i < data.comments.length; i++) {
            //    var date =  moment(data.comments[i].createdDate).format("ll").fromNow();
            let date = moment(data.comments[i].createdDate).format("ll");

            html += `<div class="personal-comment">
                        <div><span>${data.comments[i].user.firstName} ${data.comments[i].user.lastName}</span> <i class='pull-right'>${date}</i></div>
                        <div>${data.comments[i].comment}</div>            
                    </div>`;
        }
    }
    $('.user-comments').html(html);
}

function addLike(data, holder, target = null) {
    $.ajax({
        url: `/likes`,
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (result) {
            if (result.status === "00") {
                $('.heart').find('i').toggleClass('fa-heart fa-heart');
                $(target).toggleClass('fa-heart fa-heart');
                $(holder).text(result.data.likes);
            } else if (result.status === 400) {
                swal('Login to like','','warning');
                $('#ModalSignIn').modal();
            } else {
                swal('An unexpected error occurred. Please try again later','', 'error');
            }
        },
        error: function (e) {
            $.notify(e, 'error');
        }
    });

}

function addComment(data, holder, commentbox = null) {
    getToken();
    $.ajax({
        url: `/comments`,
        type: "POST",
        data: JSON.stringify(data),
        headers: token,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (result) {
            var htm = '';
            if (result.status === "00") {
                $(commentbox).removeAttr('disabled');
                $(commentbox).val('');
                if (result.data.comments.length) {
                    //    var date =  moment(result.data.comments[i].createdDate).fromNow();
                    for (i = 0; i < result.data.comments.length; i++) {
                        let date = moment(result.data.comments[i].createdDate).format("ll");

                        htm += `<div class="personal-comment">
                                <p><span>${result.data.comments[i].user.firstName} ${result.data.comments[i].user.lastName}</span>
                                ${result.data.comments[i].comment}
                                <i class='pull-right'>${date}</i>
                                </p>
                            </div>`;
                    }
                    $(holder).html(htm);
                }

            } else if (result.status === '56') {
                swal('Log in to post a comment', '', 'warning');
                $('#ModalSignIn').modal('show');
                $('#postComment').removeClass('hide');
            } else {
                $(commentbox).removeAttr('disabled');
                swal('Log in to post a comment', '', 'warning');
                $('#ModalSignIn').modal('show');
            }
        },
        error: function (e) {
            $.notify(e, 'error');
            $(commentbox).removeAttr('disabled');
        }
    });

}