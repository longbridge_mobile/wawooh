add_tag_btn = "<div><button class='add-tag-btn btn no-bd-rad btn-lg btn-success no-bd-rad ' style=''>Add Tag</button></div>";
adding_tag_info = "<h4 class='text-bold'>Double click any where on the image to add tag. Click on the added tag to set and get information about the tag</h4><hr/>";


$('.select-event').on('change', function() {
    $('#map-area').remove();
    $.ajax({
        type: 'GET',
        url: '/admin/event-management/find/' + $(this).val(),
        beforeSend: function() {
            $('.spinner').removeClass('hide');
            $('.event-container').addClass('hide');
        },
        success: function(data) {
            $('.spinner').addClass('hide');
            $('.event-container').removeClass('hide');

            $('#current-pix').attr('src', data.event.main_picture);
            var div = $("<div id='content'></div>");
            div.append(add_tag_btn);
            $('.tag-components').html(div);
            var l = data.picture.length;
            var img = '';
            for (i = 0; i < l; i++) {
                img += "<div class='other-pix-holder'><img id='" + data.picture[i].id + "' src='" + data.picture[i].picture + "' class='event-pictures img-responsive img-thumbnail' /></div>"
            }

            $('.other-picture').html(img);
        },

        error: function() {
            $('.spinner').removeClass('hide');
        }
    });
});


$(document).on('click', '.add-tag-btn', function() {

    $('#content').html(adding_tag_info);
    addTag('.picture-holder', '#current-pix', '#content');
});


$(document).on('click', '.event-pictures', function() {

    var hotspot_id = 1;
    $('#map-area').remove();

    var img_id = $(this).attr('id');
    $('.event-pictures').css({
        border: 'none',
    });
    $(this).css({
        border: '1px solid blue',
    });
    $('#current-pix').attr('src', $(this).attr('src'));
    $('#current-pix').attr('data-target', "event-image" + $(this).attr('id'));
    // var div = $("<div id='content'></div>");
    // div.append();
    var div = $("<div id='content'></div>");
    div.append(add_tag_btn);
    $('.tag-components').html(div);
    var key = "event-image" + $(this).attr('id'); //

    if (localStorage.getItem(key)) {
        clickableArea('.picture-holder', '#current-pix');

        var clickable = $('#map-area');

        var list = JSON.parse(localStorage.getItem(key));

        for (i = 0; i < list.length; i++) {
            var span = $("<span><img class='tag-img' src='../img/tag4.png' /></span>");
            span.addClass('hotspot').css({
                left: list[i].left,
                top: list[i].top,
                
            }).attr('id', hotspot_id).appendTo(clickable);
            hotspot_id++;
        }
    } else {
        $('#content').html(add_tag_btn);
        $('#map-area').remove();
    }

});


// tag plugin

function clickableArea(container, img , offset = 0) {
    $(container).css('position', 'relative');
    $(img).after("<div id='map-area'></div"); // add a clickable div after the image
    var w = Math.round($(img).width());
    var h = $(img).height();
    var left = Math.round(offset);
    // make the clickable div overlay on the image completely --> width and height inclusive
    $('#map-area').css({
        width: w,
        position: 'absolute',
        top: '0px',
        left: left,
        // backgroundColor: 'rgba(0,0,0,.3)',
        cursor: 'pointer',
        height: h,
    });
}


function addTag(container, img, content) {
    id = 1;
    $(container).css('position', 'relative'); // position the image container to be relative
    var w = $(img).width(); //get width of the image
    var h = $(img).height(); //get width of the image
    $(img).after("<div id='map-area'></div"); // add a clickable div after the image

    // make the clickable div overlay on the image completely --> width and height inclusive
    $('#map-area').css({
        width: w,
        position: 'absolute',
        top: '0px',
        backgroundColor: 'rgba(0,0,0,.3)',
        cursor: 'pointer',
        height: h,
    });


    //if the clickable is doubled clicked, create an element over the the mouse position
    $(document).on('dblclick', '#map-area', function(e) {

        var span = $("<span><img class='tag-img' src='/img/tag4.png' /></span>");
        span.attr('id', id);
        id++;
        console.log(e.pageX);
        console.log($(this).offset().left);
        var left = (e.pageX - $(this).offset().left);
        var top = (e.pageY - $(this).offset().top);
        // console.log((e.pageX - $(this).position().left) + ' ,' + (e.pageX - $(this).offset().left));


        span.addClass('hotspot').css({
            left: left + "px",
            top: top + "px"

        }).appendTo($(this));
    });


    //if the new element is clicked get information about its position, then make it focused
    // also add action buttons to either save or delete the the new element
    $(document).on('click', '.hotspot', function() {
        $('#removeTag').show();
        var tag = ''; //initialize the tag name
        var disabled = ''; //initialize a disabled of hiddden flag
        // LS_Variable = $('#map-area').siblings('img').attr('data-target'); // get the name of the image to be used as key when saving to local storage
        LS_Variable = 'pictureTag'; // get the name of the image to be used as key when saving to local storage

        //check if any tag has been saved locally by its key
        if (localStorage.getItem(LS_Variable)) {

            var ls = JSON.parse(localStorage.getItem(LS_Variable)); //parse the item in the storage
            var l = $(this).position().left; //get the left coordinates of the clicked hotspot
            var t = $(this).position().top; // get the top coordinates of the clicked hotspot

            //loop through the key 
            for (i = 0; i < ls.length; i++) {
                //check if the target hotspot exist in the key values
                if (ls[i].left == l && ls[i].top == t) {

                    tag = ls[i].tag_name; // get the tag name saved
                    disabled = 'disabled'; // initialize the disabled or hidden feature
                }
            }
        }
        //make all hotspots inactive
        $('.hotspot').css({
            'background': '#eee',
            'border': 'transparent'
        });
        //make the clicked hotspot active
        $(this).css({
            'background': 'black',
            'border': '1px solid blue'
        });
        //create an html to collect or show the saved tag information  				
        var html = "\
                   <small class='text-primary'> Top:" + $(this).css('top') + "</small>, " +
            "<small class='text-primary'>Left:" + $(this).css('left') + "</small><hr class='clear'/>";

        $('#saveTag').attr('data-target', `span#${$(this).attr('id')}`);
        $('#removeTag').attr('data-target', `span#${$(this).attr('id')}`);
       $('#tag-info').show();
        $(content).html(html).fadeIn();

        // getCategory('#cat');

    });

    //delete functon
    $(document).on('click', '#removeTag', function() {
        var targ = $(this).attr('data-target'); //get the target hotspot 
        var top = $(targ).css("top"); // get the top coordinate of the target hotspot
        var left = $(targ).css("left"); //get the left corodinate of the target hospot
        //check if any tag has been saved locally
        console.log(top + ',' + left);
        if (localStorage.getItem(LS_Variable)) {
            var ls_var = localStorage.getItem(LS_Variable); // get the key of the image  				
            console.log(localStorage.ls_var);
            var ls = JSON.parse(ls_var); //parse the result to an array  			
            //loop through the key values
            for (i = 0; i < ls.length; i++) {
                //check if the target hotspot exist in the key values
                if (ls[i].leftCoordinate == left && ls[i].topCoordinate == top) {
                    //remove that item from the array
                }
            }
            if (ls.length == 0) {
                    localStorage.removeItem(LS_Variable);
                    $(targ).remove();
                    //remove the form associated with the hotspot
                    swal('Tag Removed Successfully', '', 'success');
                    $(this).hide();
                    $('.cancelTag').hide();
                    $('.addTag').show();
                    $('#tagInfo').hide();
                    $('#map-area').remove();
            }else {
                    //stringify the array
                    ls = JSON.stringify(ls);

                    //reset the local storage
                    localStorage.setItem(LS_Variable, ls);
                    //remove the hotspot from the document
                    $(targ).remove();
                    //remove the form associated with the hotspot
                    swal('Tag Removed Successfully','', 'success');
                    $(this).hide();
            }
        }
        else{
            $(targ).remove();
            swal('Tag Removed Successfully','', 'success');
            
        }
    });
    //save function
    $(document).on('click', '#saveTag', function() {
        var targ = $(this).attr('data-target'); //get the target hotspot 

        var top = $(targ).css('top'); // get the top coordinate of the target hotspot
        var left = $(targ).css('left'); // get the left coordinate of the target hotspot
        var arr = []; //initialize an array... to be used as container for the coordinates
        var w = $(img).width();
        var h = $(img).height();
        var imgSize = `${w}w,${h}h`;
       
        var coord = {
            //  'eventPictureId': $(this).attr('picture_id'),
            'leftCoordinate': left,
            'imageSize': imgSize,
            'topCoordinate': top,
            'subCategoryId': $('#subcat').val(),
            // 'designerId': '',
            'designerId': (isNumeric($('#designers').val()))?$('#designers').val():'',
            'productId': (isNumeric($('#designerProducts').val()))?$('#designerProducts').val():''
        }; // create an object coordinate of the target hotspot  				
        
        //check if any tag has been saved locally
        if (localStorage.getItem(LS_Variable)) {
            var _arr = localStorage.getItem(LS_Variable); // get the key of the image 
            var flag = 0; //create a flag to determine if the current item has already been saved. initialize to 0 or NO
            arr = JSON.parse(_arr); //parse the key's values and add to the array

            //loop through the key values
            for (i = 0; i < arr.length; i++) {
                //check if the target offset exist in the key values
                if (arr[i].leftCoordinate == left && arr[i].topCoordinate == top) {
                    //set the flag to 1 or YES	  					
                    flag++;
                }
            }
            //check if the flag is NO
            if (flag == 0) {
                swal('Tag saved successfully','', 'success');
                arr.push(coord); //add the new coordinates to the array
                localStorage.setItem(LS_Variable, JSON.stringify(arr)); // stringify the array and store in the storage
            } else {
                //flag is YES then inform the user of this
               swal('Tag Has already been saved','','error');
            }
        } else {
            
            swal('Tag saved successfully','', 'success');
            
            localStorage.setItem(LS_Variable, JSON.stringify([coord]));
        } //create a new key and set its values the store in local storage
    });
}

function displayTags(container, tags){
    //if the clickable is doubled clicked, create an element over the the mouse position
        id = 0;
        var span = '';
       // console.log(tags.length);
       
        for(i = 0; i < tags.length; i++){
            // span += $("<span><img class='tag-img' src='/img/tag4.png' /></span>");
            // span.attr('id', id);
            // id++;
            // span.addClass('hotspot').css({
            //     left: left + "px",
            //     top: top + "px"
    
            // });
            var top = tags[i].topCoordinate.replace('px', '');
            var left = tags[i].leftCoordinate.replace('px', '');
            var imgWidth =  tags[i].imageSize.split(',')[0].replace('w','');
            var imgHeight =  tags[i].imageSize.split(',')[1].replace('h','');
            // console.log(imgHeight);
            // console.log(imgWidth);
            top = tagOffsetPosition(top, imgHeight);
            left = tagOffsetPosition(left, imgWidth);
            
            var n = JSON.stringify(tags[i]);
            span += `<span id='tag${tags[i].id}' tag='${n}' subCat='${tags[i].subCategoryId}' class='hotspot' style='left:${left}%;top:${top}%;background:white'><img class='tag-img' src='/img/tag4.png' /></span>`
        }
       
        // console.log(span);

        $(container).append(span);
    
}

function tagOffsetPosition(position, dimension){
    return (position/dimension) * 100;
}

