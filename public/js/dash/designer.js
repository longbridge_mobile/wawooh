$(document).ready(function () {
    //jQuery time
    let current_fs, next_fs, previous_fs; //fieldsets
    let left, opacity, scale; //fieldset properties which we will animate
    let animating; //flag to prevent quick multi-click glitches

    $(".next").click(function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $(".update-form .progress-title li").eq($(".update-form fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                left = (now * 50) + "%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'transform': 'scale(' + scale + ')'});
                next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            // easing: 'easeInOutBack'
        });
    });

    $(".previous").click(function () {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $(".update-form .progress-title li").eq($(".update-form fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            // easing: 'easeInOutBack'
        });
    });

    $('#printMeasure').on('click', function (e) {
        e.preventDefault();
        printPage();
    });

    $('#sizeChart').on('change', function () {
        if (this.value === 'N') {
            $('#actionBtn').show();
            $('#sizeForm').hide();
        } else {
            $('#actionBtn').hide();
            $('#sizeModal').modal('hide');
            $('#sizeForm').show();
        }
    });

    $('input[name=producttype]').on('change', function () {
        if (this.value === '2') {
            $('#bespokeAccept').hide(500);
        } else if (this.value === '3') {
            $('#bespokeAccept').hide(500);
        } else {
            $('#bespokeAccept').show(300);
           // $('#material-div').css('display', 'none');
            //$('#material-div').addClass('hide');
        }
    });

    $('input[name=patent-type]').on('change', function () {
        if (this.value === 'true') {
            $('#Yes').show(500);
            $('#No').hide(300);
        } else if (this.value === 'false') {
            $('#Yes').hide(500);
            $('#No').show(500);
        } else {
            $('#No').show(300);
        }
    });

    $('#verify-method').on('change', function () {
        if (this.value === 'Video Call') {
            $('.vm').show(400);
            $('.pv').hide(300);
        } else if (this.value === 'Physical Visit') {
            $('.vm').hide(300);
            $('.pv').show(500);
        }
    });

    $('#ondemands').on('click', function () {
        $('.measureReq').show();
    });

    $('#currency').on('change', function () {
        if (this.value === 'USD') {
            $('#swiftCode').show(200);
        } else {
            $('#swiftCode').hide();
        }
    });

    $('#desBank').on('change', function () {
        // console.log($(this).val());
        if (this.value === 'Others') {
            $('#otherBank').show(200);
            // console.log($(this).val());
        } else {
            $('#otherBank').hide();
        }
    });

    $('#sizeModal, #sizeUploadModal, #ModalSignIn, #changeEmail, #tokenModal').modal({
        show: false,
        keyboard: false,
        backdrop: 'static'
    });

    $('[name = "businessState"]').change(function () {
        //let status = document.getElementById('businessProof');
        if (this.value === "Y") {
            $('#businessProof').show();
            $('#businessID').hide();
        }
        else if (this.value === "N") {
            $('#businessProof').hide();
            $('#businessID').show();
        }
        //status.style.display = this.value == "Registered" ? "s": "none";
    });

    $('.calendar').flatpickr({
        minDate: "today",
        altInput: true,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
    });

    // $('#payment-date').flatpickr();

    //toggle between text and password, save the stress of confirm password input
    $('.pwd-show, .pwdReg-show').on('click', function () {
        $(this).toggleClass("fa-eye fa-eye-slash");
        let field = $($(this).attr("toggle"));
        if (field.attr("type") === "password") {
            field.attr("type", "text");
        } else {
            field.attr("type", "password");
        }
    });

    $('.forgetPwd').on('click', function (e) {
        e.preventDefault();
        $('#loginForm').hide(500);
        $('.regNotify').hide();

        $('#forgotPassword, #pwLogin').show(300);
    });

    $('#loginLink').on('click', function (e) {
        e.preventDefault();
        $('#forgotPassword, #pwLogin').hide(500);
        $('#loginForm').show(300);
        $('.regNotify').show();
        $('#loginForm').css({
            'marginTop': '8em'
        });
    });

    $('#regFlip').on('click', function (e) {
        e.preventDefault();
        $('#loginForm').hide(600);
        $('.regNotify').hide();
        $('#regForm').show(800);
        $('.login-form').css({
            'marginTop': '2em'
        });
    });

    $('#loginReg').on('click', function (e) {
        e.preventDefault();
        $('#regForm').hide(500);
        $('#loginForm').show(300);
        $('.regNotify').show();
    });

    $('.card').click(function () {
        $(this).toggleClass('flipped');
    });
});

/*$(document).ready(function () {
    $('#updateFabricModal').modal('show');
});*/

//Dependent Functions
function removeSpace(value) {
    return value.replace(/^\s+|\s+$/gm, '');
}

function printPage() {
    let prtContent = document.getElementById("measurementModal");
    let WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
    WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
}

function changeToUpper(data) {
    setTimeout(function () {
        data.value = data.value.toUpperCase();
    }, 1);
}

function setCookie(name, cvalue, exdays) {
    let d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

    let expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);

    let ca = decodedCookie.split(',');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === '') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length)
        }
    }
    return '';
}

