function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

function isAlphaNumeric(string) {
    var re = /^[a-zA-Z0-9]+$/i;
    return re.test(string);
}

function isNumeric(string) {
    var re = /^[0-9]+$/i;
    return re.test(string);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function isAlpha(string){
    var re = /^[a-zA-Z]+$/i;
    return re.test(string);
}

function isNotEmpty(field, error = '') {
    var empty = true;
    var numEmpty = 0;
    $(field).each(function (i, obj) {
        if ($(this).val().length == 0 || $(this).val() == -1) {
            console.log($(this).val());
            $(this).addClass(error);
            //return empty = false
            numEmpty++;
        } else {
            $(this).removeClass(error);
        }
    });
    if (numEmpty == 0) {
        return true;
    } else {
        return false;
    }
    return empty;

}

function isAllNumeric(ele) {
    var empty = true;
    var numEmpty = 0;
    $(ele).each(function () {
        if (isNumeric($(this).val())) {

        } else {
            numEmpty++;
        }

    });

    if (numEmpty == 0) {
        return true;
    } else {
        return false;
    }


}

function isValid(ele, error) {
    var empty = true;
    var numInvalid = 0;
    var numEmpty = 0;
    $(ele).each(function () {
        if ($(this).val().length > 0) {
            if (!isNumeric($(this).val())) {
                $(this).addClass(error);
                numInvalid++;
            }
        } else {
            numEmpty++;
        }
    });

    if (numInvalid === 0 && numEmpty !== $(ele).length) {
        return true;
    } else {
        return false;
    }
}