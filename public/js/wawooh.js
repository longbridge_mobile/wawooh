/**
 * Scripts for Wawooh
 */
$(document).ready(function () {
    let year = new Date().getFullYear();
    $('#year').text(year);

    $('#no-link').on('click', function (e) {
        e.preventDefault();
    });

    $(".mouse-div").click(function () {
        $("html, body").animate({
            scrollTop: $(".event-details").offset().top - 1
        }, 900);
    });

    $('[name = "d-decision"]').change(function () {
        let use = document.getElementById('acc-detail');
        use.style.display = this.value == "refund" ? "block" : "none";
    });


    $('[name = "d-decision"]').change(function () {
        if ($('#d-shopping').is(":checked")) {
            $('#shopping').show();
            $('#decision-submit').hide();
        } else {
            $('#decision-submit').show();
            $('#shopping').hide();
        }
    });

    $('#subscribe').on('click', function () {
        $('#subLoad').show();
        $(this).val('').attr('disabled', 'disabled');
    });

    /**
     * Control the international number on phone input
     */
    $('#pno, #pn').intlTelInput();

    $('[name="fitness"]').change(function () {
        if ($('.fitness-slim').is(":checked")) {
            $('#disMeasure').show(500);
            $('#nextBtnF').hide(500)
        } else {
            $('#disMeasure').hide(300);
        }
    });

    //initialize an awesome calendar better than html 5 for pickup date
    $('#datePicker').flatpickr({
        altInput: true,
        altFormat: "F, Y",
        dateFormat: "Y-m",
    });

    $('#payment-date').flatpickr();

    $('.pickupdate').flatpickr({
        minDate: "today",
        altFormat: "M j, Y",
        //altFormat: "M j, Y h:iK",
    });

});

$(document).ready(function () {
    //$("#updateModal").modal('show');

    /*$('.dbirth').flatpickr({
        dateFormat: "Y/m/d"
    });*/

    $('.openNav').on('click', function (e) {
        //e.preventDefault();
        $('.profileDrop').slideToggle();
    });

    $('#vM, #no-link').on('click', function (e) {
        e.preventDefault();
        $('.otherCat').slideToggle();
    })


    $(".scroll-icon").click(function () {
        $("html, body").animate({
            scrollTop: $(".seller").offset().top - 1
        }, 500);
    });

    $('#userPayment, #userOrders').DataTable();
});

function setCookie(cname, c_value, days) {
    let d = new Date();
    d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + c_value + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/*
* Countdown
* @credit https://www.sitepoint.com/build-javascript-countdown-timer-no-dependencies/
*/
/*
* function getTimeRemaining(endtime) {
    let t = Date.parse(endtime) - Date.parse(new Date());
    let seconds = Math.floor((t / 1000) % 60);
    let minutes = Math.floor((t / 1000 / 60) % 60);
    let hours = Math.floor((t / (1000 * 60 * 60 )) % 24);
    let days = Math.floor(t / (1000 * 60 * 60 *24));

    return {
        'total' : t,
        'days' : days,
        'hours' : hours,
        'minutes' : minutes,
        'seconds' : seconds
    };
}

function startClock(id, endtime) {
    let clock = document.getElementById(id);
    let daysSpan = clock.querySelector('.days');
    let hoursSpan = clock.querySelector('.hours');
    let minutesSpan = clock.querySelector('.minutes');
    let secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        let t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if(t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    let timeinterval = setInterval(updateClock, 1000);
}

let deadline = "2019-01-31";

//Initialise the countdown
startClock('countDown', deadline);*/
