function getAllEvents(data) {
    var temp = '';
    $.ajax({
        type: 'POST',

        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: false,

        beforeSend: function () {
            $('#loader').removeClass('hide');
        },
        url: `${base_url}/fashion/event/getevents`,
        success: function (result) {

            temp = result;

            $('#loader').addClass('hide');
        },
        error: function (e) {

            $.notify('A Network error occured', 'error');
            $('#loader').addClass('hide');

            console.log(e);
        },
    });

    return temp;

}


function getAllEventImages(id) {
    var temp = '';

    $.ajax({

        url: `${base_url}/fashion/event/${id}/geteventbyId`,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        async: false,
        success: function (result) {
            // console.log(result.data.event);
            temp = result.data.event;


        },
        error: function (e) {
            $.notify('A network error occurred. Please try again', 'error');
            console.log(e);

        }
    });

    return temp;
}

function appendAllEventsToDocument(events, holder) {

    var template = '';
    console.log(events.length);
    if (events.length) {
        console.log(events);

        for (j = 0; j < events.length; j++) {

            var title = convertToSlug(events[j].eventName);
            var active = 'active';
            var html = '';
            let date = moment(events[j].eventDate).format('ll');
            template += `<div class="container mobileCarousel d-md-none">
                <h3>${events[j].eventName}</h3>
                <hr>
                <div id="${title}${j}" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">`;
            if (events[j].eventPictures != null) {
                while (events[j].eventPictures.length) {
                    b = events[j].eventPictures.splice(0, 3);

                    html += `<div class="carousel-item ${active}">
                                        <div class="row learn20">`;
                    for (k = 0; k < b.length; k++) {
                        html += `	<div class="col-md-4 col-xs-12">
                                                 <div><img class='img-fluid' src=${b[k].picture}></div>
                                             </div>`;
                    }
                    html += `</div>
                                        </div>`;
                    active = '';

                }
            }


            template += `${html}`;

            template += `<a class="carousel-control-prev" href="#${title}${j}" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left " aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#${title}${j}" role="button" data-slide="next">
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>

                    <div class="container learn21">
                        <p><a href="/events/${title}?event=${events[j].id}" class="view">View all</a></p>
                    </div>
                    </div>
                </div>`;

        }


        $(holder).append(template);
    }

}


function setImageInfo(data) {
    $('#eventName').text(data.eventName);
    var like_length = data.likes.length;
    $('#no_of_likes').text(like_length);
    $('#like-btn').attr('pictureid', `${data.id}`);
    $('.modal-des').text(atob(getUrlParameter('d')));

    $('#commentBox').attr('pictureid', `${data.id}`);
    var htm = "<span class='arr fa fa-chevron-left'></span><span class='arr2 fa fa-chevron-right'></span>";
    htm += "<img id='mainPicture' src=" + data.picture + " class='img-fluid center block full-height'/>";
    htm += "<div id='loade'>Hello i am here</div><div class='alert  full-width hide tag-items-box'>hello</div>";
    $('.img-tag').html(htm);
    //showComments(data);
    $('#mainPicture').on('load', function () {

        var elem = $(this);
        var offset = elem.offset().left - elem.parent().offset().left;
        $('#loade').hide();
        clickableArea('.img-tag', '#mainPicture', offset);
        displayTags('#map-area', data.tags);


    });
}


function displayComments(data, element) {
    var html = '';
    if (data.length) {
        for (i = 0; i < data.length; i++) {
            // html += "<div class='personal-comment'><p><span>" + data[i].user.firstName + " " + data[i].user.lastName;
            // html += "</span>" + " " + data[i].comment + "</p></div>";
            html += `<div class="personal-comment"><p><span>${data[i].user.firstName} ${data[i].user.lastName}</span><br> ${data[i].comment}</p></div>`;
        }
    }


    $(element).html(html);
}

function showComments(data) {
    var html = '';
    if (data.comments.length) {
        for (i = 0; i < data.comments.length; i++) {
            //    var date =  moment(data.comments[i].createdDate).format("ll").fromNow();
            var date = moment(data.comments[i].createdDate).format("ll");

            html += `<div class="personal-comment">
                                <div><span>${data.comments[i].user.firstName} ${data.comments[i].user.lastName}</span> <i class='pull-right'>${date}</i></div>
                                <div>${data.comments[i].comment}</div>
                                
                                
                            </div>`;

        }

    }
    $('.user-comments').html(html);
}

// function addLike(data,holder){
//     getToken();
//     $.ajax({
//         url: `${base_url}/fashion/event/addlike`,
//         type: "POST",
//         data: JSON.stringify(data),
//         headers: token,
//         contentType: "application/json; charset=utf-8",
//         dataType: 'json',
//         success: function(result) {
//             if(result.status == 00 ){
//                 $('.heart').find('i').toggleClass('fa-heart-o fa-heart');
//                 $(holder).text(result.data.likes);
//             }else{
//                 $(holder).notify('An error occurred', 'error');
//             }
//             console.log(result);
//         },
//         error: function(e) {
//             $.notify('A network error occurred. Please try again later', 'error');
//             console.log(e);
//             console.log(token);
//         }
//     });

// }
function addLike(data, holder, target = null) {

    $.ajax({
        url: `/likes`,
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (result) {
            if (result.status === '00') {
                $('.heart').find('i').toggleClass('fa-heart-o fa-heart');
                $(target).toggleClass('fa-heart-o fa-heart');
                $(holder).text(result.data.likes);
            } else if (result.status === '56') {
                //$(holder).notify('An error occurred', 'error');
                $('#ModalSignIn').css('z-index', '1000000000');
                $('#ModalSignUp').css('z-index', '1000000000');
                $('#ModalSignIn').modal();
            } else {
                $.notify('An unexpected error occurred. Please try again later', 'error');
            }
            console.log(result);
        },
        error: function (e) {
            $.notify('A network error occurred. Please try again later', 'error');
            console.log(e);

        }
    });

}


function addComment(data, holder, commentbox = null) {
    getToken();
    $.ajax({
        url: `/comments`,
        type: "POST",
        data: JSON.stringify(data),
        headers: token,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        success: function (result) {
            var htm = '';
            if (result.status == 00) {
                $(commentbox).removeAttr('disabled');

                $(commentbox).val('');
                console.log(result);
                if (result.data.comments.length) {
                    //    var date =  moment(result.data.comments[i].createdDate).fromNow();
                    for (i = 0; i < result.data.comments.length; i++) {
                        var date = moment(result.data.comments[i].createdDate).format("ll");

                        htm += `<div class="personal-comment">
                                <p><span>${result.data.comments[i].user.firstName} ${result.data.comments[i].user.lastName}</span>
                                ${result.data.comments[i].comment}
                                <i class='pull-right'>${date}</i>
                                </p>
                            </div>`;
                    }
                    $(holder).html(htm);

                }


            } else {
                $(commentbox).removeAttr('disabled');
                $('#ModalSignIn').css('z-index', '1000000000');
                $('#ModalSignUp').css('z-index', '1000000000');
                $('#ModalSignIn').modal();

            }
        },
        error: function (e) {
            $.notify('A network error occurred. Please try again later', 'error');
            console.log(e);
            $(commentbox).removeAttr('disabled');


        }
    });

}