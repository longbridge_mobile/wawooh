function getProductsBySub(data) {
    let temp;
    $.ajax({
        url: `${mainUrl()}/fashion/product/gettagproducts`,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (result) {
            if (result.status === "00") {
                showProductsByTags('.tag-items-box', result.data);
            } else {
                //alert('error');
            }
        },
        error: function (e) {
            console.log(e);
        }
    });
    return temp;
}

function showProductsByTags(container, products) {
    if (products.length) {
        let html = '<div style="width: 800px;margin: 0 auto;"><div class="owl-carousel owl-theme carouselTag">';
        for (i = 0; i < products.length; i++) {
            let name = convertToSlug(`${products[i].name}`);
            let design = convertToSlug(`${products[i].designerName}`);
            let image = `${products[i].picture[0] === undefined}` ? " " : `${products[i].picture[0].picture}`;
            // var image = b[j].picture[0] == undefined ? " " : b[j].picture[0].picture;
            // html += "<div class='contain'>\
            //     <img src=" + products[i].picture[0].picture + " alt='product'\
            //      class='image img-fluid center block full-height'>\
            //     <div class='overlay'>\
            //         <div class='text text-center' style='width:100%'>\
            //             <div style='font-size:10px'>Designed by: <br/>\
            //                 <span style='font-size:13px !important;font-weight:bold'>" +
            //     products[i].designerName +
            //     "</span></div>\
            //             <a href='/fashion/products/" + name + products[i].id + "' style='color:white !important;padding:2px;' class='btn btn-sm brand-bgcolor view-btn'>View</a>\
            //             <br/>#" + products[i].amount +

            //     "</div>\
            //     </div>\
            // </div>";
            html += `
                <div class="item">
                    <div>
                        <img src='${products[i].picture[0].picture}' alt="">
                        <div class="tagOverlay">
                            <div class="tagContent">
                                <div>
                                    <p class="makeBold">${products[i].name}</p>
                                    <p><a href="/designer/${design}">${products[i].designerName}</a></p>
                                    <p class="makeBold">${formatCurrency(products[i].amount)}</p>
                                    <a href='/fashion/products/${name}-${products[i].id}'  class='btn btn-small btn-wawooh'>View</a>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
        }

        html += " <div class='item'>\
                <div class='btn-more'>\
                    <a class='btn btn-view-all' href='/fashion/" + products[0].subCategoryId + "/products/?subcatId=" + products[0].subCategoryId + "'" + "class='btn btn-wawooh'>View More</a>\
                </div>\
            </div></div>'";

        //     html += "<div class='contain' style='width:4%'><div class='text2'>\
        //    <a href='/fashion/" + products[0].subCategoryId + "/products/?subcatId=" + products[0].subCategoryId + "' style='color:white !important;padding:2px;' class='btn btn-sm brand-bgcolor view-btn'>View More</a></div></div>";

        $(container).html(html);
    } else {
        $(container).html('');

    }
}

function getProductById(id) {

    var temp;
    $.ajax({
        url: `${mainUrl()}/fashion/product/${id}/getproductbyid`,
        type: "GET",
        async: false,
        headers: token,
        contentType: "application/json; charset=utf-8",
        dataType: 'json',

        success: function (result) {
            temp = result.data.products;

        },
        error: function (e) {
            // console.log(e);
        }


    });

    return temp;

}

function getDiscountPercent($disPrice, $amount) {
    $dis = $disPrice / $amount;
    $disInPer = ($dis) * 100;
    return round($disInPer);
}

function getNewPrice($amount, $discPrice) {
    $new = $amount - $discPrice;
    return $new;
}

function appendAllElasticProductsToDocument(products, container){

    var template = '';
    if(products.length > 0){

        for (var i = 0; i < products.length; i++) {
            template += `<div class="row-holder">`;

            while (products.length) {
                b = products.splice(0, 3);
                for (var j = 0; j < b.length; ++j) {
                    var rating = '';
                    //$('#products').append('hello');
                    /*if(b[j].designerStatus == 'A'){*/
                    if (parseInt(b[j].productQualityRating)) {
                        for (k = 0; k < b[j].productQualityRating; k++) {
                            rating += '<span class="fa fa-star checked"></span>';
                        }
                        for (l = 0; l < (5 - b[j].productQualityRating); l++) {
                            rating += '<span class="fa fa-star"></span>';
                        }
                    } else {

                    }
                    var slashedPrice = formatCurrency(b[j].slashedPrice);
                    var p_name = convertToSlug(b[j].name);
                    var p_id = b[j].id;
                    var name = p_name +'-'+ p_id;
                    var designerId = b[j].designerId;
                    var designer = convertToSlug(b[j].designerName).toLowerCase();
                    var amt = formatCurrency(b[j].amount);
                    var getPerPrice = b[j].percentageDiscount;
                    var image = b[j].productColorStyleDTOS[0] === undefined ? " " : b[j].productColorStyleDTOS[0].productPictureDTOS[0].picture;
                    var image_hover = b[j].productColorStyleDTOS[0] === undefined ? " " : b[j].productColorStyleDTOS[0].productPictureDTOS[1].picture;
                    // console.log(image);
                    template += `<div class="row-3 effect">
                        <div class="holder-container">
                            <div class="img-height">
                                <a href='/fashion/products/${name}'>
                                 <img src="${image.replace('http','https')}">
                                 <img class="under-img" src="${image_hover.replace('http','https')}" alt="">
                                </a>                          
                            </div>
                            <div class="p-details">
                                <span class="p-name display-type">
                                    <a href='/fashion/products/${name}'>
                                        ${b[j].name}
                                    </a>
                                </span>
                                <!--{{route('designer.view', str_slug(strtolower($product->designerName)))}}/?wawoohdes={{$product->designerId}}-->
                                <a href="/designer/${designer}-${designerId}" class="d-name">Designer: ${b[j].designerName}</a>
                            </div>`;

                    if (b[j].slashedPrice > 0) {
                        template += `<div class="price">
                                            <ul class = ''>
                                                <li>
                                                    <span>${slashedPrice}</span>
                                                </li>
                                                <li class = 'old text-danger'>${amt}</li>
                                            </ul>
                                            <ul class = 'discount'>
                                                <li>&dash;${getPerPrice}%</li>
                                            </ul>
                                    </div>`;
                    } else {
                        template += `<div class="price">
                                            <ul>
                                                <li>${amt}</li>
                                            </ul>
                                    </div>`;
                    }
                    template += `<div class="col-md-12 col-6 rating">
                                            ${rating}
                                  </div>
                    </div>
                </div>`;
                    /* }else{
                         console.log('hello');
                     }*/
                }
            }
        }
        template += '</div>';
        $(container).append(template);
    }else{
       // alert('no product');
    }
}

function appendAllProductsToDocument(products, container) {
    var template = '';
    if (products.length) {
        for (i = 0; i < products.length; i++) {
            template += `<div class="row-holder">`;

            while (products.length) {
                b = products.splice(0, 3);
                for (j = 0; j < b.length; ++j) {
                    var rating = '';
                    /*if(b[j].designerStatus == 'A'){*/
                    if (parseInt(b[j].productQualityRating)) {
                        for (k = 0; k < b[j].productQualityRating; k++) {
                            rating += '<span class="fa fa-star checked"></span>';
                        }
                        for (l = 0; l < (5 - b[j].productQualityRating); l++) {
                            rating += '<span class="fa fa-star"></span>';
                        }
                    } else {

                    }
                    var slashedPrice = formatCurrency(b[j].slashedPrice);
                    var p_name = convertToSlug(b[j].name);
                    var p_id = b[j].id;
                    var name = p_name +'-'+ p_id;
                    var designerId = b[j].designerId;
                    var designer = convertToSlug(b[j].designerName).toLowerCase();
                    var amt = formatCurrency(b[j].amount);
                    var getPerPrice = b[j].percentageDiscount;
                    var bespoke = b[j].acceptCustomSizes;

                    var image = b[j].productColorStyleDTOS[0] === undefined ? " " : b[j].productColorStyleDTOS[0].productPictureDTOS[0].picture;
                    var image_hover = b[j].productColorStyleDTOS[0] === undefined ? " " : b[j].productColorStyleDTOS[0].productPictureDTOS[1].picture;
                    //var image = b[j].picture[0] == undefined ? " " : b[j].picture[0].picture;
                    template += `<div class="row-3 effect">
                        <div class="holder-container">
                           <div class="wishlist-badge"></div>
                           <div class="type-badge"></div>
                            <div class="img-height">
                                <a href='/fashion/products/${name}'>
                                 <img src="${image.replace('http','https')}">
                                 <img src="${image_hover.replace('http','https')}" class="under-img" alt="">
                                </a>                             
                            </div>
                            <div class="p-details">
                                <span class="p-name display-type">
                                    <a href='/fashion/products/${name}'>
                                        ${b[j].name}
                                    </a>
                                </span>
                                <a href="/designer/${designer}-${designerId}" class="d-name">Designer: ${b[j].designerName}</a>
                            </div>`;

                    if (b[j].slashedPrice > 0) {
                        template += `<div class="price">
                                            <ul class = 'vvvv'>
                                                <li>
                                                    <span>${slashedPrice}</span>
                                                </li>
                                                <li class = 'old text-danger'>${amt}</li>
                                            </ul>
                                            <ul class = 'discount'>
                                                <li>&dash;${getPerPrice}%</li>
                                            </ul>
                                    </div>`;
                    } else {
                        template += `<div class="price">
                                            <ul class="lll">
                                                <li>${amt}</li>
                                            </ul>
                                    </div>`;
                    }
                    template += `<div class="col-md-12 col-6 rating">
                                            ${rating}
                                  </div>
                    </div>
                </div>`;
                    /* }else{
                         console.log('hello');
                     }*/
                }
            }
        }
        template += '</div>';
    } else {
       // alert('no product');
    }
    $(container).append(template);
}

function removeImage(url, id) {
    $.ajax({
        url: url + '?pix=' + id,
        type: "GET",
        success: function (result) {
            //$('div.loader').hide();
            if (result.status == 0) {
                $.notify('Successfully removed', 'success');
                $('.p').hide();
                $('#p4').show();
            } else {
                $.notify(result.message, 'error');
            }
        },
        error: function (e) {
            $.notify(e, 'error');
            //$('div.loader').hide();
        }
    });
}