//checks if an input field is empty
function fieldIsEmpty(element) {
    if ($(element).val().length == 0) {
        return true;
    }
    return false;
}

function anyFieldIsEmpty(element) {
    var errors = 0;
    $(element).each(function(i, obj) {
        if ($(element).val().length == 0) {
            errors++;
        }
        return errors;
    });
}

//checks if a string is an email
function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

//highlights a field showing error or success
function highlight(element, className) {
    $(element).addClass(className);
}
//highlights a field showing error or success
function removeHighlight(element, className) {
    $(element).removeClass(className);
}

function validate(element){
    var error = 0;
    $(element).each(function(i,obj){
        
    });
}