function addCategory(data){
    $.ajax({
 
        url: `${base_url}/fashion/product/addcategory`,
        type: "POST",
        contentType: "application/json; charset=utf-8",

        data: JSON.stringify(data),

        success: function(result) {
           console.log(result);



        },
        error: function(e) {
            console.log(e);
        }
    });

}

function addSubCategories(data){
    $.ajax({
 
        url: `${base_url}/fashion/product/addsubcategory`,
        type: "POST",
        contentType: "application/json; charset=utf-8",

        data: JSON.stringify(data),

        success: function(result) {
           console.log(result);



        },
        error: function(e) {
            console.log(e);
        }
    });

}

function addStyles(data){
    $.ajax({
 
        url: `${base_url}/fashion/product/addstyle`,
        type: "POST",
        contentType: "application/json; charset=utf-8",

        data: JSON.stringify(data),

        success: function(result) {
           console.log(result);
            if(result.data.status == 'succcess'){
                $.notify('Styles added successfully', 'success');
            }


        },
        error: function(e) {
            console.log(e);
        }
    });

}


function getMainCategories(){
    var temp;
    $.ajax({
 
        url: `${base_url}/fashion/product/getcategories`,
        type: "GET",
        async: false,
        success: function(result) {
        
         temp = result;
         



        },
        error: function(e) {
            console.log(e);
        }
    });
    return temp;
}
function getMainSubCategories(categoryId){
   
    $.ajax({ 
        url: `/subcategory?id=${categoryId}`,
        type: "GET",       
        success: function(result) {        
        console.log(result);         
        },
        error: function(e) {
            console.log(e);
        }
    });
    
}

function showCategory(holder){
    $.ajax({
        
        url: `${base_url}/fashion/product/getcategories`,
        type: "GET",
        success: function(result) {
            var html = '';            
            for (i = 0; i < result.data.categories.length; i++) {
                html += ` <a class="nav-item nav-link " id='${result.data.categories[i].categoryName}' href="/fashion/category?cat=${result.data.categories[i].categoryName}&id=${result.data.categories[i].id}">${result.data.categories[i].categoryName}</a>`;
            }
            $(holder).append(html);
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function showSubCategory(holder){
    $.ajax({
        
        url: `${base_url}/fashion/product/getsubcategories`,
        type: "GET",
        success: function(result) {
            // var html = '';            
            // for (i = 0; i < result.data.categories.length; i++) {
            //     html += ` <a class="nav-item nav-link " id='${result.data.categories[i].categoryName}' href="/fashion/category?cat=${result.data.categories[i].categoryName}&id=${result.data.categories[i].id}">${result.data.categories[i].categoryName}</a>`;
            // }
            console.log(result);
            $(holder).append(html);
        },
        error: function(e) {
            console.log(e);
        }
    });
}