$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() >= 1)
            $('.header').addClass('scrolled');
        else
            $('.header').removeClass('scrolled');
    });

    let owl = $("#inspireSlide");

    owl.owlCarousel({
        navigation: false,
        loop: true,
        dots: false,
        items: 1,
        autoplay: true,
        //transitionStyle: "backSlide",
        //autoplayTimeout: 3000,
        animateOut: 'slideOutLeft',
        animateIn: 'fadeInLeft',
        autoplayHoverPause: false
    });

    $('#myModal').modal({
        keyboard: false,
        backdrop: "static",
        show: false
    });

    /**
     * Initalize wow js
     */
    let wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
        callback: function (box) {
            // the callback is fired every time an animation is started the argument that is
            // passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    });
    wow.init();

    $('[name="robot-gender"]').change(function () {
        if ($('.gender-m').is(":checked")) {
            $('#nextBtnM').show(500);
            $('#nextBtnF').hide(500)
        } else if($('.gender-f').is(":checked")) {
            $('#nextBtnF').show(500);
            $('#nextBtnM').hide(300);
        }
    });
});

let TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function() {
    let i = this.loopNum % this.toRotate.length;
    let fullTxt = this.toRotate[i];

    if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    let that = this;
    let delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
    }

    setTimeout(function() {
        that.tick();
    }, delta);
};

window.onload = function() {
    var elements = document.getElementsByClassName('typewrite');
    for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
            new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
    document.body.appendChild(css);
};